﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Common
{
    public class TraderdockTimeManager
    {
        static string TimeFormat = "HH:mm:ss";
        static string TimeFormatios = "hh:mm tt";
        static string TimeFormatNoSec = "HH:mm";
        static string dateFormat = "yyyy-MM-dd";
        static string dateFormatios = "dd-MM-yyyy";
        static string dateTimeFormat = "yyyy-MM-dd-HH:mm:ss.fff";
        static string UIDateTimeFormat = "yyyy-MMM-dd-hh:mm";
        static string PhotoBookTimeFormat = "yyyy-MM-dd HH:mm";
        static string UIDateFormat = "MMM. dd, yyyy";
        public static string GetTimeString(DateTime time)
        {
            return time.ToString(TimeFormat);
        }
        public static string GetTimeStringios(DateTime time)
        {
            return time.ToString(TimeFormatios);
        }
        public static string GetTimeSecString(DateTime time)
        {

            return time.ToString(TimeFormatNoSec);
        }

        public static string GetDateTimeString(DateTime dateTime)
        {
            return dateTime.ToString(dateFormat);
        }

        public static string GetDateTimeStringios(DateTime dateTime)
        {
            return dateTime.ToString(dateFormatios);
        }

        public static string GetDateString(DateTime dateTime)
        {
            return dateTime.ToString(dateTimeFormat);
        }

        public string GetUIDateString(DateTime dateTime)
        {
            return dateTime.ToString(UIDateFormat);
        }

        public static string GetUIDateTimeString(DateTime dateTime)
        {
            return dateTime.ToString(UIDateTimeFormat);
        }

        public static string GetPhotoBookTimeString(DateTime dateTime)
        {
            return dateTime.ToString(PhotoBookTimeFormat);
        }

        public static DateTime GetDateFromString(string dateTimeString)
        {
            return DateTime.ParseExact(dateTimeString, dateTimeFormat, CultureInfo.InvariantCulture);
        }
        public static DateTime GetDateFromStringios(string dateTimeString)
        {
            return DateTime.ParseExact(dateTimeString, dateTimeFormat, CultureInfo.InvariantCulture);
        }

        public static DateTime GetUIDateFromString(string dateTimeString)
        {
            return DateTime.ParseExact(dateTimeString, UIDateTimeFormat, CultureInfo.InvariantCulture);
        }

        public DateTimeOffset GetDateTimeZoneWise(DateTime dt, string TimeZone)
        {
            //get date time offset for UTC date stored in the database
            DateTimeOffset dbDateTimeOffset = new DateTimeOffset(dt, TimeSpan.Zero);
            //get user's time zone from profile stored in the database
            TimeZoneInfo userTimeZone = TimeZoneInfo.FindSystemTimeZoneById(TimeZone);
            //convert  db offset to user offset                
            DateTimeOffset userDateTimeOffset = TimeZoneInfo.ConvertTime(dbDateTimeOffset, userTimeZone);

            return userDateTimeOffset;
        }

        public static string DateTimeToUnixTimestamp(DateTime? dateTime, string sTimeZone = "")
        {
            DateTime dateTimeNew = new DateTime();
            if (dateTime == DateTime.MinValue || dateTime == null)
            {
                dateTime = DateTime.MinValue;
                dateTimeNew = DateTime.MinValue;
                return "0";
            }
            else
            {
                dateTimeNew = Convert.ToDateTime(dateTime);
            }
            double dttimestmp;
            DateTime unixStart = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            long unixTimeStampInTicks = 0;
            if (!string.IsNullOrEmpty(sTimeZone) && sTimeZone == "UTC")
            {
                unixTimeStampInTicks = (dateTimeNew - unixStart).Ticks;
            }
            else
            {
                unixTimeStampInTicks = (dateTimeNew.ToUniversalTime() - unixStart).Ticks;
            }
            dttimestmp = (double)unixTimeStampInTicks / TimeSpan.TicksPerSecond;
            long d = (long)Math.Round(dttimestmp, 0);
            return d.ToString().PadRight(19, '0');
        }
    }
}
