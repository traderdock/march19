﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Common
{
    public class ServiceResponse<T>
    {
        public ServiceResponse()
        {
            Errors = new List<string>();
        }
        public List<string> Errors { get; set; }

        public bool HasError { get { return Errors.Count > 0; } }

        public T Result { get; set; }

        public bool IsLastPage { get; set; }

        public bool IsRecordExist { get; set; }
    }
}
