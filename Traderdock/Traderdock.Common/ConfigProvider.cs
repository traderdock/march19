﻿using System;
using System.Configuration;

namespace Traderdock.Common
{
    public class ConfigProvider
    {
        public static string SmtpHost
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpHost"].ToString();
            }
        }

        public static string SmtpUID
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpUid"].ToString();
            }
        }

        public static string SmtpPass
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpPass"].ToString();
            }
        }

        public static int SmtpPort
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["smtpPort"].ToString());
            }
        }

        public static string ErrorLogPath
        {
            get
            {
                return ConfigurationManager.AppSettings["errorLog"].ToString();
            }
        }

        public static string AppLogPath
        {
            get
            {
                return ConfigurationManager.AppSettings["appLog"].ToString();
            }
        }

        public static string UploadFilePath
        {
            get
            {
                return ConfigurationManager.AppSettings["uploadFilePath"].ToString();
            }
        }
    }
}
