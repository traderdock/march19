﻿using System;

namespace Traderdock.Common.Exceptions
{
    public class ServiceLayerException : ExceptionsBase
    {
        public ServiceLayerException()
        {

        }

        public ServiceLayerException(Exception ex)
            : base(ex)
        {
            
        }
    }
}
