﻿using System;
using Traderdock.Common.FileWriters;

namespace Traderdock.Common.Exceptions
{
    public class ExceptionsBase : Exception
    {
        protected ExceptionsBase()
        {

        }
        public ExceptionsBase(Exception ex)
           : base("There was some error at the server. Please contact your administrator.", ex)
        {
            //Write to the file
            ErrorWriter writer = new ErrorWriter();
            writer.Write(ex);
            writer = null;

            //Cleanup code
            ex = null;
        }
    }
}
