﻿namespace Traderdock.Common.Constants
{
    public static class TTAPIEndPoints
    {
        public const string baseURL = "https://apigateway.trade.tt/";
        public const string tokenURL = "/token";
        public const string CreateAccountURL = "/account";
        public const string CreateUserURL = "/user";
        public const string GetCountryList = "/user/countryIds";
        public const string GetStateList = "/user/stateIds";
        public const string CreateAccountUserURL = "/accountuser";
        public const string ProductList = "/products?marketId=7"; //only for CME Market, market id=7[CME]
        public const string AccountRiskLimitProduct = "/account/risklimit/product";
        public const string AccountRiskSetting = "/account/risksettings";
        public const string AccountRiskLimitSetting = "/account/risklimitsettings";
        public const string GetFII = "/fills";
        public const string GetPosition = "/position";
        public const string AccountTradeRestrictions = "/account/traderestrictions";
        public const string GetAccount = "/account";
        public const string Instruments = "/instruments";
        public const string InstrumentDetails = "/instrument";
        public const string AccountList = "/accounts";
    }
}
