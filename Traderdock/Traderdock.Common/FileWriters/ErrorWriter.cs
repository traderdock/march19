﻿using System;
using System.IO;
using Traderdock.Common.Enumerations;

namespace Traderdock.Common.FileWriters
{
    public class ErrorWriter : FileWritersBase
    {
        public ErrorWriter()
           : base(LogWriterType.Error)
        {
        }

        public void Write(Exception ex)
        {
            try
            {
                this.Write(FormateErrorLog(ex));
            }
            catch (Exception)
            {
                throw ;
            }
        }
       
        private string FormateErrorLog(System.Exception ex)
        {
            StringWriter sw = new StringWriter();
            try
            {
                sw.WriteLine("===============================================");
                sw.WriteLine("==================Start========================");
                sw.WriteLine("Time           : " + DateTime.Now.ToString());
                sw.WriteLine("Machine Name   : " + Environment.MachineName);
                sw.WriteLine("User Name      : " + Environment.UserDomainName + "\\" + Environment.UserName);
                sw.WriteLine("OS Version     : " + Environment.OSVersion);
                sw.WriteLine("Message        : " + ex.Message);
                sw.WriteLine("StackTrace     : ");
                sw.WriteLine(ex.StackTrace);
                sw.WriteLine("InnerException : " + (ex.InnerException == null ? string.Empty : (ex.InnerException.InnerException == null ? ex.InnerException.Message : ex.InnerException.InnerException.Message)));
                sw.WriteLine("HelpLink       : " + ex.HelpLink);
                sw.WriteLine("===================End=========================");
            }
            catch (Exception)
            {
                throw;
            }
            return sw.ToString();
        }
    }
}
