﻿using System;
using Traderdock.Common.Enumerations;

namespace Traderdock.Common.FileWriters
{
    public class FileWritersBase
    {
        private LogWriterType _writerType;

        public FileWritersBase(LogWriterType writerType)
        {
            _writerType = writerType;
        }

        protected void Write(string message)
        {
            String filepath = null;
            string folder ="c:\\WebsiteLog\\";
            if (_writerType == LogWriterType.Error)
            {
                filepath = folder + "\\Errors\\TraderdockError_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            }
            else
            {
                filepath = folder + "\\Logs\\TraderdockLog_" + DateTime.Now.ToString("yyyy_MM_dd") + ".txt";
            }

            System.IO.File.AppendAllText(filepath, message);
        }

    }
}
