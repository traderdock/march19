﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumTradingPlatform
    {
        Rithmic = 1,
        TradingTechnologies = 2
    }
}
