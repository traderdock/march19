﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Common.Enumerations
{
    public enum EnumAWPaymentType
    {
        Subscription,
        ResetFee
    }
}
