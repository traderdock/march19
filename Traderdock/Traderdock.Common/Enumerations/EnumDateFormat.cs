﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumDateFormat
    {
        Normal = 1,
        NanoSec = 2,
        MiliSec = 3,
        MicroSec = 4,
    }
}
