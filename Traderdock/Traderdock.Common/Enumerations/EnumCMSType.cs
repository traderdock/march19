﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumCMSType
    {
        TermsOfService = 1,
        PrivacyPolicy = 2,
        Help = 3,
        Traderdock = 4,
        AboutUs = 5,
        ContactUs = 6,
        SocialSiteLink = 7,
        FAQ = 8,
        WhyTraderDock1 = 9,//home page content
        WhyTraderDock2 = 10,//home page content
        WhyTraderDock3 = 11,//home page content
        WhyTraderDock4 = 12,//home page content
        WhyTraderDock5 = 13,//home page content
        Bannertext = 14,//home page content
        WebPanelFooter = 15,//home page content
        Step1_Challenge = 16,//home page content
        Step1_Platform = 17,//home page content
        Step2_ShowExcel = 18,//home page content
        Step3_TraderDockCash = 19,//home page content
        LoginCMS = 20,//home page content
        SignUpCMS = 21,//home page content
        AboutUsMision = 22,//home page content
        AboutUsVision = 23,//home page content
        SatisfactionPolicy = 24,//home page content
        Facebook = 25,//home page content
        Twitter = 26,//home page content
        GooglePlus = 27,//home page content
        Linkedin = 28,//home page content
        Youtube = 29,//home page content
        //SatisfactionPolicy = 24,//home page content
        //SatisfactionPolicy = 24,//home page content
        SocialSiteData = 30,
        SubscriptionAgreement = 31,//complete profile page content
        SelfCertification = 32,//complete profile page content
        Trading_ObjectivesAndParameters = 34,//home page content added on 11-09-2017
        GlobalTeam = 35,//home page content added on 08-12-2017 
        LearnBannerText = 36, //learn how page content added on 08-12-2017 
        LearnStep1Text = 37, //learn how page content added on 08-12-2017 
        LearnStep2Text = 38, //learn how page content added on 08-12-2017 
        LearnStep3Text = 39, //learn how page content added on 08-12-2017 
        FAQSBannerText = 40, //learn how page content added on 08-12-2017 
        HowtotradewithTraderdock = 41, //home page content added on 11-12-2017 
        Takethetraderdockchallenge = 42, //home page content added on 11-12-2017 
        Showusyouexcel = 43, //home page content added on 11-12-2017 
        Beginlivetrading = 44, //home page content added on 11-12-2017 
        WhyTraderdock = 45, //home page content added on 11-12-2017 
        BannerTextStep2 = 46, //home page content added on 11-12-2017 
        BannerTextStep3 = 47, //home page content added on 11-12-2017 
        BannerTextContactUs = 48, //home page content added on 11-12-2017 
        FAQForEach = 49, //home page content added on 11-12-2017
        LayoutFooterText = 50, //layout page content added on 12-12-2017
        ProfitTableLearnHowPage = 51, //Profit table on learn how page 23-Feb-2018 (by mijaki shoaib)
    }
}
