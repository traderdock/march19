﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumSubscriptionMode
    {
        Free_Trial = 1,
        Subscribed = 2
    }
}
