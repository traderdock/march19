﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumAPI
    {
        FetchInstruments = 1,
        SaveInstruments = 2,
        FetchGetFill = 3,
        SaveGetFill = 4,
        RecordIdDuplicte = 5,
        InstrumentNotFound = 6,
        DisplayFactor = 7,
        InstrumentDetails = 8

    }
}
