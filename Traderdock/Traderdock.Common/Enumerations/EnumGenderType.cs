﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumGenderType
    {
        Female = 0,
        Male = 1
    }
}
