﻿namespace Traderdock.Common.Enumerations
{
    public enum EnumStatusMasterType
    {
        Active = 1,
        InActive = 2,
        Pending = 3,
        Deleted = 4,
        Deactivate = 5
    }
}
