﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;

namespace Traderdock.Common
{
    public class Utilities
    {

        #region Send Mail
        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="to"> Contains Mail Send To Email </param>
        /// <param name="emailTemplateName"> Contains EmailTemplateName </param>
        /// <param name="parameterValues"> Contains Parameter Values </param>
        /// <param name="attachmentPaths"> Contains Attachment Paths </param>
        /// <returns> returns True/False With Object Of Service Response </returns>
        public bool SendEmail(string to, string emailTemplateName, Dictionary<string, string> parameterValues, params string[] attachmentPaths)
        {
            bool response = new bool();
            try
            {
                StringBuilder path = new StringBuilder();
                path.Append(HttpContext.Current.Server.MapPath("~"));
                path.Append("\\EmailTemplates\\" + emailTemplateName + ".html");

                var body = System.IO.File.ReadAllText(path.ToString());
                if (body != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(body);
                    foreach (var parameter in parameterValues)
                    {
                        sb = sb.Replace(parameter.Key, parameter.Value);
                    }
                    string sBody;
                    sBody = Convert.ToString(sb);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }

            return response;
        }

        public static bool SendMail(string to, string subject, string body, string[] attachmentPaths = null)
        {
            bool status = true;
            try
            {
                MailMessage message = new MailMessage();
                message.To.Add(new MailAddress(to));
                message.Subject = subject;
                message.From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]);
                message.Body = body;
                message.IsBodyHtml = true;
                message.ReplyTo = new MailAddress(ConfigurationManager.AppSettings["ReplyTo"]);
                if (attachmentPaths != null)
                {
                    foreach (string attachmentPath in attachmentPaths)
                    {
                        if (!string.IsNullOrEmpty(attachmentPath))
                        {
                            var stream = new WebClient().OpenRead(attachmentPath);
                            System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, Path.GetFileName(attachmentPath));
                            message.Attachments.Add(attachment);
                        }
                    }
                }

                var smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPServer"];
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.UseDefaultCredentials = false;
                ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                System.Security.Cryptography.X509Certificates.X509Chain chain,
                System.Net.Security.SslPolicyErrors sslPolicyErrors)
                {
                    return true;
                };
                smtp.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["Password"]);

                smtp.Send(message);
            }
            catch (Exception)
            {
                throw;
            }

            return status;
        }
        #endregion

        #region Generate Key
        /// <summary>
        /// Generate Key
        /// </summary>
        /// <returns> returns Key Generated </returns>
        private static Random random = new Random();
        public static string GenerateKey()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, 8)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
        #endregion

        #region Encrypt Password
        /// <summary>
        /// Encrypt Password
        /// </summary>
        /// <param name="cPassWord"> Contains Password To Encrypt </param>
        /// <param name="Key"> Contains Key To Encrypt </param>
        /// <returns> returns Encrypted Password </returns>
        public static string EncryptPassword(string cPassWord, string Key)
        {

            string EncryptionKey = Key;
            byte[] clearBytes = Encoding.Unicode.GetBytes(cPassWord);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    cPassWord = Convert.ToBase64String(ms.ToArray());
                }
            }
            return cPassWord;
        }
        #endregion

        #region Decrypt Password
        /// <summary>
        /// Decrypt Password
        /// </summary>
        /// <param name="cPassWord"> Contains Encrypted Password </param>
        /// <param name="key"> Contains Key For Decrypt </param>
        /// <returns> returns Decrypted Password </returns>
        public static string DecryptPassword(string cPassWord, String key)
        {
            string EncryptionKey = key;
            cPassWord = cPassWord.Replace(" ", "+");
            int mod4 = cPassWord.Length % 4;
            if (mod4 > 0)
            {
                cPassWord += new string('=', 4 - mod4);
            }
            byte[] cipherBytes = Convert.FromBase64String(cPassWord);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[]
                     { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(),
                                               CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cPassWord = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cPassWord;
        }
        #endregion

        #region Decrypt Key
        /// <summary>
        /// Decrypt Password
        /// </summary>
        /// <param name="cPassWord"> Contains Encrypted Password </param>
        /// <param name="key"> Contains Key For Decrypt </param>
        /// <returns> returns Decrypted Password </returns>
        public static string DecryptKey(string cPassWord, String key)
        {

            cPassWord = cPassWord.Replace("-", "+");
            cPassWord = cPassWord.Replace("_", "/");
            cPassWord = cPassWord.Replace(".", "=");
            string EncryptionKey = key;
            byte[] cipherBytes = Convert.FromBase64String(cPassWord);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[]
                     { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(),
                                               CryptoStreamMode.Write))
                    {
                        cs.Write(cipherBytes, 0, cipherBytes.Length);
                        cs.Close();
                    }
                    cPassWord = Encoding.Unicode.GetString(ms.ToArray());
                }
            }
            return cPassWord;
        }
        #endregion

        #region Encrypt Key
        /// <summary>
        /// Encrypt Password
        /// </summary>
        /// <param name="cPassWord"> Contains Password To Encrypt </param>
        /// <param name="Key"> Contains Key To Encrypt </param>
        /// <returns> returns Encrypted Password </returns>
        public static string EncryptKey(string cPassWord, string Key)
        {

            string EncryptionKey = Key;
            byte[] clearBytes = Encoding.Unicode.GetBytes(cPassWord);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    cPassWord = Convert.ToBase64String(ms.ToArray());
                }
            }
            cPassWord = cPassWord.Replace("+", "-");
            cPassWord = cPassWord.Replace("/", "_");
            cPassWord = cPassWord.Replace("=", ".");
            //cPassWord.Replace("+", "-");
            //cPassWord.Replace("/", "_");
            //cPassWord.Replace("=", ".");
            return cPassWord;
        }
        #endregion

        #region Get Image Path
        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <returns></returns>
        public static string GetImagePath()
        {
            return ConfigurationManager.AppSettings["SiteLink"];
        }
        #endregion

        #region Get Image Name
        /// <summary>
        /// Get Image Name
        /// </summary>
        /// <param name="imageURL"></param>
        /// <returns></returns>
        public static string GetImageName(string imageURL)
        {
            return Path.GetFileName(imageURL);
        }
        #endregion

        #region Save Image File
        public static string SaveImageFile(HttpPostedFileBase p_File)
        {
            try
            {
                if (p_File != null)
                {
                    string _Extension = System.IO.Path.GetExtension(p_File.FileName);
                    string _FileName = GetUniqFileName(p_File.FileName);
                    var _FolderPath = HttpContext.Current.Server.MapPath("~") + "\\Images\\";
                    if (!System.IO.Directory.Exists(_FolderPath))
                        System.IO.Directory.CreateDirectory(_FolderPath);
                    p_File.SaveAs(Path.Combine(_FolderPath, _FileName));

                    return _FileName;
                }
            }
            catch (Exception _Exception)
            {
                throw _Exception;
            }
            return string.Empty;
        }

        public static string GetUniqFileName(string p_OrigFileName)
        {
            try
            {
                if (!string.IsNullOrEmpty(p_OrigFileName))
                {
                    string ext = Path.GetExtension(p_OrigFileName);
                    return Convert.ToString(DateTime.Now.Ticks + ext);
                }
                return Convert.ToString(DateTime.Now.Ticks);
            }
            catch (Exception _Exception)
            {
                throw _Exception;
            }
        }
        #endregion

        #region Read All Text
        /// <summary>
        /// Read All Text
        /// </summary>
        /// <param name="fileUrl"></param>
        /// <returns></returns>
        public static string ReadAllText(string fileUrl)
        {
            string content = string.Empty;
            try
            {
                if (string.IsNullOrWhiteSpace(fileUrl))
                {
                    return content;
                }

                string fileName = Path.GetFileName(fileUrl);
                string actualFileName = HttpContext.Current.Server.MapPath("~") + "\\EmailTemplates\\" + fileName;
                content = File.ReadAllText(actualFileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return content;
        }
        #endregion

        #region Write All Text
        /// <summary>
        /// Write All Text
        /// </summary>
        /// <param name="fileUrl"></param>
        /// <param name="content"></param>
        public static void WriteAllText(string fileUrl, string content)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(fileUrl))
                {
                    return;
                }

                string fileName = Path.GetFileName(fileUrl);
                string actualFileName = HttpContext.Current.Server.MapPath("~") + "\\EmailTemplates\\" + fileName;
                File.WriteAllText(actualFileName, content);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get FirstDate & Last Date
        /// <summary>
        /// Get First Last Date
        /// </summary>
        /// <param name="Currentyear"></param>
        /// <param name="Currentmonth"></param>
        /// <param name="WeekNumber"></param>
        /// Date : 19/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public static FirstLast GetFirstLast(int Currentyear = 0, int Currentmonth = 0, int WeekNumber = 0)
        {
            var firstDay = DateTime.UtcNow;
            var lastDay = DateTime.UtcNow;
            FirstLast obj = new FirstLast();

            if (Currentyear > 0 && Currentmonth > 0 && WeekNumber > 0)
            {
                //// Get the weeks in a month

                //DateTime date = DateTime.Today;
                //// first generate all dates in the month of 'date'
                //var dates = Enumerable.Range(1, DateTime.DaysInMonth(date.Year, date.Month)).Select(n => new DateTime(date.Year, date.Month, n));
                //// then filter the only the start of weeks
                //var weekends = from d in dates
                //               where d.DayOfWeek == DayOfWeek.Monday
                //               select d;
                obj = new FirstLast();

                DayOfWeek day = DayOfWeek.Monday;
                DateTime First;
                DateTime startOfMonth = new DateTime(Currentyear, Currentmonth, 1);
                DateTime endOfMonth = new DateTime(Currentyear, Currentmonth ,1).AddMonths(1).AddDays(-1);
                int daysToFirstCorrectDay = (((int)day - (int)startOfMonth.DayOfWeek) + 7) % 7;
                if (WeekNumber == 1)
                {
                    First = startOfMonth;
                }
                else
                {
                    First = startOfMonth.AddDays(7 * (WeekNumber - 2) + daysToFirstCorrectDay);
                }
                DateTime End = startOfMonth.AddDays(7 * (WeekNumber - 2) + daysToFirstCorrectDay).AddDays(6);
                if (endOfMonth < End)
                {
                    End = endOfMonth;
                }
                firstDay = new DateTime(Currentyear, Currentmonth, First.Day);
                lastDay = new DateTime(Currentyear, Currentmonth, End.Day);

                obj.firstDay = firstDay;
                obj.lastDay = lastDay;
                return obj;
            }
            else if (Currentyear > 0 && Currentmonth > 0 && WeekNumber == 0)
            {
                obj = new FirstLast();

                DateTime startOfMonth = new DateTime(Currentyear, Currentmonth, 1);
                DateTime First = startOfMonth;
                DateTime End = startOfMonth.AddMonths(1).AddDays(-1);

                if (DateTime.Now.Day < End.Day && DateTime.Now.Month == End.Month)
                    End = DateTime.Now;

                firstDay = new DateTime(Currentyear, Currentmonth, First.Day);
                lastDay = new DateTime(Currentyear, Currentmonth, End.Day);

                obj.firstDay = firstDay;
                obj.lastDay = lastDay;
                return obj;
            }
            else if (Currentyear > 0 && Currentmonth == 0 && WeekNumber == 0)
            {
                obj = new FirstLast();

                DateTime startOfMonth = new DateTime(Currentyear, 1, 1);
                DateTime First = startOfMonth;
                DateTime End = startOfMonth.AddMonths(12).AddMonths(-1).AddDays(31).AddDays(-1);

                firstDay = new DateTime(Currentyear, 1, First.Day);
                lastDay = new DateTime(Currentyear, 12, End.Day);

                obj.firstDay = firstDay;
                obj.lastDay = lastDay;
                return obj;
            }

            return obj;
        }
        #endregion

        #region Get Week Number By String Week
        /// <summary>
        /// Get Week Number By String Week
        /// </summary>
        /// <param name="month"></param>
        /// Date : 19/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public static int weekNumberbyString(string month = "")
        {
            int WeekNumber = 0;
            if (month == "First")
            {
                WeekNumber = 1;
            }
            else if (month == "Second")
            {
                WeekNumber = 2;
            }
            else if (month == "Third")
            {
                WeekNumber = 3;
            }
            else if (month == "Fourth")
            {
                WeekNumber = 4;
            }
            else if (month == "Fifth")
            {
                WeekNumber = 5;
            }
            else if (month == "Last")
            {
                WeekNumber = 6;
            }
            return WeekNumber;
        }
        #endregion

        #region Get Month Number By String Month
        /// <summary>
        /// Get Month Number By String Month
        /// </summary>
        /// <param name="Month"></param>
        /// Date : 19/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public static int GetMonthbyString(string Month = "")
        {
            int Currentmonth = 0;

            if (Month == "January")
            {
                Currentmonth = 1;

            }
            else if (Month == "February")
            {
                Currentmonth = 2;

            }
            else if (Month == "March")
            {
                Currentmonth = 3;

            }
            else if (Month == "April")
            {
                Currentmonth = 4;

            }
            else if (Month == "May")
            {
                Currentmonth = 5;

            }
            else if (Month == "June")
            {
                Currentmonth = 6;

            }
            else if (Month == "July")
            {
                Currentmonth = 7;

            }
            else if (Month == "August")
            {
                Currentmonth = 8;

            }
            else if (Month == "September")
            {
                Currentmonth = 9;

            }
            else if (Month == "October")
            {
                Currentmonth = 10;

            }
            else if (Month == "November")
            {
                Currentmonth = 11;

            }
            else if (Month == "December")
            {
                Currentmonth = 12;

            }
            return Currentmonth;
        }
        #endregion
    }
}
