﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Common.Constants
{
    public static class EmailTemplateNames
    {
        public const string UserRegistration = "UserRegistration";
        public const string PasswordChange = "PasswordChange";
        public const string ForgotPassword = "ForgotPassword";
        public const string ForgotPasswordNew = "ForgotPasswordNew";
        public const string AccountDelete = "AccountDelete";
        public const string RithmicAccount = "RithmicAccount";
        public const string WelcomeMail = "WelcomeMail";
        public const string WelcomeFreeSubscription = "WelcomeFreeSubscription";
        public const string WelcomePaidSubscription = "WelcomePaidSubscription";
        public const string WelcomeFreeSubscriptionTT = "WelcomeFreeSubscriptionTT";
        public const string WelcomePaidSubscriptionTT= "WelcomePaidSubscriptionTT";
        public const string ResetAccountMember = "ResetAccountMember";
        public const string ResetAccountSupport = "ResetAccountSupport";
        public const string ResetAccountConfirmation = "ResetAccountConfirmation";
        public const string SupportMailForFreeAccount = "SupportMailForFreeAccount";
        public const string PaymentTransactionSuccess = "PaymentTransactionSuccess";
        public const string PaymentTransactionFailed = "PaymentTransactionFailed";
        public const string AdminPaymentTransaction = "AdminPaymentTransaction";
        public const string EndUserSubscription = "EndUserSubscription";
        public const string DailyAPILog = "DailyAPILog";
    }
}
