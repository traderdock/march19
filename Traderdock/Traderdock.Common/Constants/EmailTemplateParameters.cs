﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Common.Constants
{
    public static class EmailTemplateParameters
    {
        public const string FullName = "%%FULLNAME%%";
        public const string Password = "%%PASSWORD%%";
        public const string UserID = "%%USERID%%";
        public const string Email = "%%EMAIL%%";
        public const string link = "%%LINK%%";
        public const string Adminlink = "%%ADMINLINK%%";
        public const string Key1 = "%%KEY%%";
        public const string SubscriptionPlanName = "%%SUBSCRIPTIONPLANNAME%%";
        public const string SubscriptionDate = "%%SubscriptionDate%%";
        public const string Approvelink = "%%APPROVELINK%%";
        public const string AccountNumber = "%%ACCOUNTNUMBER%%";
        public const string Amount = "%%AMOUNT%%";
        public const string Platform = "%%PLATFORM%%";
        public const string USEREMAIL = "%%USEREMAIL%%";
        public const string Address = "%%ADDRESS%%";
        public const string ContactNumber = "%%CONTACTNUMBER%%";
        public const string SubscriptionModeStr = "%%SUBSCRIPTIONMODESTR%%";
        public const string FirstName = "%%FIRSTNAME%%";
        public const string LastName = "%%LASTNAME%%";
        public const string City = "%%CITY%%";
        public const string Country = "%%COUNTRY%%";
        public const string State = "%%STATE%%";
        public const string AccountAllias = "%%ACCOUNTALLIAS%%";
        public const string MailContent = "%%MAILCONTENT%%";
    }
}
