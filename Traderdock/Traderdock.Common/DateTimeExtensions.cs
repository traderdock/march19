﻿using System;
using System.Globalization;

namespace Traderdock.Common
{
    public static class DateTimeExtensions
    {
        #region Get Week Of Month
        /// <summary>
        /// Get Week Of Month
        /// </summary>
        /// <param name="time"></param>
        /// Date : 19/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public static int GetWeekOfMonth(this DateTime time)
        {
            DateTime first = new DateTime(time.Year, time.Month, 1);
            return time.GetWeekOfYear() - first.GetWeekOfYear() + 1;
        }

        static int GetWeekOfYear(this DateTime time)
        {
            GregorianCalendar _gc = new GregorianCalendar();
            return _gc.GetWeekOfYear(time, CalendarWeekRule.FirstDay, DayOfWeek.Sunday);
        }
        #endregion
    }
}
