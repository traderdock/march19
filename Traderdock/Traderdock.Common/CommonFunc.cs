﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;

namespace Traderdock.Common
{
    public static class CommonFunc
    {
        public static string LogPath = System.Configuration.ConfigurationManager.AppSettings["LogPath"];
        //public static async Task GenericEmailSend(EmailViewModel model)
        //{
        //    var message = new MailMessage();
        //    foreach (MailAddress ma in model.Recepients)
        //    {
        //        message.To.Add(ma);
        //    }
        //    message.From = new MailAddress(ConfigProvider.SmtpUID);
        //    message.Subject = model.Subject;
        //    message.Body = model.Message;
        //    message.IsBodyHtml = true;

        //    using (var smtp = new SmtpClient())
        //    {
        //        var credential = new NetworkCredential
        //        {
        //            UserName = ConfigProvider.SmtpUID,
        //            Password = ConfigProvider.SmtpPass,
        //        };
        //        smtp.Credentials = credential;
        //        smtp.Host = ConfigProvider.SmtpHost;
        //        smtp.Port = ConfigProvider.SmtpPort;
        //        smtp.EnableSsl = true;
        //        await smtp.SendMailAsync(message);
        //    }
        //}

        public static void WriteLog(string message, LogWriterType logType)
        {
            try
            {
                
                string folderpath = ((logType == LogWriterType.Error) ? ConfigProvider.ErrorLogPath : ConfigProvider.AppLogPath);
                string filePath = folderpath + "Log_" + DateTime.Now.ToString("dd-MM-yyyy") + ".txt";
                if (!Directory.Exists(folderpath))
                {
                    Directory.CreateDirectory(folderpath);
                }
                using (TextWriter tw = new StreamWriter(filePath, true))
                {
                    tw.WriteLine((logType == LogWriterType.Error ? "Error occured" : "App log") + " at: " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"));
                    tw.WriteLine(message);
                    tw.WriteLine("================================================================" +
                        "=========================================================================" +
                        "=========================================================================");
                    tw.Close();
                }
            }
            catch (Exception ex)
            {
                new ServiceLayerException(ex);
            }
        }

        public static void CreateErrorLog(Exception ex, string controller, string action)
        {
            StringBuilder errorLog = new StringBuilder();
            errorLog.AppendLine("Controller: " + controller + " Action: " + action);
            errorLog.AppendLine(ex.Message);
            errorLog.AppendLine(ex.StackTrace);
            WriteLog(errorLog.ToString(), LogWriterType.Error);
        }

        public static void CreateAppLog(string message, string methodName)
        {
            StringBuilder appLog = new StringBuilder();
            appLog.AppendLine("Method: " + methodName);
            appLog.AppendLine(message);
            WriteLog(appLog.ToString(), LogWriterType.Log);
        }

        public static string UploadFile(string fileName, string base64string)
        {
            string folderpath = HttpContext.Current.Server.MapPath(ConfigProvider.UploadFilePath);
            string filePath = folderpath + fileName;
            if (!Directory.Exists(folderpath))
            {
                Directory.CreateDirectory(folderpath);
            }
            File.WriteAllBytes(filePath, Convert.FromBase64String(base64string));
            return fileName;
        }

        public static void DeleteFile(string fileName)
        {
            string filePath = HttpContext.Current.Server.MapPath(ConfigProvider.UploadFilePath) + fileName;
            if (File.Exists(filePath))
            {
                File.Delete(filePath);
            }
        }

        public static List<Int64> GetIdsFromString(string sIds)
        {
            string s = sIds.Replace(@"chkSelectAll,", @"");
            List<Int64> lIds = new List<long>();
            int[] value1 = s.Split(',').Select(n => Convert.ToInt32(n)).ToArray();
            for (int i = 0; i < value1.Length; i++)
            {
                lIds.Add(value1[i]);
            }
            return lIds;
        }

        public static List<T> ConvertDataTable<T>(DataTable dt)
        {
            List<T> data = new List<T>();
            foreach (DataRow row in dt.Rows)
            {
                T item = GetItem<T>(row);
                data.Add(item);
            }
            return data;
        }
        private static T GetItem<T>(DataRow dr)
        {
            Type temp = typeof(T);
            T obj = Activator.CreateInstance<T>();

            foreach (DataColumn column in dr.Table.Columns)
            {
                foreach (PropertyInfo pro in temp.GetProperties())
                {
                    if (pro.Name == column.ColumnName)
                        pro.SetValue(obj, dr[column.ColumnName], null);
                    else
                        continue;
                }
            }
            return obj;
        }
        public static string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }
    }
}
