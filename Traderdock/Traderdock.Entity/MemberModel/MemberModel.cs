﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.MemberModel
{
    public class MemberModel
    {
        public long user_id { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter first name.")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter last name.")]
        public string last_name { get; set; }

        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = "Please enter valid email.")]
        [Required(ErrorMessage = "Please enter email address.")]
        public string user_email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter password.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be more than 6 characters.")]
        public string password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("password", ErrorMessage = "Password and confirm password not match.")]
        public string confirm_password { get; set; }

        //public string crypto_key { get; set; }
        [Display(Name = "Gender")]
        [Required(ErrorMessage = "Please select your gender.")]
        public Nullable<bool> gender { get; set; }

        public string gender_String { get; set; }

        [Display(Name = "Birth Date")]
        //[Required(ErrorMessage = "Please select birth date.")] //Commented on client suggestion (11-Oct-2017)
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string birth_date { get; set; }

        public string birth_date_string { get; set; }

        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please enter address.")]
        public string address { get; set; }

        [Display(Name = "Latitude")]
        public string latitude { get; set; }

        [Display(Name = "Longitude")]
        public string longitude { get; set; }

        [Display(Name = "City Name")]
        [Required(ErrorMessage = "Please enter city.")]
        public string city_name { get; set; }

        [Display(Name = "State Name")]
        [Required(ErrorMessage = "Please enter state.")]
        public string state_name { get; set; }

        //[Display(Name ="State")]
        //[Required(ErrorMessage = "Please select state.")]
        //public int? state_id { get; set; }

        [Display(Name = "Country Name")]
        public string country_name { get; set; }

        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please enter postal code.")]
        public string postal_code { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please select country.")]
        public int country_id { get; set; }

        [Display(Name = "Profile Image")]
        public string profile_image_url { get; set; }

        //[Display(Name = "Category Image")]
        //[Required(ErrorMessage = "Please select category image.")]
        //public string category_image { get; set; }

        //[Range(1, 1, ErrorMessage = "Please accept subscription agreement.")]
        //[Display(Name = "I accept the Market Data Subscription Agreement")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please accept subscription agreement.")]
        public bool chkSubscriptionAgreement { get; set; }

        //[Range(1, 1, ErrorMessage = "Please accept self certification.")]
        //[Display(Name = "I accept the Market Data Self - Certification")]
        [Range(typeof(bool), "true", "true", ErrorMessage = "Please accept self certification.")]

        public bool chkSelfCertification { get; set; }

        public string FileName { get; set; }

        [Display(Name = "Skype name")]
        public string SkypeName { get; set; }

        public bool ImageNotExist { get; set; }

        //public string country_code { get; set; }
        //[Display(Name = "Contact Number")]
        //[DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Please enter contact number.")]
        //[Required(ErrorMessage = "Please enter contact number.")]
        //public string contact_number { get; set; }
        //[RegularExpression("[^0-9]", ErrorMessage = "contact number must be number.")]

        public int user_type_id { get; set; }

        public string user_status { get; set; }

        public string call_for { get; set; }

        public long created_by { get; set; }

        public System.DateTime created_on { get; set; }

        public Nullable<long> modified_by { get; set; }

        public Nullable<System.DateTime> modified_on { get; set; }

        public bool is_active { get; set; }

        public bool is_blocked { get; set; }

        public bool is_deleted { get; set; }

        public int subscription_mode { get; set; }

        public bool is_number_verified { get; set; }

        public bool is_admin_verified { get; set; }

        public bool is_password_false { get; set; }

        public bool is_email_false { get; set; }

        public string crypto_key;

        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        [Required(ErrorMessage = "Please enter phone number.")]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number.")]
        public string contact_number { get; set; }
        //[RegularExpression("[^0-9]", ErrorMessage = "contact number must be number.")]
        
        public SelectList CountryList { get; set; }

        [AllowHtml]
        public string SubscriptionAgreement { get; set; } // enum  SignUpCMS = 21,  EnumCMSType

        [AllowHtml]
        public string SelfCertification { get; set; } // enum  SignUpCMS = 21,  EnumCMSType

        [AllowHtml]
        public string SignUpCMS { get; set; } // enum  SignUpCMS = 21,  EnumCMSType

        public SelectList GenderList { get; set; }

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please accept terms and conditions.")]
        public bool AcceptsTerms { get; set; }

        public long subscription_id { get; set; }

        public List<PaypalModel> lstPaymentHistory { get; set; }

        public SubscriptionDetails objSubsciptionModel { get; set; }

        public bool IsTrial { get; set; }

        public List<PlanMemModel> Subscription_List { get; set; }

        public SelectList SubscriptionPlanList { get; set; }

        public MemberModel()
        {
            Subscription_List = new List<PlanMemModel>();
        }

        //added on 27-feb-2018
        public string promo_text { get; set; }

        public int promo_code_id { get; set; }

        [Required(ErrorMessage = "Please select trading platform.")]
        public int rbPlatform { get; set; }

        public string AccountAllias { get; set; }

    }

    public class SubscriptionDetails
    {
        public long subscription_id { get; set; }
        public string subscription_name { get; set; }
        public decimal? starting_balance { get; set; }
        public int? max_position_size { get; set; }
        public decimal? daily_loss_limit { get; set; }
        public decimal? max_drawdown { get; set; }
        public decimal? profit_target { get; set; }
        public decimal? price_per_month { get; set; }
    }

}