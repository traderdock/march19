﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Traderdock.Entity.MemberModel
{
    public class CMSDataModel
    {
        [AllowHtml]
        public string cms_about_us { get; set; }

        [AllowHtml]
        public string cms_about_us_mission { get; set; }

        [AllowHtml]
        public string cms_about_us_vision { get; set; }

        [AllowHtml]
        public string cms_contact_us { get; set; }
        
        [AllowHtml]
        public string cms_satisfaction_policy { get; set; }

        [AllowHtml]
        public string cms_privacy_policy { get; set; }

        [AllowHtml]
        public string cms_terms { get; set; }

        [AllowHtml]
        public string BannerTextContactUs { get; set; }

        public List<string> BannerImages { get; set; }
    }
}
