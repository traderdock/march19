﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.MemberModel
{
    public class ContactUsModel
    {
        public long ctc_id { get; set; }

        [Required(ErrorMessage = "Please enter your name.")]
        [Display(Name = "Name")]
        public string name { get; set; }

        [Required(ErrorMessage = "Please enter email.")]
        [EmailAddress(ErrorMessage = "You have entered invalid email.")]
        [Display(Name = "Email")]
        public string email { get; set; }

        [Required(ErrorMessage = "Please enter message.")]
        [Display(Name = "Message")]
        public string message { get; set; }

        public List<string> BannerImages { get; set; }

        [AllowHtml]
        public string cms_contact_us { get; set; }

        [AllowHtml]
        public string BannerTextContactUs { get; set; }

        public int ctc_status_id { get; set; }

        public string cms_status { get; set; }

        public bool is_active { get; set; }

        public bool is_deleted { get; set; }

        public long modified_by { get; set; }
    }
}
