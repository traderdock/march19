﻿namespace Traderdock.Entity.MemberModel
{
    public class FAQModel
    {
        public long faq_id { get; set; }

        public long faq_category_id { get; set; }

        public string question { get; set; }

        public string answer { get; set; }

        //public bool is_active { get; set; }

        //public string faq_category_title { get; set; }

        //public bool is_deleted { get; set; }

        //public long created_by { get; set; }

        //public System.DateTime created_on { get; set; }

        //public long modified_by { get; set; }

        //public System.DateTime modified_on { get; set; }

        //public List<SelectListItem> FAQCategoryList { get; set; }

        //public string faqStatus { get; set; }

    }
}
