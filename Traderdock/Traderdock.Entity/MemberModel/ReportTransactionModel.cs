﻿using System;

namespace Traderdock.Entity.MemberModel
{
    public class ReportTransactionModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string account_number { get; set; }
        public long userID { get; set; }
        public long TransactionID { get; set; }
        public long? UserSubscriptionID { get; set; }
        public bool freeTrial { get; set; }
        public string strFreeTrial { get; set; }
        public bool isActive { get; set; }
        public Nullable<System.DateTime> Transaction_date { get; set; }
        public string TransactionDateString { get; set; }
        public int? TotalContracts { get; set; } = 0;
        public int? TotalTrades { get; set; } = 0;
        public decimal? GrossProfitLossOld { get; set; } = 0;
        public string GrossProfitLoss { get; set; } = "";
        public decimal? TotalCommisionsOld { get; set; } = 0;
        public string TotalCommisions { get; set; } = "";
        public decimal? NetProfitLossOld { get; set; } = 0;
        public string NetProfitLoss { get; set; } = "";
        public decimal? CurrentBalanceOld { get; set; } = 0;
        public string CurrentBalance { get; set; } = "";
        public decimal? HighBalanceOld { get; set; } = 0;
        public string HighBalance { get; set; } = "";
        public decimal? LowBalanceOld { get; set; } = 0;
        public string LowBalance { get; set; } = "";
        public decimal? AvgWinningDayOld { get; set; } = 0;
        public string AvgWinningDay { get; set; } = "";
        public decimal? AvgLosingDayOld { get; set; } = 0;
        public string AvgLosingDay { get; set; } = "";
        public decimal? BestDayOld { get; set; } = 0;
        public string BestDay { get; set; } = "";
        public decimal? WorstDayOld { get; set; } = 0;
        public string WorstDay { get; set; } = "";
      
        public decimal? HighProfitLossOld { get; set; } = 0;
        public string HighProfitLoss { get; set; } = "";
        public decimal? LowProfitLossOld { get; set; } = 0;
        public string LowProfitLoss { get; set; } = "";
        public decimal? AvgWinningTradeOld { get; set; } = 0;
        public string AvgWinningTrade { get; set; } = "";
        public decimal? AvgLosingTradeOld { get; set; } = 0;
        public string AvgLosingTrade { get; set; } = "";
        public decimal? WinningTradeOld { get; set; } = 0;
        public string WinningTrade { get; set; } = "";
        public decimal? ProfitTargetOld { get; set; } = 0;
        public string ProfitTarget { get; set; } = "";
        public decimal? DailyLossLimitOld { get; set; } = 0;
        public string DailyLossLimit { get; set; } = "";
        public decimal? MaxDrowdownOld { get; set; } = 0;
        public string MaxDrowdown { get; set; } = "";
        public decimal? MainAccountBalanceOld { get; set; } = 0;
        public string MainAccountBalance { get; set; } = "";
        public int? TradeRequiredNoOfDays { get; set; } = 0;
        public string AvgWLDuration { get; set; }
        public int MaxConsWin { get; set; }
        public int MaxConsLoss { get; set; }
        public decimal? LosingTradePerOld { get; set; } = 0;
        public string LosingTradePer { get; set; } = "";
        public decimal? WinningTradePerOld { get; set; } = 0;
        public string WinningTradePer { get; set; } = "";
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string AvgeWLDurationColor { get; set; } = "";
        public string AvgWinningDay_str { get; set; } = "--:--:--";
        public string AvgLosingDay_str { get; set; } = "--:--:--";
        public string Symbol { get; set; } = "";
        public string Exchange { get; set; } = "";
        public string EnquiryEmail { get; set; }
        
       
    }
}
