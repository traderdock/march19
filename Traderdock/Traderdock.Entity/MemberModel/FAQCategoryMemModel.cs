﻿using System.Collections.Generic;
using Traderdock.Entity.ViewModel;

namespace Traderdock.Entity.MemberModel
{
    public class FAQViewModel
    {
        public long sub_faq_category_id { get; set; }

        public string sub_category_title { get; set; }

        public string sub_category_description { get; set; }

        public FAQCategoryModel FAQCategoryModel { get; set; }

        public List<FAQModel> FAQList { get; set; }

        //public string category_image { get; set; }

        //[AllowHtml]
        //public string faq_description { get; set; }

        //public string FileName { get; set; }

        //public bool is_active { get; set; }

        //public long created_by { get; set; }

        //public System.DateTime created_on { get; set; }

        //public long modified_by { get; set; }

        //public System.DateTime modified_on { get; set; }

    }
}