﻿using System.Web.Mvc;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Traderdock.Entity.MemberModel
{
    public class LearnHowModel
    {
        [AllowHtml]
        public string Step1_Challenge { get; set; } // enum  Step1_Challenge = 16,  EnumCMSType

        [AllowHtml]
        public string Step1_Platform { get; set; } // enum  Step1_Platform = 17, EnumCMSType

        [AllowHtml]
        public string Step2_ShowExcel { get; set; } // enum  Step2_ShowExcel = 18, EnumCMSType

        [AllowHtml]
        public string Step3_TraderDockCash { get; set; } // enum  Step3_TraderDockCash = 19, EnumCMSType

        [AllowHtml]
        public string Trading_ObjectivesAndParameters { get; set; } // enum  Trading_ObjectivesAndParameters = 34, EnumCMSType

        [AllowHtml]
        public string ProfitTableLearnHowPage { get; set; } // enum  ProfitTableLearnHowPage = 51, EnumCMSType

        [AllowHtml]
        public string LearnBannerText { get; set; } // enum  TraderdockChallenge = 36, EnumCMSType

        [AllowHtml]
        public string LearnStep1Text { get; set; } // enum  TraderdockChallenge = 37, EnumCMSType

        [AllowHtml]
        public string LearnStep2Text { get; set; } // enum  TraderdockChallenge = 38, EnumCMSType

        [AllowHtml]
        public string LearnStep3Text { get; set; } // enum  TraderdockChallenge = 39, EnumCMSType

        [AllowHtml]
        public string BannerTextStep2 { get; set; } // enum  TraderdockChallenge = 46, EnumCMSType

        [AllowHtml]
        public string BannerTextStep3 { get; set; } // enum  TraderdockChallenge = 47, EnumCMSType

        public int subscription_id { get; set; }

        public List<PlanMemModel> Subscription_List { get; set; }

        public SelectList SubscriptionPlanList { get; set; }

        [Required(ErrorMessage = "Please enter first name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please select country.")]
        public int country_id { get; set; }

        public SelectList CountryList { get; set; }

        public int state_id { get; set; }

        public SelectList StateList { get; set; }

        public MemberModel MemberModel { get; set; }

        public string transaction_id { get; set; }

        public string PaymentIsSuccess { get; set; }

        [AllowHtml]
        public string Step1Slider { get; set; } // enum  Step1_Challenge = 16,  EnumCMSType

        [AllowHtml]
        public string Step2Slider { get; set; } // enum  Step1_Platform = 17, EnumCMSType

        [AllowHtml]
        public string Step3Slider { get; set; } // enum  Step2_ShowExcel = 18, EnumCMSType
    }
}
