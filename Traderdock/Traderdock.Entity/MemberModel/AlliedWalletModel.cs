﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.MemberModel
{
    public class AlliedWalletModel
    {
        public int PromoCodeId { get; set; }

        public long UserId { get; set; }

        public Entity.MemberModel.MemberModel User { get; set; }

        public string ApiUrl { get; set; }

        public string QuickPayToken { get; set; }

        public string MerchantId { get; set; }

        public string SiteId { get; set; }

        public string SubscriptionPlanId { get; set; }

        public string SubscriptionProductName { get; set; }

        public string ResetFeeProductName { get; set; }

        public string SubscriptionProductDescription { get; set; }

        public string ResetFeeProductDescription { get; set; }

        public string SuccessTransactionMessage { get; set; }

        public string CurrencyID { get; set; }

        public string SubscriptionApprovedUrl { get; set; }

        public string ResetFeeApprovedUrl { get; set; }

        public string ConfirmUrl { get; set; }

        public string DeclinedUrl { get; set; }

        public long SubscriptionId { get; set; }

        public string SubscriptionName { get; set; }

        public string TransactionId { get; set; }

        public decimal? OrderAmount { get; set; }

        public string PurchaseDate { get; set; }
        
        public string PaymentMethod { get; set; }

        public int? PaymentStatus { get; set; }

        public bool? IsExpired { get; set; }

        public bool? IsCurrent { get; set; }

        public DateTime? SubscriptionStartDate { get; set; }

        public DateTime? SubscriptionEndDate { get; set; }

        public DateTime? CreatedDate { get; set; }

        public long CreatedBy { get; set; }

        public bool IsSuccess { get; set; }

        public DateTime? UpdatedDate { get; set; }

        public long UpdatedBy { get; set; }

        public int? PaymentResponseCode { get; set; }

        public string PaymentResponseMessage { get; set; }

        public bool IsSave { get; set; }

        public string TransactionDate { get; set; }

        public string PaymentType { get; set; }

        public string PaymentIsSuccess { get; set; }

    }

    public class AlliedWalletOrder
    {
        public decimal Amount { get; set; }

        public string OrderId { get; set; }

        public string IteamName { get; set; }

        public string ReturnUrl { get; set; }

        public string CancelUrl { get; set; }
    }

    public class AlliedWalletRedirect
    {
        public string Url { get; set; }

        public string Token { get; set; }
    }
}
