﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.MemberModel
{
    public class AlliedWalletSettings
    {
        public static string Url => Get<string>("AW:ApiUrl");

        public static string MerchantID => Get<string>("AW:MerchantID");

        public static string SiteID => Get<string>("AW:SiteID");

        public static string QuickPayToken => Get<string>("AW:QuickPayToken");

        public static string SubscriptionPlanID200 => Get<string>("AW:SubscriptionPlanID200");

        public static string SubscriptionPlanID175 => Get<string>("AW:SubscriptionPlanID175");

        public static string SubscriptionPlanID150 => Get<string>("AW:SubscriptionPlanID150");

        public static string SubscriptionPlanID125 => Get<string>("AW:SubscriptionPlanID125");

        public static string SubscriptionProductName => Get<string>("AW:SubscriptionProductName");

        public static string ResetFeeProductName => Get<string>("AW:ResetFeeProductName");

        public static string SubscriptionProductDescription => Get<string>("AW:SubscriptionProductDescription");

        public static string ResetFeeProductDescription => Get<string>("AW:ResetFeeProductDescription");

        public static string SuccessTransactionMessage => Get<string>("AW:SuccessTransactionMessage");

        public static string CurrencyID => Get<string>("AW:CurrencyID");

        public static string ResetFeeAmount => Get<string>("AW:ResetFeeAmount");

        public static string SubscriptionApprovedUrl => Get<string>("AW:SubscriptionApprovedUrl");

        public static string ResetFeeApprovedUrl => Get<string>("AW:ResetFeeApprovedUrl");

        public static string ConfirmUrl => Get<string>("AW:ConfirmUrl");

        public static string DeclinedUrl => Get<string>("AW:DeclinedUrl");

        private static T Get<T>(string name)
        {
            string value = ConfigurationManager.AppSettings[name];

            if (value == null)
            {
                throw new Exception($"Could not find setting '{name}',");
            }

            return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
        }

    }
}
