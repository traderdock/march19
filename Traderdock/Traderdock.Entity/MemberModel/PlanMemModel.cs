﻿namespace Traderdock.Entity.MemberModel
{
    public class PlanMemModel
    {
        public long subscription_id { get; set; }

        public string currency_symbol { get; set; }

        public string subscription_name { get; set; }

        public string starting_balance { get; set; }

        public string balance_unit { get; set; }

        public string max_position_size { get; set; }

        public string daily_loss_limit { get; set; }

        public string max_drawdown { get; set; }

        public string profite_target { get; set; }

        public string price_per_month { get; set; }
    }
}
