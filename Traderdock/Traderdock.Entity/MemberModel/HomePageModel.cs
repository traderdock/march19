﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Traderdock.Entity.ViewModel;

namespace Traderdock.Entity.MemberModel
{
    public class HomePageModel
    {
        [AllowHtml]
        public string WhyTraderDock1 { get; set; } // enum  WhyTraderDock1 = 9,  EnumCMSType

        [AllowHtml]
        public string WhyTraderDock2 { get; set; } // enum  WhyTraderDock1 = 10, EnumCMSType

        [AllowHtml]
        public string WhyTraderDock3 { get; set; } // enum  WhyTraderDock1 = 11, EnumCMSType

        [AllowHtml]
        public string WhyTraderDock4 { get; set; } // enum  WhyTraderDock1 = 12, EnumCMSType

        [AllowHtml]
        public string WhyTraderDock5 { get; set; } // enum  WhyTraderDock1 = 13, EnumCMSType

        [AllowHtml]
        public string Bannertext { get; set; } // enum  Bannertext = 14, EnumCMSType

        [AllowHtml]
        public string HowtotradewithTraderdock { get; set; } // enum  HowtotradewithTraderdock = 41, EnumCMSType

        [AllowHtml]
        public string Takethetraderdockchallenge { get; set; } // enum  Takethetraderdockchallenge = 42, EnumCMSType

        [AllowHtml]
        public string Showusyouexcel { get; set; } // enum  Showusyouexcel = 43, EnumCMSType

        [AllowHtml]
        public string Beginlivetrading { get; set; } // enum  Beginlivetrading = 44, EnumCMSType

        [AllowHtml]
        public string WhyTraderdock { get; set; } // enum  Beginlivetrading = 44, EnumCMSType

        public List<string> BannerImages { get; set; }

        [AllowHtml]
        public string FooterCMS { get; set; } // enum  WebPanelFooter = 15, EnumCMSType

        [AllowHtml]
        public string LayoutFooterText { get; set; } // enum  WebPanelFooter = 50, EnumCMSType

        [AllowHtml]
        public string GlobalTeam { get; set; } // enum  WebPanelFooter = 35, EnumCMSType

        public List<TestimonialModel> TestimonialList { get; set; }

        public List<GlobalTeamModel> GlobalTeamList { get; set; }

        public long TransactionID { get; set; }
        public long? UserSubscriptionID { get; set; }
        public string account_number { get; set; }
        public decimal? CurrentBalanceOld { get; set; } = 0;
        public string CurrentBalance { get; set; } = "";
        public decimal? HighBalanceOld { get; set; } = 0;
        public string HighBalance { get; set; } = "";
        public decimal? LowBalanceOld { get; set; } = 0;
        public string LowBalance { get; set; } = "";
        public decimal? AvgWinningDayOld { get; set; } = 0;
        public string AvgWinningDay { get; set; } = "";
        public decimal? AvgLosingDayOld { get; set; } = 0;
        public string AvgLosingDay { get; set; } = "";
        public decimal? BestDayOld { get; set; } = 0;
        public string BestDay { get; set; } = "";
        public decimal? WorstDayOld { get; set; } = 0;
        public string WorstDay { get; set; } = "";
        public decimal? NetProfitLossOld { get; set; } = 0;
        public string NetProfitLoss { get; set; } = "";
        public decimal? GrossProfitLossOld { get; set; } = 0;
        public string GrossProfitLoss { get; set; } = "";
        public decimal? HighProfitLossOld { get; set; } = 0;
        public string HighProfitLoss { get; set; } = "";
        public decimal? LowProfitLossOld { get; set; } = 0;
        public string LowProfitLoss { get; set; } = "";
        public int? TotalContracts { get; set; } = 0;
        public decimal? TotalCommisionsOld { get; set; } = 0;
        public string TotalCommisions { get; set; } = "";
        public int? TotalTrades { get; set; } = 0;
        public decimal? AvgWinningTradeOld { get; set; } = 0;
        public string AvgWinningTrade { get; set; } = "";
        public decimal? AvgLosingTradeOld { get; set; } = 0;
        public string AvgLosingTrade { get; set; } = "";
        public decimal? WinningTradeOld { get; set; } = 0;
        public string WinningTrade { get; set; } = "";
        public Nullable<System.DateTime> Transaction_date { get; set; }
        public string TransactionDateString { get; set; }
        public decimal? ProfitTargetOld { get; set; } = 0;
        public string ProfitTarget { get; set; } = "";
        public decimal? DailyLossLimitOld { get; set; } = 0;
        public string DailyLossLimit { get; set; } = "";
        public decimal? MaxDrowdownOld { get; set; } = 0;
        public string MaxDrowdown { get; set; } = "";
        public decimal? MainAccountBalanceOld { get; set; } = 0;
        public string MainAccountBalance { get; set; } = "";
        public int? TradeRequiredNoOfDays { get; set; } = 0;
        public string AvgWLDuration { get; set; }
        public int MaxConsWin { get; set; }
        public int MaxConsLoss { get; set; }
        public decimal? LosingTradePerOld { get; set; } = 0;
        public string LosingTradePer { get; set; } = "";
        public decimal? WinningTradePerOld { get; set; } = 0;
        public string WinningTradePer { get; set; } = "";
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string AvgeWLDurationColor { get; set; } = "";
        public string AvgWinningDay_str { get; set; } = "--:--:--";
        public string AvgLosingDay_str { get; set; } = "--:--:--";
        public string Symbol { get; set; } = "";
        public string Exchange { get; set; } = "";
        public string EnquiryEmail { get; set; }
        public long UserID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public bool isActive { get; set; }
        public bool freeTrial { get; set; }
        public string strFreeTrial { get; set; }
        public string UserEmail { get; set; }
    }


    public class AccountInfo
    {
        public long account_info_id { get; set; }
        public long? user_subscription_id { get; set; }
        public string account_number { get; set; } = "-";
        public string subscription_name { get; set; } = "-";
        public int broker_id { get; set; }
        public string broker_name { get; set; }
        public DateTime subscription_start_date { get; set; }
        public DateTime subscription_end_date { get; set; }
        public DateTime subscription_trial_date { get; set; }
        public string substartdate { get; set; } = "-";
        public string subenddate { get; set; } = "-";
        public bool is_expired { get; set; }
        public bool? is_current { get; set; }
        public bool? is_free_trial { get; set; }
        public int totdays { get; set; } = 0;
        public int completeddays { get; set; } = 0;
        public string status { get; set; }
        public string WinnigDayPer { get; set; } = "0.00";
        //Added on client suggestion - 20July2018 - by shoaib
        public DateTime challenge_start_date { get; set; }
        //Added on client suggestion - 20July2018 - by shoaib
        public string challengestartdate { get; set; } = "-";
        public SelectList account_number_lst { get; set; }
        public int platform_id { get; set; }
        public int str_platform { get; set; }
    }

    public class RuleModel
    {
        public long rule_id { get; set; }
        public long? user_subscription_id { get; set; }
        public decimal? ruleprofit_targetOld { get; set; } = 0;
        public string ruleprofit_target { get; set; } = "";
        public decimal? ruledaily_loss_limitOld { get; set; } = 0;
        public string ruledaily_loss_limit { get; set; } = "";
        public decimal? rulemax_drowdownOld { get; set; } = 0;
        public string rulemax_drowdown { get; set; } = "";
        public decimal? rulemaintain_account_balanceOld { get; set; } = 0;
        public string rulemaintain_account_balance { get; set; } = "";
        public decimal? ruletrade_required_no_of_days { get; set; } = 0;
        public decimal? ProfitTargetOld { get; set; } = 0;
        public string ProfitTarget { get; set; } = "";
        public decimal? DailyLossLimitOld { get; set; } = 0;
        public string DailyLossLimit { get; set; } = "";
        public decimal? MaxDrowdownOld { get; set; } = 0;
        public string MaxDrowdown { get; set; } = "";
        public decimal? MainAccountBalanceOld { get; set; } = 0;
        public string MainAccountBalance { get; set; } = "";
        public int? TradeRequiredNoOfDays { get; set; } = 0;
        public string profit_targetachievestatus { get; set; } = "-";
        public string daily_loss_limitachievestatus { get; set; } = "-";
        public string max_drowdownachievestatus { get; set; } = "-";
        public string maintain_account_balanceachievestatus { get; set; } = "-";
        public string trade_required_no_of_daysachievestatus { get; set; } = "-";
    }

    public class CurrentbalChart
    {
        public string chartType;
        public long user_subscription_id;
        public string x;
        public string y;
        public DateTime dx;
        public bool Is_Prev;
        public bool Is_next;
    }

    public class TempCurrentBal
    {
        public string Exchange;
        public long user_subscription_id;
        public decimal net_pnl;
        public DateTime transaction_day;
    }

    public class FirstLast
    {
        public DateTime firstDay;
        public DateTime lastDay;
    }

    public class NetPlScript
    {
        public long user_subscription_id;
        public string chartType;
        public string Script;
        public decimal? net_profit_loss;
    }
    public class TotalTrade
    {
        public long user_subscription_id;
        public string NoofwiningTrade;
        public string NooflosingTrade;
        public string NoofsketchTrade;
        public string ColorCode;
    }

    public class WEBDashboardModel
    {
        public WEBDashboardModel()
        {
            objAccountInfoModel = new AccountInfo();
            objCurrentbalChartList = new List<CurrentbalChart>();
            ObjHomeModel = new HomePageModel();
            objLearnHowModel = new LearnHowModel();
            objPlanMemModel = new SubscriptionModel();
            ObjRuleModel = new RuleModel();
            objUserMasterModel = new UserMasterModel();
            objUserSubscriptionList = new List<UserSubscriptionModel>();
            Subscription_List = new List<PlanMemModel>();
        }
        public SelectList account_number_lst { get; set; }
        public SubscriptionModel objPlanMemModel { get; set; }
        public UserMasterModel objUserMasterModel { get; set; }
        public UserSubscriptionModel objUserSubscriptionModel { get; set; }
        public HomePageModel ObjHomeModel { get; set; }
        public RuleModel ObjRuleModel { get; set; }
        public AccountInfo objAccountInfoModel { get; set; }
        public LearnHowModel objLearnHowModel { get; set; }
        public List<HomePageModel> UserTransactionList { get; set; }
        public List<CurrentbalChart> objCurrentbalChartList { get; set; }
        public List<UserSubscriptionModel> objUserSubscriptionList { get; set; }
        public List<PlanMemModel> Subscription_List { get; set; }

    }
}
