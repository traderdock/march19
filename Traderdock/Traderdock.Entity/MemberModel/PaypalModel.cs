﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.MemberModel
{
    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: Paypal Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class PaypalModel
    {
        public int promo_code_id { get; set; }

        public int platform_id { get; set; }

        public long user_id { get; set; }

        public MemberModel User { get; set; }

        public long subscription_id { get; set; }

        public string subscription_name { get; set; }

        public string transaction_id { get; set; }

        public decimal? order_amount { get; set; }

        //public DateTime? purchase_date { get; set; }

        [Required(ErrorMessage = "Please enter name.")]
        public string card_name { get; set; }

        public string purchase_date { get; set; }

        [RegularExpression("^[0-9]*$", ErrorMessage = "Please enter valid card number.")]
        [Required(ErrorMessage = "Please enter card number.")]
        public string card_number { get; set; }

        public string card_type { get; set; }

        [Required(ErrorMessage = "Please enter month and year.")]
        public string card_expiry { get; set; }

        public string card_expiry_month { get; set; }

        public string card_expiry_year { get; set; }

        [Required(ErrorMessage = "Please enter cvv.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "CVV should be at least 3 characters long.")]
        public string card_cvv { get; set; }
        public string payment_method { get; set; }

        public int? payment_status { get; set; }

        public bool? is_expired { get; set; }

        public bool? is_current { get; set; }

        public DateTime? subscription_start_date { get; set; }

        public DateTime? subscription_end_date { get; set; }

        public DateTime? created_date { get; set; }

        public long created_by { get; set; }

        public bool is_success { get; set; }

        public DateTime? updated_date { get; set; }

        public long updated_by { get; set; }

        public int? payment_response_code { get; set; }

        public string payment_response_message { get; set; }

        public bool issave { get; set; }

        public string transaction_date { get; set; }

        public string PaymentType { get; set; }

        public string PaymentIsSuccess { get; set; }

        public string PayPalMurchantID { get; set; }

        public string PayPalEnvirement  { get; set; }

        public string subscription_mode { get; set; }

    }

    public class PayPalOrder
    {
        public decimal Amount { get; set; }
        public string OrderId { get; set; }
        public string IteamName { get; set; }
        public string ReturnUrl { get; set; }
        public string CancelUrl { get; set; }
        public DateTime OrderDate { get; set; }
    }
    public class PayPalRedirect
    {
        public string Url { get; set; }
        public string Token { get; set; }
    }
}
