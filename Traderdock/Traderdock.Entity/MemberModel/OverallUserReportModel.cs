﻿using System;

namespace Traderdock.Entity.MemberModel
{
    public class OverallUserReportModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string account_number { get; set; }
        public string UserEmail { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string LastUploadDateStr { get; set; }
        public DateTime? LastUploadDate { get; set; }
        public int? TotalContracts { get; set; } = 0;
        public int? TotalTrades { get; set; } = 0;
        public decimal? GrossProfitLossOld { get; set; } = 0;
        public int? TradeCompletedNoOfDays { get; set; } = 0;
        public decimal? CurrentBalanceOld { get; set; } = 0;
        public decimal? MaxDrawDownOld { get; set; } = 0;
        public decimal? DailyLossOld { get; set; } = 0;
        public decimal? BestDayOld { get; set; } = 0;
        public decimal? AvgWinningDayOld { get; set; } = 0;
        public decimal? DiscountedBalOld { get; set; } = 0;
        public decimal? HighProfitLossOld { get; set; } = 0;
        public decimal? LowProfitLossOld { get; set; } = 0;
        public decimal? AvgLosingDayOld { get; set; } = 0;
        public decimal? WorstDayOld { get; set; } = 0;
        public long userID { get; set; }
        public long TransactionID { get; set; }
        public long? UserSubscriptionID { get; set; }
        public bool? IsActive { get; set; }
        public bool? IsFreeTrial { get; set; }
    }
}
