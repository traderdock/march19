﻿namespace Traderdock.Model.TTModel
{
    public class AccountCreationInputModel
    {
        public string accountName { get; set; }
        public string description { get; set; }
        public int accountType { get; set; }
        public int parentAccountId { get; set; }
        public int AOTCType { get; set; }
        public int tradingAllowedSelf { get; set; }
        public int tradingAllowedSubAccts { get; set; }
        public int disabledByAutoliquidate { get; set; }
    }
}
