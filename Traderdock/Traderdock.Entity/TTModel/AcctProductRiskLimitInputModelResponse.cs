﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class AcctProductRiskLimitInputModelResponse
    {
        public string status { get; set; }
        public int accountId { get; set; }
        public AccountProductRiskLimit accountProductRiskLimit { get; set; }
    }

    public class accountProductRiskLimitRequest
    {
        public int marketId { get; set; }
        public int productTypeId { get; set; }
        public string productId { get; set; }
        public string productSymbol { get; set; }
        public int tradeOutAllowed { get; set; }
        public int maxPos { get; set; }
        public int maxShortPosPerContract { get; set; }
        public int maxLongPosPerContract { get; set; }
        public int maxLongPos { get; set; }
        public int maxShortPos { get; set; }
        public int outright_tradingAllowed { get; set; }
        public int outright_maxOrderQty { get; set; }
        public int outright_appliedMarginPercent { get; set; }
        public int outright_aggressiveOnly { get; set; }
        public int outright_aggressiveOnlyPercentage { get; set; }
        public int spread_trading_allowed { get; set; }
        public int spread_appliedMarginPercent { get; set; }
        public int spread_aggressiveOnly { get; set; }
        public int spread_aggressiveOnlyPercentage { get; set; }
    }
}

