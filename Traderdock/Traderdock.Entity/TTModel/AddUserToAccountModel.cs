﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class AddUserToAccountModel
    {
        public int accountId { get; set; }
        public int userId { get; set; }
        //public int id { get; set; }
        //public int revision { get; set; }
        public int blockCrossOrders { get; set; }
        public int stagedOrders { get; set; }
        public int stagedOrdersManageOthers { get; set; }
        public int stageOrdersManageOwn { get; set; }
        public int wholesaleOrders { get; set; }
        public int deleteOrders { get; set; }
        public int dmaOrders { get; set; }
        public int GtcGtDate { get; set; }
        public int market { get; set; }
        public int mobile { get; set; }
        public int autospreader { get; set; }
        public int aggregator { get; set; }
        public int ttAlgoSDK { get; set; }
        public int adl { get; set; }
        public int ADLAlgoApprovalRequired { get; set; }
        public int ttSyntheticOrderTypes { get; set; }
        public int ttTimed { get; set; }
        public int ttStop { get; set; }
        public int ttIfTouched { get; set; }
        public int ttTrailingLimit { get; set; }
        public int ttWithATick { get; set; }
        public int ttOCO { get; set; }
        public int ttBracket { get; set; }
        public int ttIceberg { get; set; }
        public int orderByVolatility { get; set; }
        public int ttTimeSliced { get; set; }
        public int ttRetry { get; set; }
        public int manualFills { get; set; }
        public int autoRFQCross { get; set; }
        public int stagedOrderRiskCheckType { get; set; }
        public int confirmFills { get; set; }
    }
}

