﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class TTProduct
    {
        public int tt_primary_product_id { get; set; }
        public string symbol { get; set; }
        public int productTypeId { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string familyId { get; set; }
        public bool is_selected { get; set; }
    }

    public class ProductResponseClass
    {
        public ProductResponseClass()
        {
            products = new List<TTProduct>();
        }
        public string status { get; set; }
        public List<TTProduct> products { get; set; }
    }
}
