﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class State
    {
        public int stateId { get; set; }
        public string stateCode { get; set; }
        public string stateName { get; set; }
    }

    public class StateResponseClass
    {
        public string status { get; set; }
        public List<State> states { get; set; }
    }
}
