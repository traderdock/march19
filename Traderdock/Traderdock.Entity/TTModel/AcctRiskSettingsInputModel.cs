﻿namespace Traderdock.Model.TTModel
{
    public class AcctRiskSettingsInputModel
    {
        public int accountId { get; set; }
        public AccountRiskSettings accountRiskSettings { get; set; }
    }

    public class AccountRiskSettings
    {
        public int id { get; set; }
        public long revision { get; set; }
        public int resetPositionInherited { get; set; }
        public int autoLiquidateInherited { get; set; }
        public int dailyCreditLimit { get; set; }
        public int dailyCreditLimitCurrency { get; set; }
        public int positionResetTypeId { get; set; }
        public int positionResetTime { get; set; }
        public string positionResetTimezone { get; set; }
        public int dailyCreditLimitRuleId { get; set; }
        public int applyToWholesaleOrders { get; set; }
        public int checkCredit { get; set; }
        public int resetPosition { get; set; }
        public int resetSpreadPosition { get; set; }
        public int positionRoll { get; set; }
        public int checkCreditLoss { get; set; }
        public int checkCreditLoss_percentOfDailyCredit { get; set; }
        public int checkCreditLoss_disableTrading { get; set; }
        public int checkCreditLoss_cancelWorking { get; set; }
        public int checkCreditLoss_autoliquidateOpenPositions { get; set; }
    }
}
