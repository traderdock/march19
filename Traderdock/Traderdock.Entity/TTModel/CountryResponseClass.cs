﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class CountryResponseClass
    {
        public string status { get; set; }
        public List<Country> countries { get; set; }
    }
    public class Country
    {
        public int countryId { get; set; }
        public string countryCode { get; set; }
        public string countryName { get; set; }
    }
}
