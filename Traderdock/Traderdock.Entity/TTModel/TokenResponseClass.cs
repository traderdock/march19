﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class TokenResponseClass
    {
        public string status { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int seconds_until_expiry { get; set; }
    }
}
