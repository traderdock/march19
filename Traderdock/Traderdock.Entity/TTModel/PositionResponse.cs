﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class TTPositionResponse
    {
        public string instrumentId { get; set; }
        public decimal pnl { get; set; }
        public decimal pnlPrice { get; set; }
        public string pnlPriceType { get; set; }
        public decimal realizedPnl { get; set; }
        public decimal buyFillQty { get; set; }
        public decimal sellFillQty { get; set; }
        public decimal netPosition { get; set; }
        public decimal buyWorkingQty { get; set; }
        public decimal sellWorkingQty { get; set; }
        public decimal avgBuy { get; set; }
        public decimal avgSell { get; set; }
        public decimal openAvgPrice { get; set; }
        public decimal sodPrice { get; set; }
        public string sodPriceType { get; set; }
        public decimal sodNetPos { get; set; }
        public int accountId { get; set; }
    }

    public class PositionResponse
    {
        public string status { get; set; }
        public List<TTPositionResponse> positions { get; set; }
    }
}
