﻿namespace Traderdock.Model.TTModel
{
    public class AcctRiskSettingsInputModelResponse
    {
        public int accountId { get; set; }
        public AccountRiskSettingsResponse accountRiskSettings { get; set; }
    }

    public class AccountRiskSettingsResponse
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int dailyCreditLimit { get; set; }
        public int dailyCreditLimitCurrency { get; set; }
        public int positionResetTypeId { get; set; }
        public int positionResetTime { get; set; }
        public string positionResetTimezone { get; set; }
        public int dailyCreditLimitRuleId { get; set; }
        public int applyToWholesaleOrders { get; set; }
        public int checkCredit { get; set; }
        public int resetPosition { get; set; }
        public int resetSpreadPosition { get; set; }
        public int positionRoll { get; set; }
        public int checkCreditLoss { get; set; }
        public int checkCreditLoss_percentOfDailyCredit { get; set; }
        public int checkCreditLoss_disableTrading { get; set; }
        public int checkCreditLoss_cancelWorking { get; set; }
        public int checkCreditLoss_autoliquidateOpenPositions { get; set; }
    }
}
