﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class AccountListResponseClass
    {
        public string status { get; set; }
        public List<TTAccountList> accounts { get; set; }
        public string lastPage { get; set; }
        public string nextPageKey { get; set; }
        public int requestVersion { get; set; }
    }
    public class TTAccountList
    {
        public int id { get; set; }
        public string name { get; set; }
        public long? parentId { get; set; }
    }
}
