﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
  
    public class StateResponse
    {
        public string stateCode { get; set; }
        public string stateName { get; set; }
        public int stateId { get; set; }
    }

    public class CountryResponse
    {
        public string countryCode { get; set; }
        public string countryName { get; set; }
        public int countryId { get; set; }
    }

    public class AddressResponse
    {
        public string city { get; set; }
        public string primaryPhoneNumber { get; set; }
        public StateResponse state { get; set; }
        public CountryResponse country { get; set; }
    }

    public class User
    {
        public int id { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string alias { get; set; }
        public int revision { get; set; }
        public int tradeMode { get; set; }
        public int advancedOptions { get; set; }
        public int userBlocked { get; set; }
        public int userActive { get; set; }
        public int invitationPending { get; set; }
        public AddressResponse address { get; set; }
    }

    public class UserCreationInputModelResponse
    {
        public string status { get; set; }
        public List<User> user { get; set; }
    }
}
