﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Traderdock.Model.TTModel
{
    public class Fill
    {
        public long getfill_id { get; set; }
        public string recordId { get; set; }
        public string timeStamp { get; set; }
        public DateTime? datetimeStamp { get; set; }
        public string instrumentId { get; set; }
        public string Contract { get; set; }
        public string multiLegReportingType { get; set; }
        public decimal? deltaQty { get; set; }
        public long? brokerId { get; set; }
        public string manualOrderIndicator { get; set; }
        public int? marketId { get; set; }
        public int currUserId { get; set; }
        public string transactTime { get; set; }
        public int lastQty { get; set; }
        public string parentOrderId { get; set; }
        public long accountId { get; set; }
        public double avgPx { get; set; }
        public string orderId { get; set; }
        public decimal? lastPx { get; set; }
        public int? execType { get; set; }
        public decimal? cumQty { get; set; }
        public string textTT { get; set; }
        public string exchOrderAssoc { get; set; }
        public string ordStatus { get; set; }
        public string execId { get; set; }
        public decimal? source { get; set; }
        public int? side { get; set; }
        public decimal? syntheticType { get; set; }
        public string securityDesc { get; set; }
        public string account { get; set; }
        public DateTime? created_on { get; set; }
        public decimal price { get; set; }
        public decimal displayFactor { get; set; }
        public object datetimeStamp_str { get; set; }
    }

    public class TTAPIInputModel
    {
        public string account { get; set; }
        public SelectList lstaccount { get; set; }
        public List<Fill> fills { get; set; }
    }
}
