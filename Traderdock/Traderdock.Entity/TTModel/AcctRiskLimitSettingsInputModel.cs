﻿namespace Traderdock.Model.TTModel
{
    public class AcctRiskLimitSettingsInputModel
    {
        public int accountId { get; set; }
        public accountRiskLimitSettings accountRiskLimitSettings { get; set; }

    }
    public class accountRiskLimitSettings
    {
        public int id { get; set; }
        public long revision { get; set; }
        public int noLimits { get; set; }
        public int applyLimitsToWholesaleOrders { get; set; }
    }
}
