﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class TTUserModel
    {
        public long fillID { get; set; }
        public long userID { get; set; }
        public string name { get; set; }
        public string accountID { get; set; }
        public string sPlatform { get; set; }
        public int PlatformID { get; set; }
        public DateTime dtLastUpdateTime { get; set; }
        public DateTime dtFirstUpdateTime { get; set; }
        public string sLastUpdate { get; set; }
        public string sFirstUpdate { get; set; }
        
    }
}
