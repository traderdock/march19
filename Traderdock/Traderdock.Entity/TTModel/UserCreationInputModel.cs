﻿namespace Traderdock.Model.TTModel
{
    public class UserCreationInputModel
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string email { get; set; }
        public string alias { get; set; }
        public int tradeMode { get; set; }
        public int advancedOptions { get; set; }
        public int userInactivityLockReset { get; set; }
        public AddressInput address { get; set; }
    }
    public class AddressInput
    {
        public int countryId { get; set; }
        public string stateId { get; set; }
        public string city { get; set; }
        public string primaryPhoneNumber { get; set; }
        public string primaryPhoneExt { get; set; }
        public string secondaryPhoneNumber { get; set; }
        public string secondaryPhoneExt { get; set; }
        public string streetAddress1 { get; set; }
        public string streetAddress2 { get; set; }
        public int zipcode { get; set; }
    }
}
