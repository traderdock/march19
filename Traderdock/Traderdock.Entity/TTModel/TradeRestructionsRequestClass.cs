﻿namespace Traderdock.Model.TTModel
{
    public class AccountTradeRestrictions
    {
        public int applyRestrictions { get; set; }
        public int blockCrossOrders { get; set; }
        public int dmaOrders { get; set; }
        public int gtcGtDate { get; set; }
        public int market { get; set; }
        public int autospreader { get; set; }
        public int aggregator { get; set; }
        public int ttAlgoSDK { get; set; }
        public int adl { get; set; }
        public int ADLAlgoApprovalRequired { get; set; }
        public int mobile { get; set; }
        public int ttSyntheticOrderTypes { get; set; }
        public int ttTimeSliced { get; set; }
        public int ttRetry { get; set; }
        public int ttTimed { get; set; }
        public int ttStop { get; set; }
        public int ttIfTouched { get; set; }
        public int ttTrailingLimit { get; set; }
        public int ttOCO { get; set; }
        public int ttBracket { get; set; }
        public int ttIceberg { get; set; }
        public int ttWithATick { get; set; }
        public int manualFills { get; set; }
        public int confirmFills { get; set; }
    }

    public class TradeRestructionsRequestClass
    {
        public int accountId { get; set; }
        public AccountTradeRestrictions accountTradeRestrictions { get; set; }
    }
}
