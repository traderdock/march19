﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class InstrumentResponse
    {
        public int roundLotQty { get; set; }
        public string term { get; set; }
        public int marketDepth { get; set; }
        public string name { get; set; }
        public string securityId { get; set; }
        public int marketId { get; set; }
        public string productSymbol { get; set; }
        public string alias { get; set; }
        public int productTypeId { get; set; }
        public double tickSize { get; set; }
        public object expirationDate { get; set; }
        public double pointValue { get; set; }
        public int minLotSize { get; set; }
        public int seriesTermId { get; set; }
        public double tickValue { get; set; }
        public object id { get; set; }
        public decimal displayFactor { get; set; }
        public object productId { get; set; }
    }

    public class TTInstrumentResponseClass
    {
        public string status { get; set; }
        public bool lastPage { get; set; }
        public List<InstrumentResponse> instruments { get; set; }
    }

    public class TTInstrumentDetailsResponseClass
    {
        public string status { get; set; }
        public bool lastPage { get; set; }
        public List<InstrumentResponse> instrument { get; set; }
    }
}
