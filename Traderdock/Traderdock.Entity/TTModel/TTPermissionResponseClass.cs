﻿using System;
using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class TTPermissionResponseClass
    {
        public int disabledByAutoliquidate { get; set; }
        public int tradingAllowedSelf { get; set; }
        public int tradingAllowedSubAccts { get; set; }

    }
}
