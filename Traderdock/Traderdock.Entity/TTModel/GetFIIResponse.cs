﻿using System;
using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{
    public class GetFIIResponse
    {
        public string status { get; set; }
        public List<FillTTResponse> fills { get; set; }

    }
    public class FillTTResponse
    {
        public string recordId { get; set; }
        public string timeStamp { get; set; }
        public DateTime dtTimeStamp
        {
           get
           {
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(Convert.ToDouble(timeStamp) / Convert.ToDouble(1000000000))).ToUniversalTime();
                return dt;
           }
        }
        public string instrumentId { get; set; }
        public string multiLegReportingType { get; set; }
        public decimal syntheticType { get; set; }
        public decimal execInst { get; set; }
        public decimal deltaQty { get; set; }
        public decimal brokerId { get; set; }
        public string externallyCreated { get; set; }
        public string manualOrderIndicator { get; set; }
        public string securityDesc { get; set; }
        public string senderSubId { get; set; }
        public string account { get; set; }
        public decimal marketId { get; set; }
        public string tradeDate { get; set; }
        public DateTime dttradeDate
        {
            get
            {
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(Convert.ToDouble(tradeDate) / Convert.ToDouble(1000000000))).ToUniversalTime();
                return dt;
            }
        }
        public long currUserId { get; set; }
        public string transactTime { get; set; }
        public DateTime dttransactTime
        {
            get
            {
                DateTime dt = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(Convert.ToDouble(transactTime) / Convert.ToDouble(1000000000))).ToUniversalTime();
                return dt;
            }
        }
        public decimal lastQty { get; set; }
        public decimal accountId { get; set; }
        public decimal avgPx { get; set; }
        public string orderId { get; set; }
        public decimal lastPx { get; set; }
        public decimal execType { get; set; }
        public string senderLocationId { get; set; }
        public decimal cumQty { get; set; }
        public string aggressorIndicator { get; set; }
        public decimal side { get; set; }
        public decimal source { get; set; }
    }

}
