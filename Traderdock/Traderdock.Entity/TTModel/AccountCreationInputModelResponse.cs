﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{

    public class Account
    {
        public int id { get; set; }
        public string name { get; set; }
        public int revision { get; set; }
        public string description { get; set; }
        public int parentAccountId { get; set; }
        public TTPermissionResponseClass permissions { get; set; }
    }

    public class AccountCreationInputModelResponse
    {
        public string status { get; set; }
        public List<Account> account { get; set; }
    }
    //sample response through success 14-Aug-2018
    //    {
    //    "status": "Ok",
    //    "account": [
    //        {
    //            "id": 3643,
    //            "name": "TD180802",
    //            "revision": 421711,
    //            "description": "Test add for api test",
    //            "parentAccountId": 2632,
    //            "permissions": {
    //                "tradingAllowedSelf": 1,
    //                "tradingAllowedSubAccts": 0,
    //                "disabledByAutoliquidate": 1
    //            }
    //        }
    //    ]
    //}
}
