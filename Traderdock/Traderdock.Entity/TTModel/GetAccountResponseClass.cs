﻿using System.Collections.Generic;

namespace Traderdock.Model.TTModel
{


    public class TTGetAccountRestrictions
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int applyRestrictions { get; set; }
        public int blockCrossOrders { get; set; }
        public int dmaOrders { get; set; }
        public int gtcGtDate { get; set; }
        public int market { get; set; }
        public int autospreader { get; set; }
        public int aggregator { get; set; }
        public int ttAlgoSDK { get; set; }
        public int adl { get; set; }
        public int ADLAlgoApprovalRequired { get; set; }
        public int mobile { get; set; }
        public int ttSyntheticOrderTypes { get; set; }
        public int ttTimeSliced { get; set; }
        public int ttRetry { get; set; }
        public int ttTimed { get; set; }
        public int ttStop { get; set; }
        public int ttIfTouched { get; set; }
        public int ttTrailingLimit { get; set; }
        public int ttOCO { get; set; }
        public int ttBracket { get; set; }
        public int ttIceberg { get; set; }
        public int ttWithATick { get; set; }
        public int manualFills { get; set; }
        public int confirmFills { get; set; }
    }

    public class TTGetAccountEffectiveRestrictions
    {
        public int applyRestrictions { get; set; }
        public int wholesaleOrders { get; set; }
        public int dmaOrders { get; set; }
        public int gtcGtDate { get; set; }
        public int market { get; set; }
        public int autospreader { get; set; }
        public int aggregator { get; set; }
        public int ttAlgoSDK { get; set; }
        public int adl { get; set; }
        public int mobile { get; set; }
        public int ttSyntheticOrderTypes { get; set; }
        public int ttTimeSliced { get; set; }
        public int ttRetry { get; set; }
        public int ttTimed { get; set; }
        public int ttStop { get; set; }
        public int ttIfTouched { get; set; }
        public int ttTrailingLimit { get; set; }
        public int ttOCO { get; set; }
        public int ttBracket { get; set; }
        public int ttIceberg { get; set; }
        public int ttWithATick { get; set; }
        public int manualFills { get; set; }
        public int confirmFills { get; set; }
    }

    public class TTGetAccountRiskSettings
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int dailyCreditLimit { get; set; }
        public int dailyCreditLimitCurrency { get; set; }
        public int positionResetTypeId { get; set; }
        public int positionResetTime { get; set; }
        public string positionResetTimezone { get; set; }
        public int dailyCreditLimitRuleId { get; set; }
        public int applyToWholesaleOrders { get; set; }
        public int checkCredit { get; set; }
        public int resetPosition { get; set; }
        public int resetPositionInherited { get; set; }
        public int resetSpreadPosition { get; set; }
        public int positionRoll { get; set; }
        public int checkCreditLoss { get; set; }
        public int autoLiquidateInherited { get; set; }
        public int checkCreditLoss_percentOfDailyCredit { get; set; }
        public int checkCreditLoss_disableTrading { get; set; }
        public int checkCreditLoss_cancelWorking { get; set; }
        public int checkCreditLoss_autoliquidateOpenPositions { get; set; }
    }

    public class TTGetAccountRiskLimitSettings
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int noLimits { get; set; }
        public int applyLimitsToWholesaleOrders { get; set; }
    }

    public class TTGetAccountProductLimit
    {
        public int id { get; set; }
        public int marketId { get; set; }
        public int productTypeId { get; set; }
        public string productId { get; set; }
        public string productSymbol { get; set; }
        public int tradeOutAllowed { get; set; }
        public int maxPos { get; set; }
        public int maxShortPosPerContract { get; set; }
        public int maxLongPosPerContract { get; set; }
        public int maxLongPos { get; set; }
        public int maxShortPos { get; set; }
        public int outright_tradingAllowed { get; set; }
        public int outright_maxOrderQty { get; set; }
        public int outright_appliedMarginPercent { get; set; }
        public int outright_aggressiveOnly { get; set; }
        public int outright_aggressiveOnlyPercentage { get; set; }
        public int spread_trading_allowed { get; set; }
        public int spread_appliedMarginPercent { get; set; }
        public int spread_aggressiveOnly { get; set; }
        public int spread_aggressiveOnlyPercentage { get; set; }
        public int revision { get; set; }
    }

    public class TTGetAccountRiskLimits
    {
        public List<TTGetAccountProductLimit> productLimits { get; set; }
        public List<object> contractLimits { get; set; }
        public List<object> interProductLimits { get; set; }
    }

    public class TTGetAccountUserResponse
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int userId { get; set; }
        public int accountId { get; set; }
        public int blockCrossOrders { get; set; }
        public int stagedOrders { get; set; }
        public int stagedOrdersManageOthers { get; set; }
        public int stageOrdersManageOwn { get; set; }
        public int deleteOrders { get; set; }
        public int dmaOrders { get; set; }
        public int gtcGtDate { get; set; }
        public int market { get; set; }
        public int mobile { get; set; }
        public int autospreader { get; set; }
        public int aggregator { get; set; }
        public int ttAlgoSDK { get; set; }
        public int adl { get; set; }
        public int ADLAlgoApprovalRequired { get; set; }
        public int ttSyntheticOrderTypes { get; set; }
        public int ttTimed { get; set; }
        public int ttStop { get; set; }
        public int ttIfTouched { get; set; }
        public int ttTrailingLimit { get; set; }
        public int ttWithATick { get; set; }
        public int ttOCO { get; set; }
        public int ttBracket { get; set; }
        public int ttIceberg { get; set; }
        public int orderByVolatility { get; set; }
        public int ttTimeSliced { get; set; }
        public int ttRetry { get; set; }
        public int autoRFQCross { get; set; }
        public int stagedOrderRiskCheckType { get; set; }
        public int manualFills { get; set; }
        public int confirmFills { get; set; }
    }

    public class TTGetAccountResponse
    {
        public int id { get; set; }
        public string name { get; set; }
        public int parentAccountId { get; set; }
        public TTPermissionResponseClass permissions { get; set; }
        public TTGetAccountRestrictions restrictions { get; set; }
        public TTGetAccountEffectiveRestrictions effectiveRestrictions { get; set; }
        public TTGetAccountRiskSettings riskSettings { get; set; }
        public TTGetAccountRiskLimitSettings riskLimitSettings { get; set; }
        public TTGetAccountRiskLimits riskLimits { get; set; }
        public List<TTGetAccountUserResponse> users { get; set; }
    }

    public class GetAccountResponseClass
    {
        public string status { get; set; }
        public List<TTGetAccountResponse> account { get; set; }
    }
}
