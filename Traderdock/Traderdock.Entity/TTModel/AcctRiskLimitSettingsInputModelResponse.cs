﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.TTModel
{
    public class AcctRiskLimitSettingsInputModelResponse
    {
        public int accountId { get; set; }
        public accountRiskLimitSettingsResponse accountRiskLimitSettings { get; set; }

    }
    public class accountRiskLimitSettingsResponse
    {
        public int id { get; set; }
        public int revision { get; set; }
        public int noLimits { get; set; }
        public int applyLimitsToWholesaleOrders { get; set; }
    }
}
