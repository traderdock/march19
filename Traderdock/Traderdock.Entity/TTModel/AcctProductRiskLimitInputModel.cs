﻿namespace Traderdock.Model.TTModel
{
    /// <summary>
    /// Defines the <see cref="AcctProductRiskLimitInputModel" />
    /// </summary>
    public class AcctProductRiskLimitInputModel
    {
        /// <summary>
        /// Gets or sets the accountId
        /// </summary>
        public int accountId { get; set; }

        /// <summary>
        /// Gets or sets the accountProductRiskLimit
        /// </summary>
        public accountProductRiskLimitRequest accountProductRiskLimit { get; set; }
    }

    /// <summary>
    /// Defines the <see cref="AccountProductRiskLimit" />
    /// </summary>
    public class AccountProductRiskLimit
    {
        /// <summary>
        /// Gets or sets the id
        /// </summary>
        public int id { get; set; }

        /// <summary>
        /// Gets or sets the marketId
        /// </summary>
        public int marketId { get; set; }

        /// <summary>
        /// Gets or sets the productTypeId
        /// </summary>
        public int productTypeId { get; set; }

        /// <summary>
        /// Gets or sets the productId
        /// </summary>
        public string productId { get; set; }

        /// <summary>
        /// Gets or sets the productSymbol
        /// </summary>
        public string productSymbol { get; set; }

        /// <summary>
        /// Gets or sets the tradeOutAllowed
        /// </summary>
        public int tradeOutAllowed { get; set; }

        /// <summary>
        /// Gets or sets the maxPos
        /// </summary>
        public int maxPos { get; set; }

        /// <summary>
        /// Gets or sets the maxShortPosPerContract
        /// </summary>
        public int maxShortPosPerContract { get; set; }

        /// <summary>
        /// Gets or sets the maxLongPosPerContract
        /// </summary>
        public int maxLongPosPerContract { get; set; }

        /// <summary>
        /// Gets or sets the maxLongPos
        /// </summary>
        public int maxLongPos { get; set; }

        /// <summary>
        /// Gets or sets the maxShortPos
        /// </summary>
        public int maxShortPos { get; set; }

        /// <summary>
        /// Gets or sets the outright_tradingAllowed
        /// </summary>
        public int outright_tradingAllowed { get; set; }

        /// <summary>
        /// Gets or sets the outright_maxOrderQty
        /// </summary>
        public int outright_maxOrderQty { get; set; }

        /// <summary>
        /// Gets or sets the outright_appliedMarginPercent
        /// </summary>
        public int outright_appliedMarginPercent { get; set; }

        /// <summary>
        /// Gets or sets the outright_aggressiveOnly
        /// </summary>
        public int outright_aggressiveOnly { get; set; }

        /// <summary>
        /// Gets or sets the outright_aggressiveOnlyPercentage
        /// </summary>
        public int outright_aggressiveOnlyPercentage { get; set; }

        /// <summary>
        /// Gets or sets the spread_trading_allowed
        /// </summary>
        public int spread_trading_allowed { get; set; }

        /// <summary>
        /// Gets or sets the spread_appliedMarginPercent
        /// </summary>
        public int spread_appliedMarginPercent { get; set; }

        /// <summary>
        /// Gets or sets the spread_aggressiveOnly
        /// </summary>
        public int spread_aggressiveOnly { get; set; }

        /// <summary>
        /// Gets or sets the spread_aggressiveOnlyPercentage
        /// </summary>
        public int spread_aggressiveOnlyPercentage { get; set; }

        /// <summary>
        /// Gets or sets the revision
        /// </summary>
        public int revision { get; set; }
    }
}
