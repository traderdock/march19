﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Traderdock.Model.TTModel
{
    public class TTFileModel
    {
        public string account { get; set; }
        public SelectList lstaccount { get; set; }
        public List<TTFileInputModel> fill_list { get; set; }
    }
    public class TTViewInputModel
    {
        //public long fillid { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public TimeSpan tsTime { get; set; }
        public string Date_str { get; set; }
        public string Time_str { get; set; }
        public string Exchange { get; set; }
        public string Contract { get; set; }
        public string BS { get; set; }
        public decimal FillQty { get; set; }
        public decimal Price { get; set; }
        // public string PF { get; set; }
        // public string Route { get; set; }
        public string Account { get; set; }
        public string AccountCombinedName { get; set; }
        public long AccountID { get; set; }
        // public string CurrentUser { get; set; }
        // public string ManualFill { get; set; }
        public int BuySell { get; set; }
        // public string Buy_Sell { get; set; }
        public decimal NetPosition { get; set; }
        public int TradeEntry { get; set; }
        public int TradeExit { get; set; }
        public int TradeNumber { get; set; }
        public decimal BuyPrice { get; set; }
        public decimal SellPrice { get; set; }
        public decimal AverageBuyPrice { get; set; }
        public decimal AverageSellPrice { get; set; }
        public decimal realised_pnl_tick { get; set; }
        public decimal realised_pnl_usd_without_commision { get; set; }
        public decimal realised_pnl_usd_with_commision { get; set; }
        public decimal unrealised_pnl_tick { get; set; }
        public decimal current_balance { get; set; }
        public decimal threshold_value { get; set; }
        public decimal commission { get; set; }
        public int? w_trades { get; set; }
        public int? l_trades { get; set; }
        public int? s_trades { get; set; }
        public decimal total_traded_qty_trade { get; set; }
        public decimal high_pnl { get; set; }
        public decimal low_pnl { get; set; }
        public decimal cumulative_pnl { get; set; }
        public decimal cumalitve_pnl_without_commision { get; set; }
        //public DateTime? created_on { get; set; }
    }

    public class TTFileInputModel
    {
        public long fillid { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public TimeSpan tsTime { get; set; }
        public string Date_str { get; set; }
        public string Time_str { get; set; }
        public string Exchange { get; set; }
        public string Contract { get; set; }
        public string BS { get; set; }
        public decimal FillQty { get; set; }
        public decimal Price { get; set; }
        public string PF { get; set; }
        public string Route { get; set; }
        public string Account { get; set; }
        public string AccountCombinedName { get; set; }
        public long AccountID { get; set; }
        public string Originator { get; set; }
        public string CurrentUser { get; set; }
        public string TTOrderID { get; set; }
        public string ParentID { get; set; }
        public string ManualFill { get; set; }
        public int BuySell { get; set; }
        public string Buy_Sell { get; set; }
        public decimal NetPosition { get; set; }
        public int TradeEntry { get; set; }
        public int TradeExit { get; set; }
        public int TradeNumber { get; set; }
        public decimal BuyPrice { get; set; }
        public decimal SellPrice { get; set; }
        public decimal AverageBuyPrice { get; set; }
        public decimal AverageSellPrice { get; set; }
        public decimal realised_pnl_tick { get; set; }
        public decimal realised_pnl_usd_without_commision { get; set; }
        public decimal realised_pnl_usd_with_commision { get; set; }
        public decimal unrealised_pnl_tick { get; set; }
        public decimal current_balance { get; set; }
        public decimal threshold_value { get; set; }
        public decimal commission { get; set; }
        public int? w_trades { get; set; }
        public int? l_trades { get; set; }
        public int? s_trades { get; set; }
        public decimal total_traded_qty_trade { get; set; }
        public decimal high_pnl { get; set; }
        public decimal low_pnl { get; set; }
        public decimal cumulative_pnl { get; set; }
        public decimal cumalitve_pnl_without_commision { get; set; }
        public DateTime? created_on { get; set; }
    }
}
