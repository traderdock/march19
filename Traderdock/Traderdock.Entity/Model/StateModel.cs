﻿
namespace Traderdock.Entity.Model
{
    /// <summary>
    /// State Model
    /// </summary>
    public class StateModel
    {
      
        public int state_id { get; set; }

        public int country_id { get; set; }

        public string country_code { get; set; }

        public string state_code { get; set; }

        public string state_name { get; set; }

    }
}