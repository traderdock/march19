﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Traderdock.Entity.Model
{
    public class LaunchModel
    {
        [Required(ErrorMessage = "Please enter email.")]
        [EmailAddress(ErrorMessage = "You have entered invalid email.")]
        [Display(Name = "Email")]
        public string EnquiryEmail { get; set; }
    }
}