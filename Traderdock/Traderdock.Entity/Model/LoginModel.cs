﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Traderdock.Entity.Model
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Please enter email.")]
        [EmailAddress(ErrorMessage = "You have entered invalid email.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter password.")]
        //[StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be more than 6 characters.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "You have entered invalid password.")]
        public string Password { get; set; }

        public string UserEmail { get; set; }

        public string Key { get; set; }

        [Required(ErrorMessage = "Please accept terms and conditions.")]
        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }

        [AllowHtml]
        public string LoginCMS { get; set; } // enum  LoginCMS = 20,  EnumCMSType

    }
}