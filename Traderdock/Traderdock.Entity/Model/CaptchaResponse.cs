﻿using System.Collections.Generic;

namespace Traderdock.Model.Model
{
    public class CaptchaResponse
    {
        public bool Success
        {
            get;
            set;
        }

        public List<string> ErrorMessage
        {
            get;
            set;
        }
    }
}
