﻿using System;

namespace Traderdock.Model.Model
{
    public class IPNNotificationModel
    {
        public int IpnId { get; set; }
        public string Currency { get; set; }
        public string FirstName { get; set; }
        public string GrossAmount { get; set; }
        public string LastName { get; set; }
        public DateTime? NextPaymentDate { get; set; }
        public string PayedBusinessName { get; set; }
        public string PayerEmail { get; set; }
        public string PayerId { get; set; }
        public string PayerStatus { get; set; }
        public string PaymentCycle { get; set; }
        public DateTime? PaymentDate { get; set; }
        public string PaymentType { get; set; }
        public string PeriodType { get; set; }
        public string ProfileStatus { get; set; }
        public string RecurringPaymentId { get; set; }
        public string ResidenceCountry { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
