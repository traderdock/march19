﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Model.Model
{
    public class PageModel
    {
        public int page_id { get; set; }

        public string page_name { get; set; }

        public string page_value { get; set; }
    }
}
