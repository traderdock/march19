﻿using System;

namespace Traderdock.Model.Model
{
    public class EnquiryModel
    {
        public int enquiry_id { get; set; }

        public string email { get; set; }

        public DateTime? enquiry_datetime { get; set; }

        public string enquiry_datetime_string { get; set; }


    }
}
