﻿namespace Traderdock.Entity.Model
{
    public class SettingModel
    {
        public string setting_info { get; set; }

        public int setting_id { get; set; }

        public string setting_name { get; set; }

        public string setting_value { get; set; }

        public string default_label { get; set; }

        public bool is_active { get; set; }

        public bool is_delete { get; set; }

        public System.DateTime modified_on { get; set; }

        public long modified_by { get; set; }

    }
}