﻿using System.ComponentModel.DataAnnotations;

namespace Traderdock.Entity.Model
{
    public class ChangePasswordModel
    {

        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        [Required(ErrorMessage = "Please enter email address.")]
        public string email { get; set; }

        [Display(Name = "Old Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter old password.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Please enter password at least 6 character long")]
        public string old_password { get; set; }

        [Display(Name = "New Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter new password.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Please enter password at least 6 character long")]
        public string new_password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please retype new password.")]
        [Compare("new_password", ErrorMessage = "The password and confirmation password do not match.")]
        public string confirm_password { get; set; }

    }
}