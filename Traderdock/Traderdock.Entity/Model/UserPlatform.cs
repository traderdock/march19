﻿using System;

namespace Traderdock.Model.Model
{
    public class UserPlatform
    {
        public long user_platform_id { get; set; }
        public Nullable<int> platform_id { get; set; }
        public Nullable<long> user_id { get; set; }
        public long? account_id { get; set; }
        public string account_alias { get; set; }
        public string full_name_with_alias { get; set; }
        public bool? is_active { get; set; }
        public bool? is_delete { get; set; }
        public DateTime? created_on { get; set; }
        public long? created_by { get; set; }
        public DateTime modified_on { get; set; }
        public long modified_by { get; set; }
    }
}
