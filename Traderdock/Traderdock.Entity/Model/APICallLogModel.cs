﻿namespace Traderdock.Entity.Model
{
    public class APICallLogModel
    {
        public long api_log_id { get; set; }
        public string endpoint { get; set; }
        public int api_enum { get; set; }
        public bool is_success { get; set; }
        public string description { get; set; }
        public System.DateTime creted_on { get; set; }
    }
}