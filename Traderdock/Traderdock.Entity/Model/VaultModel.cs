﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Entity.Model
{
    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: Vault Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class VaultModel
    {
        //Token Return Model
        public string access_token { get; set; }
        public string scope { get; set; }
        public string token_type { get; set; }
        public string app_id { get; set; }
        public int expires_in { get; set; }
        public string id { get; set; }
        public Int64 user_id { get; set; }
        public string state { get; set; }
        public string type { get; set; }
        public string number { get; set; }
        public string expire_month { get; set; }
        public string expire_year { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string valid_until { get; set; }
        public string create_time { get; set; }
        public string update_time { get; set; }
        public IList<VaultLinkModel> links { get; set; }
    }

    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: Vault Link Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class VaultLinkModel
    {
        public string href { get; set; }
        public string rel { get; set; }
        public string method { get; set; }
    }

    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: Vault Card Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class VaultCardModel
    {
        public string number { get; set; }
        public string type { get; set; }
        public int expire_month { get; set; }
        public int expire_year { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public VaultAddressModel billing_address { get; set; }
        public VaultCardModel()
        {
            billing_address = new VaultAddressModel();
        }
    }

    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: Vault Addrress Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class VaultAddressModel
    {
        public string line1 { get; set; }
        public string city { get; set; }
        public string country_code { get; set; }
        public string postal_code { get; set; }
        public string state { get; set; }
        public string phone { get; set; }
    }

    /// <summary>
    /// Date: 1-August-2017
    /// Dev By: Hardik Savaliya
    /// Description: User Vault Model
    /// Feedback1:
    /// Feedback2:
    /// </summary>
    public class UserVaultModel
    {
        public Int32 user_vault_id { get; set; }
        public long user_id { get; set; }
        public string vault_id { get; set; }
        public string card_type { get; set; }
        public string card_url { get; set; }
    }

}
