﻿namespace Traderdock.Entity.Model
{
    /// <summary>
    /// Country Common Model
    /// </summary>
    public class CountryModel
    {
        public int country_id { get; set; }

        public string country_name { get; set; }

        public string country_code { get; set; }

        public bool is_active { get; set; }

        public string country_status { get; set; }
    }
}