﻿namespace Traderdock.Entity.ViewModel
{
    public class UserDashboardModel
    {
        public long user_id { get; set; }

        public string first_name { get; set; }
         
        public string last_name { get; set; }

        public string SubscriptionModeString { get; set; }

        public string join_date { get; set; }

        public int Subscription_count { get; set; }

        public long Total_Revenue { get; set; }
    }
}
