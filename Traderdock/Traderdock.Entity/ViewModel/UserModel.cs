﻿ using System;
using System.ComponentModel.DataAnnotations;

namespace Traderdock.Entity.ViewModel
{
    public class UserModel
    {
        public long user_id { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter first name.")]
        public string first_name { get; set; }

        [Display(Name ="Last Name")]
        [Required(ErrorMessage ="Please enter last name.")]
        public string last_name { get; set; }

        [Display(Name ="User Name")]
        [Required(ErrorMessage ="Please enter user name.")]
        public string user_name { get; set; }

        [Display(Name ="Email")]
        [Required(ErrorMessage ="Please enter email address.")]
        public string user_email { get; set; }

        [Display(Name ="Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage ="Please enter password.")]
        [StringLength(20, MinimumLength =6, ErrorMessage ="Password should be more than 6 characters.")]
        public string password { get; set; }

        [Display(Name ="Confirm Password")]
        [DataType(DataType.Password)]
        [Compare("password", ErrorMessage ="Password and confirm password not match.")]
        public string confirm_password { get; set; }

        //public string crypto_key { get; set; }
        [Display(Name ="Gender")]
        [Required(ErrorMessage ="Please select your gender.")]
        public Nullable<bool> gender { get; set; }

        public string gender_String { get; set; }

        [Display(Name = "Birth Date")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> dob { get; set; }

        [Display(Name = "Birth Date")]
        [Required(ErrorMessage = "Please select birth date.")]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public string birth_date { get; set; }

        public string birth_date_string { get; set; }

        [Display(Name ="Address")]
        [Required(ErrorMessage = "Please enter address.")]
        public string address { get; set; }

        [Display(Name = "Latitude")]
        public string latitude { get; set; }

        [Display(Name ="Longitude")]
        public string longitude { get; set; }

        [Display(Name = "city_name")]
        public string city_name { get; set; }

        [Display(Name = "state_name")]
        public string state_name { get; set; }

        [Display(Name = "country_name")]
        public string country_name { get; set; }

        [Display(Name = "country_id")]
        public long country_id { get; set; }

        [Display(Name ="Postal Code")]
        [Required(ErrorMessage ="Please enter postal code.")]
        public string postal_code { get; set; }

        [Display(Name ="Profile Image")]
        public string profile_image_url { get; set; }

        public string FileName { get; set; }

        public bool ImageNotExist { get; set; }

        //public string country_code { get; set; }
        [Display(Name ="Mobile Number")]
        [Required(ErrorMessage ="Please enter mobile number.")]
        public string mobile_number { get; set; }

        //public string contact_number_otp { get; set; }

        //public bool is_active { get; set; }
        //public bool is_blocked { get; set; }
        //public bool is_deleted { get; set; }
        public int user_type_id { get; set; }

        public string user_status { get; set; }

        public string call_for { get; set; }

        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }


        //public int subscription_mode { get; set; }
        //public bool is_number_verified { get; set; }
        //public bool is_admin_verified { get; set; }
        //public long created_by { get; set; }
        //public System.DateTime created_on { get; set; }
        //public Nullable<long> modified_by { get; set; }
        //public Nullable<System.DateTime> modified_on { get; set; }

    }
}