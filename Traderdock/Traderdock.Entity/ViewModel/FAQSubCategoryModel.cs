﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class FAQSubCategoryModel
    {
        public long faq_sub_category_id { get; set; }

        [Display(Name = "FAQ Sub Catergory")]
        [Required(ErrorMessage = "Please select FAQ category.")]
        public long faq_category_id { get; set; }

        [Display(Name = "FAQ Category Title")]
        [Required(ErrorMessage = "Please enter category title.")]
        public string sub_category_title { get; set; }

        [AllowHtml]
        [Display(Name = "FAQ Category Description")]
        public string sub_category_description { get; set; }

        public int sub_category_order_sequence { get; set; }

        public string faq_category_title { get; set; }
        
        public bool is_active { get; set; }

        public bool is_deleted { get; set; }

        public long created_by { get; set; }

        public DateTime created_on { get; set; }

        public long modified_by { get; set; }

        public DateTime modified_on { get; set; }

        public SelectList FAQCategoryList { get; set; }

        public string SubCategoryStatus { get; set; }

        public List<FAQMasterModel> FaqMasterList { get; set; }

        public FAQSubCategoryModel()
        {
            FaqMasterList = new List<FAQMasterModel>();
        }
    }

    public class MainSubFAQModel
    {
        public MainSubFAQModel()
        {
            objFaqCMSModel = new FaqCMSModel();
            objFaqCategoryModel = new List<FAQSubCategoryModel>();
        }
        public FAQCategoryModel faqCategoryModel { get; set; }
        public FaqCMSModel objFaqCMSModel { get; set; }
        public List<FAQSubCategoryModel> objFaqCategoryModel { get; set; }
    }
}
