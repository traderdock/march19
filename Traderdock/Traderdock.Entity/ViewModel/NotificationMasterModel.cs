﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Entity.ViewModel
{
    public class NotificationMasterModel
    {
        public long notification_id { get; set; }

        public long user_notification_id { get; set; }

        public System.DateTime sent_date { get; set; }

        [Display(Name = "Your Message")]
        [Required(ErrorMessage = "Please enter notification message.")]
        public string notification_message { get; set; }

        public string sent_date_string { get; set; }
    }
}
