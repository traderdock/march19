﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Entity.ViewModel
{
    public class TestimonialModel
    {

        public long testimonial_id { get; set; }

        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Please enter user name.")]
        public string user_name { get; set; }

        [Display(Name = "User Image")]
        public string user_image_url { get; set; }

        public string FileName { get; set; }

        public bool ImageNotExist { get; set; }

        [Display(Name = "Text")]
        [Required(ErrorMessage = "Please enter text for testimonial.")]
        public string description { get; set; }

        public bool is_active { get; set; }
        public bool is_deleted { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public System.DateTime modified_on { get; set; }
        public long modified_by { get; set; }
    }
}
