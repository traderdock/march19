﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Entity.ViewModel
{
    public class UserTransactionModel
    {
        public long TransactionID { get; set; }
        public long? UserSubscriptionID { get; set; }
        public string account_number { get; set; }
        public decimal? CurrentBalance { get; set; }
        public decimal? HighBalance { get; set; }
        public decimal? LowBalance { get; set; }
        public decimal? AvgWinningDay { get; set; }
        public decimal? AvgLosingDay { get; set; }
        public decimal? BestDay { get; set; }
        public decimal? WorstDay { get; set; }
        public decimal? NetProfitLoss { get; set; }
        public decimal? HighProfitLoss { get; set; }
        public decimal? LowProfitLoss { get; set; }
        public int? TotalContracts { get; set; }
        public decimal? TotalCommisions { get; set; }
        public int? TotalTrades { get; set; }
        public decimal? AvgWinningTrade { get; set; }
        public decimal? AvgLosingTrade { get; set; }
        public decimal? WinningTrade { get; set; }
        public Nullable<System.DateTime> Transaction_date { get; set; }
        public string TransactionDateString { get; set; }
        public decimal? ProfitTarget { get; set; }
        public decimal? DailyLossLimit { get; set; }
        public decimal? MaxDrowdown { get; set; }
        public decimal? MainAccountBalance { get; set; }
        public int? TradeRequiredNoOfDays { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
    }
}
