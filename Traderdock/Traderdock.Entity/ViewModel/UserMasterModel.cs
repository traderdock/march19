﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Traderdock.Model.Model;

namespace Traderdock.Entity.ViewModel
{
    public class UserMasterModel
    {
        public long user_id { get; set; }

        [Display(Name = "First Name")]
        [Required(ErrorMessage = "Please enter first name.")]
        public string first_name { get; set; }

        [Display(Name = "Last Name")]
        [Required(ErrorMessage = "Please enter last name.")]
        public string last_name { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Please enter email address.")]
        [EmailAddress(ErrorMessage = "You have entered invalid email.")]
        public string user_email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please enter password.")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Password should be more than 6 characters.")]
        public string password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("password", ErrorMessage = "Password and confirm password not match.")]
        [Required(ErrorMessage = "Please enter confirm password.")]
        public string confirm_password { get; set; }

        //public string crypto_key { get; set; }
        [Display(Name = "Gender")]
        //[Required(ErrorMessage = "Please select your gender.")]
        public Nullable<bool> gender { get; set; }

        public string SubscriptionModeString { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        [Display(Name = "Birth Date")]
        public Nullable<System.DateTime> dob { get; set; }

        [Display(Name = "Birth Date")]
        //[Required(ErrorMessage = "Please select birth date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-\d{4}", ErrorMessage = "Invalid date.")]
        public string birth_date { get; set; }


        [Display(Name = "Address")]
        [Required(ErrorMessage = "Please enter address.")]
        public string address { get; set; }

        [Display(Name = "Latitude")]
        public string latitude { get; set; }

        [Display(Name = "Longitude")]
        public string longitude { get; set; }

        [Display(Name = "City")]
        public string city_name { get; set; }

        [Display(Name = "State Name")]
        public string state_name { get; set; }

        public string country_name { get; set; }

        [Display(Name = "Country")]
        [Required(ErrorMessage = "Please select country.")]
        public int country_id { get; set; }

        [Display(Name = "Postal Code")]
        [Required(ErrorMessage = "Please enter postal code.")]
        public string postal_code { get; set; }

        [Display(Name = "Profile Image")]
        public string profile_image_url { get; set; }

        public string FileName { get; set; }

        public bool ImageNotExist { get; set; }

        //public string country_code { get; set; }
        [Display(Name = "Phone Number")]
        [DataType(DataType.PhoneNumber)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number.")]
        [Required(ErrorMessage = "Please enter phone number.")]
        public string mobile_number { get; set; }


        public long subscription_id { get; set; }    //Added by Bharat on 30/10/2017


        public int user_type_id { get; set; }

        public string user_status { get; set; }

        public string join_date { get; set; }

        public string call_for { get; set; }

        public long created_by { get; set; }

        public System.DateTime created_on { get; set; }

        public Nullable<long> modified_by { get; set; }

        public Nullable<System.DateTime> modified_on { get; set; }

        public bool UserStatus { get; set; }

        public bool is_active { get; set; }

        public bool is_blocked { get; set; }

        public bool is_deleted { get; set; }

        public int subscription_mode { get; set; }

        public bool is_number_verified { get; set; }

        public bool is_admin_verified { get; set; }

        public bool is_password_false { get; set; }

        public bool is_email_false { get; set; }

        public string crypto_key;

        public int? email_sent_count { get; set; }

        public SelectList trading_platform_list { get; set; }

        public bool user_plan { get; set; }

        public string user_plan_string { get; set; }

        public string trading_platform_string { get; set; }

        public DateTime join_date_original { get; set; }

        public DateTime? last_payment_date { get; set; }

        public DateTime? next_payment_date { get; set; }

        public decimal? current_trading_balance { get; set; }

        public string birth_date_string { get; set; }

        public SelectList CountryList { get; set; }

        public string gender_String { get; set; }

        public string account_number { get; set; }

        public bool? reset_status { get; set; }

        public long reset_request_id { get; set; }

        public string transactions_id { get; set; }

        public decimal? order_amount { get; set; }

        public DateTime? purchase_date { get; set; }

        public string payment_method { get; set; }

        public int? trading_platform { get; set; }

        public string next_payment_date_str { get; set; }

        public List<UserPlatform> account_number_lst { get; set; }

        public decimal? discounted_balance { get; set; }

        public decimal? net_pnl_without_commision { get; set; }

        public decimal? cumalative_balance { get; set; }
    }

}
