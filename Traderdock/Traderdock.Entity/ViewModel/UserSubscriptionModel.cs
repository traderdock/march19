﻿using System;
namespace Traderdock.Entity.ViewModel
{
    public class UserSubscriptionModel
    {
     

        public string full_name { get; set; }

        public long user_subscription_id { get; set; }

        public long subscription_id { get; set; }

        public string transection_id { get; set; }

        public string subscription_name { get; set; }

        public decimal starting_balance { get; set; }

        public string starting_balance_string { get; set; }

        public int balance_unit { get; set; }

        public Nullable<int> max_position_size { get; set; }

        public Nullable<decimal> daily_loss_limit { get; set; }

        public Nullable<decimal> max_drawdown { get; set; }

        public decimal profite_target { get; set; }

        public decimal? price_per_month { get; set; }

        public bool Status { get; set; }

        public string Status_string { get; set; }

        public string transection_date_string { get; set; }

        public string start_date_string { get; set; }

        public string end_date_string { get; set; }

        public long user_id { get; set; }

        public string payment_status { get; set; }

        public string payment_method { get; set; }

        public string promo_code { get; set; }

        public DateTime? transection_date { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

    
    }
}
