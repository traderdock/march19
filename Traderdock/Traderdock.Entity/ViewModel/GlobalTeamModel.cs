﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Traderdock.Entity.ViewModel
{
    public class GlobalTeamModel
    {

        public long global_team_id { get; set; }

        [Display(Name = "User Name")]
        [Required(ErrorMessage = "Please enter user name.")]
        public string user_name { get; set; }

        [Display(Name = "Designation")]
        [Required(ErrorMessage = "Please enter designation.")]
        public string designation { get; set; }

        [Display(Name = "User Image Small")]
        public string user_image_url_sm { get; set; }

        public string FileName_sm { get; set; }

        public bool ImageNotExist_sm { get; set; }

        [Display(Name = "User Image Large")]
        public string user_image_url_lg { get; set; }

        public string FileName_lg { get; set; }

        public bool ImageNotExist_lg { get; set; }


        [Display(Name = "Text")]
        [Required(ErrorMessage = "Please enter text for global team.")]
        public string description { get; set; }

        public bool is_active { get; set; }
        public bool is_deleted { get; set; }
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public System.DateTime modified_on { get; set; }
        public long modified_by { get; set; }
    }
}
