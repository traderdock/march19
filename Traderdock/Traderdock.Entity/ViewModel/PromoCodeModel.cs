﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class PromoCodeModel
    {

        public long promo_code_id { get; set; }
        public Nullable<decimal> currency_id { get; set; }

        [Display(Name = "Promo Code")]
        [Required(ErrorMessage = "Please enter promo code.")]
        public string promo_code { get; set; }

        [Display(Name = "Promo Code Desc.")]        
        public string promo_code_desc { get; set; }

        
        [Display(Name = "From date")]
        [Required(ErrorMessage = "Please enter from date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-\d{4}", ErrorMessage = "Invalid date.")]
        public string promo_code_from_date { get; set; }



        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        
        public System.DateTime from_date { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}", ApplyFormatInEditMode = true)]        
        public System.DateTime to_date { get; set; }

        [Display(Name = "To Date")]
        [Required(ErrorMessage = "Please enter to date.")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd-mm-yyyy}", ApplyFormatInEditMode = true)]
        [RegularExpression(@"(0[1-9]|[12][0-9]|3[01])-(0[1-9]|1[012])-\d{4}", ErrorMessage = "Invalid date.")]
        public string promo_code_to_date { get; set; }



        
        
        
        

        [Display(Name = "Reward Amount")]
        [Required(ErrorMessage = "Please enter reward amount.")]        
        public decimal reward_amount { get; set; }

        public bool is_expired { get; set; }
        
        public bool Status { get; set; }

        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
    }
}
