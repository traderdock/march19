﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class FAQMasterModel
    {
        public long faq_id { get; set; }

        [Display(Name = "FAQ Catergory")]
        [Required(ErrorMessage = "Please select FAQ category.")]
        public long faq_category_id { get; set; }

        [Display(Name = "FAQ Sub Catergory")]
        [Required(ErrorMessage = "Please select sub FAQ category.")]
        public long faq_sub_category_id { get; set; }

        [Display(Name = "FAQ Question")]
        [Required(ErrorMessage = "Please enter question.")]
        public string question { get; set; }

        [AllowHtml]
        [Display(Name = "FAQ Answer")]
        [Required(ErrorMessage = "Please enter answer.")]
        public string answer { get; set; }

        public bool is_active { get; set; }

        public string faq_category_title { get; set; }

        public string faq_sub_category_title { get; set; }

        public string faq_category_description { get; set; }

        public bool is_deleted { get; set; }

        public long created_by { get; set; }

        public System.DateTime created_on { get; set; }

        public long modified_by { get; set; }

        public int order_sequences { get; set; }

        public System.DateTime modified_on { get; set; }

        public List<SelectListItem> FAQCategoryList { get; set; }

        public SelectList FAQSubCategoryList { get; set; }

        public string faqStatus { get; set; }

    }

    public class imagesviewmodel
    {
        public string Url { get; set; }
    }
}
