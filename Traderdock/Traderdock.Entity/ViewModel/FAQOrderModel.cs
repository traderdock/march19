﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Traderdock.Entity.ViewModel
{
    public class FAQOrderModel
    {
        public FAQOrderModel()
        {
            category = new List<FAQCategoryOrderModel>();
            subcategory = new List<FAQSubCategoryOrderModel>();
            faq = new List<FAQQuestionOrderModel>();
        }
        public List<FAQCategoryOrderModel> category { get; set; }
        public List<FAQSubCategoryOrderModel> subcategory { get; set; }
        public List<FAQQuestionOrderModel> faq { get; set; }
    }
    public class FAQCategoryOrderModel
    {
        public int category_order_sequence { get; set; }
        public string category_title { get; set; }
        public long category_id { get; set; }
    }
    public class FAQSubCategoryOrderModel
    {
        public int subcategory_order_sequence { get; set; }
        public string subcategory_title { get; set; }
        public long subcategory_id { get; set; }
    }
    public class FAQQuestionOrderModel
    {
        public int faq_order_sequence { get; set; }
        public string faq_title { get; set; }
        public long faq_id { get; set; }
    }
}
