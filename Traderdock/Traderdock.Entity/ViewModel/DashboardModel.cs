﻿namespace Traderdock.Entity.ViewModel
{
    public class DashboardModel
    {
        public long? TotalUser { get; set; }
        public long TotalSubscribedUser { get; set; }
        public long TotalFreeTrialUser { get; set; }
        public decimal TodaysRevenue { get; set; }
        public decimal TotalRevenue { get; set; }

        public string User_Name { get; set; }
        public string Join_Date { get; set; }
        public string Subscription_Mode { get; set; }
        public long Subscription_Count { get; set; }
        public string Total_Revenue { get; set; }
    }
}