﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class BannerModel
    {
        [Display(Name = "Select Banner Type")]
        public int banner_id { get; set; }

        [Display(Name = "Banner Type")]
        [Required(ErrorMessage = "Please select banner type.")]
        public int banner_type_id { get; set; }

        public List<SelectListItem> BannerTypeList { get; set; }

        public string banner_type_name { get; set; }

        [Display(Name = "Banner Name")]
        [Required(ErrorMessage = "Please enter banner name.")]
        public string banner_name { get; set; }

        [Display(Name = "Banner Image")]
        [Required(ErrorMessage = "Please select banner image.")]
        public string banner_image_url { get; set; }

        public string banner_image_name { get; set; }

        public string banner_image_old_name { get; set; }

        public long created_by { get; set; }

        public string created_by_string { get; set; }

        public System.DateTime created_on { get; set; }

        public System.DateTime modified_on { get; set; }

        public long modified_by { get; set; }

        public string modified_by_string { get; set; }

    }
}