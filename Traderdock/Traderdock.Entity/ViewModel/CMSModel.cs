﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class CMSModel
    {        
        [AllowHtml]
        [Display(Name = "Content")]
        [Required(ErrorMessage = "Please enter description")]
        public string cms_content { get; set; }
        //public string notes { get; set; }

        [Display(Name = "CMS Type")]
        public string type { get; set; }

        [Display(Name = "CMS Title")]
        [Required(ErrorMessage = "Please enter title")]
        public string cms_title { get; set; }

        public int cms_type { get; set; }

        public long cms_id { get; set; }

        public string page_name { get; set; }

        public bool is_active { get; set; }

        public bool is_deleted { get; set; }

        public long created_by { get; set; }

        public System.DateTime created_on { get; set; }

        public System.DateTime modified_on { get; set; }

        public long modified_by { get; set; }

        public SelectList PageList { get; set; }

        public SelectList PageSectionList { get; set; }

        [Display(Name = "CMS Pages")]
        [Required(ErrorMessage = "Please select page")]
        public string page_id { get; set; }

        [Display(Name = "CMS Pages Section ")]
        [Required(ErrorMessage = "Please select page section")]
        public string page_section_id { get; set; }


    }
}