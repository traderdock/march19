﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class EmailTemplateModel
    {
        public long EmailTemplateID { get; set; }

        public string Name { get; set; }

        [Required(ErrorMessage = "Subject is required!")]
        public string Subject { get; set; }

        [AllowHtml]
        [Required(ErrorMessage = "Body is required!")]
        public string Body { get; set; }

        public string BodyPart { get; set; }
    }
}