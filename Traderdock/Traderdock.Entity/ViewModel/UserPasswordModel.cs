﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Traderdock.Entity.ViewModel
{
    public class UserPasswordModel
    {
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Please_enter_password")]
        [StringLength(20, MinimumLength = 6, ErrorMessage = "Please enter password at least 6 character long")]
        public string new_password { get; set; }

        [Display(Name = "Confirm_Password")]
        [DataType(DataType.Password)]
        [System.ComponentModel.DataAnnotations.Compare("new_password", ErrorMessage = "The password and confirmation password do not match")]
        public string confirm_password { get; set; }

        public string email { get; set; }
        public string Key { get; set; }
    }
}