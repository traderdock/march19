﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class SubscriptionModel
    {

        public long subscription_id { get; set; }
        public Nullable<decimal> currency_id { get; set; }

        [Display(Name = "Subscription Number")]
        [Required(ErrorMessage = "Please enter subscription number.")]
        public int subscription_number { get; set; }

        [Display(Name = "Name")]
        [Required(ErrorMessage = "Please enter subscription name.")]       
        public string subscription_name { get; set; }

        [Display(Name = "Starting Balance")]
        [Required(ErrorMessage = "Please enter starting balance.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than Zero.")]
        public decimal starting_balance { get; set; }

        public string starting_balance_string { get; set; }

        public int balance_unit { get; set; }

        [Display(Name = "Max Position Size")]
        [Required(ErrorMessage = "Please enter max position size.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public Nullable<int> max_position_size { get; set; }

        [Display(Name = "Daily Loss Limit")]
        [Required(ErrorMessage = "Please enter daily loss limit.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public Nullable<decimal> daily_loss_limit { get; set; }

        [Display(Name = "Max Drawdown")]
        [Required(ErrorMessage = "Please enter max drawdown.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public Nullable<decimal> max_drawdown { get; set; }

        [Display(Name = "Profit Target")]
        [Required(ErrorMessage = "Please enter profite target.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public decimal profite_target { get; set; }

        [Display(Name = "Price Per Month")]
        [Required(ErrorMessage = "Please enter price per month.")]
        [Range(1, int.MaxValue, ErrorMessage = "Please enter a value greater than zero.")]
        public decimal price_per_month { get; set; }

        public bool Status { get; set; }

        public string start_date_string { get; set; }

        public string end_date_string { get; set; }

        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
    }
}
