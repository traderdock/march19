﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Traderdock.Entity.ViewModel
{
    public class FAQCategoryModel
    {
        public long faq_category_id { get; set; }

        [Display(Name = "FAQ Category Name")]
        [Required(ErrorMessage = "Please enter category name.")]
        public string category_title { get; set; }

        [Display(Name = "Category Description")]
        [Required(ErrorMessage = "Please enter FAQ category discription.")]
        public string category_description { get; set; }

        [Display(Name = "Category Image")]
        [Required(ErrorMessage = "Please select category image.")]
        public string category_image { get; set; }

        public string FileName { get; set; }

        public bool is_active { get; set; }

        public long created_by { get; set; }

        public System.DateTime created_on { get; set; }

        public long modified_by { get; set; }

        public System.DateTime modified_on { get; set; }

        [Display(Name = "Order Sequence")]
        [Required(ErrorMessage = "Please select Order Sequence")]
        public int order_sequence { get; set; }

        public List<FAQSubCategoryModel> SubCategory { get; set; }

        public FAQCategoryModel()
        {
            SubCategory = new List<FAQSubCategoryModel>();
        }
    }

    public class FaqCMSModel
    {
        [AllowHtml]
        public string FAQSBannerText { get; set; } // enum  WhyTraderDock1 = 40,  EnumCMSType

        [AllowHtml]
        public string FAQForEach { get; set; } // enum  WhyTraderDock1 = 49,  EnumCMSType
    }

    public class MainFAQModel
    {
        public MainFAQModel()
        {
            objFaqCMSModel = new FaqCMSModel();
            objFaqCategoryModel = new List<FAQCategoryModel>();
        }
        public FaqCMSModel objFaqCMSModel { get; set; }
        public List<FAQCategoryModel> objFaqCategoryModel { get; set; }
    }
}
