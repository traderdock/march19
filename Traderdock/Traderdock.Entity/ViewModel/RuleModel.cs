﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Traderdock.Entity.ViewModel
{
    public class RuleModel
    {
        public long rule_id { get; set; }

        [Display(Name = "Profile Target")]
        [Required(ErrorMessage = "Please enter profile target.")]
        public decimal? profit_target { get; set; }

        [Display(Name = "Daily Loss Limit")]
        [Required(ErrorMessage = "Please enter daily loss limit.")]
        public decimal? daily_loss_limit { get; set; }

        [Display(Name = "Max Drawndown")]
        [Required(ErrorMessage = "Please enter max drawndown.")]
        public decimal? max_drowdown { get; set; }

        [Display(Name = "Main A/C Balance")]
        [Required(ErrorMessage = "Please enter main a/c balance.")]
        public decimal? maintain_acacount_balance { get; set; }

        [Display(Name = "Trade required no of days")]
        [Required(ErrorMessage = "Please enter no of days.")]
        public int? trade_required_no_of_days { get; set; }

   
        public long created_by { get; set; }
        public System.DateTime created_on { get; set; }
        public Nullable<long> updated_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
    }
}
