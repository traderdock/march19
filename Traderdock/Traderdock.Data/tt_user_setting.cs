//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Traderdock.ORM
{
    using System;
    using System.Collections.Generic;
    
    public partial class tt_user_setting
    {
        public int tt_setting_id { get; set; }
        public int tradeOutAllowed { get; set; }
        public int maxPos { get; set; }
        public int maxShortPosPerContract { get; set; }
        public int maxLongPosPerContract { get; set; }
        public int maxShortPos { get; set; }
        public int maxLongPos { get; set; }
        public int outright_tradingAllowed { get; set; }
        public int outright_maxOrderQty { get; set; }
        public int outright_appliedMarginPercent { get; set; }
        public int outright_aggressiveOnlyPercentage { get; set; }
        public int outright_aggressiveOnly { get; set; }
        public int spread_trading_allowed { get; set; }
        public int spread_appliedMarginPercent { get; set; }
        public int spread_aggressiveOnly { get; set; }
        public int spread_aggressiveOnlyPercentage { get; set; }
        public int noLimits { get; set; }
        public int applyLimitsToWholesaleOrders { get; set; }
        public int dailyCreditLimit { get; set; }
        public int positionResetTypeId { get; set; }
        public int positionResetTime { get; set; }
        public string positionResetTimezone { get; set; }
        public int dailyCreditLimitRuleId { get; set; }
        public int applyToWholesaleOrders { get; set; }
        public int checkCredit { get; set; }
        public int resetPosition { get; set; }
        public int resetPositionInherited { get; set; }
        public int resetSpreadPosition { get; set; }
        public int checkCreditLoss { get; set; }
        public int autoLiquidateInherited { get; set; }
        public int checkCreditLoss_percentOfDailyCredit { get; set; }
        public int checkCreditLoss_disableTrading { get; set; }
        public int checkCreditLoss_cancelWorking { get; set; }
        public int checkCreditLoss_autoliquidateOpenPositions { get; set; }
    }
}
