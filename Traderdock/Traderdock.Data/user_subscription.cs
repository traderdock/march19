//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Traderdock.ORM
{
    using System;
    using System.Collections.Generic;
    
    public partial class user_subscription
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public user_subscription()
        {
            this.account_info = new HashSet<account_info>();
            this.balance_history = new HashSet<balance_history>();
            this.reset_requests = new HashSet<reset_requests>();
            this.tt_fill_master_manual = new HashSet<tt_fill_master_manual>();
            this.tt_position_master = new HashSet<tt_position_master>();
        }
    
        public long user_subscription_id { get; set; }
        public long user_id { get; set; }
        public long subscription_id { get; set; }
        public string transaction_id { get; set; }
        public Nullable<decimal> order_amount { get; set; }
        public Nullable<System.DateTime> purchase_date { get; set; }
        public string payment_method { get; set; }
        public Nullable<int> payment_status { get; set; }
        public Nullable<int> payment_response_code { get; set; }
        public Nullable<bool> is_expired { get; set; }
        public Nullable<bool> is_current { get; set; }
        public Nullable<System.DateTime> subscription_start_date { get; set; }
        public Nullable<System.DateTime> subscription_end_date { get; set; }
        public Nullable<bool> free_trial { get; set; }
        public Nullable<int> promo_code_id { get; set; }
        public Nullable<decimal> discount_amount { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public Nullable<long> created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public Nullable<long> updated_by { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<account_info> account_info { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<balance_history> balance_history { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<reset_requests> reset_requests { get; set; }
        public virtual subscription_master subscription_master { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tt_fill_master_manual> tt_fill_master_manual { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<tt_position_master> tt_position_master { get; set; }
        public virtual user_master user_master { get; set; }
    }
}
