﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Traderdock.Web.Startup))]
namespace Traderdock.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
