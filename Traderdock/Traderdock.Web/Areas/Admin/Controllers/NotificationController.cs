﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class NotificationController : Controller
    {
        #region  Connection
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Service
        CommonService commonService = new CommonService();
        NotificationService objNotificationService = new NotificationService();
        #endregion

        #region Notification list
        /// <summary>
        /// Notificaation List
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<NotificationMasterModel> List = new List<NotificationMasterModel>();
            return View(List);
        }

        /// <summary>
        /// Notificaation List From Kendo Grid
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public JsonResult Notification_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            UserService ObjUserService = new UserService();
            var notifications = objNotificationService.NotificationList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = notifications,
                Total = notifications.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Send Notification 
        /// <summary>
        /// Send Notification Page Load Request
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SendNotification(Int64? id)
        {
            NotificationMasterModel objNotificationModel = new NotificationMasterModel();
            return View(objNotificationModel);
        }

        /// <summary>
        /// Send Notification
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpPost]
        public ActionResult SendNotification(NotificationMasterModel objModel)
        {
            if (ModelState.IsValid)
            {
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    //objModel.sent_date = System.DateTime.UtcNow;
                    objModel.sent_date = DateTime.UtcNow;
                    bool bFlag = objNotificationService.AddNotification(objModel);
                    if (bFlag)
                        transaction.Commit();
                    else
                    {
                        transaction.Rollback();
                    }
                    return RedirectToAction("Index");

                }
            }
            else
                return View(objModel);
        }
        #endregion
    }
}