﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class SubscriptionController : Controller
    {
        #region Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService commonService = new CommonService();
        SubscriptionService objSubscriptionService = new SubscriptionService();
        #endregion

        #region Get Subscription
        /// <summary>
        /// Index 
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<SubscriptionModel> List = new List<SubscriptionModel>();
            return View(List);
        }

        /// <summary>
        /// Subscription Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult Subscription_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            UserService ObjUserService = new UserService();
            var SubscriptionList = objSubscriptionService.SubscriptionList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = SubscriptionList,
                Total = SubscriptionList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Add Edit Subscription Method
        /// <summary>
        /// Add Edit Subscription
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditSubscription(Int64? id)
        {
            SubscriptionModel objSubscriptionModel = new SubscriptionModel();
            if (id > 0)
                objSubscriptionModel = objSubscriptionService.GetSubscription(id);
            else
            {
                objSubscriptionModel.max_drawdown = 0;
                objSubscriptionModel.daily_loss_limit = 0;
                objSubscriptionModel.max_position_size = 0;
            }
            return View(objSubscriptionModel);
        }

        [HttpPost]
        public ActionResult AddEditSubscription(SubscriptionModel objModel, HttpPostedFileBase objFile)
        {
            if (ModelState.IsValid)
            {
                objSubscriptionService = new SubscriptionService();
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.subscription_id > 0)
                    {
                        objModel.subscription_id = objModel.subscription_id;
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objSubscriptionService.AddEditSubscription(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.created_on = System.DateTime.UtcNow;
                        bool bFlag = objSubscriptionService.AddEditSubscription(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                        {
                            transaction.Rollback();
                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            else
                return View(objModel);
        }
        #endregion

        #region Delete Checked
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objSubscriptionService = new SubscriptionService();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<subscription_master> objSubscriptionMasterList = new List<subscription_master>();
            objSubscriptionMasterList = objSubscriptionService.GetSubscriptions(lIds);
            if (objSubscriptionMasterList != null && objSubscriptionMasterList.Count > 0)
            {
                objSubscriptionMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                objSubscriptionMasterList.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                bool bResult = objSubscriptionService.DeleteSubscriptions(objSubscriptionMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Subscription");
        }

        [HttpGet]
        public ActionResult DeleteChecked(string sID)
        {
            objSubscriptionService = new SubscriptionService();
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            SubscriptionModel objSubscriptionModel = new SubscriptionModel();
            try
            {
                if (IDs != null)
                {
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    IList<subscription_master> subscriptionlist = dbConnection.subscription_master.Where(x => TagIds.Contains(x.subscription_id)).ToList();
                    bool bResult = objSubscriptionService.DeleteSubscriptions(subscriptionlist);
                    //bool bResult = true;
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Class");
        }
        #endregion

        #region Active/In Active Checked
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            objSubscriptionService = new SubscriptionService();
            SubscriptionModel objSubscriptionModel = new SubscriptionModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objSubscriptionService.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Subscription");
        }

        /// <summary>
        /// In Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            objSubscriptionService = new SubscriptionService();
            SubscriptionModel objSubscriptionModel = new SubscriptionModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objSubscriptionService.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Subscription");
        }

        /// <summary>
        /// Active  In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            objSubscriptionService = new SubscriptionService();
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objSubscriptionService.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = objSubscriptionService.ActiveInactive(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Subscription");
        }
        #endregion
    }
}