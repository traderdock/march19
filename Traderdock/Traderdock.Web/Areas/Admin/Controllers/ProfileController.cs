﻿using System;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class ProfileController : Controller
    {
        #region  Global Variables
        UserService objUserService;
        #endregion

        #region View  Profile
        /// <summary>
        /// View Profile
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult View(long userId)
        {
            try
            {
                UserMasterModel objUserMasterModel = new UserMasterModel();
                objUserService = new UserService();
                if (userId > 0)
                {
                    objUserMasterModel = objUserService.GetUser(userId);
                    if (objUserMasterModel != null)
                    {
                        if (objUserMasterModel.birth_date != null)
                            objUserMasterModel.birth_date_string = (objUserMasterModel.birth_date).ToString();

                        if (objUserMasterModel.profile_image_url == null)
                        {
                            objUserMasterModel.profile_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                            objUserMasterModel.FileName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                        }
                    }
                    else
                    {
                        TempData["SelectedProfile"] = null;
                    }
                }
                return View(objUserMasterModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Profile Image
        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        public ActionResult UploadProfileImage()
        {
            string _FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    _FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var _FileName = TempData["SelectedProfile"].ToString();
                        var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                        {
                            System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        }
                    }
                    TempData["SelectedProfile"] = _FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(new { Data = _FileNameImage }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        public ActionResult SaveImage(string FileName)
        {
            try
            {
                objUserService = new UserService();
                bool Status = false;
                if (!string.IsNullOrEmpty(FileName))
                {
                    objUserService = new UserService();
                    long UserID = Convert.ToInt64(Session["UserID"].ToString());
                    if (UserID > 0)
                    {
                        Status = objUserService.SaveUserImage(UserID, FileName);
                        if (Status)
                        {
                            Session["ProfileImage"] = FileName;
                            return Json(true, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(false, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Change Password
        /// <summary>
        /// Change Password
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ChangePassword()
        {
            try
            {
                ChangePasswordModel changePasswordModel = new ChangePasswordModel();
                return View(changePasswordModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel storepassModel)
        {
            try
            {
                bool Status = false;
                ModelState.Remove("email");
                objUserService = new UserService();
                if (ModelState.IsValid)
                {
                    long UserID = Convert.ToInt64(Session["UserID"].ToString());
                    Status = objUserService.AdminPasswordChange(storepassModel, UserID);
                    if (Status)
                    {
                        TempData["CngPwdSMsg"] = "Your password was changed successfully.";
                        return RedirectToAction("Login", "Login");
                    }
                    else
                        TempData["CngPwdEMsg"] = "Please enter correct password.";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(storepassModel);
        }
        #endregion

        #region Save User
        /// <summary>
        /// Save User
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="callFor"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SaveUser(long? userId, string callFor = "u")
        {
            try
            {
                objUserService = new UserService();
                UserMasterModel objUserModel = new UserMasterModel();
                if (userId > 0)
                {
                    objUserModel = objUserService.GetUser(userId);
                }
                else
                {
                    TempData["SelectedProfile"] = null;
                }
                objUserModel.call_for = callFor;
                return View(objUserModel);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        [HttpPost]
        public ActionResult SaveUser(UserMasterModel objUserModel)
        {
            try
            {
                bool Status = false;
                long UserID = Convert.ToInt64(Session["UserID"].ToString());
                objUserService = new UserService();
                if (objUserModel.user_id > 0)
                {
                    ModelState.Remove("password");
                    ModelState.Remove("confirm_password");
                }
                if (ModelState.IsValid)
                {
                    if (objUserModel.birth_date != null)
                    {
                        string[] formats = { "dd-MM-yyyy" };
                        objUserModel.dob = DateTime.ParseExact(objUserModel.birth_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                    }
                    if (objUserModel.user_id > 0)
                    {
                        #region Update Record
                        Status = objUserService.AddEditUser(objUserModel);
                        if (Status)
                        {
                            return RedirectToAction("Dashboard", "Dashboard");
                        }
                        else
                        {
                            TempData["Error"] = "No matching record found.";
                            return RedirectToAction("Dashboard", "Dashboard");
                        }
                        #endregion
                    }
                    else
                    {
                        #region Add Record
                        Status = objUserService.AddEditUser(objUserModel);
                        if (Status)
                        {
                            //need redirection method here
                        }
                        #endregion
                    }
                }
                //Profile Bug 05-07
                //else
                //{
                //    objUserModel = objUserService.GetUser(UserID);
                //    return View(objUserModel);
                //}
                objUserModel.call_for = "u";
                return View(objUserModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Public Methods
        public JsonResult CheckUserExists(string Name, string Type)
        {
            try
            {
                bool IsExist = true;
                if (Type.ToLower() == "e")
                {
                    IsExist = objUserService.CheckUserEmailExist(Name);
                    if (IsExist)
                    {
                        TempData["ErrorMsg"] = "Email is already in use. please choose another";
                    }
                }
                return Json(IsExist, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}