﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class BannerController : Controller
    {
        #region Global Variables
        BannerService objService;
        #endregion

        #region Get Banner
        /// <summary>
        /// Index 
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<BannerModel> List = new List<BannerModel>();
            return View(List);
        }

        /// <summary>
        /// Subscription Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult Banner_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            //UserService ObjUserService = new UserService();//ignore 
            objService = new BannerService();
            //var SubscriptionList1 = objSubscriptionService.SubscriptionList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);//ignore
            var BannerList = objService.BannerList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = BannerList,
                Total = BannerList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region View Banner
        /// <summary>
        /// View Banner
        /// </summary>
        /// <param name="lBannerID"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ViewBanner(long? BannerId)
        {
            BannerModel objModel = null;
            objService = new BannerService();
            try
            {
                if (BannerId > 0)
                {
                    objModel = objService.GetBanner(BannerId);
                    if (objModel != null)
                        return View(objModel);
                    else
                        return RedirectToAction("GetBanner");
                }
                else
                {
                    return RedirectToAction("GetBanner");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion        

        #region Add/Edit Banner
        /// <summary>
        /// Save Banner
        /// </summary>
        /// <param name="BannerId"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SaveBanner(long? Id)
        {
            BannerModel BannerModel = new BannerModel();
            objService = new BannerService();
            try
            {
                if (Id > 0)
                {
                    BannerModel = objService.GetBanner(Id);
                }
                //if (string.IsNullOrEmpty(BannerModel.banner_image_url))
                //{
                //    BannerModel.banner_image_url = string.Empty;
                //    BannerModel.banner_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                //    BannerModel.banner_image_name = Utilities.GetImageName(BannerModel.banner_image_url);
                //}
                //BannerModel.BannerTypeList = new List<SelectListItem>()
                BannerModel.BannerTypeList = GetBannerType();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(BannerModel);
        }

        #region Private Methods
        /// <summary>
        /// Get FAQ Category
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        private List<SelectListItem> GetBannerType()
        {
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();

            try
            {
                //EnumBannerType;
                //HomePageBanner = 1,
                //ContactUsBanner = 2,
                var data = new SelectListItem { Text = "Home Page Banner", Value = "1" };
                lstSelectListItem.Add(data);
                data = new SelectListItem { Text = "Contact Us Banner", Value = "2" };
                lstSelectListItem.Add(data);

            }
            catch (Exception)
            {

                throw;
            }
            return lstSelectListItem;
        }
        #endregion


        /// <summary>
        /// Save Banner
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveBanner(BannerModel objModel)
        {
            bool Status = false;
            objService = new BannerService();
            try
            {
                if (ModelState.IsValid)
                {
                    objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                    var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                    Status = objService.SaveBanner(objModel, _AttachmentPath);
                    if (Status)
                        return View("index");
                    //return new JsonResult { Data = "Success" };
                    else
                        return View(objModel);
                }
                else
                {
                    objModel.BannerTypeList = GetBannerType();
                    return View(objModel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region FAQ Category Image Upload
        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        public ActionResult UploadImage()
        {
            string _FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    _FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var _FileName = TempData["SelectedProfile"].ToString();
                        var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                        {
                            //System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        }
                    }
                    TempData["SelectedProfile"] = _FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(new { Data = _FileNameImage }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Banner Delete
        /// <summary>
        /// Banner Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objService = new BannerService();
            if (ID > 0)
            {
                bool bResult = objService.DeleteBanner(ID);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "FAQCategory");
        }
        #endregion
    }
}