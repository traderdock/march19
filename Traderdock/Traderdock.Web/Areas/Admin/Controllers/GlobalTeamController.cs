﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class GlobalTeamController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        GlobalTeamService objService = new GlobalTeamService();
        #endregion

        #region Get Global Team
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 12/12/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<GlobalTeamModel> List = new List<GlobalTeamModel>();
            return View(List);
        }

        /// <summary>
        /// Get Global Team Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 12/12/2017
        /// Dev By: Bharat Katua
        public JsonResult GlobalTeam_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objService = new GlobalTeamService();
            var users = objService.GlobalTeamList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Add/Edit Global Team
        /// <summary>
        /// Add Edit Global Team
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 12/12/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditGlobalTeam(Int64? id)
        {
            objService = new GlobalTeamService();
            objCommonService = new CommonService();
            GlobalTeamModel objModel = new GlobalTeamModel();
            if (id > 0)
                objModel = objService.GetGlobalTeam(id);

            if (string.IsNullOrEmpty(objModel.user_image_url_sm))
            {
                objModel.user_image_url_sm = string.Empty;
                objModel.user_image_url_sm = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                objModel.FileName_sm = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
            }
            if (string.IsNullOrEmpty(objModel.user_image_url_lg))
            {
                objModel.user_image_url_lg = string.Empty;
                objModel.user_image_url_lg = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                objModel.FileName_lg = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
            }
            return View(objModel);
        }

        /// <summary>
        /// Add Edit Global Team
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="objFile"></param>
        /// <returns></returns>
        /// Date : 12/12/2017
        /// Dev By: Bharat Katua
        [HttpPost]
        public ActionResult AddEditGlobalTeam(GlobalTeamModel objModel, HttpPostedFileBase objFile)
        {
            if (ModelState.IsValid)
            {
                //Transaction will be maintain
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.global_team_id > 0)
                    {
                        objModel.global_team_id = objModel.global_team_id;
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objService.AddEditGlobalTeam(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //for Add User 
                        global_team objMaster = dbConnection.global_team.Where(x => x.is_active && x.is_deleted == false && (x.user_name.ToLower().Equals(objModel.user_name.ToLower()))).FirstOrDefault();
                        if (objMaster == null)
                        {
                            objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                            objModel.created_on = System.DateTime.UtcNow;
                            bool bFlag = objService.AddEditGlobalTeam(objModel);
                            if (bFlag)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                            }
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["Msg"] = "Record already exist.";
                            return View(objModel);
                        }
                    }
                }
            }
            else
            {
                return View(objModel);
            }
        }
        #endregion

        [HttpGet]
        public ActionResult Delete(int ID)
        {
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<global_team> objUserMasterList = new List<global_team>();
            objUserMasterList = objService.GetGlobalTeams(lIds);
            if (objUserMasterList != null && objUserMasterList.Count > 0)
            {
                objUserMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                objUserMasterList.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                bool bResult = objService.DeleteGlobalTeams(objUserMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "GlobalTeam");
        }

        public ActionResult UploadGlobalImage()
        {
            string _FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    _FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var _FileName = TempData["SelectedProfile"].ToString();
                        var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        //if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                        //{
                        //    System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        //}
                    }
                    TempData["SelectedProfile"] = _FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(new { Data = _FileNameImage }, JsonRequestBehavior.AllowGet);
        }
    }
}