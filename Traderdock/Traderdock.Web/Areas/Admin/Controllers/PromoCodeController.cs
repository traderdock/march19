﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class PromoCodeController : Controller
    {
        #region Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService commonService = new CommonService();
        PromoCodeService objPromoCodeService = new PromoCodeService();
        #endregion

        #region Get PromoCode
        /// <summary>
        /// Index 
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<PromoCodeModel> List = new List<PromoCodeModel>();
            return View(List);
        }

        /// <summary>
        /// PromoCode Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult PromoCode_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            PromoCodeService ObjPromoCodeService = new PromoCodeService();
            var PromoCodeList = objPromoCodeService.PromocodeList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = PromoCodeList,
                Total = PromoCodeList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        [AllowAnonymous]
        public JsonResult CheckUserEmailExists(string promocode, int id)
        {
            try
            {
                PromoCodeService objPromoCodeService = new PromoCodeService();
                bool bFlag = objPromoCodeService.CheckPromocode(promocode, id);
                if (bFlag)
                {
                    TempData["ErrorMsg"] = "Promo Code is already in exist. please choose another";
                }
                return Json(bFlag, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Add Edit PromoCode Method
        /// <summary>
        /// Add Edit PromoCode
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditPromoCode(Int64? id)
        {
            PromoCodeModel objPromoCodeModel = new PromoCodeModel();
            if (id > 0)
                objPromoCodeModel = objPromoCodeService.GetPromocode(id);

            return View(objPromoCodeModel);
        }

        [HttpPost]
        public ActionResult AddEditPromoCode(PromoCodeModel objModel, HttpPostedFileBase objFile)
        {
            if (ModelState.IsValid)
            {
                objPromoCodeService = new PromoCodeService();
                using (var transaction = dbConnection.Database.BeginTransaction())
                {

                    objModel.promo_code_id = objModel.promo_code_id;
                    objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                    objModel.modified_on = System.DateTime.UtcNow;
                    objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                    objModel.created_on = System.DateTime.UtcNow;
                    if (!string.IsNullOrEmpty(objModel.promo_code_from_date))
                    {
                        string[] formats = { "dd-MM-yyyy" };
                        objModel.from_date = DateTime.ParseExact(objModel.promo_code_from_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                    }
                    if (!string.IsNullOrEmpty(objModel.promo_code_to_date))
                    {
                        string[] formats = { "dd-MM-yyyy" };
                        objModel.to_date = DateTime.ParseExact(objModel.promo_code_to_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                    }
                    bool bFlag = objPromoCodeService.AddEditPromocode(objModel);
                    if (bFlag)
                        transaction.Commit();
                    else
                        transaction.Rollback();

                    return RedirectToAction("Index");




                }
            }
            else
                return View(objModel);
        }
        #endregion

        #region Delete Checked
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objPromoCodeService = new PromoCodeService();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<promo_code> objPromoCodeMasterList = new List<promo_code>();
            objPromoCodeMasterList = objPromoCodeService.GetPromocode(lIds);
            if (objPromoCodeMasterList != null && objPromoCodeMasterList.Count > 0)
            {
                objPromoCodeMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                objPromoCodeMasterList.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                bool bResult = objPromoCodeService.DeletePromocode(objPromoCodeMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "PromoCode");
        }

        [HttpGet]
        public ActionResult DeleteChecked(string sID)
        {
            objPromoCodeService = new PromoCodeService();
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            PromoCodeModel objPromoCodeModel = new PromoCodeModel();
            try
            {
                if (IDs != null)
                {
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    IList<promo_code> subscriptionlist = dbConnection.promo_code.Where(x => TagIds.Contains(x.promo_code_id)).ToList();
                    bool bResult = objPromoCodeService.DeletePromocode(subscriptionlist);
                    //bool bResult = true;
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Promocode");
        }
        #endregion

        #region Active/In Active Checked
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            objPromoCodeService = new PromoCodeService();
            PromoCodeModel objPromoCodeModel = new PromoCodeModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objPromoCodeService.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "PromoCode");
        }

        /// <summary>
        /// In Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            objPromoCodeService = new PromoCodeService();
            PromoCodeModel objPromoCodeModel = new PromoCodeModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objPromoCodeService.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "PromoCode");
        }

        /// <summary>
        /// Active  In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            objPromoCodeService = new PromoCodeService();
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objPromoCodeService.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = objPromoCodeService.ActiveInactive(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "PromoCode");
        }
        #endregion

  
    }
}