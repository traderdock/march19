﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class TestimonialController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        TestimonialService objService = new TestimonialService();
        #endregion

        #region Get Testimonial
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 08/12/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<TestimonialModel> List = new List<TestimonialModel>();
            return View(List);
        }

        /// <summary>
        /// Get Testimonial Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 08/12/2017
        /// Dev By: Bharat Katua
        public JsonResult Testimonial_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objService = new TestimonialService();
            var users = objService.TestimonialList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };

            return Json(gridModel);
        }
        #endregion


        #region Add/Edit Testimonial
        /// <summary>
        /// Add Edit Testimonial
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 08/12/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditTestimonial(Int64? id)
        {
            objService = new TestimonialService();
            objCommonService = new CommonService();
            TestimonialModel objModel = new TestimonialModel();
            if (id > 0)
                objModel = objService.GetTestimonial(id);

            if (string.IsNullOrEmpty(objModel.user_image_url))
            {
                objModel.user_image_url = string.Empty;
                objModel.user_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                objModel.FileName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
            }
            return View(objModel);
        }

        /// <summary>
        /// Add Edit Testimonial
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="objFile"></param>
        /// <returns></returns>
        /// Date : 08/12/2017
        /// Dev By: Bharat Katua
        [HttpPost]
        public ActionResult AddEditTestimonial(TestimonialModel objModel, HttpPostedFileBase objFile)
        {
            if (ModelState.IsValid)
            {
                //Transaction will be maintain
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.testimonial_id > 0)
                    {
                        objModel.testimonial_id = objModel.testimonial_id;
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objService.AddEditTestimonial(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //for Add User 
                        testimonial_master objMaster = dbConnection.testimonial_master.Where(x => x.is_active && x.is_deleted == false && (x.user_name.ToLower().Equals(objModel.user_name.ToLower()))).FirstOrDefault();
                        if (objMaster == null)
                        {
                            objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                            objModel.created_on = System.DateTime.UtcNow;
                            bool bFlag = objService.AddEditTestimonial(objModel);
                            if (bFlag)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                            }
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            TempData["Msg"] = "Record already exist.";
                            return View(objModel);
                        }
                    }
                }
            }
            else
            {
                return View(objModel);
            }
        }
        #endregion

        [HttpGet]
        public ActionResult Delete(int ID)
        {
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<testimonial_master> objUserMasterList = new List<testimonial_master>();
            objUserMasterList = objService.GetTestimonials(lIds);
            if (objUserMasterList != null && objUserMasterList.Count > 0)
            {
                objUserMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                objUserMasterList.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                bool bResult = objService.DeleteTestimonials(objUserMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Testimonial");
        }

    }
}