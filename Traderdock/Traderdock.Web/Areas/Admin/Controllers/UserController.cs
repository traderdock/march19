﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.ORM;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;
using System.IO;
using System.Globalization;
using Traderdock.Entity.MemberModel;
using System.Data;
using LumenWorks.Framework.IO.Csv;
using System.Net;
using System.Configuration;
using System.Text;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using Traderdock.Common.Enumerations;
using Traderdock.Model.Model;
using Traderdock.Model.TTModel;
using Traderdock.Common.Exceptions;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class UserController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        UserService objUserService = new UserService();
        #endregion

        #region Get User
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<UserMasterModel> List = new List<UserMasterModel>();
            return View(List);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        public JsonResult User_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objUserService = new UserService();
            var users = objUserService.UserList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };
            return Json(gridModel);
        }
        #endregion

        #region Add/Edit User
        /// <summary>
        /// Add Edit User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditUser(Int64? id)
        {
            objUserService = new UserService();
            objCommonService = new CommonService();
            UserMasterModel objUserModel = new UserMasterModel();
            if (id > 0)
                objUserModel = objUserService.GetUser(id);
            else
            {
                objUserModel.gender = true;
            }
            objUserModel.trading_platform_list = new SelectList(GetPlatform(), "Value", "Text");
            objUserModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            if (string.IsNullOrEmpty(objUserModel.profile_image_url))
            {
                objUserModel.profile_image_url = string.Empty;
                objUserModel.profile_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                objUserModel.FileName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
            }

            objUserModel.account_number_lst = objUserService.GetUserAccountList(id.Value);
            return View(objUserModel);
        }

        /// <summary>
        /// Add Edit User 
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="objFile"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [HttpPost]
        public ActionResult AddEditUser(UserMasterModel objModel, HttpPostedFileBase objFile)
        {
            if (objModel.user_id > 0)
            {
                ModelState.Remove("Password");
                ModelState.Remove("confirm_password");
            }
            if (ModelState.IsValid)
            {
                //Transaction will be maintain
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.user_id > 0)
                    {
                        objModel.user_id = objModel.user_id;
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        if (!string.IsNullOrEmpty(objModel.birth_date))
                        {
                            string[] formats = { "dd-MM-yyyy" };
                            objModel.dob = DateTime.ParseExact(objModel.birth_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                        }
                        bool bFlag = objUserService.AddEditUser(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        //for Add User 
                        user_master objUserMaster = dbConnection.user_master.Where(x => x.is_active && x.is_deleted == false && (x.user_email.ToLower().Equals(objModel.user_email.ToLower()))).FirstOrDefault();
                        if (objUserMaster == null)
                        {
                            objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                            objModel.created_on = System.DateTime.UtcNow;
                            if (!string.IsNullOrEmpty(objModel.birth_date))
                            {
                                string[] formats = { "dd-mm-yyyy" };
                                objModel.dob = DateTime.ParseExact(objModel.birth_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                            }
                            bool bFlag = objUserService.AddEditUser(objModel);
                            if (bFlag)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                            }
                            return RedirectToAction("Index");
                        }
                        else
                        {
                            objModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                            TempData["Msg"] = "Record already exist successfully.";
                            return View(objModel);
                        }
                    }
                }
            }
            else
            {
                objModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                return View(objModel);
            }
        }
        #endregion

        #region Delete User
        /// <summary>
        /// Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            //UserMasterModel objUserModel = new UserMasterModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<user_master> objUserMasterList = new List<user_master>();
            objUserMasterList = objUserService.GetUsers(lIds);
            if (objUserMasterList != null && objUserMasterList.Count > 0)
            {
                objUserMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                objUserMasterList.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                bool bResult = objUserService.DeleteUsers(objUserMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public ActionResult DeleteChecked(string sID)
        {
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            UserModel objUserModel = new UserModel();
            user_master objUserMaster = new user_master();
            try
            {
                if (IDs != null)
                {
                    UserService ObjUserService = new UserService();
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    IList<user_master> objUserMasterlist = dbConnection.user_master.Where(x => TagIds.Contains(x.user_id)).ToList();
                    bool bResult = ObjUserService.DeleteUsers(objUserMasterlist);
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Class");
        }
        #endregion

        #region Active/In Active Checked
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            UserModel objUserModel = new UserModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objUserService.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "User");
        }

        /// <summary>
        /// In Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            UserModel objUserModel = new UserModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objUserService.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "User");
        }

        /// <summary>
        /// Active In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 03/07/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    UserService ObjUserService = new UserService();
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objUserService.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = ObjUserService.ActiveInactive(lIds, LoginUserId, false);

                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            return RedirectToAction("Index", "User");
        }
        #endregion

        #region Public Method
        /// <summary>
        /// Check User email exists or not
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        [AllowAnonymous]
        public JsonResult CheckUserEmailExists(string email)
        {
            try
            {
                bool bFlag = objUserService.CheckUserEmailExist(email);
                if (bFlag)
                {
                    TempData["ErrorMsg"] = "Email is already in use. please choose another";
                }
                return Json(bFlag, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Upload Profile Image
        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        public ActionResult UploadProfileImage()
        {
            string _FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    _FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var _FileName = TempData["SelectedProfile"].ToString();
                        var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                        {
                            System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        }
                    }
                    TempData["SelectedProfile"] = _FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(new { Data = _FileNameImage }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region View User Details
        /// <summary>
        /// View User Details
        /// </summary>
        /// <param name="id">User ID</param>
        /// <returns></returns>
        /// Date : 20/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ViewUser(Int64? id)
        {
            objUserService = new UserService();
            objCommonService = new CommonService();
            DashboardService ObjDashboardService = new DashboardService();
            SubscriptionService objSubscriptionService = new SubscriptionService();
            WEBDashboardModel objWEBDashboardModel = new WEBDashboardModel();
            objWEBDashboardModel.objUserMasterModel = new UserMasterModel();
            if (id > 0)
                objWEBDashboardModel.objUserMasterModel = objUserService.GetUser(id);
            else
                objWEBDashboardModel.objUserMasterModel.gender = true;
            if (objWEBDashboardModel.objUserMasterModel != null)
            {
                objWEBDashboardModel.objUserMasterModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                if (string.IsNullOrEmpty(objWEBDashboardModel.objUserMasterModel.profile_image_url))
                {
                    objWEBDashboardModel.objUserMasterModel.profile_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                    objWEBDashboardModel.objUserMasterModel.FileName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
            if (id > 0)
            {
                objWEBDashboardModel.ObjHomeModel = ObjDashboardService.GetUserAccountBalanceNew(id.Value);
                objWEBDashboardModel.objAccountInfoModel = ObjDashboardService.GetAccountInfoNew(id.Value);
                objWEBDashboardModel.ObjRuleModel = ObjDashboardService.GetRuleNew(id.Value);
                //objWEBDashboardModel.UserTransactionList = ObjDashboardService.GetUserTransactionList(id.Value);
                long SibscriptionID = dbConnection.user_subscription.Where(x => x.user_id == id.Value && x.is_current.Value).Select(x => x.subscription_id).FirstOrDefault();
                if (SibscriptionID > 0)
                {
                    objWEBDashboardModel.objPlanMemModel = objSubscriptionService.GetSubscription(SibscriptionID);
                    if (objWEBDashboardModel.objPlanMemModel.starting_balance > 0)
                    {
                        objWEBDashboardModel.objPlanMemModel.starting_balance_string = Convert.ToString(Convert.ToInt32(objWEBDashboardModel.objPlanMemModel.starting_balance / 1000));
                    }
                }
                else
                {
                    objWEBDashboardModel.objPlanMemModel = new SubscriptionModel();
                }
                objWEBDashboardModel.objUserSubscriptionList = objSubscriptionService.GetUserSubscriptionsList1(id.Value);

            }
            else
            {
                objWEBDashboardModel.ObjHomeModel = new HomePageModel();
                objWEBDashboardModel.objAccountInfoModel = new AccountInfo();
                objWEBDashboardModel.ObjRuleModel = new Entity.MemberModel.RuleModel();
            }
            return View(objWEBDashboardModel);
        }

        public JsonResult Subscription_Read(DataSourceRequest model, string start_date = "", string end_date = "", string statusFilter = "", string user_id = "")
        {
            if (!string.IsNullOrEmpty(start_date))
            {
                string[] values = start_date.Split('-');
                start_date = values[0].Trim();
                end_date = values[1].Trim();

                string[] s_date = start_date.Split('/');
                start_date = s_date[1].Trim();
                start_date = start_date + "/" + s_date[0].Trim();
                start_date = start_date + "/" + s_date[2].Trim();

                string[] e_date = end_date.Split('/');
                end_date = e_date[1].Trim();
                end_date = end_date + "/" + e_date[0].Trim();
                end_date = end_date + "/" + e_date[2].Trim();

            }
            long UserId = Convert.ToInt64(user_id);
            SubscriptionService objSubscriptionService = new SubscriptionService();
            var SubscriptionList = objSubscriptionService.GetUserSubscriptionsList(start_date, end_date, model.Page - 1, model.PageSize, statusFilter, UserId);

            var gridModel = new DataSourceResult
            {
                Data = SubscriptionList,
                Total = SubscriptionList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Get Current Balance
        /// <summary>
        /// Get Current Balance
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 17/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ActionResult GetCurrentBalList(string Year, string Month, string Week, string user_id)
        {
            int userID = Convert.ToInt32(user_id);
            WEBDashboardModel model = new WEBDashboardModel();
            ServiceResponse<List<CurrentbalChart>> response = new ServiceResponse<List<CurrentbalChart>>();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                //response = ObjDashboardService.GetCurrentBalanceList(userID, Year, Month, Week);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CSV Read & Upload it in DB
        /// <summary>
        /// CSV Read 
        /// dev by : Bharat Katua
        /// date : 26/07/2017
        /// </summary>
        /// <returns></returns>
        public ActionResult readcsv()
        {
            return View();
        }

        /// <summary>
        /// CSV Data Upload in DB
        /// dev by : Bharat Katua
        /// date : 26/07/2017
        /// </summary>
        /// <param name="upload"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult readcsv(HttpPostedFileBase upload)
        {
            objUserService = new UserService();
            UserTransactionModel model = new UserTransactionModel();
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    if (upload.FileName.EndsWith(".csv"))
                    {
                        Stream stream = upload.InputStream;
                        DataTable csvTable = new DataTable();
                        using (CsvReader csvReader =
                            new CsvReader(new StreamReader(stream), true))
                        {
                            csvTable.Load(csvReader);
                        }
                        string[] data;
                        foreach (DataRow row in csvTable.Rows)
                        {
                            data = row.ItemArray.Select(x => x.ToString()).ToArray();
                            model = objUserService.csvtodb(data, DateTime.UtcNow.AddDays(-1).ToString());
                        }
                        return View(csvTable);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
        #endregion

        #region Direct Read CSV
        /// <summary>
        /// Direct Read CSV From Ridhmik Server And Entry to DB
        /// </summary>
        /// <returns></returns>
        public JsonResult Get_csv()
        {
            UserService objUserService = new UserService();
            UserTransactionModel model = new UserTransactionModel();
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            string strUserId = "tlohdivad@gmail.com";
            string strPassword = "1DMI_OIY";
            string Date = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyyMMdd");
            string Path = "";
            string File_name = "TraderDock_audit_fills_commissions_matched_symbol_";//string File_name = "TraderDock_audit_fills_commissions_matched_symbol_20170801.csv";// File Name Ex.
            string ACReportFile = "TraderDock_audit_fills_commissions_match_reports_";//TraderDock_audit_fills_commissions_match_reports_20170815.csv
            try
            {
                #region Symbol Summary
                File_name = File_name + Date + ".csv";
                WebClient client = new WebClient();
                //Uri ur = new Uri("https://rithmic.com/TraderDock/RithmicTest/TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");//Uri ur = new Uri("https://rithmic.com/TraderDock/RithmicTest/TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");
                string remoteDirectory;
                if (IsRidhmicTestEnv)
                {
                    remoteDirectory = "http://rithmic.com/TraderDock/RithmicTest/";
                }
                else
                {
                    remoteDirectory = "http://rithmic.com/TraderDock/RithmicPaperTrading/";
                }
                //Uri ur = new Uri("https://rithmic.com/TraderDock/RithmicTest/" + File_name);// Daynamic Date To Get File
                Uri ur = new Uri(remoteDirectory + File_name);// Daynamic Date To Get File
                client.Credentials = new NetworkCredential(strUserId, strPassword);
                Path = Server.MapPath("~") + "\\Files\\Trade_CSV\\" + File_name;
                Stream stream = client.OpenRead(ur);                //client.DownloadFileAsync(ur, @"C:\Users\Aakash.ITCPU054\Downloads\Documents\TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");
                //Stream stream = Path;
                DataTable csvTable = new DataTable();
                using (CsvReader csvReader = new CsvReader(new StreamReader(stream), true))
                {
                    csvTable.Load(csvReader);
                }
                string[] data;
                foreach (DataRow row in csvTable.Rows)
                {
                    //csvTable.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                    data = row.ItemArray.Select(x => x.ToString()).ToArray();
                    model = objUserService.csvtodb(data, DateTime.UtcNow.Date.AddDays(-1).ToString());
                }
                #endregion

                #region Account Summry
                ACReportFile = ACReportFile + Date + ".csv";
                WebClient ACclient = new WebClient();
                //Uri ur = new Uri("https://rithmic.com/TraderDock/RithmicTest/TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");//Uri ur = new Uri("https://rithmic.com/TraderDock/RithmicTest/TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");
                Uri ACur = new Uri("https://rithmic.com/TraderDock/RithmicTest/" + ACReportFile);// Daynamic Date To Get File
                ACclient.Credentials = new NetworkCredential(strUserId, strPassword);
                Path = Server.MapPath("~") + "\\Files\\Trade_CSV\\" + ACReportFile;
                Stream streamAC = ACclient.OpenRead(ACur);                //client.DownloadFileAsync(ur, @"C:\Users\Aakash.ITCPU054\Downloads\Documents\TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");
                //Stream stream = Path;
                DataTable csvACTable = new DataTable();
                using (CsvReader csvACReader = new CsvReader(new StreamReader(streamAC), true))
                {
                    csvACTable.Load(csvACReader);
                }
                string[] dataAC;
                foreach (DataRow row in csvACTable.Rows)
                {
                    //csvTable.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                    dataAC = row.ItemArray.Select(x => x.ToString()).ToArray();
                    model = objUserService.csvtodbACReport(dataAC, DateTime.UtcNow.Date.AddDays(-1).ToString());
                }
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { success = "true" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Read CSV of those who registered with errors && Listing
        /// <summary>
        /// Read registered with errors
        /// </summary>
        /// <returns></returns>
        public ActionResult ErrorRegisteredUsers()
        {
            //Credentials
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            var host = "ritpz11300.11.rithmic.com";
            var port = 22;
            var username = "u3CiKQPhV";
            var password = "xBWKWRLW";

            try
            {

                SftpClient sftpClient = new SftpClient(host, port, username, password);
                sftpClient.Connect();
                List<SftpFile> fileList = sftpClient.ListDirectory(@"test/errors").ToList();
                sftpClient.Disconnect();
                List<string> FileName = fileList.Select(x => x.Name).Where(y => y.Contains("add")).ToList();
                List<string> lstUserIds = FileName.Select(x => x.Split('_').ElementAt(2)).ToList();

                Session["UsersIDs"] = lstUserIds;

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Errors the registered user read.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="SearchFirstName">First name of the search.</param>
        /// <param name="statusFilter">The status filter.</param>
        /// <returns></returns>
        public JsonResult ErrorRegisteredUser_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {

            List<string> userIDs = new List<string>();
            userIDs = (List<string>)Session["UsersIDs"];

            objUserService = new UserService();
            var users = objUserService.ErrorRegisteredUserList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter, userIDs);

            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Add/Edit Error User
        /// <summary>
        /// Add Edit Error User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 30/10/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditErrorUser(Int64? id)
        {
            objUserService = new UserService();
            objCommonService = new CommonService();
            UserMasterModel objUserModel = new UserMasterModel();
            if (id > 0)
                objUserModel = objUserService.GetUser(id);
            else
            {
                objUserModel.gender = true;
            }
            objUserModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            if (string.IsNullOrEmpty(objUserModel.profile_image_url))
            {
                objUserModel.profile_image_url = string.Empty;
                objUserModel.profile_image_url = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                objUserModel.FileName = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
            }
            return View(objUserModel);
        }


        /// <summary>
        /// Add Edit Error User
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="objFile"></param>
        /// <returns></returns>
        /// Date : 30/10/2017
        /// Dev By: Bharat Katua
        [HttpPost]
        public ActionResult AddEditErrorUser(UserMasterModel objModel, HttpPostedFileBase objFile)
        {
            if (objModel.user_id > 0)
            {
                ModelState.Remove("Password");
                ModelState.Remove("confirm_password");
            }
            ModelState.Remove("gender");
            ModelState.Remove("birth_date");
            if (ModelState.IsValid)
            {
                //Transaction will be maintain
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.user_id > 0)
                    {
                        objModel.user_id = objModel.user_id;
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        if (!string.IsNullOrEmpty(objModel.birth_date))
                        {
                            string[] formats = { "dd-MM-yyyy" };
                            objModel.dob = DateTime.ParseExact(objModel.birth_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                        }
                        bool bFlag = objUserService.AddEditUser(objModel);
                        if (bFlag)
                        {
                            transaction.Commit();
                            //Uploading data to FTP file

                            var filePath = Server.MapPath("~") + "\\Files\\add_user_" + objModel.user_id + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";
                            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
                            UserService objService = new UserService();

                            StringBuilder sb = new StringBuilder();
                            objModel = objService.GetDataForCSV(objModel.user_id);

                            char[] CharSeparator = new char[1];
                            CharSeparator[0] = ',';
                            if (!String.IsNullOrEmpty(objModel.address))
                            {
                                string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                                objModel.address = strStreetAddress;
                            }
                            //  string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                            string StrContactNumber = objModel.mobile_number;
                            if (objModel.mobile_number == "NA")
                                StrContactNumber = "44 7700 900708";

                            if (objModel.country_name.Trim().ToLower() != "usa" && objModel.country_name.Trim().ToLower() != "canada")
                            {
                                objModel.state_name = "";
                            }

                            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", objModel.user_email.Trim(), objModel.first_name.Trim(), objModel.last_name.Trim(), objModel.address.Trim(), objModel.city_name.Trim(), objModel.country_name.Trim(), objModel.state_name.Trim(), objModel.postal_code.Trim(), StrContactNumber.Trim(), objModel.password.Trim(), objModel.subscription_id.ToString()));
                            System.IO.File.AppendAllText(filePath, sb.ToString());
                            //for copy file one path to another
                            string fileName = Path.GetFileName(filePath);
                            string sourcePath = Server.MapPath("~") + "\\Files";
                            // Use Path class to manipulate file and directory paths.
                            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                            //upload file on sftp
                            var host = "ritpz11300.11.rithmic.com";
                            var port = 22;
                            var username = "u3CiKQPhV";
                            var password = "xBWKWRLW";

                            string remoteRootDirectory;
                            if (IsRidhmicTestEnv)
                            {
                                remoteRootDirectory = @"test/new_user/";
                            }
                            else
                            {
                                remoteRootDirectory = @"production/new_user/";
                            }

                            try
                            {
                                using (var client = new SftpClient(host, port, username, password))
                                {
                                    client.Connect();
                                    client.ChangeDirectory(remoteRootDirectory);   // for change directory on sftp

                                    if (client.IsConnected)
                                    {
                                        using (var fileStream = new FileStream(sourceFile, FileMode.Open))
                                        {
                                            client.BufferSize = 4 * 1024; // bypass Payload error large files
                                            client.UploadFile(fileStream, Path.GetFileName(sourceFile));
                                        }
                                    }
                                }
                                TempData["Msg"] = "File uploaded successfully";
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }

                        }
                        else
                        {
                            transaction.Rollback();
                        }

                        return RedirectToAction("ErrorRegisteredUsers");
                    }
                    else
                    {
                        //for Add User 
                        user_master objUserMaster = dbConnection.user_master.Where(x => x.is_active && x.is_deleted == false && (x.user_email.ToLower().Equals(objModel.user_email.ToLower()))).FirstOrDefault();
                        if (objUserMaster == null)
                        {
                            objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                            objModel.created_on = System.DateTime.UtcNow;
                            if (!string.IsNullOrEmpty(objModel.birth_date))
                            {
                                string[] formats = { "dd-mm-yyyy" };
                                objModel.dob = DateTime.ParseExact(objModel.birth_date, formats, new CultureInfo("en-US"), DateTimeStyles.None);
                            }
                            bool bFlag = objUserService.AddEditUser(objModel);
                            if (bFlag)
                                transaction.Commit();
                            else
                            {
                                transaction.Rollback();
                            }
                            return RedirectToAction("ErrorRegisteredUsers");
                        }
                        else
                        {
                            objModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                            TempData["Msg"] = "Record already exist successfully.";
                            return View(objModel);
                        }
                    }
                }
            }
            else
            {
                objModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                return View(objModel);
            }
        }
        #endregion

        #region Upload Users Data into CSV
        /// <summary>
        /// Users Data into CSV
        /// </summary>
        /// dev by : Bharat Katua
        /// date : 30/10/2017
        /// <param name="userID"></param>
        public ActionResult UploadCSVData(Int64 ID)
        {
            var filePath = Server.MapPath("~") + "\\Files\\add_user_" + ID + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            bool IsLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"].ToString());
            UserService objService = new UserService();
            UserMasterModel objModel = new UserMasterModel();

            StringBuilder sb = new StringBuilder();
            objModel = objService.GetDataForCSV(ID);

            char[] CharSeparator = new char[1];
            CharSeparator[0] = ',';
            if (!String.IsNullOrEmpty(objModel.address))
            {
                string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                objModel.address = strStreetAddress;
            }
            //  string strStreetAddress = objModel.address.Split(CharSeparator)[0];
            string StrContactNumber = objModel.mobile_number;
            if (objModel.mobile_number == "NA")
                StrContactNumber = "44 7700 900708";

            if (objModel.country_name.Trim().ToLower() != "usa" && objModel.country_name.Trim().ToLower() != "canada")
            {
                objModel.state_name = "";
            }

            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", objModel.user_email.Trim(), objModel.first_name.Trim(), objModel.last_name.Trim(), objModel.address.Trim(), objModel.city_name.Trim(), objModel.country_name.Trim(), objModel.state_name.Trim(), objModel.postal_code.Trim(), StrContactNumber.Trim(), objModel.password.Trim(), objModel.subscription_id.ToString()));
            System.IO.File.AppendAllText(filePath, sb.ToString());
            //for copy file one path to another
            string fileName = Path.GetFileName(filePath);
            string sourcePath = Server.MapPath("~") + "\\Files";
            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            #region upload file on sftp
            var host = "ritpz11300.11.rithmic.com";
            var port = 22;
            var username = "u3CiKQPhV";
            var password = "xBWKWRLW";

            string remoteRootDirectory;
            if (IsRidhmicTestEnv)
            {
                remoteRootDirectory = @"test/new_user/";
            }
            else
            {
                remoteRootDirectory = @"production/new_user/";
            }

            try
            {
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    client.ChangeDirectory(remoteRootDirectory);   // for change directory on sftp
                    if (IsLive)
                    {
                        if (client.IsConnected)
                        {
                            using (var fileStream = new FileStream(sourceFile, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(sourceFile));
                            }
                            //client.RenameFile(oldpath, newpath); // for move file on sftp
                        }
                    }
                }
                return Json(new { success = true, MsgUpload = "File uploaded successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;

            }
            #endregion
        }
        #endregion

        #region User Transaction list
        /// <summary>
        /// User Transaction List
        /// </summary>
        /// <returns></returns>
        /// Date : 17/8/2017
        /// Dev By: Bharat Katua
        [Authorization]
        public JsonResult UserTransaction(string Sequence, string Exchange = "", Int64 SubscriptionID = 0, string user_id = "")
        {
            long LogedUserID = Convert.ToInt64(Session["UserID"].ToString());
            WEBDashboardModel model = new WEBDashboardModel();
            DashboardService ObjDashboardService = new DashboardService();
            long NextSubId = 0;
            model.UserTransactionList = ObjDashboardService.GetUserTransactionList(out NextSubId, Sequence, LogedUserID, Exchange, SubscriptionID);
            return Json(new { success = true, List = model.UserTransactionList }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export to Excel        
        /// <summary>
        /// Exports to excel.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public ActionResult ExportToExcel()
        {
            CommonController objCommonController = new CommonController();
            objUserService = new UserService();
            IList<UserMasterModel> Users = new List<UserMasterModel>();
            var users = objUserService.UserList("", 0, int.MaxValue, "");
            if (users.Count > 0)
            {
                Users = users.ToList();
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("first_name");
            lstHeaderRow.Add("last_name");
            lstHeaderRow.Add("user_email");
            lstHeaderRow.Add("mobile_number");
            lstHeaderRow.Add("address");
            lstHeaderRow.Add("country");
            lstHeaderRow.Add("join_date_original");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("last_payment_date");
            lstHeaderRow.Add("next_payment_date");
            lstHeaderRow.Add("current_trading_balance");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Email");
            lstHeaderRowName.Add("Mobile Number");
            lstHeaderRowName.Add("Address");
            lstHeaderRowName.Add("Country");
            lstHeaderRowName.Add("Created Date");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Last Payment");
            lstHeaderRowName.Add("Next Payment");
            lstHeaderRowName.Add("Current Trading Balance");
            ExportService<UserMasterModel> objExportService = new ExportService<UserMasterModel>();
            DataTable dt = objExportService.GenerateDataTable(Users, lstHeaderRow);
            bool bStatus = objCommonController.GenerateExcel(Response, dt, lstHeaderRow, lstHeaderRowName, "User List");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Export to PDF
        /// <summary>
        /// Export to Excel
        /// </summary>
        /// <returns></returns>
        /// Date : 24/01/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ExportToPDF()
        {
            CommonController objCommonController = new CommonController();
            objUserService = new UserService();
            IList<UserMasterModel> Users = objUserService.UserList();
            if (Users.Count == 0)
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("first_name");
            lstHeaderRow.Add("last_name");
            lstHeaderRow.Add("user_email");
            lstHeaderRow.Add("mobile_number");
            lstHeaderRow.Add("address");
            lstHeaderRow.Add("country");
            lstHeaderRow.Add("join_date_original");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("last_payment_date");
            lstHeaderRow.Add("next_payment_date");
            lstHeaderRow.Add("current_trading_balance");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Email");
            lstHeaderRowName.Add("Mobile Number");
            lstHeaderRowName.Add("Address");
            lstHeaderRowName.Add("Country");
            lstHeaderRowName.Add("Created Date");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Last Payment");
            lstHeaderRowName.Add("Next Payment");
            lstHeaderRowName.Add("Current Trading Balance");
            ExportService<UserMasterModel> objExportService = new ExportService<UserMasterModel>();
            DataTable dt = objExportService.GenerateDataTable(Users, lstHeaderRow);
            bool bStatus = objCommonController.GeneratePDF(Response, dt, lstHeaderRow, lstHeaderRowName, "User List");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Get Platform         
        /// <summary>
        /// Gets the platform.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public SelectList GetPlatform()
        {
            var directions = from EnumTradingPlatform d in Enum.GetValues(typeof(EnumTradingPlatform))
                             select new { Id = (int)d, Value = d.ToString() };
            return new SelectList(directions, "Id", "Value");
        }
        #endregion

        #region Add Account No
        /// <summary>
        /// Adds the account no.
        /// </summary>
        /// <param name="sAccountAllias">The s account allias.</param>
        /// <param name="sAccountNo">The s account no.</param>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="sUserId">The s user identifier.</param>
        /// <param name="platform_id">The platform identifier.</param>
        /// <returns></returns>
        public JsonResult AddAccountNo(string sAccountAllias = "", string sAccountNo = "", long user_id = 0, string sUserId = "", string platform_id = "")
        {
            UserService objUserService = new UserService();
            if (!string.IsNullOrEmpty(platform_id) && user_id > 0)
            {
                List<UserPlatform> lstUserPlatform = objUserService.GetUserAccountListDetails(user_id);
                if (lstUserPlatform.Count > 0)
                {
                    if (lstUserPlatform.Where(x => x.is_active == true).Count() > 0)
                    {
                        bool bStatus = objUserService.UpdateAccountUserPlatform(user_id,
                                       lstUserPlatform.Where(x => x.is_active == true).FirstOrDefault().account_id.Value,
                                        //Convert.ToInt64(lstUserPlatform.Where(x => x.is_active == 1).FirstOrDefault().account_user_id)
                                        sAccountAllias,
                                       Convert.ToInt32(platform_id));
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                    else
                        return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Delete Account No        
        /// <summary>
        /// Deletes the account no.
        /// </summary>
        /// <param name="account_allias">The account allias.</param>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="platform_id">The platform identifier.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public JsonResult DeleteAccountNo(string account_allias, long user_id = 0, string platform_id = "")
        {
            UserService objUserService = new UserService();
            if (!string.IsNullOrEmpty(platform_id) && user_id > 0 && !string.IsNullOrEmpty(account_allias))
            {
                string[] lstAccountAllias = account_allias.Split(',');
                bool bStatus = objUserService.DeleteAccountUserPlatform(user_id
                               , lstAccountAllias, Convert.ToInt32(platform_id));
                if (bStatus)
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Transaction User Report
        /// <summary>
        /// Get User Report
        /// </summary>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public ActionResult Report()
        {
            List<UserMasterModel> List = new List<UserMasterModel>();
            return View(List);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public JsonResult Report_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "", string UserStatusFilter = "")
        {
            var objReportService = new ReportService();
            List<ReportTransactionModel> objData = objReportService.GetTTUserTransactionReportList(0, "", 0, SearchFirstName, statusFilter, UserStatusFilter);
            var gridModel = new DataSourceResult
            {
                Data = objData,
                Total = objData.Count()
            };

            return Json(gridModel);
        }
        #endregion

        #region Export to Excel        
        /// <summary>
        /// Reports the transaction export to excel.
        /// </summary>
        /// <returns></returns>
        /// Date : 24/11/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ReportTransactionExportToExcel()
        {
            CommonController objCommonController = new CommonController();
            var objReportService = new ReportService();
            List<ReportTransactionModel> objData = objReportService.GetTTUserTransactionReportList(0, "", 0, "");
            IList<ReportTransactionModel> lstReportModel = new List<ReportTransactionModel>();
            if (objData.Count > 0)
            {
                lstReportModel = objData.ToList();
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("first_name");
            lstHeaderRow.Add("last_name");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("strFreeTrial");
            lstHeaderRow.Add("isActive");
            lstHeaderRow.Add("TransactionDateString");
            lstHeaderRow.Add("TotalContracts");
            lstHeaderRow.Add("TotalTrades");
            lstHeaderRow.Add("GrossProfitLoss");
            lstHeaderRow.Add("TotalCommisions");
            lstHeaderRow.Add("NetProfitLoss");
            lstHeaderRow.Add("CurrentBalance");
            lstHeaderRow.Add("HighProfitLoss");
            lstHeaderRow.Add("LowProfitLoss");
            lstHeaderRow.Add("AvgWinningTrade");
            lstHeaderRow.Add("AvgLosingTrade");
            lstHeaderRow.Add("WinningTradePer");
            lstHeaderRow.Add("MaxConsLoss");
            lstHeaderRow.Add("AvgWinningDayOld");
            lstHeaderRow.Add("AvgLosingDayOld");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Mode");
            lstHeaderRowName.Add("Is Active");
            lstHeaderRowName.Add("Date");
            lstHeaderRowName.Add("FILLS");
            lstHeaderRowName.Add("Trades");
            lstHeaderRowName.Add("GROSS P&L");
            lstHeaderRowName.Add("Total Commisions");
            lstHeaderRowName.Add("Net Profit Loss");
            lstHeaderRowName.Add("Current Balance");
            lstHeaderRowName.Add("HighProfitLoss");
            lstHeaderRowName.Add("LowProfitLoss");
            lstHeaderRowName.Add("AvgWinningTrade");
            lstHeaderRowName.Add("AvgLosingTrade");
            lstHeaderRowName.Add("WinningTradePer");
            lstHeaderRowName.Add("MaxConsLoss");
            lstHeaderRowName.Add("AvgWinningDayOld");
            lstHeaderRowName.Add("AvgLosingDayOld");
            ExportService<ReportTransactionModel> objExportService = new ExportService<ReportTransactionModel>();
            DataTable dt = objExportService.GenerateDataTable(lstReportModel, lstHeaderRow);
            bool bStatus = objCommonController.GenerateExcel(Response, dt, lstHeaderRow, lstHeaderRowName, "User Transaction Report");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Export to PDF
        /// <summary>
        /// Export to Excel
        /// </summary>
        /// <returns></returns>
        /// Date : 24/11/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ReportTransactionExportToPDF()
        {
            CommonController objCommonController = new CommonController();
            var objReportService = new ReportService();
            List<ReportTransactionModel> objData = objReportService.GetTTUserTransactionReportList(0, "", 0, "");
            IList<ReportTransactionModel> lstReportModel = new List<ReportTransactionModel>();
            if (objData.Count > 0)
            {
                lstReportModel = objData.ToList();
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("first_name");
            lstHeaderRow.Add("last_name");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("strFreeTrial");
            lstHeaderRow.Add("isActive");
            lstHeaderRow.Add("TransactionDateString");
            lstHeaderRow.Add("TotalContracts");
            lstHeaderRow.Add("TotalTrades");
            lstHeaderRow.Add("GrossProfitLoss");
            lstHeaderRow.Add("TotalCommisions");
            lstHeaderRow.Add("NetProfitLoss");
            lstHeaderRow.Add("CurrentBalance");
            lstHeaderRow.Add("HighProfitLoss");
            lstHeaderRow.Add("LowProfitLoss");
            lstHeaderRow.Add("AvgWinningTrade");
            lstHeaderRow.Add("AvgLosingTrade");
            lstHeaderRow.Add("WinningTradePer");
            lstHeaderRow.Add("MaxConsLoss");
            lstHeaderRow.Add("AvgWinningDayOld");
            lstHeaderRow.Add("AvgLosingDayOld");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("Mode");
            lstHeaderRowName.Add("Is Active");
            lstHeaderRowName.Add("Date");
            lstHeaderRowName.Add("FILLS");
            lstHeaderRowName.Add("Trades");
            lstHeaderRowName.Add("GROSS P&L");
            lstHeaderRowName.Add("Total Commisions");
            lstHeaderRowName.Add("Net Profit Loss");
            lstHeaderRowName.Add("Current Balance");
            lstHeaderRowName.Add("HighProfitLoss");
            lstHeaderRowName.Add("LowProfitLoss");
            lstHeaderRowName.Add("AvgWinningTrade");
            lstHeaderRowName.Add("AvgLosingTrade");
            lstHeaderRowName.Add("WinningTradePer");
            lstHeaderRowName.Add("MaxConsLoss");
            lstHeaderRowName.Add("AvgWinningDayOld");
            lstHeaderRowName.Add("AvgLosingDayOld");
            ExportService<ReportTransactionModel> objExportService = new ExportService<ReportTransactionModel>();
            DataTable dt = objExportService.GenerateDataTable(lstReportModel, lstHeaderRow);
            bool bStatus = objCommonController.GeneratePDF(Response, dt, lstHeaderRow, lstHeaderRowName, "User Transaction Report");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Get Overall User Report
        /// <summary>
        /// Get User Report
        /// </summary>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public ActionResult OverallReport()
        {
            List<UserMasterModel> List = new List<UserMasterModel>();
            return View(List);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public JsonResult OverallReport_Read(DataSourceRequest model, string SearchFirstName = "", string StatusFilter = "", string UserStatusFilter = "")
        {
            var objReportService = new ReportService();
            if (!string.IsNullOrEmpty(SearchFirstName))
            {
                SearchFirstName = SearchFirstName.Trim().ToLower();
            }
            List<OverallUserReportModel> objData = objReportService.GetTTUserOverallReportList(0, "", 0, "", SearchFirstName, StatusFilter, UserStatusFilter);
            var gridModel = new DataSourceResult
            {
                Data = objData,
                Total = objData.Count()
            };

            return Json(gridModel);
        }
        #endregion

        #region Export to Excel
        /// <summary>
        /// Export to Excel
        /// </summary>
        /// <returns></returns>
        /// Date : 24/11/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ReportOverallExportToExcel()
        {
            CommonController objCommonController = new CommonController();
            var objReportService = new ReportService();
            List<OverallUserReportModel> objData = objReportService.GetTTUserOverallReportList(0, "", 0, "", "");
            IList<OverallUserReportModel> lstReportModel = new List<OverallUserReportModel>();
            if (objData.Count > 0)
            {
                lstReportModel = objData.ToList();
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("FirstName");
            lstHeaderRow.Add("LastName");
            lstHeaderRow.Add("Country");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("UserEmail");
            lstHeaderRow.Add("StartDate");
            lstHeaderRow.Add("EndDate");
            lstHeaderRow.Add("LastUploadDateStr");
            lstHeaderRow.Add("TotalContracts");
            lstHeaderRow.Add("TotalTrades");
            lstHeaderRow.Add("GrossProfitLossOld");
            lstHeaderRow.Add("TradeCompletedNoOfDays");
            lstHeaderRow.Add("CurrentBalanceOld");
            lstHeaderRow.Add("MaxDrawDownOld");
            lstHeaderRow.Add("DailyLossOld");
            lstHeaderRow.Add("BestDayOld");
            lstHeaderRow.Add("AvgWinningDayOld");
            lstHeaderRow.Add("DiscountedBalOld");
            lstHeaderRow.Add("HighProfitLossOld");
            lstHeaderRow.Add("LowProfitLossOld");
            lstHeaderRow.Add("AvgLosingDayOld");
            lstHeaderRow.Add("WorstDayOld");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Country");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("User Email");
            lstHeaderRowName.Add("Start Date");
            lstHeaderRowName.Add("End Date");
            lstHeaderRowName.Add("Last Upload Date");
            lstHeaderRowName.Add("FILLS");
            lstHeaderRowName.Add("Trades");
            lstHeaderRowName.Add("Gross P&L");
            lstHeaderRowName.Add("Days");
            lstHeaderRowName.Add("Current Balance");
            lstHeaderRowName.Add("Max DrawDown");
            lstHeaderRowName.Add("Daily Loss");
            lstHeaderRowName.Add("Best Day");
            lstHeaderRowName.Add("Avg WinningDay");
            lstHeaderRowName.Add("Discounted Bal");
            lstHeaderRowName.Add("High Profit Loss");
            lstHeaderRowName.Add("Low Profit Loss");
            lstHeaderRowName.Add("Avg Losing Day");
            lstHeaderRowName.Add("Worst Day");
            ExportService<OverallUserReportModel> objExportService = new ExportService<OverallUserReportModel>();
            DataTable dt = objExportService.GenerateDataTable(lstReportModel, lstHeaderRow);
            bool bStatus = objCommonController.GenerateExcel(Response, dt, lstHeaderRow, lstHeaderRowName, " Trader Performance Report");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Export to PDF
        /// <summary>
        /// Export to Excel
        /// </summary>
        /// <returns></returns>
        /// Date : 24/11/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ReportOverallExportToPDF()
        {
            CommonController objCommonController = new CommonController();
            var objReportService = new ReportService();
            List<OverallUserReportModel> objData = objReportService.GetTTUserOverallReportList(0, "", 0, "", "");
            IList<OverallUserReportModel> lstReportModel = new List<OverallUserReportModel>();
            if (objData.Count > 0)
            {
                lstReportModel = objData.ToList();
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "user");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("FirstName");
            lstHeaderRow.Add("LastName");
            lstHeaderRow.Add("Country");
            lstHeaderRow.Add("account_number");
            lstHeaderRow.Add("UserEmail");
            lstHeaderRow.Add("StartDate");
            lstHeaderRow.Add("EndDate");
            lstHeaderRow.Add("LastUploadDateStr");
            lstHeaderRow.Add("TotalContracts");
            lstHeaderRow.Add("TotalTrades");
            lstHeaderRow.Add("GrossProfitLossOld");
            lstHeaderRow.Add("TradeCompletedNoOfDays");
            lstHeaderRow.Add("CurrentBalanceOld");
            lstHeaderRow.Add("MaxDrawDownOld");
            lstHeaderRow.Add("DailyLossOld");
            lstHeaderRow.Add("BestDayOld");
            lstHeaderRow.Add("AvgWinningDayOld");
            lstHeaderRow.Add("DiscountedBalOld");
            lstHeaderRow.Add("HighProfitLossOld");
            lstHeaderRow.Add("LowProfitLossOld");
            lstHeaderRow.Add("AvgLosingDayOld");
            lstHeaderRow.Add("WorstDayOld");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("First Name");
            lstHeaderRowName.Add("Last Name");
            lstHeaderRowName.Add("Country");
            lstHeaderRowName.Add("Account Number");
            lstHeaderRowName.Add("User Email");
            lstHeaderRowName.Add("Start Date");
            lstHeaderRowName.Add("End Date");
            lstHeaderRowName.Add("Last Upload Date");
            lstHeaderRowName.Add("FILLS");
            lstHeaderRowName.Add("Trades");
            lstHeaderRowName.Add("Gross P&L");
            lstHeaderRowName.Add("Days");
            lstHeaderRowName.Add("Current Balance");
            lstHeaderRowName.Add("Max DrawDown");
            lstHeaderRowName.Add("Daily Loss");
            lstHeaderRowName.Add("Best Day");
            lstHeaderRowName.Add("Avg WinningDay");
            lstHeaderRowName.Add("Discounted Bal");
            lstHeaderRowName.Add("High Profit Loss");
            lstHeaderRowName.Add("Low Profit Loss");
            lstHeaderRowName.Add("Avg Losing Day");
            lstHeaderRowName.Add("Worst Day");
            ExportService<OverallUserReportModel> objExportService = new ExportService<OverallUserReportModel>();
            DataTable dt = objExportService.GenerateDataTable(lstReportModel, lstHeaderRow);
            bool bStatus = objCommonController.GeneratePDF(Response, dt, lstHeaderRow, lstHeaderRowName, "Trader Performance Report");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region API Log Report        
        /// <summary>
        /// Apilogreports the specified s account name.
        /// </summary>
        /// <param name="sAccountName">Name of the s account.</param>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public ActionResult apilogreport(string sAccountName = "")
        {
            TTFileModel List = new TTFileModel();
            UserService objUserService = new UserService();
            TTFileModel objTTFileModel = new TTFileModel();
            List<UserPlatform> lstUserPlatform = new List<UserPlatform>();
            lstUserPlatform = objUserService.GetUserAccountNumberList();
            objTTFileModel.lstaccount = new SelectList(lstUserPlatform, "account_id", "account_alias");
            objTTFileModel.account = sAccountName;
            return View(objTTFileModel);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 12/10/2018
        /// Dev By: Hardik Savaliya
        public JsonResult APILogReport_Read(DataSourceRequest model, string sAccount = "", string sFromDate = "", string sToDate = "")
        {
            try
            {
                var objReportService = new ReportService();
                TTService objTTService = new TTService();
                if (!string.IsNullOrEmpty(sAccount))
                {
                    sAccount = sAccount.Trim().ToLower();
                }
                if (sAccount == "select all account")
                {
                    sAccount = "";
                }
                PagedList<ViewFill> result = objTTService.GetFillReportAPI(sAccount, model.Page - 1, model.PageSize, sFromDate, sToDate);
                var gridModel = new DataSourceResult
                {
                    Data = result,
                    Total = result.TotalCount
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Delete Selected record
        /// <summary>
        /// Delete Record for API
        /// </summary>
        /// <returns></returns>
        /// Date : 25/10/2018
        /// Dev By: Hardik 
        [HttpGet]
        public ActionResult DeleteCheckedAPIFill(string sID)
        {
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            TTFileModel objTTFileModel = new TTFileModel();
            try
            {
                if (IDs != null)
                {
                    TTService objTTService = new TTService();
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    bool bResult = objTTService.DeleteAPIFills(TagIds);
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("dashboard", "dashboard");
        }
        #endregion

        #region Processed Selected record        
        /// <summary>
        /// Processed Record for API
        /// </summary>
        /// <returns></returns>
        /// Date : 17/01/2019
        /// Dev By: Hardik 
        [HttpPost]
        public JsonResult ProcessedCheckedAPIFill(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                string IDs = id.Replace(@"chkSelectAll,", @"");
                try
                {
                    if (!string.IsNullOrEmpty(IDs))
                    {
                        TempData["CheckedFILLSAPI"] = IDs;
                        return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
            return Json(new { success = false, JsonRequestBehavior.AllowGet });
        }
        #endregion

        #region Processed Record for API        
        /// <summary>
        /// Processed Record for API
        /// </summary>
        /// <returns></returns>
        /// Date : 17/01/2019
        /// Dev By: Hardik 
        public ActionResult ProcessedSelectedRecordAPI()
        {
            return View();
        }

        /// <summary>
        /// Processeds the selected record API read.
        /// </summary>
        /// <returns></returns>
        /// Date : 17/01/2019
        /// Dev By: Hardik 
        /// <exception cref="ServiceLayerException"></exception>
        public ActionResult ProcessedSelectedRecordAPI_Read()
        {
            try
            {
                if (TempData["CheckedFILLSAPI"] != null)
                {
                    TTService objTTService = new TTService();
                    List<long> TagIds = TempData["CheckedFILLSAPI"].ToString().Split(',').Select(long.Parse).ToList();
                    if (TagIds.Count() > 0)
                    {
                        try
                        {
                            List<string> lstAccount = new List<string>();
                            List<tt_getfill_master> lstGetFillMaster = dbConnection.tt_getfill_master.Where(x => TagIds.Contains(x.getfill_id)).ToList();
                            List<TTViewInputModel> lstFileRowTemp = new List<TTViewInputModel>();
                            List<string> lstCntrct = new List<string>();
                            List<TTViewInputModel> lstFileFinalRow = new List<TTViewInputModel>();

                            foreach (var item in lstGetFillMaster)
                            {
                                TTViewInputModel objRecord = new TTViewInputModel();
                                objRecord.Date = Convert.ToDateTime(item.datetime_stamp);
                                if (item.transact_time != null)
                                {
                                    objRecord.tsTime = item.transact_time.Value;
                                }
                                if (item.market_id == 7)
                                    objRecord.Exchange = Convert.ToString("CME");// currently assuming on CME arrive in CME=7
                                else if (item.market_id == 1001)
                                    objRecord.Exchange = Convert.ToString("ALGO_INSTRUMENT");
                                objRecord.Contract = Convert.ToString(item.contract);
                                if (item.side == 1)
                                    objRecord.BS = Convert.ToString("B");
                                else if (item.side == 2)
                                    objRecord.BS = Convert.ToString("S");
                                objRecord.FillQty = Convert.ToInt32(item.cum_qty);
                                objRecord.Price = Convert.ToDecimal(item.price);
                                //objRecord.PF = Convert.ToString("P");
                                objRecord.Account = Convert.ToString(item.account);
                                objRecord.AccountID = item.account_id;
                                lstFileRowTemp.Add(objRecord);
                            }

                            //Get Commision and tick value from database
                            List<tt_product_master> lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == true).ToList();

                            //Remove other than CME market
                            lstFileRowTemp = lstFileRowTemp.Where(x => x.Exchange.Trim().ToUpper() == "CME").ToList();

                            //Get Distinct Row from table
                            List<long> lstAccountDist = lstFileRowTemp.Select(x => x.AccountID).Distinct().ToList();

                            //Get Account Name and bind if TT Does not provide it
                            List<user_platform> lstUserPlatform = dbConnection.user_platform.Where(x => x.is_active == true && x.is_delete == false).ToList();

                            //loop account
                            foreach (long lAccountID in lstAccountDist)
                            {
                                List<TTViewInputModel> lstDateWisefilter = new List<TTViewInputModel>();
                                lstDateWisefilter = lstFileRowTemp.Where(x => x.AccountID == lAccountID).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                List<string> lstStringApplicableAccountAllias = lstUserPlatform.Where(x => x.account_id == lAccountID).Select(x => x.account_alias).ToList();
                                if (lstStringApplicableAccountAllias.Count() > 0)
                                {
                                    lstDateWisefilter.ForEach(x => x.Account = lstStringApplicableAccountAllias.LastOrDefault());
                                }
                                foreach (var sDate in lstDate)
                                {
                                    int TradeNumber = 0;
                                    List<TTViewInputModel> lstAccountWisefilter = new List<TTViewInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date == sDate).ToList();
                                    List<string> lstContract = lstAccountWisefilter.Select(x => x.Contract).Distinct().ToList();
                                    foreach (string sContract in lstContract)
                                    {
                                        List<TTViewInputModel> lstContractWisefilter = new List<TTViewInputModel>();
                                        lstContractWisefilter = lstAccountWisefilter.Where(x => x.Contract == sContract).OrderBy(x => x.Time).ToList();
                                        Decimal dNetFillQty = 0;
                                        int TradeEntry = 1;
                                        foreach (var drFilterRowAccountwise in lstContractWisefilter)
                                        {
                                            drFilterRowAccountwise.Time_str = drFilterRowAccountwise.tsTime.ToString();
                                            drFilterRowAccountwise.Date_str = drFilterRowAccountwise.Date.ToString("yyyy-MM-dd");
                                            if (drFilterRowAccountwise.BS == "B")
                                            {
                                                drFilterRowAccountwise.BuySell = 1;
                                            }
                                            else if (drFilterRowAccountwise.BS == "S")
                                            {
                                                drFilterRowAccountwise.BuySell = -1;
                                            }
                                            else
                                            {
                                                throw new ServiceLayerException();
                                            }

                                            //Net Position
                                            if (dNetFillQty == 0)
                                            {
                                                dNetFillQty = Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell);
                                                TradeEntry = 1;
                                            }
                                            else
                                            {
                                                dNetFillQty = (Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell)) + dNetFillQty;
                                            }
                                            drFilterRowAccountwise.NetPosition = dNetFillQty;

                                            //Trade Entry, Exit
                                            if (TradeEntry == 1 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                            {
                                                drFilterRowAccountwise.TradeEntry = TradeEntry;
                                                drFilterRowAccountwise.TradeExit = 0;
                                                TradeEntry = 0;
                                            }
                                            else if (TradeEntry == 0 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                            {
                                                drFilterRowAccountwise.TradeEntry = 0;
                                                drFilterRowAccountwise.TradeExit = 0;
                                                TradeEntry = 0;
                                            }
                                            else
                                            {
                                                drFilterRowAccountwise.TradeEntry = 0;
                                                drFilterRowAccountwise.TradeExit = 1;
                                            }

                                            //Trader Buy Price, Sell Price
                                            if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == 1)
                                            {
                                                drFilterRowAccountwise.BuyPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                            }
                                            if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == -1)
                                            {
                                                drFilterRowAccountwise.SellPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                            }

                                            //Trade Number
                                            if (drFilterRowAccountwise.TradeEntry == 1)
                                            {
                                                drFilterRowAccountwise.TradeNumber = TradeNumber + 1;
                                                TradeNumber = TradeNumber + 1;
                                            }
                                            else
                                            {
                                                drFilterRowAccountwise.TradeNumber = TradeNumber;
                                            }
                                        }
                                        //average buy price and sell price
                                        if (lstContractWisefilter.Count > 0)
                                        {
                                            List<int> lstTradeNumber = lstContractWisefilter.Select(x => x.TradeNumber).Distinct().ToList();
                                            foreach (var item in lstTradeNumber)
                                            {
                                                decimal dAvgBuyPrice = 0;
                                                decimal dAvgSellPrice = 0;
                                                decimal dPriceTotal = 0;
                                                decimal dBuyfillTotal = 0;
                                                decimal dSellfillTotal = 0;
                                                decimal dTotalTradeQnty = 0;
                                                dTotalTradeQnty = lstContractWisefilter.Where(x => x.TradeNumber == item).Sum(x => x.FillQty);
                                                var objBuyItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == 1).ToList();
                                                foreach (var objBuy in objBuyItem)
                                                {
                                                    dPriceTotal = (objBuy.FillQty * objBuy.Price) + dPriceTotal;
                                                    dBuyfillTotal = dBuyfillTotal + objBuy.FillQty;
                                                }
                                                if (dPriceTotal != 0 && dBuyfillTotal != 0)
                                                {
                                                    dAvgBuyPrice = dPriceTotal / dBuyfillTotal;
                                                }
                                                dPriceTotal = 0;
                                                dSellfillTotal = 0;
                                                var objSellItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == -1).ToList();
                                                foreach (var objSell in objSellItem)
                                                {
                                                    dPriceTotal = (objSell.FillQty * objSell.Price) + dPriceTotal;
                                                    dSellfillTotal = dSellfillTotal + objSell.FillQty;
                                                }
                                                if (dPriceTotal != 0 && dSellfillTotal != 0)
                                                {
                                                    dAvgSellPrice = dPriceTotal / dSellfillTotal;
                                                }
                                                lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageBuyPrice = dAvgBuyPrice);
                                                lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageSellPrice = dAvgSellPrice);
                                                if (dBuyfillTotal == dSellfillTotal)
                                                {
                                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.realised_pnl_tick = ((dAvgSellPrice - dAvgBuyPrice) * dBuyfillTotal));
                                                }
                                                else
                                                {
                                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.unrealised_pnl_tick = 0);
                                                }
                                                lstContractWisefilter.Where(y => y.TradeNumber == item).ToList().ForEach(x => x.total_traded_qty_trade = dTotalTradeQnty);
                                            }
                                        }
                                        lstFileFinalRow.AddRange(lstContractWisefilter);
                                    }
                                }
                            }
                            //Realised P&L(Tick) in our example for TDTEST1 is 0.25 / 0.01 which = 25.All we do then is multiply by the TICK VALUE whic is alson on Google Sheets.For CL, it is $10.So the Realised P & L($ USD) = 25 * $10 which is $250
                            foreach (var item in lstFileFinalRow)
                            {
                                string sContractName = item.Contract.Substring(0, 3).Trim();
                                if (!string.IsNullOrEmpty(sContractName))
                                {
                                    tt_product_master objTTProductMaster = lstTTProductMaster.Where(x => x.symbol == sContractName).FirstOrDefault();
                                    if (objTTProductMaster != null)
                                    {
                                        item.realised_pnl_usd_without_commision = ((item.realised_pnl_tick / objTTProductMaster.increment) * objTTProductMaster.tick_value);
                                        item.commission = (item.total_traded_qty_trade * objTTProductMaster.commision);
                                        item.realised_pnl_usd_with_commision = item.realised_pnl_usd_without_commision - item.commission;
                                        item.w_trades = item.realised_pnl_usd_with_commision > 0 ? 1 : 0;
                                        item.l_trades = item.realised_pnl_usd_with_commision < 0 ? 1 : 0;
                                        item.s_trades = item.realised_pnl_usd_with_commision == 0 ? 1 : 0;
                                    }
                                    else
                                    {
                                        return Json(new { success = false, msg = "Contract commision does not exist in database" + sContractName }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }
                            //Re-Calculate trade Number
                            foreach (var sAccountName in lstAccountDist)
                            {
                                List<TTViewInputModel> lstDateWisefilter = new List<TTViewInputModel>();
                                lstDateWisefilter = lstFileRowTemp.Where(x => x.AccountID == sAccountName).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                foreach (var sDate in lstDate)
                                {
                                    int TradeNumber = 0;
                                    List<TTViewInputModel> lstAccountWisefilter = new List<TTViewInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                                    List<KeyValuePair<string, int>> lstTempOpenContract = new List<KeyValuePair<string, int>>();
                                    foreach (var item in lstAccountWisefilter)
                                    {
                                        if (item.TradeEntry == 1)
                                        {
                                            item.TradeNumber = TradeNumber + 1;
                                            TradeNumber = TradeNumber + 1;
                                            lstTempOpenContract.Add(new KeyValuePair<string, int>(item.Contract, item.TradeNumber));
                                        }
                                        else if (item.TradeExit == 1)
                                        {
                                            int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                            lstTempOpenContract.Remove(new KeyValuePair<string, int>(item.Contract, iTradeNumber));
                                            item.TradeNumber = iTradeNumber;
                                        }
                                        else
                                        {
                                            int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                            item.TradeNumber = iTradeNumber;
                                        }
                                    }
                                }
                            }
                            //Calculate High,low and cumalative p&l
                            foreach (var sAccountName in lstAccountDist)
                            {
                                List<TTViewInputModel> lstDateWisefilter = new List<TTViewInputModel>();
                                lstDateWisefilter = lstFileRowTemp.Where(x => x.AccountID == sAccountName).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                foreach (var sDate in lstDate)
                                {
                                    List<TTViewInputModel> lstAccountWisefilter = new List<TTViewInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                                    List<int> lstTradeNumber = lstAccountWisefilter.Select(x => x.TradeNumber).Distinct().OrderBy(x => x).ToList();
                                    decimal dHigh = 0;
                                    decimal dLow = 0;
                                    decimal dCumalitive = 0;
                                    decimal dCumalitiveWithoutCommission = 0;
                                    foreach (int iTradeNumber in lstTradeNumber)
                                    {
                                        List<TTViewInputModel> lstContractWisefilter = new List<TTViewInputModel>();
                                        lstContractWisefilter = lstAccountWisefilter.Where(x => x.TradeNumber == iTradeNumber).OrderBy(x => x.Time).ToList();
                                        dCumalitive = dCumalitive + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_with_commision;
                                        dCumalitiveWithoutCommission = dCumalitiveWithoutCommission + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_without_commision;
                                        foreach (var drFilterRowAccountwise in lstContractWisefilter)
                                        {
                                            if (dHigh < dCumalitive)
                                            {
                                                dHigh = dCumalitive;
                                            }
                                            if (dLow > dCumalitive)
                                            {
                                                dLow = dCumalitive;
                                            }
                                            drFilterRowAccountwise.high_pnl = dHigh;
                                            drFilterRowAccountwise.low_pnl = dLow;
                                        }
                                        lstContractWisefilter.ForEach(x => x.cumulative_pnl = dCumalitive);
                                        lstContractWisefilter.ForEach(x => x.cumalitve_pnl_without_commision = dCumalitiveWithoutCommission);
                                    }
                                }
                            }
                            if (lstFileFinalRow.Count != 0)
                            {
                                objTTService = new TTService();
                                var lstError = new List<KeyValuePair<string, string>>();
                                StringBuilder strUserList = new StringBuilder();
                                StringBuilder strAccountList = new StringBuilder();
                                if (lstError.Count() > 0)
                                {
                                    foreach (var item in lstError.Distinct())
                                    {
                                        if (item.Value == "ACCOUNT")
                                        {
                                            strAccountList.Append(item.Key + "," + strAccountList);
                                        }
                                        if (item.Value == "USER")
                                        {
                                            strUserList.Append(item.Key + "," + strUserList);
                                        }
                                    }
                                }
                                foreach (var item in lstFileFinalRow)
                                {
                                    item.AccountCombinedName = item.AccountID + "(" + item.Account + ")";
                                }
                                var jsonResult = Json(new
                                {
                                    success = true,
                                    data = lstFileFinalRow,
                                    strAccountList = strAccountList.ToString(),
                                    strUserList = strUserList.ToString()
                                }, JsonRequestBehavior.AllowGet);
                                jsonResult.MaxJsonLength = int.MaxValue;
                                return jsonResult;
                            }
                            else
                            {
                                return Json(new { success = true, data = "" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        catch (Exception ex)
                        {
                            return Json("Error occurred. Error details: " + ex.Message);
                        }
                    }
                    else
                    {
                        return RedirectToAction("dashboard", "dashboarad");
                    }
                }
                else
                {
                    return RedirectToAction("dashboard", "dashboard");
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }

        }
        #endregion
    }
}