﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class CountryController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CountryService objCountryService;
        #endregion

        #region Country Get
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<CountryModel> List = new List<CountryModel>();
            return View(List);
        }

        /// <summary>
        /// Country Master Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        public JsonResult CountryMaster_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objCountryService = new CountryService();
            var FAQCategoryList = objCountryService.CountryList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = FAQCategoryList,
                Total = FAQCategoryList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Active/In Active 
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            CountryModel objCountryModel = new CountryModel();
            List<Int64> lIds = new List<Int64>();
            objCountryService = new CountryService();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objCountryService.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Country Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Country");
        }

        /// <summary>
        /// In active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            CountryModel objCountryModel = new CountryModel();
            List<Int64> lIds = new List<Int64>();
            objCountryService = new CountryService();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objCountryService.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Country In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "Country");
        }

        /// <summary>
        /// Active In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            objCountryService = new CountryService();
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objCountryService.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = objCountryService.ActiveInactive(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Country Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Country In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Country");
        }
        #endregion
    }
}