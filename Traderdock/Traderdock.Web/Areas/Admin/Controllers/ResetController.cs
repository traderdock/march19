﻿using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Web.Mvc;
using Traderdock.ORM;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class ResetController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        ResetService objResetService;
        #endregion


        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 13-Nov-2017
        /// Dev By: Hardik Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            objResetService = new ResetService();
            List<UserMasterModel> List = new List<UserMasterModel>();
            var count = objResetService.ResetList();
            return View(count);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 8-Aug-2017
        /// Dev By: Hardik  Savaliya
        public JsonResult Reset_Read(DataSourceRequest model, string SearchName = "")

        {

            objResetService = new ResetService();
            var usersRevenue = objResetService.ResetList(SearchName.ToLower(), model.Page - 1, model.PageSize, "", 0);
            var gridModel = new DataSourceResult
            {
                Data = usersRevenue,
                Total = usersRevenue.TotalCount
            };
            return Json(gridModel);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 8-Aug-2017
        /// Dev By: Hardik  Savaliya
        public ActionResult ResetRequest(int id)
        {
            objResetService = new ResetService();
            bool bStatus = objResetService.ResetRequest(id);
            if (bStatus)
                return Json(new { success = true, Msgnew = "Record Resetted Successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { success = true, Msgnew = "Something went wrong." }, JsonRequestBehavior.AllowGet);
        }

    }
}