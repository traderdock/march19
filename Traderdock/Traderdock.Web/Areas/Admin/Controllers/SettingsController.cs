﻿using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class SettingsController : Controller
    {
        #region Global Variables
        SettingModel objSettingModel;
        SettingsService objSettingService;
        #endregion

        #region Get Setting
        /// <summary>
        /// Index Get
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            objSettingModel = new SettingModel();
            List<SettingModel> lstSettingModel = new List<SettingModel>();
            return View(lstSettingModel);
        }

        /// <summary>
        /// Settings_Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult Settings_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objSettingService = new SettingsService();
            var users = objSettingService.SettingsList(SearchFirstName, model.Page - 1, ((Convert.ToInt32(model.PageSize)>0)?model.PageSize:10), statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };
            return Json(gridModel);
        }
        #endregion

        #region Add Edit Setting.
        /// <summary>
        /// Edit Setting
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditSetting(Int64? id)
        {
            objSettingService = new SettingsService();
            SettingModel objSettingModel = new SettingModel();
            if (id > 0)
            {
                objSettingModel = objSettingService.GetSetting(id);
            }
            return View(objSettingModel);
        }

        /// <summary>
        /// Add Edit Settings
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="file1"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult AddEditSettings(SettingModel objSettingModel)
        {
            objSettingService = new SettingsService();
            if (ModelState.IsValid)
            {
                if (objSettingModel.setting_id > 0)
                {
                    objSettingModel.modified_on = System.DateTime.UtcNow;
                    objSettingModel.modified_by = Convert.ToInt32(Session["UserID"]);
                    bool bFlag = objSettingService.AddEditSettings(objSettingModel);
                    if (bFlag)
                    {
                        if (objSettingModel.setting_name == "PageSize")
                        {
                            Session["PageSize"] = objSettingModel.setting_value;
                        }
                    }
                    return RedirectToAction("Index");
                }
                else
                {
                    bool bFlag = objSettingService.AddEditSettings(objSettingModel);
                    return RedirectToAction("Index");
                }
            }
            return View(objSettingModel);
        }
        #endregion
    }
}