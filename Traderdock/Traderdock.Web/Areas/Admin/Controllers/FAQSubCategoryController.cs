﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class FAQSubCategoryController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        FAQSubCategoryService objFAQSubCategoryService;
        #endregion

        #region FAQ Sub Category Get
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<FAQSubCategoryModel> List = new List<FAQSubCategoryModel>();
            return View(List);
        }

        /// <summary>
        /// Get Faq Category for Page Load Kendo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult FAQSubCategory_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            try
            {
                objFAQSubCategoryService = new FAQSubCategoryService();
                var FAQCategoryList = objFAQSubCategoryService.FAQSubCategoryList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
                var gridModel = new DataSourceResult
                {
                    Data = FAQCategoryList,
                    Total = FAQCategoryList.TotalCount
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region FAQ Category Add/Edit
        /// <summary>
        /// Add Edit Faq Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditFAQSubCategory(Int64? id)
        {
            FAQCategoryService objFAQCategoryService = new FAQCategoryService();
            objFAQSubCategoryService = new FAQSubCategoryService();
            FAQSubCategoryModel objFAQSubCategoryModel = new FAQSubCategoryModel();
            if (id > 0)
            {
                objFAQSubCategoryModel = objFAQSubCategoryService.GetFAQSubCategory(id);
            }
            objFAQSubCategoryModel.FAQCategoryList = new SelectList(objFAQCategoryService.GetFaqCategories(), "faq_category_id", "category_title");
            return View(objFAQSubCategoryModel);
        }

        /// <summary>
        /// Add Edit FAQ Category
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="file1"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]

        public ActionResult AddEditFAQSubCategory(FAQSubCategoryModel objModel)
        {
            FAQCategoryService objFAQCategoryService = new FAQCategoryService();
            objFAQSubCategoryService = new FAQSubCategoryService();
            if (ModelState.IsValid)
            {
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.faq_sub_category_id > 0)
                    {
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQSubCategoryService.AddEditFAQSubCategory(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.created_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQSubCategoryService.AddEditFAQSubCategory(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();
                        return RedirectToAction("Index");
                    }
                }
            }
            else
            {
                objModel.FAQCategoryList = new SelectList(objFAQCategoryService.GetFaqCategories(), "faq_category_id", "category_title");
                return View(objModel);
            }
        }
        #endregion

        #region  FAQ Sub Catergory Delete
        /// <summary>
        /// Faq Category Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objFAQSubCategoryService = new FAQSubCategoryService();
            if (ID > 0)
            {
                bool bResult = objFAQSubCategoryService.DeleteFAQSubCategory(ID, Convert.ToInt64(Session["UserID"].ToString()));
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "FAQCategory");
        }
        #endregion

        #region FAQ Catergory Delete
        /// <summary>
        /// Faq Category Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult UpdateOrderSequence(int faq_category_id, int order_sequence)
        {
            //need to look
            //objFAQSubCategoryService = new FAQSubCategoryService();
            //if (faq_category_id > 0 && order_sequence > 0)
            //{
            //    bool bResult = objFAQSubCategoryService.updateOrderFAQ(faq_category_id, order_sequence);
            //    if (bResult)
            //        return Json(new { success = true, Msgnew = "Order Sequence updated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            //    else
            //        return Json(new { success = false, Msgnew = "Order Number already exist!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            //}
            return RedirectToAction("Index", "FAQCategory");
        }
        #endregion

        #region Active/In Active 
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            FAQSubCategoryModel objFAQSubCategoryModel = new FAQSubCategoryModel();
            List<Int64> lIds = new List<Int64>();
            objFAQSubCategoryService = new FAQSubCategoryService();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objFAQSubCategoryService.ActiveInactiveFAQSubCategory(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "FAQMaster");
        }

        /// <summary>
        /// In active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            List<Int64> lIds = new List<Int64>();
            objFAQSubCategoryService = new FAQSubCategoryService();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objFAQSubCategoryService.ActiveInactiveFAQSubCategory(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "FAQMaster");
        }

        /// <summary>
        /// Active In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            objFAQSubCategoryService = new FAQSubCategoryService();
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    UserService ObjUserService = new UserService();
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objFAQSubCategoryService.ActiveInactiveFAQSubCategory(lIds, UserID, true);
                    else
                        bResult = objFAQSubCategoryService.ActiveInactiveFAQSubCategory(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "FAQMaster");
        }
        #endregion
    }
}