﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Traderdock.ORM;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;
using System.IO;
using System.Data;
using LumenWorks.Framework.IO.Csv;
using Traderdock.Model.TTModel;
using Traderdock.Common.Exceptions;
using Newtonsoft.Json;
using System.Text;
using Traderdock.Common;
using Traderdock.Web.QuartzServcie;
using System.Web.Helpers;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class TTController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        TTService objTTService = new TTService();
        #endregion

        #region Get User
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ProductIndex()
        {
            List<TTProduct> List = new List<TTProduct>();
            return View(List);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public JsonResult Product_Read(DataSourceRequest model, string SearchName = "", string statusFilter = "")
        {
            objTTService = new TTService();
            var objProducts = objTTService.GetTradeAllowbleProduct(SearchName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = objProducts,
                Total = objProducts.TotalCount
            };
            return Json(gridModel);
        }
        #endregion

        #region Active/In Active Checked
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult ActiveCheckedProduct(int ID)
        {
            UserModel objUserModel = new UserModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);

            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objTTService.ActiveInActiveProduct(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "User");
        }

        /// <summary>
        /// In Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult InActiveCheckedProduct(int ID)
        {
            UserModel objUserModel = new UserModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objTTService.ActiveInActiveProduct(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "User");
        }
        #endregion

        #region TT User Fill Upload        
        /// <summary>
        /// Readcsvs this instance.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult readcsv()
        {
            return View();
        }
        #endregion

        #region Read Csv post        
        /// <summary>
        /// Readcsvs the specified upload.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult readcsv(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    if (upload.FileName.EndsWith(".csv"))
                    {
                        Stream stream = upload.InputStream;
                        DataTable csvTable = new DataTable();
                        using (CsvReader csvReader =
                            new CsvReader(new StreamReader(stream), true))
                        {
                            csvTable.Load(csvReader);
                        }
                        string[] data;
                        foreach (DataRow row in csvTable.Rows)
                        {
                            //csvTable.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                            data = row.ItemArray.Select(x => x.ToString()).ToArray();
                        }
                        return View(csvTable);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
        #endregion

        #region Upload Processing
        /// <summary>
        /// Uploads the processing.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        public ActionResult UploadProcessing()
        {
            if (Request.Files.Count > 0)
            {
                try
                {
                    List<TTFileInputModel> lstFileInputModelFromDB = new List<TTFileInputModel>();
                    HttpFileCollectionBase files = Request.Files;
#pragma warning disable CS0162 // Unreachable code detected
                    for (int i = 0; i < files.Count; i++)
#pragma warning restore CS0162 // Unreachable code detected
                    {
                        HttpPostedFileBase file = files[i];
                        if (file.FileName.EndsWith(".csv"))
                        {
                            Stream stream = file.InputStream;
                            List<string> lstAccount = new List<string>();
                            DataTable csvTable = new DataTable();
                            using (CsvReader csvReader =
                                new CsvReader(new StreamReader(stream), true))
                            {
                                csvTable.Load(csvReader);
                            }
                            List<TTFileInputModel> lstFileRowTemp = new List<TTFileInputModel>();
                            List<string> lstCntrct = new List<string>();
                            List<TTFileInputModel> lstFileFinalRow = new List<TTFileInputModel>();
                            for (int iCount = 0; iCount < csvTable.Rows.Count; iCount++)
                            {
                                TTFileInputModel objRecord = new TTFileInputModel();
                                objRecord.Date = Convert.ToDateTime(csvTable.Rows[iCount]["Date (UTC)"]);
                                objRecord.Time = Convert.ToDateTime(csvTable.Rows[iCount]["Time (UTC)"]);
                                objRecord.Exchange = Convert.ToString(csvTable.Rows[iCount]["Exchange"]);
                                objRecord.Contract = Convert.ToString(csvTable.Rows[iCount]["Contract"]);
                                objRecord.BS = Convert.ToString(csvTable.Rows[iCount]["B/S"]);
                                objRecord.FillQty = Convert.ToInt32(csvTable.Rows[iCount]["FillQty"]);
                                objRecord.Price = Convert.ToDecimal(csvTable.Rows[iCount]["Price"]);
                                objRecord.PF = Convert.ToString(csvTable.Rows[iCount]["P/F"]);
                                objRecord.Route = Convert.ToString(csvTable.Rows[iCount]["Route"]);
                                objRecord.Account = Convert.ToString(csvTable.Rows[iCount]["Account"]);
                                objRecord.Originator = Convert.ToString(csvTable.Rows[iCount]["Originator"]);
                                objRecord.CurrentUser = Convert.ToString(csvTable.Rows[iCount]["CurrentUser"]);
                                objRecord.TTOrderID = Convert.ToString(csvTable.Rows[iCount]["TTOrderID"]);
                                objRecord.ParentID = Convert.ToString(csvTable.Rows[iCount]["ParentID"]);
                                objRecord.ManualFill = Convert.ToString(csvTable.Rows[iCount]["ManualFill"]);
                                lstFileRowTemp.Add(objRecord);
                            }

                            //save to database
                            bool bTempSave = objTTService.saveTempTTJSON(lstFileRowTemp);
                            if (bTempSave)
                            {
                                lstFileInputModelFromDB = objTTService.getTTRawJson();
                            }
                            //Get Commision and tick value from database
                            List<tt_product_master> lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == true).ToList();

                            //Remove other than CME market
                            lstFileInputModelFromDB = lstFileInputModelFromDB.Where(x => x.Exchange.Trim().ToUpper() == "CME").ToList();

                            //Get Distinct Row from table
                            DataView view = new DataView(csvTable);
                            DataTable distinctValues = view.ToTable(true, "Account");
                            List<string> lstAccountDist = lstFileInputModelFromDB.Select(x => x.Account).Distinct().ToList();
                            //loop account
                            foreach (string sAccountName in lstAccountDist)
                            {
                                List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                                lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                foreach (var sDate in lstDate)
                                {
                                    int TradeNumber = 0;
                                    List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date == sDate).ToList();
                                    List<string> lstContract = lstAccountWisefilter.Select(x => x.Contract).Distinct().ToList();
                                    foreach (string sContract in lstContract)
                                    {
                                        List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                                        lstContractWisefilter = lstAccountWisefilter.Where(x => x.Contract == sContract).OrderBy(x => x.Time).ToList();
                                        Decimal dNetFillQty = 0;
                                        int TradeEntry = 1;
                                        foreach (var drFilterRowAccountwise in lstContractWisefilter)
                                        {
                                            drFilterRowAccountwise.Time_str = drFilterRowAccountwise.Time.ToString("HH:mm:ss");
                                            drFilterRowAccountwise.Date_str = drFilterRowAccountwise.Date.ToString("yyyy-MM-dd");
                                            if (drFilterRowAccountwise.BS == "B")
                                            {
                                                drFilterRowAccountwise.BuySell = 1;
                                            }
                                            else if (drFilterRowAccountwise.BS == "S")
                                            {
                                                drFilterRowAccountwise.BuySell = -1;
                                            }
                                            else
                                            {
                                                throw new ServiceLayerException();
                                            }

                                            //Net Position
                                            if (dNetFillQty == 0)
                                            {
                                                dNetFillQty = Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell);
                                                TradeEntry = 1;
                                            }
                                            else
                                            {
                                                dNetFillQty = (Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell)) + dNetFillQty;
                                            }
                                            drFilterRowAccountwise.NetPosition = dNetFillQty;

                                            //Trade Entry, Exit
                                            if (TradeEntry == 1 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                            {
                                                drFilterRowAccountwise.TradeEntry = TradeEntry;
                                                drFilterRowAccountwise.TradeExit = 0;
                                                TradeEntry = 0;
                                            }
                                            else if (TradeEntry == 0 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                            {
                                                drFilterRowAccountwise.TradeEntry = 0;
                                                drFilterRowAccountwise.TradeExit = 0;
                                                TradeEntry = 0;
                                            }
                                            else
                                            {
                                                drFilterRowAccountwise.TradeEntry = 0;
                                                drFilterRowAccountwise.TradeExit = 1;
                                            }
                                            
                                            //Trader Buy Price, Sell Price
                                            if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == 1)
                                            {
                                                drFilterRowAccountwise.BuyPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                            }
                                            if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == -1)
                                            {
                                                drFilterRowAccountwise.SellPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                            }

                                            //Trade Number
                                            if (drFilterRowAccountwise.TradeEntry == 1)
                                            {
                                                drFilterRowAccountwise.TradeNumber = TradeNumber + 1;
                                                TradeNumber = TradeNumber + 1;
                                            }
                                            else
                                            {
                                                drFilterRowAccountwise.TradeNumber = TradeNumber;
                                            }
                                        }
                                        //average buy price and sell price
                                        if (lstContractWisefilter.Count > 0)
                                        {
                                            List<int> lstTradeNumber = lstContractWisefilter.Select(x => x.TradeNumber).Distinct().ToList();
                                            foreach (var item in lstTradeNumber)
                                            {
                                                decimal dAvgBuyPrice = 0;
                                                decimal dAvgSellPrice = 0;
                                                decimal dPriceTotal = 0;
                                                decimal dBuyfillTotal = 0;
                                                decimal dSellfillTotal = 0;
                                                decimal dTotalTradeQnty = 0;
                                                dTotalTradeQnty = lstContractWisefilter.Where(x => x.TradeNumber == item).Sum(x => x.FillQty);
                                                var objBuyItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == 1).ToList();
                                                foreach (var objBuy in objBuyItem)
                                                {
                                                    dPriceTotal = (objBuy.FillQty * objBuy.Price) + dPriceTotal;
                                                    dBuyfillTotal = dBuyfillTotal + objBuy.FillQty;
                                                }
                                                if (dPriceTotal != 0 && dBuyfillTotal != 0)
                                                {
                                                    dAvgBuyPrice = dPriceTotal / dBuyfillTotal;
                                                }
                                                dPriceTotal = 0;
                                                dSellfillTotal = 0;
                                                var objSellItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == -1).ToList();
                                                foreach (var objSell in objSellItem)
                                                {
                                                    dPriceTotal = (objSell.FillQty * objSell.Price) + dPriceTotal;
                                                    dSellfillTotal = dSellfillTotal + objSell.FillQty;
                                                }
                                                if (dPriceTotal != 0 && dSellfillTotal != 0)
                                                {
                                                    dAvgSellPrice = dPriceTotal / dSellfillTotal;
                                                }
                                                lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageBuyPrice = dAvgBuyPrice);
                                                lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageSellPrice = dAvgSellPrice);
                                                if (dBuyfillTotal == dSellfillTotal)
                                                {
                                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.realised_pnl_tick = ((dAvgSellPrice - dAvgBuyPrice) * dBuyfillTotal));
                                                }
                                                else
                                                {
                                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.unrealised_pnl_tick = 0);
                                                }
                                                lstContractWisefilter.Where(y => y.TradeNumber == item).ToList().ForEach(x => x.total_traded_qty_trade = dTotalTradeQnty);
                                            }
                                        }
                                        lstFileFinalRow.AddRange(lstContractWisefilter);
                                    }
                                }
                            }
                            //Realised P&L(Tick) in our example for TDTEST1 is 0.25 / 0.01 which = 25.All we do then is multiply by the TICK VALUE whic is alson on Google Sheets.For CL, it is $10.So the Realised P & L($ USD) = 25 * $10 which is $250
                            foreach (var item in lstFileFinalRow)
                            {
                                string sContractName = item.Contract.Substring(0, 3).Trim();
                                if (!string.IsNullOrEmpty(sContractName))
                                {
                                    tt_product_master objTTProductMaster = lstTTProductMaster.Where(x => x.symbol == sContractName).FirstOrDefault();
                                    if (objTTProductMaster != null)
                                    {
                                        item.realised_pnl_usd_without_commision = ((item.realised_pnl_tick / objTTProductMaster.increment) * objTTProductMaster.tick_value);
                                        item.commission = (item.total_traded_qty_trade * objTTProductMaster.commision);
                                        item.realised_pnl_usd_with_commision = item.realised_pnl_usd_without_commision - item.commission;
                                        item.w_trades = item.realised_pnl_usd_with_commision > 0 ? 1 : 0;
                                        item.l_trades = item.realised_pnl_usd_with_commision < 0 ? 1 : 0;
                                        item.s_trades = item.realised_pnl_usd_with_commision == 0 ? 1 : 0;
                                    }
                                    else
                                    {
                                        return Json(new { success = false, msg = "Contract commision does not exist in database" + sContractName }, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                            //Re-Calculate trade Number
                            foreach (var sAccountName in lstAccountDist)
                            {
                                List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                                lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                foreach (var sDate in lstDate)
                                {
                                    int TradeNumber = 0;
                                    List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                                    List<KeyValuePair<string, int>> lstTempOpenContract = new List<KeyValuePair<string, int>>();
                                    foreach (var item in lstAccountWisefilter)
                                    {
                                        if (item.TradeEntry == 1)
                                        {
                                            item.TradeNumber = TradeNumber + 1;
                                            TradeNumber = TradeNumber + 1;
                                            lstTempOpenContract.Add(new KeyValuePair<string, int>(item.Contract, item.TradeNumber));
                                        }
                                        else if (item.TradeExit == 1)
                                        {
                                            int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                            lstTempOpenContract.Remove(new KeyValuePair<string, int>(item.Contract, iTradeNumber));
                                            item.TradeNumber = iTradeNumber;
                                        }
                                        else
                                        {
                                            int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                            item.TradeNumber = iTradeNumber;
                                        }
                                    }
                                }
                            }
                            //Calculate High,low and cumalative p&l
                            foreach (var sAccountName in lstAccountDist)
                            {
                                List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                                lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                                List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                                foreach (var sDate in lstDate)
                                {
                                    List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                                    lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                                    List<int> lstTradeNumber = lstAccountWisefilter.Select(x => x.TradeNumber).Distinct().OrderBy(x => x).ToList();
                                    decimal dHigh = 0;
                                    decimal dLow = 0;
                                    decimal dCumalitive = 0;
                                    decimal dCumalitiveWithoutCommission = 0;
                                    foreach (int iTradeNumber in lstTradeNumber)
                                    {
                                        List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                                        lstContractWisefilter = lstAccountWisefilter.Where(x => x.TradeNumber == iTradeNumber).OrderBy(x => x.Time).ToList();
                                        dCumalitive = dCumalitive + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_with_commision;
                                        dCumalitiveWithoutCommission = dCumalitiveWithoutCommission + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_without_commision;
                                        foreach (var drFilterRowAccountwise in lstContractWisefilter)
                                        {
                                            if (dHigh < dCumalitive)
                                            {
                                                dHigh = dCumalitive;
                                            }
                                            if (dLow > dCumalitive)
                                            {
                                                dLow = dCumalitive;
                                            }
                                            drFilterRowAccountwise.high_pnl = dHigh;
                                            drFilterRowAccountwise.low_pnl = dLow;
                                        }
                                        lstContractWisefilter.ForEach(x => x.cumulative_pnl = dCumalitive);
                                        lstContractWisefilter.ForEach(x => x.cumalitve_pnl_without_commision = dCumalitiveWithoutCommission);
                                    }
                                }
                            }
                            if (lstFileFinalRow.Count != 0)
                            {
                                objTTService = new TTService();
                                var lstError = new List<KeyValuePair<string, string>>();
                                //bool bTempSave = objTTService.saveTTJSON(lstFileFinalRow,);
                                StringBuilder strUserList = new StringBuilder();
                                StringBuilder strAccountList = new StringBuilder();
                                if (lstError.Count() > 0)
                                {
                                    foreach (var item in lstError.Distinct())
                                    {
                                        if (item.Value == "ACCOUNT")
                                        {
                                            strAccountList.Append(item.Key + "," + strAccountList);
                                        }
                                        if (item.Value == "USER")
                                        {
                                            strUserList.Append(item.Key + "," + strUserList);
                                        }
                                    }
                                }
                                if (bTempSave)
                                {
                                    return Json(new
                                    {
                                        success = true,
                                        data = lstFileFinalRow,
                                        strAccountList = strAccountList.ToString(),
                                        strUserList = strUserList.ToString(),
                                    }, JsonRequestBehavior.AllowGet);
                                }
                                else
                                {
                                    return Json(new { success = false, data = "" }, JsonRequestBehavior.AllowGet);
                                }
                            }
                            else
                            {
                                return Json(new { success = true, data = "" }, JsonRequestBehavior.AllowGet);
                            }
                        }
                        else
                        {
                            ModelState.AddModelError("File", "This file format is not supported");
                            return View();
                        }
                    }
                    return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                return Json("No files selected.");
            }
        }
        #endregion

        #region Save Processing
        /// <summary>
        /// Saves the processing.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        public JsonResult SaveProcessing()
        {
            try
            {
                TTService objTTService = new TTService();
                List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                if (!string.IsNullOrEmpty(Request.RequestContext.HttpContext.Request.Form["RecordJSON"]))
                {
                    bool bSave = objTTService.saveTTJSON(Request.RequestContext.HttpContext.Request.Form["RecordJSON"]);
                    if (bSave)
                    {
                        string sResponseJson = Request.RequestContext.HttpContext.Request.Form["RecordJSON"];
                        sResponseJson = sResponseJson.Replace("tsTime", "tsTimeOld");
                        lstContractWisefilter = JsonConvert.DeserializeObject<List<TTFileInputModel>>(sResponseJson);
                        if (lstContractWisefilter.Count > 0)
                        {
                            var lstError = new List<KeyValuePair<string, string>>();
                            StringBuilder strUserList = new StringBuilder();
                            StringBuilder strAccountList = new StringBuilder();
                            long lResult = objTTService.SaveTTUploadFile(lstContractWisefilter, out lstError);
                            if (lstError.Count() > 0)
                            {
                                lstError = lstError.Distinct().ToList();
                                foreach (var item in lstError)
                                {
                                    if (item.Value == "ACCOUNT")
                                    {
                                        strAccountList.Append(item.Key + ",");
                                    }
                                    if (item.Value == "USER")
                                    {
                                        strUserList.Append(item.Key + ",");
                                    }
                                }
                            }
                            if (lResult > 0)
                            {
                                return Json(new
                                {
                                    success = true,
                                    resultcode = lResult,
                                    withError = lstError.Count(),
                                    strAccountList = strAccountList.ToString(),
                                    strUserList = strUserList.ToString(),
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if (lResult < 0)
                                return Json(new
                                {
                                    success = false,
                                    resultcode = lResult,
                                    strAccountList = strAccountList.ToString(),
                                    strUserList = strUserList.ToString(),
                                }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { success = false, resultcode = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, resultcode = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region TT User Fill Update        
        /// <summary>
        /// Updates the specified s account name.
        /// </summary>
        /// <param name="sAccountName">Name of the s account.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult update(string sAccountName = "")
        {
            TTFileModel objTTFileModel = new TTFileModel();
            List<TTFileInputModel> lstFileInputModel = new List<TTFileInputModel>();
            TTService objTTService = new TTService();
            string sAccountId = "";
            lstFileInputModel = objTTService.GetFillReportManual(sAccountId, 0, int.MaxValue, "", "");
            //  objTTFileModel.fill_list = lstFileInputModel;
            var objAccount = lstFileInputModel.Select(x => x.Account).Distinct().ToList();
            objTTFileModel.lstaccount = new SelectList(objAccount);
            objTTFileModel.account = sAccountName;
            return View(objTTFileModel);
        }

        /// <summary>
        /// Updates the specified upload.
        /// </summary>
        /// <param name="upload">The upload.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult update(HttpPostedFileBase upload)
        {
            if (ModelState.IsValid)
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    if (upload.FileName.EndsWith(".csv"))
                    {
                        Stream stream = upload.InputStream;
                        DataTable csvTable = new DataTable();
                        using (CsvReader csvReader =
                            new CsvReader(new StreamReader(stream), true))
                        {
                            csvTable.Load(csvReader);
                        }
                        string[] data;
                        foreach (DataRow row in csvTable.Rows)
                        {
                            //csvTable.Rows[0].ItemArray.Select(x => x.ToString()).ToArray();
                            data = row.ItemArray.Select(x => x.ToString()).ToArray();
                        }
                        return View(csvTable);
                    }
                    else
                    {
                        ModelState.AddModelError("File", "This file format is not supported");
                        return View();
                    }
                }
                else
                {
                    ModelState.AddModelError("File", "Please Upload Your file");
                }
            }
            return View();
        }
        #endregion

        #region Fill Read
        /// <summary>
        /// Fills the read.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="sAccount">The s account.</param>
        /// <param name="sFromDate">The s from date.</param>
        /// <param name="sToDate">The s to date.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        public JsonResult Fill_read(DataSourceRequest model, string sAccount = "", string sFromDate = "", string sToDate = "")
        {
            try
            {
                objTTService = new TTService();
                if (!string.IsNullOrEmpty(sAccount))
                {
                    sAccount = sAccount == "Select All Account" ? "" : sAccount;
                }
                var users = objTTService.GetFillReportManual(sAccount, model.Page - 1, model.PageSize, sFromDate, sToDate);
                var gridModel = new DataSourceResult
                {
                    Data = users,
                    Total = users.TotalCount
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Delete Checked Fill
        /// <summary>
        /// Deletes the checked fill.
        /// </summary>
        /// <param name="sID">The s identifier.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult DeleteCheckedFill(string sID)
        {
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            TTFileModel objTTFileModel = new TTFileModel();
            try
            {
                if (IDs != null)
                {
                    TTService objTTService = new TTService();
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    bool bResult = objTTService.DeleteFills(TagIds);
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Class");
        }
        #endregion

        #region View History        
        /// <summary>
        /// Viewhistories this instance.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult viewhistory()
        {
            List<TTUserModel> List = new List<TTUserModel>();
            return View(List);
        }

        public JsonResult ViewHistory_Read(DataSourceRequest model, string SearchName = "", string statusFilter = "")
        {
            objTTService = new TTService();
            var objProducts = objTTService.GetUserTTReportUpdate(SearchName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = objProducts,
                Total = objProducts.TotalCount
            };
            return Json(gridModel);
        }
        #endregion

        #region Read Uploaded CSV        
        /// <summary>
        /// Reads the uploaded CSV.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult ReadUploaded_CSV()
        {
            try
            {
                List<TTFileInputModel> lstFileInputModelFromDB = new List<TTFileInputModel>();

                List<string> lstAccount = new List<string>();
                DataTable csvTable = new DataTable();
                List<TTFileInputModel> lstFileRowTemp = new List<TTFileInputModel>();
                List<string> lstCntrct = new List<string>();
                List<TTFileInputModel> lstFileFinalRow = new List<TTFileInputModel>();
                lstFileInputModelFromDB = objTTService.getTTRawJson();

                //Get Commision and tick value from database
                List<tt_product_master> lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == true).ToList();

                //Remove other than CME market
                lstFileInputModelFromDB = lstFileInputModelFromDB.Where(x => x.Exchange.Trim().ToUpper() == "CME").ToList();

                //Get Distinct Row from table
                List<string> lstAccountDist = lstFileInputModelFromDB.Select(x => x.Account).Distinct().ToList();

                //loop account
                foreach (string sAccountName in lstAccountDist)
                {
                    List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                    lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                    List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                    foreach (var sDate in lstDate)
                    {
                        int TradeNumber = 0;
                        List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                        lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date == sDate).ToList();
                        List<string> lstContract = lstAccountWisefilter.Select(x => x.Contract).Distinct().ToList();
                        foreach (string sContract in lstContract)
                        {
                            List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                            lstContractWisefilter = lstAccountWisefilter.Where(x => x.Contract == sContract).OrderBy(x => x.Time).ToList();
                            Decimal dNetFillQty = 0;
                            int TradeEntry = 1;
                            foreach (var drFilterRowAccountwise in lstContractWisefilter)
                            {
                                drFilterRowAccountwise.Time_str = drFilterRowAccountwise.Time.ToString("HH:mm:ss");
                                drFilterRowAccountwise.Date_str = drFilterRowAccountwise.Date.ToString("yyyy-MM-dd");
                                if (drFilterRowAccountwise.BS == "B")
                                {
                                    drFilterRowAccountwise.BuySell = 1;
                                }
                                else if (drFilterRowAccountwise.BS == "S")
                                {
                                    drFilterRowAccountwise.BuySell = -1;
                                }
                                else
                                {
                                    throw new ServiceLayerException();
                                }

                                //Net Position
                                if (dNetFillQty == 0)
                                {
                                    dNetFillQty = Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell);
                                    TradeEntry = 1;
                                }
                                else
                                {
                                    dNetFillQty = (Convert.ToDecimal(drFilterRowAccountwise.FillQty) * Convert.ToDecimal(drFilterRowAccountwise.BuySell)) + dNetFillQty;
                                }
                                drFilterRowAccountwise.NetPosition = dNetFillQty;

                                //Trade Entry, Exit
                                if (TradeEntry == 1 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                {
                                    drFilterRowAccountwise.TradeEntry = TradeEntry;
                                    drFilterRowAccountwise.TradeExit = 0;
                                    TradeEntry = 0;
                                }
                                else if (TradeEntry == 0 && Convert.ToDecimal(drFilterRowAccountwise.NetPosition) != 0)
                                {
                                    drFilterRowAccountwise.TradeEntry = 0;
                                    drFilterRowAccountwise.TradeExit = 0;
                                    TradeEntry = 0;
                                }
                                else
                                {
                                    drFilterRowAccountwise.TradeEntry = 0;
                                    drFilterRowAccountwise.TradeExit = 1;
                                }

                                //Trader Buy Price, Sell Price
                                if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == 1)
                                {
                                    drFilterRowAccountwise.BuyPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                }
                                if (Convert.ToInt32(drFilterRowAccountwise.BuySell) == -1)
                                {
                                    drFilterRowAccountwise.SellPrice = (Convert.ToDecimal(drFilterRowAccountwise.Price));
                                }

                                //Trade Number
                                if (drFilterRowAccountwise.TradeEntry == 1)
                                {
                                    drFilterRowAccountwise.TradeNumber = TradeNumber + 1;
                                    TradeNumber = TradeNumber + 1;
                                }
                                else
                                {
                                    drFilterRowAccountwise.TradeNumber = TradeNumber;
                                }
                            }
                            //average buy price and sell price
                            if (lstContractWisefilter.Count > 0)
                            {
                                List<int> lstTradeNumber = lstContractWisefilter.Select(x => x.TradeNumber).Distinct().ToList();
                                foreach (var item in lstTradeNumber)
                                {
                                    decimal dAvgBuyPrice = 0;
                                    decimal dAvgSellPrice = 0;
                                    decimal dPriceTotal = 0;
                                    decimal dBuyfillTotal = 0;
                                    decimal dSellfillTotal = 0;
                                    decimal dTotalTradeQnty = 0;
                                    dTotalTradeQnty = lstContractWisefilter.Where(x => x.TradeNumber == item).Sum(x => x.FillQty);
                                    var objBuyItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == 1).ToList();
                                    foreach (var objBuy in objBuyItem)
                                    {
                                        dPriceTotal = (objBuy.FillQty * objBuy.Price) + dPriceTotal;
                                        dBuyfillTotal = dBuyfillTotal + objBuy.FillQty;
                                    }
                                    if (dPriceTotal != 0 && dBuyfillTotal != 0)
                                    {
                                        dAvgBuyPrice = dPriceTotal / dBuyfillTotal;
                                    }
                                    dPriceTotal = 0;
                                    dSellfillTotal = 0;
                                    var objSellItem = lstContractWisefilter.Where(x => x.TradeNumber == item && x.BuySell == -1).ToList();
                                    foreach (var objSell in objSellItem)
                                    {
                                        dPriceTotal = (objSell.FillQty * objSell.Price) + dPriceTotal;
                                        dSellfillTotal = dSellfillTotal + objSell.FillQty;
                                    }
                                    if (dPriceTotal != 0 && dSellfillTotal != 0)
                                    {
                                        dAvgSellPrice = dPriceTotal / dSellfillTotal;
                                    }
                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageBuyPrice = dAvgBuyPrice);
                                    lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.AverageSellPrice = dAvgSellPrice);
                                    if (dBuyfillTotal == dSellfillTotal)
                                    {
                                        lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.realised_pnl_tick = ((dAvgSellPrice - dAvgBuyPrice) * dBuyfillTotal));
                                    }
                                    else
                                    {
                                        lstContractWisefilter.Where(x => x.TradeNumber == item).ToList().ForEach(x => x.unrealised_pnl_tick = 0);
                                    }
                                    lstContractWisefilter.Where(y => y.TradeNumber == item).ToList().ForEach(x => x.total_traded_qty_trade = dTotalTradeQnty);
                                }
                            }
                            lstFileFinalRow.AddRange(lstContractWisefilter);
                        }
                    }
                }

                //Realised P&L(Tick) in our example for TDTEST1 is 0.25 / 0.01 which = 25.All we do then is multiply by the TICK VALUE whic is alson on Google Sheets.For CL, it is $10.So the Realised P & L($ USD) = 25 * $10 which is $250
                foreach (var item in lstFileFinalRow)
                {
                    string sContractName = item.Contract.Substring(0, 3).Trim();
                    if (!string.IsNullOrEmpty(sContractName))
                    {
                        tt_product_master objTTProductMaster = lstTTProductMaster.Where(x => x.symbol == sContractName).FirstOrDefault();
                        if (objTTProductMaster != null)
                        {
                            item.realised_pnl_usd_without_commision = ((item.realised_pnl_tick / objTTProductMaster.increment) * objTTProductMaster.tick_value);
                            item.commission = (item.total_traded_qty_trade * objTTProductMaster.commision);
                            item.realised_pnl_usd_with_commision = item.realised_pnl_usd_without_commision - item.commission;
                            item.w_trades = item.realised_pnl_usd_with_commision > 0 ? 1 : 0;
                            item.l_trades = item.realised_pnl_usd_with_commision < 0 ? 1 : 0;
                            item.s_trades = item.realised_pnl_usd_with_commision == 0 ? 1 : 0;
                        }
                        else
                        {
                            return Json(new { success = false, msg = "Contract commision does not exist in database" + sContractName }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }

                //Re-Calculate trade Number
                foreach (var sAccountName in lstAccountDist)
                {
                    List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                    lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                    List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                    foreach (var sDate in lstDate)
                    {
                        int TradeNumber = 0;
                        List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                        lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                        List<KeyValuePair<string, int>> lstTempOpenContract = new List<KeyValuePair<string, int>>();
                        foreach (var item in lstAccountWisefilter)
                        {
                            if (item.TradeEntry == 1)
                            {
                                item.TradeNumber = TradeNumber + 1;
                                TradeNumber = TradeNumber + 1;
                                lstTempOpenContract.Add(new KeyValuePair<string, int>(item.Contract, item.TradeNumber));
                            }
                            else if (item.TradeExit == 1)
                            {
                                int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                lstTempOpenContract.Remove(new KeyValuePair<string, int>(item.Contract, iTradeNumber));
                                item.TradeNumber = iTradeNumber;
                            }
                            else
                            {
                                int iTradeNumber = lstTempOpenContract.Where(x => x.Key == item.Contract).FirstOrDefault().Value;
                                item.TradeNumber = iTradeNumber;
                            }
                        }
                    }
                }

                //Calculate High,low and cumalative p&l
                foreach (var sAccountName in lstAccountDist)
                {
                    List<TTFileInputModel> lstDateWisefilter = new List<TTFileInputModel>();
                    lstDateWisefilter = lstFileInputModelFromDB.Where(x => x.Account == sAccountName).ToList();
                    List<DateTime> lstDate = lstDateWisefilter.Select(x => x.Date).Distinct().ToList();
                    foreach (var sDate in lstDate)
                    {
                        List<TTFileInputModel> lstAccountWisefilter = new List<TTFileInputModel>();
                        lstAccountWisefilter = lstDateWisefilter.Where(x => x.Date.Date == sDate.Date).ToList();
                        List<int> lstTradeNumber = lstAccountWisefilter.Select(x => x.TradeNumber).Distinct().OrderBy(x => x).ToList();
                        decimal dHigh = 0;
                        decimal dLow = 0;
                        decimal dCumalitive = 0;
                        decimal dCumalitiveWithoutCommission = 0;
                        foreach (int iTradeNumber in lstTradeNumber)
                        {
                            List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                            lstContractWisefilter = lstAccountWisefilter.Where(x => x.TradeNumber == iTradeNumber).OrderBy(x => x.Time).ToList();
                            dCumalitive = dCumalitive + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_with_commision;
                            dCumalitiveWithoutCommission = dCumalitiveWithoutCommission + lstContractWisefilter.FirstOrDefault().realised_pnl_usd_without_commision;
                            foreach (var drFilterRowAccountwise in lstContractWisefilter)
                            {
                                if (dHigh < dCumalitive)
                                {
                                    dHigh = dCumalitive;
                                }
                                if (dLow > dCumalitive)
                                {
                                    dLow = dCumalitive;
                                }
                                drFilterRowAccountwise.high_pnl = dHigh;
                                drFilterRowAccountwise.low_pnl = dLow;
                            }
                            lstContractWisefilter.ForEach(x => x.cumulative_pnl = dCumalitive);
                            lstContractWisefilter.ForEach(x => x.cumalitve_pnl_without_commision = dCumalitiveWithoutCommission);
                        }
                    }
                }
                if (lstFileFinalRow.Count != 0)
                {
                    objTTService = new TTService();
                    var lstError = new List<KeyValuePair<string, string>>();
                    //bool bTempSave = objTTService.saveTTJSON(lstFileFinalRow,);
                    StringBuilder strUserList = new StringBuilder();
                    StringBuilder strAccountList = new StringBuilder();
                    if (lstError.Count() > 0)
                    {
                        foreach (var item in lstError.Distinct())
                        {
                            if (item.Value == "ACCOUNT")
                            {
                                strAccountList.Append(item.Key + "," + strAccountList);
                            }
                            if (item.Value == "USER")
                            {
                                strUserList.Append(item.Key + "," + strUserList);
                            }
                        }
                    }
                    return Json(new
                    {
                        success = true,
                        data = lstFileFinalRow,
                        strAccountList = strAccountList.ToString(),
                        strUserList = strUserList.ToString(),
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = true, data = "" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("Error occurred. Error details: " + ex.Message);
            }
        }
        #endregion

        #region Processed API FILL        
        /// <summary>
        /// Processeds the apifill.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <param name="sAccount">The s account.</param>
        /// <param name="sFromDate">The s from date.</param>
        /// <param name="sToDate">The s to date.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public JsonResult ProcessedAPIFILL(DataSourceRequest model, string sAccount = "", string sFromDate = "", string sToDate = "")
        {
            try
            {
                if (string.IsNullOrEmpty(sAccount))
                {
                    sAccount = "0";
                }
                else
                {
                    sAccount = sAccount == "" ? "0" : sAccount;
                }
                string sMaxTimeStamp = "";
                string sMinTimeStamp = "";
                if (!string.IsNullOrEmpty(sFromDate))
                {
                    sMinTimeStamp = TraderdockTimeManager.DateTimeToUnixTimestamp(Convert.ToDateTime(sFromDate).Date.ToUniversalTime());
                }
                if (!string.IsNullOrEmpty(sToDate))
                {
                    sMaxTimeStamp = TraderdockTimeManager.DateTimeToUnixTimestamp(Convert.ToDateTime(sToDate).Date.ToUniversalTime());
                }
                GetTTGetFillService objTTService = new GetTTGetFillService();
                bool bResult = objTTService.GetFills(Convert.ToInt64(sAccount), sMaxTimeStamp, sMinTimeStamp);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Success! Record updated." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = false, Msgnew = "Record not found! please try again." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Processed Checked Fill        
        /// <summary>
        /// Processeds the checked fill.
        /// </summary>
        /// <param name="sID">The s identifier.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        [HttpGet]
        public ActionResult ProcessedCheckedFill(string sID)
        {
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            TTFileModel objTTFileModel = new TTFileModel();
            try
            {
                if (IDs != null)
                {
                    TTService objTTService = new TTService();
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    bool bResult = objTTService.DeleteFills(TagIds);
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Class");
        }
        #endregion

        #region TT Account Mapping      
        /// <summary>
        /// Tts the account mapping.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public JsonResult TTAccountMapping()
        {
            TTService objTTService = new TTService();
            bool bResult = objTTService.TTAccountMapping();
            return Json(new { result = bResult }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Save Processing
        /// <summary>
        /// Saves the processing.
        /// </summary>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        [HttpPost]
        public JsonResult SaveProcessingAPI()
        {
            try
            {
                TTService objTTService = new TTService();
                List<TTFileInputModel> lstContractWisefilter = new List<TTFileInputModel>();
                if (!string.IsNullOrEmpty(Request.RequestContext.HttpContext.Request.Form["RecordJSON"]))
                {
                    bool bSave = objTTService.saveTTJSON(Request.RequestContext.HttpContext.Request.Form["RecordJSON"]);
                    if (bSave)
                    {
                        string sResponseJson = Request.RequestContext.HttpContext.Request.Form["RecordJSON"];
                        sResponseJson = sResponseJson.Replace("tsTime", "tsTimeOld");
                        lstContractWisefilter = JsonConvert.DeserializeObject<List<TTFileInputModel>>(sResponseJson);
                        if (lstContractWisefilter.Count > 0)
                        {
                            var lstError = new List<KeyValuePair<string, string>>();
                            //bool bTempSave = objTTService.saveTTJSON(lstFileFinalRow,);
                            StringBuilder strUserList = new StringBuilder();
                            StringBuilder strAccountList = new StringBuilder();

                            long lResult = objTTService.SaveTTUploadFile(lstContractWisefilter, out lstError);
                            if (lstError.Count() > 0)
                            {
                                lstError = lstError.Distinct().ToList();
                                foreach (var item in lstError)
                                {
                                    if (item.Value == "ACCOUNT")
                                    {
                                        strAccountList.Append(item.Key + ",");
                                    }
                                    if (item.Value == "USER")
                                    {
                                        strUserList.Append(item.Key + ",");
                                    }
                                }
                            }
                            if (lResult > 0)
                            {
                                return Json(new
                                {
                                    success = true,
                                    resultcode = lResult,
                                    withError = lstError.Count(),
                                    strAccountList = strAccountList.ToString(),
                                    strUserList = strUserList.ToString(),
                                }, JsonRequestBehavior.AllowGet);
                            }
                            else if (lResult < 0)
                                return Json(new
                                {
                                    success = false,
                                    resultcode = lResult,
                                    strAccountList = strAccountList.ToString(),
                                    strUserList = strUserList.ToString(),
                                }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    return Json(new { success = false, resultcode = 0 }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false, resultcode = 0 }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

    }
}