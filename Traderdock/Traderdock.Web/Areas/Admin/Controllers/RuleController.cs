﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;
using Traderdock.Entity.ViewModel;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class RuleController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService;
        RuleService objRuleService;
        #endregion

        #region Add/Edit Rule
        /// <summary>
        /// Add Edit Rule
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 12/7/2017
        /// Dev By: Bharat Katua
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditRule()
        {
            objRuleService = new RuleService();
            objCommonService = new CommonService();
            RuleModel objRuleModel = new RuleModel();
            objRuleModel = objRuleService.GetRule();
            
            return View(objRuleModel);
        }

        /// <summary>
        /// Add Edit Rule 
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// Date : 12/7/2017
        /// Dev By: Bharat Katua
        [HttpPost]
        public ActionResult AddEditRule(RuleModel objModel)
        {

            if (ModelState.IsValid)
            {
                //Transaction will be maintain
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.rule_id > 0)
                    {
                        objModel.rule_id = objModel.rule_id;
                        objModel.updated_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.updated_date = System.DateTime.UtcNow;

                        bool bFlag = objRuleService.AddEditRule(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Dashboard", "Dashboard");
                    }
                    else
                    {
                        objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.created_on = System.DateTime.UtcNow;
                        objModel.updated_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.updated_date = System.DateTime.UtcNow;

                        bool bFlag = objRuleService.AddEditRule(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                        {
                            transaction.Rollback();
                        }
                        return RedirectToAction("Dashboard", "Dashboard");
                    }
                }
            }
            return View(objModel);
        }
        #endregion
    }
}