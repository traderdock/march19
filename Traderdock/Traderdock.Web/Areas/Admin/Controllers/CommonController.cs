﻿using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;
using Traderdock.Entity.MemberModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class CommonController : Controller
    {

        #region Generic PDF
        public bool GeneratePDF(HttpResponseBase Response, DataTable dt, List<string> lstHeaderRow, List<string> lstHeaderRowName, string Header)
        {
            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            //Change the Header Row back to white color
            if (dt.Rows.Count > 0)
            {
                GridView1.HeaderRow.Style.Add("background-color", "#FFFFFF");
                GridView1.HeaderStyle.BorderColor = Color.Black;
                GridView1.HeaderStyle.Font.Bold = true;
                GridView1.Font.Name = "Source Sans Pro";
                GridView1.Font.Size = FontUnit.Point(10);
                GridView1.ForeColor = Color.Black;
                GridView1.CellPadding = 2;
                //table{ font - family: Helvetica; color: black; background - color:white; font - size:19px; border - collapse: collapse; width: 100 %;
                //}
                //td,th{ border: 1px solid #dddddd;text-align:left;padding:8px;}
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition",
                    "attachment;filename=" + Header + ".pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                sw.WriteLine("<h3>" + Header + "</h3><br><br>");
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                GridView1.RenderControl(hw);
                //StringReader sr = new StringReader(sw.ToString());
                string strObj;
                strObj = sw.ToString();
                for (int i = 0; i < lstHeaderRow.Count; i++)
                {
                    strObj = strObj.Replace(lstHeaderRow[i], lstHeaderRowName[i]);
                }
                StringReader srRevise = new StringReader(strObj);
                Document pdfDoc = new Document(PageSize.A4.Rotate(), 25f, 25f, 50f, 0f);
#pragma warning disable CS0612 // Type or member is obsolete
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
#pragma warning restore CS0612 // Type or member is obsolete
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(srRevise);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();
            }
            return true;
        }
        #endregion

        #region Generic Excel
        public bool GenerateExcel(HttpResponseBase Response, DataTable dt, List<string> lstHeaderRow, List<string> lstHeaderRowName, string Header)
        {
            //Create a dummy GridView
            DataTable objDataTable = new DataTable();
            //Create a dummy GridView
            GridView GridView1 = new GridView();
            GridView1.AllowPaging = false;
            GridView1.DataSource = dt;
            GridView1.DataBind();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition",
             "attachment;filename=" + Header + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            for (int i = 0; i < GridView1.Rows.Count; i++)
            {
                //Apply text style to each Row
                GridView1.Rows[i].Attributes.Add("class", "textmode");
            }
            GridView1.RenderControl(hw);
            string strObj;
            strObj = sw.ToString();
            for (int i = 0; i < lstHeaderRow.Count; i++)
            {
                strObj = strObj.Replace(lstHeaderRow[i], lstHeaderRowName[i]);
            }
            StringReader srRevise = new StringReader(strObj);
            //style to format numbers to string
            string style = @"<style> .textmode { mso-number-format:\@; } </style>";
            Response.Write(style);
            Response.Output.Write(strObj.ToString());
            Response.Flush();
            Response.End();
            return true;
        }
        #endregion

        #region Add Subscription From Admin Panel
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objPaypalModel"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddSubscriptionAdminPanel(PaypalModel objPaypalModel)
        {
            PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
            bool bResult = objPaypalPaymentService.AddSubscriptionAdminPanel(objPaypalModel);
            return Json(new { success = bResult }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}