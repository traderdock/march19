﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class EmailTemplateController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        EmailTemplateService objEmailService;
        #endregion

        #region Get Email Templates
        /// <summary>
        /// Email Templates
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            try
            {
                //objEmailService = new EmailTemplateService();
                //return View(objEmailService.GetEmailList());
                List<EmailTemplateModel> List = new List<EmailTemplateModel>();
                return View(List);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public JsonResult EmailTemplate_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objEmailService = new EmailTemplateService();
            var EmailTemplateList = objEmailService.EmailTemplatesList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = EmailTemplateList,
                Total = EmailTemplateList.TotalCount
            };

            return Json(gridModel);
        }

        /// <summary>
        /// View Email Template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ViewEmailTemplate(long id)
        {
            try
            {
                EmailTemplateModel objEmailTemplateModel = new EmailTemplateModel();
                objEmailService = new EmailTemplateService();
                if (id > 0)
                {
                    objEmailTemplateModel = objEmailService.GetEmailTemplate(id);
                    objEmailTemplateModel.Body = Utilities.ReadAllText(ConfigurationManager.AppSettings["EmailBaseURL"] + objEmailTemplateModel.BodyPart);
                    return PartialView(objEmailTemplateModel);
                }
                else
                {
                    return this.Index();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Save Email
        /// <summary>
        /// Save Email Template
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SaveEmailTemplate(long id)
        {
            try
            {
                EmailTemplateModel objEmailTemplateModel = new EmailTemplateModel();
                objEmailService = new EmailTemplateService();
                if (id > 0)
                {
                    objEmailTemplateModel = objEmailService.GetEmailTemplate(id);
                    objEmailTemplateModel.Body = Utilities.ReadAllText(ConfigurationManager.AppSettings["EmailBaseURL"] + objEmailTemplateModel.BodyPart);
                    #region commented test code
                    //model = new EmailTemplateModel()
                    //{
                    //    Body = emailTemplateResponse.body_part,
                    //    EmailTemplateID = emailTemplateResponse.email_template_id,
                    //    //LastModifiedBy = emailTemplateResponse.LastModifiedBy,
                    //    //LastModifiedUserID = emailTemplateResponse.LastModifiedUserID,
                    //    Name = emailTemplateResponse.name,
                    //    Subject = emailTemplateResponse.subject
                    //};

                    //var templateParamters = service.RetrieveEmailTemplateParameters(emailTemplateResponse.email_template_id);
                    //if (!templateParamters.HasError)
                    //{
                    //    foreach (var templateParameter in templateParamters.Result)
                    //    {
                    //        model.TemplateParameters.Add(new EmailTemplateParameterModel()
                    //        {
                    //            EmailTemplateID = templateParameter.EmailTemplateID,
                    //            EmailTemplateParameterID = templateParameter.EmailTemplateParameterID,
                    //            Parameter = templateParameter.Parameter
                    //        });
                    //    }
                    //} 
                    #endregion
                    return PartialView(objEmailTemplateModel);
                }
                else
                {
                    return this.Index();
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        /// <summary>
        /// Save Email Template
        /// </summary>
        /// <param name="objEmailModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveEmailTemplate(EmailTemplateModel objEmailModel)
        {
            try
            {
                bool status = false;
                string message = string.Empty;
                objEmailService = new EmailTemplateService();
                if (ModelState.IsValid)
                {
                    status = objEmailService.SaveEmailTemplate(objEmailModel);
                    if (status)
                    {
                        return View("index");
                        //return new JsonResult { Data = new { status = status, message = message } };
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "No data found.";
                    }
                    #region temp delete
                    //var entity = new EmailTemplateEntity()
                    //{
                    //    Body = email_template_master.Body,
                    //    EmailTemplateID = email_template_master.EmailTemplateID,
                    //    LastModifiedBy = email_template_master.LastModifiedBy,
                    //    LastModifiedUserID = email_template_master.LoggedInUser.UserID,
                    //    Name = email_template_master.Name,
                    //    Subject = email_template_master.Subject
                    //};

                    //var response = service.SaveEmailTemplate(entity);
                    //if (!response.HasError)
                    //{
                    //    status = true;
                    //} 
                    #endregion
                }
                return new JsonResult { Data = new { status = status, message = message } };
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Send Email
        public ActionResult SendEmailTemplate(string sBodyPart)
        {
            try
            {
                UserService objUserService = new UserService();
                string sEmail = ConfigurationManager.AppSettings["TestEmailAddress"];
                int sUserId = Convert.ToInt32(ConfigurationManager.AppSettings["TestUserId"]);
                if (!string.IsNullOrEmpty(sEmail) && sUserId > 0)
                {
                    UserMasterModel objUsermasterModel = objUserService.GetUser(sUserId);
                    EmailTemplateModel objEmailTemplateModel = new EmailTemplateModel();
                    objEmailService = new EmailTemplateService();
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.Email, objUsermasterModel.user_email);
                    parameters.Add(EmailTemplateParameters.FullName, objUsermasterModel.first_name);
                    parameters.Add(EmailTemplateParameters.Password, objUsermasterModel.password);
                    if (sBodyPart == "UserRegistration")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.UserRegistration, parameters, null);
                    }
                    else if (sBodyPart == "PasswordChange.html")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.PasswordChange, parameters, null);
                    }
                    else if (sBodyPart == "AccountDelete.html")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.AccountDelete, parameters, null);
                    }
                    else if (sBodyPart == "ForgotPasswordNew.html")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.ForgotPasswordNew, parameters, null);
                    }
                    else if (sBodyPart == "RithmicAccount.html")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.RithmicAccount, parameters, null);
                    }
                    else if (sBodyPart == "WelcomeMail.html")
                    {
                        new CommonService().SendEmail(sEmail, EmailTemplateNames.WelcomeMail, parameters, null);
                    }
                }
                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        public void uploadnow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/faqmedia/"), ImageName);
                upload.SaveAs(path);
            }
        }

        public ActionResult uploadPartial()
        {
            var appData = Server.MapPath("~/faqmedia");
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = Url.Content("/faqmedia/" + Path.GetFileName(x))
            });

            //ViewBag.Images = images;
            return View(images);
        }

    }
}