﻿using System;
using Traderdock.Common;
using Traderdock.Common.Constants;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Security;
using Traderdock.Services;
using Traderdock.Entity.ViewModel;
using Traderdock.Entity.Model;
using System.Web;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    public class LoginController : Controller
    {
        #region Global Variables
        CommonService commonServiceobj;
        UserService objUserService;
        SettingsService objSettingService;
        #endregion

        #region Login
        /// <summary>
        /// Login
        /// </summary>
        /// <returns></returns>
        /// Feedback : Issue-4 (login issue on back button)
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login()
        {
            //clear session when get back to login page
            Session["User"] = null;
            Session["UserID"] = null;
            Session["UserEmail"] = null;
            FormsAuthentication.SignOut();
            //clear session when get back to login page

            bool Status = false;
            objUserService = new UserService();
            LoginModel model = new LoginModel();
            try
            {
                Status = objUserService.CheckAdminUser();
                var usernamecookie = Request.Cookies["UserEmail"];
                var passwordcookie = Request.Cookies["Password"];
                if (usernamecookie != null && passwordcookie != null)
                {
                    model.Email = Request.Cookies["UserEmail"].Value;
                    model.Password = Request.Cookies["Password"].Value;
                    model.RememberMe = true;
                    return View(model);
                }
                else
                {
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Login(LoginModel loginModel)
        {
            try
            {
                objUserService = new UserService();
                objSettingService = new SettingsService();
                UserMasterModel objUserMasterModel = new UserMasterModel();
                DashboardService objDashboardService = new DashboardService();
                if (ModelState.IsValid)
                {
                    objUserMasterModel = objUserService.Login(loginModel);
                    if (objUserMasterModel != null)
                    {
                        if (objUserMasterModel.is_active)
                        {
                            SettingModel objSettingModel = new SettingModel();
                            objSettingModel = objSettingService.GetSettingByName("PageSize");
                            if (objSettingModel.setting_value != null)
                            {
                                Session["PageSize"] = objSettingModel.setting_value;
                            }
                            Session["UserID"] = objUserMasterModel.user_id;
                            Session["UserEmail"] = objUserMasterModel.user_email;
                            Session["NotificationCount"] = objDashboardService.NotificationCount(objUserMasterModel.user_id);
                            Session["UserName"] = objUserMasterModel.first_name + " " + objUserMasterModel.last_name;
                            if (!string.IsNullOrEmpty(objUserMasterModel.profile_image_url))
                            {
                                Session["ProfileImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objUserMasterModel.profile_image_url);
                            }
                            else
                            {
                                Session["ProfileImage"] = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                            }
                            FormsAuthentication.SetAuthCookie(objUserMasterModel.user_email, false);
                            if (loginModel.RememberMe == true)
                            {

                                HttpCookie ckUserName = new HttpCookie("UserEmail");
                                ckUserName.Expires = DateTime.Now.AddDays(2);
                                ckUserName.Value = loginModel.Email;
                                Response.Cookies.Add(ckUserName);

                                HttpCookie ckPassword = new HttpCookie("Password");
                                ckPassword.Expires = DateTime.Now.AddDays(2);
                                ckPassword.Value = loginModel.Password;
                                Response.Cookies.Add(ckPassword);
                            }
                            else
                            {
                                HttpCookie ckUserName = new HttpCookie("UserEmail");
                                ckUserName.Expires = DateTime.Now.AddDays(-1d);
                                Response.Cookies.Add(ckUserName);

                                HttpCookie ckPassword = new HttpCookie("Password");
                                ckPassword.Expires = DateTime.Now.AddDays(-1d);
                                Response.Cookies.Add(ckPassword);
                            }
                            return RedirectToAction("index", "user");
                        }
                        else if (objUserMasterModel.is_password_false == true || objUserMasterModel.is_email_false == true)
                        {
                            TempData["ErrorUserMsg"] = "Email or password was incorrect.";
                        }
                        else
                        {
                            if (!objUserMasterModel.is_active)
                            {
                                TempData["ErrorUserMsg"] = "Your account is not activated.";
                            }
                            else if (objUserMasterModel.is_deleted)
                            {
                                TempData["ErrorUserMsg"] = "Your account has been deleted.";
                            }
                            else if (objUserMasterModel.is_blocked)
                            {
                                TempData["ErrorUserMsg"] = "Your account has been blocked.";
                            }
                            else
                            {
                                TempData["ErrorUserMsg"] = "No account exist.";
                            }
                        }
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "Email or password was incorrect.";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View("Login", loginModel);
        }
        #endregion

        #region Forgot Password
        /// <summary>
        /// Forgot Password
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ForgotPassword(UserPasswordModel loginModel)
        {
            try
            {
                if (!string.IsNullOrEmpty(loginModel.email))
                {
                    objUserService = new UserService();
                    commonServiceobj = new CommonService();
                    UserMasterModel objUserMasterModel = new UserMasterModel();
                    objUserMasterModel = objUserService.ForgotPassword(loginModel.email);

                    if (objUserMasterModel.user_id > 0)
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(EmailTemplateParameters.FullName, "Admin");
                        parameters.Add(EmailTemplateParameters.Email, Utilities.EncryptKey(objUserMasterModel.user_email, objUserMasterModel.crypto_key));
                        parameters.Add(EmailTemplateParameters.link, ConfigurationManager.AppSettings["SiteLink"] + "/Admin/Login/AdminPassword");
                        parameters.Add(EmailTemplateParameters.Key1, objUserMasterModel.crypto_key);
                        parameters.Add(EmailTemplateParameters.Password, objUserMasterModel.password);
                        bool emailResponse = commonServiceobj.SendEmail(loginModel.email, EmailTemplateNames.ForgotPassword, parameters, null);

                        if (emailResponse == true)
                        {
                            TempData["SuccessMsg"] = "Your password has been sent successfully!";
                            return RedirectToAction("Login");
                        }
                    }
                    else
                    {
                        TempData["ErrorMsg"] = "Please enter correct email";
                        return RedirectToAction("Login");
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Login");
        }
        #endregion

        #region Admin Forgot Password
        /// <summary>
        /// Admin Password
        /// </summary>
        /// <param name="e"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AdminPassword(string e, string k)
        {
            UserPasswordModel objUserPasswordModel = new UserPasswordModel();
            UserMasterModel objUserMasterModel = new UserMasterModel();
            objUserService = new UserService();
            if (e != null && k != null)
            {
                e = Utilities.DecryptKey(e, k);
                objUserMasterModel = objUserService.AdminPasswordReset(e, k);
                if (objUserPasswordModel == null)
                {
                    TempData["ErrorUserMsg"] = "No account links to this link please try again";
                }
                else
                {
                    objUserPasswordModel.email = objUserMasterModel.user_email;
                }

            }
            else
            {
                TempData["ErrorUserMsg"] = "No account links to this link please try again";
            }
            return View(objUserPasswordModel);
        }

        [HttpPost]
        public ActionResult AdminPassword(UserPasswordModel objUserPasswordModel)
        {
            try
            {
                bool Status = false;
                objUserService = new UserService();
                if (ModelState.IsValid)
                {
                    Status = objUserService.AdminPasswordReset(objUserPasswordModel);

                    if (Status)
                    {
                        TempData["AdminForgotMessage"] = "Your password has been changed. Please login with new password";
                        return RedirectToAction("Login");
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "New password and conform password not matched try again";
                        return View(objUserPasswordModel);
                    }
                }
                else
                    return View(objUserPasswordModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        #region Logout
        [HttpGet]
        public ActionResult Logout()
        {
            Session["User"] = null;
            Session["UserID"] = null;
            Session["UserEmail"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Login", "Login");
        }
        #endregion

        #region GetPromocode
        [HttpGet]
        [AllowAnonymous]
        public JsonResult GetPromoCode(string sPromoText)
        {
            try
            {
                PromoCodeModel objPromoCodeModel = new PromoCodeModel();
                PromoCodeService objPromoCodeService = new PromoCodeService();
                objPromoCodeModel = objPromoCodeService.GetPromocode(sPromoText);
                if (objPromoCodeModel.promo_code_id > 0)
                {
                    return Json(new { success = true, amount = objPromoCodeModel.reward_amount, id = objPromoCodeModel.promo_code_id }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}