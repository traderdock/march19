﻿using Kendo.Mvc.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class CMSController : Controller
    {
        #region Global Variables
        CMSService objService;
        #endregion

        #region Get CMS
        /// <summary>
        /// Index 
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            CMSModel List = new CMSModel();
            CommonService objCommonService = new CommonService();
            List.PageList = new SelectList(objCommonService.GetPageList(), "page_value", "page_name");
            List.PageSectionList = new SelectList(new List<string>(), "", "");
            return View(List);
        }

        /// <summary>
        /// Subscription Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult CMS_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "", string PageKey="",string PageSectionKey="")
        {
            //UserService ObjUserService = new UserService();//ignore 
            objService = new CMSService();
            //var SubscriptionList1 = objSubscriptionService.SubscriptionList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);//ignore
            var CMSList = objService.CMSList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter,PageKey,PageSectionKey);

            var gridModel = new DataSourceResult
            {
                Data = CMSList,
                Total = CMSList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region View CMS
        ///// <summary>
        ///// Get CMS Method
        ///// </summary>
        ///// <returns></returns>
        //[HttpGet]
        //public ActionResult Index1()
        //{
        //    objService = new CMSService();
        //    try
        //    {
        //        return View(objService.GetCMSs());
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        /// <summary>
        /// View CMS
        /// </summary>
        /// <param name="lCMSID"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ViewCMS(long? cmsId)
        {
            CMSModel objModel = null;
            objService = new CMSService();
            try
            {
                if (cmsId > 0)
                {
                    objModel = objService.GetCMS(cmsId);
                    if (objModel != null)
                        return View(objModel);
                    else
                        return RedirectToAction("GetCMS");
                }
                else
                {
                    return RedirectToAction("GetCMS");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion        

        #region Add/Edit CMS
        /// <summary>
        /// Save CMS
        /// </summary>
        /// <param name="cmsId"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SaveCMS(long? Id)
        {
            CMSModel cmsModel = null;
            CommonService objCommonService = new CommonService();
            objService = new CMSService();
            try
            {
                if (Id > 0)
                {
                    cmsModel = objService.GetCMS(Id);
                    cmsModel.PageSectionList = new SelectList(objCommonService.GetPageSectionList(cmsModel.page_id), "page_name", "page_value");
                }               
                cmsModel.PageList = new SelectList(objCommonService.GetPageList(), "page_value", "page_name");
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(cmsModel);
        }

        /// <summary>
        /// Save CMS
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveCMS(CMSModel objModel)
        {
            CommonService objCommonService = new CommonService();
            bool Status = false;
            objService = new CMSService();
            try
            {
                if (ModelState.IsValid)
                {
                    if (objModel.cms_id > 0)
                    {
                        Status = objService.SaveCMS(objModel);
                    }
                    TempData["Msg"] = "Record Edited Successfully";
                    //if (Status)
                    //    return Json(new { success = true, Msgnew = "Record Edited Successfully" }, JsonRequestBehavior.AllowGet);
                    //else
                    //    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
                    if (Status)
                        return RedirectToAction("SaveCMS", "CMS", new { id = objModel.cms_id });
                    //return View(objModel);
                    //return new JsonResult { Data = "Success" };
                    else
                        return RedirectToAction("SaveCMS", "CMS", new { id = objModel.cms_id });
                    //return View(objModel);
                }
                else
                {
                    objModel.PageList = new SelectList(objCommonService.GetPageList(), "page_value", "page_name");
                    objModel.PageSectionList = new SelectList(objCommonService.GetPageSectionList(objModel.page_id), "page_value", "page_name");
                    return View(objModel);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
        /// <summary>
        /// PageSubSection method return list of page sub section list
        /// </summary>        
        /// <param name="Key"></param>
        /// <returns></returns>
        public JsonResult PageSubSection(string Key)
        {
            CommonService objCommonService = new CommonService();
            List<Traderdock.Model.Model.PageModel> list = new List<Model.Model.PageModel>();
            list = objCommonService.GetPageSectionList(Key);

            return Json(list);
        }

        public void uploadnow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/faqmedia/"), ImageName);
                upload.SaveAs(path);
            }
        }

        public ActionResult uploadPartial()
        {
            var appData = Server.MapPath("~/faqmedia");
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = Url.Content("/faqmedia/" + Path.GetFileName(x))
            });

            //ViewBag.Images = images;
            return View(images);
        }
    }
}