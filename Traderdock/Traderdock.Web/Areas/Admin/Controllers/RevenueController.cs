﻿
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Web.Mvc;
using Traderdock.ORM;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;
using System.Data;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class RevenueController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService = new CommonService();
        SubscriptionService objSubscriptionService = new SubscriptionService();
        #endregion

        #region Get User
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 8-Aug-2017
        /// Dev By: Hardik  Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            objSubscriptionService = new SubscriptionService();
            List<UserSubscriptionModel> List = new List<UserSubscriptionModel>();
            var count = objSubscriptionService.totalRevenue();
            ViewData["TotalReveue"] = count;
            return View(List);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 8-Aug-2017
        /// Dev By: Hardik  Savaliya
        public JsonResult Revenue_Read(DataSourceRequest model, string SearchName = "",   string start_date = "",string end_date="")
        {
            if (!string.IsNullOrEmpty(start_date))
            {
                string[] values = start_date.Split('-');
                start_date = values[0].Trim();
                end_date = values[1].Trim();

                string[] s_date = start_date.Split('/');
                start_date = s_date[1].Trim();
                start_date = start_date + "/" + s_date[0].Trim();
                start_date = start_date + "/" + s_date[2].Trim();

                string[] e_date = end_date.Split('/');
                end_date = e_date[1].Trim();
                end_date = end_date + "/" + e_date[0].Trim();
                end_date = end_date + "/" + e_date[2].Trim();
            }
            objSubscriptionService = new SubscriptionService();
            var usersRevenue = objSubscriptionService.UserRevenueList(SearchName.ToLower(), model.Page - 1, model.PageSize,start_date,end_date);
            var gridModel = new DataSourceResult
            {
                Data = usersRevenue,
                Total = usersRevenue.TotalCount
            };
            return Json(gridModel);
        }
        #endregion

        #region Export to Excel
        public ActionResult ExportToExcel()
        {
            CommonController objCommonController = new CommonController();
            objSubscriptionService = new SubscriptionService();
            IList<UserSubscriptionModel> Users = new List<UserSubscriptionModel>();
            var objUserRevenue = objSubscriptionService.UserRevenueList("", 0, int.MaxValue, "");
            if (objUserRevenue.Count > 0)
            {
                Users = objUserRevenue;
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "revenue");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("full_name");
            lstHeaderRow.Add("transection_id");
            lstHeaderRow.Add("subscription_name");
            lstHeaderRow.Add("transection_date");
            lstHeaderRow.Add("start_date");
            lstHeaderRow.Add("end_date");
            lstHeaderRow.Add("payment_status");
            lstHeaderRow.Add("payment_method");
            lstHeaderRow.Add("price_per_month");
            lstHeaderRow.Add("promo_code");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("Full Name");
            lstHeaderRowName.Add("Transaction Id");
            lstHeaderRowName.Add("Subscription Name");
            lstHeaderRowName.Add("Transaction Date");
            lstHeaderRowName.Add("Start Date");
            lstHeaderRowName.Add("End Date");
            lstHeaderRowName.Add("Payment Status");
            lstHeaderRowName.Add("Payment Method");
            lstHeaderRowName.Add("Amount");
            lstHeaderRowName.Add("Promo Code");

            ExportService<UserSubscriptionModel> objExportService = new ExportService<UserSubscriptionModel>();
            DataTable dt = objExportService.GenerateDataTable(Users, lstHeaderRow);
            bool bStatus = objCommonController.GenerateExcel(Response, dt, lstHeaderRow, lstHeaderRowName, "User Revenue List");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion

        #region Export to PDF
        /// <summary>
        /// Export to Excel
        /// </summary>
        /// <returns></returns>
        /// Date : 24/01/2018
        /// Dev By: Hardik Savaliya
        public ActionResult ExportToPDF()
        {
            CommonController objCommonController = new CommonController();
            objSubscriptionService = new SubscriptionService();
            IList<UserSubscriptionModel> Users = new List<UserSubscriptionModel>();
            var objUserRevenue = objSubscriptionService.UserRevenueList("", 0, int.MaxValue, "");
            if (objUserRevenue.Count > 0)
            {
                Users = objUserRevenue;
            }
            else
            {
                TempData["Msg"] = "No Data Found";
                return RedirectToAction("index", "revenue");
            }
            List<string> lstHeaderRow = new List<string>();
            lstHeaderRow.Add("full_name");
            lstHeaderRow.Add("transection_id");
            lstHeaderRow.Add("subscription_name");
            lstHeaderRow.Add("transection_date");
            lstHeaderRow.Add("start_date");
            lstHeaderRow.Add("end_date");
            lstHeaderRow.Add("payment_status");
            lstHeaderRow.Add("payment_method");
            lstHeaderRow.Add("price_per_month");
            lstHeaderRow.Add("promo_code");
            List<string> lstHeaderRowName = new List<string>();
            lstHeaderRowName.Add("Full Name");
            lstHeaderRowName.Add("Transaction Id");
            lstHeaderRowName.Add("Subscription Name");
            lstHeaderRowName.Add("Transaction Date");
            lstHeaderRowName.Add("Start Date");
            lstHeaderRowName.Add("End Date");
            lstHeaderRowName.Add("Payment Status");
            lstHeaderRowName.Add("Payment Method");
            lstHeaderRowName.Add("Amount");
            lstHeaderRowName.Add("Promo Code");
            ExportService<UserSubscriptionModel> objExportService = new ExportService<UserSubscriptionModel>();
            DataTable dt = objExportService.GenerateDataTable(Users, lstHeaderRow);
            bool bStatus = objCommonController.GeneratePDF(Response, dt, lstHeaderRow, lstHeaderRowName, "User Revenue List");
            if (bStatus)
            {
                return Json(new { success = true, Msgnew = "Success!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
            else
            {
                return Json(new { success = true, Msgnew = "Something went wrong" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
            }
        }
        #endregion
    }
}