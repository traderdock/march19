﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class FAQMasterController : Controller
    {

        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        FAQMasterService objFAQMasterService;
        FAQCategoryService objFAQCategoryService;
        #endregion

        #region FAQ Get
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<FAQMasterModel> List = new List<FAQMasterModel>();
            return View(List);
        }

        /// <summary>
        /// Faq Master Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public JsonResult FAQMaster_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            objFAQMasterService = new FAQMasterService();
            var FAQCategoryList = objFAQMasterService.FAQMasterList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = FAQCategoryList,
                Total = FAQCategoryList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region FAQ Category Add/Edit
        /// <summary>
        /// FAQ Category Add Edit
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditFAQMaster(Int64? id)
        {
            objFAQMasterService = new FAQMasterService();
            FAQSubCategoryService objFAQSubCategoryService = new FAQSubCategoryService();
            objFAQCategoryService = new FAQCategoryService();
            FAQMasterModel objFaqMasterModel = new FAQMasterModel();
            if (id > 0)
            {
                objFaqMasterModel = objFAQMasterService.GetFAQMaster(id);
            }
            objFaqMasterModel.FAQCategoryList = GetFAQCategory();
            objFaqMasterModel.FAQSubCategoryList = new SelectList(objFAQSubCategoryService.GetFAQSubCategoryList( objFaqMasterModel.faq_category_id), "faq_sub_category_id", "sub_category_title");

            return View(objFaqMasterModel);
        }

        /// <summary>
        /// Add Edit FAQ Master
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpPost]
        public ActionResult AddEditFAQMaster(FAQMasterModel objModel)
        {
            objFAQMasterService = new FAQMasterService();
            string _FileNameImage = string.Empty;
            if (ModelState.IsValid)
            {
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    if (objModel.faq_id > 0)
                    {
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQMasterService.AddEditFAQMaster(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.created_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQMasterService.AddEditFAQMaster(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                        {
                            transaction.Rollback();
                        }
                        return RedirectToAction("Index");
                    }
                }
            }
            else
                return View(objModel);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get FAQ Category
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        private List<SelectListItem> GetFAQCategory()
        {
            objFAQCategoryService = new FAQCategoryService();
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            IList<FAQCategoryModel> lstFaqMasterModel = new List<FAQCategoryModel>();
            try
            {
                lstFaqMasterModel = objFAQCategoryService.GetFaqCategories();
                foreach (var item in lstFaqMasterModel)
                {
                    var data = new SelectListItem { Text = item.category_title, Value = item.faq_category_id.ToString() };
                    lstSelectListItem.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstSelectListItem;
        }
        #endregion

        #region FAQ Delete
        /// <summary>
        /// FAQ Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objFAQMasterService = new FAQMasterService();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<FAQMasterModel> objFAQMasterList = new List<FAQMasterModel>();
            objFAQMasterList = objFAQMasterService.GetFAQMasterList(lIds);
            if (objFAQMasterList != null && objFAQMasterList.Count > 0)
            {
                objFAQMasterList.ForEach(x => x.modified_by = Convert.ToInt64(Session["UserID"].ToString()));
                bool bResult = objFAQMasterService.DeleteFAQs(objFAQMasterList);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "FAQMaster");
        }

        /// <summary>
        /// Delete  for checked
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult DeleteChecked(string sID)
        {
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            FAQMasterModel objFAQMasterModel = new FAQMasterModel();
            objFAQMasterService = new FAQMasterService();
            try
            {
                if (IDs != null)
                {

                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    IList<FAQMasterModel> objFAQMasterlist = objFAQMasterService.GetFAQMasterList(TagIds);
                    bool bResult = objFAQMasterService.DeleteFAQs(objFAQMasterlist);
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "FAQMaster");
        }
        #endregion

        #region Active/In Active 
        /// <summary>
        /// Active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            FAQMasterModel objFAQModel = new FAQMasterModel();
            List<Int64> lIds = new List<Int64>();
            objFAQMasterService = new FAQMasterService();
            lIds.Add(ID);

            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objFAQMasterService.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "FAQMaster");
        }

        /// <summary>
        /// In active Checked
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            FAQMasterModel objFAQMasterModel = new FAQMasterModel();
            List<Int64> lIds = new List<Int64>();
            objFAQMasterService = new FAQMasterService();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
            if (lIds != null && lIds.Count > 0)
            {
                bool bResult = objFAQMasterService.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "FAQMaster");
        }

        /// <summary>
        /// Active In Active Checked
        /// </summary>
        /// <param name="sID"></param>
        /// <param name="bIsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            objFAQMasterService = new FAQMasterService();
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    UserService ObjUserService = new UserService();
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objFAQMasterService.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = objFAQMasterService.ActiveInactive(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record In-Activated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Activated Successfully";
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "FAQMaster");
        }
        #endregion

        #region Upload Now
        public void uploadnow(HttpPostedFileWrapper upload)
        {
            if (upload != null)
            {
                string ImageName = upload.FileName;
                string path = System.IO.Path.Combine(Server.MapPath("~/faqmedia/"), ImageName);
                upload.SaveAs(path);
            }
        }

        public ActionResult uploadPartial()
        {
            var appData = Server.MapPath("~/faqmedia");
            var images = Directory.GetFiles(appData).Select(x => new imagesviewmodel
            {
                Url = Url.Content("/faqmedia/" + Path.GetFileName(x))
            });

            //ViewBag.Images = images;
            return View(images);
        }
        #endregion

        #region Arrange Ordering FAQ Page
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public ActionResult arrangementindex()
        {

            return View();
        }
        #endregion

        #region Arrange Ordering FAQ Page
        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public JsonResult GetFAQListForOrdering(string type, int id = 0)
        {
            FAQMasterService objFAQMasterService = new FAQMasterService();
            FAQSubCategoryService objFAQSubCategoryService = new FAQSubCategoryService();
            FAQCategoryService objFAQCategoryService = new FAQCategoryService();
            FAQOrderModel objFAQOrderModel = new FAQOrderModel();
            if (!string.IsNullOrEmpty(type))
            {
                switch (type)
                {
                    case "category":
                        List<FAQCategoryModel> lstCatFaqModel = objFAQCategoryService.GetFaqCategories();
                        // List<FAQCategoryModel> lstFaqModel = objFAQMasterService.GetFAQMasterList();
                        if (lstCatFaqModel.Count() > 0)
                        {
                            foreach (var item in lstCatFaqModel)
                            {
                                objFAQOrderModel.category.Add(new FAQCategoryOrderModel
                                {
                                    category_id = item.faq_category_id,
                                    category_order_sequence = item.order_sequence,
                                    category_title = item.category_title
                                });
                            }
                        }
                        break;
                    case "subcategory":
                        List<FAQSubCategoryModel> lstSubCatFaqModel = objFAQSubCategoryService.GetFAQSubCategoryList(id);
                        // List<FAQCategoryModel> lstFaqModel = objFAQMasterService.GetFAQMasterList();
                        if (lstSubCatFaqModel.Count() > 0)
                        {
                            foreach (var item in lstSubCatFaqModel)
                            {
                                objFAQOrderModel.subcategory.Add(new FAQSubCategoryOrderModel
                                {
                                    subcategory_id = item.faq_sub_category_id,
                                    subcategory_order_sequence = item.sub_category_order_sequence,
                                    subcategory_title = item.sub_category_title
                                });
                            }
                        }
                        break;
                    case "faq":
                        List<Int64> lId = new List<Int64>();
                        lId.Add(id);
                        List<FAQMasterModel> lstFaqModel = objFAQMasterService.GetFAQMasterList(lId);
                        if (lstFaqModel.Count() > 0)
                        {
                            foreach (var item in lstFaqModel)
                            {
                                objFAQOrderModel.faq.Add(new FAQQuestionOrderModel
                                {
                                    faq_id = item.faq_sub_category_id,
                                    faq_order_sequence = item.order_sequences,
                                    faq_title = item.question
                                });
                            }
                        }
                        break;
                }
                return Json(new
                {
                    result = true,
                    Data = objFAQOrderModel
                }, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region SAVE FAQ Order
        /// <summary>
        /// 
        /// </summary>
        /// <param name="catArray"></param>
        /// <param name="SubCatArray"></param>
        /// <param name="FAQArray"></param>
        /// <returns></returns>
        public JsonResult SAVEFAQOrder(string[] catArray, string[] SubCatArray, string[] FAQArray)
        {
            bool bResult = false;
            if (catArray != null)
            {
                FAQCategoryService objFAQCategoryService = new FAQCategoryService();
                bResult = objFAQCategoryService.UpdateCategoryFAQ(catArray);
            }
            if (SubCatArray != null)
            {
                FAQSubCategoryService objFAQSubCategoryService = new FAQSubCategoryService();
                bResult = objFAQSubCategoryService.UpdateSubCategoryFAQ(SubCatArray);
            }
            if (FAQArray != null)
            {
                FAQMasterService objFAQMasterService = new FAQMasterService();
                bResult = objFAQMasterService.UpdateFAQ(FAQArray);
            }
            if (bResult)
                return Json(new { result = true, Msgnew = "Order Save successfully" }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { result = false, Msgnew = "Something went wrong!" }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Sub Category
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult GetSubCategory(int id)
        {
            FAQSubCategoryService objFAQSubCategoryService = new FAQSubCategoryService();
            SelectList lstSubCategory = new SelectList(objFAQSubCategoryService.GetFAQSubCategoryList(id), "faq_sub_category_id", "sub_category_title");
            return Json(new SelectList(lstSubCategory, "value", "text"),JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}