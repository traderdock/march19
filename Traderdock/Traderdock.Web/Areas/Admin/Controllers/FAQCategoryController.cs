﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class FAQCategoryController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        FAQCategoryService objFAQCategoryService;
        #endregion

        #region FAQ Category Get
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<FAQCategoryModel> List = new List<FAQCategoryModel>();
            return View(List);
        }

        /// <summary>
        /// Get Faq Category for Page Load Kendo
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult FAQCategory_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            try
            {
                objFAQCategoryService = new FAQCategoryService();
                var FAQCategoryList = objFAQCategoryService.FAQCategoryList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
                FAQCategoryList.ForEach(x => x.category_image = Utilities.GetImagePath() + "/Images/" + (x.category_image));
                var gridModel = new DataSourceResult
                {
                    Data = FAQCategoryList,
                    Total = FAQCategoryList.TotalCount
                };
                return Json(gridModel);
            }
            catch (Exception ex)
            {
                throw  new ServiceLayerException(ex);
            }
        }
        #endregion

        #region FAQ Category Add/Edit
        /// <summary>
        /// Add Edit Faq Category
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AddEditFAQCategory(Int64? id)
        {
            objFAQCategoryService = new FAQCategoryService();
            FAQCategoryModel objFaqCategoryModel = new FAQCategoryModel();
            if (id > 0)
            {
                objFaqCategoryModel = objFAQCategoryService.GetFAQCategory(id);
                if (!string.IsNullOrEmpty(objFaqCategoryModel.category_image))
                {
                    objFaqCategoryModel.category_image = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objFaqCategoryModel.category_image);
                    objFaqCategoryModel.FileName = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objFaqCategoryModel.category_image);
                }
            }

            return View(objFaqCategoryModel);
        }

        /// <summary>
        /// Add Edit FAQ Category
        /// </summary>
        /// <param name="objModel"></param>
        /// <param name="file1"></param>
        /// <returns></returns>
        [ValidateInput(false)]
        [HttpPost]

        public ActionResult AddEditFAQCategory(FAQCategoryModel objModel, HttpPostedFileBase file1)
        {
            objFAQCategoryService = new FAQCategoryService();
            string FileNameImage = string.Empty;
            if (file1 != null)
            {
                ModelState.Remove("category_image");
            }
            if (ModelState.IsValid)
            {
                using (var transaction = dbConnection.Database.BeginTransaction())
                {
                    HttpPostedFileBase fileContent = file1;
                    FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (!string.IsNullOrEmpty(FileNameImage))
                    {
                        objModel.category_image = FileNameImage;
                    }
                    var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                    //if (System.IO.File.Exists(Path.Combine(_AttachmentPath, FileNameImage)) && FileNameImage != "Default_profile.png")
                    //{
                    //    System.IO.File.Delete(Path.Combine(_AttachmentPath, FileNameImage));
                    //}

                    if (objModel.faq_category_id > 0)
                    {
                        objModel.modified_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.modified_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQCategoryService.AddEditFAQCategory(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();

                        return RedirectToAction("Index");
                    }
                    else
                    {
                        objModel.created_by = Convert.ToInt64(Session["UserID"].ToString());
                        objModel.created_on = System.DateTime.UtcNow;
                        bool bFlag = objFAQCategoryService.AddEditFAQCategory(objModel);
                        if (bFlag)
                            transaction.Commit();
                        else
                            transaction.Rollback();
                        return RedirectToAction("Index");
                    }
                }
            }
            else
                return View(objModel);
        }
        #endregion

        #region FAQ Category Image Upload
        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        public ActionResult UploadFAQCategoryImage()
        {
            string _FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    _FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var _FileName = TempData["SelectedProfile"].ToString();
                        var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                        {
                            System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        }
                    }
                    TempData["SelectedProfile"] = _FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception _ex)
            {
                throw _ex;
            }
            return Json(new { Data = _FileNameImage }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region FAQ Catergory Delete
        /// <summary>
        /// Faq Category Delete
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objFAQCategoryService = new FAQCategoryService();
            if (ID > 0)
            {
                bool bResult = objFAQCategoryService.DeleteFAQCategory(ID, Convert.ToInt64(Session["UserID"].ToString()));
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
            }
            return RedirectToAction("Index", "FAQCategory");
        }
        #endregion

        //#region FAQ Catergory update
        ///// <summary>
        ///// Faq Category Delete
        ///// </summary>
        ///// <param name="ID"></param>
        ///// <returns></returns>
        //[HttpGet]
        //public ActionResult UpdateOrderSequence(int faq_category_id, int order_sequence)
        //{
        //    objFAQCategoryService = new FAQCategoryService();
        //    if (faq_category_id > 0 && order_sequence > 0)
        //    {
        //       // bool bResult = objFAQCategoryService.updateOrderFAQ(faq_category_id, order_sequence);
        //        if (bResult)
        //            return Json(new { success = true, Msgnew = "Order Sequence updated Successfully" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Record Deleted Successfully";
        //        else
        //            return Json(new { success = false, Msgnew = "Order Number already exist!" }, JsonRequestBehavior.AllowGet);//TempData["Msg"] = "Something Went Wrong!";
        //    }
        //    return RedirectToAction("Index", "FAQCategory");
        //}
        //#endregion
    }
}