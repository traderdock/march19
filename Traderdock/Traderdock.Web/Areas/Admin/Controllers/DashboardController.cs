﻿using Kendo.Mvc.UI;
using System;
using System.Web.Mvc;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class DashboardController : Controller
    {
        #region Global Variables
        /// <summary>
        /// Global Variables
        /// </summary>
        DashboardService objDashboardService;
        #endregion

        #region Dashboard
        /// <summary>
        /// Dashboard
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
        public ActionResult Dashboard()
        {
            objDashboardService = new DashboardService();
            DashboardModel dashboardModel = new DashboardModel();
            dashboardModel.TotalUser = objDashboardService.TotalActiveUserCount();
            dashboardModel.TotalFreeTrialUser = objDashboardService.TotalFreeTrialUserCount();
            dashboardModel.TotalSubscribedUser = objDashboardService.TotalSubscribedUserCount();
            dashboardModel.TodaysRevenue = objDashboardService.TotalTodaysRevenue();
            dashboardModel.TotalRevenue = objDashboardService.TotalRevenue();
            return View(dashboardModel);
        }
        #endregion

        #region User Read
        /// <summary>
        /// User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult User_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            UserService ObjUserService = new UserService();
            var users = ObjUserService.DashBoardUserList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);
            var gridModel = new DataSourceResult
            {
                Data = users,
                Total = users.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Notification
        /// <summary>
        /// Notifcation Read
        /// </summary>
        /// <returns></returns>
        public JsonResult Notification_Read()
        {
            objDashboardService = new DashboardService();
            try
            {
                long LogedUserID = Convert.ToInt64(Session["UserID"].ToString());
                var notification_data = objDashboardService.NotificationList(LogedUserID);
                return Json(new { success = true, Notification_data = notification_data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        /// <summary>
        /// Notification Mark Read
        /// </summary>
        /// <param name="NotificationID"></param>
        /// <returns></returns>
        public JsonResult Notification_Mark_Read(long NotificationID)
        {
            try
            {
                long LogedUserID = Convert.ToInt64(Session["UserID"].ToString());
                objDashboardService = new DashboardService();
                var notification_data = objDashboardService.NotificationRead(LogedUserID, NotificationID);
                objDashboardService = new DashboardService();

                (Session["NotificationCount"]) = objDashboardService.NotificationCount(LogedUserID);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Notification All Record
        /// </summary>
        /// <returns></returns>
        public JsonResult Notification_ALL_Readed()
        {
            try
            {
                long LogedUserID = Convert.ToInt64(Session["UserID"].ToString());
                DashboardService ObjDashboardService = new DashboardService();
                var notification_data = ObjDashboardService.Notification_ALL_Readed(LogedUserID);
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Public Method
        [HttpPost]
        public ActionResult Excel_Export_Save(string contentType, string base64, string fileName)
        {
            var fileContents = Convert.FromBase64String(base64);
            return File(fileContents, contentType, fileName);
        }
        #endregion
    }
}