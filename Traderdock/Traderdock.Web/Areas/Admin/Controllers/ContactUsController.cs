﻿using Kendo.Mvc.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class ContactUsController : Controller
    {
        #region Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService commonService = new CommonService();
        CountactUsServices objCountactUsServices = new CountactUsServices();
        #endregion

        #region Get Cotact Us
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Index page for Controller
        /// Feedback1:
        /// Feedback2:
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            List<ContactUsModel> List = new List<ContactUsModel>();
            return View(List);
        }

        /// <summary>
        /// Subscription Read
        /// </summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Contact Us Read
        /// Feedback1:
        /// Feedback2:
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        public JsonResult ContactUs_Read(DataSourceRequest model, string SearchFirstName = "", string statusFilter = "")
        {
            UserService ObjUserService = new UserService();
            var SubscriptionList = objCountactUsServices.ContactUsList(SearchFirstName, model.Page - 1, model.PageSize, statusFilter);

            var gridModel = new DataSourceResult
            {
                Data = SubscriptionList,
                Total = SubscriptionList.TotalCount
            };

            return Json(gridModel);
        }
        #endregion

        #region Delete Checked
        /// <summary>
        /// Date:  03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: delete Checked
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Delete(int ID)
        {
            objCountactUsServices = new CountactUsServices();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            List<ContactUsModel> lstContactUsModel = new List<ContactUsModel>();
            lstContactUsModel = objCountactUsServices.GetCotanctUsByList(lIds);
            if (lstContactUsModel != null && lstContactUsModel.Count > 0)
            {
                lstContactUsModel.ForEach(x => x.modified_by = Convert.ToInt32(Session["UserID"]));
                bool bResult = objCountactUsServices.DeleteContactUs(lstContactUsModel);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "ContactUs");
        }

        /// <summary>
        /// Date:  03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: delete Checked
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult DeleteChecked(string sID)
        {
            objCountactUsServices = new CountactUsServices();
            string IDs = sID.Replace(@"chkSelectAll,", @"");
            SubscriptionModel objSubscriptionModel = new SubscriptionModel();
            try
            {
                if (IDs != null)
                {
                    List<long> TagIds = IDs.Split(',').Select(long.Parse).ToList();
                    List<ContactUsModel> lstContactUsModel = objCountactUsServices.GetCotanctUsByList(TagIds);
                    bool bResult = objCountactUsServices.DeleteContactUs(lstContactUsModel);
                    //bool bResult = true;
                    if (bResult)
                        return Json(new { success = true, Msgnew = "Record Deleted Successfully" }, JsonRequestBehavior.AllowGet);
                    else
                        return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "Class");
        }
        #endregion

        #region Active/In Active Checked
        /// <summary>
        /// Resolved Checked
        /// </summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Active Checked
        /// Feedback1:
        /// Feedback2:
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveChecked(int ID)
        {
            objCountactUsServices = new CountactUsServices();
            ContactUsModel objContactUsModel = new ContactUsModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objCountactUsServices.ActiveInactive(lIds, LoginUserId, true);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record status updated successfully." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "ContactUs");
        }

        /// <summary>
        /// Pending Checked
        /// </summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Pending Checked
        /// Feedback1:
        /// Feedback2:
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult InActiveChecked(int ID)
        {
            objCountactUsServices = new CountactUsServices();
            ContactUsModel objContactUsModel = new ContactUsModel();
            List<Int64> lIds = new List<Int64>();
            lIds.Add(ID);
            long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());

            if (lIds != null && lIds.Count > 0)
            {

                bool bResult = objCountactUsServices.ActiveInactive(lIds, LoginUserId, false);
                if (bResult)
                    return Json(new { success = true, Msgnew = "Record status updated successfully." }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);
            }
            return RedirectToAction("Index", "ContactUs");
        }

        /// <summary>
        /// Active / In Active Checked
        /// </summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Active Checked
        /// Feedback1:
        /// Feedback2:
        /// <param name="ID"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ActiveInactiveChecked(string sID, bool bIsActiveRequest)
        {
            objCountactUsServices = new CountactUsServices();
            List<Int64> lIds = CommonFunc.GetIdsFromString(sID);
            try
            {
                long LoginUserId = Convert.ToInt64(Session["UserID"].ToString());
                if (lIds != null)
                {
                    bool bResult = false;
                    long UserID = Convert.ToInt64(Session["USerID"].ToString());
                    if (bIsActiveRequest)
                        bResult = objCountactUsServices.ActiveInactive(lIds, UserID, true);
                    else
                        bResult = objCountactUsServices.ActiveInactive(lIds, LoginUserId, false);
                    if (bIsActiveRequest)
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record status updated successfully." }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (bResult)
                            return Json(new { success = true, Msgnew = "Record status updated successfully." }, JsonRequestBehavior.AllowGet);
                    }
                    return Json(new { success = true, Msgnew = "Something Went Wrong!" }, JsonRequestBehavior.AllowGet);

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Index", "ContactUs");
        }
        #endregion
    }
}