﻿
using Kendo.Mvc.UI;
using System.Collections.Generic;
using System.Web.Mvc;
using Traderdock.ORM;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;
using Traderdock.Web.Areas.Admin.Authorization;

namespace Traderdock.Web.Areas.Admin.Controllers
{
    [Authorization]
    public class EnquiryController : Controller
    {
        #region  Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        EnquiryService objEnquiryService;
        #endregion

        
        /// <summary>
        /// Index
        /// </summary>
        /// <returns></returns>
        /// Date : 13-Nov-2017
        /// Dev By: Hardik Savaliya
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            objEnquiryService = new EnquiryService();
            List<UserMasterModel> List = new List<UserMasterModel>();
            var count = objEnquiryService.EnquiryList();
            return View(count);
        }

        /// <summary>
        /// Get User Read
        /// </summary>
        /// <param name="model"></param>
        /// <param name="SearchFirstName"></param>
        /// <param name="statusFilter"></param>
        /// <returns></returns>
        /// Date : 8-Aug-2017
        /// Dev By: Hardik  Savaliya
        public JsonResult Enquiry_Read(DataSourceRequest model, string SearchName = "")
        {
            objEnquiryService = new EnquiryService();
            var userEnquiry = objEnquiryService.EnquiryList(SearchName.ToLower(), model.Page - 1, model.PageSize,"",0);
            var gridModel = new DataSourceResult
            {
                Data = userEnquiry,
                Total = userEnquiry.TotalCount
            };
            return Json(gridModel);
        }

    }
}