﻿using System.Web;
using System.Web.Mvc;

namespace Traderdock.Web.Areas.Admin.Authorization
{
    public class AuthorizationAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (HttpContext.Current.Session["UserID"] == null)
            {
                filterContext.Result = new RedirectResult("~/admin/login");
                return;
            }
            base.OnActionExecuting(filterContext);
        }
    }
}