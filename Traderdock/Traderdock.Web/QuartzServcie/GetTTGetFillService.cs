﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.Model;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTGetFillService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sAccount"></param>
        /// <param name="sFromDate"></param>
        /// <param name="sToDate"></param>
        /// Dev By: Hardik Savaliya
        /// <returns></returns>
        public bool GetFills(long lAccoutId = 0, string sFromDate = "", string sToDate = "")
        {
            try
            {
                TTService objTTService = new TTService();
                LogServices objLogServices = new LogServices();
                GetTTInstrumentDataService objGetTTInstrumentDataService = new GetTTInstrumentDataService();
                CommonService objCommonService = new CommonService();
                TokenResponseClass objTokenResponseClass = new TokenResponseClass();
                objTokenResponseClass = objTTService.GetToken();
                List<FillTTResponse> lstFillTTResponse = new List<FillTTResponse>();
                List<FillTTResponse> lstFillTTResponseTmp = new List<FillTTResponse>();
                List<Int64> lstAccountNumber = new List<Int64>();
                if (lAccoutId > 0)
                {
                    lstAccountNumber.Add(lAccoutId);
                }
                else
                {
                    lstAccountNumber.AddRange(dbConnection.user_platform.Where(x => x.is_active == true && x.account_id > 0 && x.is_delete != true).Select(x => x.account_id).ToList());
                }
                StringBuilder sbItem = new StringBuilder();
                foreach (var item in lstAccountNumber)
                {
                    lstFillTTResponseTmp = new List<FillTTResponse>();
                    string sMaxTimeStamp = !string.IsNullOrEmpty(sFromDate) ? sFromDate : TraderdockTimeManager.DateTimeToUnixTimestamp(DateTime.UtcNow.Date.AddDays(-1));
                    string sMinTimeStamp = !string.IsNullOrEmpty(sToDate) ? sToDate : TraderdockTimeManager.DateTimeToUnixTimestamp(DateTime.UtcNow.Date.AddDays(-2));
                    // lstFillTTResponseTmp = objTTService.GetFII(objTokenResponseClass, item, "", "");
                    lstFillTTResponseTmp = objTTService.GetFII(objTokenResponseClass, item, sMaxTimeStamp, sMinTimeStamp);
                    sbItem.Append("(" + item + "," + lstFillTTResponseTmp.Count() + ")");
                    lstFillTTResponse.AddRange(lstFillTTResponseTmp);
                }
                List<tt_getfill_master> lstGetFillMaster = new List<tt_getfill_master>();
                if (lstFillTTResponse.Count > 0)
                {
                    List<tt_getfill_master> objTTGetFillMaster = dbConnection.tt_getfill_master.ToList();
                    foreach (var item in lstFillTTResponse)
                    {
                        List<tt_instrument_master> lstInstrumentMaster = dbConnection.tt_instrument_master.ToList();
                        ReDatabaseMapping:
                        if (objTTGetFillMaster.Any(x => x.record_id.Trim().ToLower().Equals(item.recordId.Trim().ToLower())))
                        {
                            APICallLogModel objAPICallLogErrorModel = new APICallLogModel();
                            objAPICallLogErrorModel.api_enum = (Int32)EnumAPI.RecordIdDuplicte;
                            objAPICallLogErrorModel.description = "Record id get duplicated on " + item.accountId + "(" + item.recordId + ")";
                            objAPICallLogErrorModel.endpoint = TTAPIEndPoints.GetFII;
                            objAPICallLogErrorModel.is_success = false;
                            bool bResultError = objLogServices.SaveAPILog(objAPICallLogErrorModel);
                        }
                        else
                        {
                            if (lstInstrumentMaster.Any(x => x.tt_instrument_id == item.instrumentId && !string.IsNullOrEmpty(x.alias) && x.displayFactor > 0))
                            {
                                tt_getfill_master objTTMaster = new tt_getfill_master();
                                objTTMaster.account = item.account;
                                objTTMaster.record_id = item.recordId;
                                objTTMaster.time_stamp = item.timeStamp;
                                objTTMaster.datetime_stamp = item.dtTimeStamp;
                                objTTMaster.instrument_id = item.instrumentId;
                                var objDisplayFactor = lstInstrumentMaster.Where(x => x.tt_instrument_id == item.instrumentId);
                                if (objDisplayFactor.Count() > 0)
                                {
                                    objTTMaster.contract = lstInstrumentMaster.Where(x => x.tt_instrument_id == item.instrumentId).FirstOrDefault().alias;
                                    objTTMaster.display_factor = objDisplayFactor.FirstOrDefault().displayFactor.Value;
                                }
                                objTTMaster.multi_leg_reporting_type = item.multiLegReportingType;
                                objTTMaster.synthetic_type = item.syntheticType;
                                objTTMaster.delta_qty = item.deltaQty;
                                objTTMaster.broker_id = Convert.ToInt32(item.brokerId);
                                objTTMaster.manual_order_indicator = item.manualOrderIndicator;
                                objTTMaster.security_desc = item.securityDesc;
                                objTTMaster.account = item.account;
                                objTTMaster.market_id = Convert.ToInt32(item.marketId);
                                objTTMaster.curr_user_id = item.currUserId;
                                //DateTime thisTime = DateTime.ParseExact(item.dttransactTime.TimeOfDay.ToString(), "HH:mm:ss", CultureInfo.InvariantCulture);
                                //Get Denmark Standard Time zone - not sure about that
                                objTTMaster.transact_time = item.dttransactTime.TimeOfDay;
                                //TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
                                //bool isDaylight = tst.IsDaylightSavingTime(thisTime);
                                //if (isDaylight)
                                //{
                                //    objTTMaster.transact_time = item.dttransactTime.TimeOfDay;
                                //}
                                //else
                                //{
                                //    objTTMaster.transact_time = thisTime.TimeOfDay;//.AddHours(5).AddMinutes(30).TimeOfDay;
                                //}
                                objTTMaster.last_qty = item.lastQty;
                                objTTMaster.account_id = Convert.ToInt64(item.accountId);
                                objTTMaster.avg_px  = item.avgPx;
                                objTTMaster.order_id = item.orderId;
                                objTTMaster.last_px = item.lastPx;
                                objTTMaster.exec_type = Convert.ToInt32(item.execType);
                                objTTMaster.cum_qty = item.lastQty;
                                objTTMaster.side = Convert.ToInt32(item.side);
                                objTTMaster.source = item.source;
                                if (objTTMaster.display_factor > 0)
                                    objTTMaster.price = item.avgPx * Convert.ToDecimal(objTTMaster.display_factor);
                                else
                                {
                                    APICallLogModel objAPICallLogModelInstrument = new APICallLogModel();
                                    objAPICallLogModelInstrument.api_enum = (Int32)EnumAPI.DisplayFactor;
                                    objAPICallLogModelInstrument.description = "Fills not saved due to this Display Factor: " + item.instrumentId + ". Account: " + item.accountId;
                                    objAPICallLogModelInstrument.endpoint = TTAPIEndPoints.GetFII;
                                    objAPICallLogModelInstrument.is_success = false;
                                    bool bInstrumentResult = objLogServices.SaveAPILog(objAPICallLogModelInstrument);
                                    break;
                                }
                                objTTMaster.created_on = DateTime.UtcNow;
                                lstGetFillMaster.Add(objTTMaster);
                            }
                            else
                            {
                                APICallLogModel objAPICallLogModelInstrument = new APICallLogModel();
                                objAPICallLogModelInstrument.api_enum = (Int32)EnumAPI.InstrumentNotFound;
                                objAPICallLogModelInstrument.description = "Fills not saved due to this instruments: " + item.instrumentId + ". Account: " + item.accountId;
                                objAPICallLogModelInstrument.endpoint = TTAPIEndPoints.GetFII;
                                objAPICallLogModelInstrument.is_success = false;
                                bool bInstrumentResult = objLogServices.SaveAPILog(objAPICallLogModelInstrument);
                                if (bInstrumentResult)
                                {
                                    objTTService = new TTService();
                                    bool bStatus = objGetTTInstrumentDataService.GetInstruments(item.instrumentId);
                                    if (bStatus)
                                    {
                                        goto ReDatabaseMapping;
                                    }
                                }
                            }
                        }
                    }
                     dbConnection.tt_getfill_master.AddRange(lstGetFillMaster);
                    dbConnection.SaveChanges();
                    List<string> lstString = new List<string>();
                    APICallLogModel objAPICallLogModel = new APICallLogModel();
                    objAPICallLogModel.api_enum = (Int32)EnumAPI.SaveGetFill;
                    objAPICallLogModel.description = "Fills Get Saved: " + sbItem.ToString();
                    objAPICallLogModel.endpoint = TTAPIEndPoints.GetFII;
                    objAPICallLogModel.is_success = true;
                    bool bResult = objLogServices.SaveAPILog(objAPICallLogModel);
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}