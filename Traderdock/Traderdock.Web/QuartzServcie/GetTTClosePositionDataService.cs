﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.ORM;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTClosePositionDataService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT Position
        public bool ClosePosition()
        {
            try
            {
                List<tt_position_master> lstTTPosition = new List<tt_position_master>();
                lstTTPosition = dbConnection.tt_position_master.Where(x => x.is_trading_closed ==false).ToList();
                lstTTPosition.ForEach(x => x.is_trading_closed = true);
                dbConnection.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }

        }
        #endregion
    }
}