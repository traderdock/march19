﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common.Exceptions;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTJobService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT data
        public bool UpdateProductList()
        {
            try
            {
                TTService objTTService = new TTService();
                TokenResponseClass objTokenResponseClass = new TokenResponseClass();
                objTokenResponseClass = objTTService.GetToken();
                List<TTProduct> lstTTProduct = new List<TTProduct>();
                lstTTProduct = objTTService.GetProducts(objTokenResponseClass);
                List<tt_product_master> lstTTProductRevise = new List<tt_product_master>();
                if (lstTTProduct != null)
                {
                    List<tt_product_master> objTTProductMaster = dbConnection.tt_product_master.ToList();
                    lstTTProductRevise = (from l1 in lstTTProduct
                                          where !(from l2 in objTTProductMaster
                                                  select l2.symbol.ToLower()).Contains(l1.symbol.ToLower())
                                          select new tt_product_master
                                          {
                                              id = l1.id,
                                              name = l1.name,
                                              familyId = l1.familyId,
                                              productTypeId = l1.productTypeId,
                                              symbol = l1.symbol,
                                              created_by = 0,
                                              modified_on = System.DateTime.UtcNow,
                                              modified_by = 0,
                                          }).ToList();
                    dbConnection.tt_product_master.AddRange(lstTTProductRevise);
                    dbConnection.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}