﻿using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTInstrumentData
    {
        public async void Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            await scheduler.Start();
            IJobDetail job = JobBuilder.Create<TTData>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("TraderDockInstruments", "TDInstrumentsData")
                .StartNow()
                .WithSimpleSchedule(s => s
                .WithIntervalInHours(40)
                .RepeatForever())
                .Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        [DisallowConcurrentExecution()]
        public class TTData : IJob
        {
            Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                Task taskA = new Task(() => Console.WriteLine("Hello from task at {0}", DateTime.Now.ToString()));
                taskA.Start();
                GetTTInstrumentDataService objService = new GetTTInstrumentDataService();
                objService.GetInstruments();
                return taskA;
            }
        }
    }
}