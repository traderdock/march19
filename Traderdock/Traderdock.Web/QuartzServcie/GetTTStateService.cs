﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTStateService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT data
        public bool UpdateProductList()
        {
            try
            {
                TTService objTTService = new TTService();
                TokenResponseClass objTokenResponseClass = new TokenResponseClass();
                objTokenResponseClass = objTTService.GetToken();
                StateResponseClass objStateResponseClass = new StateResponseClass();
                objStateResponseClass = objTTService.GetState(objTokenResponseClass);
                List<state_master> lstTTProductRevise = new List<state_master>();
                if (objStateResponseClass.states.Count > 0)
                {  
                    List<state_master> lstStateMaster = dbConnection.state_master.ToList();
                    lstTTProductRevise = (from l1 in objStateResponseClass.states
                                          where !(from l2 in lstStateMaster
                                                  select l2.state_code.ToLower()).Contains(l1.stateCode.ToLower())
                                          select new state_master
                                          {
                                             //


                                              modified_by = 0,
                                          }).ToList();
                    dbConnection.state_master.AddRange(lstTTProductRevise);
                    dbConnection.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}