﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common.Exceptions;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTAccountListJobService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT data
        public bool MapTTAccountNumber()
        {
            try
            {
                TTService objTTService = new TTService();
                return objTTService.TTAccountMapping();
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}