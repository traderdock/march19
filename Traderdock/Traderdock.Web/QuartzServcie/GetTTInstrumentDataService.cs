﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.Model;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTInstrumentDataService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get TT Instruments
        public bool GetInstruments(string sIntrumentsId = "")
        {
            try
            {
                TTService objTTService = new TTService();
                LogServices objLogService = new LogServices();
                TokenResponseClass objTokenResponseClass = new TokenResponseClass();
                objTokenResponseClass = objTTService.GetToken();
                List<InstrumentResponse> lstInstrumentResponse = new List<InstrumentResponse>();
                if (!string.IsNullOrEmpty(sIntrumentsId))
                {
                    lstInstrumentResponse = objTTService.GetInstrumentDetails(sIntrumentsId);
                }
                else
                {
                    lstInstrumentResponse = objTTService.GetInstruments(objTokenResponseClass);
                }
                List<tt_instrument_master> lstInstrumentsMaster = new List<tt_instrument_master>();
                Task.WaitAll();
                if (lstInstrumentResponse != null)
                {
                    lstInstrumentsMaster = dbConnection.tt_instrument_master.ToList();
                    var objData = lstInstrumentResponse.Where(p => !lstInstrumentsMaster.Any(l => l.tt_instrument_id.ToString().Equals(p.id.ToString())));
                    APICallLogModel objAPICallLogModel = new APICallLogModel();
                    objAPICallLogModel.api_enum = (Int32)EnumAPI.SaveInstruments;
                    StringBuilder sId = new StringBuilder();
                    foreach (var item in objData)
                    {
                        sId.Append(item.id + ",");
                    }
                    objAPICallLogModel.description = "Following list of Instrument need to save in database " + sId.ToString();
                    objAPICallLogModel.endpoint = TTAPIEndPoints.Instruments;
                    objAPICallLogModel.is_success = true;
                    bool bResult = objLogService.SaveAPILog(objAPICallLogModel);
                    foreach (var item in objData)
                    {
                        tt_instrument_master objTTInstrumentMaster = new tt_instrument_master();
                        objTTInstrumentMaster.alias = item.alias.ToString();
                        objTTInstrumentMaster.displayFactor = item.displayFactor;
                        objTTInstrumentMaster.expirationDate = item.expirationDate.ToString();
                        objTTInstrumentMaster.marketDepth = item.marketDepth.ToString();
                        objTTInstrumentMaster.marketId = item.marketId.ToString();
                        objTTInstrumentMaster.minLotSize = item.minLotSize.ToString();
                        objTTInstrumentMaster.name = item.name.ToString();
                        objTTInstrumentMaster.pointValue = item.pointValue.ToString();
                        objTTInstrumentMaster.alias = item.alias.ToString(); 
                        objTTInstrumentMaster.productId = item.productId.ToString();
                        objTTInstrumentMaster.productSymbol = item.productSymbol.ToString();
                        objTTInstrumentMaster.productTypeId = item.productTypeId.ToString();
                        objTTInstrumentMaster.roundLotQty = item.roundLotQty.ToString();
                        objTTInstrumentMaster.securityId = item.securityId.ToString();
                        objTTInstrumentMaster.seriesTermId = item.seriesTermId.ToString();
                        objTTInstrumentMaster.term = item.term.ToString();
                        objTTInstrumentMaster.tickSize = item.tickSize.ToString();
                        objTTInstrumentMaster.tt_instrument_id = item.id.ToString();
                        objTTInstrumentMaster.created_on = System.DateTime.UtcNow;
                        dbConnection.tt_instrument_master.Add(objTTInstrumentMaster);
                        dbConnection.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }

        }
        #endregion
    }
}