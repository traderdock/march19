﻿using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTPositionData
    {
        public async void Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            await scheduler.Start();
            IJobDetail job = JobBuilder.Create<TTData>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("TraderDockPositionData", "TDPositionData")
                .StartNow()
                .WithSimpleSchedule(s => s
                .WithIntervalInSeconds(2)
                .RepeatForever())
                .Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        [DisallowConcurrentExecutionAttribute()]
        public class TTData : IJob
        {
            Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                Task taskA = new Task(() => Console.WriteLine("Hello from task at {0}", DateTime.Now.ToString()));
                taskA.Start();
                GetTTPositionDataService objService = new GetTTPositionDataService();
                objService.GetPosition();
                return taskA;
            }
        }
    }
}