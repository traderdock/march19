﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class EndSubscriptionNotificationService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Fire Email
        public bool FireEmail()
        {
            IList<user_subscription> objUserSubscription = dbConnection.user_subscription.Where(x => x.subscription_end_date == System.DateTime.UtcNow.AddDays(-5)).ToList();
            foreach (var item in objUserSubscription)
            {
                if (item != null)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.Email, item.user_master.user_email);
                    new CommonService().SendEmail(item.user_master.user_email, EmailTemplateNames.EndUserSubscription, parameters, null);
                }
            }
            return true;
        }
        #endregion
    }
}