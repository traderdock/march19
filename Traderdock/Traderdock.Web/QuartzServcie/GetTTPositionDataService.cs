﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Threading.Tasks;
using Traderdock.Common.Exceptions;
using Traderdock.Model.TTModel;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    [DisallowConcurrentExecution]
    public class GetTTPositionDataService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        public string sTTAdminAccountID = System.Configuration.ConfigurationManager.AppSettings["TTAdminAccountID"];
        #endregion

        #region Get TT Position
        public bool GetPosition()
        {
            try
            {
                // CommonFunc.WriteLog("Position Called ", LogWriterType.Log);
                TTService objTTService = new TTService();
                TokenResponseClass objTokenResponseClass = new TokenResponseClass();
                objTokenResponseClass = objTTService.GetToken();
                List<TTPositionResponse> lstTTPositionResponse = new List<TTPositionResponse>();
                lstTTPositionResponse = objTTService.GetPosition(objTokenResponseClass);
                List<tt_position_master> lstPositionMaster = new List<tt_position_master>();
                Task.WaitAll();
                if (lstTTPositionResponse.Count > 0)
                {
                    if (!string.IsNullOrEmpty(sTTAdminAccountID))
                    {
                        List<string> lstRRAdmin = sTTAdminAccountID.Split(new char[] { ',' }).ToList();
                        if (lstRRAdmin.Count() > 0)
                        {
                            lstTTPositionResponse = lstTTPositionResponse.Where(x => !lstRRAdmin.Contains(x.accountId.ToString())).ToList();
                        }
                    }
                    foreach (var item in lstTTPositionResponse)
                    {
                        var currentDate = System.DateTime.Now.Date;
                        var objTTPositionMaster = (dbConnection.tt_position_master
                                                .Where(x => System.Data.Entity.DbFunctions.TruncateTime(x.trading_date) == currentDate.Date
                                                && x.instrumentId == item.instrumentId
                                                && x.is_trading_closed == false
                                                && x.accountId == item.accountId)
                                                ).FirstOrDefault();

                        if (objTTPositionMaster != null)
                        {
                            if (item.pnlPriceType.ToLower() == "last" || item.pnlPriceType.ToLower() == "midpoint" || item.pnlPriceType.ToLower() == "settle")
                            {
                                objTTPositionMaster.pnl = item.pnl;
                                objTTPositionMaster.pnlPrice = item.pnlPrice;
                                objTTPositionMaster.pnlPriceType = item.pnlPriceType;
                                objTTPositionMaster.realizedPnl = item.realizedPnl;
                                objTTPositionMaster.buyFillQty = item.buyFillQty;
                                objTTPositionMaster.sellFillQty = item.sellFillQty;
                                objTTPositionMaster.netPosition = item.netPosition;
                                objTTPositionMaster.buyWorkingQty = item.buyWorkingQty;
                                objTTPositionMaster.sellWorkingQty = item.sellWorkingQty;
                                objTTPositionMaster.avgBuy = item.avgBuy;
                                objTTPositionMaster.avgSell = item.avgSell;
                                objTTPositionMaster.openAvgPrice = item.openAvgPrice;
                                objTTPositionMaster.sodPrice = item.sodPrice;
                                objTTPositionMaster.sodPriceType = item.sodPriceType;
                                objTTPositionMaster.sodNetPos = item.sodNetPos;
                                objTTPositionMaster.is_trading_closed = false;
                                objTTPositionMaster.updated_on = System.DateTime.UtcNow;
                                if (objTTPositionMaster.heighest_pnl < item.pnl)
                                {
                                    objTTPositionMaster.heighest_pnl = item.pnl;
                                }
                                if (objTTPositionMaster.lowest_pnl > item.pnl)
                                {
                                    objTTPositionMaster.lowest_pnl = item.pnl;
                                }
                                dbConnection.SaveChanges();
                            }
                        }
                        else
                        {
                            var objUserData = (from UM in dbConnection.user_master
                                               join US in dbConnection.user_subscription
                                               on UM.user_id equals US.user_id into temp
                                               from t in temp.DefaultIfEmpty()
                                               where UM.account_number == item.accountId.ToString()
                                               where t.is_current == true
                                               select new
                                               {
                                                   user_id = UM.user_id,
                                                   user_subscription_id = t.user_subscription_id
                                               }).FirstOrDefault();
                            if (objUserData != null)
                            {
                                objTTPositionMaster = (new tt_position_master
                                {
                                    instrumentId = item.instrumentId,
                                    pnl = item.pnl,
                                    pnlPrice = item.pnlPrice,
                                    pnlPriceType = item.pnlPriceType,
                                    instrument_id = dbConnection.tt_instrument_master.Where(x => x.tt_instrument_id == item.instrumentId).FirstOrDefault().instrument_id,
                                    realizedPnl = item.realizedPnl,
                                    buyFillQty = item.buyFillQty,
                                    sellFillQty = item.sellFillQty,
                                    netPosition = item.netPosition,
                                    buyWorkingQty = item.buyWorkingQty,
                                    sellWorkingQty = item.sellWorkingQty,
                                    avgBuy = item.avgBuy,
                                    avgSell = item.avgSell,
                                    openAvgPrice = item.openAvgPrice,
                                    sodPriceType = item.sodPriceType,
                                    trading_date = DateTime.UtcNow,
                                    sodNetPos = item.sodNetPos,
                                    accountId = item.accountId,
                                    created_by = 0,
                                    heighest_pnl = item.pnl,
                                    lowest_pnl = item.pnl,
                                    sodPrice = item.sodPrice,
                                    user_id = objUserData.user_id,
                                    is_trading_closed = false,
                                    user_subscription_id = objUserData.user_subscription_id
                                });
                                dbConnection.tt_position_master.Add(objTTPositionMaster);
                                dbConnection.SaveChanges();
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}