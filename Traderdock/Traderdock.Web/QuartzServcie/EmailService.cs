﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.QuartzServcie
{
    public class EmailService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Fire Email
        public bool FireEmail()
        {
            //First get an email of Instrument API success. Just a confirmation fired after that process complete. 
            //Store a 1 somewhere in database to say successful instrument API today.
            //Get Fill Main email to include:
            //1.GetFill API Success notification[or fail with reason]
            //2.Upload Success Notification[or fail with reason]
            //3.Instrument ID API Success noted from database above.
            //4.Summary Performance Report Table.
            try
            {
                DateTime dt = DateTime.Now.Date;
                List<api_call_log> objAPILog = dbConnection.api_call_log.Where(x => DbFunctions.TruncateTime(x.created_on).Value.Equals(dt)).ToList();
                StringBuilder sMailContent = new StringBuilder();
                sMailContent.Append("Daily API Log for " + DateTime.UtcNow.Date);
                sMailContent.Append("===========================================================================================================<br/>");
                foreach (var item in objAPILog.Where(x => x.api_enum == (Int32)EnumAPI.FetchGetFill))
                {
                    sMailContent.Append("Get FILL API Notification");
                    sMailContent.Append(item.description);
                    item.is_sent = true;
                    sMailContent.Append("===========================================================================================================<br/>");
                }
                foreach (var item in objAPILog.Where(x => x.api_enum == (Int32)EnumAPI.SaveGetFill))
                {
                    sMailContent.Append("Save FILL API Notification");
                    sMailContent.Append(item.description);
                    item.is_sent = true;
                    sMailContent.Append("===========================================================================================================<br/>");
                }
                foreach (var item in objAPILog.Where(x => x.api_enum == (Int32)EnumAPI.FetchInstruments))
                {
                    sMailContent.Append("Fectch Instrument Notification");
                    sMailContent.Append(item.description);
                    item.is_sent = true;
                    sMailContent.Append("===========================================================================================================<br/>");
                }
                foreach (var item in objAPILog.Where(x => x.api_enum == (Int32)EnumAPI.SaveInstruments))
                {
                    sMailContent.Append("Save Instrument Notification");
                    sMailContent.Append(item.description);
                    item.is_sent = true;
                    sMailContent.Append("===========================================================================================================<br/>");
                }
                sMailContent.Append("Performance Report");
                sMailContent.Append("Daily API Log for " + DateTime.UtcNow.Date);
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add(EmailTemplateParameters.Email, "david@traderdock.com");
                parameters.Add(EmailTemplateParameters.MailContent, sMailContent.ToString());
                new CommonService().SendEmail("support@traderdock.com", EmailTemplateNames.DailyAPILog, parameters, null);
                dbConnection.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
          
        }
        #endregion
    }
}