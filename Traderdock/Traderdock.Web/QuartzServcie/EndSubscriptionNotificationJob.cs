﻿using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace Traderdock.Web.QuartzServcie
{
    public class EndSubscriptionNotificationJob
    {
        public async void Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            await scheduler.Start();
            IJobDetail job = JobBuilder.Create<SubscriptionNotificationJob>().Build();
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("Traderdock")
                .WithCronSchedule("0 0/1 1-2 * * ?")
                .StartAt(DateTime.UtcNow)
                .WithPriority(1)
                .Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        [DisallowConcurrentExecutionAttribute()]
        public class SubscriptionNotificationJob : IJob
        {
            Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                Task taskA = new Task(() => Console.WriteLine("Hello from task at {0}", DateTime.Now.ToString()));
                taskA.Start();
                EndSubscriptionNotificationService objService = new EndSubscriptionNotificationService();
                objService.FireEmail();
                return taskA;
            }
        }

    }

}