﻿using Quartz;
using Quartz.Impl;
using System;
using System.Threading.Tasks;

namespace Traderdock.Web.QuartzServcie
{
    public class GetTTClosePositionData
    {
        public async void Start()
        {
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            IScheduler scheduler = await schedulerFactory.GetScheduler();
            await scheduler.Start();
            IJobDetail job = JobBuilder.Create<TTData>().Build();
            ITrigger trigger = TriggerBuilder.Create()
               .WithIdentity("TraderdockClosePosition")
               .WithCronSchedule("0 0/55 22-23 * * ?")
               .StartAt(DateTime.UtcNow)
               .WithPriority(1)
               .Build();
            await scheduler.ScheduleJob(job, trigger);
        }

        [DisallowConcurrentExecutionAttribute()]
        public class TTData : IJob
        {
            Task IJob.Execute(IJobExecutionContext context)
            {
                if (context == null)
                {
                    throw new ArgumentNullException(nameof(context));
                }
                Task taskA = new Task(() => Console.WriteLine("Hello from task at {0}", DateTime.Now.ToString()));
                taskA.Start();
                GetTTClosePositionDataService objService = new GetTTClosePositionDataService();
                objService.ClosePosition();
                return taskA;
            }
        }
    }
}