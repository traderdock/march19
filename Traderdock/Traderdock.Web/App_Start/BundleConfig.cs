﻿using System.Web;
using System.Web.Optimization;

namespace Traderdock.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-2.2.3.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/others").Include(
                      "~/Scripts/jquery-ui-1.12.1.js",
                      "~/Content/plugins/bootstrap-fileinput/js/bootstrap-fileinput.js",
                      "~/Content/plugins/fastclick/fastclick.js",
                      "~/Scripts/app.min.js",
                      "~/Content/plugins/sparkline/jquery.sparkline.min.js",
                      "~/Content/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Content/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                      "~/Content/plugins/slimScroll/jquery.slimscroll.min.js",
                      "~/Content/plugins/chartjs/Chart.min.js",
                      "~/Content/plugins/bootstrap-toastr/toastr.js",
                      "~/Content/plugins/datepicker/bootstrap-datepicker.js",
                      "~/Scripts/bootstrap.min.js",
                      "~/Content/plugins/iCheck/icheck.min.js"
                      ));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap/css/bootstrap.css",
                "~/Content/plugins/datepicker/datepicker3.css",
                "~/Content/plugins/daterangepicker/daterangepicker.css",
                "~/Content/dist/css/skins/_all-skins.min.css",
                "~/Content/plugins/iCheck/flat/blue.css",
                "~/Content/plugins/morris/morris.css",
                "~/Content/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                "~/Content/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css",
                "~/Content/dist/css/AdminLTE.css",
                "~/Content/plugins/bootstrap-toastr/toastr.css",
                "~/Content/plugins/iCheck/square/blue.css",
                "~/Content/plugins/iCheck/all.css",
                "~/Content/dist/css/custom.css"
            ));

            bundles.Add(new StyleBundle("~/bundles/kendocss").Include(
              "~/Content/plugins/Kendo/kendo.common.min.css",
              "~/Content/plugins/Kendo/kendo.metro.min.css",
              "~/Content/front/plugins/select2/css/select2.css"
            ));

            bundles.Add(new ScriptBundle("~/bundles/kendojs").Include(
            "~/Content/plugins/Kendo/kendo.web.min.js",
            "~/Content/plugins/Kendo/jszip.min.js",
            "~/Content/plugins/Kendo/kendo.all.min.js",
            "~/Content/front/plugins/select2/js/select2.min.js",
            "~/Content/front/plugins/select2/js/select2.full.min.js"
            ));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
            "~/Content/bootstrap/js/bootstrap.min.js"
            ));
            BundleTable.EnableOptimizations = false;
        }
    }
}
