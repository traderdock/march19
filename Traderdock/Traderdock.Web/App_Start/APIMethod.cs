﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace Traderdock.Web.App_Start
{
    public class APIMethod
    {
        private static readonly string _rootEndPoint = ConfigurationManager.AppSettings["PayPalVault"].ToString().EndsWith("/") ?
          ConfigurationManager.AppSettings["PayPalVault"].ToString() : ConfigurationManager.AppSettings["PayPalVault"].ToString() + "/";
        public sealed class PayPalVault
        {
            private static string _TokenEndPoint = "oauth2";
            private static string _Vault = "vault";
            public static string GetToken = _rootEndPoint + _TokenEndPoint + "/token";
            public static string SaveCard= _rootEndPoint + _Vault + "/credit-cards";
        }
    }
}