
// Fixed Sidebar
//$(document).ready(function() {

    //$('.navbar').scrollToFixed();

//});

// Select 2
$('.select-single').select2();

// Main Height
function manage_height() {
    var w_height = $( window ).height();
    var navbar_height = $('.navbar').height();
    var footer_height = $('.footer').height();
    //var banner_height = $('.banner').height();
    var main_height=w_height-footer_height-navbar_height-1;
    $('.main-container').css('min-height',main_height);
}

$( window ).ready(function () {
    manage_height();
});
$( window ).resize(function() {
    manage_height();
});

// Help
$(document).ready(function(){
    $(".helpHeader-toggle").click(function(){
        $(this).parent().toggleClass("active");
    });
});
$('body').click(function() {
   $(".helpHeader-toggle").parent().removeClass("active");
});

$('.helpHeader').click(function(event){
   event.stopPropagation();
});

// Date/Time Picker (https://eonasdan.github.io/bootstrap-datetimepicker/)
$('.datepickershow').datepicker({
  //autoclose: true,
  format: "dd/mm/yyyy"
});

//Timepicker
$(".timepickershow").timepicker({
  showInputs: false,
  showMeridian: false
});

// Modal Button Click 
$('.modal-open-click').click(function(){
    setTimeout(function(){ $('body').addClass("modal-open"); }, 400);
});

/* ********************************************************************************************
   SmoothScroll
*********************************************************************************************** */

$(function() {
  $('.smoothScroll').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000); // The number here represents the speed of the scroll in milliseconds
        return false;
      }
    }
  });
});