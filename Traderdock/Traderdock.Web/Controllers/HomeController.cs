using System.Web.Mvc;
using Traderdock.Services;
using Traderdock.Entity.MemberModel;
using System;
using System.Collections.Generic;
using Traderdock.Entity.ViewModel;
using Traderdock.Common.Enumerations;
using Traderdock.ORM;
using System.Linq;
using Traderdock.Common;
using System.Text;
using System.IO;
using Renci.SshNet;
using Traderdock.Entity.Model;
using System.Web.Security;
using System.Configuration;
using Traderdock.Common.Constants;
using System.Web;
using Traderdock.Model.MemberModel;
using Traderdock.Common.Exceptions;

namespace Traderdock.Web.Controllers
{
    public class HomeController : BaseController
    {
        #region Global Variables
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CMSService objCMSService;
        SubscriptionService objSubscriptionService;
        MemberService objMemberService;
        SettingsService objSettingsService;
        DashboardService objDashboardService;
        CommonService objCommonService;
        #endregion

        #region Home/Index Page CMS Data
        /// <summary>
        /// Get All CMS Contant For Home/Index Page 
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index()
        {
            try
            {
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                objHomePageModel.TestimonialList = objCMSService.GetTestimonial();
                //objHomePageModel.GlobalTeamList = objCMSService.GetGlobalTeam();
                //For social links in footer - by bharat
                string Social_content = string.Empty;
                Social_content = objCMSService.GetSocialCMS();
                Session["SocialLinksData"] = Social_content;

                int userID = Convert.ToInt32(Session["MemberID"]);
                var count = dbConnection.user_notifications.Where(m => m.user_id == userID && m.notification_status != true).Count();
                //ViewBag.UserNotificationCount = count;
                Session["UserNotificationCount"] = count;

                if (objHomePageModel.FooterCMS != null)
                {
                    Session["FooterData"] = objHomePageModel.FooterCMS;
                    Session["IsFooterData"] = true;
                }

                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                SubscriptionService objSubscriptionService = new SubscriptionService();
                List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
                Subscription_List = objSubscriptionService.SubscriptionListForFront();
                Session["front_subscription_id"] = Subscription_List[0].subscription_id;
                Session["front_starting_balance"] = Subscription_List[0].starting_balance;
                Session["front_price_per_month"] = Subscription_List[0].price_per_month;
                return View(objHomePageModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Get All CMS Contant For Home/Index Page 
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Index1()
        {
            try
            {
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();

                //For social links in footer - by bharat
                string Social_content = string.Empty;
                Social_content = objCMSService.GetSocialCMS();
                Session["SocialLinksData"] = Social_content;

                int userID = Convert.ToInt32(Session["MemberID"]);
                var count = dbConnection.user_notifications.Where(m => m.user_id == userID && m.notification_status != true).Count();
                //ViewBag.UserNotificationCount = count;
                Session["UserNotificationCount"] = count;

                if (objHomePageModel.FooterCMS != null)
                {
                    Session["FooterData"] = objHomePageModel.FooterCMS;
                    Session["IsFooterData"] = true;
                }

                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objHomePageModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Dash board
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult DashBoard(string transaction_id = "", string PaymentIsSuccess = "", string saccountallias = "")
        {
            if (Session["MemberID"] != null)
            {
                int userID = Convert.ToInt32(Session["MemberID"]);
                var count = dbConnection.user_notifications.Where(m => m.user_id == userID && m.notification_status != true).Count();
                ViewBag.UserNotificationCount = count;
                string Sequence = "";
                Int64 userSubscriptionID = 0;
                UserService objUserService = new UserService();
                NotificationService objNotificationService = new NotificationService();
                DashboardService ObjDashboardService = new DashboardService();
                WEBDashboardModel model = new WEBDashboardModel();
                SubscriptionService objSubscriptionService = new SubscriptionService();
                LearnHowModel objLearnHowModel = new LearnHowModel();
                model.objLearnHowModel = objLearnHowModel;
                List<SelectListItem> List_All = new List<SelectListItem>();
                long count_1 = 1;
                int ShowDashboard = 0;
                int GraphDisplay = 0;
                int iTradingPlatform = dbConnection.user_master.Where(x => x.user_id == userID).FirstOrDefault().trading_platform.Value;
                if (iTradingPlatform == (Int32)EnumTradingPlatform.Rithmic)
                {
                    model.ObjHomeModel = ObjDashboardService.GetUserAccountBalanceNew(userID, Sequence, userSubscriptionID);
                    model.objAccountInfoModel = ObjDashboardService.GetAccountInfoNew(userID, Sequence, userSubscriptionID);
                    //by shoaib - for dashboard status - 15-Sep-2017
                    var UserSubList = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                    int UserTrnCount = dbConnection.user_transactions.Where(x => UserSubList.Contains(x.user_subscription_id)).ToList().Count;
                    ViewBag.UserTrnCount = UserTrnCount;
                    model.ObjRuleModel = ObjDashboardService.GetRuleNew(userID);

                    var ResetCount = (from UT in dbConnection.reset_requests
                                      join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                      where US.user_id == userID
                                      select UT).ToList();

                    if (UserTrnCount > 0)
                    {
                        ShowDashboard = 1;
                    }
                    else
                    {
                        ShowDashboard = 0;
                    }
                    //For dynamically bind an Exchage to view
                    //var tempdata = dbConnection.user_transactions.Select(m => m.exchange).Distinct().ToList();
                    var tempdata = (from UT in dbConnection.user_transactions
                                    join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                    where US.user_id == userID
                                    select new { UT.exchange, UT.symbol }).Distinct().ToList();
                    foreach (var item in tempdata)
                    {
                        List_All.Add(new SelectListItem
                        {
                            Text = item.exchange + ": " + item.symbol,
                            //Value = count_1.ToString()
                            Value = item.exchange,
                            Selected = item.exchange == "All Markets" ? true : false
                        });
                        count_1++;
                    }

                    if (tempdata.Count > 0)
                    {
                        GraphDisplay = 1;
                    }
                    model.objUserMasterModel.trading_platform = (Int32)EnumTradingPlatform.Rithmic;
                    model.objAccountInfoModel.account_number_lst = new SelectList(objUserService.GetUserAccountList(userID), "account_number", "account_number");
                }
                else if (iTradingPlatform == (Int32)EnumTradingPlatform.TradingTechnologies)
                {
                    model.ObjHomeModel = ObjDashboardService.TTGetUserAccountBalance(userID, Sequence, userSubscriptionID, saccountallias);
                    model.objAccountInfoModel = ObjDashboardService.GetAccountInfoNew(userID, Sequence, userSubscriptionID, saccountallias);
                    var UserSubList = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                    int UserTrnCount = dbConnection.tt_fill_master_manual.Where(x => UserSubList.Contains(x.user_subscription_id.Value)).ToList().Count;
                    ViewBag.UserTrnCount = UserTrnCount;
                    model.ObjRuleModel = ObjDashboardService.TTGetRule(userID, "", 0, saccountallias);
                    var ResetCount = (from UT in dbConnection.reset_requests
                                      join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                      where US.user_id == userID
                                      select UT).ToList();
                    if (UserTrnCount > 0)
                    {
                        ShowDashboard = 1;
                    }
                    else
                    {
                        ShowDashboard = 0;
                    }
                    ViewBag.ShowDashboard = ShowDashboard;
                    var tempdata = (from UT in dbConnection.tt_fill_master_manual
                                    join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                    where US.user_id == userID
                                    select new { UT.contract }).Distinct().ToList();
                    if (tempdata.Count > 0)
                    {
                        GraphDisplay = 1;
                    }
                    if (!string.IsNullOrEmpty(saccountallias))
                    {
                        model.objAccountInfoModel.account_number = saccountallias;
                    }
                    model.objUserMasterModel.trading_platform = (Int32)EnumTradingPlatform.TradingTechnologies;
                    model.objAccountInfoModel.account_number_lst = new SelectList(objUserService.GetUserAccountList(userID), "account_alias", "account_alias");
                }
                ViewBag.ShowDashboard = ShowDashboard;
                //model.UserTransactionList = ObjDashboardService.GetUserTransactionList(userID);
                model.objUserSubscriptionList = objSubscriptionService.GetUserSubscriptionsList1(userID);
                //SubscriptionModel
                //model.objCurrentbalChartList = ObjDashboardService.GetCurrentBalChartList(userID);
                objNotificationService.sendNotification(userID); // For Auto Notification Send
                ViewBag.IsUserSubscribed = dbConnection.user_subscription.Where(x => x.user_id == userID).Count();
                ViewBag.ddlExchange = List_All;
                // End Exchange View
                // No need to display graphs when there is no transactions for that user
                ViewBag.GraphDisplay = GraphDisplay;
                if (transaction_id != null && !string.IsNullOrWhiteSpace(transaction_id)
                    && PaymentIsSuccess != null && !string.IsNullOrWhiteSpace(PaymentIsSuccess))
                {
                    model.objLearnHowModel.transaction_id = transaction_id;
                    model.objLearnHowModel.PaymentIsSuccess = PaymentIsSuccess;
                    ViewBag.Message = AlliedWalletSettings.SuccessTransactionMessage.Replace("{0}", transaction_id);
                }
                else
                {
                    ViewBag.Message = "Payment has been unsuccessful. Please follow the link to retry.";
                }
                List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
                Subscription_List = objSubscriptionService.SubscriptionListForFront();
                foreach (var Subcription in Subscription_List)
                {
                    if (Subcription.Status)
                    {
                        PlanMemModel objPlanList = new PlanMemModel();
                        objPlanList.subscription_name = Subcription.subscription_name;
                        objPlanList.subscription_id = Subcription.subscription_id;
                        objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                        objPlanList.max_position_size = Subcription.max_position_size.ToString();
                        objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                        objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                        objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                        objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                        model.Subscription_List.Add(objPlanList);
                    }
                }

                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        #region Get Current Balance
        /// <summary>
        /// Get Current Balance
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 17/7/2017    getnetpllist
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ActionResult GetCurrentBalList(string Sequence, string Exchange = "", Int64 SubscriptionID = 0, Int32 Platform = 0, string saccountallias = "")
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            WEBDashboardModel model = new WEBDashboardModel();
            ServiceResponse<List<CurrentbalChart>> response = new ServiceResponse<List<CurrentbalChart>>();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                DashboardService objDashboardService = new DashboardService();
                if (Platform == (Int32)EnumTradingPlatform.Rithmic)
                {
                    response = ObjDashboardService.GetCurrentBalanceList(userID, Sequence, Exchange, SubscriptionID);
                }
                else if (Platform == (Int32)EnumTradingPlatform.TradingTechnologies)
                {
                    response = ObjDashboardService.GetTTCurrentBalanceList(userID, Sequence, Exchange, SubscriptionID, saccountallias);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult AccountInfo(string Sequence, Int64 SubscriptionID = 0)
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            //WEBDashboardModel model = new WEBDashboardModel();
            AccountInfo Model = new AccountInfo();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                Model = ObjDashboardService.GetAccountInfo(userID, Sequence, SubscriptionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { success = true, AccountInfo_ = Model }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccountBalance(string Sequence, Int64 SubscriptionID = 0)
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            HomePageModel Model = new HomePageModel();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                Model = ObjDashboardService.GetUserAccountBalance(userID, Sequence, SubscriptionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(new { success = true, AccountBal_ = Model }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AccountRule(string Sequence, Int64 SubscriptionID = 0)
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            Entity.MemberModel.RuleModel Model = new Entity.MemberModel.RuleModel();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                Model = ObjDashboardService.GetRule(userID, Sequence, SubscriptionID);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Json(new { success = true, AccountRule_ = Model }, JsonRequestBehavior.AllowGet);
        }

        #region Get Net P/L
        /// <summary>
        /// Get Net P/L
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ActionResult GetNetPLList(string Sequence, string Exchange = "", Int64 SubscriptionID = 0, Int32 Platform = 0, string saccountallias = "")
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            WEBDashboardModel model = new WEBDashboardModel();
            ServiceResponse<List<NetPlScript>> response = new ServiceResponse<List<NetPlScript>>();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                if (Platform == (Int32)EnumTradingPlatform.Rithmic)
                {
                    response = ObjDashboardService.GetNetPLList(userID, Sequence, Exchange, SubscriptionID);
                    //added on 20-Sept-2018 Hardik
                    // if value arrive in negative else blank chart not working 
                    if (response.Result != null)
                    {
                        if (response.Result.Count() == 1 || response.Result.Count() == 0)
                        {
                            NetPlScript obj = new NetPlScript();
                            obj.Script = "";
                            obj.net_profit_loss = 0;
                            response.Result.Insert(0, obj);
                        }
                    }
                }
                else if (Platform == (Int32)EnumTradingPlatform.TradingTechnologies)
                {
                    response = ObjDashboardService.GetTTNetPLList(userID, Sequence, Exchange, SubscriptionID, saccountallias);
                    //added on 20-Sept-2018 Hardik
                    // if value arrive in negative else blank chart not working 
                    if (response.Result != null)
                    {
                        if (response.Result.Count() == 1 || response.Result.Count() == 0)
                        {
                            NetPlScript obj = new NetPlScript();
                            obj.Script = "";
                            obj.net_profit_loss = 0;
                            response.Result.Insert(0, obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Get Total Trades
        /// <summary>
        /// Get Total Trades
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ActionResult GetTotalTradesList(string Sequence, string Exchange = "", Int64 SubscriptionID = 0, Int32 Platform = 0, string saccountallias = "")
        {
            int userID = Convert.ToInt32(Session["MemberID"]);
            WEBDashboardModel model = new WEBDashboardModel();
            ServiceResponse<List<TotalTrade>> response = new ServiceResponse<List<TotalTrade>>();
            try
            {
                DashboardService ObjDashboardService = new DashboardService();
                if (Platform == (Int32)EnumTradingPlatform.Rithmic)
                {
                    response = ObjDashboardService.GetTradeLList(userID, Sequence, Exchange, SubscriptionID);
                }
                else if (Platform == (Int32)EnumTradingPlatform.TradingTechnologies)
                {
                    response = ObjDashboardService.GetTTTradeLList(userID, Sequence, Exchange, SubscriptionID, saccountallias);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Json(response.Result, JsonRequestBehavior.AllowGet);
        }
        #endregion

        /// <summary>
        /// Get Data For Footer if Footer is null
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public JsonResult Footer()
        {
            try
            {
                string Footer_content = string.Empty;
                objCMSService = new CMSService();
                Footer_content = objCMSService.GetHomeCMSFooter();
                Session["FooterData"] = Footer_content;
                Session["IsFooterData"] = true;
                return Json(new { success = true, Footer_data = Footer_content }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Traderdock Learn How Steps
        /// <summary>
        /// Setp1 - Traderdock Learn How
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        /// Changed by Shoaib Mijaki on client request (removed all 3 steps and one common page instead 3 different pages
        /// 22-Feb-2018
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult learnhow(string transaction_id = "", string PaymentIsSuccess = "")
        {
            try
            {
                LearnHowModel objLearnHowModel = new LearnHowModel();
                objCMSService = new CMSService();
                objSubscriptionService = new SubscriptionService();
                objLearnHowModel = objCMSService.GetLearnMore(1);
                CommonService objCommonService = new CommonService();
                objLearnHowModel.Subscription_List = new List<PlanMemModel>();
                #region Get Subscription List
                List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
                //Changed by shoaib to hide free subscription from the list - 14Sep2017
                //Subscription_List = objSubscriptionService.SubscriptionList();
                Subscription_List = objSubscriptionService.SubscriptionListForFront();
                #endregion
                foreach (var Subcription in Subscription_List)
                {
                    if (Subcription.Status)
                    {
                        PlanMemModel objPlanList = new PlanMemModel();
                        objPlanList.subscription_name = Subcription.subscription_name;
                        objPlanList.subscription_id = Subcription.subscription_id;
                        objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                        objPlanList.max_position_size = Subcription.max_position_size.ToString();
                        objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                        objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                        objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                        objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                        objLearnHowModel.Subscription_List.Add(objPlanList);
                    }
                }
                objLearnHowModel.SubscriptionPlanList = new SelectList(objLearnHowModel.Subscription_List, "subscription_id", "subscription_name");
                objLearnHowModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objLearnHowModel.StateList = new SelectList("", 0, "Select State");
                objLearnHowModel.transaction_id = transaction_id;
                objLearnHowModel.PaymentIsSuccess = PaymentIsSuccess;

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                //remove hard fix 
                //need to look
                objLearnHowModel.Step1Slider = objCMSService.GetCMS(323).cms_content;
                objLearnHowModel.Step2Slider = objCMSService.GetCMS(327).cms_content;
                objLearnHowModel.Step3Slider = objCMSService.GetCMS(328).cms_content;
                return View(objLearnHowModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Setp1 - Traderdock Learn How
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step1(string transaction_id = "", string PaymentIsSuccess = "")
        {
            try
            {
                LearnHowModel objLearnHowModel = new LearnHowModel();
                objCMSService = new CMSService();
                objSubscriptionService = new SubscriptionService();
                objLearnHowModel = objCMSService.GetLearnMore(1);
                CommonService objCommonService = new CommonService();
                objLearnHowModel.Subscription_List = new List<PlanMemModel>();
                #region Get Subscription List
                List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();

                //Changed by shoaib to hide free subscription from the list - 14Sep2017
                //Subscription_List = objSubscriptionService.SubscriptionList();
                Subscription_List = objSubscriptionService.SubscriptionListForFront();
                #endregion
                foreach (var Subcription in Subscription_List)
                {
                    if (Subcription.Status)
                    {
                        PlanMemModel objPlanList = new PlanMemModel();
                        objPlanList.subscription_name = Subcription.subscription_name;

                        objPlanList.subscription_id = Subcription.subscription_id;
                        objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                        objPlanList.max_position_size = Subcription.max_position_size.ToString();
                        objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                        objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                        objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                        objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                        objLearnHowModel.Subscription_List.Add(objPlanList);
                    }
                }
                objLearnHowModel.SubscriptionPlanList = new SelectList(Subscription_List, "subscription_id", "subscription_name");
                objLearnHowModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objLearnHowModel.StateList = new SelectList("", 0, "Select State");
                objLearnHowModel.transaction_id = transaction_id;
                objLearnHowModel.PaymentIsSuccess = PaymentIsSuccess;

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objLearnHowModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Setp2 - Traderdock Learn How
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step2()
        {
            try
            {
                LearnHowModel objLearnHowModel = new LearnHowModel();
                objCMSService = new CMSService();
                objLearnHowModel = objCMSService.GetLearnMore(2);

                HomePageModel objHomePageModel = new HomePageModel();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objLearnHowModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Setp3 - Traderdock Learn How
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Step3()
        {
            try
            {
                LearnHowModel objLearnHowModel = new LearnHowModel();
                objCMSService = new CMSService();
                objLearnHowModel = objCMSService.GetLearnMore(3);

                HomePageModel objHomePageModel = new HomePageModel();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objLearnHowModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region About Us CMS
        /// <summary>
        /// About Us CMS Page Get Data 
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult AboutUs()
        {
            try
            {
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.AboutUs);

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objCMSDataModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Contact Us CMS
        /// <summary>
        /// Contact Us CMS Page Get Data
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public ActionResult ContactUs()
        {
            try
            {
                ContactUsModel objContactUsModel = new ContactUsModel();
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.ContactUs);
                objContactUsModel.cms_contact_us = objCMSDataModel.cms_contact_us;
                objContactUsModel.BannerTextContactUs = objCMSDataModel.BannerTextContactUs;
                objContactUsModel.BannerImages = objCMSDataModel.BannerImages;

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objContactUsModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Request Data Save From Countact us form
        /// </summary>
        /// <param name="objContactUsModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ContactUs(ContactUsModel objContactUsModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    CountactUsServices objCountactUsServices = new CountactUsServices();
                    using (var transaction = dbConnection.Database.BeginTransaction())
                    {
                        bool bFlag = objCountactUsServices.AddContactUs(objContactUsModel);
                        if (bFlag)
                        {
                            transaction.Commit();
                            TempData["ContactUsSuccessMsg"] = "Message sent successfully.";
                        }
                        else
                        {
                            TempData["ContactUsErrorMsg"] = "Please try agian after some time.";
                            transaction.Rollback();
                        }

                        return RedirectToAction("ContactUs");
                    }
                }
                else
                    return View(objContactUsModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Satisfaction Policy CMS
        /// <summary>
        /// Satisfaction Policy CMS Page Get Data
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult SatisfactionPolicy()
        {
            try
            {
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.SatisfactionPolicy);

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objCMSDataModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Privacy Policy CMS
        /// <summary>
        /// Privacy Policy CMS Page Get Data
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult PrivacyPolicy()
        {
            try
            {
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.PrivacyPolicy);
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }

                return View(objCMSDataModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Terms And Conditions CMS
        /// <summary>
        /// Terms And Conditions CMS Page Get Data
        /// </summary>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Terms()
        {
            try
            {
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.TermsOfService);

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objCMSDataModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region SocialLink CMS
        /// <summary>
        /// Privacy Policy CMS Page Get Data
        /// </summary>
        /// <returns></returns>
        public ActionResult SocialLink()
        {
            try
            {
                CMSDataModel objCMSDataModel = new CMSDataModel();
                objCMSService = new CMSService();
                objCMSDataModel = objCMSService.GetCMSFromTypeID((Int32)EnumCMSType.PrivacyPolicy);

                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objCMSDataModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Public Method
        public JsonResult GetPlanDetails(int id)
        {
            objSubscriptionService = new SubscriptionService();
            var SubscriptionPlan = objSubscriptionService.GetSubscription(id);
            return Json(new { success = true, SubscriptionPlan = SubscriptionPlan }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Date: 09-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Create Profile Ajax Partial view
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult CallProfile(long? id = 0)
        {
            objCommonService = new CommonService();
            objSubscriptionService = new SubscriptionService();
            MemberModel objMemberModel = new MemberModel();
            #region Get Subscription List
            List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();

            //Changed by shoaib to hide free subscription from the list - 14Sep2017
            Subscription_List = objSubscriptionService.SubscriptionListForFront();
            //Subscription_List = objSubscriptionService.SubscriptionList();

            foreach (var Subcription in Subscription_List)
            {
                if (Subcription.Status)
                {
                    PlanMemModel objPlanList = new PlanMemModel();
                    objPlanList.subscription_name = Subcription.subscription_name;
                    objPlanList.subscription_id = Subcription.subscription_id;
                    objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                    objPlanList.max_position_size = Subcription.max_position_size + " Lot";
                    objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                    objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                    objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                    objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                    objMemberModel.Subscription_List.Add(objPlanList);
                }
            }
            #endregion
            objMemberModel.SubscriptionPlanList = new SelectList(objMemberModel.Subscription_List, "subscription_id", "subscription_name");
            objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            if (id > 0)
                objMemberModel.subscription_id = id.Value;
            else
                objMemberModel.subscription_id = 1;
            return PartialView("createProfileModal", objMemberModel);
        }

        /// <summary>
        /// Date: 09-November-2017
        /// Dev By: Hardik Savaliya
        /// Description: Reset User Data
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult CallResetUserData(int id)
        {
            MemberModel objMemberModel = new MemberModel();
            objMemberModel.user_id = id;
            return PartialView("callResetConfirmationModel", objMemberModel);
        }

        /// <summary>
        /// Date: 11-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Review Subscription model Ajax Partial view
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult CallReviewModel(int id)
        {
            objCommonService = new CommonService();
            objSubscriptionService = new SubscriptionService();
            MemberModel objMemberModel = new MemberModel();
            #region Get Subscription List
            List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();

            //Changed by shoaib to hide free subscription from the list - 14Sep2017
            //Subscription_List = objSubscriptionService.SubscriptionList();
            Subscription_List = objSubscriptionService.SubscriptionListForFront();

            foreach (var Subcription in Subscription_List)
            {
                if (Subcription.Status)
                {
                    PlanMemModel objPlanList = new PlanMemModel();
                    objPlanList.subscription_name = Subcription.subscription_name;

                    objPlanList.subscription_id = Subcription.subscription_id;
                    objPlanList.starting_balance = "$ " + (Int32)(Subcription.starting_balance / 1000) + "k";
                    objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                    objPlanList.max_position_size = Subcription.max_position_size + " Lot";
                    objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                    objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                    objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                    objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                    objMemberModel.Subscription_List.Add(objPlanList);
                }
            }
            #endregion
            objMemberModel.SubscriptionPlanList = new SelectList(Subscription_List, "subscription_id", "subscription_name");

            objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            objMemberModel.subscription_id = id;

            return PartialView("reviewSubscriptionModal", objMemberModel);
        }

        /// <summary>
        /// Date: 10-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Create Payment Ajax Partial view
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult CallPayment(int id, int sid, int pid = 0)
        {
            PaypalModel objPaypalModel = new PaypalModel();
            objPaypalModel.user_id = id;
            objPaypalModel.promo_code_id = pid;
            objPaypalModel.subscription_id = sid;
            TempData["addPaymentModal"] = true;
            objPaypalModel.PayPalMurchantID = System.Web.Configuration.WebConfigurationManager.AppSettings["PayPal:UserID"];
            objPaypalModel.PayPalEnvirement = System.Web.Configuration.WebConfigurationManager.AppSettings["PayPal:environment"];
            return PartialView("addPaymentModal", objPaypalModel);
        }

        /// <summary>
        /// Date: 10-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Create Payment Ajax Partial view
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult CallResetPayment(int id)
        {
            PaypalModel objPaypalModel = new PaypalModel();
            objPaypalModel.user_id = id;
            objPaypalModel.PayPalMurchantID = System.Web.Configuration.WebConfigurationManager.AppSettings["PayPal:UserID"];
            objPaypalModel.PayPalEnvirement = System.Web.Configuration.WebConfigurationManager.AppSettings["PayPal:environment"];
            return PartialView("resetPaymentModal", objPaypalModel);
        }
        #endregion

        #region User Notification list
        /// <summary>
        /// User Notification List
        /// </summary>
        /// <returns></returns>
        /// Date : 11/7/2017
        /// Dev By: Bharat Katua
        public JsonResult UserNotification(int userID = 0)
        {
            NotificationService ObjUserService = new NotificationService();
            List<NotificationMasterModel> model = new List<NotificationMasterModel>();
            model = ObjUserService.GetNotificationList(userID);
            return Json(model);
        }
        #endregion

        #region Upload Member Data into CSV
        /// <summary>
        /// Upload Member Data into CSV
        /// </summary>
        /// dev by : Bharat Katua
        /// date : 25/07/2017
        /// <param name="userID"></param>
        public ActionResult UploadCSVData(int userID = 0, bool IsLoginRedirected = false)
        {
            bool IsLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"].ToString());
            //var filePath = Server.MapPath("~") + "\\Files\\add_user_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + userID + ".csv";
            var filePath = Server.MapPath("~") + "\\Files\\add_user_" + userID + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            MemberService objService = new MemberService();
            MemberModel objModel = new MemberModel();

            StringBuilder sb = new StringBuilder();
            objModel = objService.GetDataForCSV(userID);

            //sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", objModel.user_email, objModel.first_name, objModel.last_name, objModel.address, objModel.city_name, objModel.country_name, objModel.state_name, objModel.postal_code, objModel.contact_number.ToString(), objModel.password) + Environment.NewLine);
            //System.IO.File.AppendAllText(filePath, sb.ToString());
            //by shoaib
            //for street address related change (remove extra , from address as it will considered as separate field)
            char[] CharSeparator = new char[1];
            CharSeparator[0] = ',';
            if (!String.IsNullOrEmpty(objModel.address))
            {
                string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                objModel.address = strStreetAddress;
            }
            //  string strStreetAddress = objModel.address.Split(CharSeparator)[0];
            string StrContactNumber = objModel.contact_number;
            if (objModel.contact_number == "NA")
                StrContactNumber = "44 7700 900708";

            //Feedback (10-Oct-2017) 
            //Added extra field at EOD for subscription number in the file uploaded to Rithmic
            //By Shoaib

            if (objModel.country_name.Trim().ToLower() != "usa" && objModel.country_name.Trim().ToLower() != "canada")
            {
                objModel.state_name = "";
            }

            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", objModel.user_email.Trim(), objModel.first_name.Trim(), objModel.last_name.Trim(), objModel.address.Trim(), objModel.city_name.Trim(), objModel.country_name.Trim(), objModel.state_name.Trim(), objModel.postal_code.Trim(), StrContactNumber.Trim(), objModel.password.Trim(), objModel.subscription_id.ToString()));
            System.IO.File.AppendAllText(filePath, sb.ToString());
            //for copy file one path to another
            string fileName = Path.GetFileName(filePath);
            string sourcePath = Server.MapPath("~") + "\\Files";
            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
            #region upload file on sftp
            var host = "ritpz11300.11.rithmic.com";
            var port = 22;
            var username = "u3CiKQPhV";
            var password = "xBWKWRLW";

            string remoteRootDirectory;
            if (IsRidhmicTestEnv)
            {
                remoteRootDirectory = @"test/new_user/";
            }
            else
            {
                remoteRootDirectory = @"production/new_user/";
            }

            try
            {
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    //client.ChangeDirectory(@"test/in_flight/");   // for change directory on sftp
                    //client.ChangeDirectory(@"test/new_user/");   // for change directory on sftp
                    client.ChangeDirectory(remoteRootDirectory);   // for change directory on sftp

                    //string oldpath = @"test/in_flight/add_user_141.csv" + fileName;
                    //string newpath = @"test/new_user/add_user_141.csv" + fileName;

                    if (IsLive)
                    {
                        if (client.IsConnected)
                        {
                            using (var fileStream = new FileStream(sourceFile, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(sourceFile));
                            }

                            //client.RenameFile(oldpath, newpath); // for move file on sftp
                        }
                    }
                }

                if (IsLoginRedirected)
                {
                    objMemberService = new MemberService();
                    objSettingsService = new SettingsService();
                    MemberModel ModelForDashboard = new MemberModel();
                    ModelForDashboard = objMemberService.GetUser(objModel.user_id);
                    SettingModel objSettingModel = new SettingModel();
                    objSettingModel = objSettingsService.GetSettingByName("PageSize");
                    if (objSettingModel.setting_value != null)
                    {
                        Session["PageSize"] = objSettingModel.setting_value;
                    }
                    //Session["User"] = objMemberModel.user_name;
                    Session["MemberID"] = ModelForDashboard.user_id;
                    Session["MemberEmail"] = ModelForDashboard.user_email;

                    // by Bharat NotificationCount 11 / 07 / 2017
                    Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                    var count = dbConnection.user_notifications.Where(m => m.user_id == ModelForDashboard.user_id).Count();
                    Session["UserNotificationCount"] = count;
                    Session["UserIDForNotification"] = ModelForDashboard.user_id;

                    DashboardService objDashboardService = new DashboardService();
                    Session["NotificationCount"] = objDashboardService.NotificationCount(ModelForDashboard.user_id);
                    Session["MemberName"] = ModelForDashboard.first_name;
                    if (!string.IsNullOrEmpty(ModelForDashboard.profile_image_url))
                    {
                        Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(ModelForDashboard.profile_image_url);
                    }
                    else
                    {
                        Session["MemberImage"] = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                    }

                    FormsAuthentication.SetAuthCookie(ModelForDashboard.user_email, false);
                    try
                    {
                        if (!string.IsNullOrEmpty(ModelForDashboard.user_email))
                        {
                            Traderdock_DBEntities DbCon = new Traderdock_DBEntities();
                            var tmpQuery = (from UM in DbCon.user_master
                                            where UM.user_id == ModelForDashboard.user_id && UM.is_active == true
                                            select UM).First();

                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            parameters.Add(EmailTemplateParameters.Email, ModelForDashboard.user_email);
                            parameters.Add(EmailTemplateParameters.FullName, ModelForDashboard.first_name);
                            parameters.Add(EmailTemplateParameters.Password, Utilities.DecryptPassword(tmpQuery.password, tmpQuery.crypto_key));//1May2018 - for adding password to email
                            //string emailattachment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["attachmentemail"]);

                            // Client Feedback - separate email templates for free and paid subscriptions
                            // By shoaib - 10-Oct-2017
                            // Changed template from "Welcome email" to "Welcome email free"
                            //new CommonService().SendEmail(ModelForDashboard.user_email, EmailTemplateNames.WelcomeMail, parameters, null);
                            new CommonService().SendEmail(ModelForDashboard.user_email, EmailTemplateNames.WelcomePaidSubscription, parameters, null);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw new ServiceLayerException(ex);
                    }
                }

                //changed by shoaib - 5Aug2017
                //for proper account subscription message
                SubscriptionService objSubscriptionService = new SubscriptionService();
                SubscriptionModel objSubscrModel = objSubscriptionService.GetSubscription(Convert.ToInt64(TempData["SubscriptionId"]));

                //TempData["ModelTitle"] = objSubscrModel.subscription_name;
                if (objSubscrModel.subscription_id == 8)
                {
                    TempData["ModelTitle"] = objSubscrModel.subscription_name;
                    TempData["ModelText"] = "Your 14 - Days Free Trial account is successfully created!!!";
                }
                else
                {
                    TempData["ModelTitle"] = "Payment Successful";
                    //TempData["ModelText"] = "Your " + objSubscrModel.subscription_name + " account is successfully created!!!";
                    TempData["ModelText"] = "Thank you for subscribing to the TraderDock Challenge. Your TraderDock account has been successfully created.Please check your email for instructions on how to get started.";
                }

                TempData["IsFreeTrialActivated"] = '2';
                //


                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                //throw ex;
            }
            #endregion
        }
        #endregion

        [AllowAnonymous]
        public JsonResult GetStates(int id)
        {
            CommonService objCommonService = new CommonService();
            SelectList lstStates = new SelectList(objCommonService.GetStateList(id), "state_id", "state_name");
            return Json(new SelectList(lstStates, "value", "text"));
        }

        #region Notification
        /// <summary>
        /// Notifcation Read
        /// </summary>
        /// <returns></returns>
        public JsonResult Notification_Read()
        {
            objDashboardService = new DashboardService();
            try
            {
                long LogedUserID = Convert.ToInt64(Session["MemberID"].ToString());
                var notification_data = objDashboardService.NotificationList(LogedUserID);
                var count = dbConnection.user_notifications.Where(m => m.user_id == LogedUserID && m.notification_status != true).Count();
                Session["UserNotificationCount"] = count;
                return Json(new { success = true, Notification_data = notification_data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Notification Mark As A Read from notification dropdown
        /// </summary>
        /// <param name="NotificationID"></param>
        /// <returns></returns>
        public JsonResult Notification_Mark_Read(long NotificationID)
        {
            try
            {
                long LogedUserID = Convert.ToInt64(Session["MemberID"].ToString());
                objDashboardService = new DashboardService();
                var notification_data = objDashboardService.NotificationRead(LogedUserID, NotificationID);
                objDashboardService = new DashboardService();

                var count = dbConnection.user_notifications.Where(m => m.user_id == LogedUserID && m.notification_status != true).Count();
                //Session["UserNotificationCount"] = count;
                return Json(new { success = true, notificationCount = count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Notification All Record
        /// </summary>
        /// <returns></returns>
        public JsonResult Notification_ALL_Readed()
        {
            try
            {
                long LogedUserID = Convert.ToInt64(Session["MemberID"].ToString());
                DashboardService ObjDashboardService = new DashboardService();
                var notification_data = ObjDashboardService.Notification_ALL_Readed(LogedUserID);
                var count = dbConnection.user_notifications.Where(m => m.user_id == LogedUserID && m.notification_status != true).Count();
                Session["UserNotificationCount"] = count;
                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public JsonResult Notification_count()
        {
            try
            {
                long LogedUserID = Convert.ToInt64(Session["MemberID"].ToString());
                var count = dbConnection.user_notifications.Where(m => m.user_id == LogedUserID && m.notification_status != true).Count();
                //Session["UserNotificationCount"] = count;
                return Json(new { success = true, notificationCount = count }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region User Transaction list
        /// <summary>
        /// User Transaction List
        /// </summary>
        /// <returns></returns>
        /// Date : 10/8/2017
        /// Dev By: Bharat Katua
        public JsonResult UserTransaction(string Sequence, string Exchange = "", Int64 SubscriptionID = 0, Int32 Platform = 0, string saccountallias = "")
        {
            long LogedUserID = Convert.ToInt64(Session["MemberID"]);
            WEBDashboardModel model = new WEBDashboardModel();
            DashboardService ObjDashboardService = new DashboardService();
            long NextSubId = 0;
            if (Platform == (Int32)EnumTradingPlatform.Rithmic)
            {
                model.UserTransactionList = ObjDashboardService.GetUserTransactionList(out NextSubId, Sequence, LogedUserID, Exchange, SubscriptionID);
            }
            else if (Platform == (Int32)EnumTradingPlatform.TradingTechnologies)
            {
                model.UserTransactionList = ObjDashboardService.GetTTUserTransactionList(out NextSubId, Sequence, LogedUserID, Exchange, SubscriptionID, saccountallias);
            }
            return Json(new { success = true, List = model.UserTransactionList, user_sub_id = NextSubId }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Accept Privacy Policy Cookies
        /// <summary>
        ///  Accept Privacy Policy Cookies
        /// </summary>
        /// <returns></returns>
        /// Date : 26/03/2018
        /// Dev By: Hardik Savaliya
        public JsonResult AcceptPrivacyPolicyCookies()
        {
            try
            {
                HttpCookie cookie = Request.Cookies["AcceptPrivacyPolicyTraderdock"];
                if (cookie == null)
                {
                    Response.Cookies["AcceptPrivacyPolicyTraderdock"].Value = "true";
                    Response.Cookies["AcceptPrivacyPolicyTraderdock"].Expires = DateTime.UtcNow.AddDays(1);
                    return Json(new { success = true }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public ActionResult CallPaymentReviewModel(int id)
        {
            objCommonService = new CommonService();
            objSubscriptionService = new SubscriptionService();
            MemberModel objMemberModel = new MemberModel();
            #region Get Subscription List
            List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
            //Changed by shoaib to hide free subscription from the list - 14Sep2017
            //Subscription_List = objSubscriptionService.SubscriptionList();
            Subscription_List = objSubscriptionService.SubscriptionListForFront();
            foreach (var Subcription in Subscription_List)
            {
                if (Subcription.Status)
                {
                    PlanMemModel objPlanList = new PlanMemModel();
                    objPlanList.subscription_name = Subcription.subscription_name;
                    objPlanList.subscription_id = Subcription.subscription_id;
                    objPlanList.starting_balance = "$ " + (Int32)(Subcription.starting_balance / 1000) + "k";
                    objPlanList.starting_balance = "$" + (Int32)(Subcription.starting_balance / 1000) + "K";
                    objPlanList.max_position_size = Subcription.max_position_size + " Lot";
                    objPlanList.daily_loss_limit = "$" + Convert.ToDecimal(Subcription.daily_loss_limit).ToString("#,##0");
                    objPlanList.max_drawdown = "$" + Convert.ToDecimal(Subcription.max_drawdown).ToString("#,##0");
                    objPlanList.profite_target = "$" + Convert.ToDecimal(Subcription.profite_target).ToString("#,##0");
                    objPlanList.price_per_month = "$" + Convert.ToDecimal(Subcription.price_per_month).ToString("#,##0");
                    objMemberModel.Subscription_List.Add(objPlanList);
                }
            }
            #endregion
            objMemberModel.SubscriptionPlanList = new SelectList(Subscription_List, "subscription_id", "subscription_name");
            objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            objMemberModel.subscription_id = id;
            return PartialView("reviewPaymentModal", objMemberModel);
        }

        #region Admin Dashboard
        /// <summary>
        /// Dash board
        /// </summary>
        /// <returns></returns>
        /// Date : 14/03/2019
        /// Dev By: Hardik Savaliya
        [Authorize]
        public ActionResult AdminDashBoard(string saccountallias = "")
        {
            WEBDashboardModel objWebDashboardModel = new WEBDashboardModel();
            UserService objUserService = new UserService();
            DashboardService ObjDashboardService = new DashboardService();
            SettingModel objSettingModel = new SettingModel();
            objWebDashboardModel.account_number_lst = new SelectList(objUserService.GetUserAccountNumberList(), "account_alias", "full_name_with_alias");
            if (!string.IsNullOrEmpty(saccountallias))
            {
                long lUserId = objUserService.GetUserViaAccountAllias(saccountallias);
                if (lUserId > 0)
                {
                    UserMasterModel objUserMasterModel = objUserService.GetUser(lUserId);
                    Session["MemberID"] = objUserMasterModel.user_id;
                    Session["MemberEmail"] = objUserMasterModel.user_email;
                    Session["MemberName"] = objUserMasterModel.first_name;
                    if (!string.IsNullOrEmpty(objUserMasterModel.profile_image_url))
                    {
                        Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objUserMasterModel.profile_image_url);
                    }
                    else
                    {
                        Session["MemberImage"] = Convert.ToString(ConfigurationManager.AppSettings["DefaultImageURL"]);
                    }
                    string Sequence = "";
                    Int64 userSubscriptionID = 0;
                    NotificationService objNotificationService = new NotificationService();
                    SubscriptionService objSubscriptionService = new SubscriptionService();
                    LearnHowModel objLearnHowModel = new LearnHowModel();
                    objWebDashboardModel.objLearnHowModel = objLearnHowModel;
                    List<SelectListItem> List_All = new List<SelectListItem>();
                    int ShowDashboard = 0;
                    if (objUserMasterModel.trading_platform == (Int32)EnumTradingPlatform.TradingTechnologies)
                    {
                        objWebDashboardModel.ObjHomeModel = ObjDashboardService.TTGetUserAccountBalance(lUserId, Sequence, userSubscriptionID, saccountallias);
                        objWebDashboardModel.objAccountInfoModel = ObjDashboardService.GetAccountInfoNew(lUserId, Sequence, userSubscriptionID, saccountallias);
                        var UserSubList = dbConnection.user_subscription.Where(x => x.user_id == lUserId).Select(x => x.user_subscription_id).ToList();
                        int UserTrnCount = dbConnection.tt_fill_master_manual.Where(x => UserSubList.Contains(x.user_subscription_id.Value)).ToList().Count;
                        ViewBag.UserTrnCount = UserTrnCount;
                        objWebDashboardModel.ObjRuleModel = ObjDashboardService.TTGetRule(lUserId, "", 0, saccountallias);
                        var ResetCount = (from UT in dbConnection.reset_requests
                                          join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                          where US.user_id == lUserId
                                          select UT).ToList();
                        if (UserTrnCount > 0)
                        {
                            ShowDashboard = 1;
                        }
                        else
                        {
                            ShowDashboard = 0;
                        }
                        ViewBag.ShowDashboard = ShowDashboard;
                        if (!string.IsNullOrEmpty(saccountallias))
                        {
                            objWebDashboardModel.objAccountInfoModel.account_number = saccountallias;
                        }
                        objWebDashboardModel.objUserMasterModel.trading_platform = (Int32)EnumTradingPlatform.TradingTechnologies;
                        objWebDashboardModel.objAccountInfoModel.account_number_lst = new SelectList(objUserService.GetUserAccountNumberList(), "account_alias", "account_alias");
                        ViewBag.ShowDashboard = ShowDashboard;
                    }
                }
            }
            return View(objWebDashboardModel);
        }
        #endregion
    }
}