﻿using System;
using System.Web.Mvc;
using Traderdock.Entity.Model;
using Traderdock.ORM;
using Traderdock.Services;

namespace Traderdock.Web.Controllers
{
    public class IndexController : Controller
    {

        #region Global Variables
        CommonService objCommonService;
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion
        // GET: Auth
        public ActionResult index()
        {
            LaunchModel objLoginModel = new LaunchModel();
            return View(objLoginModel);
        }

        /// <summary>
        /// Requested email enqiury 
        /// </summary>
        /// <param name="objContactUsModel"></param>
        /// <returns></returns>
        /// Date : 20-Nov-2017
        /// Dev By:Hardik Savaliya
        [HttpPost]
        public ActionResult Enquiry(LaunchModel objHomePageModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (!string.IsNullOrEmpty(objHomePageModel.EnquiryEmail))
                    {
                        objCommonService = new CommonService();
                        using (var transaction = dbConnection.Database.BeginTransaction())
                        {
                            bool bFlag = objCommonService.SaveEnquiryList(objHomePageModel.EnquiryEmail);
                            if (bFlag)
                            {
                                TempData["EnquirySuccessMsg"] = "Thanks for subscribing. We will update soon!";
                            }
                            else
                            {
                                TempData["EnquiryErrorMsg"] = "Please try agian after some time.";
                            }
                            return RedirectToAction("Index");
                        }
                    }
                    else
                    {
                        TempData["ContactUsErrorMsg"] = "Please try agian after some time.";
                        return RedirectToAction("Index");
                    }
                }
                else
                    return RedirectToAction("index");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    
    }
}