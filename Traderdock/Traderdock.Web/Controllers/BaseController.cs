﻿using System;
using System.Configuration;
using System.Web.Mvc;
using System.Web.Routing;

namespace Traderdock.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            //IsAuthorised();
        }

        public ActionResult IsAuthorised()
        {
            string SiteLink = ConfigurationManager.AppSettings["SiteLink"];
            string StrAction =  this.ControllerContext.RouteData.Values["action"].ToString();
            try
            {
                if (!string.IsNullOrEmpty(Session["IsAuthorised"] as string))
                {
                    if (Session["IsAuthorised"].ToString().ToUpper() == "TRUE" || StrAction.ToLower() == "activate")
                    {
                        //Response.Redirect(SiteLink + "/Account/Login", false);
                        //return RedirectToAction("SubscriptionList", "Subscription");
                    }                  
                    else
                    {
                        Response.Redirect(SiteLink + "/auth/lock", false);
                    }
                }
                else if (StrAction.ToLower() == "activate")
                {
                    //do nothing
                    //no activation needed when activate method is called
                }
                else
                {
                    Response.Redirect(SiteLink + "/Auth/Lock", false);
                }
            }
            catch (Exception)
            {
                Response.Redirect(SiteLink + "/Auth/Lock", false);
            }
            return null;
        }
    }
}