﻿using Newtonsoft.Json;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.MemberModel;
using Traderdock.ORM;
using Traderdock.Services;
using Traderdock.Web.App_Start;


namespace Traderdock.Web.Controllers
{
    public class PayController : BaseController
    {
        /// <summary>
        /// Date: 01-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Pay To Paypal
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public JsonResult PayToPaypal(PaypalModel objPaypalModel)//,string PaymentType, long user_id,long subscription_id)
        {
            PayService objPayService = new PayService();
            PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
            PayPalOrder objPay = new PayPalOrder();
            try
            {
                if (objPaypalModel.user_id > 0 && objPaypalModel.subscription_id > 0 && ModelState.IsValid)
                {
                    PaypalModel objModel = new PaypalModel();

                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                    objModel = objPaypalPaymentService.PaypalPaymentfirst(objPaypalModel);
                    if (objModel.is_success)
                    {
                        if (objPaypalModel.issave == true)
                        {
                            var Data = SaveCardtoVault(objPaypalModel).ConfigureAwait(false);
                            TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                            return Json(new { success = true, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                            return Json(new { success = true, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        return Json(new { success = false, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { success = false, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Date: 09-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: Reset Pay To Paypal
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult ResetPayToPaypal(PaypalModel objPaypalModel)//,string PaymentType, long user_id,long subscription_id)
        {
            PayService objPayService = new PayService();
            PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
            PayPalOrder objPay = new PayPalOrder();
            try
            {
                var objResetAmount = Session["ResetAmount"];
                if (objPaypalModel.user_id > 0 && ModelState.IsValid && objResetAmount != null)
                {
                    PaypalModel objModel = new PaypalModel();
                    objPaypalModel.order_amount = Convert.ToInt32(objResetAmount);
                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                    objModel = objPaypalPaymentService.ResetPaypalPaymentfirst(objPaypalModel);
                    if (objModel.is_success)
                    {
                        if (objPaypalModel.issave == true)
                        {
                            var Data = SaveCardtoVault(objPaypalModel).ConfigureAwait(false);
                            objPaypalModel.PaymentIsSuccess = "Transaction successful";
                            TempData["ReturnMessage"] = "Transaction successful";
                            TempData["transaction_id"] = objPaypalModel.transaction_id;
                            return Json(new { success = true, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objPaypalModel.PaymentIsSuccess = "Transaction successful";
                            TempData["ReturnMessage"] = "Transaction successful";
                            TempData["transaction_id"] = objPaypalModel.transaction_id;
                            return Json(new { success = true, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else
                    {
                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                        return Json(new { success = false, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                    TempData["ReturnMessage"] = "Transaction unsuccessful";
                }
                return Json(new { success = false, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                //return RedirectToAction("Dashboard", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });

            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Paypal express checkout
        /// </summary>
        /// <param name="PaymentType"></param>
        /// <param name="user_id"></param>
        /// <param name="subscription_id"></param>
        /// <param name="promo_code_id"></param>
        /// <returns></returns>
        public JsonResult PayWithPaypal(string PaymentType, long user_id, long subscription_id, int promo_code_id, int platform_id)
        {
            try
            {
                PayService objPayService = new PayService();
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayPalOrder objPay = new PayPalOrder();
                PaypalModel objPaypalModel = new PaypalModel();
                PayPalRedirect redirect = new PayPalRedirect();
                if (user_id > 0 && subscription_id > 0)
                {
                    objPaypalModel.user_id = user_id;
                    objPaypalModel.subscription_id = subscription_id;
                    objPaypalModel.promo_code_id = promo_code_id;
                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                    string strGUID = (Session["TicketLockedId"] != null ? Session["TicketLockedId"].ToString() : "");
                    objPay.OrderDate = System.DateTime.UtcNow;
                    objPay.Amount = objPaypalModel.order_amount.Value;
                    objPay.OrderId = "Trader_" + objPaypalModel.user_id + DateTime.UtcNow;
                    objPay.IteamName = objPaypalModel.subscription_name;
                    redirect = objPaypalPaymentService.ExpressCheckout(objPay);
                    Session["Token"] = redirect.Token;
                    Session["Amount"] = objPaypalModel.order_amount;
                    Session["user_id"] = objPaypalModel.user_id;
                    Session["platform_id"] = platform_id;
                    Session["subscription_id"] = objPaypalModel.subscription_id;
                    Session["promo_code_id"] = objPaypalModel.promo_code_id;
                }
                else
                {
                    redirect.Token = "";
                }
                return Json(redirect.Token, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public JsonResult ResetPayWithPaypal(string PaymentType, long user_id)
        {
            try
            {
                var objResetAmount = Session["ResetAmount"];
                PayService objPayService = new PayService();
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayPalOrder objPay = new PayPalOrder();
                PaypalModel objPaypalModel = new PaypalModel();
                PayPalRedirect redirect = new PayPalRedirect();
                if (user_id > 0 && objResetAmount != null)
                {
                    objPaypalModel.user_id = user_id;
                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                    string strGUID = (Session["TicketLockedId"] != null ? Session["TicketLockedId"].ToString() : "");
                    objPay.Amount = Convert.ToInt32(objResetAmount);
                    objPay.OrderId = "Treder_" + objPaypalModel.user_id + DateTime.UtcNow;
                    objPay.IteamName = "Reset Fee";
                    objPaypalModel.order_amount = objPay.Amount;
                    redirect = objPaypalPaymentService.ResetExpressCheckout(objPay);
                    Session["Token"] = redirect.Token;
                    Session["Amount"] = objPaypalModel.order_amount;
                    Session["user_id"] = objPaypalModel.user_id;

                }
                else
                {
                    redirect.Token = "";
                }
                return Json(redirect.Token, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        #region Get Paypal Token
        /// <summary>
        /// Date: 01-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Page Load Event
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public async Task<VaultModel> GetVaultToken()
        {
            try
            {
                VaultModel objVaultModel = new VaultModel();
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en_US"));
                    var clientId = System.Configuration.ConfigurationManager.AppSettings["clientId"];
                    var clientSecret = System.Configuration.ConfigurationManager.AppSettings["clientSecret"];
                    var bytes = Encoding.UTF8.GetBytes($"{clientId}:{clientSecret}");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(bytes));
                    var keyValues = new List<KeyValuePair<string, string>>();
                    keyValues.Add(new KeyValuePair<string, string>("grant_type", "client_credentials"));
                    var responseMessage = await client.PostAsync(APIMethod.PayPalVault.GetToken, new FormUrlEncodedContent(keyValues)).ConfigureAwait(false);
                    if (responseMessage.IsSuccessStatusCode)
                    {
                        string responseString = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                        objVaultModel = JsonConvert.DeserializeObject<VaultModel>(responseString);
                        TempData["vault_access_token"] = objVaultModel.access_token;
                    }
                    return objVaultModel;
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Save Card TO Vault
        /// <summary>
        /// Date: 01-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Save Card to Vault
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="objPaypalModel"></param>
        /// <returns></returns>
        public async Task<VaultCardModel> SaveCardtoVault(PaypalModel objPaypalModel)
        {
            VaultModel objVaultModel = new VaultModel();
            VaultCardModel objResponseVCardModel = new VaultCardModel();
            PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
            try
            {
                if (objPaypalModel != null)
                {
                    VaultCardModel objRequestVCardModel = new VaultCardModel();
                    objRequestVCardModel = BindPaypaltoVaultCardModel(objPaypalModel);
                    string access_token = null;
                    if (string.IsNullOrEmpty(TempData["vault_access_token"] as string))
                    {
                        objVaultModel = await GetVaultToken().ConfigureAwait(true);
                        access_token = objVaultModel.access_token;
                    }
                    using (HttpClient client = new HttpClient())
                    {
                        access_token = TempData["vault_access_token"] as string;
                        client.DefaultRequestHeaders.Add(HttpRequestHeader.ContentType.ToString(), "application/json");
                        //client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "zMXGa8WNQuaBxWzgUhO2dw==");
                        client.DefaultRequestHeaders.TryAddWithoutValidation("Authorization", "Bearer " + access_token);
                        client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en_US"));
                        var responseMessage = await client.PostAsJsonAsync(APIMethod.PayPalVault.SaveCard, objRequestVCardModel).ConfigureAwait(false);
                        if (responseMessage != null)
                        {
                            string responseString = await responseMessage.Content.ReadAsStringAsync().ConfigureAwait(false);
                            objVaultModel = JsonConvert.DeserializeObject<VaultModel>(responseString);
                            objVaultModel.user_id = objPaypalModel.user_id;
                            if (objVaultModel.state == "ok")
                            {
                                if (objPaypalPaymentService.AddCardVault(objVaultModel))
                                {
                                    return objResponseVCardModel;
                                };
                            }
                        }
                        return objResponseVCardModel;
                    }
                }
                return objResponseVCardModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Date: 01-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Bind Paypal to Vault Card Model
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="objPaypalModel"></param>
        /// <returns></returns>
        private VaultCardModel BindPaypaltoVaultCardModel(PaypalModel objPaypalModel)
        {
            try
            {
                VaultCardModel objVaultCardModel = new VaultCardModel();
                VaultAddressModel objVaultAddressModel = new VaultAddressModel();

                objVaultCardModel.billing_address = null;
                objVaultCardModel.expire_month = Convert.ToInt32(objPaypalModel.card_expiry.Substring(0, 2));
                objVaultCardModel.expire_year = Convert.ToInt32(objPaypalModel.card_expiry.Substring(3, 4));
                objVaultCardModel.type = objPaypalModel.card_type.ToLower();
                objVaultCardModel.first_name = objPaypalModel.card_name;
                objVaultCardModel.last_name = null;
                objVaultCardModel.number = objPaypalModel.card_number;
                objVaultCardModel.billing_address = null;
                return objVaultCardModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        public ActionResult CheckoutReview(string token, string PayerID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                UserService objUserService = new UserService();
                PaypalModel objPaypalModel = new PaypalModel();
                MemberService objMemberService = new MemberService();
                MemberModel objMemberModel = new MemberModel();

                string retMsg = "";
                string PayerID1 = "";
                if (string.IsNullOrEmpty(PayerID))
                {
                    //return RedirectToAction("step1", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                    return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }

                bool GetCheckoutDetailsIsSucc = objPaypalPaymentService.GetCheckoutDetails(token, ref PayerID1, ref retMsg);
                if (GetCheckoutDetailsIsSucc == true)
                {
                    if (string.IsNullOrEmpty(PayerID1))
                    {
                        //return RedirectToAction("step1", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                        return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                    }
                    string tkn = Convert.ToString(Session["Token"]);
                    if (tkn == token)
                    {
                        string Amt = Convert.ToString(Session["Amount"]);
                        if (!string.IsNullOrEmpty(Amt))
                        {
                            objPaypalPaymentService.DoCheckoutPayment(Amt, token, PayerID, ref retMsg);
                            if (!string.IsNullOrEmpty(retMsg))
                            {
                                string Tran_id = retMsg;
                                long user_id = Convert.ToInt64(Session["user_id"]);
                                long subscription_id = Convert.ToInt64(Session["subscription_id"]);
                                int promo_code_id = Convert.ToInt32(Session["promo_code_id"]);
                                int Platform = Convert.ToInt32(Session["platform_id"]);
                                if (user_id > 0 && subscription_id > 0)
                                {
                                    objPaypalModel.user_id = user_id;
                                    objPaypalModel.subscription_id = subscription_id;
                                    objPaypalModel.promo_code_id = promo_code_id;
                                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                                    objPaypalModel.transaction_id = Tran_id;
                                    objPaypalModel.PaymentType = "Ppal";
                                    objMemberModel = objMemberService.GetUser(user_id);
                                    string sAccountAllias = objUserService.GetAccountAlliasName(false, objMemberModel.first_name.Substring(0, 1), objMemberModel.last_name);
                                    bool SubPaypalModel = objPaypalPaymentService.AddSubscription(objPaypalModel);
                                    objMemberModel.AccountAllias = sAccountAllias;
                                    if (Platform == (Int32)EnumTradingPlatform.TradingTechnologies)
                                    {
                                        bool bAddUser = objUserService.AddtoTTPlatform(objMemberModel, false);
                                    }
                                    else
                                    {
                                        //need to look for rithmic
                                    }
                                    if (SubPaypalModel)
                                    {
                                        //For creating recurring payment profile - 16-Apr-2018
                                        bool bResult = objPaypalPaymentService.CreateRecurringProfile(Convert.ToDecimal(Session["Amount"]), token, PayerID, user_id);

                                        //send mail of payment activity
                                        if (bResult)
                                        {
                                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                                            parameters.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                                            parameters.Add(EmailTemplateParameters.FullName, objMemberModel.first_name);
                                            parameters.Add(EmailTemplateParameters.Amount, Convert.ToString(Session["Amount"]));
                                            new CommonService().SendEmail("support@traderdock.com", EmailTemplateNames.PaymentTransactionSuccess, parameters, null);
                                            TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                                            objPaypalModel.PaymentIsSuccess = "Transaction successful";
                                            TempData["ReturnMessage"] = "Transaction successful";
                                            TempData["transaction_id"] = objPaypalModel.transaction_id;

                                        }
                                        else
                                        {
                                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                                            parameters.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                                            parameters.Add(EmailTemplateParameters.FullName, objMemberModel.first_name);
                                            parameters.Add(EmailTemplateParameters.Amount, Convert.ToString(Session["Amount"]));
                                            // new CommonService().SendEmail("support@traderdock.com", EmailTemplateNames.PaymentTransactionFailed, parameters, null);
                                            TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                                            objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                                            TempData["ReturnMessage"] = "Transaction unsuccessful";
                                            TempData["transaction_id"] = objPaypalModel.transaction_id;
                                        }
                                        if (RedirectToDashboard(Convert.ToInt64(Session["user_id"])))
                                        {
                                            return RedirectToAction("dashboard", "home");
                                        }
                                    }
                                    else
                                    {
                                        retMsg = "Transaction unsuccessful";
                                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                                    }
                                }
                                else
                                {
                                    retMsg = "Transaction unsuccessful";
                                    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                                    TempData["ReturnMessage"] = "Transaction unsuccessful";
                                }
                            }
                            else
                            {
                                retMsg = "Transaction unsuccessful something goes wrong";
                                objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                                TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                            }
                        }
                        else
                        {
                            retMsg = "Transaction unsuccessful something goes wrong";
                            objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                            TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                        }
                    }
                    else
                    {
                        retMsg = "Transaction unsuccessful";
                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                    }
                    TempData["token"] = token;
                    TempData["PayerID"] = PayerID1;
                }
                return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Date: 01-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: Page Load Event
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public ActionResult ResetCheckoutReview(string token, string PayerID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                PaypalModel objPaypalModel = new PaypalModel();
                string retMsg = "";
                string PayerID1 = "";
                if (string.IsNullOrEmpty(PayerID))
                {
                    return RedirectToAction("Dashboard", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }

                bool GetCheckoutDetailsIsSucc = objPaypalPaymentService.GetCheckoutDetails(token, ref PayerID1, ref retMsg);
                if (GetCheckoutDetailsIsSucc == true)
                {
                    if (string.IsNullOrEmpty(PayerID1))
                    {
                        return RedirectToAction("Dashboard", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                    }

                    string tkn = Convert.ToString(Session["Token"]);
                    if (tkn == token)
                    {
                        string Amt = Convert.ToString(Session["Amount"]);
                        if (!string.IsNullOrEmpty(Amt))
                        {

                            objPaypalPaymentService.DoCheckoutPayment(Amt, token, PayerID, ref retMsg);
                            if (!string.IsNullOrEmpty(retMsg))
                            {
                                string Tran_id = retMsg;
                                long user_id = Convert.ToInt64(Session["user_id"]);
                                long subscription_id = Convert.ToInt64(Session["subscription_id"]);
                                PaypalPaymentService objResetPaypalPaymentService = new PaypalPaymentService();
                                if (user_id > 0)
                                {
                                    objPaypalModel.user_id = user_id;
                                    objPaypalModel.subscription_id = subscription_id;
                                    objPaypalModel.order_amount = Convert.ToDecimal(Amt);
                                    objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                                    objPaypalModel.transaction_id = Tran_id;
                                    objPaypalModel.PaymentType = "Ppal";
                                    bool IsDone = objResetPaypalPaymentService.AddResetRequest(objPaypalModel);
                                    if (IsDone)
                                    {

                                        //return Json(new { success = true, Msg = objPaypalModel.payment_response_message }, JsonRequestBehavior.AllowGet);
                                        objPaypalModel.PaymentIsSuccess = "Transaction successful";
                                        TempData["ReturnMessage"] = "Transaction successful";
                                        TempData["transaction_id"] = objPaypalModel.transaction_id;

                                    }
                                    else
                                    {
                                        retMsg = "Transaction unsuccessful";
                                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                                    }
                                }
                                else
                                {
                                    retMsg = "Transaction unsuccessful";
                                    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                                    TempData["ReturnMessage"] = "Transaction unsuccessful";
                                }
                            }
                            else
                            {
                                retMsg = "Transaction unsuccessful something goes wrong";
                                objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                                TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                            }
                        }
                        else
                        {
                            retMsg = "Transaction unsuccessful something goes wrong";
                            objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                            TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(PayerID1))
                        {
                            retMsg = "Transaction unsuccessful";
                            objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                            TempData["ReturnMessage"] = "Transaction unsuccessful";
                        }
                    }

                    //if (string.IsNullOrEmpty(PayerID1))
                    //{
                    //    retMsg = "Transaction unsuccessful";
                    //    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                    //    TempData["ReturnMessage"] = "Transaction unsuccessful";
                    //}

                    //TempData["ReturnMessage"] = retMsg;
                    TempData["token"] = token;
                    TempData["PayerID"] = PayerID1;
                }
                return RedirectToAction("Dashboard", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// To redirect to Dashboard once payment successfully completed
        /// Developer : Shoaib
        /// Date : 19/Sep/2017
        /// </summary>
        bool RedirectToDashboard(long UserId)
        {
            bool IsSuccess = false;
            string StrExpMessage = "";
            try
            {
                SettingsService objSettingService;
                MemberService objMemberService;
                MemberModel objMemeberModel = new MemberModel();
                SettingModel objSettingModel = new SettingModel();
                objMemberService = new MemberService();
                objSettingService = new SettingsService();
                objMemeberModel = objMemberService.GetUser(UserId);
                objSettingModel = objSettingService.GetSettingByName("PageSize");
                if (objSettingModel.setting_value != null)
                {
                    Session["PageSize"] = objSettingModel.setting_value;
                }
                //Session["User"] = objMemberModel.user_name;
                Session["MemberID"] = objMemeberModel.user_id;
                Session["MemberEmail"] = objMemeberModel.user_email;
                //by Bharat NotificationCount 11 / 07 / 2017
                Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                var count = dbConnection.user_notifications.Where(m => m.user_id == objMemeberModel.user_id).Count();
                Session["UserNotificationCount"] = count;
                Session["UserIDForNotification"] = objMemeberModel.user_id;
                DashboardService objDashboardService = new DashboardService();
                Session["NotificationCount"] = objDashboardService.NotificationCount(objMemeberModel.user_id);
                Session["MemberName"] = objMemeberModel.first_name;
                if (!string.IsNullOrEmpty(objMemeberModel.profile_image_url))
                {
                    Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objMemeberModel.profile_image_url);
                }
                else
                {
                    Session["MemberImage"] = Convert.ToString(ConfigurationManager.AppSettings["DefaultImageURL"]);
                }
                FormsAuthentication.SetAuthCookie(objMemeberModel.user_email, false);
                IsSuccess = true;
            }
            catch (Exception ex)
            {
                StrExpMessage = ex.Message;
                IsSuccess = false;
                throw new ServiceLayerException(ex);
            }
            return IsSuccess;
            //return RedirectToAction("Dashboard", "Home");
        }

        /// <summary>
        /// Upload Member Data into CSV
        /// </summary>
        /// Developer : Shoaib
        /// Date : 19/Sep/2017
        /// <param name="userID"></param>
        public bool UploadCSVData(long userID = 0)
        {
            bool IsSuccess = false;
            bool IsLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"].ToString());
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            //var filePath = Server.MapPath("~") + "\\Files\\add_user_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + userID + ".csv";
            var filePath = Server.MapPath("~") + "\\Files\\add_user_" + userID + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";

            MemberService objService = new MemberService();
            MemberModel objModel = new MemberModel();

            StringBuilder sb = new StringBuilder();
            //sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", "Email", "First Name", "Last Name", "Street Address", "City", "Country", "State", "Postal Code", "Phone Number", "Password") + Environment.NewLine);

            objModel = objService.GetDataForCSV(userID);
            char[] CharSeparator = new char[1];
            CharSeparator[0] = ',';
            if (!String.IsNullOrEmpty(objModel.address))
            {
                string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                objModel.address = strStreetAddress;
            }

            string StrContactNumber = objModel.contact_number;
            if (objModel.contact_number == "NA")
                StrContactNumber = "44 7700 900708";

            //Feedback (10-Oct-2017) 
            //Added extra field at EOD for subscription number in the file uploaded to Rithmic
            //By Shoaib

            if (objModel.country_name.Trim().ToLower() != "usa" && objModel.country_name.Trim().ToLower() != "canada")
            {
                objModel.state_name = "";
            }

            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10}", objModel.user_email.Trim(), objModel.first_name.Trim(), objModel.last_name.Trim(), objModel.address.Trim(), objModel.city_name.Trim(), objModel.country_name.Trim(), objModel.state_name.Trim(), objModel.postal_code.Trim(), StrContactNumber.Trim(), objModel.password.Trim(), objModel.subscription_id.ToString()));

            System.IO.File.AppendAllText(filePath, sb.ToString());

            //for copy file one path to another
            string fileName = Path.GetFileName(filePath);
            string sourcePath = Server.MapPath("~") + "\\Files";

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);

            var host = "ritpz11300.11.rithmic.com";
            var port = 22;
            var username = "u3CiKQPhV";
            var password = "xBWKWRLW";

            string remoteRootDirectory;
            if (IsRidhmicTestEnv)
            {
                remoteRootDirectory = @"test/new_user/";
            }
            else
            {
                remoteRootDirectory = @"production/new_user/";
            }

            try
            {
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    client.ChangeDirectory(remoteRootDirectory);   // for change directory on sftp
                    //string oldpath = @"test/in_flight/add_user_141.csv" + fileName;
                    //string newpath = @"test/new_user/add_user_141.csv" + fileName;
                    if (IsLive)
                    {
                        if (client.IsConnected)
                        {
                            using (var fileStream = new FileStream(sourceFile, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(sourceFile));
                            }
                            //client.RenameFile(oldpath, newpath); // for move file on sftp
                        }
                    }
                }

                // Client Feedback - separate email templates for free and paid subscriptions
                // By shoaib - 10-Oct-2017
                // Changed template from "Welcome email" to "Welcome email free"
                if (!string.IsNullOrEmpty(objModel.user_email))
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.Email, objModel.user_email);
                    parameters.Add(EmailTemplateParameters.FullName, objModel.first_name);
                    parameters.Add(EmailTemplateParameters.Password, objModel.password);
                    //string emailattachment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["attachmentemail"]);
                    new CommonService().SendEmail(objModel.user_email, EmailTemplateNames.WelcomePaidSubscription, parameters, null);
                }

                //changed by shoaib - 5Aug2017
                //for proper account subscription message
                SubscriptionService objSubscriptionService = new SubscriptionService();
                SubscriptionModel objSubscrModel = objSubscriptionService.GetSubscription(Convert.ToInt64(TempData["SubscriptionId"]));

                //TempData["ModelTitle"] = objSubscrModel.subscription_name;
                if (objSubscrModel.subscription_id == 8)
                {
                    TempData["ModelTitle"] = objSubscrModel.subscription_name;
                    TempData["ModelText"] = "Your 14 - Days Free Trial account is successfully created!!!";
                }
                else
                {
                    TempData["ModelTitle"] = "Payment Successful";
                    //TempData["ModelText"] = "Your " + objSubscrModel.subscription_name + " account is successfully created!!!";
                    TempData["ModelText"] = "Thank you for subscribing to the TraderDock Challenge. Your TraderDock account has been successfully created.Please check your email for instructions on how to get started.";
                }

                TempData["IsFreeTrialActivated"] = '2';
                IsSuccess = true;

            }
            catch (Exception ex)
            {
                //throw ex;
                IsSuccess = false;
                throw new ServiceLayerException(ex);
            }

            return IsSuccess;
        }

        //IPN for recurring payment 
        //By shoaib - 20Apr2018
        [HttpPost]
        public HttpStatusCodeResult ipn()
        {
            try
            {
                //Store the IPN received from PayPal
                LogRequest(Request);
                Task.Run(() => VerifyTask(Request));
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        private void VerifyTask(HttpRequestBase ipnRequest)
        {
            var verificationResponse = string.Empty;
            string paypalIPNURL = System.Configuration.ConfigurationManager.AppSettings["PayPalIPNURL"];

            try
            {
                var verificationRequest = (HttpWebRequest)WebRequest.Create(paypalIPNURL);

                //Set values for the verification request
                verificationRequest.Method = "POST";
                verificationRequest.ContentType = "application/x-www-form-urlencoded";
                var param = Request.BinaryRead(ipnRequest.ContentLength);
                var strRequest = Encoding.ASCII.GetString(param);

                //Add cmd=_notify-validate to the payload
                strRequest = "cmd=_notify-validate&" + strRequest;
                verificationRequest.ContentLength = strRequest.Length;

                //Attach payload to the verification request
                var streamOut = new StreamWriter(verificationRequest.GetRequestStream(), Encoding.ASCII);
                streamOut.Write(strRequest);
                streamOut.Close();

                //Send the request to PayPal and get the response
                var streamIn = new StreamReader(verificationRequest.GetResponse().GetResponseStream());
                verificationResponse = streamIn.ReadToEnd();
                streamIn.Close();
                //var ipn = Request.Form.AllKeys.ToDictionary(k => k, k => Request[k]);
                //string requestJson = MyDictionaryToJson(ipn);
                //ipn_payment paypalPayment = new ipn_payment();
                //paypalPayment.json_request = requestJson.Replace("[", @"""").Replace("]", @"""");
                //string strPaymentStatus = ipn.ContainsKey("payment_status") ? ipn["payment_status"] : null;
                //string strRecurringPayment = ipn.ContainsKey("recurring_payment") ? ipn["recurring_payment"] : null;
                //if (!string.IsNullOrEmpty(strPaymentStatus))
                //{
                //    if (strPaymentStatus.Trim().ToLower() == "completed" && strRecurringPayment.Trim().ToLower() == "recurring_payment")
                //    {
                //        ProcessVerificationResponse("VERIFIED");
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            ProcessVerificationResponse(verificationResponse);
        }

        private void LogRequest(HttpRequestBase request)
        {
            try
            {
                var ipn = Request.Form.AllKeys.ToDictionary(k => k, k => Request[k]);
                string requestJson = MyDictionaryToJson(ipn);
                Traderdock_DBEntities DbCon = new Traderdock_DBEntities();
                ipn_payment paypalPayment = new ipn_payment();
                paypalPayment.json_request = requestJson.Replace("[", @"""").Replace("]", @"""");
                paypalPayment.address_city = ipn.ContainsKey("address_city") ? ipn["address_city"] : null;
                paypalPayment.address_country = ipn.ContainsKey("address_country") ? ipn["address_country"] : null;
                paypalPayment.address_country_code = ipn.ContainsKey("address_country_code") ? ipn["address_country_code"] : null; //not able to find in recurring payment
                paypalPayment.address_name = ipn.ContainsKey("address_name") ? ipn["address_name"] : null;
                paypalPayment.address_state = ipn.ContainsKey("address_state") ? ipn["address_state"] : null;
                paypalPayment.address_status = ipn.ContainsKey("address_status") ? ipn["address_status"] : null;
                paypalPayment.address_street = ipn.ContainsKey("address_street") ? ipn["address_street"] : null;
                paypalPayment.address_zip = ipn.ContainsKey("address_zip") ? ipn["address_zip"] : null;
                paypalPayment.amount = ipn.ContainsKey("amount") ? ipn["amount"] : null;
                paypalPayment.amount_per_cycle = ipn.ContainsKey("amount_per_cycle") ? ipn["amount_per_cycle"] : null;
                paypalPayment.business = ipn.ContainsKey("business") ? ipn["business"] : null;
                paypalPayment.first_name = ipn.ContainsKey("first_name") ? ipn["first_name"] : null;
                paypalPayment.initial_payment_amount = ipn.ContainsKey("initial_payment_amount") ? ipn["initial_payment_amount"] : null;
                paypalPayment.ipn_track_id = ipn.ContainsKey("ipn_track_id") ? ipn["ipn_track_id"] : null;
                paypalPayment.last_name = ipn.ContainsKey("last_name") ? ipn["last_name"] : null;
                paypalPayment.mc_currency = ipn.ContainsKey("mc_currency") ? ipn["mc_currency"] : null;
                paypalPayment.mc_fee = ipn.ContainsKey("mc_fee") ? ipn["mc_fee"] : null;
                paypalPayment.mc_gross = ipn.ContainsKey("mc_gross") ? ipn["mc_gross"] : null;
                paypalPayment.next_payment_date = ipn.ContainsKey("next_payment_date") ? ipn["next_payment_date"] : null;
                paypalPayment.notify_version = ipn.ContainsKey("notify_version") ? ipn["notify_version"] : null;
                paypalPayment.outstanding_balance = ipn.ContainsKey("outstanding_balance") ? ipn["outstanding_balance"] : null;
                paypalPayment.payer_email = ipn.ContainsKey("payer_email") ? ipn["payer_email"] : null;
                paypalPayment.payer_id = ipn.ContainsKey("payer_id") ? ipn["payer_id"] : null;
                paypalPayment.payer_status = ipn.ContainsKey("payer_status") ? ipn["payer_status"] : null;
                paypalPayment.payment_date = ipn.ContainsKey("payment_date") ? ipn["payment_date"] : null;
                paypalPayment.payment_status = ipn.ContainsKey("payment_status") ? ipn["payment_status"] : null;
                paypalPayment.payment_cycle = ipn.ContainsKey("payment_cycle") ? ipn["payment_cycle"] : null;
                paypalPayment.payment_fee = ipn.ContainsKey("payment_fee") ? ipn["payment_fee"] : null;
                paypalPayment.payment_gross = ipn.ContainsKey("payment_gross") ? ipn["payment_gross"] : null;
                paypalPayment.time_created = ipn.ContainsKey("time_created") ? ipn["time_created"] : null;
                paypalPayment.recurring_payment_id = ipn.ContainsKey("recurring_payment_id") ? ipn["recurring_payment_id"] : null;
                paypalPayment.payment_type = ipn.ContainsKey("payment_type") ? ipn["payment_type"] : null;
                paypalPayment.receiver_email = ipn.ContainsKey("receiver_email") ? ipn["receiver_email"] : null;
                paypalPayment.receiver_id = ipn.ContainsKey("receiver_id") ? ipn["receiver_id"] : null;
                paypalPayment.residence_country = ipn.ContainsKey("residence_country") ? ipn["residence_country"] : null;
                paypalPayment.tax = ipn.ContainsKey("tax") ? ipn["tax"] : null;
                paypalPayment.txn_id = ipn.ContainsKey("txn_id") ? ipn["txn_id"] : null;
                paypalPayment.txn_type = ipn.ContainsKey("txn_type") ? ipn["txn_type"] : null;
                paypalPayment.verify_sign = ipn.ContainsKey("verify_sign") ? ipn["verify_sign"] : null;
                DbCon.ipn_payment.Add(paypalPayment);
                DbCon.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        private void ProcessVerificationResponse(string verificationResponse)
        {
            try
            {
                var ipn = Request.Form.AllKeys.ToDictionary(k => k, k => Request[k]);
                string requestJson = MyDictionaryToJson(ipn);
                MemberModel objMemberModel = new MemberModel();
                PaypalModel objPaypalModel = new PaypalModel();
                PayService objPayService = new PayService();
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                SubscriptionService objSubscriptionService;
                MemberService objMemberService = new MemberService();
                objMemberModel = objMemberService.GetUserByEmail(ipn.ContainsKey("payer_email") ? ipn["payer_email"] : null);
                if (objMemberModel.user_id > 0)
                {


                    if (verificationResponse.Equals("VERIFIED"))
                    {
                        // check that Payment_status=Completed
                        // check that Txn_id has not been previously processed
                        // check that Receiver_email is your Primary PayPal email
                        // check that Payment_amount/Payment_currency are correct
                        // process payment

                        objSubscriptionService = new SubscriptionService();
                        objPaypalModel.user_id = objMemberModel.user_id;
                        objPaypalModel.subscription_id = 3;
                        objPaypalModel.promo_code_id = objMemberModel.promo_code_id;
                        objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                        objPaypalModel.transaction_id =
                            Convert.ToString(ipn.ContainsKey("txn_id") ? ipn["txn_id"] : null);
                        objPaypalModel.PaymentType = "IPN";
                        bool SubPaypalModel = objPaypalPaymentService.AddSubscription(objPaypalModel);
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                        parameters.Add(EmailTemplateParameters.FullName, objMemberModel.first_name);
                        parameters.Add(EmailTemplateParameters.Amount,
                            Convert.ToString(ipn.ContainsKey("mc_gross") ? ipn["mc_gross"] : null));
                        new CommonService().SendEmail(objMemberModel.user_email,
                            EmailTemplateNames.PaymentTransactionSuccess, parameters, null);
                    }
                    else if (verificationResponse.Equals("INVALID"))
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                        parameters.Add(EmailTemplateParameters.FullName, objMemberModel.first_name);
                        parameters.Add(EmailTemplateParameters.Amount,
                            Convert.ToString(ipn.ContainsKey("mc_gross") ? ipn["mc_gross"] : null));
                        //  new CommonService().SendEmail(objMemberModel.user_email, EmailTemplateNames.PaymentTransactionFailed, parameters, null);
                    }
                    else
                    {
                        //Log error
                    }

                    Dictionary<string, string> parametersAdmin = new Dictionary<string, string>();
                    parametersAdmin.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                    parametersAdmin.Add(EmailTemplateParameters.FullName, objMemberModel.first_name);
                    parametersAdmin.Add(EmailTemplateParameters.Amount,
                        Convert.ToString(ipn.ContainsKey("mc_gross") ? ipn["mc_gross"] : null));
                    new CommonService().SendEmail("support@traderdock.com", EmailTemplateNames.AdminPaymentTransaction, parametersAdmin, null);
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public ActionResult ApproveAWTransaction(int TransactionID)
        {
            try
            {
                return RedirectToAction("Dashboard", "Home", new { transaction_id = TransactionID.ToString(), PaymentIsSuccess = "true" });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public JsonResult ResetPayWithAW(long userId)
        {
            try
            {
                PayService payService = new PayService();
                AlliedWalletModel model = new AlliedWalletModel();
                Dictionary<string, string> values = new Dictionary<string, string>();

                Session["ResetAmount"] = AlliedWalletSettings.ResetFeeAmount;

                if (userId > 0)
                {
                    model.UserId = userId;
                    model = payService.GetAWPayDetails(model);

                    values["ApiUrl"] = model.ApiUrl;
                    values["QuickPayToken"] = model.QuickPayToken;
                    values["SiteID"] = model.SiteId;
                    values["AmountTotal"] = model.OrderAmount.ToString().Replace(",", ".");
                    values["CurrencyID"] = model.CurrencyID;
                    values["AmountShipping"] = "0.00";
                    values["ShippingRequired"] = "false";
                    values["MembershipRequired"] = "false";
                    values["ItemName"] = model.ResetFeeProductName;
                    values["ItemQuantity"] = "1";
                    values["ItemAmount"] = model.OrderAmount.ToString().Replace(",", ".");
                    values["ItemDesc"] = model.ResetFeeProductDescription;
                    values["Address"] = model.User.address;
                    values["City"] = model.User.city_name;
                    values["State"] = model.User.state_name;
                    values["Email"] = model.User.user_email;
                    values["Phone"] = model.User.contact_number;
                    values["PostalCode"] = model.User.postal_code;
                    values["FirstName"] = model.User.first_name;
                    values["LastName"] = model.User.last_name;
                    values["ApprovedURL"] = model.ResetFeeApprovedUrl;
                    values["ConfirmURL"] = model.ConfirmUrl;
                    values["DeclinedURL"] = model.DeclinedUrl;

                    // save info about payment
                    Session["aw_amount"] = model.OrderAmount.ToString().Replace(",", ".");
                    Session["aw_userId"] = userId;
                }

                return Json(values, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public JsonResult PayWithAW(long userId, int promoCodeId, int subscriptionId)
        {
            try
            {
                PayService payService = new PayService();
                AlliedWalletModel model = new AlliedWalletModel();
                Dictionary<string, string> values = new Dictionary<string, string>();

                Session["ResetAmount"] = AlliedWalletSettings.ResetFeeAmount;

                if (userId > 0)
                {
                    model.UserId = userId;
                    model.PromoCodeId = promoCodeId;
                    model.SubscriptionId = subscriptionId;
                    model = payService.GetAWPayDetails(model);

                    values["ApiUrl"] = model.ApiUrl;
                    values["QuickPayToken"] = model.QuickPayToken;
                    values["SiteID"] = model.SiteId;
                    values["MerchantId"] = model.MerchantId;
                    values["SubscriptionPlanId"] = model.SubscriptionPlanId;
                    values["AmountTotal"] = "0.00";//model.OrderAmount.ToString().Replace(",", ".");
                    values["CurrencyID"] = model.CurrencyID;
                    values["AmountShipping"] = "0.00";
                    values["ShippingRequired"] = "false";
                    values["MembershipRequired"] = "false";
                    values["ItemName"] = model.SubscriptionProductName;
                    values["ItemQuantity"] = "1";
                    values["ItemAmount"] = "0.00";//model.OrderAmount.ToString().Replace(",", ".");
                    values["ItemDesc"] = model.SubscriptionProductDescription;
                    values["Address"] = model.User.address;
                    values["City"] = model.User.city_name;
                    values["State"] = model.User.state_name;
                    values["Email"] = model.User.user_email;
                    values["Phone"] = model.User.contact_number;
                    values["PostalCode"] = model.User.postal_code;
                    values["FirstName"] = model.User.first_name;
                    values["LastName"] = model.User.last_name;
                    values["ApprovedURL"] = model.SubscriptionApprovedUrl;
                    values["ConfirmURL"] = model.ConfirmUrl;
                    values["DeclinedURL"] = model.DeclinedUrl;

                    // save info about payment
                    Session["aw_amount"] = model.OrderAmount.ToString().Replace(",", ".");
                    Session["aw_userId"] = userId;
                    Session["aw_subscriptionId"] = subscriptionId;
                    Session["aw_promoCodeId"] = promoCodeId;
                }
                return Json(values, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public ActionResult CheckoutApproveAWReview(string TransactionID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                PaypalModel objPaypalModel = new PaypalModel();

                if (string.IsNullOrEmpty(TransactionID))
                {
                    return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }


                string amount = Convert.ToString(Session["aw_amount"]);

                if (!string.IsNullOrEmpty(amount))
                {

                    long userId = Convert.ToInt64(Session["aw_userId"]);
                    long subscriptionId = Convert.ToInt64(Session["aw_subscriptionId"]);
                    int promoCodeId = Convert.ToInt32(Session["aw_promoCodeId"]);

                    if (userId > 0 && subscriptionId > 0)
                    {
                        objPaypalModel.user_id = userId;
                        objPaypalModel.subscription_id = subscriptionId;
                        objPaypalModel.promo_code_id = promoCodeId;
                        objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                        objPaypalModel.transaction_id = TransactionID;
                        objPaypalModel.PaymentType = "Card";

                        //TODO: Change to AW
                        bool isAdded = objPaypalPaymentService.AddSubscription(objPaypalModel);

                        if (isAdded)
                        {
                            TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                            objPaypalModel.PaymentIsSuccess = "Transaction successful";
                            TempData["ReturnMessage"] = "Transaction successful";
                            TempData["transaction_id"] = objPaypalModel.transaction_id;

                            if (RedirectToDashboard(userId))
                            {
                                return RedirectToAction("dashboard", "home");
                            }
                        }
                        else
                        {
                            objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                            TempData["ReturnMessage"] = "Transaction unsuccessful";
                        }
                    }
                    else
                    {
                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                    }
                }
                else
                {
                    objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                    TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                }
                return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public ActionResult CheckoutConfirmAWReview(string TransactionID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                PaypalModel objPaypalModel = new PaypalModel();
                if (string.IsNullOrEmpty(TransactionID))
                {
                    return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }
                objPaypalModel.transaction_id = TransactionID;
                objPaypalModel.PaymentType = "Card";
                bool isAdded = objPaypalPaymentService.AddSubscription(objPaypalModel);
                if (isAdded)
                {
                    TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                    objPaypalModel.PaymentIsSuccess = "Transaction successful";
                    TempData["ReturnMessage"] = "Transaction successful";
                    TempData["transaction_id"] = objPaypalModel.transaction_id;
                }
                else
                {
                    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                    TempData["ReturnMessage"] = "Transaction unsuccessful";
                }
                return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public ActionResult CheckoutDeclineAWReview(string TransactionID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                PaypalModel objPaypalModel = new PaypalModel();
                if (string.IsNullOrEmpty(TransactionID))
                {
                    return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }
                objPaypalModel.order_amount = 0;//Decimal.Parse(Amount);
                objPaypalModel.transaction_id = TransactionID;
                objPaypalModel.PaymentType = "Card";
                bool isAdded = objPaypalPaymentService.AddSubscription(objPaypalModel);
                if (isAdded)
                {
                    TempData["SubscriptionId"] = objPaypalModel.subscription_id;
                    objPaypalModel.PaymentIsSuccess = "Transaction declined";
                    TempData["ReturnMessage"] = "Transaction declined";
                    TempData["transaction_id"] = objPaypalModel.transaction_id;
                }
                else
                {
                    objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                    TempData["ReturnMessage"] = "Transaction unsuccessful";
                }
                return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        public ActionResult CheckoutResetAWReview(string TransactionID)
        {
            try
            {
                PaypalPaymentService objPaypalPaymentService = new PaypalPaymentService();
                PayService objPayService = new PayService();
                PaypalModel objPaypalModel = new PaypalModel();
                if (string.IsNullOrEmpty(TransactionID))
                {
                    return RedirectToAction("learnhow", "home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = "Transaction unsuccessful" });
                }
                string amount = Convert.ToString(Session["aw_amount"]);
                if (!string.IsNullOrEmpty(amount))
                {
                    long userId = Convert.ToInt64(Session["aw_userId"]);
                    long subscriptionId = Convert.ToInt64(Session["aw_subscriptionId"]);
                    PaypalPaymentService objResetPaypalPaymentService = new PaypalPaymentService();
                    if (userId > 0)
                    {
                        objPaypalModel.user_id = userId;
                        objPaypalModel.subscription_id = subscriptionId;
                        objPaypalModel.order_amount = Convert.ToDecimal(amount);
                        objPaypalModel = objPayService.GetAllPayDetails(objPaypalModel);
                        objPaypalModel.transaction_id = TransactionID;
                        objPaypalModel.PaymentType = "Card";
                        bool isReset = objResetPaypalPaymentService.AddResetRequest(objPaypalModel);
                        if (isReset)
                        {
                            objPaypalModel.PaymentIsSuccess = "Transaction successful";
                            TempData["ReturnMessage"] = "Transaction successful";
                            TempData["transaction_id"] = objPaypalModel.transaction_id;

                        }
                        else
                        {
                            objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                            TempData["ReturnMessage"] = "Transaction unsuccessful";
                        }
                    }
                    else
                    {
                        objPaypalModel.PaymentIsSuccess = "Transaction unsuccessful";
                        TempData["ReturnMessage"] = "Transaction unsuccessful";
                    }
                }
                else
                {
                    objPaypalModel.PaymentIsSuccess = "Transaction something goes wrong";
                    TempData["ReturnMessage"] = "Transaction unsuccessful something goes wrong";
                }
                return RedirectToAction("Dashboard", "Home", new { transaction_id = objPaypalModel.transaction_id, PaymentIsSuccess = objPaypalModel.PaymentIsSuccess });
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        string MyDictionaryToJson(Dictionary<string, string> dict)
        {
            var entries = dict.Select(d =>
                string.Format("\"{0}\": [{1}]", d.Key, string.Join(",", d.Value)));
            return "{" + string.Join(",", entries) + "}";
        }

    }
}

