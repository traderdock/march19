﻿using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using Traderdock.Services;
namespace Traderdock.Web.Controllers
{
    /// <summary>
    /// Defines the <see cref="AccountController" />
    /// </summary>
    public class AccountController : BaseController
    {
        /// <summary>
        /// Defines the objMemberService
        /// </summary>
        internal MemberService objMemberService;

        /// <summary>
        /// Defines the objSettingService
        /// </summary>
        internal SettingsService objSettingService;

        /// <summary>
        /// Defines the objCommonService
        /// </summary>
        internal CommonService objCommonService;

        /// <summary>
        /// Defines the objPayService
        /// </summary>
        internal PayService objPayService;

        /// <summary>
        /// Defines the objCMSService
        /// </summary>
        internal CMSService objCMSService;

        /// <summary>
        /// Defines the objUserService
        /// </summary>
        internal UserService objUserService;

        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        /// <summary>
        /// Login Get
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult Login(string returnUrl)
        {
            try
            {
                Session["MemberID"] = null;
                Session["MemberEmail"] = null;
                Session["NotificationCount"] = null;
                Session["MemberName"] = null;
                Session["MemberImage"] = null;
                Session["ResetAmount"] = null;
                FormsAuthentication.SignOut();
                var usernamecookie = Request.Cookies["MemberEmail"];
                var passwordcookie = Request.Cookies["MemberPassword"];
                LoginModel objLoginModel = new LoginModel();
                if (usernamecookie != null && passwordcookie != null)
                {
                    objLoginModel.Email = Request.Cookies["MemberEmail"].Value;
                    objLoginModel.Password = Request.Cookies["MemberPassword"].Value;
                    objLoginModel.RememberMe = true;
                }
                ViewBag.ReturnUrl = returnUrl;
                objCMSService = new CMSService();
                CMSModel objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.LoginCMS);
                objLoginModel.LoginCMS = objCMSModel.cms_content;
                //For social media links - by bharat
                string Social_content = string.Empty;
                objCMSService = new CMSService();
                Social_content = objCMSService.GetSocialCMS();
                Session["SocialLinksData"] = Social_content;
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objLoginModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Login Post
        /// </summary>
        /// <param name="objLoginModel"></param>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel objLoginModel, string returnUrl)
        {
            SubscriptionService objSubscriptionService = new SubscriptionService();
            Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
            // var passwordTest = Utilities.DecryptPassword("D5UszWGlJSWFtYnPY/0DqX1l0YdL8+nP7sYfF2CeaX4=", "WDXG7OQ6");
            try
            {
                objMemberService = new MemberService();
                objSettingService = new SettingsService();
                MemberModel objMemberModel = new MemberModel();
                DashboardService objDashboardService = new DashboardService();
                if (ModelState.IsValid)
                {
                    objMemberModel = objMemberService.Login(objLoginModel);
                    if (objMemberModel != null)
                    {
                        if (objMemberModel.user_type_id == (Int32)EnumUserType.Admin)
                        {
                            FormsAuthentication.SetAuthCookie(objMemberModel.user_email, false);
                            return RedirectToAction("AdminDashboard", "Home");
                        }
                        else
                        {
                            if (objMemberModel.is_active)
                            {
                                SettingModel objSettingModel = new SettingModel();
                                objSettingModel = objSettingService.GetSettingByName("PageSize");
                                if (objSettingModel.setting_value != null)
                                {
                                    Session["PageSize"] = objSettingModel.setting_value;
                                }
                                objSettingModel = objSettingService.GetSettingByName("ResetAmount");
                                if (objSettingModel.setting_value != null)
                                {
                                    Session["ResetAmount"] = objSettingModel.setting_value;
                                }
                                //Session["User"] = objMemberModel.user_name;
                                Session["MemberID"] = objMemberModel.user_id;
                                Session["MemberEmail"] = objMemberModel.user_email;

                                // by Bharat NotificationCount 11 / 07 / 2017
                                var count = dbConnection.user_notifications.Where(m => m.user_id == objMemberModel.user_id && m.notification_status != true).Count();
                                Session["UserNotificationCount"] = count;
                                Session["UserIDForNotification"] = objMemberModel.user_id;
                                Session["NotificationCount"] = objDashboardService.NotificationCount(objMemberModel.user_id);
                                Session["MemberName"] = objMemberModel.first_name;
                                if (!string.IsNullOrEmpty(objMemberModel.profile_image_url))
                                {
                                    Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objMemberModel.profile_image_url);
                                }
                                else
                                {
                                    Session["MemberImage"] = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                                }
                                FormsAuthentication.SetAuthCookie(objMemberModel.user_email, false);
                                if (objLoginModel.RememberMe == true)
                                {
                                    HttpCookie ckUserName = new HttpCookie("MemberEmail");
                                    ckUserName.Expires = DateTime.Now.AddDays(2);
                                    ckUserName.Value = objLoginModel.Email;
                                    Response.Cookies.Add(ckUserName);
                                    HttpCookie ckPassword = new HttpCookie("MemberPassword");
                                    ckPassword.Expires = DateTime.Now.AddDays(2);
                                    ckPassword.Value = objLoginModel.Password;
                                    Response.Cookies.Add(ckPassword);
                                }
                                else
                                {
                                    HttpCookie ckUserName = new HttpCookie("MemberEmail");
                                    ckUserName.Expires = DateTime.Now.AddDays(-1d);
                                    Response.Cookies.Add(ckUserName);
                                    HttpCookie ckPassword = new HttpCookie("MemberPassword");
                                    ckPassword.Expires = DateTime.Now.AddDays(-1d);
                                    Response.Cookies.Add(ckPassword);
                                }
                                objSubscriptionService = new SubscriptionService();
                                List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
                                Subscription_List = objSubscriptionService.SubscriptionListForFront();
                                Session["front_subscription_id"] = Subscription_List[0].subscription_id;
                                Session["front_starting_balance"] = Subscription_List[0].starting_balance;
                                Session["front_price_per_month"] = Subscription_List[0].price_per_month;
                                return RedirectToAction("Dashboard", "Home");
                            }
                            else if (objMemberModel.is_password_false == true || objMemberModel.is_email_false == true)
                            {
                                if (objMemberModel.is_email_false == true)
                                    TempData["NoAccountExist"] = "No account exist.";
                                else
                                    TempData["ErrorUserMsg"] = "Email or password was incorrect.";
                            }
                            else
                            {
                                if (!objMemberModel.is_active)
                                {
                                    TempData["ErrorUserMsg"] = "Your account is not activated. Please check your email address for verification.";
                                }
                                else if (objMemberModel.is_deleted)
                                {
                                    TempData["ErrorUserMsg"] = "Your account has been deleted.";
                                }
                                else if (objMemberModel.is_blocked)
                                {
                                    TempData["ErrorUserMsg"] = "Your account has been blocked.";
                                }
                                else
                                {
                                    TempData["NoAccountExist"] = "No account exist.";
                                }
                            }
                        }
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "Your account is not activated. Please verify your email address.";
                    }
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            objCMSService = new CMSService();
            CMSModel objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.LoginCMS);
            objLoginModel.LoginCMS = objCMSModel.cms_content;
            HomePageModel objHomePageModel = new HomePageModel();
            objCMSService = new CMSService();
            objHomePageModel = objCMSService.GetHomeCMSByID();
            if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
            {
                ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
            }

            return View("Login", objLoginModel);
        }

        /// <summary>
        /// The UserPassword
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        [HttpGet]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult UserPassword(string k)
        {
            try
            {
                UserPasswordModel usermodel = new UserPasswordModel();
                if (k != null)
                {
                    Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                    user_master objuser_master = dbConnection.user_master.Where(x => x.crypto_key == k && x.is_deleted != true).FirstOrDefault();
                    if (objuser_master != null)
                    {
                        if (objuser_master.is_active == true)
                        {
                            usermodel.email = objuser_master.user_email;
                        }
                        else
                        {
                            TempData["ErrorUserMsg"] = "No account links to this link please try again";
                        }
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "No account links to this link please try again";
                    }
                }
                else
                {
                    TempData["ErrorUserMsg"] = "No account links to this link please try again";
                }
                return View(usermodel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// UserPassword
        /// Date: 10-July-2017
        /// Dev By: Bharat Katua
        /// For set user password 
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="userModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult UserPassword(UserPasswordModel userModel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                    user_master objuser_master = dbConnection.user_master.Where(x => x.user_email.Equals(userModel.email) && x.is_active == true).FirstOrDefault();
                    if (objuser_master != null)
                    {
                        if (userModel.new_password == userModel.confirm_password)
                        {
                            objuser_master.crypto_key = Utilities.GenerateKey();
                            objuser_master.password = Utilities.EncryptPassword(userModel.new_password, objuser_master.crypto_key);
                            //objstore_master.password = storepassModel.new_password;
                            dbConnection.SaveChanges();
                            TempData["PasswordResetMessage"] = "Your password has been changed. Please login with new password";
                            return RedirectToAction("Login");
                        }
                        else
                        {
                            TempData["ErrorUserMsg"] = "New password and confirm password not matched try again";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return View(userModel);
        }

        /// <summary>
        /// Forgot password
        /// Date: 10-July-2017
        /// Dev By: Bharat Katua
        /// For get forgot password Post
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ForgotPassword(LoginModel loginModel)
        {
            try
            {
                CommonService commonService = new CommonService();
                Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                user_master userMaster = dbConnection.user_master.Where(x => x.user_email.Equals(loginModel.Email) && x.user_type_id == (int)EnumUserType.Normal && x.is_active != false).FirstOrDefault();
                if (userMaster != null)
                {
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.FullName, userMaster.first_name);
                    parameters.Add(EmailTemplateParameters.Email, userMaster.crypto_key);
                    parameters.Add(EmailTemplateParameters.link, ConfigurationManager.AppSettings["SiteLink"] + "/Account/UserPassword");
                    parameters.Add(EmailTemplateParameters.Key1, userMaster.crypto_key);
                    bool emailResponse = commonService.SendEmail(userMaster.user_email, EmailTemplateNames.ForgotPasswordNew, parameters, null);
                    TempData["ForgetPasswordCSuccessMsg"] = "Please check your registered Email address.";
                }
                else
                {
                    TempData["ErrorMsg"] = "The email you have entered is incorrect";
                    return RedirectToAction("Login");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return RedirectToAction("Login");
        }

        /// <summary>
        /// Register a Member
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult Register()
        {
            objCommonService = new CommonService();
            objCMSService = new CMSService();
            MemberModel objMemberModel = new MemberModel();
            CMSModel objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SignUpCMS);
            objMemberModel.SignUpCMS = objCMSModel.cms_content;
            objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
            //objMemberModel.StateList = new SelectList("", 0, "Select State");

            //Client Feedback - 14Sep2017 (By Shoaib)
            //Entire Profile Form on Sign Up
            objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SubscriptionAgreement);
            objMemberModel.SubscriptionAgreement = objCMSModel.cms_content;
            objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SelfCertification);
            objMemberModel.SelfCertification = objCMSModel.cms_content;
            HomePageModel objHomePageModel = new HomePageModel();
            objCMSService = new CMSService();
            objHomePageModel = objCMSService.GetHomeCMSByID();
            if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
            {
                ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
            }

            return View(objMemberModel);
        }

        /// <summary>
        /// Forgot password
        /// Date: 10-July-2017
        /// Dev By: Bharat Katua
        /// For get forgot password Post
        /// Feedback1: Rewrap with an tt integration on 22 July 2018
        /// Feedback2:
        /// </summary>
        /// <param name="loginModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Register(MemberModel model)
        {
            SubscriptionService objSubscriptionService = new SubscriptionService();
            try
            {
                TTService objTTService = new TTService();
                MemberModel objMemberModel = new MemberModel();
                objMemberService = new MemberService();
                objUserService = new UserService();
                ModelState.Remove("birth_date");
                ModelState.Remove("gender");
                ModelState.Remove("profile_image_url");
                ModelState.Remove("AcceptsTerms");
                if (!model.IsTrial)
                {
                    ModelState.Remove("chkSubscriptionAgreement");
                    ModelState.Remove("chkSelfCertification");
                }
                if (model.postal_code == null)
                {
                    ModelState.Remove("postal_code");
                }
                if (ModelState.IsValid)
                {
                    model.created_on = System.DateTime.UtcNow;
                    objMemberModel = objMemberService.AddEditUser(model);
                    objMemberModel = objMemberService.GetUser(objMemberModel.user_id);
                    if (objMemberModel != null)
                    {
                        if (objMemberModel.user_id > 0)
                        {
                            //   if User subscribed then return execution for payment
                            if (model.subscription_id > 0 && model.subscription_id != 8)
                            {
                                Session["RegisterMemberID"] = objMemberModel.user_id;
                                return Json(new { success = true, id = objMemberModel.user_id }, JsonRequestBehavior.AllowGet);
                            }
                            string sAccountAllias = objUserService.GetAccountAlliasName(true, model.first_name.Substring(0, 1), model.last_name);
                            objMemberModel.AccountAllias = sAccountAllias;
                            if (!string.IsNullOrEmpty(model.user_email))
                            {
                                Dictionary<string, string> parameters = new Dictionary<string, string>();
                                parameters.Add(EmailTemplateParameters.FirstName, objMemberModel.first_name);
                                parameters.Add(EmailTemplateParameters.LastName, objMemberModel.last_name);
                                parameters.Add(EmailTemplateParameters.Email, objMemberModel.user_email);
                                parameters.Add(EmailTemplateParameters.Address, objMemberModel.address);
                                parameters.Add(EmailTemplateParameters.City, objMemberModel.city_name);
                                parameters.Add(EmailTemplateParameters.Country, objMemberModel.country_name);
                                parameters.Add(EmailTemplateParameters.State, objMemberModel.state_name);
                                parameters.Add(EmailTemplateParameters.ContactNumber, objMemberModel.contact_number);
                                parameters.Add(EmailTemplateParameters.AccountAllias, sAccountAllias);
                                if (model.subscription_id == 8)
                                {
                                    parameters.Add(EmailTemplateParameters.SubscriptionModeStr, "Demo");
                                }
                                else
                                {
                                    parameters.Add(EmailTemplateParameters.SubscriptionModeStr, "Challenge");
                                }
                                parameters.Add(EmailTemplateParameters.Password, model.password);
                                new CommonService().SendEmail("support@traderdock.com", EmailTemplateNames.SupportMailForFreeAccount, parameters, null);
                            }
                            Traderdock_DBEntities DbCon = new Traderdock_DBEntities();
                            var tmpQuery = (from UM in DbCon.user_master
                                            where UM.user_id == objMemberModel.user_id && UM.is_deleted == false
                                            select UM).First();
                            if (objMemberModel.rbPlatform == 1)
                            {
                                if (!string.IsNullOrEmpty(model.user_email))
                                {
                                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                                    parameters.Add(EmailTemplateParameters.link, System.Configuration.ConfigurationManager.AppSettings["SiteLink"] + "/Account/ConfirmRegistration?email=" + model.user_email + "&&key=" + objMemberModel.crypto_key);
                                    parameters.Add(EmailTemplateParameters.Email, model.user_email);
                                    parameters.Add(EmailTemplateParameters.FullName, model.first_name);
                                    parameters.Add(EmailTemplateParameters.Password, Utilities.DecryptPassword(tmpQuery.password, tmpQuery.crypto_key));//1May2018 - for adding password to email
                                    new CommonService().SendEmail(model.user_email, EmailTemplateNames.WelcomeFreeSubscription, parameters, null);
                                    bool bEmailCountUpdate = objCommonService.UpdateEmailSentCount(model.user_email);
                                }
                                TempData["SubscriptionId"] = objMemberModel.subscription_id;
                                UploadCSVData(objMemberModel.user_id, objMemberModel.subscription_id);
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(model.user_email))
                                {
                                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                                    parameters.Add(EmailTemplateParameters.link, System.Configuration.ConfigurationManager.AppSettings["SiteLink"] + "/Account/ConfirmRegistration?email=" + model.user_email + "&&key=" + objMemberModel.crypto_key);
                                    parameters.Add(EmailTemplateParameters.Email, model.user_email);
                                    parameters.Add(EmailTemplateParameters.FullName, model.first_name);
                                    parameters.Add(EmailTemplateParameters.Password, model.password);
                                    new CommonService().SendEmail(model.user_email, EmailTemplateNames.WelcomeFreeSubscriptionTT, parameters, null);
                                    objCommonService = new CommonService();
                                    bool bEmailCountUpdate = objCommonService.UpdateEmailSentCount(model.user_email);
                                }
                                TempData["SubscriptionId"] = objMemberModel.subscription_id;
                                bool bResult = objUserService.AddtoTTPlatform(objMemberModel, true);
                            }
                            SettingModel objSettingModel = new SettingModel();
                            objSettingService = new SettingsService();
                            objSettingModel = objSettingService.GetSettingByName("PageSize");

                            if (objSettingModel.setting_value != null)
                            {
                                Session["PageSize"] = objSettingModel.setting_value;
                            }
                            Session["MemberID"] = objMemberModel.user_id;
                            Session["MemberEmail"] = objMemberModel.user_email;
                            Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
                            var count = dbConnection.user_notifications.Where(m => m.user_id == objMemberModel.user_id).Count();
                            Session["UserNotificationCount"] = count;
                            Session["UserIDForNotification"] = objMemberModel.user_id;
                            DashboardService objDashboardService = new DashboardService();
                            Session["NotificationCount"] = objDashboardService.NotificationCount(objMemberModel.user_id);
                            Session["MemberName"] = objMemberModel.first_name;
                            if (!string.IsNullOrEmpty(objMemberModel.profile_image_url))
                            {
                                Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objMemberModel.profile_image_url);
                            }
                            else
                            {
                                Session["MemberImage"] = Convert.ToString(ConfigurationManager.AppSettings["DefaultImageURL"]);
                            }
                            FormsAuthentication.SetAuthCookie(objMemberModel.user_email, false);
                            objSubscriptionService = new SubscriptionService();
                            List<SubscriptionModel> Subscription_List = new List<SubscriptionModel>();
                            Subscription_List = objSubscriptionService.SubscriptionListForFront();
                            Session["front_subscription_id"] = Subscription_List[0].subscription_id;
                            Session["front_starting_balance"] = Subscription_List[0].starting_balance;
                            Session["front_price_per_month"] = Subscription_List[0].price_per_month;
                            TempData["SuccessMsg"] = "Please check your email address for verification.";
                            TempData["ErrorUserMsg"] = null;
                            return RedirectToAction("Login", "Account");
                        }
                        else
                        {
                            TempData["ErrorUserMsg"] = "Email already exist. please chosse another!";
                            return Json(new { success = false }, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                objCMSService = new CMSService();
                objCommonService = new CommonService();
                CMSModel objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SignUpCMS);
                model.SignUpCMS = objCMSModel.cms_content;
                model.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SubscriptionAgreement);
                model.SubscriptionAgreement = objCMSModel.cms_content;
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SelfCertification);
                model.SelfCertification = objCMSModel.cms_content;
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
            }
            catch (Exception ex)
            {
                new ServiceLayerException(ex);
            }
            return View(model);
        }

        /// <summary>
        /// The Complete Profile
        /// </summary>
        /// <returns>The <see cref="ActionResult"/></returns>
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult CompleteProfile()
        {
            try
            {
                MemberModel objMemberModel = new MemberModel();
                objCMSService = new CMSService();
                objCommonService = new CommonService();
                objMemberService = new MemberService();
                CMSModel objCMSModel = new CMSModel();
                if (Session["MemberID"] != null)
                {
                    objMemberModel = objMemberService.GetUser(Convert.ToInt32(Session["MemberID"]));
                }
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SubscriptionAgreement);
                objMemberModel.SubscriptionAgreement = objCMSModel.cms_content;
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SelfCertification);
                objMemberModel.SelfCertification = objCMSModel.cms_content;
                objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                //objMemberModel.chkSelfCertification = false;
                //objMemberModel.chkSubscriptionAgreement = false;
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objMemberModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// The CompleteProfile
        /// </summary>
        /// <param name="model">The model<see cref="MemberModel"/></param>
        /// <returns>The <see cref="ActionResult"/></returns>
        [HttpPost]
        public ActionResult CompleteProfile(MemberModel model)
        {
            try
            {
                model.IsTrial = true;
                objMemberService = new MemberService();
                objCommonService = new CommonService();
                objCMSService = new CMSService();
                CMSModel objCMSModel = new CMSModel();
                MemberModel objMemberModel = new MemberModel();
                ModelState.Remove("birth_date");
                ModelState.Remove("contact_number");
                ModelState.Remove("gender");
                ModelState.Remove("profile_image_url");
                ModelState.Remove("first_name");
                ModelState.Remove("last_name");
                ModelState.Remove("user_email");
                ModelState.Remove("password");
                ModelState.Remove("profile_image_url");
                ModelState.Remove("AcceptsTerms");
                if (ModelState.IsValid)
                {
                    model.created_on = System.DateTime.UtcNow;
                    model.modified_on = System.DateTime.UtcNow;
                    objMemberModel = objMemberService.AddEditUser(model);
                    if (objMemberModel != null)
                    {
                        if (!string.IsNullOrEmpty(model.user_email))
                        {
                            Dictionary<string, string> parameters = new Dictionary<string, string>();
                            parameters.Add(EmailTemplateParameters.FullName, model.first_name);
                            string sEncrEmail = Utilities.EncryptPassword(model.user_email, model.user_email);
                            parameters.Add(EmailTemplateParameters.Email, sEncrEmail);
                            parameters.Add(EmailTemplateParameters.Password, model.password);
                            parameters.Add(EmailTemplateParameters.Adminlink, System.Configuration.ConfigurationManager.AppSettings["SiteLink"] + "/Account/ConfirmRegistration?email=" + model.user_email + "&&key=" + model.crypto_key);
                            new CommonService().SendEmail(model.user_email, EmailTemplateNames.UserRegistration, parameters, null);
                        }
                        TempData["SubscriptionId"] = objMemberModel.subscription_id;
                        // UploadCSVData(model.user_id);
                        return RedirectToAction("Dashboard", "Home");
                    }
                }
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SubscriptionAgreement);
                model.SubscriptionAgreement = objCMSModel.cms_content;
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SelfCertification);
                model.SelfCertification = objCMSModel.cms_content;
                model.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objCMSModel = objCMSService.GetCMSByType((Int32)EnumCMSType.SignUpCMS);
                model.SignUpCMS = objCMSModel.cms_content;
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(model);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Log Off
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult LogOff()
        {
            Session["MemberID"] = null;
            Session["MemberEmail"] = null;
            Session["NotificationCount"] = null;
            Session["MemberName"] = null;
            Session["MemberImage"] = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Terms and Condition page
        /// </summary>
        /// <returns></returns>
        public ActionResult TermsAndConditions()
        {
            return View();
        }

        /// <summary>
        /// My Profile
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult MyProfile()
        {
            try
            {
                MemberModel objMemberModel = new MemberModel();
                objPayService = new PayService();
                objMemberService = new MemberService();
                if (Session["MemberID"] != null)
                {
                    objMemberModel = objMemberService.GetUser(Convert.ToInt32(Session["MemberID"]));
                }
                else
                {
                    return RedirectToAction("Login", "Account");
                }
                objCommonService = new CommonService();
                objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objMemberModel.GenderList = new SelectList(new List<SelectListItem>
                {
                new SelectListItem { Selected = false, Text = Enum.GetName(typeof(EnumGenderType),0), Value = bool.FalseString },
                new SelectListItem { Selected = false, Text = Enum.GetName(typeof(EnumGenderType),1), Value = bool.TrueString },
                }, "Value", "Text");
                objMemberModel.lstPaymentHistory = objPayService.GetUserPaymentHistory(objMemberModel.user_id);
                objMemberModel.objSubsciptionModel = objMemberService.GetSubsciptionDetails(objMemberModel.user_id);
                return View(objMemberModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// My Profile Post
        /// </summary>
        /// <param name="objMemberModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult MyProfile(MemberModel objMemberModel)
        {
            try
            {
                ModelState.Remove("password");
                ModelState.Remove("gender");
                ModelState.Remove("AcceptsTerms");
                ModelState.Remove("state_id");
                ModelState.Remove("chkSubscriptionAgreement");
                ModelState.Remove("chkSelfCertification");
                objCommonService = new CommonService();
                objMemberService = new MemberService();
                objPayService = new PayService();
                MemberModel objModel = new MemberModel();
                if (ModelState.IsValid)
                {
                    objMemberService = new MemberService();
                    objMemberModel.modified_by = Convert.ToInt32(Session["MemberID"]);
                    objMemberModel.modified_on = DateTime.UtcNow;
                    var _AttachmentPath = Server.MapPath("~") + "\\Images\\";
                    objModel = objMemberService.AddEditUser(objMemberModel, _AttachmentPath);
                    if (objModel != null)
                    {
                        Session["MemberEmail"] = objMemberModel.user_email;
                        Session["MemberName"] = objMemberModel.first_name;
                        if (!string.IsNullOrEmpty(objMemberModel.profile_image_url))
                        {
                            Session["MemberImage"] = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(objMemberModel.profile_image_url);
                        }
                        else
                        {
                            Session["MemberImage"] = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"]);
                        }
                        objMemberModel.FileName = objMemberModel.profile_image_url;
                        TempData["SuccessMsg"] = "Your profile is updated successfully.";
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "Please try again.";
                    }
                }
                objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                objMemberModel.GenderList = new SelectList(new List<SelectListItem>
                {
                new SelectListItem { Selected = false, Text = Enum.GetName(typeof(EnumGenderType),0), Value = bool.FalseString },
                new SelectListItem { Selected = false, Text = Enum.GetName(typeof(EnumGenderType),1), Value = bool.TrueString },
                }, "Value", "Text");

                int userID = Convert.ToInt32(Session["MemberID"]);
                objMemberModel.objSubsciptionModel = objMemberService.GetSubsciptionDetails(userID);

                objMemberModel.lstPaymentHistory = objPayService.GetUserPaymentHistory(objMemberModel.user_id);

                return View(objMemberModel);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// The GetProfileAddress
        /// </summary>
        /// <returns>The <see cref="JsonResult"/></returns>
        [HttpGet]
        public JsonResult GetProfileAddress()
        {
            try
            {
                bool success = false;
                MemberModel objMemberModel = new MemberModel();
                objCommonService = new CommonService();
                objMemberService = new MemberService();
                objPayService = new PayService();
                if (Session["MemberID"] != null)
                {
                    objMemberModel = objMemberService.GetUser(Convert.ToInt32(Session["MemberID"]));
                    success = true;
                }
                else
                {
                    success = false;
                }
                objMemberModel.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                return Json(new { success, Data = objMemberModel }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// The EditProfileAddress
        /// </summary>
        /// <param name="model">The model<see cref="MemberModel"/></param>
        /// <returns>The <see cref="JsonResult"/></returns>
        [HttpPost]
        public JsonResult EditProfileAddress(MemberModel model)
        {
            try
            {
                objCommonService = new CommonService();
                objMemberService = new MemberService();
                objPayService = new PayService();
                MemberModel objMemberModel = new MemberModel();
                if (model.country_id > 0 && !string.IsNullOrEmpty(model.state_name) && !string.IsNullOrEmpty(model.city_name) && !string.IsNullOrEmpty(model.postal_code) && !string.IsNullOrEmpty(model.address))
                {
                    objMemberService = new MemberService();
                    model.user_id = Convert.ToInt32(Session["MemberID"]);
                    model.modified_by = Convert.ToInt32(Session["MemberID"]);
                    model.modified_on = DateTime.UtcNow;
                    objMemberModel = objMemberService.AddEditUser(model);
                }
                model.CountryList = new SelectList(objCommonService.GetCountryList(), "country_id", "country_name");
                return Json(new { success = true, id = objMemberModel.user_id }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Upload Profile Image
        /// </summary>
        /// <returns> reutrns Uploaded File Name</returns>
        [HttpPost]
        public ActionResult UploadImage()
        {
            string FileNameImage = string.Empty;
            try
            {
                foreach (string file in Request.Files)
                {
                    HttpPostedFileBase fileContent = Request.Files[file];
                    FileNameImage = Utilities.SaveImageFile(fileContent);
                    if (TempData["SelectedProfile"] != null)
                    {
                        var FileName = TempData["SelectedProfile"].ToString();
                        var AttachmentPath = Server.MapPath("~") + "\\Images\\";
                        if (System.IO.File.Exists(Path.Combine(AttachmentPath, FileName)) && FileName != "Default_profile.png")
                        {
                            //System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                        }
                    }
                    TempData["SelectedProfile"] = FileNameImage;
                    TempData.Keep("SelectedProfile");
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            return Json(new { Data = FileNameImage }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Date: 30-June-2017
        /// Dev By: Hardik Savaliya
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="objChangePasswordModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordModel objChangePasswordModel)
        {
            try
            {
                ModelState.Remove("email");
                if (ModelState.IsValid)
                {
                    objMemberService = new MemberService();
                    bool bResult = false;
                    if (String.IsNullOrEmpty(Session["MemberID"] as string))
                        bResult = objMemberService.PasswordChange(objChangePasswordModel, Convert.ToInt32(Session["MemberID"]));

                    if (bResult)
                        TempData["CngPwdSMsg"] = "Your password was changed successfully.";
                    else
                        TempData["CngPwdEMsg"] = "Please enter correct password.";
                }
                return RedirectToAction("Login", "Account");
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Date: 30-June-2017
        /// Dev By: Hardik Savaliya
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult ChangePassword()
        {
            if (Session["MemberID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        /// <summary>
        /// Check User Email Exists
        /// Date: 30-June-2017  
        /// Dev By:Hardik Savaliya
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult CheckUserEmailExists(string email)
        {
            objMemberService = new MemberService();
            try
            {
                bool bFlag = objMemberService.CheckUserEmailExist(email);
                if (bFlag)
                {
                    //TempData["ErrorMsg"] = "Email is already in use. please choose another";
                    TempData["ErrorMsg"] = "Email id already exist.";
                }
                //return Json(bFlag, JsonRequestBehavior.AllowGet);
                return Json(new { success = bFlag }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Confirm Registration
        /// Date: 30-June-2017  
        /// Dev By:Hardik Savaliya
        /// Description: Confirm Registration come from registration mail
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="email"></param>
        /// <param name="key">The key<see cref="string"/></param>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult ConfirmRegistration(string email, string key)
        {
            objMemberService = new MemberService();
            try
            {
                bool bFlag = objMemberService.ConfirmRegistration(email, key);
                if (bFlag)
                {
                    TempData["ForgetPasswordCSuccessMsg"] = "Your account has been activated. Please Login!";
                }
                else
                {
                    TempData["ForgetPasswordCErrorMsg"] = "Your link has been expired.";
                }
                return RedirectToAction("Login");
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Date: 25-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Get States
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult GetStates(int id)
        {
            objCommonService = new CommonService();
            SelectList lstStates = new SelectList(objCommonService.GetStateList(id), "state_id", "state_name");
            return Json(new SelectList(lstStates, "value", "text"));
        }

        /// <summary>
        /// Upload Member Data into CSV
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="subId">The subId<see cref="long"/></param>
        /// <returns>The <see cref="JsonResult"/></returns>
        public JsonResult UploadCSVData(long userID = 0, long subId = 0)
        {
            bool IsLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"].ToString());
            bool IsRidhmicTestEnv = Convert.ToBoolean(ConfigurationManager.AppSettings["IsRidhmicTestEnv"].ToString());
            //var filePath = Server.MapPath("~") + "\\Files\\add_user_" + DateTime.Now.ToString("yyyy_MM_dd") + "_" + userID + ".csv";
            var filePath = Server.MapPath("~") + "\\Files\\add_user_" + userID + "_" + DateTime.Now.ToString("yyyy_MM_dd") + ".csv";

            MemberService objService = new MemberService();
            MemberModel objModel = new MemberModel();

            StringBuilder sb = new StringBuilder();
            //sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", "Email", "First Name", "Last Name", "Street Address", "City", "Country", "State", "Postal Code", "Phone Number", "Password") + Environment.NewLine);

            objModel = objService.GetDataForCSV(userID);
            char[] CharSeparator = new char[1];
            CharSeparator[0] = ',';
            if (!String.IsNullOrEmpty(objModel.address))
            {
                string strStreetAddress = objModel.address.Split(CharSeparator)[0];
                objModel.address = strStreetAddress;
            }

            string StrContactNumber = objModel.contact_number;
            if (objModel.contact_number == "NA")
                StrContactNumber = "44 7700 900708";

            //Feedback (10-Oct-2017) 
            //Added extra field for subscription number in the file uploaded to Rithmic
            //By Shoaib

            if (objModel.country_name.Trim().ToLower() != "usa" && objModel.country_name.Trim().ToLower() != "canada")
            {
                objModel.state_name = "";
            }

            sb.Append(string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}, {10}", objModel.user_email.Trim(), objModel.first_name.Trim(), objModel.last_name.Trim(), objModel.address.Trim(), objModel.city_name.Trim(), objModel.country_name.Trim(), objModel.state_name.Trim(), objModel.postal_code.Trim(), StrContactNumber.Trim(), objModel.password.Trim(), objModel.subscription_id.ToString()));

            System.IO.File.AppendAllText(filePath, sb.ToString());

            //for copy file one path to another
            string fileName = Path.GetFileName(filePath);
            string sourcePath = Server.MapPath("~") + "\\Files";

            // Use Path class to manipulate file and directory paths.
            string sourceFile = System.IO.Path.Combine(sourcePath, fileName);

            var host = "ritpz11300.11.rithmic.com";
            var port = 22;
            var username = "u3CiKQPhV";
            var password = "xBWKWRLW";

            string remoteRootDirectory;
            if (IsRidhmicTestEnv)
            {
                remoteRootDirectory = @"test/new_user/";
            }
            else
            {
                remoteRootDirectory = @"production/new_user/";
            }

            try
            {
                using (var client = new SftpClient(host, port, username, password))
                {
                    client.Connect();
                    //client.ChangeDirectory(@"test/in_flight/");   // for change directory on sftp
                    //client.ChangeDirectory(@"test/new_user/");   // for change directory on sftp
                    client.ChangeDirectory(remoteRootDirectory);   // for change directory on sftp
                    //string oldpath = @"test/in_flight/add_user_141.csv" + fileName;
                    //string newpath = @"test/new_user/add_user_141.csv" + fileName;
                    if (IsLive)
                    {
                        if (client.IsConnected)
                        {
                            using (var fileStream = new FileStream(sourceFile, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(sourceFile));
                            }
                            //client.RenameFile(oldpath, newpath); // for move file on sftp
                        }
                    }
                }

                //changed by shoaib - 5Aug2017
                //for proper account subscription message
                SubscriptionService objSubscriptionService = new SubscriptionService();
                //SubscriptionModel objSubscrModel = objSubscriptionService.GetSubscription(Convert.ToInt64(TempData["SubscriptionId"]));
                SubscriptionModel objSubscrModel = objSubscriptionService.GetSubscription(subId);

                //TempData["ModelTitle"] = objSubscrModel.subscription_name;
                //if (objSubscrModel.subscription_id == 8) // by shoaib - always getting 0 in subscription id because it's not set in GetSubscription - 30Apr2018
                if (subId == 8)
                {
                    TempData["ModelTitle"] = objSubscrModel.subscription_name;
                    TempData["ModelText"] = "Your 14 - Days Free Trial account is successfully created!!!";
                }
                else
                {
                    TempData["ModelTitle"] = "Payment Successful";
                    //TempData["ModelText"] = "Your " + objSubscrModel.subscription_name + " account is successfully created!!!";
                    TempData["ModelText"] = "Thank you for subscribing to the TraderDock Challenge. Your TraderDock account has been successfully created.Please check your email for instructions on how to get started.";
                }

                TempData["IsFreeTrialActivated"] = '2';
                //

                return Json(new { success = true }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                //throw ex;
                return Json(new { success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Activate user account, callback URL which Rithmic will use to notify us.
        /// By Shoaib - 10-Aug-2017
        /// </summary>
        /// <param name="Email"></param>
        /// <param name="FCM"></param>
        /// <param name="IB"></param>
        /// <param name="Account"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Activate(string Email, string FCM, string IB, string Account)
        {
            bool IsSuccess = false;
            try
            {
                //Email FCM IB Account
                objUserService = new UserService();
                IsSuccess = objUserService.UpdateUserAccountDetails(Email, FCM, IB, Account);
                return Json(new { success = IsSuccess.ToString().ToLower() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { success = "false" }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Resent/update Count
        /// <summary>
        /// Resend Mail
        /// By Hardik - 14-March-2019
        /// </summary>
        /// <param name="Email"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult ResendMail(string strEmail = "")
        {
            try
            {
                if (!string.IsNullOrEmpty(strEmail))
                {
                    objUserService = new UserService();
                    bool bResult = objUserService.ResendMail(strEmail);
                    if (bResult)
                    {
                        TempData["SuccessMsg"] = "Please check your email address for verification.";
                        return Json(new { result = bResult }, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        TempData["ErrorUserMsg"] = "You have reached max mail sending limit.";
                        TempData["EMailLimitExhaust"] = "You have reached max mail sending limit.";
                        return Json(new { result = bResult }, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    return Json(new { result = false }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception)
            {
                return Json(new { result = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}
