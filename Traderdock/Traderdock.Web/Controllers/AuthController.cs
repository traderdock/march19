﻿using System;
using System.Configuration;
using System.Web.Mvc;
using Traderdock.Entity.Model;

namespace Traderdock.Web.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Lock()
        {
            LoginModel objLoginModel = new LoginModel();
            return View(objLoginModel);
        }
       
        [HttpPost]
        public ActionResult Lock(LoginModel objLoginModel)
        {
            try
            {
                string SiteLink = ConfigurationManager.AppSettings["SiteLink"];
                if (ModelState.IsValid)
                {
                    if (objLoginModel.Email.ToLower().Equals("david@traderdock.com") && objLoginModel.Password.Equals("8d$k@+DcUe"))
                    {
                        Session["IsAuthorised"] = "true";
                        Response.Redirect(SiteLink + "/home/index", false);
                    }
                    else if (objLoginModel.Email.ToLower().Equals("guest@traderdock.com") && objLoginModel.Password.Equals("traderdock2017$"))
                    {
                        Session["IsAuthorised"] = "true";
                        Response.Redirect(SiteLink + "/home/index", false);
                    }
                    else if (objLoginModel.Email.ToLower().Equals("admin@traderdock.com") && objLoginModel.Password.Equals("traderdock2017$"))
                    {
                        Session["IsAuthorised"] = "true";
                        Response.Redirect(SiteLink + "/home/index", false);
                    }
                    else
                    {
                        return View(objLoginModel);
                    }                    
                }
                else
                {
                    return View(objLoginModel);
                }
            }
            catch (Exception)
            {
                return View(objLoginModel);
            }
            return View(objLoginModel);
        }

        [HttpGet]
        public ActionResult notfound()
        {
            return View();
        }
        [HttpGet]
        public ActionResult internalserver()
        {
            return View();
        }
        
    }
}