﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;

namespace Traderdock.Web.Controllers
{
    public class FAQController : BaseController
    {
        #region Global Variables
        FAQCategoryService objFAQCategoryService;
        #endregion

        #region FAQ  Category List
        /// <summary>
        /// FAQ Category Member List
        /// </summary>
        /// <returns></returns>
        ///[Route("faq/category")]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult category()
        {
            try
            {
                MainFAQModel objModel = new MainFAQModel();
                objFAQCategoryService = new FAQCategoryService();
                CMSService objCMSService = new CMSService();
                //IList<FAQCategoryModel> lstFaqCategoryModel = new List<FAQCategoryModel>();
                objModel.objFaqCategoryModel = objFAQCategoryService.GetFaqCategories();
                objModel.objFaqCMSModel = objCMSService.GetFaqCMSByID((Int32)EnumCMSType.FAQSBannerText);
                foreach (var FaqCategory in objModel.objFaqCategoryModel)
                {
                    FaqCategory.category_image = Utilities.GetImagePath() + "/Images/" + Utilities.GetImageName(FaqCategory.category_image);
                }
                HomePageModel objHomePageModel = new HomePageModel();
                objCMSService = new CMSService();
                objHomePageModel = objCMSService.GetHomeCMSByID();
                if (!string.IsNullOrEmpty(objHomePageModel.LayoutFooterText))
                {
                    ViewBag.LayoutFooterText = objHomePageModel.LayoutFooterText;
                }
                return View(objModel);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FAQ Detail View
        /// <summary>
        /// FAQ Detailed View
        /// </summary>
        /// <param FAQ_category_ID="id"></param>
        /// <returns></returns>
        //[Route("faq/view")]
        [OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
        public ActionResult view(int catid, int subcatid, string search)
        {
            try
            {
                FAQMemberService objFAQMemberService = new FAQMemberService();
                objFAQCategoryService = new FAQCategoryService();
                FAQViewModel objFAQViewModel = new FAQViewModel();
                if (catid > 0)
                {
                    objFAQViewModel = objFAQMemberService.FAQListBySubCategoryID(subcatid, search);
                }
                if (objFAQViewModel != null)
                    return View(objFAQViewModel);
                else
                    return RedirectToAction("category", "faq");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult search(FAQViewModel objFAQViewModel, int id)
        {
            try
            {
                FAQMemberService objFAQMemberService = new FAQMemberService();
                objFAQCategoryService = new FAQCategoryService();
                FAQViewModel objFAQCategoryMemModel = new FAQViewModel();
                if (id > 0)
                {
                    objFAQCategoryMemModel = objFAQMemberService.FAQListBySubCategoryID(id, null);
                }
                return RedirectToAction("View", new { id = id });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get FAQ Category
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> GetFAQCategory()
        {
            objFAQCategoryService = new FAQCategoryService();
            List<SelectListItem> lstSelectListItem = new List<SelectListItem>();
            IList<FAQCategoryModel> lstFaqMasterModel = new List<FAQCategoryModel>();
            try
            {
                lstFaqMasterModel = objFAQCategoryService.GetFaqCategories();
                foreach (var item in lstFaqMasterModel)
                {
                    var data = new SelectListItem { Text = item.category_title, Value = item.faq_category_id.ToString() };
                    lstSelectListItem.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return lstSelectListItem;
        }
        #endregion
    }
}