﻿using System;
using System.Net;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Traderdock.Common;
using Traderdock.Web.QuartzServcie;

namespace Traderdock.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AntiForgeryConfig.SuppressIdentityHeuristicChecks = false;
            EndSubscriptionNotificationJob objEndJob = new EndSubscriptionNotificationJob();
            //objEndJob.Start();
            //update product
            GetTTJob objTTJob = new GetTTJob();
            objTTJob.Start();
            //GetTTPositionData objGetTTPositionData = new GetTTPositionData();
            //objGetTTPositionData.Start();
            //GetTTClosePositionData objTTClosePositionData = new GetTTClosePositionData();
            //objTTClosePositionData.Start();
            GetTTInstrumentData objGetTTInstrumentData = new GetTTInstrumentData();
            objGetTTInstrumentData.Start();
            EmailServiceJob objEmailService = new EmailServiceJob();
            //objEmailService.Start();
            GetTTGetFillJob objGetTTGetFillJob = new GetTTGetFillJob();
            objGetTTGetFillJob.Start();
            TTAccountListJob objTTAccountListJob = new TTAccountListJob();
            objTTAccountListJob.Start();
        }

        public static string ImageFileLocation
        {
            get
            {
                return Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ImageBaseURL"]);
            }
        }
    }
}
