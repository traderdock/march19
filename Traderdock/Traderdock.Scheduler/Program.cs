﻿using LumenWorks.Framework.IO.Csv;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.Services;

namespace TestApp
{
    class Program
    {
        /// <summary>
        /// Direct Read CSV From Ridhmik Server And Entry to DB
        /// </summary>
        /// <returns></returns>
        public static void UpdateTradingDBOld()
        {
            UserService objUserService = new UserService();
            UserTransactionModel model = new UserTransactionModel();
            bool IsRidhmicTestEnv = false;
            string strUserId = "tlohdivad@gmail.com";
            string strPassword = "1DMI_OIY";
            string Date = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyyMMdd");
            string File_name = "TraderDock_audit_fills_commissions_matched_symbol_";
            string ACReportFile = "TraderDock_audit_fills_commissions_match_reports_";

            Console.WriteLine("Fetching data ....");

            try
            {
                File_name = File_name + Date + ".csv";
                WebClient client = new WebClient();
                string remoteDirectory;
                if (IsRidhmicTestEnv)
                {
                    remoteDirectory = "https://rithmic.com/TraderDock/RithmicTest/";
                }
                else
                {
                    remoteDirectory = "https://rithmic.com/TraderDock/RithmicPaperTrading/";
                }

                Uri SymbolURL = new Uri(remoteDirectory + File_name);// Daynamic Date To Get File
                client.Credentials = new NetworkCredential(strUserId, strPassword);
                Stream stream = client.OpenRead(SymbolURL);                
                DataTable csvTable = new DataTable();
                using (CsvReader csvReader = new CsvReader(new StreamReader(stream), true))
                {
                    csvTable.Load(csvReader);
                    Console.WriteLine("Account info updated ....");
                }
                
                string[] data;
                foreach (DataRow row in csvTable.Rows)
                {
                    data = row.ItemArray.Select(x => x.ToString()).ToArray();
                    model = objUserService.csvtodb(data, DateTime.UtcNow.Date.AddDays(-1).ToString());
                }

                ACReportFile = ACReportFile + Date + ".csv";
                WebClient ACclient = new WebClient();
                Uri ACur = new Uri("https://rithmic.com/TraderDock/RithmicTest/" + ACReportFile);// Daynamic Date To Get File
                ACclient.Credentials = new NetworkCredential(strUserId, strPassword);
                Stream streamAC = ACclient.OpenRead(ACur);                
                //client.DownloadFileAsync(ur, @"C:\Users\Aakash.ITCPU054\Downloads\Documents\TraderDock_audit_fills_commissions_matched_symbol_20170801.csv");
                DataTable csvACTable = new DataTable();
                using (CsvReader csvACReader = new CsvReader(new StreamReader(streamAC), true))
                {
                    csvACTable.Load(csvACReader);
                }
                string[] dataAC;
                foreach (DataRow row in csvACTable.Rows)
                {
                    dataAC = row.ItemArray.Select(x => x.ToString()).ToArray();
                    model = objUserService.csvtodbACReport(dataAC, DateTime.UtcNow.Date.AddDays(-1).ToString());
                    Console.WriteLine("Trading info updated ....");
                }      
            }
            catch (Exception ex)
            {
                throw ex;
            }            
        }

        public static void UpdateDashboardData(string StrRithmicURL="", string StrReportType = "1")
        {
            //Report Type: 1= Symbol, 2=Account
            UserService objUserService = new UserService();
            UserTransactionModel model = new UserTransactionModel();
            string strUserId = "tlohdivad@gmail.com";
            string strPassword = "1DMI_OIY";
            //string Date = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyyMMdd");

            Console.WriteLine("Fetching data ....");

            try
            {
                WebClient ACclient = new WebClient();
                Uri AccountURL = new Uri(StrRithmicURL);
                ACclient.Credentials = new NetworkCredential(strUserId, strPassword);
                Stream streamAC = ACclient.OpenRead(AccountURL);

                DataTable dbDashboard = new DataTable();
                using (CsvReader csvDashboardReader = new CsvReader(new StreamReader(streamAC), true))
                {
                    dbDashboard.Load(csvDashboardReader);
                }

                string[] dataAC;
                foreach (DataRow row in dbDashboard.Rows)
                {
                    dataAC = row.ItemArray.Select(x => x.ToString()).ToArray();
                    if (StrReportType == "1")
                    {
                        //model = objUserService.csvtodb(dataAC, DateTime.UtcNow.Date.AddDays(-1).ToString());
                        model = objUserService.csvtodb(dataAC, DateTime.UtcNow.Date.ToString());
                    }
                    else if (StrReportType == "2")
                    {
                        //model = objUserService.csvtodbACReport(dataAC, DateTime.UtcNow.Date.AddDays(-1).ToString());
                        model = objUserService.csvtodbACReport(dataAC, DateTime.UtcNow.Date.ToString());
                    }

                    Console.WriteLine("Trading info updated ....");
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        static void Main(string[] args)
        {
            //string StrReportDate = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyyMMdd");
            string StrReportDate = DateTime.UtcNow.Date.ToString("yyyyMMdd");
            //StrReportDate = "20180301";
            string remoteDirectory;
            bool IsRidhmicTestEnv = false;

            if (IsRidhmicTestEnv)
            {
                remoteDirectory = "https://rithmic.com/TraderDock/RithmicTest/";
            }
            else
            {
                remoteDirectory = "https://rithmic.com/TraderDock/RithmicPaperTrading/";
            }

            string StrSymbolFileName = remoteDirectory + "TraderDock_audit_fills_commissions_matched_symbol_" + StrReportDate + ".csv";
            string StrAccountFileName = remoteDirectory + "TraderDock_audit_fills_commissions_match_reports_" + StrReportDate + ".csv";

            UpdateDashboardData(StrSymbolFileName, "1");
            Console.WriteLine("Symbol report fetched and updated");
            UpdateDashboardData(StrAccountFileName, "2");
            Console.WriteLine("Account report fetched and updated");
            //Console.ReadLine();

            //UpdateTradingDBOld();
        }
    }
}

