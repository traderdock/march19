﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.TTModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class TTService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        public string sEnvironment = System.Configuration.ConfigurationManager.AppSettings["TTEnvironment"];
        public string sTTAPIKey = System.Configuration.ConfigurationManager.AppSettings["TTAPIKey"];
        public string sTTAPIKeySet = System.Configuration.ConfigurationManager.AppSettings["TTAPIKeySet"];
        public string sTTAPIGrantType = System.Configuration.ConfigurationManager.AppSettings["TTAPIGrantType"];
        public string URL = TTAPIEndPoints.baseURL;
        #endregion

        #region Get Token
        /// <summary>
        /// Date: 08-Aug-2018
        /// Dev By: Hardik Savaliya
        /// Description: Get Token
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public TokenResponseClass GetToken()
        {
            TokenResponseClass objTokenResponseClass = new TokenResponseClass();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                var objKeyValueList = new List<KeyValuePair<string, string>>();
                objKeyValueList.Add(new KeyValuePair<string, string>("grant_type", sTTAPIGrantType));
                objKeyValueList.Add(new KeyValuePair<string, string>("app_key", sTTAPIKeySet));
                var objRequest = new HttpRequestMessage(HttpMethod.Post, URL + "ttid/" + sEnvironment + TTAPIEndPoints.tokenURL)
                { Content = new FormUrlEncodedContent(objKeyValueList) };
                var objResponse = client.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var data = objResult.Content.ReadAsStringAsync();
                    objTokenResponseClass = objResult.Content.ReadAsAsync<TokenResponseClass>().Result;
                    if (objTokenResponseClass.status.ToLower() == "ok")
                    {
                        return objTokenResponseClass;
                        //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
                        //                "TTToken",
                        //                DateTime.Now,
                        //                DateTime.Now.AddHours(11), //12 hours validated
                        //                true,
                        //                objTokenResponseClass.access_token,
                        //                FormsAuthentication.FormsCookiePath);
                        //// Encrypt the ticket.
                        //string encTicket = FormsAuthentication.Encrypt(ticket);
                        //// Create the cookie.
                        //return (new HttpCookie(FormsAuthentication.FormsCookieName, encTicket));
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Add Account        
        /// <summary>
        /// Adds the account.
        /// </summary>
        /// <param name="lUserId">The l user identifier.</param>
        /// <param name="objToken">The object token.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        public List<string> AddAccount(long lUserId, TokenResponseClass objToken)
        {
            List<string> lstResponse = new List<string>();
            AccountCreationInputModel objAccountCreationInputModel = new AccountCreationInputModel();
            AccountCreationInputModelResponse objAccountCreationInputModelResponse = new AccountCreationInputModelResponse();
            List<user_master> objUserMaster = dbConnection.user_master.Where(x => x.created_on.Month == DateTime.UtcNow.Month).ToList();
            objAccountCreationInputModel.accountName = "TD" + (System.DateTime.UtcNow.Year).ToString().Substring(2)
                                                            + (System.DateTime.UtcNow.Month).ToString("##")
                                                            + (objUserMaster.Count + 1).ToString();
            objAccountCreationInputModel.description = "Cretaed in " + sEnvironment;
            objAccountCreationInputModel.accountType = 1;// 1: Routing (external clearing) - A clearing account which is sent to the exchange on all orders. * 3: Non-Routing - Cannot send orders, used for back-office purposes. * 4: Routing (internal sub-account) - Defines the account as a sub-account which sends parent account to the exchange. * 5: Broker Matching
            objAccountCreationInputModel.parentAccountId = 2632;
            objAccountCreationInputModel.AOTCType = 1;
            objAccountCreationInputModel.tradingAllowedSelf = 1;
            objAccountCreationInputModel.tradingAllowedSubAccts = 0;
            objAccountCreationInputModel.disabledByAutoliquidate = 1;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objToken.access_token.ToString());
                    var postTask = client.PostAsJsonAsync<AccountCreationInputModel>("/risk/" + sEnvironment + TTAPIEndPoints.CreateAccountURL, objAccountCreationInputModel);
                    postTask.Wait();
                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        objAccountCreationInputModelResponse = result.Content.ReadAsAsync<AccountCreationInputModelResponse>().Result;
                    }
                }
                if (objAccountCreationInputModelResponse.account.Count() > 0)
                {
                    lstResponse.Add(objAccountCreationInputModelResponse.account.FirstOrDefault().id.ToString());
                    lstResponse.Add(objAccountCreationInputModel.accountName);
                }
                return lstResponse;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Add User        
        /// <summary>
        /// Adds the user.
        /// </summary>
        /// <param name="lUserId">The l user identifier.</param>
        /// <param name="lAccountId">The l account identifier.</param>
        /// <param name="objMemberModel">The object member model.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long AddUser(long lUserId, string lAccountId, UserMasterModel objMemberModel, TokenResponseClass objTokenResponseClass)
        {
            UserCreationInputModel objUserCreationInputModel = new UserCreationInputModel();
            UserCreationInputModelResponse objUserCreationInputModelResponse = new UserCreationInputModelResponse();
            AddressInput objAddress = new AddressInput();
            CountryResponseClass objCountryResponseClass = new CountryResponseClass();
            objCountryResponseClass = GetCountry(objTokenResponseClass);
            if (objCountryResponseClass.countries.Count > 0)
            {
                country_master objCountryMaster = dbConnection.country_master.Where(x => x.country_id == objMemberModel.country_id).FirstOrDefault();
                Country objCountry = objCountryResponseClass.countries.Where(x => x.countryName.Trim().ToLower().Equals(objCountryMaster.country_name.Trim().ToLower())).FirstOrDefault();
                if (!string.IsNullOrEmpty(objCountry.countryName))
                {
                    objAddress.countryId = objCountry.countryId;
                    if (objAddress.countryId == 236)
                        objAddress.stateId =
                    objAddress.city = objMemberModel.city_name;
                    if (objMemberModel.address.Length > 64)
                    {
                        objAddress.streetAddress1 = objMemberModel.address.Substring(0, 64);
                        objAddress.streetAddress2 = objMemberModel.address.Substring(64);
                    }
                    else
                    {
                        objAddress.streetAddress1 = objMemberModel.address;
                    }
                    objAddress.primaryPhoneNumber = objMemberModel.mobile_number;
                    //objAddress.zipcode = objMemberModel.postal_code;
                }
            }
            else
            {
                return 0;
            }

            objUserCreationInputModel.firstName = objMemberModel.first_name;
            objUserCreationInputModel.lastName = objMemberModel.last_name;
            objUserCreationInputModel.email = objMemberModel.user_email;
            List<user_master> objUserMaster = dbConnection.user_master.Where(x => x.created_on.Month == DateTime.UtcNow.Month).ToList();
            objUserCreationInputModel.alias = "TD" + (DateTime.UtcNow.Year).ToString().Substring(2)
                                                    + (DateTime.UtcNow.Month).ToString("##")
                                                    + (objUserMaster.Count + 1).ToString();
            objUserCreationInputModel.tradeMode = 2;
            objUserCreationInputModel.advancedOptions = 0;
            objUserCreationInputModel.userInactivityLockReset = 1;
            objUserCreationInputModel.address = objAddress;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Add("accept", "application/json");
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objTokenResponseClass.access_token.ToString());
                    var postTask = client.PostAsJsonAsync<UserCreationInputModel>("risk/" + sEnvironment + TTAPIEndPoints.CreateUserURL, objUserCreationInputModel);
                    postTask.Wait();
                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        objUserCreationInputModelResponse = result.Content.ReadAsAsync<UserCreationInputModelResponse>().Result;
                    }
                }
                return objUserCreationInputModelResponse.user.FirstOrDefault().id;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Country        
        /// <summary>
        /// Gets the country.
        /// </summary>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public CountryResponseClass GetCountry(TokenResponseClass objTokenResponseClass)
        {
            CountryResponseClass objCountryResponseClass = new CountryResponseClass();
            try
            {
                HttpClient clientCountry = new HttpClient();
                clientCountry.BaseAddress = new Uri(URL);
                clientCountry.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                clientCountry.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "risk/" + sEnvironment + TTAPIEndPoints.GetCountryList);
                var objResponse = clientCountry.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objCountryResponseClass = objResult.Content.ReadAsAsync<CountryResponseClass>().Result;
                    if (objCountryResponseClass.status.ToLower() == "ok")
                    {
                        return objCountryResponseClass;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Map Account User        
        /// <summary>
        /// Maps the account user.
        /// </summary>
        /// <param name="lUserId">The l user identifier.</param>
        /// <param name="lTTAccountId">The l tt account identifier.</param>
        /// <param name="lTTUserId">The l tt user identifier.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long MapAccountUser(long lUserId, string lTTAccountId, long lTTUserId, TokenResponseClass objTokenResponseClass)
        {
            AddUserToAccountModel objAddUserToAccountModel = new AddUserToAccountModel();
            objAddUserToAccountModel.accountId = Convert.ToInt32(lTTAccountId);
            objAddUserToAccountModel.userId = Convert.ToInt32(lTTUserId);
            objAddUserToAccountModel.blockCrossOrders = 1;
            objAddUserToAccountModel.stagedOrders = 0;
            objAddUserToAccountModel.stagedOrdersManageOthers = 0;
            objAddUserToAccountModel.stageOrdersManageOwn = 1;
            objAddUserToAccountModel.wholesaleOrders = 0;
            objAddUserToAccountModel.deleteOrders = 1;
            objAddUserToAccountModel.dmaOrders = 1;
            objAddUserToAccountModel.GtcGtDate = 1;
            objAddUserToAccountModel.market = 1;
            objAddUserToAccountModel.mobile = 1;
            objAddUserToAccountModel.autospreader = 1;
            objAddUserToAccountModel.aggregator = 1;
            objAddUserToAccountModel.ttAlgoSDK = 1;
            objAddUserToAccountModel.adl = 1;
            objAddUserToAccountModel.ADLAlgoApprovalRequired = 1;
            objAddUserToAccountModel.ttSyntheticOrderTypes = 1;
            objAddUserToAccountModel.ttTimed = 1;
            objAddUserToAccountModel.ttStop = 1;
            objAddUserToAccountModel.ttIfTouched = 1;
            objAddUserToAccountModel.ttTrailingLimit = 1;
            objAddUserToAccountModel.ttWithATick = 1;
            objAddUserToAccountModel.ttOCO = 1;
            objAddUserToAccountModel.ttBracket = 1;
            objAddUserToAccountModel.ttIceberg = 1;
            objAddUserToAccountModel.orderByVolatility = 1;
            objAddUserToAccountModel.ttTimeSliced = 1;
            objAddUserToAccountModel.ttRetry = 1;
            objAddUserToAccountModel.manualFills = 0;
            objAddUserToAccountModel.autoRFQCross = 0;
            objAddUserToAccountModel.stagedOrderRiskCheckType = 1;
            objAddUserToAccountModel.confirmFills = 1;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Add("accept", "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                    var postTask = client.PostAsJsonAsync<AddUserToAccountModel>("risk/" + sEnvironment + TTAPIEndPoints.CreateAccountUserURL, objAddUserToAccountModel);
                    postTask.Wait();
                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        string sResult = result.Content.ReadAsStringAsync().Result;
                        objAddUserToAccountModel = result.Content.ReadAsAsync<AddUserToAccountModel>().Result;
                    }
                }
                return objAddUserToAccountModel.accountId;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Product List from TT        
        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public List<TTProduct> GetProducts(TokenResponseClass objTokenResponseClass)
        {
            ProductResponseClass objProductResponseClass = new ProductResponseClass();
            try
            {
                HttpClient clientCountry = new HttpClient();
                clientCountry.BaseAddress = new Uri(URL);
                clientCountry.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "pds/" + sEnvironment + TTAPIEndPoints.ProductList);
                var objResponse = clientCountry.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objProductResponseClass = objResult.Content.ReadAsAsync<ProductResponseClass>().Result;
                    if (objProductResponseClass.status.ToLower() == "ok")
                    {
                        return objProductResponseClass.products;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Map Risk Limit Product        
        /// <summary>
        /// Maps the account market.
        /// </summary>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="lTTAccountId">The l tt account identifier.</param>
        /// <param name="lTTUserId">The l tt user identifier.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long MapAccountMarket(long user_id, string lTTAccountId, long lTTUserId, TokenResponseClass objTokenResponseClass)
        {
            PagedList<TTProduct> objProductResponseClass = GetTradeAllowbleProduct();
            tt_user_setting objTTUserSetting = dbConnection.tt_user_setting.FirstOrDefault();
            foreach (var item in objProductResponseClass.Where(x => x.is_selected == true))
            {
                AcctProductRiskLimitInputModelResponse objAcctProductRiskLimitInputModelResponse = new AcctProductRiskLimitInputModelResponse();
                AcctProductRiskLimitInputModel objAcctProductRiskLimitInputModel = new AcctProductRiskLimitInputModel();
                objAcctProductRiskLimitInputModel.accountId = Convert.ToInt32(lTTAccountId);
                accountProductRiskLimitRequest objaccountProductRiskLimit = new accountProductRiskLimitRequest();
                objaccountProductRiskLimit.marketId = Convert.ToInt32(7); //will be static for CME only
                objaccountProductRiskLimit.productTypeId = item.productTypeId;
                objaccountProductRiskLimit.productId = item.id;
                objaccountProductRiskLimit.productSymbol = item.symbol;
                objaccountProductRiskLimit.tradeOutAllowed = 1;//objTTUserSetting.tradeOutAllowed;
                objaccountProductRiskLimit.maxPos = 5;//objTTUserSetting.maxPos;
                objaccountProductRiskLimit.maxShortPosPerContract = 5;// objTTUserSetting.maxShortPosPerContract;
                objaccountProductRiskLimit.maxLongPosPerContract = 5;// objTTUserSetting.maxLongPosPerContract;
                objaccountProductRiskLimit.maxLongPos = 5;// objTTUserSetting.maxLongPos;
                objaccountProductRiskLimit.maxShortPos = 5;// objTTUserSetting.maxShortPos;
                objaccountProductRiskLimit.outright_tradingAllowed = 1;// objTTUserSetting.outright_tradingAllowed;
                objaccountProductRiskLimit.outright_maxOrderQty = 10;// objTTUserSetting.outright_maxOrderQty;
                objaccountProductRiskLimit.outright_appliedMarginPercent = 100;// objTTUserSetting.outright_appliedMarginPercent;
                objaccountProductRiskLimit.outright_aggressiveOnly = 0;// objTTUserSetting.outright_aggressiveOnly;
                objaccountProductRiskLimit.outright_aggressiveOnlyPercentage = 0;// objTTUserSetting.outright_aggressiveOnlyPercentage;
                objaccountProductRiskLimit.spread_trading_allowed = 0;// objTTUserSetting.spread_trading_allowed;
                objaccountProductRiskLimit.spread_appliedMarginPercent = 100;// objTTUserSetting.spread_appliedMarginPercent;
                objaccountProductRiskLimit.spread_aggressiveOnly = 0;/// objTTUserSetting.spread_aggressiveOnly;
                objaccountProductRiskLimit.spread_aggressiveOnlyPercentage = 0;// objTTUserSetting.spread_aggressiveOnlyPercentage;
                objAcctProductRiskLimitInputModel.accountProductRiskLimit = objaccountProductRiskLimit;
                try
                {
                    using (var client = new HttpClient())
                    {
                        client.BaseAddress = new Uri(URL);
                        client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                        client.DefaultRequestHeaders.Add("accept", "application/json");
                        client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                        var postTask = client.PostAsJsonAsync<AcctProductRiskLimitInputModel>("risk/" + sEnvironment + TTAPIEndPoints.AccountRiskLimitProduct, objAcctProductRiskLimitInputModel);
                        postTask.Wait();
                        var result = postTask.Result;
                        if (result.IsSuccessStatusCode)
                        {
                            objAcctProductRiskLimitInputModelResponse = result.Content.ReadAsAsync<AcctProductRiskLimitInputModelResponse>().Result;


                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return 0;
        }
        #endregion

        #region Get allowable Product list        
        /// <summary>
        /// Gets the trade allowble product.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="statusFilter">The status filter.</param>
        /// <param name="UserId">The user identifier.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public PagedList<TTProduct> GetTradeAllowbleProduct(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            ProductResponseClass objProductResponseClass = new ProductResponseClass();
            List<tt_product_master> lstTTProductMaster = new List<tt_product_master>();
            List<TTProduct> lstTTProduct = new List<TTProduct>();
            lstTTProductMaster = dbConnection.tt_product_master.ToList();
            if (!string.IsNullOrEmpty(statusFilter))
            {
                if (statusFilter == "Yes")
                    lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == true).ToList();
                else if (statusFilter == "No")
                    lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == false).ToList();
                else
                    lstTTProductMaster = dbConnection.tt_product_master.ToList();
            }
            if (!string.IsNullOrEmpty(filter))
            {
                lstTTProductMaster = dbConnection.tt_product_master.Where(x => x.symbol.ToLower().Contains(filter.ToLower())).ToList();
            }
            foreach (var item in lstTTProductMaster)
            {
                TTProduct objTTProduct = new TTProduct();
                objTTProduct.tt_primary_product_id = item.tt_primary_product_id;
                objTTProduct.familyId = item.familyId;
                objTTProduct.id = item.id;
                objTTProduct.name = item.name;
                objTTProduct.productTypeId = item.productTypeId.Value;
                objTTProduct.symbol = item.symbol;
                if (item.is_selected != null)
                {
                    objTTProduct.is_selected = item.is_selected.Value;
                }
                else
                {
                    objTTProduct.is_selected = false;
                }
                objProductResponseClass.products.Add(objTTProduct);
            }
            var result = new PagedList<TTProduct>(objProductResponseClass.products, pageIndex, pageSize);
            return result;
        }
        #endregion

        #region Market Risk Limit Setting        
        /// <summary>
        /// Maps the market risk limit setting.
        /// </summary>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="lTTAccountId">The l tt account identifier.</param>
        /// <param name="lTTUserId">The l tt user identifier.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <param name="objTTGetAccountResponse">The object tt get account response.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long MapMarketRiskLimitSetting(long user_id, string lTTAccountId, long lTTUserId, TokenResponseClass objTokenResponseClass, TTGetAccountResponse objTTGetAccountResponse)
        {
            tt_user_setting objTTUserSetting = dbConnection.tt_user_setting.FirstOrDefault();
            AcctRiskLimitSettingsInputModel objAcctRiskLimitSettingsInputModel = new AcctRiskLimitSettingsInputModel();
            AcctRiskLimitSettingsInputModelResponse objAcctRiskLimitSettingsInputModelResponse = new AcctRiskLimitSettingsInputModelResponse();
            accountRiskLimitSettings objaccountRiskLimitSettings = new accountRiskLimitSettings();
            objAcctRiskLimitSettingsInputModel.accountId = Convert.ToInt32(lTTAccountId);
            objaccountRiskLimitSettings.id = objTTGetAccountResponse.riskLimitSettings.id;
            objaccountRiskLimitSettings.revision = objTTGetAccountResponse.riskLimitSettings.revision;
            objaccountRiskLimitSettings.applyLimitsToWholesaleOrders = objTTUserSetting.applyLimitsToWholesaleOrders;
            objaccountRiskLimitSettings.noLimits = objTTUserSetting.noLimits;
            objAcctRiskLimitSettingsInputModel.accountRiskLimitSettings = objaccountRiskLimitSettings;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Add("accept", "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                    var postTask = client.PostAsJsonAsync("risk/" + sEnvironment + TTAPIEndPoints.AccountRiskLimitSetting, objAcctRiskLimitSettingsInputModel);
                    postTask.Wait();
                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        string sResult = result.Content.ReadAsStringAsync().Result;
                        objAcctRiskLimitSettingsInputModelResponse = result.Content.ReadAsAsync<AcctRiskLimitSettingsInputModelResponse>().Result;
                    }
                }
                return objAcctRiskLimitSettingsInputModelResponse.accountId;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Map Risk Setting        
        /// <summary>
        /// Maps the risk setting.
        /// </summary>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="lTTAccountId">The l tt account identifier.</param>
        /// <param name="lTTUserId">The l tt user identifier.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <param name="objTTGetAccountResponse">The object tt get account response.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long MapRiskSetting(long user_id, string lTTAccountId, long lTTUserId, TokenResponseClass objTokenResponseClass, TTGetAccountResponse objTTGetAccountResponse)
        {
            tt_user_setting objTTUserSetting = dbConnection.tt_user_setting.FirstOrDefault();
            AcctRiskSettingsInputModel objAcctRiskSettingsInputModel = new AcctRiskSettingsInputModel();
            AccountRiskSettings objAccountRiskSettings = new AccountRiskSettings();
            AcctRiskSettingsInputModelResponse objAcctRiskSettingsInputModelResponse = new AcctRiskSettingsInputModelResponse();
            objAcctRiskSettingsInputModel.accountId = Convert.ToInt32(lTTAccountId);
            objAccountRiskSettings.id = objTTGetAccountResponse.riskSettings.id;
            objAccountRiskSettings.revision = objTTGetAccountResponse.riskSettings.revision;
            objAccountRiskSettings.dailyCreditLimit = 1000;// objTTUserSetting.dailyCreditLimit;
            objAccountRiskSettings.positionResetTypeId = 2;// objTTUserSetting.positionResetTypeId;
            objAccountRiskSettings.positionResetTime = 0;// objTTUserSetting.positionResetTime;
            objAccountRiskSettings.positionResetTimezone = "GMT-Europe/Dublin";// objTTUserSetting.positionResetTimezone;
            objAccountRiskSettings.dailyCreditLimitRuleId = 1;// objTTUserSetting.dailyCreditLimitRuleId;
            objAccountRiskSettings.applyToWholesaleOrders = 1;// objTTUserSetting.applyToWholesaleOrders;
            objAccountRiskSettings.checkCredit = 1;// objTTUserSetting.checkCredit;
            objAccountRiskSettings.resetPosition = 1;// objTTUserSetting.resetPosition;
            objAccountRiskSettings.resetPositionInherited = 1;// objTTUserSetting.resetPositionInherited;
            objAccountRiskSettings.resetSpreadPosition = 1;// objTTUserSetting.resetSpreadPosition;
            objAccountRiskSettings.checkCreditLoss = 1;// objTTUserSetting.checkCreditLoss;
            objAccountRiskSettings.autoLiquidateInherited = 1;// objTTUserSetting.autoLiquidateInherited;
            objAccountRiskSettings.checkCreditLoss_percentOfDailyCredit = 1;// objTTUserSetting.checkCreditLoss_percentOfDailyCredit;
            objAccountRiskSettings.checkCreditLoss_disableTrading = 1;// objTTUserSetting.checkCreditLoss_disableTrading;
            objAccountRiskSettings.checkCreditLoss_cancelWorking = 1;// objTTUserSetting.checkCreditLoss_cancelWorking;
            objAccountRiskSettings.checkCreditLoss_autoliquidateOpenPositions = 1;// objTTUserSetting.checkCreditLoss_autoliquidateOpenPositions;
            objAcctRiskSettingsInputModel.accountRiskSettings = objAccountRiskSettings;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Add("accept", "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                    var postTask = client.PostAsJsonAsync("risk/" + sEnvironment + TTAPIEndPoints.AccountRiskSetting, objAcctRiskSettingsInputModel);
                    postTask.Wait();
                    var result = postTask.Result;

                    if (result.IsSuccessStatusCode)
                    {
                        string sResult = result.Content.ReadAsStringAsync().Result;
                        objAcctRiskSettingsInputModelResponse = result.Content.ReadAsAsync<AcctRiskSettingsInputModelResponse>().Result;
                    }
                }
                return objAcctRiskSettingsInputModelResponse.accountId;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Active In Active Product        
        /// <summary>
        /// Actives the in active product.
        /// </summary>
        /// <param name="lIds">The l ids.</param>
        /// <param name="LoginUserId">The login user identifier.</param>
        /// <param name="IsActiveRequest">if set to <c>true</c> [is active request].</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool ActiveInActiveProduct(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<tt_product_master> objTTProduct = (from UM in dbConnection.tt_product_master
                                                        where (lIds).Contains(UM.tt_primary_product_id)
                                                        select UM).AsEnumerable().ToList();
                objTTProduct.ForEach(x => x.modified_by = LoginUserId);
                objTTProduct.ForEach(x => x.modified_on = DateTime.UtcNow);
                objTTProduct.ForEach(x => x.is_selected = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Data of FII
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objToken"></param>
        /// <param name="accountId"></param>
        /// <param name="maxTimestamp"></param>
        /// <param name="minTimestamp"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public List<FillTTResponse> GetFII(TokenResponseClass objToken, long lAccountID = 0, string maxTimestamp = "", string minTimestamp = "", string productId = "")
        {
            GetFIIResponse objGetFIIResponse = new GetFIIResponse();
            try
            {

                LogServices objLogService = new LogServices();
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objToken.access_token.ToString());
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "ledger/" + sEnvironment + TTAPIEndPoints.GetFII + "/?accountId=" + lAccountID + "&maxTimestamp=" + maxTimestamp + "&productId=" + productId + "&minTimestamp=" + minTimestamp);
                var objResponse = client.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objGetFIIResponse = objResult.Content.ReadAsAsync<GetFIIResponse>().Result;
                    if (objGetFIIResponse.status.ToLower() == "ok")
                    {
                        APICallLogModel objAPICallLogModel = new APICallLogModel();
                        objAPICallLogModel.api_enum = (Int32)EnumAPI.FetchGetFill;
                        objAPICallLogModel.description = "For Account ID " + lAccountID + "for this date range(max,min)" + maxTimestamp + "," + minTimestamp + "able to fetch " + objGetFIIResponse.fills.Count();
                        objAPICallLogModel.endpoint = TTAPIEndPoints.GetFII;
                        objAPICallLogModel.is_success = true;
                        bool bResult = objLogService.SaveAPILog(objAPICallLogModel);
                        return objGetFIIResponse.fills;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Get Data of Position        
        /// <summary>
        /// Gets the position.
        /// </summary>
        /// <param name="objToken">The object token.</param>
        /// <param name="accountIds">The account ids.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public List<TTPositionResponse> GetPosition(TokenResponseClass objToken, string accountIds = "")
        {
            PositionResponse objPositionResponse = new PositionResponse();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objToken.access_token.ToString());
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "monitor/" + sEnvironment + TTAPIEndPoints.GetPosition + "/??accountIds=" + accountIds);
                var objResponse = client.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objPositionResponse = objResult.Content.ReadAsAsync<PositionResponse>().Result;
                    if (objPositionResponse.status.ToLower() == "ok")
                    {
                        return objPositionResponse.positions;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Get Data of Account
        /// <summary>
        /// Gets the account.
        /// </summary>
        /// <param name="objToken">The object token.</param>
        /// <param name="accountIds">The account ids.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public TTGetAccountResponse GetAccount(TokenResponseClass objToken, string accountIds = "")
        {
            GetAccountResponseClass objGetAccountResponseClass = new GetAccountResponseClass();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objToken.access_token.ToString());
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "risk/" + sEnvironment + TTAPIEndPoints.GetAccount + "/" + accountIds);
                var objResponse = client.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objGetAccountResponseClass = objResult.Content.ReadAsAsync<GetAccountResponseClass>().Result;
                    if (objGetAccountResponseClass.status.ToLower() == "ok")
                    {
                        return objGetAccountResponseClass.account.FirstOrDefault();
                    }
                    return null;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Map Trade Restrictions        
        /// <summary>
        /// Maps the trade restrictions.
        /// </summary>
        /// <param name="user_id">The user identifier.</param>
        /// <param name="lTTAccountId">The l tt account identifier.</param>
        /// <param name="lTTUserId">The l tt user identifier.</param>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        public bool MapTradeRestrictions(long user_id, string lTTAccountId, long lTTUserId, TokenResponseClass objTokenResponseClass)
        {
            tt_user_setting objTTUserSetting = dbConnection.tt_user_setting.FirstOrDefault();
            TradeRestructionsRequestClass objTradeRestructionsRequestClass = new TradeRestructionsRequestClass();
            AccountTradeRestrictions objAccountTradeRestrictions = new AccountTradeRestrictions();
            objTradeRestructionsRequestClass.accountId = Convert.ToInt32(lTTAccountId);
            objAccountTradeRestrictions.applyRestrictions = 1;
            objAccountTradeRestrictions.blockCrossOrders = 1;
            objAccountTradeRestrictions.dmaOrders = 1;
            objAccountTradeRestrictions.gtcGtDate = 1;
            objAccountTradeRestrictions.market = 1;
            objAccountTradeRestrictions.autospreader = 1;
            objAccountTradeRestrictions.aggregator = 1;
            objAccountTradeRestrictions.ttAlgoSDK = 1;
            objAccountTradeRestrictions.adl = 1;
            objAccountTradeRestrictions.ADLAlgoApprovalRequired = 1;
            objAccountTradeRestrictions.mobile = 1;
            objAccountTradeRestrictions.ttSyntheticOrderTypes = 1;
            objAccountTradeRestrictions.ttTimeSliced = 1;
            objAccountTradeRestrictions.ttRetry = 1;
            objAccountTradeRestrictions.ttTimed = 1;
            objAccountTradeRestrictions.ttStop = 1;
            objAccountTradeRestrictions.ttIfTouched = 1;
            objAccountTradeRestrictions.ttTrailingLimit = 1;
            objAccountTradeRestrictions.ttOCO = 1;
            objAccountTradeRestrictions.ttBracket = 1;
            objAccountTradeRestrictions.ttIceberg = 1;
            objAccountTradeRestrictions.ttWithATick = 1;
            objAccountTradeRestrictions.manualFills = 0;
            objAccountTradeRestrictions.confirmFills = 1;
            objTradeRestructionsRequestClass.accountTradeRestrictions = objAccountTradeRestrictions;
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(URL);
                    client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                    client.DefaultRequestHeaders.Add("accept", "application/json");
                    client.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                    var postTask = client.PostAsJsonAsync("risk/" + sEnvironment + TTAPIEndPoints.AccountTradeRestrictions, objTradeRestructionsRequestClass);
                    postTask.Wait();
                    var result = postTask.Result;
                    if (result.IsSuccessStatusCode)
                    {
                        string sResult = result.Content.ReadAsStringAsync().Result;
                        objTradeRestructionsRequestClass = result.Content.ReadAsAsync<TradeRestructionsRequestClass>().Result;
                        if (objTradeRestructionsRequestClass.accountId > 0)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Data of Position        
        /// <summary>
        /// Gets the instruments.
        /// </summary>
        /// <param name="objToken">The object token.</param>
        /// <param name="accountIds">The account ids.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        public List<InstrumentResponse> GetInstruments(TokenResponseClass objToken, string accountIds = "")
        {
            List<InstrumentResponse> objInstrumentResponse = new List<InstrumentResponse>();
            string sproductId = string.Empty, sProductTypeId = string.Empty;
            TTInstrumentResponseClass objTTInstrumentResponseClass = new TTInstrumentResponseClass();
            LogServices objLogService = new LogServices();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                client.DefaultRequestHeaders.Add("accept", "application/json");
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", objToken.access_token.ToString());
                List<tt_product_master> objTTProductMaster = dbConnection.tt_product_master.Where(x => x.is_selected == true).ToList();
                foreach (var item in objTTProductMaster)
                {
                    int milliseconds = 2000;
                    Thread.Sleep(milliseconds);
                    var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "pds/" + sEnvironment + TTAPIEndPoints.Instruments + "?productId=" + item.id + "&productTypeId=" + item.productTypeId);
                    var objResponse = client.SendAsync(objRequest);
                    var objResult = objResponse.Result;
                    Task.WaitAll();
                    if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        objTTInstrumentResponseClass = objResult.Content.ReadAsAsync<TTInstrumentResponseClass>().Result;
                        if (objTTInstrumentResponseClass.status.ToLower() == "ok")
                        {
                            objInstrumentResponse.AddRange(objTTInstrumentResponseClass.instruments);
                            APICallLogModel objAPICallLogModel = new APICallLogModel();
                            objAPICallLogModel.api_enum = (Int32)EnumAPI.FetchInstruments;
                            objAPICallLogModel.endpoint = TTAPIEndPoints.Instruments;
                            objAPICallLogModel.is_success = true;
                            objAPICallLogModel.description = "For Product ID=" + item.id + ", Product TypeI ID=" + item.productTypeId + " getting total " + objTTInstrumentResponseClass.instruments.Count + " instruments";
                            bool btrue = objLogService.SaveAPILog(objAPICallLogModel);
                        }
                    }
                    else
                    {
                        return objInstrumentResponse;
                    }
                    Task.WaitAll();
                }
                return objInstrumentResponse;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Save TT Upload File        
        /// <summary>
        /// Saves the tt upload file.
        /// </summary>
        /// <param name="lstContractWisefilter">The LST contract wisefilter.</param>
        /// <param name="lstError">The LST error.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public long SaveTTUploadFile(List<TTFileInputModel> lstContractWisefilter, out List<KeyValuePair<string, string>> lstError)
        {
            lstError = new List<KeyValuePair<string, string>>();
            if (lstContractWisefilter.Count > 0)
            {
                //map to an database object 
                List<tt_fill_master_manual> lstTTMaster = new List<tt_fill_master_manual>();
                List<user_platform> lstUserMaster = dbConnection.user_platform.ToList();
                List<user_subscription> lstUserSubscription = dbConnection.user_subscription.Where(x => x.is_current == true).ToList();
                foreach (var item in lstContractWisefilter)
                {
                    tt_fill_master_manual objTTMaster = new tt_fill_master_manual();
                    objTTMaster.buy_price = item.BuyPrice;
                    objTTMaster.buy_sell = item.BuySell;
                    objTTMaster.account_no = item.Account;
                    objTTMaster.contract = item.Contract;
                    objTTMaster.date = item.Date.ToLocalTime();
                    objTTMaster.exchange = item.Exchange;
                    objTTMaster.fill_qty = item.FillQty;
                    objTTMaster.manual_fill = item.ManualFill;
                    objTTMaster.net_position = item.NetPosition;
                    objTTMaster.parent_id = item.ParentID;
                    objTTMaster.pf = item.PF;
                    objTTMaster.price = item.Price;
                    objTTMaster.sell_price = item.SellPrice;
                    objTTMaster.time = DateTime.ParseExact(System.DateTime.UtcNow.Date.ToString("dd/MM/yyyy") + " " + item.Time_str, "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                    objTTMaster.trade_entry = item.TradeEntry;
                    objTTMaster.trade_exit = item.TradeExit;
                    objTTMaster.trade_number = item.TradeNumber;
                    objTTMaster.total_traded_qty_trade = item.total_traded_qty_trade;
                    objTTMaster.average_buy_price = item.AverageBuyPrice;
                    objTTMaster.average_sell_price = item.AverageSellPrice;
                    objTTMaster.realised_pnl_tick = item.realised_pnl_tick;
                    objTTMaster.realised_pnl_usd_without_commision = item.realised_pnl_usd_without_commision;
                    objTTMaster.realised_pnl_usd_with_commision = item.realised_pnl_usd_with_commision;
                    objTTMaster.unrealised_pnl_tick = item.unrealised_pnl_tick;
                    objTTMaster.commission = item.commission;
                    objTTMaster.w_trades = item.w_trades;
                    objTTMaster.l_trades = item.l_trades;
                    objTTMaster.s_trades = item.s_trades;
                    objTTMaster.ttorder_id = item.TTOrderID;
                    objTTMaster.high_pnl = item.high_pnl;
                    objTTMaster.low_pnl = item.low_pnl;
                    objTTMaster.cumulative_pnl = item.cumulative_pnl;
                    objTTMaster.cumalitve_pnl_without_commision = item.cumalitve_pnl_without_commision;
                    objTTMaster.created_on = DateTime.UtcNow;
                    //bind user subscription id and user id
                    //commented due to issue on 22 feb 
                    //x.account_id == item.AccountID 
                    var objUserMaster = lstUserMaster.Where(x => x.account_alias == item.Account).ToList();
                    if (objUserMaster.Count() > 0)
                    {
                        var objSubscriptionMaster = lstUserSubscription.Where(x => x.user_id == objUserMaster.FirstOrDefault().user_id).ToList();
                        if (objSubscriptionMaster.Count() > 0)
                        {
                            if (objUserMaster.FirstOrDefault().user_id != null || objSubscriptionMaster.FirstOrDefault().user_subscription_id != null)
                            {
                                objTTMaster.user_id = objUserMaster.FirstOrDefault().user_id;
                                objTTMaster.user_subscription_id = objSubscriptionMaster.FirstOrDefault().user_subscription_id;
                            }
                        }
                        else
                        {
                            lstError.Add(new KeyValuePair<string, string>(item.Account, "ACCOUNT"));
                        }
                    }
                    else
                    {
                        lstError.Add(new KeyValuePair<string, string>(item.Account, "USER"));
                    }
                    lstTTMaster.Add(objTTMaster);
                }
                List<tt_fill_master_manual> lstFaultyTTMaster = new List<tt_fill_master_manual>();
                List<tt_fill_master_manual> lstTTMasterRevised = new List<tt_fill_master_manual>();
                if (lstTTMaster.Any(x => x.user_id > 0) || lstTTMaster.Any(x => x.user_subscription_id > 0))
                {
                    lstFaultyTTMaster = lstTTMaster.Where(x => x.user_id < 0 || x.user_subscription_id < 0 || x.user_id == null || x.user_subscription_id == null).ToList();
                    lstTTMasterRevised = lstTTMaster.Where(x => x.user_id > 0 || x.user_subscription_id > 0).ToList();
                }
                List<tt_fill_master_temp> lstTTFillMasterTemp = new List<tt_fill_master_temp>();
                List<tt_fill_master_temp> lstTTFillMaster = dbConnection.tt_fill_master_temp.ToList();
                foreach (var item in lstTTMasterRevised)
                {
                    lstTTFillMasterTemp.AddRange(lstTTFillMaster.Where(x => x.account == item.account_no).ToList());
                }
                dbConnection.tt_fill_master_temp.RemoveRange(lstTTFillMasterTemp);
                //Get an saved record and avoid to duplicate them (created on and tt orderid)
                //List<tt_fill_master_manual> lstTTDbObj = dbConnection.tt_fill_master_manual.ToList();
                //List<tt_fill_master_manual> lstfilterdFillMaster = lstTTMaster.Where(x => lstTTDbObj.Select(y => y.ttorder_id).ToList().Any(n => n.Contains(x.ttorder_id))).ToList();
                //if (lstfilterdFillMaster.Count() == 0)
                //{
                dbConnection.tt_fill_master_manual.AddRange(lstTTMasterRevised);
                dbConnection.SaveChanges();
                //Current balance master
                List<tt_fill_master_manual> lstTTCBMaster = new List<tt_fill_master_manual>();
                lstTTCBMaster = dbConnection.tt_fill_master_manual.ToList();
                List<string> lstCBAccountDist = lstTTCBMaster.Select(x => x.account_no).Distinct().ToList();
                foreach (var sAccountName in lstCBAccountDist)
                {
                    List<tt_fill_master_manual> lstCBDateWisefilter = new List<tt_fill_master_manual>();
                    lstCBDateWisefilter = lstTTCBMaster.Where(x => x.account_no == sAccountName).ToList();
                    List<DateTime> lstDate = lstCBDateWisefilter.Select(x => x.date.Value).Distinct().ToList();
                    decimal dCurrentBalance = 0;
                    foreach (var sDate in lstDate)
                    {
                        List<tt_fill_master_manual> lstCBAccountWisefilter = new List<tt_fill_master_manual>();
                        lstCBAccountWisefilter = lstCBDateWisefilter.Where(x => x.date.Value.Date == sDate.Date).ToList();
                        List<int> lstTradeNumber = lstCBAccountWisefilter.Select(x => x.trade_number).Distinct().OrderByDescending(x => x).ToList();
                        decimal dCumalitive = 0;
                        foreach (int iTradeNumber in lstTradeNumber)
                        {
                            List<tt_fill_master_manual> lstCBContractWisefilter = new List<tt_fill_master_manual>();
                            lstCBContractWisefilter = lstCBAccountWisefilter.Where(x => x.trade_number == iTradeNumber).OrderBy(x => x.time).ToList();
                            dCumalitive = dCumalitive + lstCBContractWisefilter.FirstOrDefault().realised_pnl_usd_with_commision.Value;
                        }
                        dCurrentBalance = dCumalitive + dCurrentBalance;
                        lstCBAccountWisefilter.ForEach(x => x.current_balance_pnl = dCurrentBalance);
                    }
                }
                dbConnection.SaveChanges();
                return 1;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Get Fill Report Manual          
        /// <summary>
        /// Gets the fill report manual.
        /// </summary>
        /// <param name="sAccountId">The s account identifier.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sFromDate">The s from date.</param>
        /// <param name="sToDate">The s to date.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public PagedList<TTFileInputModel> GetFillReportManual(string sAccountId = "", int pageIndex = 0, int pageSize = int.MaxValue, string sFromDate = "", string sToDate = "")
        {
            TTFileModel objTTFileModel = new TTFileModel();
            List<TTFileInputModel> lstFileInputModel = new List<TTFileInputModel>();
            List<tt_fill_master_manual> lstTTFillMaster = dbConnection.tt_fill_master_manual.ToList();
            if (!string.IsNullOrEmpty(sAccountId))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => x.account_no == sAccountId).ToList();
            }
            if (!string.IsNullOrEmpty(sFromDate))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => x.date.Value.Date >= Convert.ToDateTime(sFromDate).Date).ToList();
            }
            if (!string.IsNullOrEmpty(sToDate))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => x.date.Value.Date <= Convert.ToDateTime(sToDate).Date).ToList();
            }
            TTFileInputModel objTTFilnputModel = new TTFileInputModel();
            foreach (var item in lstTTFillMaster)
            {
                objTTFilnputModel = new TTFileInputModel();
                objTTFilnputModel.fillid = item.fillid;
                objTTFilnputModel.Account = item.account_no;
                objTTFilnputModel.AverageBuyPrice = item.average_buy_price.Value;
                objTTFilnputModel.AverageSellPrice = item.average_sell_price.Value;
                objTTFilnputModel.BS = item.buy_sell.ToString();
                if (item.buy_price != null)
                    objTTFilnputModel.BuyPrice = item.buy_price.Value;
                if (item.buy_sell != null)
                    objTTFilnputModel.BuySell = item.buy_sell.Value;
                if (item.commission != null)
                    objTTFilnputModel.commission = item.commission.Value;
                objTTFilnputModel.Contract = item.contract;
                //objTTFilnputModel.CurrentUser = item.CurrentUser;
                //objTTFilnputModel.current_balance = item.ba;
                if (item.date != null)
                {
                    objTTFilnputModel.Date = item.date.Value;
                    objTTFilnputModel.Date_str = item.date.Value.ToString("dd-MMM-yyyy");
                }
                objTTFilnputModel.Exchange = item.exchange;
                if (item.fill_qty != null)
                    objTTFilnputModel.FillQty = item.fill_qty.Value;
                objTTFilnputModel.l_trades = item.l_trades;
                objTTFilnputModel.ManualFill = item.manual_fill;
                if (item.net_position != null)
                    objTTFilnputModel.NetPosition = item.net_position.Value;
                //objTTFilnputModel.Originator = item.Originator;
                objTTFilnputModel.ParentID = item.parent_id;
                objTTFilnputModel.PF = item.pf;
                if (item.price != null)
                    objTTFilnputModel.Price = item.price.Value;
                if (item.realised_pnl_tick != null)
                    objTTFilnputModel.realised_pnl_tick = item.realised_pnl_tick.Value;
                if (item.realised_pnl_usd_without_commision != null)
                    objTTFilnputModel.realised_pnl_usd_without_commision = item.realised_pnl_usd_without_commision.Value;
                if (item.realised_pnl_usd_with_commision != null)
                    objTTFilnputModel.realised_pnl_usd_with_commision = item.realised_pnl_usd_with_commision.Value;
                //objTTFilnputModel.Route = item.Route;
                if (item.sell_price != null)
                    objTTFilnputModel.SellPrice = item.sell_price.Value;
                objTTFilnputModel.s_trades = item.s_trades;
                //objTTFilnputModel.threshold_value = item.threshold_value;
                objTTFilnputModel.Time = item.time.Value;
                if (item.total_traded_qty_trade != null)
                    objTTFilnputModel.total_traded_qty_trade = item.total_traded_qty_trade.Value;
                objTTFilnputModel.TradeEntry = item.trade_entry.Value;
                objTTFilnputModel.TradeExit = item.trade_exit.Value;
                objTTFilnputModel.TradeNumber = item.trade_number;
                objTTFilnputModel.TTOrderID = item.ttorder_id;
                if (item.unrealised_pnl_tick != null)
                    objTTFilnputModel.unrealised_pnl_tick = item.unrealised_pnl_tick.Value;
                objTTFilnputModel.w_trades = item.w_trades;
                lstFileInputModel.Add(objTTFilnputModel);

            }
            objTTFileModel.fill_list = lstFileInputModel;
            var result = new PagedList<TTFileInputModel>(lstFileInputModel, pageIndex, pageSize);
            return result;
        }
        #endregion

        #region Delete Fills        
        /// <summary>
        /// Deletes the fills.
        /// </summary>
        /// <param name="tagIds">The tag ids.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool DeleteFills(List<long> tagIds)
        {
            try
            {
                bool bResult = false;
                List<tt_fill_master_manual> objTTFillMaster = (from UM in dbConnection.tt_fill_master_manual
                                                               where (tagIds).Contains(UM.fillid)
                                                               select UM).ToList();
                dbConnection.tt_fill_master_manual.RemoveRange(objTTFillMaster);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get User TTReport Update        
        /// <summary>
        /// Gets the user tt report update.
        /// </summary>
        /// <param name="filter">The filter.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="statusFilter">The status filter.</param>
        /// <param name="UserId">The user identifier.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        /// Dev By: Hardik Savaliya
        public PagedList<TTUserModel> GetUserTTReportUpdate(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                ProductResponseClass objProductResponseClass = new ProductResponseClass();
                List<TTUserModel> lstTTUserModel = new List<TTUserModel>();
                TTUserModel objTTUserModel;
                List<tt_fill_master_manual> lstTTFillMasterManual = new List<tt_fill_master_manual>();
                var dataFillMasterManual = dbConnection.tt_fill_master_manual.OrderBy(x => x.fillid).ToList().GroupBy(x => x.user_id).ToList();
                foreach (var item in dataFillMasterManual)
                {
                    objTTUserModel = new TTUserModel();
                    objTTUserModel.fillID = item.FirstOrDefault().fillid;
                    objTTUserModel.userID = item.FirstOrDefault().user_id.Value;
                    objTTUserModel.accountID = item.FirstOrDefault().account_no;
                    objTTUserModel.dtFirstUpdateTime = item.FirstOrDefault().created_on.Value;
                    objTTUserModel.dtLastUpdateTime = item.LastOrDefault().created_on.Value;
                    objTTUserModel.sFirstUpdate = item.FirstOrDefault().created_on.Value.ToString();
                    objTTUserModel.sLastUpdate = item.LastOrDefault().created_on.Value.ToString();
                    objTTUserModel.name = item.FirstOrDefault().user_master.first_name + " " + item.FirstOrDefault().user_master.last_name;
                    objTTUserModel.PlatformID = (Int32)EnumTradingPlatform.TradingTechnologies;
                    objTTUserModel.sPlatform = "Trading Technologies";
                    lstTTUserModel.Add(objTTUserModel);
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    //lstTTUserModel = lstTTUserModel.tt_product_master.Where(x => x.symbol.ToLower().Contains(filter.ToLower())).ToList();
                }
                var result = new PagedList<TTUserModel>(lstTTUserModel, pageIndex, pageSize);
                return result;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Save TT Json original upload        
        /// <summary>
        /// Saves the ttjson.
        /// </summary>
        /// <param name="sJson">
        /// The json</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool saveTTJSON(string sJson)
        {
            tt_fill_upload_history objTTFillUploadHistory = new tt_fill_upload_history();
            objTTFillUploadHistory.created_on = System.DateTime.UtcNow;
            objTTFillUploadHistory.json = sJson;
            dbConnection.tt_fill_upload_history.Add(objTTFillUploadHistory);
            dbConnection.SaveChanges();
            return true;
        }
        #endregion

        #region Save TT Json Temp        
        /// <summary>
        /// Saves the temporary ttjson.
        /// </summary>
        /// <param name="lstFileFinalRow">The LST file final row.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool saveTempTTJSON(List<TTFileInputModel> lstFileFinalRow)
        {
            if (lstFileFinalRow.Count > 0)
            {
                //map to an database object 
                List<tt_fill_master_temp> lstTTMaster = new List<tt_fill_master_temp>();
                List<user_master> lstUserMaster = dbConnection.user_master.Where(x => x.is_deleted == false
                                                && x.user_type_id == (Int32)EnumUserType.Normal
                                                && x.trading_platform == (Int32)EnumTradingPlatform.TradingTechnologies
                                                && !string.IsNullOrEmpty(x.account_number)).ToList();
                List<user_subscription> lstUserSubscription = dbConnection.user_subscription.Where(x => x.is_current == true).ToList();

                foreach (var item in lstFileFinalRow)
                {
                    tt_fill_master_temp objTTMaster = new tt_fill_master_temp();
                    objTTMaster.date = item.Date;
                    objTTMaster.time = item.Time;
                    objTTMaster.exchange = item.Exchange;
                    objTTMaster.contract = item.Contract;
                    objTTMaster.buy_sell = item.BS;
                    objTTMaster.fill_qty = Convert.ToInt32(item.FillQty);
                    objTTMaster.price = item.Price;
                    objTTMaster.pf = item.PF;
                    //objTTMaster.route = item.Route;
                    objTTMaster.account = item.Account;
                    objTTMaster.originator = item.Originator;
                    //objTTMaster.currentuser = item.CurrentUser;
                    objTTMaster.ttorderid = item.TTOrderID;
                    objTTMaster.parentid = item.ParentID;
                    //objTTMaster.manualfill = item.ManualFill;
                    objTTMaster.createdon = DateTime.UtcNow;
                    lstTTMaster.Add(objTTMaster);
                }
                dbConnection.tt_fill_master_temp.RemoveRange(dbConnection.tt_fill_master_temp.ToList());
                dbConnection.SaveChanges();
                dbConnection.tt_fill_master_temp.AddRange(lstTTMaster);
                dbConnection.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Get TT Raw Json        
        /// <summary>
        /// Gets the tt raw json.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public List<TTFileInputModel> getTTRawJson()
        {
            List<TTFileInputModel> lstTTFileInputModel = new List<TTFileInputModel>();
            List<tt_fill_master_temp> lstTTFileMasterTemp = dbConnection.tt_fill_master_temp.ToList();
            TTFileInputModel objTTFileInputModel = new TTFileInputModel();
            foreach (var item in lstTTFileMasterTemp)
            {
                objTTFileInputModel = new TTFileInputModel();
                tt_fill_master_temp objTTMaster = new tt_fill_master_temp();
                objTTFileInputModel.Date = item.date.Value;
                objTTFileInputModel.Time = item.time.Value;
                objTTFileInputModel.Exchange = item.exchange;
                objTTFileInputModel.Contract = item.contract;
                objTTFileInputModel.BS = item.buy_sell;
                objTTFileInputModel.FillQty = Convert.ToInt32(item.fill_qty);
                objTTFileInputModel.Price = item.price.Value;
                objTTFileInputModel.PF = item.pf;
                //objTTFileInputModel.Route = item.route;
                objTTFileInputModel.Account = item.account;
                objTTFileInputModel.Originator = item.originator;
                //objTTFileInputModel.CurrentUser = item.currentuser;
                objTTFileInputModel.TTOrderID = item.ttorderid;
                objTTFileInputModel.ParentID = item.parentid;
                //objTTFileInputModel.ManualFill = item.manualfill;
                objTTFileInputModel.created_on = DateTime.UtcNow;
                lstTTFileInputModel.Add(objTTFileInputModel);
            }
            return lstTTFileInputModel;
        }
        #endregion

        #region Get Fill Report API          
        /// <summary>
        /// Gets the fill report API.
        /// </summary>
        /// <param name="sAccountId">The s account identifier.</param>
        /// <param name="pageIndex">Index of the page.</param>
        /// <param name="pageSize">Size of the page.</param>
        /// <param name="sFromDate">The s from date.</param>
        /// <param name="sToDate">The s to date.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public PagedList<ViewFill> GetFillReportAPI(string sAccountId = "", int pageIndex = 0, int pageSize = int.MaxValue, string sFromDate = "", string sToDate = "")
        {
            TTAPIInputModel objTTAPIInputModel = new TTAPIInputModel();
            List<ViewFill> lstFill = new List<ViewFill>();
            List<tt_getfill_master> lstTTFillMaster = dbConnection.tt_getfill_master.OrderByDescending(x => x.created_on).ToList();
            if (!string.IsNullOrEmpty(sAccountId))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => (!string.IsNullOrEmpty(x.account) ? x.account.Trim().ToLower() == sAccountId : x.getfill_id != x.getfill_id) || (x.account_id.ToString() == sAccountId)).ToList();
            }
            if (!string.IsNullOrEmpty(sFromDate))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => x.datetime_stamp >= Convert.ToDateTime(sFromDate).Date).ToList();
            }
            if (!string.IsNullOrEmpty(sToDate))
            {
                lstTTFillMaster = lstTTFillMaster.Where(x => x.datetime_stamp <= Convert.ToDateTime(sToDate).Date).ToList();
            }
            ViewFill objFill = new ViewFill();
            foreach (var item in lstTTFillMaster)
            {
                objFill = new ViewFill();
                objFill.getfill_id = item.getfill_id;
                //objFill.recordId = item.record_id;
                //objFill.timeStamp = item.time_stamp;
                if (item.side != null)
                {
                    if (item.side == 1)
                        objFill.BuySell = Convert.ToString("B");
                    else if (item.side == 2)
                        objFill.BuySell = Convert.ToString("S");
                }
                objFill.datetimeStamp = item.datetime_stamp;
                if (item.datetime_stamp != null)
                {
                    objFill.datetimeStamp = item.datetime_stamp;
                    objFill.datetimeStamp_str = item.datetime_stamp.Value.ToString("dd-MMM-yyyy");
                }
                //objFill.instrumentId = item.instrument_id;
                objFill.Contract = item.contract;
                //objFill.multiLegReportingType = item.multi_leg_reporting_type;
                //objFill.syntheticType = item.synthetic_type;
                //objFill.deltaQty = item.delta_qty;
                //objFill.brokerId = item.broker_id;
                //objFill.manualOrderIndicator = item.manual_order_indicator;
                //objFill.securityDesc = item.security_desc;
                objFill.accountId = item.account_id;
                objFill.account = item.account;
                //objFill.marketId = item.market_id;
                //objFill.orderId = item.order_id;
                objFill.price = item.price;
                objFill.displayFactor = item.display_factor;
                //objFill.lastPx = item.last_px;
                //objFill.execType = item.exec_type;
                objFill.cumQty = item.cum_qty;
                objFill.side = item.side;
                //objFill.source = item.source;
                objFill.created_on = item.created_on;
                lstFill.Add(objFill);
            }
            var result = new PagedList<ViewFill>(lstFill, pageIndex, pageSize);
            return result;
        }
        #endregion

        #region Get Country        
        /// <summary>
        /// Gets the state.
        /// </summary>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public StateResponseClass GetState(TokenResponseClass objTokenResponseClass)
        {
            StateResponseClass objStateResponseClass = new StateResponseClass();
            try
            {
                HttpClient clientState = new HttpClient();
                clientState.BaseAddress = new Uri(URL);
                clientState.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                clientState.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "risk/" + sEnvironment + TTAPIEndPoints.GetStateList);
                var objResponse = clientState.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objStateResponseClass = objResult.Content.ReadAsAsync<StateResponseClass>().Result;
                    if (objStateResponseClass.status.ToLower() == "ok")
                    {
                        return objStateResponseClass;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Instrument Service        
        /// <summary>
        /// Date: 10-Jan-2019
        /// Dev By: Hardik Savaliya
        /// Description: Get Instrument Details
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="sInstrumentIds">The account ids.</param>
        /// <returns></returns>
        /// <exception cref="ServiceLayerException"></exception>
        public List<InstrumentResponse> GetInstrumentDetails(string sInstrumentIds)
        {
            List<InstrumentResponse> objInstrumentResponse = new List<InstrumentResponse>();
            string sproductId = string.Empty, sProductTypeId = string.Empty;
            TTInstrumentDetailsResponseClass objTTInstrumentResponseClass = new TTInstrumentDetailsResponseClass();
            LogServices objLogService = new LogServices();
            try
            {
                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(URL);
                client.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                client.DefaultRequestHeaders.Add("accept", "application/json");
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "pds/" + sEnvironment + TTAPIEndPoints.InstrumentDetails + "/" + sInstrumentIds);
                var objResponse = client.SendAsync(objRequest);
                var objResult = objResponse.Result;
                Task.WaitAll();
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objTTInstrumentResponseClass = objResult.Content.ReadAsAsync<TTInstrumentDetailsResponseClass>().Result;
                    if (objTTInstrumentResponseClass.status.ToLower() == "ok")
                    {
                        objInstrumentResponse.AddRange(objTTInstrumentResponseClass.instrument);
                        APICallLogModel objAPICallLogModel = new APICallLogModel();
                        objAPICallLogModel.api_enum = (Int32)EnumAPI.InstrumentDetails;
                        objAPICallLogModel.endpoint = TTAPIEndPoints.InstrumentDetails;
                        objAPICallLogModel.is_success = true;
                        objAPICallLogModel.description = "Instrument Deatils Called for: " + sInstrumentIds;
                        bool btrue = objLogService.SaveAPILog(objAPICallLogModel);
                        return objTTInstrumentResponseClass.instrument;
                    }
                }
                return objTTInstrumentResponseClass.instrument;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Delete Fills        
        /// <summary>
        /// Deletes the API fills.
        /// </summary>
        /// <param name="tagIds">The tag ids.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool DeleteAPIFills(List<long> tagIds)
        {
            try
            {
                bool bResult = false;
                List<tt_getfill_master> objTTFillMaster = (from UM in dbConnection.tt_getfill_master
                                                           where (tagIds).Contains(UM.getfill_id)
                                                           select UM).ToList();
                dbConnection.tt_getfill_master.RemoveRange(objTTFillMaster);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Account List from TT        
        /// <summary>
        /// Gets the products.
        /// </summary>
        /// <param name="objTokenResponseClass">The object token response class.</param>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public List<TTAccountList> GetTTAccountList(TokenResponseClass objTokenResponseClass)
        {
            AccountListResponseClass objAccountListResponseClass = new AccountListResponseClass();
            try
            {
                HttpClient clientCountry = new HttpClient();
                clientCountry.BaseAddress = new Uri(URL);
                clientCountry.DefaultRequestHeaders.Add("x-api-key", sTTAPIKey);
                clientCountry.DefaultRequestHeaders.Add("Authorization", "Bearer " + objTokenResponseClass.access_token);
                var objRequest = new HttpRequestMessage(HttpMethod.Get, URL + "risk/" + sEnvironment + TTAPIEndPoints.AccountList);
                var objResponse = clientCountry.SendAsync(objRequest);
                var objResult = objResponse.Result;
                if (objResult.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    objAccountListResponseClass = objResult.Content.ReadAsAsync<AccountListResponseClass>().Result;
                    if (objAccountListResponseClass.status.ToLower() == "ok")
                    {
                        if (objAccountListResponseClass.lastPage == "true")
                        {
                            return objAccountListResponseClass.accounts;
                        }
                        else
                        {
                            //next page key need to look
                        }
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region MapAccount Ids To Traderdock
        /// <summary>
        /// Maps the account ids to td.
        /// </summary>
        /// <param name="objTTProductMaster">The object tt product master.</param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        /// Dev By: Hardik Savaliya
        public bool MapAccountIdsToTD()
        {
            List<tt_accountids> objTTAccountIds = dbConnection.tt_accountids.ToList();
            var lstUserPlatform = dbConnection.user_platform.Where(x => x.is_delete != true).ToList();
            if (lstUserPlatform != null)
            {
                if (objTTAccountIds.Count > 0)
                {
                    foreach (var item in lstUserPlatform)
                    {
                        if (objTTAccountIds.Any(x => x.name.Equals(item.account_alias)))
                        {
                            item.account_id = objTTAccountIds.Where(x => x.name.Equals(item.account_alias)).FirstOrDefault().account_id;
                            objTTAccountIds.Where(x => x.name.Equals(item.account_alias)).FirstOrDefault().is_mapped = true;
                            objTTAccountIds.Where(x => x.name.Equals(item.account_alias)).FirstOrDefault().td_user_id = item.user_id;
                        }
                    }
                    dbConnection.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion

        #region TT Account Mapping      
        /// <summary>
        /// Tts the account mapping.
        /// </summary>
        /// <returns></returns>
        /// Dev By: Hardik Savaliya
        public bool TTAccountMapping()
        {
            TTService objTTService = new TTService();
            TokenResponseClass objTokenResponseClass = new TokenResponseClass();
            objTokenResponseClass = objTTService.GetToken();
            List<TTAccountList> lstTTAccountList = objTTService.GetTTAccountList(objTokenResponseClass);
            if (lstTTAccountList.Count > 0)
            {
                List<tt_accountids> lstTTAccountIds = dbConnection.tt_accountids.ToList();
                var lstTTAccountFilterIds = (from l1 in lstTTAccountList
                                             where !(from l2 in lstTTAccountIds
                                                     select l2.name).Contains(l1.name)
                                             select new tt_accountids
                                             {
                                                 account_id = Convert.ToInt64(l1.id),
                                                 name = l1.name,
                                                 parentid = l1.parentId,
                                                 is_mapped = false,
                                                 created_on = DateTime.UtcNow
                                             }).ToList();
                dbConnection.tt_accountids.AddRange(lstTTAccountFilterIds);
                dbConnection.SaveChanges();
                bool bMapAccountIds = objTTService.MapAccountIdsToTD();
                if (bMapAccountIds)
                    return true;
                else
                    return false;
            }
            return false;
        }
        #endregion
    }
}