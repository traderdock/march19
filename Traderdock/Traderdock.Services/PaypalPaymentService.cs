﻿using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;
using static Traderdock.Model.MemberModel.PayPalSetting;

namespace Traderdock.Services
{
    public class PaypalPaymentService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion
        /// <summary>
        /// Date: 15-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Paypal Payment Service
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        public PaypalModel PaypalPaymentfirst(PaypalModel objPaypalModel)
        {
            objPaypalModel = PaypalPayment(objPaypalModel);
            if (objPaypalModel.is_success)
            {
                bool IsDone = AddSubscription(objPaypalModel);
                if (IsDone)
                {
                    return objPaypalModel;
                }
            }
            objPaypalModel.is_success = false;
            return objPaypalModel;
        }

        /// <summary>
        /// Date: 15-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Paypal Payment
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="Args"></param>
        //public static bool PaypalPayment(string[] Args)
        public PaypalModel PaypalPayment(PaypalModel objPaypalModel)
        {
            objPaypalModel.is_success = false;
            #region Paypal Comments
            // 
            // PLEASE READ ALL COMMENTS BELOW:
            // All information regarding the available objects within payflow_dotNET.dll can be found in the API doc
            // found under the "Docs" directory of the installed SDK.  You will also need to refer to the Website
            // Payments Pro Payflow Edition or Payflow Pro Developer’s Guide found in the downloads section of PayPal
            // Manager at https://manager.paypal.com or on the "X.COM" developers forum at 
            // https://www.x.com/community/ppx/payflow_pro?view=documents
            // 
            // This SDK is based on the HTTPS Service and it is highly suggested you read the post on the developer's forum
            // at https://www.x.com/docs/DOC-1642 to better understand what is happening behind the scenes.
            // 
            // Regarding the Request ID:
            // 
            // The request Id is a unique id that you send with your transaction data.  This Id if not changed
            // will help prevent duplicate transactions.  The idea is to set this Id outside the loop or if on a page,
            // prior to the final confirmation page.
            // 
            // Once the transaction is sent and if you don't receive a response you can resend the transaction and the 
            // server will respond with the response data of the original submission.  Also, the object, 
            // Trans.Response.TransactionResponse.Duplicate will be set to "1" if the transaction is a duplicate.
            // 
            // This allows you to resend transaction requests should there be a network or user issue without re-charging
            // a customers credit card.
            // 
            // COMMON ISSUES:
            // 
            // Result Code 1:
            // Is usually caused by one of the following:
            //		** Invalid login information, see result code 26 below.
            //		** IP Restrictions on the account. Verify there are no ip restrictions in Manager under Service Settings.
            // 
            // Result Code 26:
            // Verify USER, VENDOR, PARTNER and PASSWORD. Remember, USER and VENDOR are both the merchant login
            // ID unless a Payflow Pro USER was created.  All fields are case-sensitive. See this post for more 
            // information: http://www.x.com/docs/DOC-1884.
            // 
            // Receiving Communication Exceptions or No Response: 
            // Since this service is based on HTTPS it is possible that due to network issues either on PayPal's side or 
            // yours that you can not process a transaction.  If this is the case, what is suggested is that you put some 
            // type of loop in your code to try up to X times before "giving up".  This example will try to get a response 
            // up to 3 times before it fails and by using the Request ID as described above, you can do these attempts without
            // the chance of causing duplicate charges on your customer's credit card.
            // 
            // END COMMENTS

            // Begin Application
            //
            // Set the Request ID
            // Uncomment the line below and run two concurrent transactions to show how duplicate works.  You will notice on
            // the second transaction that the response returned is identical to the first, but the duplicate object will be set.
            // String strRequestID = "123456";
            // Comment out this line if testing duplicate response.  
            #endregion
            String RequestID = PayflowUtility.RequestId;
            #region Paypal Comments
            // *** Create the Data Objects. ***
            //
            // *** Create the User data object with the required user details. ***
            //         
            // Should you choose to store the login information (Vendor, User, Partner and Password) in
            // app.config, you can retrieve the data using PayflowUtility.AppSettings. 
            //
            // For Example:
            //
            //App.Config Entry: < add key = "PayflowPartner" value = "PayPal" />

            #endregion
            String mUser = PayflowUtility.AppSettings("PayflowUser");
            String mVendor = PayflowUtility.AppSettings("PayflowVendor");
            String mPartner = PayflowUtility.AppSettings("PayflowPartner");
            String mPassword = PayflowUtility.AppSettings("PayflowPassword");
            UserInfo User = new UserInfo(mUser, mVendor, mPartner, mPassword);
            #region Paypal Comments
            // Remember: <vendor> = your merchant (login id), <user> = <vendor> unless you created a separate <user> for Payflow Pro.
            // Result code 26 will be issued if you do not provide both the <vendor> and <user> fields.

            // The other most common error with authentication is result code 1, user authentication failed.  This is usually
            // due to invalid account information or ip restriction on the account.  You can verify ip restriction by logging 
            // into Manager.
            //UserInfo User = new UserInfo("<user>", "<vendor>", "<partner>", "<password>");

            //   UserInfo User = new UserInfo("hardik@peerbits.com", "hardik-facilitator@peerbits.com", "hardik@peerbits.com", "AYGA98CMKAJ7AJQC");

            // *** Create the Payflow Connection data object with the required connection details. ***
            //
            // To allow the ability to change easily between the live and test servers, the PFPRO_HOST 
            // property is defined in the App.config (or web.config for a web site) file.  However,
            // you can also pass these fields and others directly from the PayflowConnectionData contructor. 
            // This will override the values passed in the App.config file.
            //
            // For Example:
            //
            //      Example values passed below are as follows:
            //      Payflow Pro Host address : pilot-payflowpro.paypal.com 
            //      Payflow Pro Host Port : 443
            //      Timeout : 45 ( in seconds )
            //
            //      PayflowConnectionData Connection = new PayflowConnectionData("pilot-payflowpro.paypal.com", 443, 45, "",0,"","");
            //
            // Obtain Host address from the app.config file and use default values for
            // timeout and proxy settings.

            #endregion
            PayflowConnectionData Connection = new PayflowConnectionData();
            #region Paypal Comments

            // *** Create a new Invoice data object ***
            // Set Invoice object with the Amount, Billing & Shipping Address, etc. ***

            #endregion
            Invoice Inv = new Invoice();
            #region Paypal Comments
            // Creates a CultureInfo for English in the U.S.
            // Not necessary, just here for example of using currency formatting. 
            #endregion
            CultureInfo us = new CultureInfo("en-US");
            String usCurrency = "USD";
            #region Paypal Comments

            // Set the amount object. For Partial Authorizations, refer to the DoPartialAuth example.
            // Currency Code 840 (USD) is US ISO currency code.  If no code passed, 840 is default.
            // See the Developer's Guide for the list of the three-digit currency codes.
            //Currency Amt = new Currency(new decimal(49.25), usCurrency); 
            #endregion
            decimal OrderAmount = Convert.ToDecimal(objPaypalModel.order_amount);
            Currency Amt = new Currency(OrderAmount, usCurrency);
            #region Paypal Comments
            // A valid amount has either no decimal value or only a two decimal value. 
            // An invalid amount will generate a result code 4.
            //
            // For values which have more than two decimal places such as:
            // Currency Amt = new Currency(new Decimal(25.1214));
            // You will either need to truncate or round as needed using the following property: Amt.NoOfDecimalDigits
            //
            // If the NoOfDecimalDigits property is used then it is mandatory to set one of the following
            // properties to true.
            //
            //Amt.Round = true;
            //Amt.Truncate = true;
            //
            // For Currencies without a decimal, you'll need to set the NoOfDecimalDigits = 0.
            //Amt.NoOfDecimalDigits = 0; 
            #endregion
            Inv.Amt = Amt;
            #region Paypal Comments
            // PONum, InvNum and CustRef are sent to the processors and could show up on a customers
            // or your bank statement. These fields are reportable but not searchable in PayPal Manager. 
            #endregion
            Inv.PoNum = "PO12345";
            Inv.InvNum = "INV12345";
            Inv.CustRef = "CustRef1";
            Inv.CustRef = "Traderdock";
            #region Paypal Comments
            // Soft descriptor, used to change information on customer's credit card. Refer to the Payflow Pro Developer's
            // Guide to verify that your processor supports these.
            //Inv.MerchDescr = "Merchant Descr"
            //Inv.MerchSvc = "Merchant Svc"

            // Comment1 and Comment2 fields are searchable within PayPal Manager .
            // You may want to populate these fields with any of the above three fields or any other data.
            // However, the search is a case-sensitive and is a non-wildcard search, so plan accordingly. 
            #endregion
            Inv.Comment1 = "user_" + objPaypalModel.User.first_name + "_" + objPaypalModel.User.last_name + "_" + objPaypalModel.user_id;
            Inv.Comment2 = "plan_" + objPaypalModel.subscription_name + "_" + objPaypalModel.order_amount;
            #region Paypal Comments

            // There are additional Invoice parameters that could assist you in obtaining a better rate
            // from your merchant bank.  Refer to the Payflow Pro Developer’s Guide
            // and consult your Internet Merchant Bank on what parameters (if any) you can use.
            // Some of the parameters could include:
            // Inv.Recurring = "Y";
            // Inv.TaxExempt = "Y";

            // *** Set the Billing Address details. ***
            //
            // The billing details below except for Street and Zip are for reporting purposes only. 
            // It is suggested that you pass all the billing details for enhanced reporting and as data backup.

            // Create the BillTo object.

            #endregion
            BillTo Bill = new BillTo();  // Set the customer name and other billing details.
            Bill.BillToFirstName = !string.IsNullOrEmpty(objPaypalModel.User.first_name) ? objPaypalModel.User.first_name : string.Empty;
            Bill.BillToMiddleName = "";
            Bill.BillToLastName = !string.IsNullOrEmpty(objPaypalModel.User.last_name) ? objPaypalModel.User.last_name : string.Empty;
            Bill.BillToCompanyName = Bill.BillToFirstName + "_" + Bill.BillToLastName;
            #region Paypal Comments
            // It is highly suggested that you pass at minimum Street and Zip for AVS response.
            // However, AVS is only supported by US banks and some foreign banks.  See the Payflow 
            // Developer's Guide for more information.  Sending these fields could help in obtaining
            // a lower discount rate from your Internet merchant Bank.  Consult your bank for more information. 
            #endregion
            Bill.BillToStreet = !string.IsNullOrEmpty(objPaypalModel.User.address) ? objPaypalModel.User.address : string.Empty;
            Bill.BillToStreet2 = "";
            Bill.BillToCity = !string.IsNullOrEmpty(objPaypalModel.User.city_name) ? objPaypalModel.User.city_name : string.Empty;
            Bill.BillToState = !string.IsNullOrEmpty(objPaypalModel.User.state_name) ? objPaypalModel.User.state_name : string.Empty;
            Bill.BillToZip = !string.IsNullOrEmpty(objPaypalModel.User.postal_code) ? objPaypalModel.User.postal_code : string.Empty;
            #region Paypal Comments
            // BillToCountry code is based on numeric ISO country codes. (e.g. 840 = USA)
            // For more information, refer to the Payflow Developer's Guide. 
            #endregion
            Bill.BillToCountry = "840";
            Bill.BillToPhone = !string.IsNullOrEmpty(objPaypalModel.User.contact_number) ? objPaypalModel.User.contact_number : string.Empty;
            Bill.BillToPhone2 = !string.IsNullOrEmpty(objPaypalModel.User.contact_number) ? objPaypalModel.User.contact_number : string.Empty;
            Bill.BillToHomePhone = "";
            Bill.BillToFax = "";
            Bill.BillToEmail = !string.IsNullOrEmpty(objPaypalModel.User.user_email) ? objPaypalModel.User.user_email : string.Empty;
            Inv.BillTo = Bill;  // Set the BillTo object into invoice.
            #region Paypal Comments
            // ECHODATA allows you to trigger data sent in the request to be returned in the request.
            // 'ADDRESS' will return both shipping and billing address data, if sent. 
            #endregion
            Inv.EchoData = "";
            #region Paypal Comments

            // Shipping details may not be necessary if providing a
            // service or downloadable product such as software etc.
            //
            // Set the Shipping Address details.
            // The shipping details are for reporting purposes only. 
            // It is suggested that you pass all the shipping details for enhanced reporting.
            //
            // Create the ShipTo object.

            //ShipTo Ship = new ShipTo(); by Aakash

            // To prevent an 'Address Mismatch' fraud trigger, we are shipping to the billing address.  However,
            // shipping parameters are listed.
            // Comment line below if you want a separate Ship To address.

            //Ship = Bill.Copy();//by Aakash

            // Uncomment statements below to send to separate Ship To address.

            // Set the recipient's name.			
            // Ship.ShipToFirstName = "Sam";
            // Ship.ShipToMiddleName = "J";
            // Ship.ShipToLastName = "Spade";
            // Ship.ShipToStreet = "456 Shipping St.";
            // Ship.ShipToStreet2 = "Apt A";
            // Ship.ShipToCity = "Las Vegas";
            // Ship.ShipToState = "NV";
            // Ship.ShipToZip = "99999";
            // ShipToCountry code is based on numeric ISO country codes. (e.g. 840 = USA)
            // For more information, refer to the Payflow Pro Developer's Guide.
            // Ship.ShipToCountry = "840";
            // Ship.ShipToPhone = "555-123-1233";
            // Secondary phone numbers (could be mobile number etc).
            // Ship.ShipToPhone2 = "555-333-1222";
            // Ship.ShipToEmail = "Sam.Spade@email.com";
            // Ship.ShipFromZip = Bill.BillToZip;

            // Following 2 items are just for reporting purposes and are not required.
            // Ship.ShipCarrier = "UPS";
            // Ship.ShipMethod = "Ground";

            //Inv.ShipTo = Ship;// by Aakash

            // *** 	Create Customer Data ***
            // There are additional CustomerInfo parameters that are used for Level 2 Purchase Cards.
            // Refer to the Payflow Pro Developer’s Guide and consult with your Internet
            // Merchant Bank regarding what parameters to send.
            // Some of the parameters could include:
            // 
            #endregion
            CustomerInfo CustInfo = new CustomerInfo();
            CustInfo.CustCode = "cust_" + objPaypalModel.user_id.ToString();    // Customer Code
            CustInfo.CustId = objPaypalModel.user_id.ToString();
            CustInfo.CustIP = "255.255.255.255";  // Customer's ip address
            Inv.CustomerInfo = CustInfo;
            #region Paypal Comments

            // *** Send User fields ***
            // You can send up to ten string type parameters to store temporary data (for example, variables, 
            // session IDs, order numbers, and so on). These fields will be echoed back either via API response
            // or as part of the Silent / Return post if using the hosted checkout page.
            // 
            // Note: UserItem1 through UserItem10 are not displayed to the customer and are not stored in 
            // the PayPal transaction database.
            //             
            #endregion
            UserItem nUser = new UserItem();
            nUser.UserItem1 = !string.IsNullOrEmpty(objPaypalModel.subscription_name) ? objPaypalModel.subscription_name : string.Empty;  // nUser.UserItem2 = "TUSER2";
            Inv.UserItem = nUser;
            #region Paypal Comments

            // *** Create a new Payment Device - Credit Card data object. ***
            // The input parameters are Credit Card Number and Expiration Date of the Credit Card.
            // Note: Expiration date is in the format MMYY. 
            #endregion
            string card_number = !string.IsNullOrEmpty(objPaypalModel.card_number) ? objPaypalModel.card_number : string.Empty;
            string expire_date = !string.IsNullOrEmpty(objPaypalModel.card_expiry_month) ? !string.IsNullOrEmpty(objPaypalModel.card_expiry_year) ? objPaypalModel.card_expiry_month + objPaypalModel.card_expiry_year : string.Empty : string.Empty;
            CreditCard CC = new CreditCard(card_number, expire_date);
            #region Paypal Comments
            //CreditCard CC = new CreditCard("5555555555554444", "0822");

            //Example of Swipe Transaction.
            //See DOSwipe.cs example for more information.
            //SwipeCard Swipe = new SwipeCard(";5105105105105100=15121011000012345678?");

            // *** Card Security Code ***
            // This is the 3 or 4 digit code on either side of the Credit Card.
            // It is highly suggested that you obtain and pass this information to help prevent fraud.
            // Sending this fields could help in obtaining a lower discount rate from your Internet merchant Bank.
            //// CVV2 is not required when performing a Swipe transaction as the card is present.
            //CC.Cvv2 = "123"; 
            #endregion
            CC.Cvv2 = !string.IsNullOrEmpty(objPaypalModel.card_cvv) ? objPaypalModel.card_cvv : string.Empty;
            #region Paypal Comments
            //Name on Credit Card is optional and not used as part of the authorization.
            //Also, this field populates the NAME field which is the same as FIRSTNAME, so if you
            //are already populating first name, do not use this field.
            //CC.Name = "Joe Smith";
            // *** Create a new Tender - Card Tender data object. *** 
            #endregion
            CardTender Card = new CardTender(CC);  // credit card
            #region Paypal Comments

            // If you are doing card-present (retail)type transactions you will want to use the swipe object.  Before doing so, verify with
            // your merchant bank that you are setup to process card-present transactions and contact Payflow support to request your account
            // be setup to process these types of transactions.  You will need to request your market seqment be changed from e-commerce (default)
            // to retail.
            //CardTender Card = new CardTender(Swipe);

            // *** Create a new Sale Transaction. ***
            // The Request Id is the unique id necessary for each transaction.  If you are performing an authorization
            // - delayed capture trnsaction, make sure that you pass two different unique request ids for each of the
            // transaction.
            // If you pass a non-unique request id, you will receive the transaction details from the original request.
            // The only difference is you will also receive a parameter DUPLICATE indicating this request id has been used
            // before.
            // The Request Id can be any unique number such order id, invoice number from your implementation or a random
            // id can be generated using the PayflowUtility.RequestId. 
            #endregion
            SaleTransaction Trans = new SaleTransaction(User, Connection, Inv, Card, RequestID);
            ClientInfo cInfo = new ClientInfo();
            cInfo.IntegrationProduct = "Test";
            cInfo.IntegrationVersion = "1.0";
            Trans.ClientInfo = cInfo;
            #region Paypal Comments


            // Transaction results (especially values for declines and error conditions) returned by each PayPal-supported
            // processor vary in detail level and in format. The Payflow Verbosity parameter enables you to control the kind
            // and level of information you want returned. 
            //
            // By default, Verbosity is set to LOW. A LOW setting causes PayPal to normalize the transaction result values. 
            // Normalizing the values limits them to a standardized set of values and simplifies the process of integrating 
            // the Payflow SDK.
            //
            // By setting Verbosity to HIGH, you can view the processor's raw response values along with card information. This 
            // setting is more verbose than the LOW or MEDIUM setting in that it returns more detailed, processor and card specific
            // information. 
            //
            // Review the chapter in the Payflow Pro Developer's Guide regarding VERBOSITY and the INQUIRY function for more details.

            // Set the transaction verbosity to HIGH to display most details. 
            #endregion
            Trans.Verbosity = "HIGH";
            #region Paypal Comments

            // Try to submit the transaction up to 3 times with 5 second delay.  This can be used
            // in case of network issues.  The idea here is since you are posting via HTTPS behind the scenes there
            // could be general network issues, so try a few times before you tell customer there is an issue. 
            #endregion
            int trxCount = 1;
            bool RespRecd = false;
            while (trxCount <= 3 && !RespRecd)
            {
                #region Paypal Comments
                // Notice we set the request id earlier in the application and outside our loop.  This way if a response was not received
                // but PayPal processed the original request, you'll receive the original response with DUPLICATE set.

                // Submit the Transaction 
                #endregion
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                Response Resp = Trans.SubmitTransaction();
                #region Paypal Comments

                // Uncomment line below to simulate "No Response"
                //Resp = null;

                // Display the transaction response parameters. 
                #endregion
                if (Resp != null)
                {
                    #region Got a response. 
                    RespRecd = true;  // Got a response.   
                    TransactionResponse TrxnResponse = Resp.TransactionResponse; // Get the Transaction Response parameters.
                    #region Paypal Comments

                    // Refer to the Payflow Pro .NET API Reference Guide and the Payflow Pro Developer's Guide
                    // for explanation of the items returned and for additional information and parameters available. 
                    #endregion
                    if (TrxnResponse != null)
                    {
                        //Refer link for more details if required
                        //https://www.paypalobjects.com/webstatic/en_US/developer/docs/pdf/pp_payflowlink_guide.pdf
                        //Approved;
                        if (TrxnResponse.Result == 0)
                        {
                            objPaypalModel.is_success = true;
                            objPaypalModel.transaction_id = TrxnResponse.Pnref;
                            objPaypalModel.payment_response_code = TrxnResponse.Result;
                            objPaypalModel.payment_response_message = TrxnResponse.RespMsg;
                            if (TrxnResponse.CardType != null)
                            {
                                switch (TrxnResponse.CardType)
                                {
                                    case "0":
                                        objPaypalModel.card_type = "Visa";  // Debug.Write("Visa");
                                        break;
                                    case "1":
                                        objPaypalModel.card_type = "MasterCard";  // Debug.Write("MasterCard");
                                        break;
                                    case "2":
                                        objPaypalModel.card_type = "Discover";  // Debug.Write("Discover");
                                        break;
                                    case "3":
                                        objPaypalModel.card_type = "American Express";  // Debug.Write("American Express");
                                        break;
                                    case "4":
                                        objPaypalModel.card_type = "Diners Club";  // Debug.Write("Diner's Club");
                                        break;
                                    case "5":
                                        objPaypalModel.card_type = "JCB";  // Debug.Write("JCB");
                                        break;
                                    case "6":
                                        objPaypalModel.card_type = "Maestro";  // Debug.Write("Maestro");
                                        break;
                                    case "S":
                                        objPaypalModel.card_type = "Solo";  // Debug.Write("Solo");
                                        break;
                                }
                            }
                        }
                        else
                        {
                            objPaypalModel.is_success = false;
                            objPaypalModel.payment_response_code = TrxnResponse.Result;
                            objPaypalModel.payment_response_message = TrxnResponse.RespMsg;
                        }
                    }
                    #region Paypal Comments
                    // Get the Transaction Context and check for any contained SDK specific errors (optional code).
                    // This is not normally used in production. 
                    #endregion
                    Context TransCtx = Resp.TransactionContext;
                    if (TransCtx != null && TransCtx.getErrorCount() > 0)
                    {
                        //Debug.Write("------------------------------------------------------");
                        //Debug.Write("Transaction Context Errors: " + TransCtx.ToString());
                    }
                    //Debug.Write("------------------------------------------------------");
                    //Debug.Write("Press Enter to Exit ...");
                    //Console.ReadLine(); 
                    #endregion
                }
                else
                {
                    Thread.Sleep(5000); //let's wait 5 seconds to see if this is a temporary network issue.
                                        //Debug.Write("Retry #: " + trxCount.ToString());
                    trxCount++;
                }
            }
            if (!RespRecd)
            {
                //Debug.Write("There is a problem obtaining an authorization for your order.");
                //Debug.Write("Please contact Customer Support.");
                //Debug.Write("------------------------------------------------------");
                //Debug.Write("Press Enter to Exit ...");
                //Console.ReadLine();
            }
            return objPaypalModel;

        }

        public bool AddSubscription(PaypalModel objPaypalModel)
        {
            try
            {
                user_subscription User_subcription = new user_subscription();
                User_subcription.user_id = objPaypalModel.user_id;
                User_subcription.subscription_id = objPaypalModel.subscription_id;
                User_subcription.transaction_id = objPaypalModel.transaction_id;
                User_subcription.order_amount = Convert.ToDecimal(objPaypalModel.order_amount);
                User_subcription.purchase_date = DateTime.UtcNow;
                User_subcription.payment_method = objPaypalModel.PaymentType == null ? "Card" : objPaypalModel.PaymentType;
                User_subcription.payment_status = (Int32)PaymentStatus.Paid;
                User_subcription.is_expired = false;
                User_subcription.is_current = true;
                User_subcription.free_trial = false;
                User_subcription.subscription_start_date = DateTime.UtcNow;
                User_subcription.subscription_end_date = DateTime.UtcNow.AddMonths(1);
                User_subcription.created_date = DateTime.UtcNow;
                User_subcription.created_by = objPaypalModel.user_id;
                User_subcription.updated_date = User_subcription.created_date;
                User_subcription.updated_by = objPaypalModel.user_id;
                if (objPaypalModel.promo_code_id > 0)
                {
                    User_subcription.promo_code_id = objPaypalModel.promo_code_id;
                    User_subcription.discount_amount = dbConnection.promo_code.Where(x => x.promo_code_id == objPaypalModel.promo_code_id).Select(x => x.reward_amount).FirstOrDefault();
                }
                dbConnection.user_subscription.Add(User_subcription);
                List<user_subscription> objSubscriptionList = dbConnection.user_subscription.Where(x => x.user_id == objPaypalModel.user_id
                                                                && x.user_subscription_id != User_subcription.user_subscription_id).ToList();
                objSubscriptionList.ForEach(x => x.is_current = false);
                user_master objUserMaster = new user_master();
                objUserMaster = dbConnection.user_master.Where(x => x.user_id == objPaypalModel.user_id).FirstOrDefault();
                objUserMaster.subscription_mode = Convert.ToInt32(EnumSubscriptionMode.Subscribed);
                objUserMaster.modified_by = objUserMaster.user_id;
                objUserMaster.modified_on = System.DateTime.UtcNow;
                dbConnection.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            return true;
        }

        public bool AddCardVault(VaultModel objVaultModel)
        {
            try
            {
                user_vault objUserVault = new user_vault();
                objUserVault.user_id = objVaultModel.user_id;
                objUserVault.vault_id = objVaultModel.id;
                objUserVault.card_type = objVaultModel.type;
                objUserVault.card_url = objVaultModel.links.Select(x => x.href).FirstOrDefault();
                dbConnection.user_vault.Add(objUserVault);
                dbConnection.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Date: 09-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: Reset Paypal Payment Service
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        public PaypalModel ResetPaypalPaymentfirst(PaypalModel objPaypalModel)
        {
            objPaypalModel = PaypalPayment(objPaypalModel);
            if (objPaypalModel.is_success)
            {
                bool IsDone = AddResetRequest(objPaypalModel);
                if (IsDone)
                {
                    return objPaypalModel;
                }
            }
            objPaypalModel.is_success = false;
            return objPaypalModel;
        }


        /// <summary>
        /// Date: 09-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: Reset Paypal Payment Service
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        public bool AddResetRequest(PaypalModel objPaypalModel)
        {
            user_subscription objUserSubscription = dbConnection.user_subscription.Where(x => x.user_id == objPaypalModel.user_id && x.is_current == true).FirstOrDefault();
            reset_requests objUserResetRequests = new reset_requests();
            objUserResetRequests.user_id = objPaypalModel.user_id;
            objUserResetRequests.transaction_id = objPaypalModel.transaction_id;
            objUserResetRequests.order_amount = Convert.ToDecimal(objPaypalModel.order_amount);
            objUserResetRequests.purchase_date = DateTime.UtcNow;
            objUserResetRequests.payment_method = objPaypalModel.PaymentType == null ? "Card" : objPaypalModel.PaymentType;
            objUserResetRequests.payment_status = (Int32)PaymentStatus.Paid;
            objUserResetRequests.payment_response_code = objPaypalModel.payment_response_code;
            objUserResetRequests.reset_status = false;
            objUserResetRequests.created_date = DateTime.UtcNow;
            objUserResetRequests.created_by = objPaypalModel.user_id;
            objUserResetRequests.updated_date = DateTime.UtcNow;
            objUserResetRequests.updated_by = objPaypalModel.user_id;
            //added for fetch current subscription in reset requeset
            objUserResetRequests.user_subscription_id = objUserSubscription.user_subscription_id;
            dbConnection.reset_requests.Add(objUserResetRequests);
            dbConnection.SaveChanges();

            //send reset account information mail to user
            UserMasterModel objUserMasterModel = new UserMasterModel();
            UserService objUserService = new UserService();
            CommonService objCommonService = new CommonService();
            objUserMasterModel = objUserService.GetUser(objPaypalModel.user_id);
            if (!string.IsNullOrEmpty(objUserMasterModel.user_email))
            {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add(EmailTemplateParameters.Email, objUserMasterModel.user_email);
                parameters.Add(EmailTemplateParameters.FullName, objUserMasterModel.first_name + " " + objUserMasterModel.last_name);
                //string emailattachment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["attachmentemail"]);
                new CommonService().SendEmail(objUserMasterModel.user_email, EmailTemplateNames.ResetAccountMember, parameters, null);
                //send reset account information mail to Support
                string sSupportMail = objCommonService.GetSettingValue("ReplyTo");
                Dictionary<string, string> dicSupportParameters = new Dictionary<string, string>();
                dicSupportParameters.Add(EmailTemplateParameters.Email, sSupportMail);
                dicSupportParameters.Add(EmailTemplateParameters.USEREMAIL, objUserMasterModel.user_email);
                dicSupportParameters.Add(EmailTemplateParameters.FullName, objUserMasterModel.first_name + " " + objUserMasterModel.last_name);
                dicSupportParameters.Add(EmailTemplateParameters.AccountNumber, objUserMasterModel.account_number);
                dicSupportParameters.Add(EmailTemplateParameters.Platform, objUserMasterModel.trading_platform_string);
                new CommonService().SendEmail(sSupportMail, EmailTemplateNames.ResetAccountSupport, dicSupportParameters, null);
            }

            return true;
        }

        /// <summary>
        /// Create Recurring Profile
        /// 16-Apr-2018
        /// By Shoaib Mijaki
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public bool CreateRecurringProfile(decimal OrderAmount, string TOKEN, string PAYERID, long lUserID)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "CreateRecurringPaymentsProfile";
            values["USER"] = PayPalSettings.Username;
            values["PWD"] = PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSettings.Signature;
            values["VERSION"] = "93";
            values["TOKEN"] = TOKEN;
            values["PAYERID"] = PAYERID;
            //VERSION = 86
            //Recurring related payment
            //values["PROFILESTARTDATE"] = DateTime.UtcNow.ToString();
            values["PROFILESTARTDATE"] = System.DateTime.UtcNow.AddMonths(1).ToString("yyyy-MM-ddTHH:mm:ss");
            //values["BRANDNAME"] = "TraderDock";
            values["DESC"] = "TraderDock";
            values["PROFILEREFERENCE"] = lUserID.ToString();
            values["BILLINGPERIOD"] = "Day";
            values["BILLINGFREQUENCY"] = "30";
            values["AMT"] = OrderAmount.ToString();
            values["CURRENCYCODE"] = "USD";
            values["AUTOBILLAMT"] = "AddToNextBilling";
            values["MAXFAILEDPAYMENTS"] = "3";
            values = Submit(values);
            string ack = values["ACK"].ToLower();
            if (ack == "success" || ack == "successwithwarning")
                return true;
            else
                return false;
        }

        /// <summary>
        /// Paypal Express Checkout
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public PayPalRedirect ExpressCheckout(PayPalOrder order)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "SetExpressCheckout";
            values["RETURNURL"] = PayPalSettings.ReturnUrl;
            values["CANCELURL"] = PayPalSettings.CancelUrl;
            values["PAYMENTACTION"] = "SALE";
            values["CURRENCYCODE"] = "USD";
            values["BUTTONSOURCE"] = "PP-ECWizard";
            values["USER"] = PayPalSettings.Username;
            values["PWD"] = PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSettings.Signature;
            values["SUBJECT"] = "";
            values["VERSION"] = "93";
            values["BRANDNAME"] = "TraderDock";
            values["item_name"] = "product1";
            values["AMT"] = order.Amount.ToString();
            //Change related to recurring payment - 13Apr2018
            //By shoaib
            values["DESC"] = "TraderDock";
            //&PROFILESTARTDATE = 2012-05-11T00: 00:00Z    #Billing date start, in UTC/GMT format
            values["L_BILLINGTYPE0"] = "RecurringPayments";
            values["L_BILLINGAGREEMENTDESCRIPTION0"] = "TraderDock";
            //values["L_PAYMENTREQUEST_0_NAME0"] = order.IteamName;
            values = Submit(values);
            string ack = values["ACK"].ToLower();
            if (ack == "success" || ack == "successwithwarning")
            {
                return new PayPalRedirect
                {
                    Token = values["TOKEN"],
                    Url = String.Format("https://{0}/cgi-bin/webscr?cmd=_express-checkout&token={1}&rm=2",
                      PayPalSettings.CgiDomain, values["TOKEN"])
                };
            }
            else
            {
                throw new Exception(values["L_LONGMESSAGE0"]);
            }

        }

        /// <summary>
        /// Date: 13-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: Reset Paypal Payment Service
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        public PayPalRedirect ResetExpressCheckout(PayPalOrder order)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "SetExpressCheckout";
            values["RETURNURL"] = PayPalSettings.ResetReturnUrl;
            values["CANCELURL"] = PayPalSettings.ResetCancelUrl;
            values["PAYMENTACTION"] = "SALE";
            values["CURRENCYCODE"] = "USD";
            values["BUTTONSOURCE"] = "PP-ECWizard";
            values["USER"] = PayPalSettings.Username;
            values["PWD"] = PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSettings.Signature;
            values["SUBJECT"] = "";
            values["VERSION"] = "93";
            values["BRANDNAME"] = "TrederDock";
            values["item_name"] = "product1";
            values["AMT"] = order.Amount.ToString();
            //values["L_PAYMENTREQUEST_0_NAME0"] = order.IteamName;
            values = Submit(values);

            string ack = values["ACK"].ToLower();

            if (ack == "success" || ack == "successwithwarning")
            {
                return new PayPalRedirect
                {
                    Token = values["TOKEN"],
                    Url = String.Format("https://{0}/cgi-bin/webscr?cmd=_express-checkout&token={1}&rm=2",
                      PayPalSettings.CgiDomain, values["TOKEN"])
                };
            }
            else
            {
                throw new Exception(values["L_LONGMESSAGE0"]);
            }

        }

        private NameValueCollection Submit(NameValueCollection values)
        {
            string data = String.Join("&", values.Cast<string>().Select(key => String.Format("{0}={1}", key, values[key])));
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(String.Format("https://{0}/nvp", PayPalSettings.ApiDomain));
            request.Method = "POST";
            request.KeepAlive = false;
            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;
            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return HttpUtility.ParseQueryString(reader.ReadToEnd());
            }
        }

        public bool GetCheckoutDetails(string token, ref string PayerID, ref string retMsg)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "GetExpressCheckoutDetails";
            values["TOKEN"] = token;
            values["USER"] = PayPalSettings.Username;
            values["PWD"] = PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSettings.Signature;
            values["SUBJECT"] = "";
            values["VERSION"] = "93";
            values = Submit(values);
            string ack = values["ACK"].ToLower();
            if (ack == "success" || ack == "successwithwarning")
            {
                PayerID = values["PayerID"];
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + values["L_ERRORCODE0"] + "&" +
                "Desc=" + values["L_SHORTMESSAGE0"] + "&" +
                "Desc2=" + values["L_LONGMESSAGE0"];

                return false;
            }
        }

        public bool DoCheckoutPayment(string finalPaymentAmount, string token, string PayerID, ref string retMsg)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "DoExpressCheckoutPayment";
            values["TOKEN"] = token;
            values["PAYERID"] = PayerID;
            values["PAYMENTREQUEST_0_AMT"] = finalPaymentAmount;
            values["PAYMENTREQUEST_0_CURRENCYCODE"] = "USD";
            values["PAYMENTREQUEST_0_PAYMENTACTION"] = "Sale";
            values["USER"] = PayPalSettings.Username;
            values["PWD"] = PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSettings.Signature;
            values["SUBJECT"] = "";
            values["VERSION"] = "93";
            //values["item_name"] = "product1";
            values = Submit(values);
            string ack = values["ACK"].ToLower();
            if (ack == "success" || ack == "successwithwarning")
            //if (ack == "success")
            {
                retMsg = values["PAYMENTINFO_0_TRANSACTIONID"].ToString(); ;
                return true;
            }
            //else if (ack == "successwithwarning")
            //{
            //    retMsg = "ErrorCode=" + values["L_ERRORCODE0"] + "&" +
            //    "Desc=" + values["L_SHORTMESSAGE0"] + "&" +
            //    "Desc2=" + values["L_LONGMESSAGE0"];
            //    return false;
            //}
            else
            {
                retMsg = "ErrorCode=" + values["L_ERRORCODE0"] + "&" +
                "Desc=" + values["L_SHORTMESSAGE0"] + "&" +
                "Desc2=" + values["L_LONGMESSAGE0"];
                return false;
            }
        }

        public bool AddSubscriptionAdminPanel(PaypalModel objPaypalModel)
        {
            try
            {
                user_subscription objUserSubscription = new user_subscription();
                objUserSubscription.user_id = objPaypalModel.user_id;
                if (!string.IsNullOrEmpty(objPaypalModel.subscription_mode))
                {
                    if (objPaypalModel.subscription_mode == "C")
                    {
                        objUserSubscription.subscription_id = 3;
                        objUserSubscription.subscription_start_date = objPaypalModel.subscription_start_date;
                        objUserSubscription.subscription_end_date = objPaypalModel.subscription_start_date.Value.AddMonths(1);
                        objUserSubscription.order_amount = Convert.ToDecimal(200);
                    }
                    else if (objPaypalModel.subscription_mode == "D")
                    {
                        objUserSubscription.subscription_id = 8;
                        objUserSubscription.subscription_start_date = objPaypalModel.subscription_start_date;
                        objUserSubscription.subscription_end_date = objPaypalModel.subscription_start_date.Value.AddDays(15);
                        objUserSubscription.order_amount = Convert.ToDecimal(0);
                    }
                }
                else
                    return false;
                
                objUserSubscription.purchase_date = objPaypalModel.subscription_start_date;
                objUserSubscription.payment_method = "Ppal";
                objUserSubscription.payment_status = (Int32)PaymentStatus.Paid;
                objUserSubscription.is_expired = false;
                objUserSubscription.is_current = true;
                objUserSubscription.free_trial = false;
                objUserSubscription.created_date = DateTime.UtcNow;
                objUserSubscription.created_by = objPaypalModel.user_id;
                objUserSubscription.updated_date = objUserSubscription.created_date;
                objUserSubscription.updated_by = objPaypalModel.user_id;
                if (objPaypalModel.promo_code_id > 0)
                {
                    objUserSubscription.promo_code_id = objPaypalModel.promo_code_id;
                    objUserSubscription.discount_amount = dbConnection.promo_code.Where(x => x.promo_code_id == objPaypalModel.promo_code_id).Select(x => x.reward_amount).FirstOrDefault();
                }
                dbConnection.user_subscription.Add(objUserSubscription);
                List<user_subscription> objSubscriptionList = dbConnection.user_subscription.Where(x => x.user_id == objPaypalModel.user_id
                                                                && x.user_subscription_id != objUserSubscription.user_subscription_id).ToList();
                objSubscriptionList.ForEach(x => x.is_current = false);
                user_master objUserMaster = new user_master();
                objUserMaster = dbConnection.user_master.Where(x => x.user_id == objPaypalModel.user_id).FirstOrDefault();
                objUserMaster.subscription_mode = Convert.ToInt32(EnumSubscriptionMode.Subscribed);
                objUserMaster.modified_by = objUserMaster.user_id;
                objUserMaster.modified_on = System.DateTime.UtcNow;
                dbConnection.SaveChanges();
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            return true;
        }

    }
}
