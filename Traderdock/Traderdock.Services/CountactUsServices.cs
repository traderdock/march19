﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Entity.MemberModel;
using Traderdock.ORM;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.Model;
using Traderdock.Common;
using Traderdock.Common.Exceptions;

namespace Traderdock.Services
{
    public class CountactUsServices
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Add/Edit Contact Us
        /// <summary>
        /// Add Edit Contact Us
        /// </summary>
        /// <param name="objContactUsModel"></param>
        /// <returns></returns>
        public bool AddContactUs(ContactUsModel objContactUsModel)
        {
            try
            {
                bool bFlag = false;
                user_master AdminDetails = dbConnection.user_master.Where(x => x.user_type_id == (Int32)EnumUserType.Admin && x.is_active).FirstOrDefault();
                contact_us_master objContactUsMaster = new contact_us_master();
                objContactUsMaster.ctc_name = objContactUsModel.name;
                objContactUsMaster.ctc_email = objContactUsModel.email;
                objContactUsMaster.ctc_message = objContactUsModel.message;
                objContactUsMaster.ctc_status_id = (Int32)EnumStatusMasterType.Pending;
                objContactUsMaster.is_active = false;
                objContactUsMaster.is_deleted = false;
                objContactUsMaster.created_by = AdminDetails.user_id;
                objContactUsMaster.created_on = DateTime.UtcNow;//Default False need Email Account Confirmation
                objContactUsMaster.modified_by = AdminDetails.user_id;
                objContactUsMaster.modified_on = DateTime.UtcNow;
                dbConnection.contact_us_master.Add(objContactUsMaster);
                dbConnection.SaveChanges();

                #region Get Email
                List<SettingModel> lstSettingModel = new List<SettingModel>();
                lstSettingModel = CommonService.GetSettingValues();
                CommonService objCommonService = new CommonService();
                #endregion
                try
                {
                    #region Send Mail To Admin
                    string strSubject = "Contact Us Enquiry";
                    string strBody = "<b>Hello</b> Admin," + "<br/><br/> <b>" + objContactUsModel.name + "</b> Wants to contact you.<br><by>Message : " + objContactUsModel.message + "<br/> Sender Email: " + objContactUsModel.email + " <br /> <p>Thank you,<br />Traderdock Team.";
                    new CommonService().SendEmailWithBody("support@traderdock.com", strSubject, strBody);
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new ServiceLayerException(ex);
                }
                try
                {
                    #region Send Mail To Applicant
                    string strSubject = "Contact Us Enquiry";
                    string strBody = "<b>Hello</b> " + objContactUsModel.name + "," + "<br/><br/>We would like to acknowledge that we have received your request.<br/>A support representative will be reviewing your request and will send you a personal response.(usually within 24 hours).<br/><br/>Thank you for your patience.<br/><br/>Please don’t reply to this automated email – we’ll reply to you directly from your support request.<br/><p>Thank you,<br />Traderdock Team.";
                    new CommonService().SendEmailWithBody(objContactUsModel.email, strSubject, strBody);
                    #endregion
                }
                catch (Exception ex)
                {
                    throw new ServiceLayerException(ex);
                }
                bFlag = true;
                return bFlag;
            }
            catch (Exception ex)
            {

                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Get Contact Us
        /// <summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Method for Get List Contact Us
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<ContactUsModel> GetCotanctUsByList(List<Int64> iDs)
        {
            try
            {
                List<ContactUsModel> lstContactUsModel = new List<ContactUsModel>();
                ContactUsModel objContactUsModel;
                List<contact_us_master> lstContactUsMaster = (from CUM in dbConnection.contact_us_master
                                                              where iDs.Contains(CUM.ctc_id)
                                                              select CUM).ToList();
                foreach (var item in lstContactUsMaster)
                {
                    objContactUsModel = new ContactUsModel();
                    objContactUsModel.ctc_id = item.ctc_id;
                    objContactUsModel.ctc_status_id = item.ctc_status_id;
                    objContactUsModel.email = item.ctc_email;
                    objContactUsModel.name = item.ctc_name;
                    objContactUsModel.message = item.ctc_message;
                    objContactUsModel.is_active = item.is_active;
                    objContactUsModel.is_deleted = item.is_deleted;
                    lstContactUsModel.Add(objContactUsModel);
                }
                return lstContactUsModel;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Method for Get single Contact Us
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ContactUsModel GetContactUs(Int64? id)
        {
            try
            {
                contact_us_master objContactUsMaster = dbConnection.contact_us_master.Where(x => x.ctc_id == id).FirstOrDefault();
                if (objContactUsMaster != null)
                {
                    ContactUsModel objContactUsModel = new ContactUsModel();
                    objContactUsModel.ctc_id = objContactUsMaster.ctc_id;
                    objContactUsModel.ctc_status_id = objContactUsMaster.ctc_status_id;
                    objContactUsModel.email = objContactUsMaster.ctc_email;
                    objContactUsModel.name = objContactUsMaster.ctc_name;
                    objContactUsModel.message = objContactUsMaster.ctc_message;
                    objContactUsModel.is_active = objContactUsMaster.is_active;
                    objContactUsModel.is_deleted = objContactUsMaster.is_deleted;
                    return objContactUsModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Method for Active In Active Contact Us
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="lIds"></param>
        /// <param name="LoginUserId"></param>
        /// <param name="IsActiveRequest"></param>
        /// <returns></returns>
        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<contact_us_master> objContactUsMaster = (from CUM in dbConnection.contact_us_master
                                                              where (lIds).Contains(CUM.ctc_id)
                                                              select CUM).AsEnumerable().ToList();
                objContactUsMaster.ForEach(x => x.modified_by = LoginUserId);
                objContactUsMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objContactUsMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region ContactUs List
        public PagedList<ContactUsModel> ContactUsList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<ContactUsModel> lstContactUsModel = new List<ContactUsModel>();

                //Linq Query
                var query =
                        from CUM in dbConnection.contact_us_master
                            //((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                            //  .Where(um => um.ctc_name.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where CUM.is_deleted == false && CUM.ctc_name.Contains(filter) && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? CUM.is_active == true || CUM.is_active == false : ((statusFilter == "Yes") ? CUM.is_active == true : CUM.is_active == false))
                        orderby CUM.ctc_id descending
                        select new
                        {
                            ctc_id = CUM.ctc_id,
                            ctc_name = CUM.ctc_name,
                            ctc_message = CUM.ctc_message,
                            ctc_email = CUM.ctc_email,
                            is_active = CUM.is_active,
                            is_deleted = CUM.is_deleted,
                            ctc_status_id = CUM.ctc_status_id,
                        };

                if (query != null)
                {
                    ContactUsModel objContactUsModel;
                    foreach (var model in query)
                    {
                        objContactUsModel = new ContactUsModel();
                        objContactUsModel.ctc_id = model.ctc_id;
                        objContactUsModel.name = model.ctc_name;
                        objContactUsModel.message = model.ctc_message;
                        objContactUsModel.email = model.ctc_email;
                        objContactUsModel.is_active = model.is_active;
                        objContactUsModel.is_deleted = model.is_deleted;
                        objContactUsModel.ctc_status_id = model.ctc_status_id;
                        objContactUsModel.cms_status = (model.is_active == true) ? "true" : "false";
                        lstContactUsModel.Add(objContactUsModel);
                    }
                    var result = new PagedList<ContactUsModel>(lstContactUsModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete Contact Us
        /// <summary>
        /// Date: 03-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Delete Contact Us 
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="lstCotactUsModel"></param>
        /// <returns></returns>
        public bool DeleteContactUs(IList<ContactUsModel> lstCotactUsModel)
        {
            try
            {
                bool bResult = false;
                List<long> IDs = lstCotactUsModel.Select(x => x.ctc_id).ToList();
                List<contact_us_master> objContctUsMaster = dbConnection.contact_us_master.Where(y => IDs.Contains(y.ctc_id)).ToList();
                objContctUsMaster.ForEach(x => x.is_deleted = true);
                objContctUsMaster.ForEach(x => x.modified_by = lstCotactUsModel.Select(y => y.modified_by).FirstOrDefault());
                objContctUsMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}
