﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Entity.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class CountryService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region COuntry Master List
        /// <summary>
        /// FAQ MAster List Kendo Grid
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        public PagedList<CountryModel> CountryList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<CountryModel> lsCountryModel = new List<CountryModel>();
                 
                //Linq Query
                var query =  from FM in dbConnection.country_master
                             .Where(x => ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? x.country_status_id == true || x.country_status_id == false : ((statusFilter == "Yes") ? x.country_status_id == true : x.country_status_id == false))).DefaultIfEmpty()
                             orderby FM.country_name ascending
                             select new
                            {
                                country_id = FM.country_id,
                                country_name = FM.country_name,
                                country_code = FM.country_code,
                                is_active = FM.country_status_id
                                };
                if (!string.IsNullOrEmpty(filter))
                {
                    query = query.Where(x => x.country_name.Contains(filter));
                }
                if (query != null)
                {
                    CountryModel objCountryModel;
                    foreach (var model in query)
                    {
                        objCountryModel = new CountryModel();
                        objCountryModel.country_id = model.country_id;
                        objCountryModel.country_name = model.country_name;
                        objCountryModel.country_code = model.country_code;
                        objCountryModel.is_active = model.is_active;
                        objCountryModel.country_status = model.is_active == true ? "Active" : "InActive";
                        lsCountryModel.Add(objCountryModel);
                    }
                    var result = new PagedList<CountryModel>(lsCountryModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Country
        /// <summary>
        /// Get FAQ By FAQ ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        public CountryModel GetCountryModel(Int64? id)
        {
            try
            {
                country_master objCountryMaster = dbConnection.country_master.Where(x => x.country_id == id).FirstOrDefault();
                if (objCountryMaster != null)
                {
                    CountryModel objCountryModel = new CountryModel();
                    objCountryModel.country_id = objCountryMaster.country_id;
                    objCountryModel.country_name = objCountryMaster.country_name;
                    objCountryModel.country_code = objCountryMaster.country_code;
                    objCountryModel.is_active = objCountryMaster.country_status_id;
                    objCountryModel.country_status = objCountryMaster.country_status_id == true ? "True" : "False";
                    return objCountryModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Country Active/Inactive
        /// <summary>
        /// FAQ Active And Inactive From Kendo Grid
        /// </summary>
        /// <param name="lIds"></param>
        /// <param name="LoginUserId"></param>
        /// <param name="IsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 03/Dec/2018
        /// Dev By: Hardik Savaliya
        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;
                List<country_master> objCountryMaster = (from FM in dbConnection.country_master
                                                 where (lIds).Contains(FM.country_id)
                                                 select FM).AsEnumerable().ToList();
                objCountryMaster.ForEach(x => x.modified_by =  Convert.ToInt32(LoginUserId));
                objCountryMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objCountryMaster.ForEach(x => x.country_status_id = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 
        #endregion

    }
}
