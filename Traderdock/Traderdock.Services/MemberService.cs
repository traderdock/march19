﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class MemberService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        CommonService objCommonService;
        #endregion

        #region Login
        /// <summary>
        /// Login
        /// </summary>
        /// <param name="objLoginModel"></param>
        /// <returns></returns>
        public MemberModel Login(LoginModel objLoginModel)
        {
            MemberModel objMemberModel = new MemberModel();
            user_master objMemberMaster = dbConnection.user_master.Where(x => (x.user_email == objLoginModel.Email) && x.is_deleted == false).FirstOrDefault();
            if (objMemberMaster != null)
            {
                var password = Utilities.DecryptPassword(objMemberMaster.password, objMemberMaster.crypto_key);
                if (password == objLoginModel.Password)
                {
                    objMemberModel = GetUser(objMemberMaster.user_id);
                }
                else
                {
                    objMemberModel.is_password_false = true;
                }
            }
            else
            {
                objMemberModel.is_email_false = true;
            }
            return objMemberModel;
        }
        #endregion

        #region Add/Edit User
        /// <summary>
        /// Add Edit User
        /// </summary>
        /// <param name="objMemberModel"></param>
        /// <returns></returns>
        public MemberModel AddEditUser(MemberModel objMemberModel, string _AttachmentPath = "")
        {
            try
            {
                if (objMemberModel.user_id > 0)
                {
                    user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == objMemberModel.user_id).FirstOrDefault();
                    objUserMaster.first_name = objMemberModel.first_name == null ? objUserMaster.first_name : objMemberModel.first_name;
                    objUserMaster.last_name = objMemberModel.last_name == null ? objUserMaster.last_name : objMemberModel.last_name;
                    objUserMaster.user_email = objMemberModel.user_email == null ? objUserMaster.user_email : objMemberModel.user_email;
                    objUserMaster.address = objMemberModel.address == null ? objUserMaster.address : objMemberModel.address;
                    objUserMaster.city_name = objMemberModel.city_name == null ? objUserMaster.city_name : objMemberModel.city_name;
                    objUserMaster.postal_code = objMemberModel.postal_code == null ? objUserMaster.postal_code : objMemberModel.postal_code;
                    objUserMaster.country_id = objMemberModel.country_id == 0 ? objUserMaster.country_id : objMemberModel.country_id;
                    objUserMaster.state_name = objMemberModel.state_name == null ? objUserMaster.state_name : objMemberModel.state_name;
                    objUserMaster.gender = objMemberModel.gender == null ? objUserMaster.gender : objMemberModel.gender;
                    if (objMemberModel.birth_date != null)
                    {
                        var dateString = objMemberModel.birth_date.ToString();
                        if (dateString.Contains("-"))
                        {
                            dateString = dateString.Replace("-", "/");
                        }
                        var dateTime = DateTime.ParseExact(dateString, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
                        objUserMaster.birth_date = Convert.ToDateTime(dateTime);
                    }
                    objUserMaster.skypename = objMemberModel.SkypeName == null ? objUserMaster.skypename : objMemberModel.SkypeName;
                    objUserMaster.contact_number = objMemberModel.contact_number == null ? objUserMaster.contact_number : objMemberModel.contact_number;
                    if (objMemberModel.profile_image_url != null)
                    {
                        if (!string.IsNullOrEmpty(objUserMaster.profile_image_url))
                        {
                            var _FileName = Utilities.GetImageName(objUserMaster.profile_image_url);
                            if (!string.IsNullOrEmpty(_AttachmentPath) && _FileName != Utilities.GetImageName(objMemberModel.profile_image_url))
                            {
                                if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                                {
                                    System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                                }
                            }
                        }
                        objUserMaster.profile_image_url = Utilities.GetImageName(objMemberModel.profile_image_url);
                    }
                    objUserMaster.modified_on = objMemberModel.modified_on;
                    objUserMaster.modified_by = objMemberModel.modified_by;
                    dbConnection.SaveChanges();
                    //Trial Entry into user_subscription
                    if (objMemberModel.IsTrial == true)
                    {
                        user_subscription objuserSubscription = new user_subscription();
                        objuserSubscription.user_id = objMemberModel.user_id;
                        objuserSubscription.subscription_id = 8; //by default for trial user
                        objuserSubscription.transaction_id = "";
                        objuserSubscription.order_amount = 0;
                        objuserSubscription.payment_method = "";
                        objuserSubscription.subscription_start_date = DateTime.UtcNow;
                        objuserSubscription.subscription_end_date = DateTime.UtcNow.AddDays(14);
                        objuserSubscription.is_expired = false;
                        objuserSubscription.is_current = true;
                        objuserSubscription.free_trial = true;
                        objuserSubscription.created_by = objMemberModel.created_by;
                        objuserSubscription.created_date = objMemberModel.created_on;
                        objuserSubscription.updated_by = objMemberModel.modified_by;
                        objuserSubscription.updated_date = objMemberModel.modified_on;
                        dbConnection.user_subscription.Add(objuserSubscription);
                        dbConnection.SaveChanges();
                        objMemberModel.subscription_id = 8;
                    }
                    return objMemberModel;
                }
                else
                {
                    if (dbConnection.user_master.Where(x => x.user_email.Trim().ToLower() == objMemberModel.user_email && x.is_deleted == false).Count() > 0)
                    {
                        objMemberModel = new MemberModel();
                        objMemberModel.user_id = -1;
                        return objMemberModel;
                    }
                    user_master objUserMaster = new user_master();
                    objUserMaster.first_name = objMemberModel.first_name;
                    objUserMaster.last_name = objMemberModel.last_name;
                    objUserMaster.user_email = objMemberModel.user_email;
                    objUserMaster.crypto_key = Utilities.GenerateKey();
                    objUserMaster.password = Utilities.EncryptPassword(objMemberModel.password, objUserMaster.crypto_key);
                    objUserMaster.gender = objMemberModel.gender;
                    objUserMaster.trading_platform = objMemberModel.rbPlatform;
                    objUserMaster.postal_code = objMemberModel.postal_code;
                    objUserMaster.city_name = objMemberModel.city_name;
                    objUserMaster.state_name = objMemberModel.state_name;
                    if (objMemberModel.country_id > 0)
                    {
                        objUserMaster.country_id = objMemberModel.country_id;
                    }
                    objUserMaster.address = objMemberModel.address == null ? "" : objMemberModel.address;
                    objUserMaster.contact_number = objMemberModel.contact_number;
                    objUserMaster.profile_image_url = Utilities.GetImageName(objMemberModel.profile_image_url);
                    //Change by shoaib - 08-Aug-2017 (posted a message on teamwork)
                    //No need of verification now, once user is registered redirect it to dashboard
                    if (objMemberModel.IsTrial == true)
                        objUserMaster.is_active = false;
                    else
                        objUserMaster.is_active = true;
                    objUserMaster.is_blocked = false;
                    objUserMaster.is_deleted = false;
                    objUserMaster.user_type_id = (Int32)EnumUserType.Normal;
                    if (objMemberModel.subscription_id > 0)
                        objUserMaster.subscription_mode = (Int32)EnumSubscriptionMode.Subscribed;
                    else
                        objUserMaster.subscription_mode = (Int32)EnumSubscriptionMode.Free_Trial;
                    objUserMaster.is_number_verified = false;
                    objUserMaster.is_admin_verified = true;
                    objUserMaster.created_on = objMemberModel.created_on;
                    objUserMaster.created_by = objMemberModel.created_by;
                    dbConnection.user_master.Add(objUserMaster);
                    dbConnection.SaveChanges();
                    objMemberModel.user_id = objUserMaster.user_id;

                    //For Free Trial  - Client Feedback (14-Sep-2017) - By Shoaib
                    //Trial Entry into user_subscription
                    if (objMemberModel.IsTrial == true)
                    {
                        user_subscription objuserSubscription = new user_subscription();
                        objuserSubscription.user_id = objMemberModel.user_id;
                        objuserSubscription.subscription_id = 8; //by default for trial user
                        objuserSubscription.transaction_id = "";
                        objuserSubscription.order_amount = 0;
                        objuserSubscription.payment_method = "";
                        objuserSubscription.subscription_start_date = DateTime.UtcNow;
                        objuserSubscription.subscription_end_date = DateTime.UtcNow.AddDays(14);
                        objuserSubscription.is_expired = false;
                        objuserSubscription.is_current = true;
                        objuserSubscription.free_trial = true;
                        objuserSubscription.created_by = objMemberModel.created_by;
                        objuserSubscription.created_date = objMemberModel.created_on;
                        objuserSubscription.updated_by = objMemberModel.modified_by;
                        objuserSubscription.updated_date = objMemberModel.modified_on;
                        dbConnection.user_subscription.Add(objuserSubscription);
                        dbConnection.SaveChanges();
                        objMemberModel.subscription_id = 8;
                    }
                }
                return objMemberModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Users
        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="iDs"></param>
        /// <returns></returns>
        public List<user_master> GetUsers(List<Int64> iDs)
        {
            try
            {
                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where iDs.Contains(UM.user_id)
                                                   select UM).ToList();
                return objUserMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }

        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public MemberModel GetUser(Int64? id)
        {
            try
            {
                var Query = (from UM in dbConnection.user_master
                             join CM in dbConnection.country_master on UM.country_id equals CM.country_id into bag1
                             from to in bag1.DefaultIfEmpty()
                             where UM.user_id == id && UM.is_deleted == false
                             select new
                             {
                                 UM,
                                 CountryID = to == null ? 0 : to.country_id,
                                 CountryName = to == null ? string.Empty : to.country_name
                             }).FirstOrDefault();
                if (Query != null)
                {
                    MemberModel objMemberModel = new MemberModel();
                    objMemberModel.user_id = Query.UM.user_id;
                    objMemberModel.first_name = Query.UM.first_name;
                    objMemberModel.last_name = Query.UM.last_name;
                    objMemberModel.postal_code = Query.UM.postal_code;
                    objMemberModel.user_email = Query.UM.user_email;
                    objMemberModel.gender = Query.UM.gender;
                    objMemberModel.gender_String = Query.UM.gender != null ? (Query.UM.gender == true ? "Male" : "Female") : null;
                    objMemberModel.birth_date = Query.UM.birth_date != null ? (Query.UM.birth_date.Value.ToString("dd-MM-yyyy")) : null;
                    objMemberModel.city_name = Query.UM.city_name;
                    objMemberModel.state_name = Query.UM.state_name;
                    ///not proper condition
                    //if (UserDetails.country_id != null)
                    if (Query.UM.country_id > 0)
                    {
                        objMemberModel.country_id = Query.UM.country_id.Value;
                        //objMemberModel.country_name = objMemberModel.country_id > 0 ? UserDetails.country_master.country_name : "Select Country";
                        objMemberModel.country_name = Query.CountryName;
                    }
                    objMemberModel.SkypeName = Query.UM.skypename;
                    objMemberModel.address = Query.UM.address;
                    objMemberModel.profile_image_url = !string.IsNullOrEmpty(Query.UM.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + Query.UM.profile_image_url : string.Empty;//System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"];
                    objMemberModel.FileName = !string.IsNullOrEmpty(Query.UM.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + Query.UM.profile_image_url : string.Empty;//System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"];
                    objMemberModel.contact_number = Query.UM.contact_number;
                    objMemberModel.user_status = Query.UM.is_active == true ? "Active" : "Inactive";
                    objMemberModel.is_active = Query.UM.is_active;
                    objMemberModel.is_blocked = Query.UM.is_blocked;
                    objMemberModel.user_type_id = Query.UM.user_type_id;
                    objMemberModel.is_deleted = Query.UM.is_deleted;
                    objMemberModel.is_admin_verified = Query.UM.is_admin_verified;
                    objMemberModel.is_number_verified = Query.UM.is_number_verified;
                    objMemberModel.crypto_key = Query.UM.crypto_key;
                    return objMemberModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Active In Active
        /// <summary>
        /// Active In Active
        /// </summary>
        /// <param name="lIds"></param>
        /// <param name="LoginUserId"></param>
        /// <param name="IsActiveRequest"></param>
        /// <returns></returns>
        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where (lIds).Contains(UM.user_id)
                                                   select UM).AsEnumerable().ToList();
                objUserMaster.ForEach(x => x.modified_by = LoginUserId);
                objUserMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objUserMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delete User
        /// <summary>
        /// Delete User
        /// </summary>
        /// <param name="objUserMasterModelList"></param>
        /// <returns></returns>
        public bool DeleteUsers(IList<user_master> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;

                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where (objUserMasterModelList.Select(x => x.user_id)).Contains(UM.user_id)
                                                   select UM).ToList();
                objUserMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check User Email Exists
        /// <summary>
        /// Check User Email Exists
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool CheckUserEmailExist(string email)
        {
            try
            {
                var userMaster = dbConnection.user_master.Where(x => x.user_email == email && x.is_deleted == false).FirstOrDefault();
                bool bFlag = (userMaster != null) ? true : false;
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Forgot/Change Password
        /// <summary>
        /// Forrget Password
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public bool ForgotPassword(string email, string sSiteLink)
        {
            MemberModel objMemberModel = new MemberModel();
            user_master objMemberMaster = new user_master();

            dbConnection = new Traderdock_DBEntities();
            objCommonService = new CommonService();
            bool Result = false;
            if (!string.IsNullOrEmpty(email))
            {
                objMemberMaster = dbConnection.user_master.Where(x => x.user_email == email && x.is_deleted != true && x.user_type_id == (Int32)EnumUserType.Normal).FirstOrDefault();
                if (objMemberMaster != null)
                {
                    objMemberMaster.crypto_key = Utilities.GenerateKey();
                    objMemberMaster.password = Utilities.EncryptPassword(objMemberMaster.crypto_key, objMemberMaster.crypto_key);
                    dbConnection.SaveChanges();
                    Result = true;

                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.FullName, objMemberMaster.first_name);
                    parameters.Add(EmailTemplateParameters.Email, Utilities.EncryptPassword(objMemberMaster.user_email, objMemberMaster.first_name));
                    parameters.Add(EmailTemplateParameters.Password, objMemberMaster.crypto_key);
                    parameters.Add(EmailTemplateParameters.Adminlink, sSiteLink + "/ConfirmRegistration");
                    bool bEmailResult = objCommonService.SendEmail(email, EmailTemplateNames.ForgotPassword, parameters, null);

                }
            }
            return Result;
        }

        /// <summary>
        /// Password Reset
        /// </summary>
        /// <param name="e"></param>
        /// <param name="k"></param>
        /// <returns></returns>
        public MemberModel PasswordReset(string e, string k)
        {
            MemberModel objUserMasterModel = new MemberModel();
            user_master userMaster = dbConnection.user_master.Where(x => x.user_email.Equals(e) && x.user_type_id == (int)EnumUserType.Normal && x.is_active).FirstOrDefault();
            if (userMaster != null)
            {
                objUserMasterModel = GetUser(userMaster.user_id);
            }
            return objUserMasterModel;

        }

        /// <summary>
        /// Password Reset on Request
        /// </summary>
        /// <param name="objUserPasswordModel"></param>
        /// <returns></returns>
        public bool PasswordReset(ChangePasswordModel objUserPasswordModel)
        {
            bool Status = false;
            user_master objuser_master = dbConnection.user_master.Where(x => x.user_email.Equals(objUserPasswordModel.email) && x.is_active).FirstOrDefault();
            if (objuser_master != null)
            {
                if (objUserPasswordModel.new_password == objUserPasswordModel.confirm_password)
                {
                    objuser_master.crypto_key = Utilities.GenerateKey();
                    objuser_master.password = Utilities.EncryptPassword(objUserPasswordModel.new_password, objuser_master.crypto_key);
                    dbConnection.SaveChanges();
                    Status = true;
                }
            }
            return Status;
        }

        /// <summary>
        /// Password Change
        /// </summary>
        /// <param name="objChangePasswordModel"></param>
        /// <param name="lUserId"></param>
        /// <returns></returns>
        public bool PasswordChange(ChangePasswordModel objChangePasswordModel, long lUserId)
        {
            bool Status = false;
            user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == lUserId).FirstOrDefault();
            if (objUserMaster != null)
            {
                string OLD_PASSWORD = Utilities.DecryptPassword(objUserMaster.password, objUserMaster.crypto_key);
                if (OLD_PASSWORD.Equals(objChangePasswordModel.old_password))
                {
                    objUserMaster.crypto_key = Utilities.GenerateKey();
                    objUserMaster.password = Utilities.EncryptPassword(objChangePasswordModel.new_password, objUserMaster.crypto_key);
                    dbConnection.SaveChanges();
                    Status = true;
                }
            }
            return Status;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Confirm Registration
        /// Date: 30-June-2017
        /// Dev By: Hardik Savaliya
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="email"></param>
        /// <param name="first_name"></param>
        /// <returns></returns>
        public bool ConfirmRegistration(string email, string key)
        {
            dbConnection = new Traderdock_DBEntities();
            bool bResult = false;
            user_master objUserMaster = dbConnection.user_master.Where(x =>  x.crypto_key == key && x.is_deleted == false).FirstOrDefault();
            if (objUserMaster != null)
            {
                if (objUserMaster.is_active)
                {
                    bResult = false;
                }
                else
                {
                    objUserMaster.is_active = true;
                    dbConnection.SaveChanges();
                    bResult = true;
                }
            }
            return bResult;
        }
        #endregion

        #region Get Subscription Details
        /// <summary>
        /// Get Subscription Details
        /// </summary>
        /// <param name="userID"></param>
        ///  Date : 21/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public SubscriptionDetails GetSubsciptionDetails(Int64 userID = 0)
        {
            try
            {
                SubscriptionDetails objModel = new SubscriptionDetails();

                var query = (from SM in dbConnection.subscription_master
                             join US in dbConnection.user_subscription on SM.subscription_id equals US.subscription_id
                             where US.user_id == userID & US.is_current == true
                             select new { SM }).FirstOrDefault();
                if (query != null)
                {
                    objModel = new SubscriptionDetails();
                    objModel.subscription_id = query.SM.subscription_id;
                    objModel.subscription_name = query.SM.subscription_name;
                    objModel.starting_balance = decimal.Round(query.SM.starting_balance, 0, MidpointRounding.AwayFromZero);
                    objModel.max_position_size = query.SM.max_position_size == null ? 0 : query.SM.max_position_size;
                    objModel.daily_loss_limit = query.SM.daily_loss_limit == null ? 0 : query.SM.daily_loss_limit;
                    objModel.daily_loss_limit = decimal.Round(objModel.daily_loss_limit.Value, 0, MidpointRounding.AwayFromZero);
                    objModel.max_drawdown = query.SM.max_drawdown == null ? 0 : query.SM.max_drawdown;
                    objModel.max_drawdown = decimal.Round(objModel.max_drawdown.Value, 0, MidpointRounding.AwayFromZero);
                    objModel.profit_target = decimal.Round(query.SM.profite_target, 0, MidpointRounding.AwayFromZero);
                    objModel.price_per_month = decimal.Round(query.SM.price_per_month, 0, MidpointRounding.AwayFromZero);

                    return objModel;
                }
                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Member Data For CSV
        /// <summary>
        /// GetDataForCSV
        /// </summary>
        /// dev by : Bharat Katua
        /// date : 26/07/2017
        /// <param name="userID"></param>
        /// <returns></returns>
        public MemberModel GetDataForCSV(Int64 userID = 0)
        {
            try
            {
                MemberModel objmodel = new MemberModel();

                var Query = (from UM in dbConnection.user_master
                                 //join CM in dbConnection.country_master on UM.country_id equals CM.country_id
                                 //Commented on 26/07/2017 due to state_master change
                                 //join SM in dbConnection.state_master on UM.state_id equals SM.state_id
                             where UM.is_active == true && UM.user_id == userID
                             //select new { UM, CM }).FirstOrDefault();
                             select new { UM }).FirstOrDefault();
                //select new { UM, CM, SM }).FirstOrDefault();

                if (Query != null)
                {
                    objmodel = new MemberModel();
                    objmodel.user_id = Query.UM.user_id;
                    objmodel.first_name = Query.UM.first_name == null ? "NA" : Query.UM.first_name;
                    objmodel.last_name = Query.UM.last_name == null ? "NA" : Query.UM.last_name;
                    objmodel.user_email = Query.UM.user_email == null ? "NA" : Query.UM.user_email;
                    objmodel.address = Query.UM.address == null ? "NA" : Query.UM.address;
                    objmodel.city_name = Query.UM.city_name == null ? "NA" : Query.UM.city_name;
                    objmodel.country_id = Query.UM.country_id.Value;
                    objmodel.country_name = dbConnection.country_master.Where(x => x.country_id == objmodel.country_id).Select(x => x.country_name).FirstOrDefault(); // Query.CM.country_name == null ? "NA" : Query.CM.country_name;
                    objmodel.country_name = string.IsNullOrEmpty(objmodel.country_name) ? string.Empty : objmodel.country_name;
                    //Commented on 26/07/2017 due to state_master change
                    objmodel.state_name = Query.UM.state_name == null ? "NA" : Query.UM.state_name;
                    objmodel.postal_code = Query.UM.postal_code == null ? "NA" : Query.UM.postal_code;
                    objmodel.contact_number = Query.UM.contact_number == null ? "NA" : Query.UM.contact_number;
                    string encPassword = Query.UM.password;
                    string CryptoKey = Query.UM.crypto_key;
                    objmodel.password = Utilities.DecryptPassword(encPassword, CryptoKey);
                    objmodel.subscription_id = dbConnection.user_subscription.Where(x => x.user_id == objmodel.user_id).OrderByDescending(x => x.user_subscription_id).FirstOrDefault().subscription_id;

                    return objmodel;
                }
                return objmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Users by Email
        /// <summary>
        /// Get User
        /// </summary>
        /// <param name="sEmail"></param>
        /// <returns></returns>
        public MemberModel GetUserByEmail(string sEmail)
        {
            try
            {
                var Query = (from UM in dbConnection.user_master
                             join CM in dbConnection.country_master on UM.country_id equals CM.country_id into bag1
                             from to in bag1.DefaultIfEmpty()
                             where UM.user_email.Equals(sEmail) && UM.is_active == true
                             select new
                             {
                                 UM,
                                 CountryID = to == null ? 0 : to.country_id,
                                 CountryName = to == null ? string.Empty : to.country_name
                             }).FirstOrDefault();
                if (Query != null)
                {
                    MemberModel objMemberModel = new MemberModel();
                    objMemberModel.user_id = Query.UM.user_id;
                    objMemberModel.first_name = Query.UM.first_name;
                    objMemberModel.last_name = Query.UM.last_name;
                    objMemberModel.postal_code = Query.UM.postal_code;
                    objMemberModel.user_email = Query.UM.user_email;
                    objMemberModel.gender = Query.UM.gender;
                    objMemberModel.gender_String = Query.UM.gender != null ? (Query.UM.gender == true ? "Male" : "Female") : null;
                    objMemberModel.birth_date = Query.UM.birth_date != null ? (Query.UM.birth_date.Value.ToString("dd-MM-yyyy")) : null;
                    objMemberModel.city_name = Query.UM.city_name;
                    objMemberModel.state_name = Query.UM.state_name;
                    ///not proper condition
                    //if (UserDetails.country_id != null)
                    if (Query.UM.country_id > 0)
                    {
                        objMemberModel.country_id = Query.UM.country_id.Value;
                        //objMemberModel.country_name = objMemberModel.country_id > 0 ? UserDetails.country_master.country_name : "Select Country";
                        objMemberModel.country_name = Query.CountryName;
                    }
                    objMemberModel.SkypeName = Query.UM.skypename;
                    objMemberModel.address = Query.UM.address;
                    objMemberModel.profile_image_url = !string.IsNullOrEmpty(Query.UM.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + Query.UM.profile_image_url : string.Empty;//System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"];
                    objMemberModel.FileName = !string.IsNullOrEmpty(Query.UM.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + Query.UM.profile_image_url : string.Empty;//System.Configuration.ConfigurationManager.AppSettings["DefaultImageURL"];
                    objMemberModel.contact_number = Query.UM.contact_number;
                    objMemberModel.user_status = Query.UM.is_active == true ? "Active" : "Inactive";
                    objMemberModel.is_active = Query.UM.is_active;
                    objMemberModel.is_blocked = Query.UM.is_blocked;
                    objMemberModel.is_deleted = Query.UM.is_deleted;
                    objMemberModel.is_admin_verified = Query.UM.is_admin_verified;
                    objMemberModel.is_number_verified = Query.UM.is_number_verified;
                    objMemberModel.crypto_key = Query.UM.crypto_key;
                    return objMemberModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }

}

