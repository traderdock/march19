﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Entity.MemberModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class FAQMemberService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get FAQ From CategoryID
        /// <summary>
        /// FAQ Details From CategoryID
        /// </summary>
        /// <param name="FAQ_cat_id"></param>
        /// <returns></returns>
        public FAQViewModel FAQListBySubCategoryID(Int64 iFAQSubCategoryID, string sSearchDesc)
        {
            try
            {
                FAQViewModel objFAQViewModel = new FAQViewModel();
                List<faq_master> FAQCategoryDetails = dbConnection.faq_master.Where(x => x.faq_sub_category_id == iFAQSubCategoryID && x.is_active).OrderBy(x => x.order_sequence).ToList();
                if (FAQCategoryDetails.Count() > 0)
                {
                    objFAQViewModel.sub_faq_category_id = FAQCategoryDetails.FirstOrDefault().faq_sub_category_master.faq_sub_category_id;
                    objFAQViewModel.sub_category_title = FAQCategoryDetails.FirstOrDefault().faq_sub_category_master.sub_category_title;
                    objFAQViewModel.sub_category_description = FAQCategoryDetails.FirstOrDefault().faq_sub_category_master.sub_category_description;
                    objFAQViewModel.FAQList = new List<FAQModel>();
                    List<faq_master> objFAQMasterList;
                    if (!string.IsNullOrEmpty(sSearchDesc))
                    {
                        objFAQMasterList = FAQCategoryDetails.Where(x => x.is_active && !x.is_deleted && (x.question.ToLower().Contains(sSearchDesc.ToLower()) || (x.answer.ToLower().Contains(sSearchDesc.ToLower())))).ToList();
                    }
                    else
                    {
                        objFAQMasterList = FAQCategoryDetails.Where(x => x.is_active && !x.is_deleted).ToList();
                    }
                    foreach (var FAQMasterDetails in objFAQMasterList)
                    {
                        FAQModel objFAQModel = new FAQModel();
                        objFAQModel.faq_id = FAQMasterDetails.faq_id;
                        objFAQModel.question = FAQMasterDetails.question;
                        objFAQModel.answer = FAQMasterDetails.answer;
                        objFAQViewModel.FAQList.Add(objFAQModel);
                    }
                    objFAQViewModel.FAQCategoryModel = new Entity.ViewModel.FAQCategoryModel()
                    {
                        faq_category_id = FAQCategoryDetails.FirstOrDefault().faq_category_master.faq_category_id,
                        category_title = FAQCategoryDetails.FirstOrDefault().faq_category_master.category_title,
                        category_description = FAQCategoryDetails.FirstOrDefault().faq_category_master.category_description,
                        category_image = FAQCategoryDetails.FirstOrDefault().faq_category_master.category_image
                    };
                }
                else
                {
                    return null;
                }
                return objFAQViewModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
