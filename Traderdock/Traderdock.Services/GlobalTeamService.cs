﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class GlobalTeamService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region GlobalTeam List
        public PagedList<GlobalTeamModel> GlobalTeamList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "")
        {
            try
            {
                IList<GlobalTeamModel> lstUserModel = new List<GlobalTeamModel>();
                //Linq Query
                var query =
                        from UM in dbConnection.global_team
                          .Where(um => um.user_name.Contains(filter) && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where UM.is_deleted == false
                        orderby UM.global_team_id descending
                        select new
                        {
                            global_team_id = UM.global_team_id,
                            user_name = UM.user_name,
                            designation = UM.designation,
                            description = UM.description,
                            user_image_url_sm = UM.user_image_url_sm,
                            user_image_url_lg = UM.user_image_url_lg,
                            is_active = UM.is_active,
                        };

                if (query != null)
                {
                    GlobalTeamModel objModel;
                    foreach (var model in query)
                    {
                        objModel = new GlobalTeamModel();
                        objModel.global_team_id = model.global_team_id;
                        objModel.user_name = model.user_name;
                        objModel.designation = model.designation;
                        objModel.description = model.description;
                        objModel.user_image_url_sm = !string.IsNullOrEmpty(model.user_image_url_sm) ? model.user_image_url_sm : "experience-img1.png";
                        objModel.user_image_url_lg = !string.IsNullOrEmpty(model.user_image_url_lg) ? model.user_image_url_lg : "experience-img1.png";
                        objModel.is_active = model.is_active;
                        lstUserModel.Add(objModel);
                    }
                    var result = new PagedList<GlobalTeamModel>(lstUserModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public GlobalTeamModel GetGlobalTeam(Int64? id)
        {
            try
            {
                global_team objDetails = dbConnection.global_team.Where(x => x.global_team_id == id).FirstOrDefault();
                if (objDetails != null)
                {
                    GlobalTeamModel objModel = new GlobalTeamModel();
                    objModel.global_team_id = objDetails.global_team_id;
                    objModel.user_name = objDetails.user_name;
                    objModel.designation = objDetails.designation;
                    objModel.description = objDetails.description;
                    objModel.user_image_url_sm = !string.IsNullOrEmpty(objDetails.user_image_url_sm) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url_sm : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objModel.FileName_sm = !string.IsNullOrEmpty(objDetails.user_image_url_sm) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url_sm : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objModel.user_image_url_lg = !string.IsNullOrEmpty(objDetails.user_image_url_lg) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url_lg : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objModel.FileName_lg = !string.IsNullOrEmpty(objDetails.user_image_url_lg) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url_lg : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    return objModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddEditGlobalTeam(GlobalTeamModel objModel)
        {
            try
            {
                bool bFlag = false;
                if (objModel.global_team_id > 0)
                {
                    global_team objMaster = dbConnection.global_team.Where(x => x.global_team_id == objModel.global_team_id).FirstOrDefault();
                    objMaster.user_name = objModel.user_name;
                    objMaster.designation = objModel.designation;
                    objMaster.description = objModel.description;
                    if (objModel.user_image_url_sm != null)
                        objMaster.user_image_url_sm = Utilities.GetImageName(objModel.user_image_url_sm);
                    if (objModel.user_image_url_lg != null)
                        objMaster.user_image_url_lg = Utilities.GetImageName(objModel.user_image_url_lg);
                    objMaster.modified_on = objModel.modified_on;
                    objMaster.modified_by = objModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {

                    global_team objMaster = new global_team();
                    objMaster.user_name = objModel.user_name;
                    objMaster.designation = objModel.designation;
                    objMaster.description = objModel.description;
                    objMaster.user_image_url_sm = Utilities.GetImageName(objModel.user_image_url_sm);
                    objMaster.user_image_url_lg = Utilities.GetImageName(objModel.user_image_url_lg);
                    objMaster.is_deleted = false;
                    objMaster.is_active = true;
                    objMaster.created_on = objModel.created_on;
                    objMaster.created_by = objModel.created_by;
                    objMaster.modified_on = objModel.created_on;
                    objMaster.modified_by = objModel.created_by;
                    dbConnection.global_team.Add(objMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<global_team> GetGlobalTeams(List<Int64> iDs)
        {
            try
            {
                List<global_team> objUserMaster = (from UM in dbConnection.global_team
                                                   where iDs.Contains(UM.global_team_id)
                                                   select UM).ToList();
                return objUserMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool DeleteGlobalTeams(IList<global_team> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                var Ids = objUserMasterModelList.Select(x => x.global_team_id).ToList();
                List<global_team> objUserMaster = (from UM in dbConnection.global_team
                                                          where (Ids).Contains(UM.global_team_id)
                                                          select UM).ToList();
                objUserMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
