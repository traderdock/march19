﻿namespace Traderdock.Services
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Data;
    using System.Linq;

    /// <summary>
    /// Defines the <see cref="ExportService{T}" />
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class ExportService<T> : List<T>
    {
        /// <summary>
        /// The GenerateDataTable
        /// </summary>
        /// <param name="source">The source<see cref="IList{T}"/></param>
        /// <param name="HeaderRow">The HeaderRow<see cref="List{string}"/></param>
        /// <returns>The <see cref="DataTable"/></returns>
        public DataTable GenerateDataTable(IList<T> source, List<string> HeaderRow)
        {
            DataTable objDt = new DataTable();
            DataTable dataTable = new DataTable();

            if (source != null)
            {
                if (HeaderRow != null)
                {
                    PropertyDescriptorCollection propertyDescriptorCollection =
         TypeDescriptor.GetProperties(typeof(T));
                    for (int i = 0; i < propertyDescriptorCollection.Count; i++)
                    {
                        PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                        Type type = propertyDescriptor.PropertyType;

                        if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                            type = Nullable.GetUnderlyingType(type);


                        dataTable.Columns.Add(propertyDescriptor.Name, type);
                    }
                    object[] values = new object[propertyDescriptorCollection.Count];
                    foreach (T iListItem in source)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                        }
                        dataTable.Rows.Add(values);
                    }
                    string[] columnNames = dataTable.Columns.Cast<DataColumn>()
                                 .Select(x => x.ColumnName)
                                 .ToArray();
                    var c = columnNames.Except(HeaderRow).ToArray();
                    foreach (var item in c)
                    {
                        dataTable.Columns.Remove(item);
                    }
                    return dataTable;

                    //objDt.Rows.Add(new object[] { "James Bond, LLC", 120, "Garrison Neely", "123 3428749020", 35, "6.000", "$24,590", "$13,432",
                    //"$12,659", "12/13/21"});
                }
                else
                    return objDt;
            }
            else
                return objDt;
        }
    }
}
