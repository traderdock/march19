﻿namespace Traderdock.Services
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Entity.Model;
    using ORM;

    /// <summary>
    /// Defines the <see cref="ProfileSettingsService" />
    /// </summary>
    public class ProfileSettingsService
    {
        /// <summary>
        /// Defines the dbConnection
        /// </summary>
        internal Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();

        /// <summary>
        /// Settings List
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<ProfileTargetSettingsModel> SettingsList(string filter = "", int pageIndex = 1, int pageSize = 1, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<ProfileTargetSettingsModel> lstSettingModel = new List<ProfileTargetSettingsModel>();
                //Linq Query
                var query =
                        from S in dbConnection.settings
                        select new
                        {
                            SettingsId = S.settings_id,
                            SettingName = S.setting_name,
                            SettingValue = S.setting_value,
                            is_active = S.is_active,
                            is_delete = S.is_delete
                        };
                if (query != null)
                {
                    ProfileTargetSettingsModel objSettingModel;
                    foreach (var model in query)
                    {
                        objSettingModel = new ProfileTargetSettingsModel();
                        objSettingModel.setting_id = model.SettingsId;
                        objSettingModel.setting_name = model.SettingName;
                        objSettingModel.setting_value = model.SettingValue;
                        objSettingModel.is_delete = model.is_delete.Value;
                        objSettingModel.is_active = model.is_active.Value;
                        lstSettingModel.Add(objSettingModel);
                    }
                    var result = new PagedList<ProfileTargetSettingsModel>(lstSettingModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Common.Exceptions.ServiceLayerException(ex);
            }
        }

        /// <summary>
        /// Get Settings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ProfileTargetSettingsModel GetSetting(Int64? id)
        {
            try
            {
                setting objSettings = dbConnection.settings.Where(x => x.settings_id == id).FirstOrDefault();
                if (objSettings != null)
                {
                    ProfileTargetSettingsModel objSettingsModel = new ProfileTargetSettingsModel();
                    objSettingsModel.setting_id = objSettings.settings_id;
                    objSettingsModel.setting_name = objSettings.setting_name;
                    objSettingsModel.setting_value = objSettings.setting_value;
                    objSettingsModel.is_active = objSettings.is_active.Value;
                    objSettingsModel.is_delete = objSettings.is_delete.Value;
                    return objSettingsModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get Setting By Name
        /// </summary>
        /// <param name="sValue">The sValue<see cref="string"/></param>
        /// <returns></returns>
        public ProfileTargetSettingsModel GetSettingByName(string sValue)
        {
            try
            {
                setting objSettings = dbConnection.settings.Where(x => x.setting_name == sValue).FirstOrDefault();
                if (objSettings != null)
                {
                    ProfileTargetSettingsModel objSettingsModel = new ProfileTargetSettingsModel();
                    objSettingsModel.setting_id = objSettings.settings_id;
                    objSettingsModel.setting_name = objSettings.setting_name;
                    objSettingsModel.setting_value = objSettings.setting_value;
                    objSettingsModel.is_active = objSettings.is_active.Value;
                    objSettingsModel.is_delete = objSettings.is_delete.Value;
                    return objSettingsModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetSetting Values
        /// </summary>
        /// <returns></returns>
        public List<ProfileTargetSettingsModel> GetSettingList()
        {
            try
            {
                List<ProfileTargetSettingsModel> lstSettingModel = new List<ProfileTargetSettingsModel>();
                List<setting> lstSettings = dbConnection.settings.Where(x => x.is_delete == false).ToList();
                if (lstSettings != null)
                {
                    foreach (var item in lstSettings)
                    {
                        ProfileTargetSettingsModel objSettingsModel = new ProfileTargetSettingsModel();
                        objSettingsModel.setting_id = item.settings_id;
                        objSettingsModel.setting_name = item.setting_name;
                        objSettingsModel.setting_value = item.setting_value;
                        objSettingsModel.is_active = item.is_active.Value;
                        lstSettingModel.Add(objSettingsModel);
                    }
                    return lstSettingModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Add Edit Settings
        /// </summary>
        /// <param name="objSettingModel"></param>
        /// <returns></returns>
        public bool AddEditSettings(ProfileTargetSettingsModel objSettingModel)
        {
            try
            {
                bool bFlag = false;
                if (objSettingModel.setting_id > 0)
                {
                    setting objSetting = dbConnection.settings.Where(x => x.settings_id == objSettingModel.setting_id).FirstOrDefault();
                    objSetting.settings_id = objSettingModel.setting_id;
                    objSetting.setting_name = objSettingModel.setting_name;
                    objSetting.setting_value = objSettingModel.setting_value;
                    objSetting.modified_on = objSettingModel.modified_on;
                    objSetting.modified_by = objSettingModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }

                else
                {
                    setting objSetting = new setting();
                    objSetting.setting_name = objSettingModel.setting_name;
                    objSetting.setting_value = objSettingModel.setting_value;
                    objSetting.created_on = objSettingModel.modified_on;
                    objSetting.created_by = objSettingModel.modified_by;
                    dbConnection.settings.Add(objSetting);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
