﻿using System;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class LogServices
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Save API Log
        public bool SaveAPILog(APICallLogModel objAPICallModel)
        {
            try
            {
                api_call_log objAPICallMaster = new api_call_log();
                objAPICallMaster.api_enum = objAPICallModel.api_enum;
                objAPICallMaster.created_on = DateTime.UtcNow;
                objAPICallMaster.endpoint = objAPICallModel.endpoint;
                objAPICallMaster.is_success = objAPICallModel.is_success;
                objAPICallMaster.description = objAPICallModel.description;
                dbConnection.api_call_log.Add(objAPICallMaster);
                dbConnection.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }

        #endregion

    }
}
