﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using PayPal.Payments.Common;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.MemberModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class AlliedWalletPaymentService
    {
        #region Variables

        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();

        #endregion
        
        /// <summary>
        /// Paypal Express Checkout
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public AlliedWalletRedirect ExpressCheckout(AlliedWalletOrder order)
        {
            NameValueCollection values = new NameValueCollection();
            
            values["QuickPayToken"] = AlliedWalletSettings.QuickPayToken;
            values["MerchantID"] = AlliedWalletSettings.MerchantID;
            values["SiteID"] = AlliedWalletSettings.SiteID;
            values["SubscriptionPlanID"] = AlliedWalletSettings.SubscriptionPlanID200;
            values["AmountTotal"] = order.Amount.ToString();
            values = Submit(values);

            string ack = values["ACK"].ToLower();

            if (ack == "success" || ack == "successwithwarning")
            {
                return new AlliedWalletRedirect
                {
                    Token = values["TOKEN"],
                    Url = String.Format("https://{0}/cgi-bin/webscr?cmd=_express-checkout&token={1}&rm=2",
                        PayPalSetting.PayPalSettings.CgiDomain, values["TOKEN"])
                };
            }
            else
            {
                throw new Exception(values["L_LONGMESSAGE0"]);
            }

        }

        private NameValueCollection Submit(NameValueCollection values)
        {
            string data = String.Join("&", values.Cast<string>().Select(key => $"{key}={values[key]}"));
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(string.Format("https://{0}/nvp", PayPalSetting.PayPalSettings.ApiDomain));
            request.Method = "POST";
            request.KeepAlive = false;

            byte[] byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = byteArray.Length;

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();

            using (StreamReader reader = new StreamReader(request.GetResponse().GetResponseStream()))
            {
                return HttpUtility.ParseQueryString(reader.ReadToEnd());
            }
        }

        public bool GetCheckoutDetails(string token, ref string PayerID, ref string retMsg)
        {
            NameValueCollection values = new NameValueCollection();
            values["METHOD"] = "GetExpressCheckoutDetails";
            values["TOKEN"] = token;
            values["USER"] = PayPalSetting.PayPalSettings.Username;
            values["PWD"] = PayPalSetting.PayPalSettings.Password;
            values["SIGNATURE"] = PayPalSetting.PayPalSettings.Signature;
            values["SUBJECT"] = "";
            values["VERSION"] = "93";
            values = Submit(values);

            string ack = values["ACK"].ToLower();

            if (ack == "success" || ack == "successwithwarning")
            {
                PayerID = values["PayerID"];
                return true;
            }
            else
            {
                retMsg = "ErrorCode=" + values["L_ERRORCODE0"] + "&" +
                         "Desc=" + values["L_SHORTMESSAGE0"] + "&" +
                         "Desc2=" + values["L_LONGMESSAGE0"];

                return false;
            }
        }
    }
}
