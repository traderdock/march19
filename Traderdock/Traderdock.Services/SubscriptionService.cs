﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class SubscriptionService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Subscription List
        public PagedList<SubscriptionModel> SubscriptionList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<SubscriptionModel> lstSubscriptionModel = new List<SubscriptionModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.subscription_master
                          .Where(um => um.subscription_name.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where UM.is_deleted == false  //&& UM.subscription_id != 8

                        select new
                        {
                            subscription_id = UM.subscription_id,
                            subscription_number = UM.subscription_number,
                            subscription_name = UM.subscription_name,
                            starting_balance = UM.starting_balance,
                            max_position_size = UM.max_position_size,
                            daily_loss_limit = UM.daily_loss_limit,
                            max_drawdown = UM.max_drawdown,
                            profite_target = UM.profite_target,
                            price_per_month = UM.price_per_month,
                            Status = UM.is_active,
                        };

                if (query != null)
                {
                    SubscriptionModel objSubscriptionModel;
                    foreach (var model in query)
                    {
                        objSubscriptionModel = new SubscriptionModel();
                        objSubscriptionModel.subscription_name = model.subscription_name;
                        objSubscriptionModel.subscription_number = model.subscription_number == null ? 0 : model.subscription_number.Value;
                        objSubscriptionModel.subscription_id = model.subscription_id;
                        objSubscriptionModel.starting_balance = model.starting_balance;
                        objSubscriptionModel.max_position_size = model.max_position_size;
                        objSubscriptionModel.daily_loss_limit = model.daily_loss_limit;
                        objSubscriptionModel.max_drawdown = model.max_drawdown;
                        objSubscriptionModel.profite_target = model.profite_target;
                        objSubscriptionModel.price_per_month = model.price_per_month;
                        objSubscriptionModel.Status = model.Status;
                        lstSubscriptionModel.Add(objSubscriptionModel);
                    }
                    var result = new PagedList<SubscriptionModel>(lstSubscriptionModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Subscription list for front  - to hide free trial option
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<SubscriptionModel> SubscriptionListForFront(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<SubscriptionModel> lstSubscriptionModel = new List<SubscriptionModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.subscription_master
                          .Where(um => um.subscription_name.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where UM.is_deleted == false  && UM.subscription_id != 8

                        select new
                        {
                            subscription_id = UM.subscription_id,
                            subscription_name = UM.subscription_name,
                            starting_balance = UM.starting_balance,
                            max_position_size = UM.max_position_size,
                            daily_loss_limit = UM.daily_loss_limit,
                            max_drawdown = UM.max_drawdown,
                            profite_target = UM.profite_target,
                            price_per_month = UM.price_per_month,
                            Status = UM.is_active,
                        };

                if (query != null)
                {
                    SubscriptionModel objSubscriptionModel;
                    foreach (var model in query)
                    {
                        objSubscriptionModel = new SubscriptionModel();
                        objSubscriptionModel.subscription_name = model.subscription_name;
                        objSubscriptionModel.subscription_id = model.subscription_id;
                        objSubscriptionModel.starting_balance = model.starting_balance;
                        objSubscriptionModel.max_position_size = model.max_position_size;
                        objSubscriptionModel.daily_loss_limit = model.daily_loss_limit;
                        objSubscriptionModel.max_drawdown = model.max_drawdown;
                        objSubscriptionModel.profite_target = model.profite_target;
                        objSubscriptionModel.price_per_month = model.price_per_month;
                        objSubscriptionModel.Status = model.Status;
                        lstSubscriptionModel.Add(objSubscriptionModel);
                    }
                    var result = new PagedList<SubscriptionModel>(lstSubscriptionModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Date: 09-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: total Revenue method
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public string totalRevenue()
        {
            try
            {
                var count = dbConnection.user_subscription.Where(x => x.payment_status == 1).Sum(x => x.order_amount);
                return count.ToString();
            }

            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Add/Edit Subscription
        public bool AddEditSubscription(SubscriptionModel objSubscriptionModel)
        {
            try
            {
                bool bFlag = false;
                if (objSubscriptionModel.subscription_id > 0)
                {
                    subscription_master objSubscriptionMaster = dbConnection.subscription_master.Where(x => x.subscription_id == objSubscriptionModel.subscription_id).FirstOrDefault();
                    objSubscriptionMaster.subscription_name = objSubscriptionModel.subscription_name;
                    objSubscriptionMaster.starting_balance = objSubscriptionModel.starting_balance;
                    objSubscriptionMaster.balance_unit = objSubscriptionModel.balance_unit;
                    objSubscriptionMaster.max_position_size = objSubscriptionModel.max_position_size;
                    objSubscriptionMaster.daily_loss_limit = objSubscriptionModel.daily_loss_limit;
                    objSubscriptionMaster.max_drawdown = objSubscriptionModel.max_drawdown;
                    objSubscriptionMaster.profite_target = objSubscriptionModel.profite_target;
                    objSubscriptionMaster.price_per_month = objSubscriptionModel.price_per_month;
                    objSubscriptionMaster.modified_on = objSubscriptionModel.modified_on;
                    objSubscriptionMaster.modified_by = objSubscriptionModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {
                    subscription_master objSubscriptionMaster = new subscription_master();
                    objSubscriptionMaster.subscription_name = objSubscriptionModel.subscription_name;
                    objSubscriptionMaster.starting_balance = objSubscriptionModel.starting_balance;
                    objSubscriptionMaster.balance_unit = objSubscriptionModel.balance_unit;
                    objSubscriptionMaster.max_position_size = objSubscriptionModel.max_position_size;
                    objSubscriptionMaster.daily_loss_limit = objSubscriptionModel.daily_loss_limit;
                    objSubscriptionMaster.max_drawdown = objSubscriptionModel.max_drawdown;
                    objSubscriptionMaster.profite_target = objSubscriptionModel.profite_target;
                    objSubscriptionMaster.price_per_month = objSubscriptionModel.price_per_month;
                    objSubscriptionMaster.is_active = true;//Default False need Email Account Confirmation
                    objSubscriptionMaster.is_deleted = false;
                    objSubscriptionMaster.created_on = objSubscriptionModel.created_on;
                    objSubscriptionMaster.created_by = objSubscriptionModel.created_by;
                    dbConnection.subscription_master.Add(objSubscriptionMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Get Subscription
        public List<subscription_master> GetSubscriptions(List<Int64> iDs)
        {
            try
            {
                List<subscription_master> objSubscriptionMaster = (from UM in dbConnection.subscription_master
                                                                   where iDs.Contains(UM.subscription_id)
                                                                   select UM).ToList();
                return objSubscriptionMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public SubscriptionModel GetSubscription(Int64? id)
        {
            try
            {
                subscription_master SubscriptionDetails = dbConnection.subscription_master.Where(x => x.subscription_id == id).FirstOrDefault();
                if (SubscriptionDetails != null)
                {
                    SubscriptionModel objSubscriptionModel = new SubscriptionModel();
                    objSubscriptionModel = new SubscriptionModel();
                    objSubscriptionModel.subscription_name = SubscriptionDetails.subscription_name;
                    objSubscriptionModel.starting_balance = SubscriptionDetails.starting_balance;
                    objSubscriptionModel.balance_unit = SubscriptionDetails.balance_unit;
                    objSubscriptionModel.max_position_size = SubscriptionDetails.max_position_size;
                    objSubscriptionModel.daily_loss_limit = SubscriptionDetails.daily_loss_limit;
                    objSubscriptionModel.max_drawdown = SubscriptionDetails.max_drawdown;
                    objSubscriptionModel.profite_target = SubscriptionDetails.profite_target;
                    objSubscriptionModel.price_per_month = SubscriptionDetails.price_per_month;
                    objSubscriptionModel.Status = SubscriptionDetails.is_active;
                    return objSubscriptionModel;
                }
                else
                    return null;
                //Linq Query
                //var query =
                //      from UM in dbConnection.user_master
                //        .Where(um => um.user_id == id && um.is_deleted == false).DefaultIfEmpty()
                //      where UM.is_deleted == false

                //      select new
                //      {
                //          UserId = UM.user_id,
                //          FirstName = UM.first_name,
                //          LastName = UM.last_name,
                //          UserName = UM.user_name,
                //          UserEmail = UM.user_email,
                //          Gender = UM.gender,
                //          BirthDate = UM.birth_date,
                //          CityName = UM.city_name,
                //          StateName = UM.state_name,
                //          CountryID = UM.country_id,
                //          Address = UM.address,
                //          PostalCode = UM.postal_code,
                //          ProfileImageUrl = UM.profile_image_url,
                //          CotactNumber = UM.contact_number,
                //          UserStatus = UM.is_active,
                //      };
                //if (query != null)
                //{
                //    UserMasterModel objUserModel = new UserMasterModel();
                //    foreach (var model in query)
                //    {
                //        objUserModel = new UserMasterModel();
                //        objUserModel.user_id = model.UserId;
                //        objUserModel.first_name = model.FirstName;
                //        objUserModel.last_name = model.LastName;
                //        objUserModel.user_name = model.UserName;
                //        objUserModel.user_email = model.UserEmail;
                //        objUserModel.gender = model.Gender;
                //        objUserModel.birth_date = model.BirthDate.Value.ToString("MM-dd-yyyy");
                //        objUserModel.city_name = model.CityName;
                //        objUserModel.state_name = model.StateName;
                //        objUserModel.country_id = model.CountryID.Value;
                //        objUserModel.address = model.Address;
                //        objUserModel.postal_code = model.PostalCode;
                //        objUserModel.profile_image_url = Utilities.GetImagePath + "/Images" + model.ProfileImageUrl;
                //        objUserModel.mobile_number = model.CotactNumber;
                //        objUserModel.user_status = model.UserStatus == true ? "Active" : "Inactive";
                //    }
                //    return objUserModel;
                //}
                //return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<subscription_master> objSubscriptionMaster = (from UM in dbConnection.subscription_master
                                                                   where (lIds).Contains(UM.subscription_id)
                                                                   select UM).AsEnumerable().ToList();
                objSubscriptionMaster.ForEach(x => x.modified_by = LoginUserId);
                objSubscriptionMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objSubscriptionMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delete Subscription
        public bool DeleteSubscriptions(IList<subscription_master> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                List<long> IDs = objUserMasterModelList.Select(x => x.subscription_id).ToList();
                //List<subscription_master> objSubscriptionMaster = (from UM in dbConnection.subscription_master
                //                                           where (objUserMasterModelList.Select(x => x.subscription_id)).Contains(UM.subscription_id)
                //                                   select UM).ToList();
                List<subscription_master> objSubscriptionMaster = dbConnection.subscription_master.Where(y => IDs.Contains(y.subscription_id)).ToList();
                //List<subscription_master> objSubscriptionMaster = dbConnection.subscription_master.Where(y => (objUserMasterModelList.Select(x => x.subscription_id)).Contains(y.subscription_id)).ToList();
                objSubscriptionMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region User Revenue List
        /// <summary>
        /// Date: 08-August-2017
        /// Dev By: Hardik Savaliya
        /// Description: User Revenue List
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<UserSubscriptionModel> UserRevenueList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string start_date = "", string end_date = "")
        {
            try
            {
                IList<UserSubscriptionModel> lstSubscriptionModel = new List<UserSubscriptionModel>();
                DateTime StartDate = new DateTime();
                DateTime EndDate = new DateTime();
                if (!string.IsNullOrEmpty(start_date) && !string.IsNullOrEmpty(end_date))
                {
                    StartDate = Convert.ToDateTime(start_date);
                    EndDate = Convert.ToDateTime(end_date).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                var query =
                        from US in dbConnection.user_subscription
                          .Where(us => us.user_master.first_name.ToLower().Contains(filter)
                          || us.user_master.last_name.ToLower().Contains(filter)
                          || us.transaction_id.ToLower().Contains(filter)
                          || us.subscription_master.subscription_name.ToLower().Contains(filter)
                          )

                          .DefaultIfEmpty()
                        where US.payment_status == 1

                        select new
                        {
                            transaction_id = US.transaction_id,
                            transaction_date = US.purchase_date,
                            transaction_amount = US.order_amount,
                            user_id = US.user_id,
                            promo_amount=US.discount_amount,
                            subscription_id = US.subscription_id,
                            start_date=US.subscription_start_date,
                            end_date = US.subscription_end_date,
                            full_name = US.user_master.first_name + " " + US.user_master.last_name,
                            subcription_name = US.subscription_master.subscription_name,
                            status = US.payment_status,
                            method = US.payment_method
                        };
                if (!string.IsNullOrEmpty(start_date))
                {
                    query = query.Where(((x => x.transaction_date >= StartDate && x.transaction_date <= EndDate)));
                }
                if (query != null)
                {
                    UserSubscriptionModel objSubscriptionModel;
                    foreach (var model in query)
                    {
                        objSubscriptionModel = new UserSubscriptionModel();
                        objSubscriptionModel.subscription_id = model.subscription_id;
                        objSubscriptionModel.transection_id = model.transaction_id;
                        objSubscriptionModel.transection_date_string = model.transaction_date.Value.ToString("dd/MM/yyyy");
                        objSubscriptionModel.transection_date = model.transaction_date;
                        objSubscriptionModel.price_per_month = model.transaction_amount;
                        objSubscriptionModel.subscription_name = model.subcription_name;
                        objSubscriptionModel.user_id = model.user_id;
                        objSubscriptionModel.full_name = model.full_name;
                        objSubscriptionModel.promo_code = model.promo_amount.ToString();
                        objSubscriptionModel.start_date_string = model.start_date.Value.ToString("dd/MM/yyyy");
                        objSubscriptionModel.end_date_string = model.end_date.Value.ToString("dd/MM/yyyy");
                        objSubscriptionModel.start_date = model.start_date;
                        objSubscriptionModel.end_date = model.end_date;
                        objSubscriptionModel.payment_method = model.method;
                        objSubscriptionModel.payment_status = (model.status == null || model.status == (int)PaymentStatus.Paid) ? "PAID" : "UNPAID";
                        lstSubscriptionModel.Add(objSubscriptionModel);
                    }
                    var result = new PagedList<UserSubscriptionModel>(lstSubscriptionModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public List<UserSubscriptionModel> GetUserSubscriptionsList1(long UserID)
        {
            try
            {
                List<UserSubscriptionModel> UserSubscriptionModelList = new List<UserSubscriptionModel>();
                List<user_subscription> objuser_subscriptionList = dbConnection.user_subscription.Where(x => x.user_id == UserID).ToList();
                foreach (var objUserSubscription in objuser_subscriptionList)
                {
                    UserSubscriptionModel entUserSubscriptionModel = new UserSubscriptionModel();
                    entUserSubscriptionModel.user_subscription_id = objUserSubscription.user_subscription_id;
                    entUserSubscriptionModel.transection_id = objUserSubscription.transaction_id;
                    entUserSubscriptionModel.subscription_id = objUserSubscription.subscription_id;
                    entUserSubscriptionModel.price_per_month = objUserSubscription.order_amount.Value;
                    entUserSubscriptionModel.Status_string = objUserSubscription.payment_status == 1 ? "Paid" : "Unpaid";
                    entUserSubscriptionModel.transection_date_string = objUserSubscription.created_date.Value.ToString("dd-MM-yyyy");
                    entUserSubscriptionModel.start_date_string = objUserSubscription.subscription_start_date.Value.ToString("dd-MM-yyyy");
                    entUserSubscriptionModel.end_date_string = objUserSubscription.subscription_end_date.Value.ToString("dd-MM-yyyy");
                    UserSubscriptionModelList.Add(entUserSubscriptionModel);
                }
                return UserSubscriptionModelList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public PagedList<UserSubscriptionModel> GetUserSubscriptionsList(string start_date = "", string end_date = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<UserSubscriptionModel> lstSubscriptionModel = new List<UserSubscriptionModel>();

                DateTime StartDate;
                DateTime EndDate;
                if (!string.IsNullOrEmpty(start_date) && !string.IsNullOrEmpty(end_date))
                {
                    StartDate = Convert.ToDateTime(start_date);
                    EndDate = Convert.ToDateTime(end_date).AddHours(23).AddMinutes(59).AddSeconds(59);
                }
                else
                {
                    StartDate = Convert.ToDateTime("2015-07-15 12:13:39.420");
                    EndDate = DateTime.UtcNow.AddDays(1);
                }
                //Linq Query
                var query =
                        from UM in dbConnection.user_subscription
                          //.Where(um => um.subscription_name.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                          .Where(um => um.purchase_date >= StartDate && um.purchase_date <= EndDate && um.user_id == UserId).DefaultIfEmpty()
                            //.Where(um => um.user_id == UserId).DefaultIfEmpty()
                        where UM.user_id == UserId

                        select new
                        {
                            user_subscription_id = UM.user_subscription_id,
                            transaction_id = UM.transaction_id,
                            subscription_id = UM.subscription_id,
                            order_amount = UM.order_amount,
                            payment_status = UM.payment_status,
                            purchase_date = UM.purchase_date,
                            created_date = UM.created_date,
                            promo_code = dbConnection.promo_code.Where(x => x.promo_code_id == UM.promo_code_id).Select(x => x.promo_code1).FirstOrDefault(),
                            promo_code_amount = dbConnection.promo_code.Where(x => x.promo_code_id == UM.promo_code_id).Select(x => x.reward_amount).FirstOrDefault(),
                            subscription_start_date = UM.subscription_start_date,
                            subscription_end_date = UM.subscription_end_date,
                        };
                query = query.OrderByDescending(x => x.created_date);
                if (query != null)
                {
                    UserSubscriptionModel entUserSubscriptionModel;
                    foreach (var objUserSubscription in query)
                    {
                        entUserSubscriptionModel = new UserSubscriptionModel();
                        entUserSubscriptionModel.user_subscription_id = objUserSubscription.user_subscription_id;
                        entUserSubscriptionModel.transection_id = objUserSubscription.transaction_id;
                        entUserSubscriptionModel.subscription_id = objUserSubscription.subscription_id;
                        entUserSubscriptionModel.price_per_month = objUserSubscription.order_amount.Value;
                        entUserSubscriptionModel.Status_string = objUserSubscription.payment_status == 1 ? "Paid" : "Unpaid";
                        //entUserSubscriptionModel.transection_date_string = objUserSubscription.created_date.Value.ToString("dd-MM-yyyy");
                        entUserSubscriptionModel.transection_date_string = objUserSubscription.purchase_date.Value.ToString("dd-MM-yyyy");
                        entUserSubscriptionModel.start_date_string = objUserSubscription.subscription_start_date.Value.ToString("dd-MM-yyyy");
                        entUserSubscriptionModel.end_date_string = objUserSubscription.subscription_end_date.Value.ToString("dd-MM-yyyy");
                        entUserSubscriptionModel.promo_code = objUserSubscription.promo_code + "(" +objUserSubscription.promo_code_amount.ToString()+")" ;
                        lstSubscriptionModel.Add(entUserSubscriptionModel);
                    }
                    var result = new PagedList<UserSubscriptionModel>(lstSubscriptionModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
