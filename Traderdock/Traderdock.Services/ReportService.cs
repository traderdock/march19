﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class ReportService
    {
        #region Variable
        /// <summary>
        /// Db Connection String
        /// </summary>

        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region User Transaction List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 11/7/2017
        /// Dev By: Bharat Katua
        public List<ReportTransactionModel> GetTTUserTransactionReportList(Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0,
            string saccountallias = "", string statusFilter = "", string UserStatusFilter = "")
        {
            try
            {
                List<ReportTransactionModel> lstReportTransactionModel = new List<ReportTransactionModel>();
                ReportTransactionModel objReportTransactionModel = new ReportTransactionModel();
                List<HomePageModel> lstHomePageModels = new List<HomePageModel>();
                DashboardService objDashboardService = new DashboardService();
                var objData = dbConnection.user_platform.ToList().Where(x => !string.IsNullOrEmpty(x.account_alias) && x.user_id > 0 && x.platform_id > 0).ToList();
                long lSubId = 0;
                if (objData != null)
                {
                    foreach (var item in objData)
                    {
                        lstHomePageModels = objDashboardService.GetTTUserTransactionList(out lSubId, "",
                            Convert.ToInt64(item.user_id), "", 0, item.account_alias);
                        if (lstHomePageModels.Any())
                        {
                            foreach (var subItem in lstHomePageModels)
                            {
                                objReportTransactionModel = new ReportTransactionModel();
                                objReportTransactionModel.userID = subItem.UserID;
                                objReportTransactionModel.FirstName = subItem.FirstName;
                                objReportTransactionModel.LastName = subItem.LastName;
                                objReportTransactionModel.isActive = subItem.isActive;
                                objReportTransactionModel.freeTrial = subItem.freeTrial;
                                objReportTransactionModel.strFreeTrial = subItem.strFreeTrial;
                                objReportTransactionModel.AvgLosingDay = subItem.AvgLosingDay;
                                objReportTransactionModel.AvgLosingDayOld = subItem.AvgLosingDayOld;
                                objReportTransactionModel.AvgLosingDay_str = subItem.AvgLosingDay_str;
                                objReportTransactionModel.AvgLosingTrade = subItem.AvgLosingTrade;
                                objReportTransactionModel.AvgLosingTradeOld = subItem.AvgLosingTradeOld;
                                objReportTransactionModel.AvgWLDuration = subItem.AvgWLDuration;
                                objReportTransactionModel.AvgWinningDay = subItem.AvgWinningDay;
                                objReportTransactionModel.AvgWinningDayOld = subItem.AvgWinningDayOld;
                                objReportTransactionModel.AvgWinningDay_str = subItem.AvgWinningDay_str;
                                objReportTransactionModel.AvgWinningTrade = subItem.AvgWinningTrade;
                                objReportTransactionModel.AvgWinningTradeOld = subItem.AvgWinningTradeOld;
                                objReportTransactionModel.AvgeWLDurationColor = subItem.AvgeWLDurationColor;
                                objReportTransactionModel.BestDay = subItem.BestDay;
                                objReportTransactionModel.BestDayOld = subItem.BestDayOld;
                                objReportTransactionModel.CurrentBalance = subItem.CurrentBalance;
                                objReportTransactionModel.CurrentBalanceOld = subItem.CurrentBalanceOld;
                                objReportTransactionModel.DailyLossLimit = subItem.DailyLossLimit;
                                objReportTransactionModel.DailyLossLimitOld = subItem.DailyLossLimitOld;
                                objReportTransactionModel.Exchange = subItem.Exchange;
                                objReportTransactionModel.GrossProfitLoss = subItem.GrossProfitLoss;
                                objReportTransactionModel.GrossProfitLossOld = subItem.GrossProfitLossOld;
                                objReportTransactionModel.HighBalance = subItem.HighBalance;
                                objReportTransactionModel.HighBalanceOld = subItem.HighBalanceOld;
                                objReportTransactionModel.HighProfitLoss = subItem.HighProfitLoss;
                                objReportTransactionModel.HighProfitLossOld = subItem.HighProfitLossOld;
                                objReportTransactionModel.LosingTradePer = subItem.LosingTradePer;
                                objReportTransactionModel.LosingTradePerOld = subItem.LosingTradePerOld;
                                objReportTransactionModel.LowBalance = subItem.LowBalance;
                                objReportTransactionModel.LowBalanceOld = subItem.LowBalanceOld;
                                objReportTransactionModel.LowProfitLoss = subItem.LowProfitLoss;
                                objReportTransactionModel.LowProfitLossOld = subItem.LowProfitLossOld;
                                objReportTransactionModel.MainAccountBalance = subItem.MainAccountBalance;
                                objReportTransactionModel.MainAccountBalanceOld = subItem.MainAccountBalanceOld;
                                objReportTransactionModel.MaxConsLoss = subItem.MaxConsLoss;
                                objReportTransactionModel.MaxConsWin = subItem.MaxConsWin;
                                objReportTransactionModel.MaxDrowdown = subItem.MaxDrowdown;
                                objReportTransactionModel.MaxDrowdownOld = subItem.MaxDrowdownOld;
                                objReportTransactionModel.NetProfitLoss = subItem.NetProfitLoss;
                                objReportTransactionModel.NetProfitLossOld = subItem.NetProfitLossOld;
                                objReportTransactionModel.ProfitTarget = subItem.ProfitTarget;
                                objReportTransactionModel.ProfitTargetOld = subItem.ProfitTargetOld;
                                objReportTransactionModel.TotalCommisions = subItem.TotalCommisions;
                                objReportTransactionModel.TotalCommisionsOld = subItem.TotalCommisionsOld;
                                objReportTransactionModel.TotalContracts = subItem.TotalContracts;
                                objReportTransactionModel.TotalTrades = subItem.TotalTrades;
                                objReportTransactionModel.TradeRequiredNoOfDays = subItem.TradeRequiredNoOfDays;
                                objReportTransactionModel.TransactionDateString = subItem.TransactionDateString;
                                objReportTransactionModel.UserSubscriptionID = subItem.UserSubscriptionID;
                                objReportTransactionModel.WinningTrade = subItem.WinningTrade;
                                objReportTransactionModel.WinningTradeOld = subItem.WinningTradeOld;
                                objReportTransactionModel.WinningTradePer = subItem.WinningTradePer;
                                objReportTransactionModel.WinningTradePerOld = subItem.WinningTradePerOld;
                                objReportTransactionModel.WorstDay = subItem.WorstDay;
                                objReportTransactionModel.WorstDayOld = subItem.WorstDayOld;
                                objReportTransactionModel.account_number = subItem.account_number;
                                lstReportTransactionModel.Add(objReportTransactionModel);
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(saccountallias))
                {
                    lstReportTransactionModel = lstReportTransactionModel.Where(x => (!string.IsNullOrEmpty(x.FirstName) ? x.FirstName.ToLower().Trim().Contains(saccountallias) : x.FirstName != x.FirstName)
                                                                                || (!string.IsNullOrEmpty(x.LastName) ? x.LastName.ToLower().Trim().Contains(saccountallias) : x.LastName != x.LastName)
                                                                                || (!string.IsNullOrEmpty(x.account_number) ? x.account_number.ToLower().Trim().Contains(saccountallias) : x.account_number != x.account_number)).ToList();

                }
                if (!string.IsNullOrEmpty(statusFilter))
                {
                    if (statusFilter.Trim().ToLower().Equals("yes"))
                    {
                        lstReportTransactionModel = lstReportTransactionModel.Where(x => x.isActive == true).ToList();
                    }
                    else if (statusFilter.Trim().ToLower().Equals("no"))
                    {
                        lstReportTransactionModel = lstReportTransactionModel.Where(x => x.isActive == false || x.isActive == null).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(UserStatusFilter))
                {
                    if (UserStatusFilter.Trim().ToLower().Equals("yes"))
                    {
                        lstReportTransactionModel = lstReportTransactionModel.Where(x => x.freeTrial == true).ToList();
                    }
                    else if (UserStatusFilter.Trim().ToLower().Equals("no"))
                    {
                        lstReportTransactionModel = lstReportTransactionModel.Where(x => x.freeTrial == false).ToList();
                    }
                }
                return lstReportTransactionModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region User Overall List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 12/Dec/2018
        /// Dev By: Hardik Savaliya
        public List<OverallUserReportModel> GetTTUserOverallReportList(Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0,
            string saccountallias = "", string strSearch = "", string statusFilter = "", string UserStatusFilter = "")
        {
            try
            {
                List<OverallUserReportModel> lstOveallUserReportModel = new List<OverallUserReportModel>();
                OverallUserReportModel objOveallUserReportModel = new OverallUserReportModel();
                List<HomePageModel> lstHomePageModels = new List<HomePageModel>();
                DashboardService objDashboardService = new DashboardService();
                var objData = dbConnection.user_master.Where(x => x.user_type_id == (Int32)EnumUserType.Normal && x.trading_platform == (Int32)EnumTradingPlatform.TradingTechnologies && x.is_deleted != true).OrderByDescending(x => x.user_id).ToList();
                long lSubId = 0;
                if (objData != null)
                {
                    foreach (var item in objData)
                    {
                        HomePageModel objHomePageModel = new HomePageModel();
                        AccountInfo objAccountInfoModel = new AccountInfo();
                        RuleModel objRuleModel = new RuleModel();
                        if (item.trading_platform == (Int32)EnumTradingPlatform.Rithmic)
                        {
                            objHomePageModel = objDashboardService.GetUserAccountBalance(item.user_id, "", 0);
                            objAccountInfoModel = objDashboardService.GetAccountInfoNew(item.user_id, "", userSubscriptionID);
                            objRuleModel = objDashboardService.GetRule(userID, "", 0);
                            lstHomePageModels = objDashboardService.GetUserTransactionList(out lSubId, "",
                            Convert.ToInt64(item.user_id), "", 0);
                        }
                        else
                        {
                            objHomePageModel = objDashboardService.TTGetUserAccountBalance(item.user_id, "", 0);
                            objAccountInfoModel = objDashboardService.GetAccountInfo(item.user_id, "", userSubscriptionID);
                            objRuleModel = objDashboardService.TTGetRule(item.user_id, "", 0, item.account_number);
                            lstHomePageModels = objDashboardService.GetTTUserTransactionList(out lSubId, "", item.user_id, Exchange, 0, item.account_number);
                        }
                        objOveallUserReportModel = new OverallUserReportModel();
                        objOveallUserReportModel.userID = item.user_id;
                        objOveallUserReportModel.FirstName = objHomePageModel.FirstName;
                        objOveallUserReportModel.LastName = objHomePageModel.LastName;
                        objOveallUserReportModel.IsActive = objAccountInfoModel.is_current;
                        objOveallUserReportModel.IsFreeTrial = objAccountInfoModel.is_free_trial;
                        objOveallUserReportModel.Country = objHomePageModel.Country;
                        objOveallUserReportModel.account_number = objHomePageModel.account_number;
                        objOveallUserReportModel.UserEmail = objHomePageModel.UserEmail;
                        objOveallUserReportModel.StartDate = objAccountInfoModel.substartdate;
                        objOveallUserReportModel.EndDate = objAccountInfoModel.subenddate;
                        if (item.tt_fill_master_manual.Any())
                        {
                            objOveallUserReportModel.LastUploadDate = item.tt_fill_master_manual.LastOrDefault().created_on;
                            objOveallUserReportModel.LastUploadDateStr = item.tt_fill_master_manual.LastOrDefault().created_on.ToString();
                        }
                        if (lstHomePageModels.Any())
                        {
                            objOveallUserReportModel.TotalContracts = lstHomePageModels.Where(x => x.TransactionDateString != "Overall").LastOrDefault().TotalContracts;
                            objOveallUserReportModel.TotalTrades = lstHomePageModels.Where(x => x.TransactionDateString != "Overall").LastOrDefault().TotalTrades;
                            objOveallUserReportModel.GrossProfitLossOld = lstHomePageModels.Where(x => x.TransactionDateString != "Overall").LastOrDefault().GrossProfitLossOld;
                        }
                        objOveallUserReportModel.TradeCompletedNoOfDays = objRuleModel.TradeRequiredNoOfDays;
                        objOveallUserReportModel.CurrentBalanceOld = objHomePageModel.CurrentBalanceOld;
                        objOveallUserReportModel.MaxDrawDownOld = objHomePageModel.MaxDrowdownOld;
                        objOveallUserReportModel.DailyLossOld = objHomePageModel.DailyLossLimitOld;
                        objOveallUserReportModel.BestDayOld = objHomePageModel.BestDayOld;
                        objOveallUserReportModel.AvgWinningDayOld = objHomePageModel.AvgWinningDayOld;
                        objOveallUserReportModel.DiscountedBalOld = objHomePageModel.CurrentBalanceOld + objHomePageModel.AvgWinningDayOld - objHomePageModel.BestDayOld;
                        objOveallUserReportModel.HighProfitLossOld = objHomePageModel.HighBalanceOld;
                        objOveallUserReportModel.LowProfitLossOld = objHomePageModel.LowProfitLossOld;
                        objOveallUserReportModel.AvgLosingDayOld = objHomePageModel.AvgLosingDayOld;
                        objOveallUserReportModel.WorstDayOld = objHomePageModel.WorstDayOld;
                        objOveallUserReportModel.UserSubscriptionID = objAccountInfoModel.user_subscription_id;
                        lstOveallUserReportModel.Add(objOveallUserReportModel);
                    }
                }
                if (!string.IsNullOrEmpty(strSearch))
                {
                    lstOveallUserReportModel = lstOveallUserReportModel.Where(x => (!string.IsNullOrEmpty(x.FirstName) ? x.FirstName.ToLower().Trim().Contains(strSearch) : x.FirstName != x.FirstName)
                                                                                || (!string.IsNullOrEmpty(x.LastName) ? x.LastName.ToLower().Trim().Contains(strSearch) : x.LastName != x.LastName)
                                                                                || (!string.IsNullOrEmpty(x.Country) ? x.Country.ToLower().Trim().Contains(strSearch) : x.Country != x.Country)
                                                                                || (!string.IsNullOrEmpty(x.account_number) ? x.account_number.ToLower().Trim().Contains(strSearch) : x.account_number != x.account_number)
                                                                                || (!string.IsNullOrEmpty(x.UserEmail) ? x.UserEmail.ToLower().Trim().Contains(strSearch) : x.UserEmail != x.UserEmail)).ToList();
                }
                if (!string.IsNullOrEmpty(statusFilter))
                {
                    if (statusFilter.Trim().ToLower().Equals("yes"))
                    {
                        lstOveallUserReportModel = lstOveallUserReportModel.Where(x => x.IsActive == true).ToList();
                    }
                    else if (statusFilter.Trim().ToLower().Equals("no"))
                    {
                        lstOveallUserReportModel = lstOveallUserReportModel.Where(x => x.IsActive == false || x.IsActive == null).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(UserStatusFilter))
                {
                    if (UserStatusFilter.Trim().ToLower().Equals("yes"))
                    {
                        lstOveallUserReportModel = lstOveallUserReportModel.Where(x => x.IsFreeTrial == true).ToList();
                    }
                    else if (UserStatusFilter.Trim().ToLower().Equals("no"))
                    {
                        lstOveallUserReportModel = lstOveallUserReportModel.Where(x => x.IsFreeTrial == false).ToList();
                    }
                }
                return lstOveallUserReportModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region User Overall List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 12/Dec/2018
        /// Dev By: Hardik Savaliya
        public List<OverallUserReportModel> GetDiscountedOverallReportList(Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0,
            string saccountallias = "", string strSearch = "", string statusFilter = "", string UserStatusFilter = "")
        {
            try
            {
                List<OverallUserReportModel> lstOveallUserReportModel = new List<OverallUserReportModel>();
                OverallUserReportModel objOveallUserReportModel = new OverallUserReportModel();
                List<HomePageModel> lstHomePageModels = new List<HomePageModel>();
                DashboardService objDashboardService = new DashboardService();
                var objData = dbConnection.user_master.Where(x => x.user_type_id == (Int32)EnumUserType.Normal && x.trading_platform == (Int32)EnumTradingPlatform.TradingTechnologies && x.is_deleted != true).OrderByDescending(x => x.user_id).ToList();
                if (objData != null)
                {
                    foreach (var item in objData)
                    {
                        HomePageModel objHomePageModel = new HomePageModel();
                        objHomePageModel = objDashboardService.TTGetUserAccountBalance(item.user_id, "", 0);
                        objOveallUserReportModel = new OverallUserReportModel();
                        objOveallUserReportModel.userID = item.user_id;
                        objOveallUserReportModel.DiscountedBalOld = objHomePageModel.CurrentBalanceOld + objHomePageModel.AvgWinningDayOld - objHomePageModel.BestDayOld;
                        lstOveallUserReportModel.Add(objOveallUserReportModel);
                    }
                }
                return lstOveallUserReportModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}
