﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class EmailTemplateService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Subscription List
        /// <summary>
        /// Get Kendo All Email List
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        public PagedList<EmailTemplateModel> EmailTemplatesList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<EmailTemplateModel> lstEmailTemplateModel = new List<EmailTemplateModel>();

                //Linq Query
                var query =
                        from ETM in dbConnection.email_template_master
                          .Where(um => um.name.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where ETM.is_deleted == false
                        orderby ETM.email_template_id descending
                        select new
                        {
                            email_template_id = ETM.email_template_id,
                            name = ETM.name,
                            bodypart = ETM.body_part,
                            subject = ETM.subject,
                            Status = ETM.is_active,
                        };

                if (query != null)
                {
                    EmailTemplateModel objEmailTemplateModel;
                    foreach (var model in query)
                    {
                        objEmailTemplateModel = new EmailTemplateModel();
                        objEmailTemplateModel.Name = model.name;
                        objEmailTemplateModel.BodyPart = model.bodypart;
                        objEmailTemplateModel.EmailTemplateID = model.email_template_id;
                        objEmailTemplateModel.Subject = model.subject;
                        lstEmailTemplateModel.Add(objEmailTemplateModel);
                    }
                    var result = new PagedList<EmailTemplateModel>(lstEmailTemplateModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Save Email Template
        /// <summary>
        /// Edit Email Templates
        /// </summary>
        /// <param name="objEmailModel"></param>
        /// <returns></returns>
        /// Date : 03/7/2017
        /// Dev By: Aakash Prajapati
        public bool SaveEmailTemplate(EmailTemplateModel objEmailModel)
        {
            try
            {
                bool status = false;
                email_template_master email_template_master = dbConnection.email_template_master.Where(e => e.email_template_id == objEmailModel.EmailTemplateID).FirstOrDefault();
                if (email_template_master != null)
                {
                    email_template_master.subject = objEmailModel.Subject;
                    Utilities.WriteAllText(email_template_master.body_part, objEmailModel.Body);
                    dbConnection.SaveChanges();
                    status = true;
                }
                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region Get Email Service
        /// <summary>
        ///Get Email Templates By EmailID
        /// </summary>
        /// <param name="objEmailid"></param>
        /// <returns></returns>
        public EmailTemplateModel GetEmailTemplate(long objEmailid)
        {
            try
            {

                EmailTemplateModel objEmailTemplateModel = new EmailTemplateModel();
                email_template_master objEmailTemplateMaster = dbConnection.email_template_master.Where(e => e.email_template_id == objEmailid).FirstOrDefault();
                if (objEmailTemplateMaster != null)
                {
                    objEmailTemplateModel.EmailTemplateID = objEmailTemplateMaster.email_template_id;
                    objEmailTemplateModel.Name = objEmailTemplateMaster.name;
                    objEmailTemplateModel.Subject = objEmailTemplateMaster.subject;
                    objEmailTemplateModel.BodyPart = objEmailTemplateMaster.body_part;
                }
                return objEmailTemplateModel;
            }
            catch (Exception)
            {

                throw;
            }
        }
        /// <summary>
        /// Date: 05-July-2017  
        /// Dev By: Hardik Savaliya
        /// Description: Get Email Template Subject
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="objEmailfile"></param>
        /// <returns></returns>
        public EmailTemplateModel GetEmailTemplateSubject(string objEmailfile)
        {
            try
            {
                EmailTemplateModel objEmailTemplateModel = new EmailTemplateModel();
                email_template_master objEmailTemplateMaster = dbConnection.email_template_master.Where(e => e.body_part.Contains(objEmailfile)).FirstOrDefault();
                if (objEmailTemplateMaster != null)
                {
                    objEmailTemplateModel.EmailTemplateID = objEmailTemplateMaster.email_template_id;
                    objEmailTemplateModel.Name = objEmailTemplateMaster.name;
                    objEmailTemplateModel.Subject = objEmailTemplateMaster.subject;
                    objEmailTemplateModel.BodyPart = objEmailTemplateMaster.body_part;
                }
                return objEmailTemplateModel;
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
    }
}
