﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class PromoCodeService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Subscription List
        public PagedList<PromoCodeModel> PromocodeList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<PromoCodeModel> lstPromoCodeModel = new List<PromoCodeModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.promo_code
                          .Where(um => um.promo_code1.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where UM.is_deleted == false  //&& UM.subscription_id != 8

                        select new
                        {
                            promo_code_id = UM.promo_code_id,
                            promo_code = UM.promo_code1,
                            promo_code_desc = UM.promo_code_desc,
                            promo_code_from_date = UM.promo_code_from_date,
                            promo_code_to_date = UM.promo_code_to_date,
                            reward_amount = UM.reward_amount,
                            is_expired = UM.is_expired,
                            Status = UM.is_active,
                        };

                if (query != null)
                {
                    PromoCodeModel objPromoCodeModel;
                    foreach (var model in query)
                    {
                        objPromoCodeModel = new PromoCodeModel();
                        objPromoCodeModel.promo_code_id = model.promo_code_id;
                        objPromoCodeModel.promo_code = model.promo_code;
                        objPromoCodeModel.promo_code_desc = model.promo_code_desc;
                        objPromoCodeModel.promo_code_from_date = model.promo_code_from_date.ToString("dd-MM-yyyy"); ;
                        objPromoCodeModel.promo_code_to_date = model.promo_code_to_date.ToString("dd-MM-yyyy"); ;
                        objPromoCodeModel.reward_amount = model.reward_amount;
                        objPromoCodeModel.is_expired = model.is_expired;
                        objPromoCodeModel.Status = model.Status;
                        lstPromoCodeModel.Add(objPromoCodeModel);
                    }
                    var result = new PagedList<PromoCodeModel>(lstPromoCodeModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Promocode list for 
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<PromoCodeModel> PromocodeListForFront(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<PromoCodeModel> lstPromoCodeModel = new List<PromoCodeModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.promo_code
                          .Where(um => um.promo_code1.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()
                        where UM.is_deleted == false

                        select new
                        {
                            promo_code_id = UM.promo_code_id,
                            promo_code = UM.promo_code1,
                            promo_code_desc = UM.promo_code_desc,
                            promo_code_from_date = UM.promo_code_from_date,
                            promo_code_to_date = UM.promo_code_to_date,
                            reward_amount = UM.reward_amount,
                            is_expired = UM.is_expired,
                            Status = UM.is_active,
                        };

                if (query != null)
                {
                    PromoCodeModel objPromoCodeModel;
                    foreach (var model in query)
                    {
                        objPromoCodeModel = new PromoCodeModel();
                        objPromoCodeModel.promo_code_id = model.promo_code_id;
                        objPromoCodeModel.promo_code = model.promo_code;
                        objPromoCodeModel.promo_code_desc = model.promo_code_desc;
                        objPromoCodeModel.promo_code_from_date = model.promo_code_from_date.ToString("dd-MM-yyyy"); ;
                        objPromoCodeModel.promo_code_to_date = model.promo_code_to_date.ToString("dd-MM-yyyy"); ;
                        objPromoCodeModel.reward_amount = model.reward_amount;
                        objPromoCodeModel.is_expired = model.is_expired;
                        objPromoCodeModel.Status = model.Status;
                        lstPromoCodeModel.Add(objPromoCodeModel);
                    }
                    var result = new PagedList<PromoCodeModel>(lstPromoCodeModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion

        #region Add/Edit Promocode
        public bool AddEditPromocode(PromoCodeModel objPromoCodeModel)
        {
            try
            {
                bool bFlag = false;
                if (objPromoCodeModel.promo_code_id > 0)
                {
                    promo_code objSubscriptionMaster = dbConnection.promo_code.Where(x => x.promo_code_id == objPromoCodeModel.promo_code_id).FirstOrDefault();
                    objSubscriptionMaster.promo_code1 = objPromoCodeModel.promo_code;
                    objSubscriptionMaster.promo_code_desc = objPromoCodeModel.promo_code_desc;
                    objSubscriptionMaster.promo_code_from_date = objPromoCodeModel.from_date;
                    objSubscriptionMaster.promo_code_to_date = objPromoCodeModel.to_date;
                    objSubscriptionMaster.reward_amount = objPromoCodeModel.reward_amount;
                    objSubscriptionMaster.is_expired = false;
                    objSubscriptionMaster.is_active = true;
                    objSubscriptionMaster.is_deleted = false;
                    objSubscriptionMaster.modified_on = objPromoCodeModel.modified_on.Value;
                    objSubscriptionMaster.modified_by = objPromoCodeModel.modified_by.Value;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {
                    promo_code objSubscriptionMaster = new promo_code();
                    objSubscriptionMaster.promo_code1 = objPromoCodeModel.promo_code;
                    objSubscriptionMaster.promo_code_desc = objPromoCodeModel.promo_code_desc;
                    objSubscriptionMaster.promo_code_from_date = objPromoCodeModel.from_date;
                    objSubscriptionMaster.promo_code_to_date = objPromoCodeModel.to_date;
                    objSubscriptionMaster.reward_amount = objPromoCodeModel.reward_amount;
                    objSubscriptionMaster.is_expired = false;
                    objSubscriptionMaster.modified_on = objPromoCodeModel.modified_on.Value;
                    objSubscriptionMaster.modified_by = objPromoCodeModel.modified_by.Value;
                    objSubscriptionMaster.is_active = true;//Default False need Email Account Confirmation
                    objSubscriptionMaster.is_deleted = false;
                    objSubscriptionMaster.created_on = objPromoCodeModel.created_on;
                    objSubscriptionMaster.created_by = objPromoCodeModel.created_by;
                    dbConnection.promo_code.Add(objSubscriptionMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        #region Get Subscription
        public List<promo_code> GetPromocode(List<Int64> iDs)
        {
            try
            {
                List<promo_code> objSubscriptionMaster = (from UM in dbConnection.promo_code
                                                          where iDs.Contains(UM.promo_code_id)
                                                          select UM).ToList();
                return objSubscriptionMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public PromoCodeModel GetPromocode(Int64? id)
        {
            try
            {
                promo_code SubscriptionDetails = dbConnection.promo_code.Where(x => x.promo_code_id == id).FirstOrDefault();
                if (SubscriptionDetails != null)
                {
                    PromoCodeModel objPromoCodeModel = new PromoCodeModel();
                    objPromoCodeModel.promo_code_id = SubscriptionDetails.promo_code_id;
                    objPromoCodeModel.promo_code = SubscriptionDetails.promo_code1;
                    objPromoCodeModel.promo_code_desc = SubscriptionDetails.promo_code_desc;
                    objPromoCodeModel.promo_code_from_date = SubscriptionDetails.promo_code_from_date.ToString("dd-MM-yyyy"); ;
                    objPromoCodeModel.promo_code_to_date = SubscriptionDetails.promo_code_to_date.ToString("dd-MM-yyyy"); ;
                    objPromoCodeModel.reward_amount = SubscriptionDetails.reward_amount;
                    objPromoCodeModel.is_expired = SubscriptionDetails.is_expired;
                    objPromoCodeModel.Status = SubscriptionDetails.is_active;
                    return objPromoCodeModel;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<promo_code> objSubscriptionMaster = (from UM in dbConnection.promo_code
                                                          where (lIds).Contains(UM.promo_code_id)
                                                          select UM).AsEnumerable().ToList();
                objSubscriptionMaster.ForEach(x => x.modified_by = LoginUserId);
                objSubscriptionMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objSubscriptionMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delete Promocode
        public bool DeletePromocode(IList<promo_code> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                List<int> IDs = objUserMasterModelList.Select(x => x.promo_code_id).ToList();

                List<promo_code> objSubscriptionMaster = dbConnection.promo_code.Where(y => IDs.Contains(y.promo_code_id)).ToList();

                objSubscriptionMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public bool CheckPromocode(string promo_code, int id)
        {
            try
            {
                promo_code opromo_code = new promo_code();
                if (id > 0)
                {
                    opromo_code = dbConnection.promo_code.Where(x => x.promo_code1 == promo_code && x.is_deleted != true && x.promo_code_id == id).FirstOrDefault();
                }
                else
                {
                    opromo_code = dbConnection.promo_code.Where(x => x.promo_code1 == promo_code && x.is_deleted != true).FirstOrDefault();
                }
                bool bFlag = (opromo_code != null) ? true : false;
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public PromoCodeModel GetPromocode(string promo_code)
        {
            try
            {
                PromoCodeModel objPromoCodeModel = new PromoCodeModel();
                promo_code opromo_code = new promo_code();
                opromo_code = dbConnection.promo_code.Where(x => x.promo_code1 == promo_code && x.is_deleted != true && x.promo_code_from_date <= DateTime.UtcNow && x.promo_code_to_date >= DateTime.UtcNow).FirstOrDefault();
                if (opromo_code != null)
                {
                    objPromoCodeModel.promo_code_id = opromo_code.promo_code_id;
                    objPromoCodeModel.promo_code = opromo_code.promo_code1;
                    objPromoCodeModel.promo_code_desc = opromo_code.promo_code_desc;
                    objPromoCodeModel.promo_code_from_date = opromo_code.promo_code_from_date.ToString();
                    objPromoCodeModel.promo_code_to_date = opromo_code.promo_code_to_date.ToString();
                    objPromoCodeModel.reward_amount = opromo_code.reward_amount;
                    objPromoCodeModel.is_expired = opromo_code.is_expired;
                }
                return objPromoCodeModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
    }
}
