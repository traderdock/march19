﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class RuleService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get Rule
        /// <summary>
        /// Get rule Rule Service for get an rule with detail description by ID
        /// </summary>
        /// <param name="objRuleModel"></param>
        /// Date : 12/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public RuleModel GetRule()
        {
            try
            {
                rule_master RuleDetails = dbConnection.rule_master.FirstOrDefault();
                if (RuleDetails != null)
                {
                    RuleModel objRuleModel = new RuleModel();
                    objRuleModel.rule_id = RuleDetails.rule_id;
                    objRuleModel.profit_target = RuleDetails.profit_target == null ? 0 : RuleDetails.profit_target;
                    objRuleModel.daily_loss_limit = RuleDetails.daily_loss_limit == null ? 0 : RuleDetails.daily_loss_limit;
                    objRuleModel.max_drowdown = RuleDetails.max_drowdown == null ? 0 : RuleDetails.max_drowdown;
                    objRuleModel.maintain_acacount_balance = RuleDetails.maintain_account_balance == null ? 0 : RuleDetails.maintain_account_balance;
                    objRuleModel.trade_required_no_of_days = RuleDetails.trade_required_no_of_days == null ? 0 : RuleDetails.trade_required_no_of_days;
                    
                    return objRuleModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Add/Edit Rule
        /// <summary>
        /// Add Edit Rule Service for add & edit rule
        /// </summary>
        /// <param name="objRuleModel"></param>
        /// Date : 12/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public bool AddEditRule(RuleModel objRuleModel)
        {
            try
            {
                bool bFlag = false;
                if (objRuleModel.rule_id > 0)
                {
                    rule_master objRuleMaster = dbConnection.rule_master.Where(x => x.rule_id == objRuleModel.rule_id).FirstOrDefault();
                    objRuleMaster.profit_target = objRuleModel.profit_target;
                    objRuleMaster.daily_loss_limit = objRuleModel.daily_loss_limit;
                    objRuleMaster.max_drowdown = objRuleModel.max_drowdown;
                    objRuleMaster.maintain_account_balance = objRuleModel.maintain_acacount_balance;
                    objRuleMaster.trade_required_no_of_days= objRuleModel.trade_required_no_of_days;
                    objRuleMaster.updated_date= objRuleModel.updated_date;
                    objRuleMaster.updated_by = objRuleModel.updated_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {
                    rule_master objRuleMaster = new rule_master();
                    objRuleMaster.profit_target = objRuleModel.profit_target;
                    objRuleMaster.daily_loss_limit = objRuleModel.daily_loss_limit;
                    objRuleMaster.max_drowdown= objRuleModel.max_drowdown;
                    objRuleMaster.maintain_account_balance = objRuleModel.maintain_acacount_balance;
                    objRuleMaster.trade_required_no_of_days = objRuleModel.trade_required_no_of_days;
                    objRuleMaster.created_by= objRuleModel.created_by;
                    objRuleMaster.updated_by = objRuleModel.updated_by;
                    objRuleMaster.created_date = objRuleModel.created_on;
                    objRuleMaster.updated_date = objRuleModel.updated_date;
                    dbConnection.rule_master.Add(objRuleMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
    }
}
