﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Model.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class EnquiryService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Enquiry List
        /// <summary>
        /// Date: 13-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: Enquiry List
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public PagedList<EnquiryModel> EnquiryList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<EnquiryModel> lstEnquiryModellist = new List<EnquiryModel>();

                //Linq Query
                IList<enquiry> lstEnquiry = new List<enquiry>();
              
                var query =
                        from UM in dbConnection.enquiries
                        where UM.email.Contains(filter)
                        orderby UM.enquiry_id descending
                        select new
                        {
                            email = UM.email,
                            datetime = UM.enquiry_datetime
                        };


                if (query != null)
                {
                    EnquiryModel objEnquiryModel;
                    foreach (var model in query)
                    {
                        objEnquiryModel = new EnquiryModel();
                        objEnquiryModel.email = model.email;
                        objEnquiryModel.enquiry_datetime = model.datetime;
                        objEnquiryModel.enquiry_datetime_string = model.datetime != null ? model.datetime.Value.ToString("dd-MM-yyyy") : "";
                        lstEnquiryModellist.Add(objEnquiryModel);
                    }
                    var result = new PagedList<EnquiryModel>(lstEnquiryModellist, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ResetRequest(int id)
        {
            var objData = dbConnection.user_subscription.Where(x => x.user_id == id && x.is_current == true).FirstOrDefault();
            if (objData != null)
            {
                objData.is_current = false;
                dbConnection.SaveChanges();
                objData.is_current = true;
                dbConnection.user_subscription.Add(objData);
                var objResetData = dbConnection.reset_requests.Where(x => x.user_id == objData.user_id && x.reset_status == false).FirstOrDefault();
                objResetData.resetted_date = System.DateTime.UtcNow;
                objResetData.reset_status = true;
                dbConnection.SaveChanges();
            }
            return true;
        }
        #endregion
    }
}
