﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class NotificationService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Notification List
        /// <summary>
        /// Notification List Kendo Grid
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public PagedList<NotificationMasterModel> NotificationList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<NotificationMasterModel> lstNotificationMasterModel = new List<NotificationMasterModel>();

                var query =
                        from NM in dbConnection.notification_master
                          .Where(um => um.notification_message.Contains(filter))
                        orderby NM.notification_id descending
                        select new
                        {
                            notification_id = NM.notification_id,
                            notification_message = NM.notification_message,
                            sent_date = NM.sent_date,
                        };
                if (query != null)
                {
                    NotificationMasterModel objNotificationMasterModel;
                    foreach (var model in query)
                    {
                        objNotificationMasterModel = new NotificationMasterModel();
                        objNotificationMasterModel.notification_id = model.notification_id;
                        objNotificationMasterModel.notification_message = model.notification_message;
                        //objNotificationMasterModel.sent_date_string = model.sent_date.Value.ToString("dd-MM-yyyy");
                        objNotificationMasterModel.sent_date_string = model.sent_date.ToString("dd-MM-yyyy");
                        lstNotificationMasterModel.Add(objNotificationMasterModel);
                    }
                    var result = new PagedList<NotificationMasterModel>(lstNotificationMasterModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Send Email Notification
        /// <summary>
        /// Send Email Notifiacation
        /// </summary>
        /// <param name="objNotificationMasterModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool AddNotification(NotificationMasterModel objNotificationMasterModel)
        {
            try
            {
                bool bFlag = false;
                notification_master objnotification_master = new notification_master();
                objnotification_master.notification_message = objNotificationMasterModel.notification_message;
                objnotification_master.sent_date = DateTime.UtcNow;
                dbConnection.notification_master.Add(objnotification_master);
                dbConnection.SaveChanges();
                long New_notification_ID = objnotification_master.notification_id;

                IList<UserMasterModel> lstUserModel = new List<UserMasterModel>();

                //added by shoaib to get setting values from db - 30June2017                
                List<SettingModel> lstSettingModel = new List<SettingModel>();
                lstSettingModel = CommonService.GetSettingValues();

                var query =
                       from UM in dbConnection.user_master
                       where UM.user_type_id != (Int32)EnumUserType.Admin && UM.is_active
                       select new
                       {
                           user_id = UM.user_id,
                           user_email = UM.user_email
                       };

                if (query != null)
                {

                    var result = query.ToList();
                    string EmailIdList = string.Empty;
                    foreach (var UserDetail in result)
                    {
                        user_notifications objuser_notifications = new user_notifications();
                        objuser_notifications.notification_id = New_notification_ID;
                        objuser_notifications.user_id = UserDetail.user_id;
                        objuser_notifications.notification_status = false;
                        if (EmailIdList.Length > 1)
                            EmailIdList = EmailIdList + "," + UserDetail.user_email;
                        else
                            EmailIdList = UserDetail.user_email;
                        objuser_notifications.notification_date = DateTime.UtcNow;
                        dbConnection.user_notifications.Add(objuser_notifications);
                        dbConnection.SaveChanges();
                    }
                    if (EmailIdList.Length > 1)
                    {
                        #region Send Mail
                        MailMessage Msg = new MailMessage();
                        //Msg.From = new MailAddress(ConfigurationManager.AppSettings["EmailFrom"]);
                        Msg.From = new MailAddress(lstSettingModel.Where(x => x.setting_name == "EmailFrom").Select(x => x.setting_value).FirstOrDefault());

                        Msg.To.Add(EmailIdList);
                        Msg.Subject = "Admin Notification";
                        Msg.Body = "<b>Hello</b> User," + "<br/><br/>" + objNotificationMasterModel.notification_message + "<br/><p>Thank you,<br />Traderdock Team.";
                        Msg.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        //smtp.Host = ConfigurationManager.AppSettings["SMTPServer"];
                        smtp.Host = lstSettingModel.Where(x => x.setting_name == "SMTPServer").Select(x => x.setting_value).FirstOrDefault();
                        smtp.Port = 587;
                        //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["EmailFrom"], ConfigurationManager.AppSettings["Password"]);
                        smtp.Credentials = new System.Net.NetworkCredential(lstSettingModel.Where(x => x.setting_name == "EmailFrom").Select(x => x.setting_value).FirstOrDefault(), lstSettingModel.Where(x => x.setting_name == "Password").Select(x => x.setting_value).FirstOrDefault());
                        smtp.EnableSsl = true;
                        smtp.Send(Msg);
                        #endregion
                    }
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region User Notification List
        /// <summary>
        ///  Notification List In DropdownList
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 11/7/2017
        /// Dev By: Bharat Katua
        public List<NotificationMasterModel> GetNotificationList(Int64 userID = 0)
        {
            try
            {
                List<NotificationMasterModel> lstNotificationMasterModel = new List<NotificationMasterModel>();

                var query =
                        from NM in dbConnection.notification_master
                        join UNM in dbConnection.user_notifications on NM.notification_id equals UNM.notification_id
                        join US in dbConnection.user_subscription on UNM.user_id equals US.user_id
                        join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                        where UNM.user_id == userID && UNM.notification_status != true
                        orderby NM.notification_id descending
                        select new
                        {
                            user_notification_id = UNM.user_notification_id,
                            notification_id = NM.notification_id,
                            notification_message = NM.notification_message,
                            sent_date = UNM.notification_date,
                            subscriptionName = SM.subscription_name,
                            subscription_end_date = US.subscription_end_date,
                            subscription_start_date = US.subscription_start_date
                        };
                if (query != null)
                {
                    NotificationMasterModel objNotificationMasterModel;

                    TimeSpan difference2;
                    int daysRemaining = 0;

                    foreach (var model in query)
                    {
                        difference2 = model.subscription_end_date.Value - DateTime.Now;
                        daysRemaining = Convert.ToInt32(difference2.TotalDays);
                        string days = daysRemaining.ToString();

                        objNotificationMasterModel = new NotificationMasterModel();
                        objNotificationMasterModel.user_notification_id = model.user_notification_id;
                        objNotificationMasterModel.notification_id = model.notification_id;
                        if (model.notification_id == 9)  //Your Free Trial account will expire in {2} days.
                        {
                            //string days = "2";
                            objNotificationMasterModel.notification_message = string.Format(model.notification_message, days);
                        }
                        else if (model.notification_id == 10) //Your Free Trial account is expired.
                        {
                            objNotificationMasterModel.notification_message = model.notification_message;
                        }
                        else if (model.notification_id == 11) //Your Subscription for Trading Challenge {0} is will expire in {1} days.
                        {
                            string subscription = model.subscriptionName;
                            //string days = "7";
                            objNotificationMasterModel.notification_message = string.Format(model.notification_message, subscription, days);
                        }
                        else
                        {
                            objNotificationMasterModel.notification_message = model.notification_message;
                        }
                        objNotificationMasterModel.sent_date_string = model.sent_date.ToString("dd/MM, HH:mm tt").Replace("-", "/");
                        lstNotificationMasterModel.Add(objNotificationMasterModel);
                    }
                    return lstNotificationMasterModel;
                }
                return lstNotificationMasterModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Send Notification
        /// <summary>
        /// Send Notification   (used for send an notification to user based on  subsciption start & End date , subsciption type (Trial/Paid))
        /// dev by : Bharat Katua
        /// date : 01/08/2017
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public void sendNotification(int userID = 0)
        {
            user_notifications userNotification = new user_notifications();
            HomePageModel model = new HomePageModel();
            if (userID > 0)
            {
                try
                {
                    //subscription - free or paid ? (is_current)
                    //if free then check expired or not 
                    //2 or 1 day notification
                    //update db record, set it to expired 
                    //paid 

                    var query = (from US in dbConnection.user_subscription
                                     //join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                 where US.user_id == userID & US.is_current == true
                                 select new { US }).FirstOrDefault();

                    if (query != null)
                    { 
                        DateTime subStartDate = query.US.subscription_start_date.Value;
                        DateTime subEndDate = query.US.subscription_end_date.Value;
                        DateTime nowDate = DateTime.UtcNow;
                        long? subscriptionID = query.US.subscription_id;   // SubscriptionID -->8<--bydefault for Trial
                        long? UserSubscriptionId = query.US.user_subscription_id;

                        //subscription expired or not check
                        bool isExpired = false;
                        TimeSpan difference2 = DateTime.Now.Subtract(query.US.subscription_start_date.Value);
                        int completedTotalTays = Convert.ToInt32(difference2.TotalDays);

                        Double days = (query.US.subscription_end_date.Value - query.US.subscription_start_date.Value).TotalDays;
                        int totaldays = Convert.ToInt32(days);

                        if (completedTotalTays >= totaldays)
                        {
                            isExpired = true;
                        }
                        //

                        //TimeSpan difference2 =  DateTime.Now.Subtract(query.US.subscription_end_date.Value);
                        //int daysRemaining = Convert.ToInt32(difference2.TotalDays);
                        TimeSpan difference3 = query.US.subscription_end_date.Value - DateTime.Now;
                        int daysRemaining = Convert.ToInt32(difference3.TotalDays);
                        int twoDays = 9;//--> Your Free Trial account will expire in two days.
                        int expired = 10; //--> Your Free Trial account is expired.

                        //for trial expire check
                        if (subscriptionID == 8)
                            expired = 10;   //Your Free Trial account is expired.
                        else
                            expired = 12;   //Paid Subscription is expired.
                        var notificationExists2 = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == expired).FirstOrDefault();

                        if (notificationExists2 != null)
                        {
                            userNotification = notificationExists2;
                            DateTime notificationDate1 = notificationExists2.notification_date;
                            if (notificationDate1.Date != nowDate.Date)
                            {
                                userNotification.notification_status = false;
                                userNotification.notification_date = DateTime.UtcNow;
                                dbConnection.SaveChanges();
                            }
                        }
                        else if (notificationExists2 == null && isExpired == true)
                        {
                            userNotification = new user_notifications();
                            
                            userNotification.notification_id = expired;   //Your Free Trial account is expired.                            
                            userNotification.user_id = userID;
                            userNotification.notification_status = false;
                            userNotification.notification_date = DateTime.UtcNow;
                            dbConnection.user_notifications.Add(userNotification);
                            dbConnection.SaveChanges();
                        }
                        // Trial Subscription
                        else if (subscriptionID == 8)   // 8  for Trial subscription
                        {
                            //for trial two days check
                            var notificationExists = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == twoDays).FirstOrDefault();
                            if (notificationExists != null)
                            {
                                userNotification = notificationExists;
                                DateTime notificationDate = notificationExists.notification_date;
                                //update existing entry with today's date
                                if (notificationDate.Date != nowDate.Date)
                                {
                                    //if (nowDate.Date > subEndDate.AddDays(-2).Date)
                                    if (daysRemaining < 2)
                                    {
                                        userNotification.notification_status = false;
                                        userNotification.notification_date = DateTime.UtcNow;
                                        dbConnection.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                //new entry - when there is no entry
                                //if (nowDate.Date > subEndDate.AddDays(-2).Date)
                                if (daysRemaining < 2)
                                {
                                    userNotification = new user_notifications();
                                    userNotification.notification_id = 9;   //Your Free Trial account will expire in two days.
                                    userNotification.user_id = userID;
                                    userNotification.notification_status = false;
                                    userNotification.notification_date = DateTime.UtcNow;
                                    dbConnection.user_notifications.Add(userNotification);
                                    dbConnection.SaveChanges();
                                }
                            }
                        }
                        else    //paid subscription
                        {
                            int sevendays = 11; //--> Your Subscription for Trading Challenge #ID is will expire in 7 days.
                            var notificationExists3 = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == sevendays).FirstOrDefault();
                            if (notificationExists3 != null)
                            {
                                userNotification = notificationExists3;
                                DateTime notificationDate = notificationExists3.notification_date;
                                if (notificationDate.Date != nowDate.Date)
                                {
                                    if (daysRemaining < 7)
                                    {
                                        userNotification.notification_status = false;
                                        userNotification.notification_date = DateTime.UtcNow.Date;
                                        dbConnection.SaveChanges();
                                    }
                                }
                            }
                            else
                            {
                                if (daysRemaining < 7)
                                {
                                    userNotification = new user_notifications();
                                    userNotification.notification_id = 11;   //Your Subscription for Trading Challenge #ID is will expire in 7 days.
                                    userNotification.user_id = userID;
                                    userNotification.notification_status = false;
                                    userNotification.notification_date = DateTime.UtcNow.Date;
                                    dbConnection.user_notifications.Add(userNotification);
                                    dbConnection.SaveChanges();
                                }
                            }
                        }
                    }

                    /*if (query != null)
                    {
                        DateTime subStartDate = query.US.subscription_start_date.Value;
                        DateTime subEndDate = query.US.subscription_end_date.Value;
                        DateTime nowDate = DateTime.UtcNow;
                        long? subscriptionID = query.US.subscription_id;   // SubscriptionID -->8<--bydefault for Trial
                        long? UserSubscriptionId = query.US.user_subscription_id;

                        //TimeSpan difference2 =  DateTime.Now.Subtract(query.US.subscription_end_date.Value);
                        //int daysRemaining = Convert.ToInt32(difference2.TotalDays);
                        TimeSpan difference2 = query.US.subscription_end_date.Value - DateTime.Now;
                        int daysRemaining = Convert.ToInt32(difference2.TotalDays);

                        // Trial Subscription
                        if (subscriptionID == 8)   // 8  for Trial subscription
                        {
                            if (query.US.is_current == true && query.US.is_expired == false)
                            {
                                int twoDays = 9;//--> Your Free Trial account will expire in two days.
                                int expired = 10; //--> Your Free Trial account is expired.

                                //for trial two days check
                                var notificationExists = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == twoDays).FirstOrDefault();
                                if (notificationExists != null)
                                {
                                    userNotification = notificationExists;
                                    DateTime notificationDate = notificationExists.notification_date;
                                    //update existing entry with today's date
                                    if (notificationDate.Date != nowDate.Date)
                                    {
                                        //if (nowDate.Date > subEndDate.AddDays(-2).Date)
                                        if (daysRemaining < 2)
                                        {
                                            //userNotification = new user_notifications();
                                            //userNotification.notification_id = 9;   //Your Free Trial account will expire in two days.
                                            //userNotification.user_id = userID;
                                            userNotification.notification_status = false;
                                            userNotification.notification_date = DateTime.UtcNow;
                                            //dbConnection.user_notifications.Add(userNotification);
                                            dbConnection.SaveChanges();
                                        }
                                    }
                                }
                                else
                                {
                                    //new entry - when there is no entry
                                    //if (nowDate.Date > subEndDate.AddDays(-2).Date)
                                    if (daysRemaining < 2)
                                    {
                                        userNotification = new user_notifications();
                                        userNotification.notification_id = 9;   //Your Free Trial account will expire in two days.
                                        userNotification.user_id = userID;
                                        userNotification.notification_status = false;
                                        userNotification.notification_date = DateTime.UtcNow;
                                        dbConnection.user_notifications.Add(userNotification);
                                        dbConnection.SaveChanges();
                                    }
                                }

                                //for trial expire check
                                var notificationExists2 = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == expired).FirstOrDefault();
                                if (notificationExists2 != null)
                                {
                                    userNotification = notificationExists;
                                    DateTime notificationDate1 = notificationExists2.notification_date;
                                    if (notificationDate1.Date != nowDate.Date)
                                    {
                                        //if (nowDate.Date == subEndDate.Date)
                                        //{
                                            //userNotification = new user_notifications();
                                            //userNotification.notification_id = 10;   //Your Free Trial account is expired.
                                            //userNotification.user_id = userID;
                                            userNotification.notification_status = false;
                                            userNotification.notification_date = DateTime.UtcNow;
                                            //dbConnection.user_notifications.Add(userNotification);
                                            dbConnection.SaveChanges();

                                            ////for Update UserSubscription
                                            //var userSubscription = dbConnection.user_subscription.Where(x => x.user_subscription_id == UserSubscriptionId).FirstOrDefault();
                                            //DateTime subsciptionEndDate = userSubscription.subscription_end_date.Value.Date;
                                            //if (subEndDate.Date == subsciptionEndDate.Date)
                                            //{
                                            //    userSubscription.is_current = false;
                                            //    userSubscription.is_expired = true;
                                            //    userSubscription.updated_date = DateTime.UtcNow;
                                            //    dbConnection.SaveChanges();
                                            //}
                                        //}
                                    }
                                }
                                else
                                {
                                    //if (nowDate.Date == subEndDate.Date )
                                    //{
                                        userNotification = new user_notifications();
                                        userNotification.notification_id = 10;   //Your Free Trial account is expired.
                                        userNotification.user_id = userID;
                                        userNotification.notification_status = false;
                                        userNotification.notification_date = DateTime.UtcNow;
                                        dbConnection.user_notifications.Add(userNotification);
                                        dbConnection.SaveChanges();

                                        ////for Update UserSubscription
                                        //var userSubscription = dbConnection.user_subscription.Where(x => x.user_subscription_id == UserSubscriptionId).FirstOrDefault();
                                        //DateTime subsciptionEndDate = userSubscription.subscription_end_date.Value.Date;
                                        //if (subEndDate.Date == subsciptionEndDate.Date)
                                        //{
                                        //    userSubscription.is_current = false;
                                        //    userSubscription.is_expired = true;
                                        //    dbConnection.SaveChanges();
                                        //}
                                    //}
                                }
                            }
                        }
                        else    //paid subscription
                        {
                            int sevendays = 11; //--> Your Subscription for Trading Challenge #ID is will expire in 7 days.
                            var notificationExists3 = dbConnection.user_notifications.Where(x => x.user_id == userID && x.notification_id == sevendays).FirstOrDefault();
                            if (notificationExists3 != null)
                            {
                                userNotification = notificationExists3;
                                DateTime notificationDate = notificationExists3.notification_date;

                                //if (notificationDate.Date != nowDate.Date)
                                //{
                                    //if (nowDate.Date > subEndDate.AddDays(-7).Date)
                                    if (daysRemaining < 7)
                                    {
                                        //userNotification = new user_notifications();
                                        //userNotification.notification_id = 11;   //Your Subscription for Trading Challenge #ID is will expire in 7 days.
                                        //userNotification.user_id = userID;
                                        userNotification.notification_status = false;
                                        userNotification.notification_date = DateTime.UtcNow.Date;
                                        //dbConnection.user_notifications.Add(userNotification);
                                        dbConnection.SaveChanges();
                                    }
                                //}
                            }
                            else
                            {
                                //if (nowDate.Date == subEndDate.AddDays(-7).Date)
                                if (daysRemaining < 7)
                                {
                                    userNotification = new user_notifications();
                                    userNotification.notification_id = 11;   //Your Subscription for Trading Challenge #ID is will expire in 7 days.
                                    userNotification.user_id = userID;
                                    userNotification.notification_status = false;
                                    userNotification.notification_date = DateTime.UtcNow.Date;
                                    dbConnection.user_notifications.Add(userNotification);
                                    dbConnection.SaveChanges();
                                }
                            }
                        }
                    }*/
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            //return model;
        }
        #endregion
    }
}
