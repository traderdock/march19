﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class CMSService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Subscription List
        public PagedList<CMSModel> CMSList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "",  string PageKey = "", string PageSectionKey = "")
        {
            try
            {
                IList<CMSModel> lstCMSModel = new List<CMSModel>();

                //Linq Query
                var queryOld =from CM in dbConnection.cms_master
                            .Where(um => um.cms_title.Contains(filter) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false)).DefaultIfEmpty()                          
                           where CM.is_deleted == false
                        orderby CM.cms_title
                        select new
                        {
                            cms_id = CM.cms_id,
                            cms_title = CM.cms_title,
                            cms_type = CM.cms_type,
                            Status = CM.is_active,
                            cms_page = CM.cms_page,
                            cms_section = CM.cms_section,
                        };

                var query = dbConnection.cms_master.Where(x => x.is_deleted == false && x.cms_title.Contains(filter));

                if (!string.IsNullOrEmpty(statusFilter) && statusFilter.ToLower() == "yes")
                {
                    query = query.Where(x => x.is_active == true);
                }
                else if (!string.IsNullOrEmpty(statusFilter) && statusFilter.ToLower() == "no")
                {
                    query = query.Where(x => x.is_active == false);
                }               

                if (!string.IsNullOrEmpty(PageKey))
                {
                    query = query.Where(um => PageKey ==um.cms_page);
                }
                if (!string.IsNullOrEmpty(PageSectionKey))
                {
                    query = query.Where(um => PageSectionKey == um.cms_section);
                }

                if (query != null)
                {
                    CMSModel objCMSModel;
                    foreach (var model in query)
                    {
                        objCMSModel = new CMSModel();
                        objCMSModel.cms_id = model.cms_id;
                        objCMSModel.cms_title = model.cms_title;
                        objCMSModel.cms_type = model.cms_type;
                        lstCMSModel.Add(objCMSModel);
                    }
                    var result = new PagedList<CMSModel>(lstCMSModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get CMS
        /// <summary>
        /// Get All CMS 
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public List<CMSModel> GetCMSs()
        {
            List<CMSModel> lstCmsModel = new List<CMSModel>();
            List<cms_master> cmsMasterList = dbConnection.cms_master.Where(x => x.is_active).OrderBy(x => x.cms_title).ToList();
            if (cmsMasterList.Count() > 0)
            {
                lstCmsModel = cmsMasterList.Select(Mapper.DynamicMap<CMSModel>).ToList();
            }
            else
            {
                List<string> a = new List<string>();
                a.Add(EnumCMSType.TermsOfService.ToString());
                a.Add(EnumCMSType.PrivacyPolicy.ToString());
                a.Add(EnumCMSType.Help.ToString());
                a.Add(EnumCMSType.Traderdock.ToString());
                a.Add(EnumCMSType.AboutUs.ToString());
                a.Add(EnumCMSType.ContactUs.ToString());
                a.Add(EnumCMSType.SocialSiteLink.ToString());
                a.Add(EnumCMSType.FAQ.ToString());

                for (int i = 0; i < 8; i++)
                {
                    cms_master CMSMaster = new cms_master();
                    CMSMaster.cms_title = a[i];
                    CMSMaster.cms_content = "<p>" + a[i] + "<p>";
                    CMSMaster.cms_type = i + 1;
                    CMSMaster.is_active = true;
                    CMSMaster.is_deleted = false;
                    CMSMaster.modified_by = CMSMaster.created_by = 1;//Convert.ToInt64(Session["UserID"].ToString())
                    CMSMaster.modified_on = CMSMaster.created_on = DateTime.UtcNow;
                    dbConnection.cms_master.Add(CMSMaster);
                    dbConnection.SaveChanges();
                }
                cmsMasterList = dbConnection.cms_master.Where(x => x.is_active).ToList();
                lstCmsModel = cmsMasterList.Select(Mapper.DynamicMap<CMSModel>).ToList();
            }
            return lstCmsModel;
        }

        /// <summary>
        /// Get CMS By CMS ID
        /// </summary>
        /// <param name="lCMSID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public CMSModel GetCMS(long? lCMSID)
        {
            CMSModel objModel = new CMSModel();
            cms_master cmsMaster = dbConnection.cms_master.Where(x => x.cms_id == lCMSID).FirstOrDefault();
            if (cmsMaster != null)
            {
                objModel = new CMSModel();
                objModel.cms_id = cmsMaster.cms_id;
                objModel.cms_title = cmsMaster.cms_title;
                objModel.cms_content = cmsMaster.cms_content;
                objModel.page_id = cmsMaster.cms_page;
                objModel.page_section_id = cmsMaster.cms_section;
            }
            return objModel;
        }

        /// <summary>
        /// Get CMS By TypeID
        /// </summary>
        /// <param name="iCMSType"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public CMSModel GetCMSByType(int iCMSType)
        {
            CMSModel objModel = null;
            cms_master cmsMaster = dbConnection.cms_master.Where(x => x.cms_type == iCMSType && x.is_active).FirstOrDefault();
            if (cmsMaster != null)
            {
                objModel = new CMSModel();
                objModel.cms_content = cmsMaster.cms_content;
                if (iCMSType == 1)
                {
                    objModel.page_name = "Terms And Conditions";
                }
                else if (iCMSType == 2)
                {
                    objModel.page_name = "Privacy Policy";
                }
                else if (iCMSType == 3)
                {
                    objModel.page_name = "Help";
                }
                else if (iCMSType == 5)
                {
                    objModel.page_name = "About Us";
                }
                else if (iCMSType == 6)
                {
                    objModel.page_name = "Contact Us";
                }
                else if (iCMSType == 7)
                {
                    objModel.page_name = "Social Site Link";
                }
                else if (iCMSType == 8)
                {
                    objModel.page_name = "FAQ";
                }
                else
                {
                    objModel.page_name = "Traderdock";
                }
            }
            return objModel;
        }
        #endregion

        #region Save CMS
        /// <summary>
        /// Save CMS Data
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool SaveCMS(CMSModel objModel)
        {
            bool Status = false;
            cms_master cmsMaster = dbConnection.cms_master.Where(x => x.cms_id == objModel.cms_id).FirstOrDefault();
            if (cmsMaster != null)
            {
                cmsMaster.cms_title = objModel.cms_title;
                cmsMaster.cms_content = objModel.cms_content;
                cmsMaster.cms_page = objModel.page_id;
                cmsMaster.cms_section = objModel.page_section_id;
                dbConnection.SaveChanges();
                Status = true;
            }
            return Status;
        }
        #endregion

        #region Home Page All CMS Content
        /// <summary>
        /// Home Page Footer Content while Session Get Null
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public string GetHomeCMSFooter()
        {
            try
            {
                string Footer_content = string.Empty;
                cms_master cmsMaster = dbConnection.cms_master.Where(x => x.cms_type == (Int32)EnumCMSType.WebPanelFooter).FirstOrDefault();
                if (cmsMaster != null)
                {
                    Footer_content = cmsMaster.cms_content;
                }
                return Footer_content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Home Page CMS Details
        /// </summary>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public HomePageModel GetHomeCMSByID()
        {
            try
            {
                HomePageModel objHomePageModel = new HomePageModel();
                List<long> CMSIDs = new List<long>();
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderDock1);
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderDock2);
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderDock3);
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderDock4);
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderDock5);
                CMSIDs.Add((Int32)EnumCMSType.Bannertext);
                CMSIDs.Add((Int32)EnumCMSType.WebPanelFooter);
                CMSIDs.Add((Int32)EnumCMSType.GlobalTeam);
                CMSIDs.Add((Int32)EnumCMSType.HowtotradewithTraderdock);
                CMSIDs.Add((Int32)EnumCMSType.Takethetraderdockchallenge);
                CMSIDs.Add((Int32)EnumCMSType.Showusyouexcel);
                CMSIDs.Add((Int32)EnumCMSType.Beginlivetrading);
                CMSIDs.Add((Int32)EnumCMSType.WhyTraderdock);
                CMSIDs.Add((Int32)EnumCMSType.LayoutFooterText);
                List<cms_master> cmsMasterList = dbConnection.cms_master.Where(x => CMSIDs.Contains(x.cms_type)).ToList();
                if (cmsMasterList.Count > 0)
                {
                    foreach (var CMSDetails in cmsMasterList)
                    {
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderDock1)
                            objHomePageModel.WhyTraderDock1 = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderDock2)
                            objHomePageModel.WhyTraderDock2 = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderDock3)
                            objHomePageModel.WhyTraderDock3 = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderDock4)
                            objHomePageModel.WhyTraderDock4 = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderDock5)
                            objHomePageModel.WhyTraderDock5 = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.Bannertext)
                            objHomePageModel.Bannertext = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WebPanelFooter)
                            objHomePageModel.FooterCMS = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.GlobalTeam)
                            objHomePageModel.GlobalTeam = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.HowtotradewithTraderdock)
                            objHomePageModel.HowtotradewithTraderdock = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.Takethetraderdockchallenge)
                            objHomePageModel.Takethetraderdockchallenge = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.Showusyouexcel)
                            objHomePageModel.Showusyouexcel = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.Beginlivetrading)
                            objHomePageModel.Beginlivetrading = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.WhyTraderdock)
                            objHomePageModel.WhyTraderdock = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.LayoutFooterText)
                            objHomePageModel.LayoutFooterText = CMSDetails.cms_content;
                    }
                }
                List<banner_master> BannerList = dbConnection.banner_master.Where(x => x.banner_type_id == (Int32)EnumBannerType.HomePageBanner).ToList();
                string BaseImageURL = Utilities.GetImagePath() + "/Images/";
                objHomePageModel.BannerImages = new List<string>();
                foreach (var objBanner in BannerList)
                {
                    string ImageUrl = BaseImageURL + objBanner.banner_image;
                    objHomePageModel.BannerImages.Add(ImageUrl);
                }
                return objHomePageModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public FaqCMSModel GetFaqCMSByID(long CMSTypeID)
        {
            try
            {
                FaqCMSModel objModel = new FaqCMSModel();
                List<long> CMSIDs = new List<long>();
                if (CMSTypeID == (Int32)EnumCMSType.FAQSBannerText)
                {
                    CMSIDs.Add((Int32)EnumCMSType.FAQSBannerText);
                }
                else if (CMSTypeID == (Int32)EnumCMSType.FAQForEach)
                {
                    CMSIDs.Add((Int32)EnumCMSType.FAQForEach);
                }
                List<cms_master> cmsMasterList = dbConnection.cms_master.Where(x => CMSIDs.Contains(x.cms_type)).ToList();
                if (cmsMasterList.Count > 0)
                {
                    foreach (var CMSDetails in cmsMasterList)
                    {
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.FAQSBannerText)
                            objModel.FAQSBannerText = CMSDetails.cms_content;
                        if (CMSDetails.cms_type == (Int32)EnumCMSType.FAQForEach)
                        {
                            objModel.FAQForEach = CMSDetails.cms_content;
                        }
                    }
                }
                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Learn More CMS Contant Step wise
        /// <summary>
        /// Learn More CMS Contant Step wise With StepNo
        /// </summary>
        /// <param name="StepNo"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public LearnHowModel GetLearnMore(long StepNo)
        {
            LearnHowModel objLearnHowModel = new LearnHowModel();
            List<long> CMSIDs = new List<long>();
            if (StepNo == 1)
            {
                CMSIDs.Add((Int32)EnumCMSType.Step1_Challenge);
                CMSIDs.Add((Int32)EnumCMSType.Step1_Platform);
                CMSIDs.Add((Int32)EnumCMSType.Trading_ObjectivesAndParameters);//ADDED on 11-09-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnBannerText);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep1Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep2Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep3Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.ProfitTableLearnHowPage);//ADDED on 08-12-2017
            }
            if (StepNo == 2)
            {
                CMSIDs.Add((Int32)EnumCMSType.Step2_ShowExcel);
                //design change on 24-Nov-2017
                CMSIDs.Add((Int32)EnumCMSType.Step1_Platform);
                CMSIDs.Add((Int32)EnumCMSType.Trading_ObjectivesAndParameters);//ADDED on 11-09-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep1Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep2Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep3Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.BannerTextStep2);//ADDED on 11-12-2017
            }
            if (StepNo == 3)
            {
                CMSIDs.Add((Int32)EnumCMSType.Step3_TraderDockCash);
                CMSIDs.Add((Int32)EnumCMSType.Step1_Platform);
                CMSIDs.Add((Int32)EnumCMSType.Trading_ObjectivesAndParameters);
                CMSIDs.Add((Int32)EnumCMSType.LearnStep1Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep2Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.LearnStep3Text);//ADDED on 08-12-2017
                CMSIDs.Add((Int32)EnumCMSType.BannerTextStep3);//ADDED on 11-12-2017
            }
            List<cms_master> cmsMasterList = dbConnection.cms_master.Where(x => CMSIDs.Contains(x.cms_type)).ToList();
            if (cmsMasterList.Count > 0)
            {
                foreach (var CMSDetails in cmsMasterList)
                {
                    if (CMSDetails.cms_type == (Int32)EnumCMSType.Step1_Challenge)
                        objLearnHowModel.Step1_Challenge = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.Step1_Platform)
                        objLearnHowModel.Step1_Platform = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.Trading_ObjectivesAndParameters)
                        objLearnHowModel.Trading_ObjectivesAndParameters = CMSDetails.cms_content;// added on 11-09-2017

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.ProfitTableLearnHowPage)
                        objLearnHowModel.ProfitTableLearnHowPage = CMSDetails.cms_content;// added on 23-Feb-2018

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.LearnBannerText)
                        objLearnHowModel.LearnBannerText = CMSDetails.cms_content;// added on 11-09-2017

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.Step2_ShowExcel)
                        objLearnHowModel.Step2_ShowExcel = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.BannerTextStep2)  // added on 11-12-2017
                        objLearnHowModel.BannerTextStep2 = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.Step3_TraderDockCash)
                        objLearnHowModel.Step3_TraderDockCash = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.LearnStep1Text)
                        objLearnHowModel.LearnStep1Text = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.LearnStep2Text)
                        objLearnHowModel.LearnStep2Text = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.BannerTextStep3)
                        objLearnHowModel.BannerTextStep3 = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.LearnStep3Text)
                        objLearnHowModel.LearnStep3Text = CMSDetails.cms_content;
                }
            }
            return objLearnHowModel;
        }
        #endregion

        #region Get CMS Contant From CMS Type ID
        /// <summary>
        /// Get CMS Contant by providing  CMS Type ID
        /// </summary>
        /// <param name="CMSTypeID"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public CMSDataModel GetCMSFromTypeID(long CMSTypeID)
        {
            CMSDataModel objCMSDataModel = new CMSDataModel();
            List<long> CMSIDs = new List<long>();
            if (CMSTypeID == (Int32)EnumCMSType.AboutUs)
            {
                CMSIDs.Add((Int32)EnumCMSType.AboutUs);
                CMSIDs.Add((Int32)EnumCMSType.AboutUsMision);
                CMSIDs.Add((Int32)EnumCMSType.AboutUsVision);
            }
            if (CMSTypeID == (Int32)EnumCMSType.ContactUs)
            {
                CMSIDs.Add((Int32)EnumCMSType.BannerTextContactUs);
            }
            if (CMSTypeID == (Int32)EnumCMSType.ContactUs)
                CMSIDs.Add((Int32)EnumCMSType.ContactUs);
            if (CMSTypeID == (Int32)EnumCMSType.SatisfactionPolicy)
                CMSIDs.Add((Int32)EnumCMSType.SatisfactionPolicy);
            if (CMSTypeID == (Int32)EnumCMSType.PrivacyPolicy)
                CMSIDs.Add((Int32)EnumCMSType.PrivacyPolicy);
            if (CMSTypeID == (Int32)EnumCMSType.TermsOfService)
                CMSIDs.Add((Int32)EnumCMSType.TermsOfService);
            if (CMSTypeID == (Int32)EnumCMSType.BannerTextContactUs)
                CMSIDs.Add((Int32)EnumCMSType.BannerTextContactUs);
            List<cms_master> cmsMasterList = dbConnection.cms_master.Where(x => CMSIDs.Contains(x.cms_type)).ToList();
            if (cmsMasterList.Count > 0)
            {
                foreach (var CMSDetails in cmsMasterList)
                {
                    if (CMSDetails.cms_type == (Int32)EnumCMSType.AboutUs)
                        objCMSDataModel.cms_about_us = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.AboutUsMision)
                        objCMSDataModel.cms_about_us_mission = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.AboutUsVision)
                        objCMSDataModel.cms_about_us_vision = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.SatisfactionPolicy)
                        objCMSDataModel.cms_satisfaction_policy = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.PrivacyPolicy)
                        objCMSDataModel.cms_privacy_policy = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.ContactUs)
                        objCMSDataModel.cms_contact_us = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.TermsOfService)
                        objCMSDataModel.cms_terms = CMSDetails.cms_content;

                    if (CMSDetails.cms_type == (Int32)EnumCMSType.BannerTextContactUs)
                        objCMSDataModel.BannerTextContactUs = CMSDetails.cms_content;
                }
            }
            if (CMSTypeID == (Int32)EnumCMSType.ContactUs)
            {
                objCMSDataModel.BannerImages = new List<string>();
                List<banner_master> BanerMasterList = dbConnection.banner_master.Where(x => x.banner_type_id == (Int32)EnumBannerType.ContactUsBanner).ToList();
                string BaseImageURL = Utilities.GetImagePath() + "/Images/";
                foreach (var objBanerMaster in BanerMasterList)
                {
                    string ImageUrl = BaseImageURL + objBanerMaster.banner_image;
                    objCMSDataModel.BannerImages.Add(ImageUrl);
                }
            }
            return objCMSDataModel;
        }
        #endregion

        #region Home Page Social Links Content
        /// <summary>
        /// Home Page Social Links Content 
        /// </summary>
        /// <returns></returns>
        /// Date : 11/7/2017
        /// Dev By: Bharat Katua
        public string GetSocialCMS()
        {
            try
            {
                string Footer_content = string.Empty;
                cms_master cmsMaster = dbConnection.cms_master.Where(x => x.cms_type == (Int32)EnumCMSType.SocialSiteData).FirstOrDefault();
                if (cmsMaster != null)
                {
                    Footer_content = cmsMaster.cms_content.Replace("<p>", string.Empty).Replace("</p>", string.Empty);
                }
                return Footer_content;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        public List<TestimonialModel> GetTestimonial()
        {
            List<TestimonialModel> TestimonialList = new List<TestimonialModel>();
            try
            {
                TestimonialList = (from m in dbConnection.testimonial_master
                                   where m.is_deleted == false
                                   select new TestimonialModel
                                   {
                                       testimonial_id = m.testimonial_id,
                                       user_name = m.user_name,
                                       description = m.description,
                                       user_image_url = !string.IsNullOrEmpty(m.user_image_url) ? m.user_image_url : "experience-img1.png",
                                   }).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return TestimonialList;
        }

        public List<GlobalTeamModel> GetGlobalTeam()
        {
            List<GlobalTeamModel> GlobalTeamList = new List<GlobalTeamModel>();
            try
            {
                GlobalTeamList = (from m in dbConnection.global_team
                                   where m.is_deleted == false
                                   select new GlobalTeamModel
                                   {
                                       global_team_id = m.global_team_id,
                                       user_name = m.user_name,
                                       designation = m.designation,
                                       description = m.description,
                                       user_image_url_sm = !string.IsNullOrEmpty(m.user_image_url_sm) ? m.user_image_url_sm : "experience-img1.png",
                                       user_image_url_lg = !string.IsNullOrEmpty(m.user_image_url_lg) ? m.user_image_url_lg : "experience-img1.png",
                                   }).ToList();


            }
            catch (Exception ex)
            {
                throw ex;
            }
            return GlobalTeamList;
        }

    }
}