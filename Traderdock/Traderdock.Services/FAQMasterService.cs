﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class FAQMasterService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region FAQ Master List
        /// <summary>
        /// FAQ MAster List Kendo Grid
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public PagedList<FAQMasterModel> FAQMasterList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<FAQMasterModel> lstFaqMasterModel = new List<FAQMasterModel>();
                //Linq Query
                var query = dbConnection.faq_master.Where(x => x.is_deleted == false).ToList();
                //from FM in dbConnection.faq_master
                //  //join FCM in dbConnection.faq_category_master on FM.faq_category_id==
                //  .Where(fcm => fcm.is_deleted == false && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? fcm.is_active == true || fcm.is_active == false : ((statusFilter == "Yes") ? fcm.is_active == true : fcm.is_active == false))).DefaultIfEmpty()
                //orderby FM.faq_id descending
                //select new
                //{
                //    faq_id = FM.faq_id,
                //    faq_catergory_title = FM.faq_sub_category_master.sub_category_title,
                //    faq_catergory_description = FM.faq_sub_category_master.sub_category_description,
                //    faq_sub_category_id = FM.faq_sub_category_id,
                //    question = FM.question,
                //    answer = FM.answer,
                //    is_active = FM.is_active
                //};
                if (!string.IsNullOrEmpty(statusFilter) || statusFilter == "All")
                {
                    if (statusFilter == "Yes")
                    {
                        query = query.Where(x => x.is_active == true).ToList();
                    }
                    else
                    {
                        query = query.Where(x => x.is_active == false).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    query = query.Where(x => x.question.Contains(filter) || x.answer.Contains(filter)).ToList();
                }
                if (query != null)
                {
                    FAQMasterModel objFaqMasterModel;
                    foreach (var model in query)
                    {
                        objFaqMasterModel = new FAQMasterModel();
                        objFaqMasterModel.faq_id = model.faq_id;
                        objFaqMasterModel.faq_sub_category_id = model.faq_sub_category_id;
                        objFaqMasterModel.faq_category_id = model.faq_category_id;
                        objFaqMasterModel.faq_category_title = model.faq_category_master.category_title;
                        objFaqMasterModel.faq_sub_category_title = model.faq_sub_category_master.sub_category_title;
                        objFaqMasterModel.question = model.question;
                        objFaqMasterModel.answer = model.answer;
                        objFaqMasterModel.is_active = model.is_active;
                        objFaqMasterModel.faqStatus = model.is_active == true ? "Active" : "InActive";
                        lstFaqMasterModel.Add(objFaqMasterModel);
                    }
                    var result = new PagedList<FAQMasterModel>(lstFaqMasterModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Add/Edit Category
        /// <summary>
        /// Add / Edit MAQ Master
        /// </summary>
        /// <param name="objFAQMasterModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool AddEditFAQMaster(FAQMasterModel objFAQMasterModel)
        {
            try
            {
                bool bFlag = false;
                if (objFAQMasterModel.faq_id > 0)
                {
                    #region Update
                    faq_master objFAQMaster = dbConnection.faq_master.Where(x => x.faq_id == objFAQMasterModel.faq_id).FirstOrDefault();
                    objFAQMaster.faq_id = objFAQMasterModel.faq_id;
                    objFAQMaster.faq_sub_category_id = objFAQMasterModel.faq_sub_category_id;
                    objFAQMaster.faq_category_id = objFAQMasterModel.faq_category_id;
                    objFAQMaster.question = objFAQMasterModel.question;
                    objFAQMaster.answer = objFAQMasterModel.answer;
                    objFAQMaster.modified_on = objFAQMasterModel.modified_on;
                    objFAQMaster.modified_by = objFAQMasterModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                    #endregion
                }
                else
                {
                    #region Add New FAQ
                    faq_master objFAQMaster = new faq_master();
                    objFAQMaster.faq_sub_category_id = objFAQMasterModel.faq_sub_category_id;
                    objFAQMaster.question = objFAQMasterModel.question;
                    objFAQMaster.answer = objFAQMasterModel.answer;
                    objFAQMaster.faq_category_id = objFAQMasterModel.faq_category_id;
                    objFAQMaster.is_active = true;
                    objFAQMaster.created_on = objFAQMasterModel.created_on = objFAQMasterModel.created_on;
                    objFAQMaster.created_by = objFAQMasterModel.created_by = objFAQMasterModel.created_by;
                    dbConnection.faq_master.Add(objFAQMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                    #endregion
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get FAQ
        /// <summary>
        /// Get FAQ By FAQ ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public FAQMasterModel GetFAQMaster(Int64? id)
        {
            try
            {
                faq_master objFAQMaster = dbConnection.faq_master.Where(x => x.faq_id == id).FirstOrDefault();
                if (objFAQMaster != null)
                {
                    FAQMasterModel objFaqMasterModel = new FAQMasterModel();
                    objFaqMasterModel = new FAQMasterModel();
                    objFaqMasterModel.faq_id = objFAQMaster.faq_id;
                    objFaqMasterModel.faq_sub_category_id = objFAQMaster.faq_sub_category_id;
                    objFaqMasterModel.faq_category_id = objFAQMaster.faq_category_id;
                    objFaqMasterModel.question = objFAQMaster.question;
                    objFaqMasterModel.answer = objFAQMaster.answer;
                    objFaqMasterModel.faqStatus = objFAQMaster.is_active == true ? "True" : "False";
                    return objFaqMasterModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get List FAQ By List of IDs 
        /// </summary>
        /// <param name="iDs"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public List<FAQMasterModel> GetFAQMasterList(List<Int64> iDs)
        {
            try
            {
                List<FAQMasterModel> lstFAQMasterModel = new List<FAQMasterModel>();
                FAQMasterModel objFAQMasterModel;
                List<faq_master> objFAQMaster = (from FM in dbConnection.faq_master
                                                 where iDs.Contains(FM.faq_id)
                                                 select FM).ToList();
                foreach (var objFaqModel in objFAQMaster)
                {
                    objFAQMasterModel = new FAQMasterModel();
                    objFAQMasterModel.faq_id = objFaqModel.faq_id;
                    objFAQMasterModel.faq_sub_category_id = objFaqModel.faq_sub_category_id;
                    objFAQMasterModel.faq_category_title = objFaqModel.faq_sub_category_master.sub_category_title;
                    objFAQMasterModel.faq_category_description = objFaqModel.faq_sub_category_master.sub_category_description;
                    objFAQMasterModel.question = objFaqModel.question;
                    objFAQMasterModel.answer = objFaqModel.answer;
                    lstFAQMasterModel.Add(objFAQMasterModel);
                }
                return lstFAQMasterModel;
            }
            catch (Exception)
            {
                throw;
            }

        }
        #endregion

        #region Delete FAQ
        /// <summary>
        /// Delete Selected FAQ KEndo Grid
        /// </summary>
        /// <param name="objUserMasterModelList"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool DeleteFAQs(IList<FAQMasterModel> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                var Ids = objUserMasterModelList.Select(x => x.faq_id).ToList();
                List<faq_master> objFAQMaster = (from FM in dbConnection.faq_master
                                                 where (Ids).Contains(FM.faq_id)
                                                 select FM).ToList();
                objFAQMaster.ForEach(x => x.modified_by = objUserMasterModelList.Select(y => y.modified_by).FirstOrDefault());
                objFAQMaster.ForEach(x => x.modified_on = System.DateTime.UtcNow);
                objFAQMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FAQ Active/Inactive
        /// <summary>
        /// FAQ Active And Inactive From Kendo Grid
        /// </summary>
        /// <param name="lIds"></param>
        /// <param name="LoginUserId"></param>
        /// <param name="IsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;
                List<faq_master> objFAQMaster = (from FM in dbConnection.faq_master
                                                 where (lIds).Contains(FM.faq_id)
                                                 select FM).AsEnumerable().ToList();
                objFAQMaster.ForEach(x => x.modified_by = LoginUserId);
                objFAQMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objFAQMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region updateOrderFAQ
        /// <summary>
        /// update Order FAQ
        /// </summary>
        /// <param name="ifaqid"></param>
        /// <param name="isequence"></param>
        /// <returns></returns>
        public bool UpdateFAQ(string[] arrCategoryID)
        {
            List<faq_master> lstFAQMaster = dbConnection.faq_master.ToList();
            if (lstFAQMaster.Count() > 0)
            {
                int i = 0;
                foreach (var item in arrCategoryID)
                {
                    lstFAQMaster.Where(x => x.faq_id == Convert.ToInt64(item)).ToList().ForEach(x => x.order_sequence = i);
                    i++;
                }
                dbConnection.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion
    }
}
