﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Entity.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class TTSettingsService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Get Setting
        /// <summary>
        /// Settings List
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<SettingModel> SettingsList(string filter = "", int pageIndex = 1, int pageSize = 1, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                bool Status = false;
                Status = CheckSettingExist();
                if (Status)
                {
                    IList<SettingModel> lstSettingModel = new List<SettingModel>();
                    //Linq Query
                    var query =
                            from S in dbConnection.tt_user_settings
                            select new
                            {
                                SettingsId = S.tt_settings_id,
                                SettingName = S.tt_setting_name,
                                SettingValue = S.tt_setting_value,
                                defaultlabel = S.tt_default_label,
                                SettingInfo=S.tt_default_info
                            };

                    if (query != null)
                    {
                        SettingModel objSettingModel;
                        foreach (var model in query)
                        {
                            objSettingModel = new SettingModel();
                            objSettingModel.setting_id = model.SettingsId;
                            objSettingModel.setting_name = model.SettingName;
                            objSettingModel.setting_value = model.SettingValue;
                            objSettingModel.setting_info = model.SettingInfo;
                            objSettingModel.default_label = model.defaultlabel;
                            lstSettingModel.Add(objSettingModel);
                        }
                        var result = new PagedList<SettingModel>(lstSettingModel, pageIndex, pageSize);
                        return result;
                    }
                    return null;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Settings
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SettingModel GetSetting(Int64? id)
        {
            try
            {
                tt_user_settings objSettings = dbConnection.tt_user_settings.Where(x => x.tt_settings_id == id).FirstOrDefault();
                if (objSettings != null)
                {
                    SettingModel objSettingsModel = new SettingModel();
                    objSettingsModel.setting_id = objSettings.tt_settings_id;
                    objSettingsModel.setting_name = objSettings.tt_setting_name;
                    objSettingsModel.setting_value = objSettings.tt_setting_value;
                    objSettingsModel.default_label = objSettings.tt_default_label;
                    return objSettingsModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Get Setting By Name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SettingModel GetSettingByName(string sValue)
        {
            try
            {
                setting objSettings = dbConnection.settings.Where(x => x.setting_name == sValue).FirstOrDefault();
                if (objSettings != null)
                {
                    SettingModel objSettingsModel = new SettingModel();
                    objSettingsModel.setting_id = objSettings.settings_id;
                    objSettingsModel.setting_name = objSettings.setting_name;
                    objSettingsModel.setting_value = objSettings.setting_value;
                    objSettingsModel.default_label = objSettings.default_label;
                    return objSettingsModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// GetSetting Values
        /// </summary>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public List<SettingModel> GetSettingList()
        {
            try
            {
                List<SettingModel> lstSettingModel = new List<SettingModel>();
                List<setting> lstSettings = dbConnection.settings.ToList();
                if (lstSettings != null)
                {
                    foreach (var item in lstSettings)
                    {
                        SettingModel objSettingsModel = new SettingModel();
                        objSettingsModel.setting_id = item.settings_id;
                        objSettingsModel.setting_name = item.setting_name;
                        objSettingsModel.setting_value = item.setting_value;
                        objSettingsModel.default_label = item.default_label;
                        lstSettingModel.Add(objSettingsModel);
                    }
                    return lstSettingModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Add Edit Settingss
        /// <summary>
        /// Add Edit Settings
        /// </summary>
        /// <param name="objSettingModel"></param>
        /// <returns></returns>
        public bool AddEditSettings(SettingModel objSettingModel)
        {
            try
            {
                bool bFlag = false;
                if (objSettingModel.setting_id > 0)
                {
                    setting objSetting = dbConnection.settings.Where(x => x.settings_id == objSettingModel.setting_id).FirstOrDefault();
                    objSetting.settings_id = objSettingModel.setting_id;
                    objSetting.setting_name = objSettingModel.setting_name;
                    objSetting.setting_value = objSettingModel.setting_value;
                    objSetting.default_label = objSettingModel.default_label;
                    objSetting.modified_on = objSettingModel.modified_on;
                    objSetting.modified_by = objSettingModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }

                else
                {
                    // Add Setting is not for end user, only Developer can use.
                    setting objSetting = new setting();
                    objSetting.setting_name = objSettingModel.setting_name;
                    objSetting.setting_value = objSettingModel.setting_value;
                    objSetting.default_label = objSettingModel.default_label;
                    objSetting.modified_on = objSettingModel.modified_on;
                    objSetting.modified_by = objSettingModel.modified_by;
                    dbConnection.settings.Add(objSetting);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check Setting Exist or not
        /// <summary>
        /// Check with database setting exist or not, if not then do entry of default setting
        /// </summary>
        /// <returns></returns>
        public bool CheckSettingExist()
        {
            bool Status = false;
            setting objSettings = new setting();
            int Count = dbConnection.settings.Count();
            if (Count < 6)
            {
                objSettings = new setting();
                objSettings.setting_name = "PageSize";
                objSettings.setting_value = "10";
                objSettings.default_label = "Page Size";
                dbConnection.settings.Add(objSettings);
                dbConnection.SaveChanges();
                objSettings = new setting();
                objSettings.setting_name = "SMTPServer";
                objSettings.setting_value = "smtp.gmail.com";
                objSettings.default_label = "SMTPServer";
                dbConnection.settings.Add(objSettings);
                objSettings = new setting();
                objSettings.setting_name = "EmailFrom";
                objSettings.setting_value = "phppeerbits@gmail.com";
                objSettings.default_label = "EmailFrom";
                dbConnection.settings.Add(objSettings);
                objSettings = new setting();
                objSettings.setting_name = "Password";
                objSettings.setting_value = "php123456";
                objSettings.default_label = "Password";
                dbConnection.settings.Add(objSettings);
                objSettings = new setting();
                objSettings.setting_name = "Port";
                objSettings.setting_value = "587";
                objSettings.default_label = "Port";
                dbConnection.settings.Add(objSettings);
                objSettings = new setting();
                objSettings.setting_name = "ReplyTo";
                objSettings.setting_value = "phppeerbits@gmail.com";
                objSettings.default_label = "ReplyTo";
                dbConnection.settings.Add(objSettings);
                dbConnection.SaveChanges();
                Status = true;
            }
            else
                Status = true;
            return Status;
        }
        #endregion
    }
}
