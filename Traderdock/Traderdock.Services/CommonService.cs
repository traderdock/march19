﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class CommonService
    {
        #region Global Variables
        EmailTemplateService objEmailTemplateService;
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Send Mail
        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="to"> Contains Mail Send To Email </param>
        /// <param name="emailTemplateName"> Contains EmailTemplateName </param>
        /// <param name="parameterValues"> Contains Parameter Values </param>
        /// <param name="attachmentPaths"> Contains Attachment Paths </param>
        /// <returns> returns True/False With Object Of Service Response </returns>
        public bool SendEmail(string to, string emailTemplateName, Dictionary<string, string> parameterValues, params string[] attachmentPaths)
        {
            objEmailTemplateService = new EmailTemplateService();
            EmailTemplateModel objEmailMaster = new EmailTemplateModel();
            objEmailMaster = objEmailTemplateService.GetEmailTemplateSubject(emailTemplateName);
            string sPath = ConfigurationManager.AppSettings["EmailSiteLink"];
            bool response = new bool();
            try
            {
                StringBuilder path = new StringBuilder();
                //hardik, 18-July-2018
                //ipn mail does not contain server context
                //path.Append(HttpContext.Current.Server.MapPath("~"));
                path.Append(sPath);
                path.Append("\\EmailTemplates\\" + emailTemplateName + ".html");
                var body = System.IO.File.ReadAllText(path.ToString());
                if (body != null)
                {
                    StringBuilder sb = new StringBuilder();
                    sb.Append(body);
                    foreach (var parameter in parameterValues)
                    {
                        sb = sb.Replace(parameter.Key, parameter.Value);
                    }
                    string sBody;
                    sBody = Convert.ToString(sb);
                    response = SendMail(to, objEmailMaster.Subject, sBody);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            //finally
            //{
            //    response = false;
            //}

            return response;
        }
        #endregion

        #region Send Mail
        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="to"> Contains Mail Send To Email </param>
        /// <param name="emailTemplateName"> Contains EmailTemplateName </param>
        /// <param name="parameterValues"> Contains Parameter Values </param>
        /// <param name="attachmentPaths"> Contains Attachment Paths </param>
        /// <returns> returns True/False With Object Of Service Response </returns>
        public bool SendEmailWithBody(string to, string strSubject, string strBody)
        {
            bool response = new bool();
            try
            {
                if (strBody != null)
                {
                    response = SendMail(to, strSubject, strBody);
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
            return response;
        }
        #endregion

        #region Get Setting Value
        /// <summary>
        /// Get Setting Value
        /// </summary>
        /// <param name="sSetingName"></param>
        /// <returns></returns>
        public string GetSettingValue(string sSetingName)
        {
            SettingsService objSettingService = new SettingsService();
            string sReturnString = string.Empty;
            if (!string.IsNullOrEmpty(sSetingName))
            {
                SettingModel objSettingModel = new SettingModel();
                objSettingModel = objSettingService.GetSettingByName(sSetingName);
                sReturnString = objSettingModel.setting_value;
            }
            return sReturnString;
        }

        /// <summary>
        /// Get Setting all value
        /// </summary>
        /// <returns></returns>
        public static List<SettingModel> GetSettingValues()
        {
            SettingsService objSettingService = new SettingsService();
            string sReturnString = string.Empty;
            List<SettingModel> lstSettingModel = new List<SettingModel>();
            lstSettingModel = objSettingService.GetSettingList();
            return lstSettingModel;
        }
        #endregion

        #region Send Mail
        /// <summary>
        /// Send Mail
        /// </summary>
        /// <param name="to"></param>
        /// <param name="subject"></param>
        /// <param name="body"></param>
        /// <param name="attachmentPaths"></param>
        /// <returns></returns>
        public static bool SendMail(string to, string subject, string body, string[] attachmentPaths = null)
        {
            bool status = true;
            List<SettingModel> lstSettingModel = new List<SettingModel>();
            lstSettingModel = GetSettingValues();
            try
            {
                // Replace sender@example.com with your "From" address. 
                // This address must be verified with Amazon SES.
                String FROM = lstSettingModel.Where(x => x.setting_name == "EmailFrom").Select(x => x.setting_value).FirstOrDefault();
                String FROMNAME = "Support Traderdock";

                // Replace recipient@example.com with a "To" address. If your account 
                // is still in the sandbox, this address must be verified.
                String TO = to;

                // Replace smtp_username with your Amazon SES SMTP user name.
                String SMTP_USERNAME = lstSettingModel.Where(x => x.setting_name == "UserName").Select(x => x.setting_value).FirstOrDefault();

                // Replace smtp_password with your Amazon SES SMTP user name.
                String SMTP_PASSWORD = lstSettingModel.Where(x => x.setting_name == "Password").Select(x => x.setting_value).FirstOrDefault();

                // If you're using Amazon SES in a region other than US West (Oregon), 
                // replace email-smtp.us-west-2.amazonaws.com with the Amazon SES SMTP  
                // endpoint in the appropriate AWS Region.
                String HOST = lstSettingModel.Where(x => x.setting_name == "SMTPServer").Select(x => x.setting_value).FirstOrDefault();
                // The port you will connect to on the Amazon SES SMTP endpoint. We
                // are choosing port 587 because we will use STARTTLS to encrypt
                // the connection.
                int PORT = Convert.ToInt32(lstSettingModel.Where(x => x.setting_name == "Port").Select(x => x.setting_value).FirstOrDefault());

                // The subject line of the email
                String SUBJECT = subject;
                // The body of the email
                String BODY = body;
                // Create and build a new MailMessage object
                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = new MailAddress(FROM, FROMNAME);
                message.To.Add(new MailAddress(TO));
                message.Subject = SUBJECT;
                message.Body = BODY;
                // Comment or delete the next line if you are not using a configuration set
                //message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);
                using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
                {
                    // Pass SMTP credentials
                    client.Credentials =
                        new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);
                    // Enable SSL encryption
                    client.EnableSsl = true;
                    // Try to send the message. Show status in console.
                    try
                    {
                        client.Send(message);
                    }
                    catch (Exception ex)
                    {
                        throw new ServiceLayerException(ex);
                    }
                }


                // MailMessage message1 = new MailMessage();
                // message.To.Add(new MailAddress(to));
                // message.Subject = subject;
                // message.From = new MailAddress(lstSettingModel.Where(x => x.setting_name == "EmailFrom").Select(x => x.setting_value).FirstOrDefault());
                // message.Body = body;
                // message.IsBodyHtml = true;
                // message.ReplyTo = new MailAddress(lstSettingModel.Where(x => x.setting_name == "ReplyTo").Select(x => x.setting_value).FirstOrDefault());
                // if (attachmentPaths != null)
                // {
                //     foreach (string attachmentPath in attachmentPaths)
                //     {
                //         if (!string.IsNullOrEmpty(attachmentPath))
                //         {
                //             var stream = new WebClient().OpenRead(attachmentPath);
                //             System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(stream, Path.GetFileName(attachmentPath));
                //             message.Attachments.Add(attachment);
                //         }
                //     }
                // }
                // var smtp = new SmtpClient();
                // smtp.Host = lstSettingModel.Where(x => x.setting_name == "SMTPServer").Select(x => x.setting_value).FirstOrDefault();
                // smtp.Port = Convert.ToInt32(lstSettingModel.Where(x => x.setting_name == "Port").Select(x => x.setting_value).FirstOrDefault());
                // smtp.EnableSsl = true;
                // smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network; ;
                // smtp.UseDefaultCredentials = false;
                // System.Net.ServicePointManager.ServerCertificateValidationCallback = delegate (object s,
                //System.Security.Cryptography.X509Certificates.X509Certificate certificate,
                //System.Security.Cryptography.X509Certificates.X509Chain chain,
                //System.Net.Security.SslPolicyErrors sslPolicyErrors)
                // {
                //     return true;
                // };
                // smtp.Credentials = new System.Net.NetworkCredential(lstSettingModel.Where(x => x.setting_name == "EmailFrom").Select(x => x.setting_value).FirstOrDefault(), lstSettingModel.Where(x => x.setting_name == "Password").Select(x => x.setting_value).FirstOrDefault());
                // smtp.Send(message);
            }
            catch (Exception)
            {
                throw;
            }

            return status;
        }
        #endregion

        #region Country Method
        /// <summary>
        /// /Get Country List
        /// </summary>
        /// <returns></returns>
        public List<CountryModel> GetCountryList()
        {
            Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
            List<CountryModel> list = new List<CountryModel>();
            try
            {
                List<country_master> CountryLists = dbConnection.country_master.Where(X => X.country_status_id == true).ToList();
                foreach (var item in CountryLists)
                {
                    var data = new CountryModel { country_name = item.country_name.ToUpper(), country_id = item.country_id };
                    list.Add(data);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }
        #endregion

        #region State Method
        /// <summary>
        /// Date: 25-July-2017
        /// Dev By: Hardik Savaliya
        /// Description: Get State List
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List<StateModel></returns>
        public List<StateModel> GetStateList(int id)
        {
            Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
            List<StateModel> list = new List<StateModel>();
            try
            {
                List<state_master> StateLists = dbConnection.state_master.Where(x => x.country_id == id).ToList();
                if (StateLists.Count > 0)
                {
                    foreach (var item in StateLists)
                    {
                        var data = new StateModel { state_name = item.state_name, state_id = item.state_id };
                        list.Add(data);
                    }
                }
                else
                {
                    var data = new StateModel { state_name = "No state available.", state_id = 0 };
                    list.Add(data);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return list;
        }

        #endregion

        #region Page Method
        /// <summary>
        /// GetPageList is return list of pages and it is fix and predefined
        /// </summary>
        /// <returns></returns>
        public List<PageModel> GetPageList()
        {
            List<PageModel> Pagelist = new List<PageModel>();
            try
            {
                var dictionary =
                new Dictionary<string, string> {
                { "Home", "Home" },
                { "Login", "Login" },
                { "LearnHow", "Learn How" },
                { "ContactUs","Contact Us" },
                { "AboutUs", "About Us" },
                { "Register", "Register" },
                { "CompleteProfile", "Complete Profile" },
                { "Help", "Help" },
                { "FAQS", "FAQS" },
            };

                foreach (KeyValuePair<string, string> pair in dictionary)
                {
                    PageModel obj = new PageModel();
                    obj.page_name = pair.Value;
                    obj.page_value = pair.Key;
                    Pagelist.Add(obj);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Pagelist;
        }
        /// <summary>
        /// GetPageSectionList service is use for get related page section it is predefined or fixed
        /// </summary>
        /// <param name="key">page key</param>        
        /// <returns></returns>
        public List<PageModel> GetPageSectionList(string key)
        {
            List<PageModel> PageSectionlist = new List<PageModel>();
            try
            {
                var dictionary =
                new Dictionary<string, string> {                    

                //{ "Home", "WhyTraderDock1:Why Trader Dock 1,WhyTraderDock2:Why Trader Dock 2,WhyTraderDock3:Why Trader Dock 3,WhyTraderDock4:Why Trader Dock 4,WhyTraderDock5:Why Trader Dock 5,Bannertext:Banner text,WebPanelFooter:WebPanel Footer,Step1_Challenge:Step1 Challenge,Step1_Challenge:Step1 Challenge,Step1_Platform:Step1 Platform,Step2_ShowExcel:Step2 ShowExcel,Step3_TraderDockCash:Step3 TraderDockCash,LoginCMS:Login CMS,SignUpCMS:Sign Up CMS,AboutUsMision:About Us Mision,AboutUsVision:About Us Vision,AboutUsVision:About Us Vision,SatisfactionPolicy:Satis faction Policy,Facebook:Facebook,Twitter:Twitter,GooglePlus:Google Plus,Linkedin:Linked in,Youtube:Youtube" },
                { "Home", "WhyTraderDock1:Why Trader Dock 1,WhyTraderDock2:Why Trader Dock 2,WhyTraderDock3:Why Trader Dock 3,WhyTraderDock4:Why Trader Dock 4,WhyTraderDock5:Why Trader Dock 5,Bannertext:Banner text,SocialSiteLink:Social Site Link,Footer:Footer,SatisfactionPolicy:Satisfaction Policy,TradingObjectives&Parameters:Trading Objectives & Parameters,GlobalTeam:Global Team,HowtotradewithTraderdock:How to trade with Traderdock,Takethetraderdockchallenge:Take the traderdock challenge,Showusyouexcel:Show us you excel,Beginlivetrading:Begin live trading,WhyTraderdock:Why Traderdock,LayoutFooterText:Layout Footer Text" },
                { "Login","LoginCMS:Login CMS" },
                { "LearnHow","LearnStep1TaketheTradeDockChallenge:Learn Step1 Take the TradeDock Challenge,LearnStep1Platform:Learn Step1 Platform,LearnStep2ShowYouExcelConsistently:Learn Step2 Show You Excel…Consistently,LearnStep3BeginLiveTradingwithTraderDockCash:Learn Step3 Begin Live Trading with TraderDock Cash,TradingObjectives&Parameters:Trading Objectives & Parameters,LearnBannerText:Learn Banner Text,LearnStep1Text:Learn Step1 Text,LearnStep2Text:Learn Step2 Text,LearnStep3Text:Learn Step3 Text,BannerTextStep2:Banner Text Step2,BannerTextStep3:Banner Text Step3,ProfitTable:Profit Table" },
                { "ContactUs","ContactUs:Contact Us,Banner Text ContactUS:BannerTextContactUS" },
                 { "AboutUs", "AboutUs:About Us,AboutUsVision:About Us Vision,AboutUsMision:About Us Mision" },
                { "Register", "TermsandConditions:Terms and Conditions,PrivacyPolicy:Privacy Policy,SignUpCMS:Sign Up CMS" },
                { "CompleteProfile", "SubscriptionAgreement:SubscriptionAgreement,SelfCertification:Self Certification" },
                { "Help", "Help:Help" },
                { "FAQS", "FAQSBannerText:FAQS Banner Text,FAQForEach:FAQ For Each"},
                };

                foreach (KeyValuePair<string, string> pair in dictionary)
                {
                    if (key == pair.Key)
                    {

                        var myarray = pair.Value.Split(',');
                        for (var i = 0; i < myarray.Length; i++)
                        {
                            var myarrayiiner = myarray[i].Split(':');
                            PageModel obj = new PageModel();
                            obj.page_name = myarrayiiner[0];
                            obj.page_value = myarrayiiner[1];
                            PageSectionlist.Add(obj);
                        }



                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return PageSectionlist;
        }

        public bool SaveEnquiryList(string enquiryEmail)
        {
            dbConnection = new Traderdock_DBEntities();
            enquiry objEnquiry = new enquiry();
            objEnquiry.email = enquiryEmail;
            objEnquiry.enquiry_datetime = System.DateTime.UtcNow;
            dbConnection.enquiries.Add(objEnquiry);
            dbConnection.SaveChanges();
            return true;
        }
        #endregion

        #region Date conversion Method
        public static DateTime TimeStamptoDate(string sTimeStamp, int iEnumDateFormat)
        {
            DateTime dtDateTime = new DateTime();
            if ((Int32)EnumDateFormat.NanoSec == iEnumDateFormat)
            {
                dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(Math.Round(Convert.ToDouble(sTimeStamp) / Convert.ToDouble(1000))).ToUniversalTime();
            }
            return dtDateTime;
        }
        #endregion

        #region Update Email Sent Count
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEmailAddress"></param>
        /// <param name="lUserId"></param>
        /// <returns></returns>
        public bool UpdateEmailSentCount(string strEmailAddress = "", long lUserId = 0)
        {
            if (!string.IsNullOrEmpty(strEmailAddress))
            {
                user_master objUserMaster = dbConnection.user_master.Where(x => x.user_email.Trim().ToLower().Equals(strEmailAddress.Trim().ToLower())).FirstOrDefault();
                if (objUserMaster != null)
                {
                    if (objUserMaster.email_sent_count == null)
                        objUserMaster.email_sent_count = 1;
                    else
                        objUserMaster.email_sent_count = objUserMaster.email_sent_count + 1;
                    dbConnection.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            else if (lUserId > 0)
            {
                user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == lUserId).FirstOrDefault();
                if (objUserMaster != null)
                {
                    objUserMaster.email_sent_count += objUserMaster.email_sent_count;
                    dbConnection.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion

    }
}
