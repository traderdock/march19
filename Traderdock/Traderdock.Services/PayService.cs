﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.MemberModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class PayService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        public AlliedWalletModel GetAWPayDetails(AlliedWalletModel model)
        {
            try
            {
                model.ApiUrl = AlliedWalletSettings.Url;
                model.QuickPayToken = AlliedWalletSettings.QuickPayToken;
                model.MerchantId = AlliedWalletSettings.MerchantID;
                model.SiteId = AlliedWalletSettings.SiteID;
                model.SubscriptionProductName = AlliedWalletSettings.SubscriptionProductName;
                model.ResetFeeProductName = AlliedWalletSettings.ResetFeeProductName;
                model.SubscriptionProductDescription = AlliedWalletSettings.SubscriptionProductDescription;
                model.ResetFeeProductDescription = AlliedWalletSettings.ResetFeeProductDescription;
                model.SuccessTransactionMessage = AlliedWalletSettings.SuccessTransactionMessage;
                model.CurrencyID = AlliedWalletSettings.CurrencyID;
                model.SubscriptionApprovedUrl = AlliedWalletSettings.SubscriptionApprovedUrl;
                model.ResetFeeApprovedUrl = AlliedWalletSettings.ResetFeeApprovedUrl;
                model.ConfirmUrl = AlliedWalletSettings.ConfirmUrl;
                model.DeclinedUrl = AlliedWalletSettings.DeclinedUrl;

                model.User = new MemberModel();

                if (model.UserId > 0)
                {
                    user_master userDetails = dbConnection.user_master.FirstOrDefault(x => x.user_id == model.UserId);

                    if (userDetails != null)
                    {
                        model.User.user_id = userDetails.user_id;
                        model.User.first_name = userDetails.first_name;
                        model.User.last_name = userDetails.last_name;
                        model.User.address = userDetails.address;
                        model.User.city_name = userDetails.city_name;
                        model.User.state_name = userDetails.state_name;
                        model.User.postal_code = userDetails.postal_code;
                        model.User.contact_number = userDetails.contact_number;
                        model.User.user_email = userDetails.user_email;
                    }

                    model.OrderAmount = 100;
                    subscription_master subscriptionDetails = dbConnection.subscription_master.FirstOrDefault(x => x.subscription_id == model.SubscriptionId);
                    if (subscriptionDetails != null)
                    {
                        model.SubscriptionName = subscriptionDetails.subscription_name;
                        model.OrderAmount = subscriptionDetails.price_per_month;
                        model.SubscriptionPlanId = AlliedWalletSettings.SubscriptionPlanID200;
                    }

                    if (model.PromoCodeId > 0)
                    {

                        PromoCodeService objPromoService = new PromoCodeService();
                        PromoCodeModel objPromoCode = new PromoCodeModel();
                        objPromoCode = objPromoService.GetPromocode(model.PromoCodeId);
                        model.OrderAmount = (model.OrderAmount - objPromoCode.reward_amount);

                        switch ((int)model.OrderAmount)
                        {
                            case 175:
                                model.SubscriptionPlanId = AlliedWalletSettings.SubscriptionPlanID175;
                                break;
                            case 150:
                                model.SubscriptionPlanId = AlliedWalletSettings.SubscriptionPlanID150;
                                break;
                            case 125:
                                model.SubscriptionPlanId = AlliedWalletSettings.SubscriptionPlanID125;
                                break;
                        }
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get All Pay Details
        /// <summary>
        /// Add Edit Rule Service for add & edit rule
        /// </summary>
        /// <param name="objRuleModel"></param>
        /// Date : 12/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public PaypalModel GetAllPayDetails(PaypalModel objPaypalModel)
        {
            try
            {
                objPaypalModel.User = new MemberModel();
                if (objPaypalModel.user_id > 0)
                {
                    user_master objUserDetails = dbConnection.user_master.Where(x => x.user_id == objPaypalModel.user_id).FirstOrDefault();
                    if (objUserDetails != null)
                    {
                        objPaypalModel.User.user_id = objUserDetails.user_id;
                        objPaypalModel.User.first_name = objUserDetails.first_name;
                        objPaypalModel.User.last_name = objUserDetails.last_name;
                        objPaypalModel.User.address = objUserDetails.address;
                        objPaypalModel.User.city_name = objUserDetails.city_name;
                        objPaypalModel.User.state_name = objUserDetails.state_name;
                        objPaypalModel.User.postal_code = objUserDetails.postal_code;
                        objPaypalModel.User.contact_number = objUserDetails.contact_number;
                        objPaypalModel.User.user_email = objUserDetails.user_email;
                    }
                    subscription_master SubscriptionDetails = dbConnection.subscription_master.Where(x => x.subscription_id == objPaypalModel.subscription_id).FirstOrDefault();
                    if (SubscriptionDetails != null)
                    {
                        objPaypalModel.subscription_name = SubscriptionDetails.subscription_name;
                        objPaypalModel.order_amount = SubscriptionDetails.price_per_month;
                    }
                    if (objPaypalModel.promo_code_id > 0)
                    {
                        PromoCodeService objPromoService = new PromoCodeService();
                        PromoCodeModel objPromoCode = new PromoCodeModel();
                        objPromoCode = objPromoService.GetPromocode(objPaypalModel.promo_code_id);
                        objPaypalModel.order_amount = (objPaypalModel.order_amount - objPromoCode.reward_amount);
                    }
                    if (!string.IsNullOrEmpty(objPaypalModel.card_expiry))
                    {
                        DateTime Expiredatetime = Convert.ToDateTime(objPaypalModel.card_expiry);
                        objPaypalModel.card_expiry_month = Expiredatetime.ToString("MM");
                        objPaypalModel.card_expiry_year = Expiredatetime.ToString("yy");
                    }
                }
                return objPaypalModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get User Payment History

        public List<PaypalModel> GetUserPaymentHistory(long id)
        {
            List<PaypalModel> lstPaypalModel = new List<PaypalModel>();
            try
            {

                var lstUserSubscription = from objUS in dbConnection.user_subscription
                                          join objS in dbConnection.subscription_master on objUS.subscription_id equals objS.subscription_id
                                          where objUS.user_id == id && objUS.free_trial != true
                                          select new { objUserSubscription = objUS, objSubscriptionMaster = objS };

                if (lstUserSubscription != null)
                {
                    PaypalModel objPaypalModel;
                    foreach (var item in lstUserSubscription)
                    {
                        objPaypalModel = new PaypalModel();
                        objPaypalModel.user_id = item.objUserSubscription.user_id;
                        objPaypalModel.subscription_id = item.objUserSubscription.user_subscription_id;
                        objPaypalModel.transaction_id = item.objUserSubscription.transaction_id;
                        objPaypalModel.order_amount = item.objUserSubscription.order_amount;
                        objPaypalModel.purchase_date = item.objUserSubscription.purchase_date.Value.ToString("dd/MM/yyyy");
                        objPaypalModel.subscription_name = item.objSubscriptionMaster.subscription_name;
                        objPaypalModel.payment_status = item.objUserSubscription.payment_status;
                        objPaypalModel.payment_response_code = item.objUserSubscription.payment_response_code;
                        objPaypalModel.is_current = item.objUserSubscription.is_current;
                        objPaypalModel.is_current = item.objUserSubscription.is_current;
                        objPaypalModel.subscription_start_date = item.objUserSubscription.subscription_start_date;
                        objPaypalModel.subscription_end_date = item.objUserSubscription.subscription_end_date;
                        lstPaypalModel.Add(objPaypalModel);
                    }
                }
                return lstPaypalModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion
    }
}
