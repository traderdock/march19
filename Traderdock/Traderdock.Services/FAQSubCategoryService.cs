﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class FAQSubCategoryService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region FAQ Sub Category List
        /// <summary>
        /// FAQ MAster List Kendo Grid
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public PagedList<FAQSubCategoryModel> FAQSubCategoryList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<FAQSubCategoryModel> lstFaqMasterModel = new List<FAQSubCategoryModel>();
                var query = dbConnection.faq_sub_category_master.Where(x => x.is_deleted == false).ToList();
                if (!string.IsNullOrEmpty(statusFilter) || statusFilter == "All")
                {
                    if (statusFilter == "Yes")
                    {
                        query = query.Where(x => x.is_active == true).ToList();
                    }
                    else
                    {
                        query = query.Where(x => x.is_active == false).ToList();
                    }
                }
                if (!string.IsNullOrEmpty(filter))
                {
                    query = query.Where(x => x.sub_category_title.Contains(filter) || x.sub_category_description.Contains(filter)).ToList();
                }
                if (query != null)
                {
                    FAQSubCategoryModel objFAQSubCategoryMasterModel;
                    foreach (var model in query)
                    {
                        objFAQSubCategoryMasterModel = new FAQSubCategoryModel();
                        objFAQSubCategoryMasterModel.faq_sub_category_id = model.faq_sub_category_id;
                        objFAQSubCategoryMasterModel.sub_category_title = model.sub_category_title;
                        objFAQSubCategoryMasterModel.sub_category_description = model.sub_category_description;
                        objFAQSubCategoryMasterModel.sub_category_order_sequence = model.order_sequence;
                        objFAQSubCategoryMasterModel.is_active = model.is_active;
                        objFAQSubCategoryMasterModel.faq_category_id = model.faq_category_id;
                        objFAQSubCategoryMasterModel.faq_category_title = model.faq_category_master.category_title;
                        objFAQSubCategoryMasterModel.SubCategoryStatus = model.is_active == true ? "Active" : "InActive";
                        lstFaqMasterModel.Add(objFAQSubCategoryMasterModel);
                    }
                    var result = new PagedList<FAQSubCategoryModel>(lstFaqMasterModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Add/Edit Sub Category
        /// <summary>
        /// Add / Edit MAQ Master
        /// </summary>
        /// <param name="objFAQMasterModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool AddEditFAQSubCategory(FAQSubCategoryModel objFAQSubCategoryModel)
        {
            try
            {
                bool bFlag = false;
                if (objFAQSubCategoryModel.faq_sub_category_id > 0)
                {
                    #region Update
                    faq_sub_category_master objFAQMaster = dbConnection.faq_sub_category_master.Where(x => x.faq_sub_category_id == objFAQSubCategoryModel.faq_sub_category_id).FirstOrDefault();
                    objFAQMaster.faq_sub_category_id = objFAQSubCategoryModel.faq_sub_category_id;
                    objFAQMaster.faq_category_id = objFAQSubCategoryModel.faq_category_id;
                    objFAQMaster.sub_category_title = objFAQSubCategoryModel.sub_category_title;
                    objFAQMaster.sub_category_description = objFAQSubCategoryModel.sub_category_description;
                    objFAQMaster.modified_on = objFAQSubCategoryModel.modified_on;
                    objFAQMaster.modified_by = objFAQSubCategoryModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                    #endregion
                }
                else
                {
                    #region Add New FAQ
                    faq_sub_category_master objFAQMaster = new faq_sub_category_master();
                    objFAQMaster.faq_category_id = objFAQSubCategoryModel.faq_category_id;
                    objFAQMaster.sub_category_title = objFAQSubCategoryModel.sub_category_title;
                    objFAQMaster.sub_category_description = objFAQSubCategoryModel.sub_category_description;
                    objFAQMaster.is_active = true;
                    objFAQMaster.created_on = objFAQSubCategoryModel.created_on;
                    objFAQMaster.created_by = objFAQSubCategoryModel.created_by;
                    dbConnection.faq_sub_category_master.Add(objFAQMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                    #endregion
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get FAQ
        /// <summary>
        /// Get FAQ By FAQ ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public FAQSubCategoryModel GetFAQSubCategory(Int64? id)
        {
            try
            {
                faq_sub_category_master objFAQSubCategory = dbConnection.faq_sub_category_master.Where(x => x.faq_sub_category_id == id).FirstOrDefault();
                if (objFAQSubCategory != null)
                {
                    FAQSubCategoryModel objFAQSubCategoryModel = new FAQSubCategoryModel();
                    objFAQSubCategoryModel.faq_sub_category_id = objFAQSubCategory.faq_sub_category_id;
                    objFAQSubCategoryModel.sub_category_title = objFAQSubCategory.sub_category_title;
                    objFAQSubCategoryModel.sub_category_description = objFAQSubCategory.sub_category_description;
                    objFAQSubCategoryModel.sub_category_order_sequence = objFAQSubCategory.order_sequence;
                    objFAQSubCategoryModel.created_by = objFAQSubCategory.created_by;
                    objFAQSubCategoryModel.created_on = objFAQSubCategory.created_on;
                    objFAQSubCategoryModel.faq_category_id = objFAQSubCategory.faq_category_id;
                    objFAQSubCategoryModel.SubCategoryStatus = objFAQSubCategory.is_active == true ? "True" : "False";
                    return objFAQSubCategoryModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Get List FAQ By List of IDs 
        /// </summary>
        /// <param name="iDs"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public List<FAQSubCategoryModel> GetFAQSubCategoryList(long id)
        {
            try
            {
                List<FAQSubCategoryModel> lstFAQSubCategoryModel = new List<FAQSubCategoryModel>();
                FAQSubCategoryModel objFAQSubCategoryModel;
                List<faq_sub_category_master> objFAQMaster = dbConnection.faq_sub_category_master.Where(x => x.faq_category_id == id && x.is_deleted == false).OrderBy(x => x.order_sequence).ToList();
                foreach (var objFaqModel in objFAQMaster)
                {
                    objFAQSubCategoryModel = new FAQSubCategoryModel();
                    objFAQSubCategoryModel.faq_sub_category_id = objFaqModel.faq_sub_category_id;
                    objFAQSubCategoryModel.faq_category_id = objFaqModel.faq_category_id;
                    objFAQSubCategoryModel.sub_category_title = objFaqModel.sub_category_title;
                    objFAQSubCategoryModel.sub_category_description = objFaqModel.sub_category_description;
                    objFAQSubCategoryModel.sub_category_order_sequence = objFaqModel.order_sequence;
                    foreach (var item in objFaqModel.faq_master.Where(x => x.is_active == true && x.is_deleted != true))
                    {
                        objFAQSubCategoryModel.FaqMasterList.Add(new FAQMasterModel()
                        {
                            faq_id = item.faq_id,
                            faq_category_id = item.faq_category_id,
                            faq_sub_category_id = item.faq_sub_category_id,
                            question = item.question,
                            order_sequences = item.order_sequence,
                            answer = item.answer,
                        });
                    }
                   
                    lstFAQSubCategoryModel.Add(objFAQSubCategoryModel);
                }
                return lstFAQSubCategoryModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }

        }
        #endregion

        #region Delete FAQ
        /// <summary>
        /// Delete Selected FAQ KEndo Grid
        /// </summary>
        /// <param name="objUserMasterModelList"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool DeleteFAQSubCategory(Int64 iFaqID, Int64 lUserId)
        {
            try
            {
                bool bResult = false;
                if (iFaqID > 0 && lUserId > 0)
                {
                    faq_sub_category_master objFaqMasterId = (from UM in dbConnection.faq_sub_category_master
                                                              where (UM.faq_sub_category_id == iFaqID)
                                                              select UM).FirstOrDefault();
                    objFaqMasterId.is_deleted = true;
                    objFaqMasterId.modified_by = lUserId;
                    objFaqMasterId.modified_on = System.DateTime.UtcNow;
                    dbConnection.SaveChanges();
                    bResult = true;
                }
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FAQ Active/Inactive
        /// <summary>
        /// FAQ Active And Inactive From Kendo Grid
        /// </summary>
        /// <param name="lIds"></param>
        /// <param name="LoginUserId"></param>
        /// <param name="IsActiveRequest"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool ActiveInactiveFAQSubCategory(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;
                List<faq_sub_category_master> objFAQMaster = (from FM in dbConnection.faq_sub_category_master
                                                              where (lIds).Contains(FM.faq_sub_category_id)
                                                              select FM).AsEnumerable().ToList();
                objFAQMaster.ForEach(x => x.modified_by = LoginUserId);
                objFAQMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objFAQMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region updateOrderFAQ
        /// <summary>
        /// update Order FAQ
        /// </summary>
        /// <param name="ifaqid"></param>
        /// <param name="isequence"></param>
        /// <returns></returns>
        public bool UpdateSubCategoryFAQ(string[] arrCategoryID)
        {
            List<faq_sub_category_master> lstFAQSubCategoryMaster = dbConnection.faq_sub_category_master.ToList();
            if (lstFAQSubCategoryMaster.Count() > 0)
            {
                int i = 0;
                foreach (var item in arrCategoryID)
                {
                    lstFAQSubCategoryMaster.Where(x => x.faq_sub_category_id == Convert.ToInt64(item)).ToList().ForEach(x => x.order_sequence = i);
                    i++;
                }
                dbConnection.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion

    }
}
