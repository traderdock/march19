﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class ResetService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Reset List
        /// <summary>
        /// Date: 13-Nov-2017
        /// Dev By: Hardik Savaliya
        /// Description: total Revenue method
        /// Feedback1:
        /// Feedback2:
        /// </summary>
        /// <returns></returns>
        public PagedList<UserMasterModel> ResetList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<UserMasterModel> lstUserMasterModel = new List<UserMasterModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.reset_requests
                          .Where(um => um.user_master.user_email.ToLower().Contains(filter.ToLower()) && (string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.reset_status == true || um.reset_status == false : (um.reset_status == true || um.reset_status == false))
                        orderby UM.reset_request_id descending
                        select new
                        {
                            reset_request_id = UM.reset_request_id,
                            user_id = UM.user_id,
                            transactions_id = UM.transaction_id,
                            order_amount = UM.order_amount,
                            purchase_date = UM.purchase_date,
                            payment_method = UM.payment_method,
                            payment_status = UM.payment_status,
                            Email = UM.user_master.user_email,
                            reset_status = UM.reset_status,
                            subscription_id = UM.user_subscription_id,
                        };


                if (query != null)
                {
                    UserMasterModel objUserMasterModel;
                    foreach (var model in query)
                    {
                        objUserMasterModel = new UserMasterModel();
                        objUserMasterModel.reset_request_id = model.reset_request_id;
                        objUserMasterModel.user_id = model.user_id;
                        objUserMasterModel.transactions_id = model.transactions_id;
                        objUserMasterModel.order_amount = model.order_amount;
                        objUserMasterModel.purchase_date = model.purchase_date;
                        objUserMasterModel.payment_method = model.payment_method;
                        objUserMasterModel.user_email = model.Email;
                        objUserMasterModel.SubscriptionModeString = dbConnection.user_subscription.Where(x => x.user_subscription_id == model.subscription_id).Select(x => x.subscription_master.subscription_name).FirstOrDefault();
                        objUserMasterModel.reset_status = model.reset_status;
                        lstUserMasterModel.Add(objUserMasterModel);
                    }
                    var result = new PagedList<UserMasterModel>(lstUserMasterModel, pageIndex, pageSize);
                    return result;
                }
                else
                {
                    var result = new PagedList<UserMasterModel>(lstUserMasterModel, pageIndex, pageSize);
                    return result;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ResetRequest(int id)
        {
            var objData = dbConnection.user_subscription.Where(x => x.user_id == id && x.is_current == true).FirstOrDefault();
            if (objData != null)
            {
                var objResetData = dbConnection.reset_requests.Where(x => x.user_id == objData.user_id && x.reset_status == false).FirstOrDefault();
                if (objResetData != null)
                {
                    objData.is_current = false;
                    dbConnection.SaveChanges();
                    objData.is_current = true;
                    dbConnection.user_subscription.Add(objData);
                    objResetData.resetted_date = System.DateTime.UtcNow;
                    objResetData.reset_status = true;
                    dbConnection.SaveChanges();

                    //send reset account information mail to user
                    UserMasterModel objUserMasterModel = new UserMasterModel();
                    UserService objUserService = new UserService();
                    CommonService objCommonService = new CommonService();
                    objUserMasterModel = objUserService.GetUser(objResetData.user_id);
                    if (!string.IsNullOrEmpty(objUserMasterModel.user_email))
                    {
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(EmailTemplateParameters.Email, objUserMasterModel.user_email);
                        parameters.Add(EmailTemplateParameters.FullName, objUserMasterModel.first_name);
                        //string emailattachment = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["attachmentemail"]);
                        new CommonService().SendEmail(objUserMasterModel.user_email, EmailTemplateNames.ResetAccountConfirmation, parameters, null);
                    }
                    return true;
                }
                else
                    return false;
            }
            else
                return false;
        }
        #endregion
    }
}
