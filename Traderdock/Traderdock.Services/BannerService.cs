﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class BannerService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Subscription List
        public PagedList<BannerModel> BannerList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<BannerModel> lstBannerModel = new List<BannerModel>();

                //Linq Query
                var query =
                        from CM in dbConnection.banner_master
                            //.Where(um => um.banner_name.Contains(filter)).DefaultIfEmpty()
                        where CM.banner_name.Contains(filter)
                        orderby CM.banner_name
                        select new
                        {
                            banner_id = CM.banner_id,
                            banner_name = CM.banner_name,
                            banner_type_id = CM.banner_type_id,
                        };

                if (query != null)
                {
                    BannerModel objBannerModel;
                    foreach (var model in query)
                    {
                        objBannerModel = new BannerModel();
                        objBannerModel.banner_id = model.banner_id;
                        objBannerModel.banner_name = model.banner_name;
                        objBannerModel.banner_type_id = model.banner_type_id;
                        if (model.banner_type_id == (Int32)EnumBannerType.HomePageBanner)
                        {
                            objBannerModel.banner_type_name = "Home Page Banner";
                        }
                        if (model.banner_type_id == (Int32)EnumBannerType.ContactUsBanner)
                        {
                            objBannerModel.banner_type_name = "Contact Us Banner";
                        }
                        lstBannerModel.Add(objBannerModel);
                    }
                    var result = new PagedList<BannerModel>(lstBannerModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Banner
        /// <summary>
        /// Get Banner By Banner ID
        /// </summary>
        /// <param name="BannerID"></param>
        /// <returns></returns>
        /// Date : 18/8/2017
        /// Dev By: Aakash Prajapati
        public BannerModel GetBanner(long? BannerID)
        {
            BannerModel objModel = new BannerModel();
            banner_master BannerMaster = dbConnection.banner_master.Where(x => x.banner_id == BannerID).FirstOrDefault();
            if (BannerMaster != null)
            {
                objModel = new BannerModel();
                objModel.banner_id = BannerMaster.banner_id;
                objModel.banner_name = BannerMaster.banner_name;
                objModel.banner_type_id = BannerMaster.banner_type_id;
                objModel.banner_image_url = Utilities.GetImagePath()+ "/Images/" + BannerMaster.banner_image;
                objModel.banner_image_old_name = BannerMaster.banner_image;
                objModel.banner_image_name = BannerMaster.banner_image;
            }
            return objModel;
        }

        /// <summary>
        /// Get Banner By TypeID
        /// </summary>
        /// <param name="BannerType"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public BannerModel GetBannerByType(int BannerType)
        {
            BannerModel objModel = null;
            banner_master BannerMaster = dbConnection.banner_master.Where(x => x.banner_type_id == BannerType).FirstOrDefault();
            if (BannerMaster != null)
            {
                objModel = new BannerModel();
                objModel.banner_name = BannerMaster.banner_name;
                if (BannerMaster.banner_type_id == (Int32)EnumBannerType.HomePageBanner)
                {
                    objModel.banner_type_name = "Home Page Banner";
                }
                if (BannerMaster.banner_type_id == (Int32)EnumBannerType.ContactUsBanner)
                {
                    objModel.banner_type_name = "Contact Us Banner";
                }
                else
                {
                    objModel.banner_type_name = "Traderdock";
                }
            }
            return objModel;
        }
        #endregion

        #region Save Banner
        /// <summary>
        /// Save Banner Data
        /// </summary>
        /// <param name="objModel"></param>
        /// <returns></returns>
        /// Date : 30/7/2017
        /// Dev By: Aakash Prajapati
        public bool SaveBanner(BannerModel objModel, string _AttachmentPath)
        {
            bool Status = false;
            try
            {
                if (objModel.banner_id > 0)
                {
                    banner_master BannerMaster = dbConnection.banner_master.Where(x => x.banner_id == objModel.banner_id).FirstOrDefault();
                    if (BannerMaster != null)
                    {
                        BannerMaster.banner_name = objModel.banner_name;
                        BannerMaster.banner_type_id = objModel.banner_type_id;
                        if (objModel.banner_image_name != BannerMaster.banner_image)
                        {
                            BannerMaster.banner_image = objModel.banner_image_name;
                            var _FileName = objModel.banner_image_old_name;
                            //var _AttachmentPath = Utilities.GetImagePath() + "\\Images\\";
                            if (System.IO.File.Exists(Path.Combine(_AttachmentPath, _FileName)) && _FileName != "Default_profile.png")
                            {
                                System.IO.File.Delete(Path.Combine(_AttachmentPath, _FileName));
                            }

                        }
                        BannerMaster.modified_by = objModel.created_by;
                        BannerMaster.modified_date = DateTime.UtcNow;
                        dbConnection.SaveChanges();
                        Status = true;
                    }
                }
                else
                {
                    banner_master BannerMaster = new banner_master();
                    if (BannerMaster != null)
                    {
                        BannerMaster.banner_name = objModel.banner_name;
                        BannerMaster.banner_image = Utilities.GetImageName(objModel.banner_image_url);
                        BannerMaster.banner_type_id = objModel.banner_type_id;
                        BannerMaster.created_by = BannerMaster.modified_by = objModel.created_by;
                        BannerMaster.created_date = BannerMaster.modified_date = DateTime.UtcNow;
                        dbConnection.banner_master.Add(BannerMaster);
                        dbConnection.SaveChanges();
                        Status = true;
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return Status;
        }
        #endregion

        #region Banner Delete
        public bool DeleteBanner(long BannerID)
        {
            try
            {
                bool bResult = false;
                if (BannerID > 0)
                {
                    banner_master objBannerMaster = (from UM in dbConnection.banner_master
                                                          where (UM.banner_id == BannerID)
                                                          select UM).FirstOrDefault();
                    dbConnection.banner_master.Remove(objBannerMaster);
                    dbConnection.SaveChanges();
                    bResult = true;
                }
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

    }
}