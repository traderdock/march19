﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Traderdock.Common;
using Traderdock.Entity.ViewModel;

using Traderdock.ORM;

namespace Traderdock.Services
{
    public class TestimonialService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        public TestimonialModel GetTestimonial(Int64? id)
        {
            try
            {
                testimonial_master objDetails = dbConnection.testimonial_master.Where(x => x.testimonial_id == id).FirstOrDefault();
                if (objDetails != null)
                {
                    TestimonialModel objModel = new TestimonialModel();
                    objModel.testimonial_id = objDetails.testimonial_id;
                    objModel.user_name = objDetails.user_name;
                    objModel.description = objDetails.description;
                    objModel.user_image_url = !string.IsNullOrEmpty(objDetails.user_image_url) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objModel.FileName = !string.IsNullOrEmpty(objDetails.user_image_url) ? Utilities.GetImagePath() + "/Images/" + objDetails.user_image_url : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    return objModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool AddEditTestimonial(TestimonialModel objModel)
        {
            try
            {
                bool bFlag = false;
                if (objModel.testimonial_id> 0)
                {
                    testimonial_master objMaster = dbConnection.testimonial_master.Where(x => x.testimonial_id == objModel.testimonial_id).FirstOrDefault();
                    objMaster.user_name = objModel.user_name;
                    objMaster.description = objModel.description;
                    if (objModel.user_image_url != null)
                        objMaster.user_image_url= Utilities.GetImageName(objModel.user_image_url);
                    objMaster.modified_on = objModel.modified_on;
                    objMaster.modified_by = objModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {

                    testimonial_master objMaster = new testimonial_master();
                    objMaster.user_name = objModel.user_name;
                    objMaster.description = objModel.description;
                    objMaster.user_image_url= Utilities.GetImageName(objModel.user_image_url);
                    objMaster.is_deleted = false;
                    objMaster.is_active = true;
                    objMaster.created_on = objModel.created_on;
                    objMaster.created_by = objModel.created_by;
                    objMaster.modified_on = objModel.created_on;
                    objMaster.modified_by = objModel.created_by;
                    dbConnection.testimonial_master.Add(objMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Testimonial List
        public PagedList<TestimonialModel> TestimonialList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "")
        {
            try
            {
                IList<TestimonialModel> lstUserModel = new List<TestimonialModel>();
                //Linq Query
                var query =
                        from UM in dbConnection.testimonial_master
                          .Where(um => um.user_name.Contains(filter) && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where UM.is_deleted == false
                        orderby UM.testimonial_id descending
                        select new
                        {
                            testimonial_id = UM.testimonial_id,
                            user_name = UM.user_name,
                            description = UM.description,
                            user_image_url = UM.user_image_url,
                            is_active = UM.is_active,
                        };

                if (query != null)
                {
                    TestimonialModel objModel;
                    foreach (var model in query)
                    {
                        objModel = new TestimonialModel();
                        objModel.testimonial_id = model.testimonial_id;
                        objModel.user_name= model.user_name;
                        objModel.description = model.description;
                        objModel.user_image_url = !string.IsNullOrEmpty(model.user_image_url) ?  model.user_image_url : "experience-img1.png";
                        objModel.is_active= model.is_active;
                        lstUserModel.Add(objModel);
                    }
                    var result = new PagedList<TestimonialModel>(lstUserModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        public List<testimonial_master> GetTestimonials(List<Int64> iDs)
        {
            try
            {
                List<testimonial_master> objUserMaster = (from UM in dbConnection.testimonial_master
                                                          where iDs.Contains(UM.testimonial_id)
                                                   select UM).ToList();
                return objUserMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool DeleteTestimonials(IList<testimonial_master> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                var Ids = objUserMasterModelList.Select(x => x.testimonial_id).ToList();
                List<testimonial_master> objUserMaster = (from UM in dbConnection.testimonial_master
                                                   where (Ids).Contains(UM.testimonial_id)
                                                   select UM).ToList();
                objUserMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
