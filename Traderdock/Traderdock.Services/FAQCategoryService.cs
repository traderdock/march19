﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class FAQCategoryService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region FAQ Category List
        public PagedList<FAQCategoryModel> FAQCategoryList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<FAQCategoryModel> lstFaqCategoryModel = new List<FAQCategoryModel>();
                lstFaqCategoryModel = GetFaqCategories();
                if (lstFaqCategoryModel.Count > 0)
                {
                    var result = new PagedList<FAQCategoryModel>(lstFaqCategoryModel, pageIndex, pageSize);
                    return result;
                }
                else
                    return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        //public IList<FAQCategoryModel> GetFaqCategories()
        public List<FAQCategoryModel> GetFaqCategories()
        {
            //IList<FAQCategoryModel> lstFaqCategoryModel = new List<FAQCategoryModel>();
            List<FAQCategoryModel> lstFaqCategoryModel = new List<FAQCategoryModel>();
            //Linq Query
            var query = from FCM in dbConnection.faq_category_master.Where(fcm => fcm.is_active).DefaultIfEmpty()
                        where FCM.is_active == true
                        orderby FCM.order_sequence ascending
                        select new
                        {
                            faq_category_id = FCM.faq_category_id,
                            category_title = FCM.category_title,
                            order_sequence = FCM.order_sequence,
                            category_description = FCM.category_description,
                            category_image = FCM.category_image,
                            faq_description = FCM.faq_description,
                            faq_sub_category = FCM.faq_sub_category_master
                        };

            if (query != null)
            {
                FAQCategoryModel objFaqCategoryModel;
                foreach (var model in query)
                {
                    objFaqCategoryModel = new FAQCategoryModel();
                    objFaqCategoryModel.faq_category_id = model.faq_category_id;
                    objFaqCategoryModel.category_title = model.category_title;
                    objFaqCategoryModel.order_sequence = model.order_sequence;
                    objFaqCategoryModel.category_description = model.category_description;
                    //objFaqCategoryModel.faq_description = model.faq_description;
                    objFaqCategoryModel.category_image = model.category_image;
                    foreach (var item in model.faq_sub_category.Where(x => x.is_deleted != true && x.is_active == true))
                    {
                        objFaqCategoryModel.SubCategory.Add(new FAQSubCategoryModel
                        {
                            faq_sub_category_id = item.faq_sub_category_id,
                            faq_category_id = item.faq_category_id,
                            faq_category_title = item.sub_category_title,
                            sub_category_description = item.sub_category_description,
                            sub_category_order_sequence = item.order_sequence,
                            is_active = item.is_active,
                            is_deleted = item.is_deleted
                        });
                    }
                    lstFaqCategoryModel.Add(objFaqCategoryModel);
                }
            }
            return lstFaqCategoryModel;
        }
        #endregion

        #region Add/Edit Category
        public bool AddEditFAQCategory(FAQCategoryModel objFaqCategoryModel)
        {
            try
            {
                bool bFlag = false;
                if (objFaqCategoryModel.faq_category_id > 0)
                {
                    faq_category_master objfaq_category_master = dbConnection.faq_category_master.Where(x => x.faq_category_id == objFaqCategoryModel.faq_category_id).FirstOrDefault();
                    objfaq_category_master.category_title = objFaqCategoryModel.category_title;
                    objfaq_category_master.category_description = objFaqCategoryModel.category_description;
                    //objfaq_category_master.faq_description = objFaqCategoryModel.faq_description;
                    objfaq_category_master.category_image = objFaqCategoryModel.category_image;
                    objfaq_category_master.modified_on = objFaqCategoryModel.modified_on;
                    objfaq_category_master.modified_by = objFaqCategoryModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {
                    #region working code but this module not listed in SRS doc that's why commented
                    faq_category_master objfaq_category_master = new faq_category_master();
                    objfaq_category_master.category_title = objFaqCategoryModel.category_title;
                    objfaq_category_master.category_description = objFaqCategoryModel.category_description;
                    //objfaq_category_master.faq_description = objFaqCategoryModel.faq_description;
                    objfaq_category_master.category_image = objFaqCategoryModel.category_image;
                    objfaq_category_master.is_active = true;
                    objfaq_category_master.modified_on = objfaq_category_master.created_on = objFaqCategoryModel.created_on;
                    objfaq_category_master.modified_by = objfaq_category_master.created_by = objFaqCategoryModel.created_by;
                    dbConnection.faq_category_master.Add(objfaq_category_master);
                    dbConnection.SaveChanges();
                    bFlag = true;
                    #endregion
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get FAQ Catergory
        public FAQCategoryModel GetFAQCategory(Int64? id)
        {
            try
            {
                faq_category_master objFAQCategoryMaster = dbConnection.faq_category_master.Where(x => x.faq_category_id == id).FirstOrDefault();
                if (objFAQCategoryMaster != null)
                {
                    FAQCategoryModel objFaqCategoryModel = new FAQCategoryModel();
                    objFaqCategoryModel = new FAQCategoryModel();
                    objFaqCategoryModel.faq_category_id = objFAQCategoryMaster.faq_category_id;
                    objFaqCategoryModel.category_title = objFAQCategoryMaster.category_title;
                    objFaqCategoryModel.category_description = objFAQCategoryMaster.category_description;
                    objFaqCategoryModel.category_image = objFAQCategoryMaster.category_image;
                    foreach (var item in objFAQCategoryMaster.faq_sub_category_master.Where(x => x.is_deleted != true && x.is_active == true))
                    {
                        objFaqCategoryModel.SubCategory.Add(new FAQSubCategoryModel
                        {
                            faq_sub_category_id = item.faq_sub_category_id,
                            faq_category_id = item.faq_category_id,
                            faq_category_title = item.sub_category_title,
                            sub_category_description = item.sub_category_description,
                            sub_category_order_sequence = item.order_sequence,
                            is_active = item.is_active,
                            is_deleted = item.is_deleted
                        });
                    }
                    return objFaqCategoryModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region FAQ Catergory Service
        public bool DeleteFAQCategory(Int32 iFaqID, Int64 lUserId)
        {
            try
            {
                bool bResult = false;
                if (iFaqID > 0 && lUserId > 0)
                {
                    faq_category_master objFaqMasterId = (from UM in dbConnection.faq_category_master
                                                          where (UM.faq_category_id == iFaqID)
                                                          select UM).FirstOrDefault();
                    objFaqMasterId.is_active = false;
                    objFaqMasterId.modified_by = lUserId;
                    objFaqMasterId.modified_on = System.DateTime.UtcNow;
                    dbConnection.SaveChanges();
                    bResult = true;
                }
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region updateOrderFAQ
        /// <summary>
        /// update Order FAQ
        /// </summary>
        /// <param name="ifaqid"></param>
        /// <param name="isequence"></param>
        /// <returns></returns>
        public bool UpdateCategoryFAQ(string[] arrCategoryID)
        {
            List<faq_category_master> lstFAQCategoryMaster = dbConnection.faq_category_master.ToList();
            if (lstFAQCategoryMaster.Count() > 0)
            {
                int i = 0;
                foreach (var item in arrCategoryID)
                {
                    lstFAQCategoryMaster.Where(x => x.faq_category_id == Convert.ToInt64(item)).ToList().ForEach(x => x.order_sequence = i);
                    i++;
                }
                dbConnection.SaveChanges();
                return true;
            }
            return false;
        }
        #endregion
    }
}
