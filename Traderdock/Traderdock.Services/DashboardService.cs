﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.ViewModel;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class DashboardService
    {
        #region Variable
        /// <summary>
        /// Db Connection String
        /// </summary>

        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region Dashboard User List
        /// <summary>
        /// Kendo Dashboard User List
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="UserId"></param>
        /// <returns></returns>
        public PagedList<UserDashboardModel> DashBoardUserList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<UserDashboardModel> lstUserModel = new List<UserDashboardModel>();

                //Linq Query
                var query =
                        from UM in dbConnection.user_master
                          .Where(um => um.user_type_id == (Int32)EnumUserType.Normal && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where UM.is_deleted == false
                        orderby UM.user_id descending
                        select new
                        {
                            UserId = UM.user_id,
                            Email = UM.user_email,
                            SubscriptionMode = UM.subscription_mode,
                            JoinDate = UM.created_on,
                            UserStatus = UM.is_active,
                        };

                if (query != null)
                {
                    UserDashboardModel objUserDashboardModel;
                    foreach (var model in query)
                    {
                        objUserDashboardModel = new UserDashboardModel();
                        objUserDashboardModel.user_id = model.UserId;
                        objUserDashboardModel.SubscriptionModeString = model.SubscriptionMode == (Int32)EnumSubscriptionMode.Free_Trial ? EnumSubscriptionMode.Free_Trial.ToString() : EnumSubscriptionMode.Subscribed.ToString();
                        objUserDashboardModel.join_date = model.JoinDate.ToString("dd-MM-yyyy");
                        objUserDashboardModel.Subscription_count = 2;
                        objUserDashboardModel.Total_Revenue = 12000;
                        lstUserModel.Add(objUserDashboardModel);
                    }
                    var result = new PagedList<UserDashboardModel>(lstUserModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Dashboard Notification

        /// <summary>
        /// Get all notification using login id
        /// </summary>
        /// <param name="LogedUserID"></param>
        /// <returns></returns>
        public string NotificationList(long LogedUserID)
        {
            try
            {
                string StrPath = System.Web.Configuration.WebConfigurationManager.AppSettings["SiteLink"];
                //NotificationMasterModel objNotificationMasterModel = new NotificationMasterModel();
                List<user_notifications> objuser_notificationsList = dbConnection.user_notifications.Where(x => x.user_id == LogedUserID && x.notification_status).ToList();
                string notification_data = string.Empty;
                foreach (var Notification in objuser_notificationsList)
                {
                    StrPath = StrPath + "/admin/dashboard/Notification_Mark_Read?NotificationID=" + Notification.user_notification_id;
                    //<li><a href="#"><i class="fa fa-users text-aqua"></i> 5 new members joined today</a></li>
                    if (notification_data.Length > 0)
                    {
                        notification_data = notification_data + "<li><a onclick='return NotificationReaded(" + Notification.user_notification_id + ")'><i class='text-aqua'></i>" + Notification.notification_master.notification_message + "</a></li>";
                        //notification_data = notification_data + "<li><a href='"+ StrPath + "'><i class='text-aqua'></i>" + Notification.notification_master.notification_message + "</a></li>";
                    }
                    else
                    {
                        //notification_data = "<li><a href='"+ StrPath +"'><i class='text-aqua'></i>" + Notification.notification_master.notification_message + "</a></li>";
                        notification_data = "<li><a onclick='return NotificationReaded(" + Notification.user_notification_id + ")'><i class='text-aqua'></i>" + Notification.notification_master.notification_message + "</a></li>";
                    }//NotificationReaded(5)
                }
                return notification_data;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get Notification Count
        /// </summary>
        /// <param name="LogedUserID"></param>
        /// <returns></returns>
        public int NotificationCount(long LogedUserID)
        {
            try
            {
                string StrPath = string.Empty;
                List<user_notifications> objUserNotificationList = dbConnection.user_notifications.Where(x => x.user_id == LogedUserID && x.notification_status).ToList();
                return objUserNotificationList.Count;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Single Notification Read
        /// </summary>
        /// <param name="LogedUserID"></param>
        /// <param name="NotificationID"></param>
        /// <returns></returns>
        public bool NotificationRead(long LogedUserID, long NotificationID)
        {
            try
            {
                user_notifications objuser_notifications = dbConnection.user_notifications.Where(x => x.user_notification_id == NotificationID).FirstOrDefault();
                if (objuser_notifications != null)
                {
                    objuser_notifications.notification_status = true;
                    dbConnection.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Notification read for all using login id
        /// </summary>
        /// <param name="LogedUserID"></param>
        /// <returns></returns>
        public bool Notification_ALL_Readed(long LogedUserID)
        {
            try
            {
                List<user_notifications> objuser_notificationsList = dbConnection.user_notifications.Where(x => x.user_id == LogedUserID).ToList();
                objuser_notificationsList.ForEach(x => x.notification_status = false);
                dbConnection.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Get User Total Count
        /// <summary>
        /// Get Total Active User Count
        /// </summary>
        /// <returns></returns>
        /// Date : 03/07/2017
        /// Dev By: Aakash Prajapati
        public long TotalActiveUserCount()
        {
            try
            {
                long objUserCount = dbConnection.user_master.Where(x => x.user_type_id != (Int32)EnumUserType.Admin && x.is_deleted == false).Count();
                return objUserCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Subscribed User Count
        /// <summary>
        /// Get Total Subscribed User Count
        /// </summary>
        /// <returns></returns>
        /// Date : 03/07/2017
        /// Dev By: Aakash Prajapati
        public long TotalSubscribedUserCount()
        {
            try
            {
                long objSubscribedUserCount = dbConnection.user_subscription.Where(x => x.free_trial == false && x.is_current == true).GroupBy(x => x.user_id).Count();
                //long  = dbConnection.user_master.Where(x => x.user_type_id != (Int32)EnumUserType.Admin && x.subscription_mode == (Int32)EnumSubscriptionMode.Subscribed && x.is_active).Count();
                return objSubscribedUserCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Total Todays Revenue
        /// <summary>
        /// Get Total Todays Revenue
        /// </summary>
        /// <returns></returns>
        /// Date : 19/08/2017
        /// Dev By: Aakash Prajapati
        public decimal TotalTodaysRevenue()
        {
            try
            {
                decimal objSubscribedUserCount = 0;
                //decimal objSubscribedUserCount1 = 0;
                DateTime Today_start = DateTime.UtcNow.Date;
                DateTime Today_end = Today_start.AddHours(23).AddMinutes(59).AddSeconds(59);
                //objSubscribedUserCount1 = dbConnection.user_subscription.Where(x => x.created_date >= Today_start && x.created_date <= Today_end && x.payment_status == 1).Sum(x => x.order_amount.Value);

                var objSubscriptionList = dbConnection.user_subscription
                    .Where(x => x.created_date != null && x.created_date >= Today_start && x.created_date <= Today_end && x.payment_status == 1 && x.order_amount != null).Select(x => x.order_amount.Value).ToList();
                objSubscribedUserCount = objSubscriptionList.Sum();
                return objSubscribedUserCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Total Todays Revenue
        /// <summary>
        /// Get Total Todays Revenue
        /// </summary>
        /// <returns></returns>
        /// Date : 19/08/2017
        /// Dev By: Aakash Prajapati
        public decimal TotalRevenue()
        {
            try
            {
                decimal objSubscribedUserCount = dbConnection.user_subscription.Where(x => x.payment_status == 1 && x.order_amount != null).Sum(x => x.order_amount.Value);
                //foreach (var item in objSubscriptionList)
                //{
                //    if (item != null)
                //    {
                //        objSubscribedUserCount = objSubscribedUserCount + item;
                //    }
                //    //if (item.order_amount != null)
                //    //{
                //    //    objSubscribedUserCount = objSubscribedUserCount + item.order_amount.Value;
                //    //}
                //}

                return objSubscribedUserCount;

            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Free Trial User Count
        /// <summary>
        /// Get Total Free Trial User Count
        /// </summary>
        /// <returns></returns>
        /// Date : 03/07/2017
        /// Dev By: Aakash Prajapati
        public long TotalFreeTrialUserCount()
        {
            try
            {
                long objFreeTrialUserCount = dbConnection.user_subscription.Where(x => x.free_trial == true && x.is_current == true).GroupBy(x => x.user_id).Count();
                return objFreeTrialUserCount;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region User Transaction List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 11/7/2017
        /// Dev By: Bharat Katua
        public List<HomePageModel> GetUserTransactionList(out long NextSubId, string Sequence = "", Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                List<HomePageModel> lstUserTransactionModel = new List<HomePageModel>();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                //from c in categories
                //join p in products on c.Category equals p.Category into ps
                //from p in ps.DefaultIfEmpty()
                //select new { Category = c, ProductName = p == null ? "(No products)" : p.ProductName };

                var query =
                    (from UT in dbConnection.user_transactions
                     join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                     where US.user_id == userID & US.user_subscription_id == user_subscription_id
                     orderby UT.transaction_date descending
                     select new
                     {
                         transaction_id = UT.transaction_id,
                         user_subscription_id = UT.user_subscription_id,
                         //current_balance = UT.current_balance,
                         high_balance = UT.high_balance,
                         low_balance = UT.low_balance,
                         avg_winning_day = UT.avg_winning_day,
                         avg_losing_day = UT.avg_losing_day,
                         best_day = UT.best_day,
                         worst_day = UT.worst_day,
                         net_profit_loss = UT.net_profit_loss,
                         high_profit_loss = UT.high_profit_loss,
                         low_profit_loss = UT.low_profit_loss,
                         total_contracts = UT.total_contracts,
                         total_commision = UT.total_commision,
                         total_trades = UT.total_trades,
                         avg_winning_trade = UT.avg_winning_trade,
                         avg_losing_trader = UT.avg_losing_trader,
                         winning_trade = UT.winning_trade,
                         transaction_date = UT.transaction_date,
                         profit_target = UT.profit_target,
                         daily_loss_limit = UT.daily_loss_limit,
                         max_drowdown = UT.max_drowdown,
                         maintain_account_balance = UT.maintain_account_balance,
                         trade_required_no_of_days = UT.trade_required_no_of_days,
                         max_cons_win = UT.max_cons_win,
                         max_cons_loss = UT.max_cons_loss,
                         losing_trade_per = UT.losing_trade_per,
                         winning_trade_per = UT.winning_trade_per,
                         symbol = UT.symbol,
                         Exchange = UT.exchange,
                     });

                if (string.IsNullOrEmpty(Exchange))
                {
                    Exchange = "All Markets";
                }

                if (Exchange != "All Markets")
                {
                    query = query.Where(x => x.Exchange == Exchange);
                }

                NextSubId = user_subscription_id;
                if (query.Count() > 0)
                {
                    HomePageModel objTransactionModel;
                    foreach (var model in query)
                    {
                        objTransactionModel = new HomePageModel();

                        objTransactionModel.TransactionID = model.transaction_id;
                        objTransactionModel.UserSubscriptionID = model.user_subscription_id;
                        objTransactionModel.CurrentBalanceOld = decimal.Round(dbConnection.balance_history.Where(x => x.transaction_date == model.transaction_date && x.user_subscription_id == model.user_subscription_id).FirstOrDefault().current_balance.Value, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.CurrentBalance = (objTransactionModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.CurrentBalanceOld.Value).ToString("#,##0.00");
                        objTransactionModel.HighBalanceOld = decimal.Round(model.high_balance, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.HighBalance = (objTransactionModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighBalanceOld.Value).ToString("#,##0.00");
                        objTransactionModel.LowBalanceOld = decimal.Round(model.low_balance, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.LowBalance = (objTransactionModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowBalanceOld.Value).ToString("#,##0.00");
                        objTransactionModel.AvgWinningDayOld = decimal.Round(model.avg_winning_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.LowBalance = (objTransactionModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.AvgLosingDayOld = decimal.Round(model.avg_losing_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.AvgLosingDay = (objTransactionModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.BestDayOld = decimal.Round(model.best_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.BestDay = (objTransactionModel.BestDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.BestDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.WorstDayOld = decimal.Round(model.worst_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.WorstDay = (objTransactionModel.WorstDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.WorstDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.NetProfitLossOld = decimal.Round(model.net_profit_loss, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.NetProfitLoss = (objTransactionModel.NetProfitLossOld < 0 ? "-" : "") + "$" + Convert.ToDecimal(Math.Abs(objTransactionModel.NetProfitLossOld.Value)).ToString("#,##0.00");
                        objTransactionModel.HighProfitLossOld = decimal.Round(model.high_profit_loss, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.HighProfitLoss = (objTransactionModel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighProfitLossOld.Value).ToString("#,##0.00");
                        objTransactionModel.LowProfitLossOld = decimal.Round(model.low_profit_loss, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.LowProfitLoss = (objTransactionModel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowProfitLossOld.Value).ToString("#,##0.00");
                        objTransactionModel.TotalContracts = model.total_contracts;
                        objTransactionModel.TotalCommisionsOld = decimal.Round(model.total_commision, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.TotalCommisions = (objTransactionModel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.TotalCommisionsOld.Value).ToString("#,##0.00");
                        objTransactionModel.TotalTrades = model.total_trades;
                        objTransactionModel.AvgWinningTradeOld = decimal.Round(model.avg_winning_trade, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.AvgWinningTrade = (objTransactionModel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                        objTransactionModel.AvgLosingTradeOld = decimal.Round(model.avg_losing_trader, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.AvgLosingTrade = (objTransactionModel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                        objTransactionModel.WinningTradeOld = decimal.Round(model.winning_trade, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.WinningTrade = (objTransactionModel.WinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.WinningTradeOld.Value).ToString("#,##0.00");
                        objTransactionModel.TransactionDateString = model.transaction_date.ToString("MMM d");
                        objTransactionModel.ProfitTargetOld = decimal.Round(model.profit_target, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.ProfitTarget = (objTransactionModel.ProfitTargetOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.ProfitTargetOld.Value).ToString("#,##0.00");
                        objTransactionModel.DailyLossLimitOld = decimal.Round(model.daily_loss_limit, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.DailyLossLimit = (objTransactionModel.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.DailyLossLimitOld.Value).ToString("#,##0.00");
                        objTransactionModel.MaxDrowdownOld = decimal.Round(model.max_drowdown, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.MaxDrowdown = (objTransactionModel.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.MaxDrowdownOld.Value).ToString("#,##0.00");
                        objTransactionModel.MainAccountBalanceOld = decimal.Round(model.maintain_account_balance, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.MainAccountBalance = (objTransactionModel.MainAccountBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.MainAccountBalanceOld.Value).ToString("#,##0.00");
                        objTransactionModel.TradeRequiredNoOfDays = model.trade_required_no_of_days;
                        objTransactionModel.MaxConsWin = model.max_cons_win;
                        objTransactionModel.MaxConsLoss = model.max_cons_loss;
                        objTransactionModel.WinningTradePerOld = decimal.Round(model.winning_trade_per, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.WinningTradePer = (objTransactionModel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objTransactionModel.WinningTradePerOld.Value).ToString("#,##0.00");
                        objTransactionModel.LosingTradePerOld = decimal.Round(model.losing_trade_per, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.LosingTradePer = (objTransactionModel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objTransactionModel.LosingTradePerOld.Value).ToString("#,##0.00");
                        objTransactionModel.AvgLosingDayOld = decimal.Round(model.avg_losing_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.AvgLosingDay = (objTransactionModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.AvgWinningDayOld = decimal.Round(model.avg_winning_day, 2, MidpointRounding.AwayFromZero);
                        objTransactionModel.AvgWinningDay = (objTransactionModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningDayOld.Value).ToString("#,##0.00");
                        objTransactionModel.Exchange = model.Exchange;
                        objTransactionModel.Symbol = model.symbol;
                        objTransactionModel.Transaction_date = model.transaction_date;

                        double avg_winning_day = Convert.ToDouble(model.avg_winning_day);
                        TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);

                        int hh = time_avg_winning_day.Hours;
                        int mm = time_avg_winning_day.Minutes;
                        int ss = time_avg_winning_day.Seconds;
                        int ms = time_avg_winning_day.Milliseconds;


                        string str_avg_Winning_day = mm + ":" + ss + ":" + ms;

                        double avg_losing_day = Convert.ToDouble(model.avg_losing_day);
                        TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);

                        mm = time_avg_losing_day.Minutes;
                        hh = time_avg_losing_day.Hours;
                        ss = time_avg_losing_day.Seconds;
                        ms = time_avg_losing_day.Milliseconds;

                        string str_avg_losing_day = mm + ":" + ss + ":" + ms;
                        objTransactionModel.AvgWinningDay_str = str_avg_Winning_day == "0:0:0" ? "--:--:--" : str_avg_Winning_day;
                        objTransactionModel.AvgLosingDay_str = str_avg_losing_day == "0:0:0" ? "--:--:--" : str_avg_losing_day;
                        lstUserTransactionModel.Add(objTransactionModel);
                    }
                    //return lstUserTransactionModel;
                }

                //By Shoaib - for cumulative report in case of All Markets option
                if (Exchange == "All Markets")
                {
                    List<HomePageModel> result = lstUserTransactionModel
                    .GroupBy(x => x.Transaction_date)
                    .Select(cl => new HomePageModel
                    {
                        TransactionDateString = cl.First().TransactionDateString,
                        //Quantity = cl.Count().ToString(),
                        CurrentBalanceOld = cl.First().CurrentBalanceOld,
                        CurrentBalance = cl.First().CurrentBalance,
                        NetProfitLossOld = decimal.Round(cl.Sum(c => c.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        NetProfitLoss = (cl.Sum(c => c.NetProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.NetProfitLossOld.Value)).ToString("#,##0.00"),
                        HighProfitLossOld = decimal.Round(cl.Sum(c => c.HighProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        HighProfitLoss = (cl.Sum(c => c.HighProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.HighProfitLossOld.Value)).ToString("#,##0.00"),
                        LowProfitLossOld = decimal.Round(cl.Sum(c => c.LowProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        LowProfitLoss = (cl.Sum(c => c.LowProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.LowProfitLossOld.Value)).ToString("#,##0.00"),
                        TotalContracts = cl.Sum(c => c.TotalContracts),
                        TotalCommisionsOld = decimal.Round(cl.Sum(c => c.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero),
                        TotalCommisions = (cl.Sum(c => c.TotalCommisionsOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.TotalCommisionsOld.Value)).ToString("#,##0.00"),

                        TotalTrades = cl.Sum(c => c.TotalTrades),
                        AvgWinningTradeOld = decimal.Round(cl.Sum(c => c.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningTrade = (cl.Sum(c => c.AvgWinningTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningTradeOld.Value)).ToString("#,##0.00"),

                        AvgLosingTradeOld = decimal.Round(cl.Sum(c => c.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingTrade = (cl.Sum(c => c.AvgLosingTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingTradeOld.Value)).ToString("#,##0.00"),

                        WinningTradePerOld = cl.Average(c => c.WinningTradePerOld),
                        //WinningTradePerOld = decimal.Round(cl.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero),
                        WinningTradePer = (cl.Average(c => c.WinningTradePerOld) < 0 ? "-" : "") + Math.Abs(cl.Average(c => c.WinningTradePerOld.Value)).ToString("#,##0.00"),

                        MaxConsWin = cl.Sum(c => c.MaxConsWin),
                        MaxConsLoss = cl.Sum(c => c.MaxConsLoss),
                        AvgWinningDayOld = decimal.Round(cl.Average(c => c.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningDay = (cl.Sum(c => c.AvgWinningDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningDayOld.Value)).ToString("#,##0.00"),

                        AvgLosingDayOld = decimal.Round(cl.Average(c => c.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingDay = (cl.Sum(c => c.AvgLosingDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingDayOld.Value)).ToString("#,##0.00"),


                        AvgWinningDay_str = string.Empty,
                        AvgLosingDay_str = string.Empty,
                        UserSubscriptionID = user_subscription_id,//Added by Bharat
                    }).ToList();

                    List<HomePageModel> result1 = new List<HomePageModel>();
                    foreach (var item in result)
                    {
                        string StrTemp = "";
                        TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgWinningDayOld));
                        //item.AvgWinningDay_str = ts.ToString(@"hh\:mm\:ss");
                        StrTemp = ts.Minutes + ":" + ts.Seconds + ":" + ts.Milliseconds;
                        item.AvgWinningDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;

                        ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgLosingDayOld));
                        //item.AvgLosingDay_str = ts.ToString(@"hh\:mm\:ss");
                        StrTemp = ts.Minutes + ":" + ts.Seconds + ":" + ts.Milliseconds;
                        item.AvgLosingDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;

                        result1.Add(item);
                    }

                    lstUserTransactionModel = result1;
                }

                if (Exchange == "All Markets" && lstUserTransactionModel.Count() > 1)
                {
                    HomePageModel objmodel = new HomePageModel();
                    objmodel.TransactionDateString = "Daily Average";
                    objmodel.Exchange = string.Empty;
                    objmodel.Symbol = string.Empty;
                    objmodel.CurrentBalanceOld = decimal.Round(lstUserTransactionModel.Average(x => x.CurrentBalanceOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.CurrentBalance = (objmodel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.CurrentBalanceOld.Value).ToString("#,##0.00");
                    objmodel.NetProfitLossOld = decimal.Round(lstUserTransactionModel.Average(x => x.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.NetProfitLoss = (objmodel.NetProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.NetProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.HighProfitLossOld = decimal.Round(lstUserTransactionModel.Average(x => x.HighProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.HighProfitLoss = (objmodel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.LowProfitLossOld = decimal.Round(lstUserTransactionModel.Average(x => x.LowProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.LowProfitLoss = (objmodel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.TotalContracts = Convert.ToInt32(lstUserTransactionModel.Average(x => x.TotalContracts));
                    objmodel.TotalCommisionsOld = decimal.Round(lstUserTransactionModel.Average(x => x.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.TotalCommisions = (objmodel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objmodel.TotalTrades = Convert.ToInt32(lstUserTransactionModel.Average(x => x.TotalTrades));
                    objmodel.AvgWinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningTrade = (objmodel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.AvgLosingTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingTrade = (objmodel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTrade = (objmodel.WinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.WinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.MaxDrowdownOld = decimal.Round(lstUserTransactionModel.Average(x => x.MaxDrowdownOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.MaxDrowdown = (objmodel.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.MaxDrowdownOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTradePer = (objmodel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objmodel.LosingTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.LosingTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.LosingTradePer = (objmodel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.LosingTradePerOld.Value).ToString("#,##0.00");
                    objmodel.MaxConsWin = Convert.ToInt32(lstUserTransactionModel.Average(x => x.MaxConsWin));
                    objmodel.MaxConsLoss = Convert.ToInt32(lstUserTransactionModel.Average(x => x.MaxConsLoss));
                    //objmodel.LosingTradePer = decimal.Round(lstUserTransactionModel.Average(x => x.LosingTradePer.Value), 0, MidpointRounding.AwayFromZero);
                    //objmodel.WinningTradePer = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradePer.Value), 0, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingDay = (objmodel.AvgLosingDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWinningDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningDay = (objmodel.AvgWinningDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWLDuration = "Avg. W/LDuration";
                    double avg_winning_day = Convert.ToDouble(objmodel.AvgWinningDay);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);
                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = mm + ":" + ss + ":" + ms;
                    double avg_losing_day = Convert.ToDouble(objmodel.AvgLosingDay);

                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = mm + ":" + ss + ":" + ms;
                    objmodel.AvgWinningDay_str = str_avg_Winning_day == "0:0:0" ? "--:--:--" : str_avg_Winning_day;
                    objmodel.AvgLosingDay_str = str_avg_losing_day == "0:0:0" ? "--:--:--" : str_avg_losing_day;
                    objmodel.UserSubscriptionID = user_subscription_id;//Added by Bharat
                    lstUserTransactionModel.Insert(0, objmodel);
                    return lstUserTransactionModel;
                }
                else if (lstUserTransactionModel.Count() > 1)
                {
                    HomePageModel objmodel = new HomePageModel();
                    objmodel.TransactionDateString = "Average";
                    objmodel.Exchange = string.Empty;
                    objmodel.Symbol = string.Empty;
                    objmodel.NetProfitLossOld = decimal.Round(query.Average(x => x.net_profit_loss), 2, MidpointRounding.AwayFromZero);
                    objmodel.NetProfitLoss = (objmodel.NetProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.NetProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.HighProfitLossOld = decimal.Round(query.Average(x => x.high_profit_loss), 2, MidpointRounding.AwayFromZero);
                    objmodel.HighProfitLoss = (objmodel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.LowProfitLossOld = decimal.Round(query.Average(x => x.low_profit_loss), 2, MidpointRounding.AwayFromZero);
                    objmodel.LowProfitLoss = (objmodel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.TotalContracts = Convert.ToInt32(query.Average(x => x.total_contracts));
                    objmodel.TotalCommisionsOld = decimal.Round(query.Average(x => x.total_commision), 2, MidpointRounding.AwayFromZero);
                    objmodel.TotalCommisions = (objmodel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objmodel.TotalTrades = Convert.ToInt32(query.Average(x => x.total_trades));
                    objmodel.AvgWinningTradeOld = decimal.Round(query.Average(x => x.avg_winning_trade), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningTrade = (objmodel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.AvgLosingTradeOld = decimal.Round(query.Average(x => x.avg_losing_trader), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingTrade = (objmodel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradeOld = decimal.Round(query.Average(x => x.winning_trade), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTrade = (objmodel.WinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.WinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.MaxDrowdownOld = decimal.Round(query.Average(x => x.max_drowdown), 2, MidpointRounding.AwayFromZero);
                    objmodel.MaxDrowdown = (objmodel.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.MaxDrowdownOld.Value).ToString("#,##0.00");
                    //objmodel.WinningTradePerOld = decimal.Round(query.Average(x => x.winning_trade_per), 2, MidpointRounding.AwayFromZero);
                    //objmodel.WinningTradePer = (objmodel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objmodel.LosingTradePerOld = decimal.Round(query.Average(x => x.losing_trade_per), 2, MidpointRounding.AwayFromZero);
                    objmodel.LosingTradePer = (objmodel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.LosingTradePerOld.Value).ToString("#,##0.00");
                    objmodel.MaxConsWin = Convert.ToInt32(query.Average(x => x.max_cons_win));
                    objmodel.MaxConsLoss = Convert.ToInt32(query.Average(x => x.max_cons_loss));
                    objmodel.LosingTradePerOld = decimal.Round(query.Average(x => x.losing_trade_per), 0, MidpointRounding.AwayFromZero);
                    objmodel.LosingTradePer = (objmodel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.LosingTradePerOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradePerOld = decimal.Round(query.Average(x => x.winning_trade_per), 0, MidpointRounding.AwayFromZero);
                    objmodel.WinningTradePer = (objmodel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objmodel.AvgLosingDayOld = decimal.Round(query.Average(x => x.avg_losing_day), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingDay = (objmodel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWinningDayOld = decimal.Round(query.Average(x => x.avg_winning_day), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningDay = (objmodel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWLDuration = "Avg. W/LDuration";

                    double avg_winning_day = Convert.ToDouble(objmodel.AvgWinningDayOld);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);

                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = mm + ":" + ss + ":" + ms;
                    double avg_losing_day = Convert.ToDouble(objmodel.AvgLosingDayOld);

                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = mm + ":" + ss + ":" + ms;
                    objmodel.AvgWinningDay_str = str_avg_Winning_day == "0:0:0" ? "--:--:--" : str_avg_Winning_day;
                    objmodel.AvgLosingDay_str = str_avg_losing_day == "0:0:0" ? "--:--:--" : str_avg_losing_day; ;
                    objmodel.UserSubscriptionID = user_subscription_id;//Added by Bharat
                    //lstUserTransactionModel.Add(objmodel);            //by Bharat for ordering
                    lstUserTransactionModel.Insert(0, objmodel);
                    //lstUserTransactionModel = lstUserTransactionModel.OrderBy(x => x.TransactionDateString).ToList();
                    /*lstUserTransactionModel.Reverse();      *///Not Appropriate
                    return lstUserTransactionModel;
                }

                return lstUserTransactionModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get User Account Balance
        /// <summary>
        /// Get User Account Info & Balance
        /// </summary>
        /// <param name="userID"></param>
        ///  Date : 11/7/2017
        /// Dev By: Bharat Katua
        /// Client Feedback - 19July2018 - Cumulative figure for fields like High/Current/Low etc
        /// By Shoaib        
        /// <returns></returns>
        public HomePageModel GetUserAccountBalance(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                HomePageModel objModel = new HomePageModel();
                DateTime CurrentDate = System.DateTime.UtcNow.AddDays(-1);
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;

                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                var query = (from UT in dbConnection.balance_history
                             join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                             where US.user_id == userID & US.user_subscription_id == user_subscription_id & UT.transaction_date <= CurrentDate
                             orderby UT.transaction_date descending
                             select new
                             {
                                 UT
                                 //HighBalance = dbConnection.balance_history.Where(x=>x.i US.subscription_id==.Max(x => x.current_balance),
                                 //LowBalance = dbConnection.balance_history.Min(x => x.current_balance),
                                 //BestDay = dbConnection.balance_history.Max(x => x.net_pnl),
                                 //WorstDay = dbConnection.balance_history.Min(x => x.net_pnl),
                                 //totalWinningDay = dbConnection.balance_history.Where(x => x.trading_day_status == 1).Count(),
                                 //totalLosingDay = dbConnection.balance_history.Where(x => x.trading_day_status == 0).Count(),
                                 //totalWinningAmount = dbConnection.balance_history.Where(x => x.net_pnl > 0).Sum(x => x.net_pnl),
                                 //totalLossingAmount = dbConnection.balance_history.Where(x => x.net_pnl < 0).Sum(x => x.net_pnl)
                             }
                             ).ToList();
                if (query.Count > 0)
                {
                    decimal totalWinningDay = query.Where(x => x.UT.trading_day_status == 1).Count();
                    decimal totalLosingDay = query.Where(x => x.UT.trading_day_status == 0).Count();
                    decimal totalWinningAmount = Convert.ToDecimal(query.Where(x => x.UT.net_pnl > 0).Sum(x => x.UT.net_pnl));
                    decimal totalLossingAmount = Convert.ToDecimal(query.Where(x => x.UT.net_pnl < 0).Sum(x => x.UT.net_pnl));

                    objModel = new HomePageModel();
                    objModel.UserSubscriptionID = Convert.ToInt64(query.Select(x => x.UT.user_subscription_id).FirstOrDefault());
                    objModel.HighBalanceOld = decimal.Round(query.Max(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);
                    objModel.HighBalance = (objModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.HighBalanceOld.Value).ToString("#,##0");

                    if (query.Select(x => x.UT.current_balance).FirstOrDefault() != null)
                    {
                        objModel.CurrentBalanceOld = decimal.Round(query.Select(x => x.UT.current_balance).FirstOrDefault().Value, 0, MidpointRounding.AwayFromZero);
                        objModel.CurrentBalance = (objModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.CurrentBalanceOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.CurrentBalance = "$0";
                    }


                    //changed by shoaib 
                    //6Sep2017
                    //client feedback
                    //fail test: Account Balance -The "Low" account balance should read $100,000.
                    //The account never fell below that value but that was the starting balance and this should be reflected as 
                    //the lowest point of the trading.If there is a day where a lot of money is lost and the account balance 
                    //drops to $98,500, then this is the figure that will show here.

                    //objModel.LowBalance = decimal.Round(query.Min(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);
                    decimal LowBalance = 0;
                    LowBalance = decimal.Round(query.Min(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);

                    //commented on 17-07-2018 hardik 
                    //decimal SubscriptionStartingBalance = 0;
                    //SubscriptionStartingBalance = (from X1 in dbConnection.user_subscription
                    //                               join X2 in dbConnection.subscription_master on X1.subscription_id equals X2.subscription_id
                    //                               where X1.user_subscription_id == objModel.UserSubscriptionID
                    //                               select new
                    //                               { X2 }).FirstOrDefault().X2.starting_balance;
                    //if (LowBalance >= SubscriptionStartingBalance)
                    //{
                    //    objModel.LowBalanceOld = decimal.Round(SubscriptionStartingBalance, 0, MidpointRounding.AwayFromZero);
                    //    objModel.LowBalance = (objModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.LowBalanceOld.Value).ToString("#,##0");
                    //}
                    //else
                    //{
                    //    objModel.LowBalanceOld = decimal.Round(LowBalance, 0, MidpointRounding.AwayFromZero);
                    //    objModel.LowBalance = (objModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.LowBalanceOld.Value).ToString("#,##0");
                    //}
                    objModel.LowBalanceOld = decimal.Round(LowBalance, 0, MidpointRounding.AwayFromZero);
                    objModel.LowBalance = (objModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.LowBalanceOld.Value).ToString("#,##0");
                    //change end - 6sep2017


                    if (totalWinningDay != 0 && totalWinningAmount != 0)
                    {
                        objModel.AvgWinningDayOld = decimal.Round(totalWinningAmount / totalWinningDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgWinningDay = (objModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgWinningDayOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.AvgWinningDay = "$0";
                    }

                    objModel.BestDayOld = decimal.Round(query.Max(x => x.UT.net_pnl).Value, 0, MidpointRounding.AwayFromZero);
                    objModel.BestDay = (objModel.BestDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.BestDayOld.Value).ToString("#,##0");
                    if (totalLosingDay != 0 && totalLossingAmount != 0)
                    {
                        objModel.AvgLosingDayOld = decimal.Round(totalLossingAmount / totalLosingDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgLosingDay = (objModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgLosingDayOld.Value).ToString("#,##0");

                    }
                    else
                    {
                        objModel.AvgLosingDay = "$0";
                    }
                    objModel.WorstDayOld = decimal.Round(query.Min(x => x.UT.net_pnl).Value, 0, MidpointRounding.AwayFromZero);
                    objModel.WorstDay = (objModel.WorstDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.WorstDayOld.Value).ToString("#,##0");
                    return objModel;
                }
                else
                {
                    objModel = new HomePageModel();
                    objModel.HighBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.CurrentBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.LowBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgWinningDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.BestDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgLosingDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.WorstDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.UserSubscriptionID = user_subscription_id;
                }
                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// Function logic changed to fetch dashboard data (top 3 boxes) for specific user's all subscriptions, earlier it's 
        /// just current subscription
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Sequence"></param>
        /// <param name="userSubscriptionID"></param>
        /// <returns></returns>
        public HomePageModel GetUserAccountBalanceNew(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                HomePageModel objModel = new HomePageModel();
                DateTime CurrentDate = System.DateTime.UtcNow.AddDays(-1);
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;

                //Client Feedback - 19July2018 - by shoaib
                //Display High/Low/Current from all subscriptions of user not just current subscription
                //So Next/Prev functionality will not apply this function


                //newly added code - 19July2018 
                //In case of RESET consider stats for those user subscriptions which are purchased after reset user subscription

                var LstUserResets = dbConnection.reset_requests.Where(x => x.user_id == userID && x.reset_status == true).OrderByDescending(x => x.reset_request_id).ToList();

                var ObjUserSubscriptions = dbConnection.user_subscription.Where(x => x.user_id == userID).ToList();

                if (LstUserResets.Count() > 0)
                {
                    ObjUserSubscriptions.Where(x => x.user_subscription_id > LstUserResets.First().user_subscription_id);
                }

                List<long> ArrSubIds = new List<long>();

                if (ObjUserSubscriptions.Count() == 1 && ObjUserSubscriptions[0].free_trial == true)
                {
                    ArrSubIds.Add(ObjUserSubscriptions[0].user_subscription_id);
                }
                else if (ObjUserSubscriptions.Count() > 0)
                {
                    ArrSubIds = ObjUserSubscriptions.Where(x => x.free_trial == false).Select(x => x.user_subscription_id).ToList();
                }
                //newly added code - 19July2018 

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                var query = (from UT in dbConnection.balance_history
                             join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                             //where US.user_id == userID && US.user_subscription_id == user_subscription_id && UT.transaction_date <= CurrentDate
                             where US.user_id == userID && ArrSubIds.Contains(US.user_subscription_id) && UT.transaction_date <= CurrentDate
                             orderby UT.transaction_date descending
                             select new
                             {
                                 UT
                             }).ToList();
                if (query.Count > 0)
                {
                    decimal totalWinningDay = query.Where(x => x.UT.trading_day_status == 1).Count();
                    decimal totalLosingDay = query.Where(x => x.UT.trading_day_status == 0).Count();
                    decimal totalWinningAmount = Convert.ToDecimal(query.Where(x => x.UT.net_pnl > 0).Sum(x => x.UT.net_pnl));
                    decimal totalLossingAmount = Convert.ToDecimal(query.Where(x => x.UT.net_pnl < 0).Sum(x => x.UT.net_pnl));

                    objModel = new HomePageModel();
                    objModel.UserSubscriptionID = Convert.ToInt64(query.Select(x => x.UT.user_subscription_id).FirstOrDefault());
                    objModel.HighBalanceOld = decimal.Round(query.Max(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);
                    //added to stop negative high balance on 25-July-2018 hardik
                    objModel.HighBalanceOld = objModel.HighBalanceOld > 0 ? objModel.HighBalanceOld : 0;
                    objModel.HighBalance = (objModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.HighBalanceOld.Value).ToString("#,##0");

                    if (query.Select(x => x.UT.current_balance).FirstOrDefault() != null)
                    {
                        objModel.CurrentBalanceOld = decimal.Round(query.Select(x => x.UT.current_balance).FirstOrDefault().Value, 0, MidpointRounding.AwayFromZero);
                        objModel.CurrentBalance = (objModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.CurrentBalanceOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.CurrentBalance = "$0";
                    }


                    //changed by shoaib 
                    //6Sep2017
                    //client feedback
                    //fail test: Account Balance -The "Low" account balance should read $100,000.
                    //The account never fell below that value but that was the starting balance and this should be reflected as 
                    //the lowest point of the trading.If there is a day where a lot of money is lost and the account balance 
                    //drops to $98,500, then this is the figure that will show here.

                    //objModel.LowBalance = decimal.Round(query.Min(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);
                    decimal LowBalance = 0;
                    LowBalance = decimal.Round(query.Min(x => x.UT.current_balance).Value, 0, MidpointRounding.AwayFromZero);

                    objModel.LowBalanceOld = decimal.Round(LowBalance, 0, MidpointRounding.AwayFromZero);
                    //added to stop negative high balance on 25-July-2018 hardik
                    objModel.LowBalanceOld = objModel.LowBalanceOld < 0 ? objModel.LowBalanceOld : 0;
                    objModel.LowBalance = (objModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.LowBalanceOld.Value).ToString("#,##0");
                    //change end - 6sep2017


                    if (totalWinningDay != 0 && totalWinningAmount != 0)
                    {
                        objModel.AvgWinningDayOld = decimal.Round(totalWinningAmount / totalWinningDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgWinningDay = (objModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgWinningDayOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.AvgWinningDay = "$0";
                    }

                    objModel.BestDayOld = decimal.Round(query.Max(x => x.UT.net_pnl).Value, 0, MidpointRounding.AwayFromZero);
                    objModel.BestDay = (objModel.BestDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.BestDayOld.Value).ToString("#,##0");
                    if (totalLosingDay != 0 && totalLossingAmount != 0)
                    {
                        objModel.AvgLosingDayOld = decimal.Round(totalLossingAmount / totalLosingDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgLosingDay = (objModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgLosingDayOld.Value).ToString("#,##0");

                    }
                    else
                    {
                        objModel.AvgLosingDay = "$0";
                    }

                    objModel.WorstDayOld = decimal.Round(query.Min(x => x.UT.net_pnl).Value, 0, MidpointRounding.AwayFromZero);
                    objModel.WorstDay = (objModel.WorstDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.WorstDayOld.Value).ToString("#,##0");

                    return objModel;
                }
                else
                {
                    objModel = new HomePageModel();
                    objModel.HighBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.CurrentBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.LowBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgWinningDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.BestDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgLosingDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.WorstDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.UserSubscriptionID = user_subscription_id;
                }

                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #region Get User Account Info
        /// <summary>
        /// Get User Account Info 
        /// </summary>
        /// <param name="userID"></param>
        ///  Date : 14/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public AccountInfo GetAccountInfo(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                AccountInfo objModel = new AccountInfo();

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        switch (Sequence)
                        {
                            case "Prev":
                                user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                                break;
                            case "Next":
                                user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                                break;
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }



                var query = (from UM in dbConnection.user_master
                             join US in dbConnection.user_subscription on UM.user_id equals US.user_id
                             join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                             where US.user_subscription_id == user_subscription_id//US.user_id == userID & US.is_current == true
                             select new { SM, US, UM }).FirstOrDefault();
                if (query != null)
                {
                    objModel = new AccountInfo();
                    //objModel.account_info_id = query.AI.account_info_id;
                    objModel.user_subscription_id = query.US.user_subscription_id;
                    objModel.account_number = query.UM.account_number == null ? "NA" : query.UM.account_number;
                    objModel.subscription_name = query.SM.subscription_name == null ? "NA" : query.SM.subscription_name;
                    objModel.is_current = query.US.is_current;

                    objModel.substartdate = query.US.subscription_start_date == null ? "NA" : query.US.subscription_start_date.Value.ToString("dd MMM, yyyy");
                    objModel.subenddate = query.US.subscription_end_date == null ? "NA" : query.US.subscription_end_date.Value.ToString("dd MMM, yyyy");
                    objModel.is_expired = query.US.is_expired.Value;
                    if (query.US.free_trial != null)
                        objModel.is_free_trial = query.US.free_trial.Value;
                    else
                        objModel.is_free_trial = false;

                    if (query.US.subscription_start_date != null)
                    {
                        TimeSpan difference2 = DateTime.Now.Subtract(query.US.subscription_start_date.Value);
                        int completedTotalTays = Convert.ToInt32(difference2.TotalDays);

                        Double days = (query.US.subscription_end_date.Value - query.US.subscription_start_date.Value).TotalDays;
                        int totaldays = Convert.ToInt32(days);

                        if (completedTotalTays >= totaldays)
                        {
                            objModel.completeddays = 0;
                            UserService objUserService = new UserService();
                            objUserService.SetSubscriptionAsExpired(objModel.user_subscription_id.Value);
                        }
                        else
                        {
                            objModel.completeddays = Convert.ToInt32(difference2.TotalDays);
                        }
                    }
                    else
                    {
                        objModel.completeddays = 0;
                    }
                    //added by Dhaval  - 14-09-2017 //client feedback if transaction have no recoreds then its in active if more then one then its active
                    //var GetRec = (from db_User in dbConnection.user_master
                    //              join db_User_subscription in dbConnection.user_subscription on db_User.user_id equals db_User_subscription.user_id
                    //              join db_user_transactions in dbConnection.user_transactions on db_User_subscription.user_subscription_id equals db_user_transactions.user_subscription_id
                    //              where db_User.user_id == userID
                    //              select new { transactionID = db_user_transactions.transaction_id }).ToList();
                    int UserTrnCount = dbConnection.user_transactions.Where(x => x.user_subscription_id == objModel.user_subscription_id).ToList().Count;

                    if (objModel.is_expired == true)
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - TRIAL";
                        else
                            objModel.status = "INACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - DEMO";
                        else
                            objModel.status = "INACTIVE - CHALLENGE";

                        objModel.is_expired = true;
                    }
                    else if (objModel.is_expired == false && UserTrnCount > 0)
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "ACTIVE - TRIAL";
                        else
                            objModel.status = "ACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "ACTIVE - DEMO";
                        else
                            objModel.status = "ACTIVE - CHALLENGE";

                        objModel.is_expired = false;
                    }
                    else
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - TRIAL";
                        else
                            objModel.status = "INACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - DEMO";
                        else
                            objModel.status = "INACTIVE - CHALLENGE";

                        objModel.is_expired = true;
                    }


                    //if (objModel.is_expired == true)
                    //    objModel.status = "Expired";
                    //else
                    //    objModel.status = "Active";                    


                    //added by shoaib - 6Sep2017
                    //client feedback
                    //for getting winning day percentage 
                    var queryWinningDay = (from UM in dbConnection.balance_history
                                           where UM.user_subscription_id == objModel.user_subscription_id
                                           select new { UM }).ToList();

                    float WinningDays = 0, LosingDays = 0, NeutralDays = 0, TotalDays = 0;
                    float WinningDayPer = 0;

                    if (queryWinningDay.Count() > 0)
                    {
                        WinningDays = queryWinningDay.Where(x => x.UM.trading_day_status == 1).ToList().Count();
                        LosingDays = queryWinningDay.Where(x => x.UM.trading_day_status == 0).ToList().Count();
                        NeutralDays = queryWinningDay.Where(x => x.UM.trading_day_status == 2).ToList().Count();
                        TotalDays = WinningDays + LosingDays;

                        WinningDayPer = (WinningDays * 100) / TotalDays;
                    }
                    decimal WinningDayPerRound = decimal.Round(Convert.ToDecimal(WinningDayPer), 2, MidpointRounding.AwayFromZero);
                    objModel.WinnigDayPer = WinningDayPerRound.ToString();


                    return objModel;
                }
                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        #endregion

        /// <summary>
        /// Changed function according to client feedback - 20July2018
        /// By shoaib
        /// Earlier we are displaying stats first row (3 boxs) according to currently selected user subscription
        /// Now it's cumulative and it will not be changed on Next/Previous buttons, so created new method for the same and old 
        /// one is considered as backup
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Sequence"></param>
        /// <param name="userSubscriptionID"></param>
        /// <returns></returns>
        public AccountInfo GetAccountInfoNew(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0, string sAccountAllias = "")
        {
            try
            {
                AccountInfo objModel = new AccountInfo();

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;

                user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();

                var query = (from UM in dbConnection.user_master
                             join US in dbConnection.user_subscription on UM.user_id equals US.user_id
                             join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                             where US.user_subscription_id == user_subscription_id//US.user_id == userID & US.is_current == true
                             select new { SM, US, UM }).FirstOrDefault();
                if (query != null)
                {
                    objModel = new AccountInfo();
                    // Newly added code - 20July2018 
                    // Client comment 
                    // INSTEAD OF DAYS COMPLETED, Add new Statistic "Started Challenge" which shows the date of 
                    // First Payment OR if, there was an Account Reset, the Date of that latest RESET.
                    //Started Challenge - New column
                    var LstUserResets = dbConnection.reset_requests.Where(x => x.user_id == userID && x.reset_status == true).OrderByDescending(x => x.reset_request_id).ToList();

                    if (LstUserResets.Count() > 0)
                    {
                        objModel.challengestartdate = LstUserResets.First().resetted_date == null ? "NA" : LstUserResets.First().resetted_date.Value.ToString("dd MMM, yyyy");
                    }
                    else
                    {
                        var LstUserSubscriptions = dbConnection.user_subscription.Where(x => x.user_id == userID).OrderBy(x => x.user_subscription_id).ToList();
                        DateTime? TmpStartDate;
                        if (LstUserSubscriptions.Where(x => x.free_trial == false).Count() > 0)
                        {
                            TmpStartDate = LstUserSubscriptions.Where(x => x.free_trial == false).First().subscription_start_date;
                            objModel.challengestartdate = TmpStartDate == null ? "NA" : TmpStartDate.Value.ToString("dd MMM, yyyy");
                        }
                        else
                        {
                            TmpStartDate = LstUserSubscriptions.Where(x => x.free_trial == true).First().subscription_start_date;
                            objModel.challengestartdate = TmpStartDate == null ? "NA" : TmpStartDate.Value.ToString("dd MMM, yyyy");
                        }
                    }
                    //Newly added code - 20July2018 
                    //objModel.account_info_id = query.AI.account_info_id;
                    objModel.user_subscription_id = query.US.user_subscription_id;
                    objModel.account_number = query.UM.account_number == null ? "NA" : query.UM.account_number;
                    objModel.broker_id = query.UM.trading_platform.Value;
                    objModel.broker_name = query.UM.trading_platform == 1 ? "Rithmic" : "Trading Technologies";
                    objModel.subscription_name = query.SM.subscription_name == null ? "NA" : query.SM.subscription_name;
                    objModel.is_current = query.US.is_current;
                    objModel.is_free_trial = query.US.free_trial;
                    objModel.subscription_start_date = query.US.subscription_start_date == null ? DateTime.UtcNow : query.US.subscription_start_date.Value;
                    objModel.subscription_trial_date = query.US.subscription_start_date == null ? DateTime.UtcNow : query.US.subscription_start_date.Value.Date.AddDays(15);
                    objModel.subscription_end_date = query.US.subscription_end_date == null ? DateTime.UtcNow : query.US.subscription_end_date.Value;
                    objModel.substartdate = query.US.subscription_start_date == null ? "NA" : query.US.subscription_start_date.Value.ToString("dd MMM, yyyy");
                    objModel.subenddate = query.US.subscription_end_date == null ? "NA" : query.US.subscription_end_date.Value.ToString("dd MMM, yyyy");
                    objModel.is_expired = query.US.is_expired.Value;


                    if (query.US.subscription_start_date != null)
                    {
                        TimeSpan difference2 = DateTime.Now.Subtract(query.US.subscription_start_date.Value);
                        int completedTotalTays = Convert.ToInt32(difference2.TotalDays);

                        Double days = (query.US.subscription_end_date.Value - query.US.subscription_start_date.Value).TotalDays;
                        int totaldays = Convert.ToInt32(days);

                        if (completedTotalTays >= totaldays)
                        {
                            objModel.completeddays = 0;
                            UserService objUserService = new UserService();
                            objUserService.SetSubscriptionAsExpired(objModel.user_subscription_id.Value);
                        }
                        else
                        {
                            objModel.completeddays = Convert.ToInt32(difference2.TotalDays);
                        }
                    }
                    else
                    {
                        objModel.completeddays = 0;
                    }

                    //added by Dhaval  - 14-09-2017 //client feedback if transaction have no recoreds then its in active if more then one then its active
                    //var GetRec = (from db_User in dbConnection.user_master
                    //              join db_User_subscription in dbConnection.user_subscription on db_User.user_id equals db_User_subscription.user_id
                    //              join db_user_transactions in dbConnection.user_transactions on db_User_subscription.user_subscription_id equals db_user_transactions.user_subscription_id
                    //              where db_User.user_id == userID
                    //              select new { transactionID = db_user_transactions.transaction_id }).ToList();
                    int UserTrnCount = dbConnection.user_transactions.Where(x => x.user_subscription_id == objModel.user_subscription_id).ToList().Count;

                    if (objModel.is_expired == true)
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - TRIAL";
                        else
                            objModel.status = "INACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - DEMO";
                        else
                            objModel.status = "INACTIVE - CHALLENGE";

                        objModel.is_expired = true;
                    }
                    //not necessory to check user traded or not 
                    //26-07-2018 hardik
                    else if (objModel.is_expired == false)
                    //else if (objModel.is_expired == false && UserTrnCount > 0)
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "ACTIVE - TRIAL";
                        else
                            objModel.status = "ACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "ACTIVE - DEMO";
                        else
                            objModel.status = "ACTIVE - CHALLENGE";

                        objModel.is_expired = false;
                    }
                    else
                    {
                        //Text changed after david suggestion - 19July2018
                        /*if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - TRIAL";
                        else
                            objModel.status = "INACTIVE - LIVE";*/

                        if (query.US.subscription_id == 8)
                            objModel.status = "INACTIVE - DEMO";
                        else
                            objModel.status = "INACTIVE - CHALLENGE";

                        objModel.is_expired = true;
                    }

                    //if (objModel.is_expired == true)
                    //    objModel.status = "Expired";
                    //else
                    //    objModel.status = "Active"; 

                    //added by shoaib - 6Sep2017
                    //client feedback
                    //for getting winning day percentage 
                    var queryWinningDay = (from UM in dbConnection.balance_history
                                           where UM.user_subscription_id == objModel.user_subscription_id
                                           select new { UM }).ToList();

                    float WinningDays = 0, LosingDays = 0, NeutralDays = 0, TotalDays = 0;
                    float WinningDayPer = 0;

                    if (queryWinningDay.Count() > 0)
                    {
                        WinningDays = queryWinningDay.Where(x => x.UM.trading_day_status == 1).ToList().Count();
                        LosingDays = queryWinningDay.Where(x => x.UM.trading_day_status == 0).ToList().Count();
                        NeutralDays = queryWinningDay.Where(x => x.UM.trading_day_status == 2).ToList().Count();
                        TotalDays = WinningDays + LosingDays;

                        WinningDayPer = (WinningDays * 100) / TotalDays;
                    }

                    decimal WinningDayPerRound = decimal.Round(Convert.ToDecimal(WinningDayPer), 2, MidpointRounding.AwayFromZero);
                    objModel.WinnigDayPer = WinningDayPerRound.ToString();

                    return objModel;
                }
                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        #region Get Rule 
        /// <summary>
        /// Get Rule & Calculation
        /// </summary>
        /// <param name="userID"></param>
        ///  Date : 14/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public Entity.MemberModel.RuleModel GetRule(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                Entity.MemberModel.RuleModel model = new Entity.MemberModel.RuleModel();


                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }



                //var query = (from UT in dbConnection.user_transactions
                //             join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                //             where US.user_id == userID & US.is_current == true
                //             select new { UT }).FirstOrDefault();
                var query = (from BH in dbConnection.balance_history
                             join US in dbConnection.user_subscription on BH.user_subscription_id equals US.user_subscription_id
                             join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                             where US.user_subscription_id == user_subscription_id
                             select new { BH, SM }).OrderByDescending(x => x.BH.balance_history_id).FirstOrDefault();

                var QueryTradingCount = (from BH in dbConnection.balance_history
                                         join US in dbConnection.user_subscription on BH.user_subscription_id equals US.user_subscription_id
                                         where US.user_id == userID & US.is_current == true
                                         select new { BH }).ToList();

                if (query != null)
                {
                    model = new Entity.MemberModel.RuleModel();
                    //By shoaib for Getting Rule Values from user current Subscription - 15-Sep-2017
                    model.user_subscription_id = user_subscription_id;
                    model.ruleprofit_targetOld = query.SM.profite_target;
                    model.ruleprofit_target = (model.ruleprofit_targetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruleprofit_targetOld.Value).ToString("#,##0");
                    model.ruledaily_loss_limitOld = query.SM.daily_loss_limit;
                    model.ruledaily_loss_limit = (model.ruledaily_loss_limitOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruledaily_loss_limitOld.Value).ToString("#,##0");
                    model.rulemax_drowdownOld = query.SM.max_drawdown;
                    model.rulemax_drowdown = (model.rulemax_drowdownOld < 0 ? "-" + "$" + Math.Abs(model.rulemax_drowdownOld.Value).ToString("#,##0") : "0");
                    model.rulemaintain_account_balanceOld = query.SM.starting_balance;
                    model.rulemaintain_account_balance = (model.rulemaintain_account_balanceOld < 0 ? "-" : "") + "$" + Math.Abs(model.rulemaintain_account_balanceOld.Value).ToString("#,##0");
                    model.ruletrade_required_no_of_days = 15;
                    /*rule_master ObjModel = dbConnection.rule_master.FirstOrDefault();
                    model.ruleprofit_target = (objModel.profit_target == null ? 0 : ObjModel.profit_target;
                    model.ruleprofit_target = decimal.Round(model.ruleprofit_target.Value, 0, MidpointRounding.AwayFromZero);
                    model.ruledaily_loss_limit = (objModel.daily_loss_limit == null ? 0 : ObjModel.daily_loss_limit;
                    model.ruledaily_loss_limit = decimal.Round(model.ruledaily_loss_limit.Value, 0, MidpointRounding.AwayFromZero);
                    model.rulemax_drowdown = (objModel.max_drowdown == null ? 0 : ObjModel.max_drowdown;
                    model.rulemax_drowdown = decimal.Round(model.rulemax_drowdown.Value, 0, MidpointRounding.AwayFromZero);
                    //decimal.Round(query.Select(x => x.UT.current_balance).FirstOrDefault().Value, 0, MidpointRounding.AwayFromZero);
                    model.rulemaintain_account_balance = (objModel.maintain_account_balance == null ? 0 : ObjModel.maintain_account_balance;
                    model.rulemaintain_account_balance = decimal.Round(model.rulemaintain_account_balance.Value, 0, MidpointRounding.AwayFromZero);
                    model.ruletrade_required_no_of_days = (objModel.trade_required_no_of_days == null ? 0 : ObjModel.trade_required_no_of_days;
                    model.ruletrade_required_no_of_days = decimal.Round(model.ruletrade_required_no_of_days.Value, 0, MidpointRounding.AwayFromZero);*/
                    #region for Cumulative Profit Target
                    //var firstDay = DateTime.UtcNow;
                    var cumulativequery =
                    from UT in dbConnection.balance_history
                    join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                    where US.user_subscription_id == user_subscription_id /*US.user_id == userID & US.is_current == true*/ & UT.transaction_date.Value >= DbFunctions.TruncateTime(US.subscription_start_date) & UT.transaction_date.Value <= DbFunctions.TruncateTime(US.subscription_end_date)
                    orderby UT.transaction_date ascending
                    select new
                    {
                        transaction_day = UT.transaction_date,
                        net_pnl = UT.net_pnl,
                    };

                    decimal? CumulativeProfitTarget = 0;

                    if (cumulativequery != null)
                    {
                        List<decimal> CumulativeList = cumulativequery.Select(x => x.net_pnl.Value).ToList();
                        CumulativeProfitTarget = CumulativeList.Sum();
                    }
                    #endregion

                    if (CumulativeProfitTarget.Value >= 0)
                    {
                        model.ProfitTargetOld = CumulativeProfitTarget;
                        model.ProfitTarget = (model.ProfitTargetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ProfitTargetOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.ProfitTarget = "$0";
                    }


                    //if (query.BH.net_pnl.Value >= 0)
                    //{
                    //    model.ProfitTarget = decimal.Round(query.BH.net_pnl.Value, 0, MidpointRounding.AwayFromZero);
                    //}
                    //else
                    //{
                    //    model.ProfitTarget = 0;
                    //}
                    if (query.BH.net_pnl.Value < 0)
                    {
                        model.DailyLossLimitOld = decimal.Round(query.BH.net_pnl.Value, 0, MidpointRounding.AwayFromZero);
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.DailyLossLimit = "$0";
                    }

                    //Developer - Shoaib M
                    //6 Sep 2017
                    //Client Feedback
                    //Max Drawdown is the cumulative loss on the account. In this test scenario, my balance is higher than my starting balance. 
                    //I did not lose money. Therefore, the max drawdown should be zero. And the green checkmark should therefore be showing, not the red X. 
                    //decimal MAxDrowdown = dbConnection.balance_history.OrderBy(x => x.net_pnl).Select(x => x.net_pnl).FirstOrDefault().Value;
                    long UserSubscriptionId = query.BH.user_subscription_id.Value;
                    decimal? MAxDrowdown = dbConnection.balance_history.Where(x => x.net_pnl < 0 && x.user_subscription_id == UserSubscriptionId).ToList().Sum(x => x.net_pnl);
                    model.MaxDrowdownOld = decimal.Round(MAxDrowdown.Value, 0, MidpointRounding.AwayFromZero);
                    model.MaxDrowdown = (model.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(model.MaxDrowdownOld.Value).ToString("#,##0");
                    if (query.BH.cash_balance.Value > 0)
                    {
                        model.MainAccountBalanceOld = decimal.Round(query.BH.cash_balance.Value, 0, MidpointRounding.AwayFromZero);
                        model.MainAccountBalance = (model.MainAccountBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(model.MainAccountBalanceOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.MainAccountBalance = "$0";
                    }

                    //Developer - Shoaib M
                    //6 Sep 2017
                    //Client Feedback
                    //Trade Number of Required Days: I traded only one day on this account so rather than "70" in test column, 
                    //it should read "1" and therefore the red X should be showing. 
                    //The green checkmark should only show when the number of test days traded is equal to or greater than the number 
                    //in the Required field(10)
                    if (QueryTradingCount.Count > 0)
                    {
                        model.TradeRequiredNoOfDays = QueryTradingCount.Count;
                    }
                    else
                    {
                        model.TradeRequiredNoOfDays = 0;
                    }

                    //For Profit Target Check Mark
                    if (model.ProfitTargetOld <= model.ruleprofit_targetOld)
                    {
                        model.profit_targetachievestatus = "cancel";
                    }
                    else
                    {
                        model.profit_targetachievestatus = "check_circle";
                    }

                    //For Daily Loss Limit Check Mark
                    if (model.DailyLossLimitOld <= 0)
                    {
                        model.DailyLossLimitOld = model.DailyLossLimitOld * -1;
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }
                    if (model.DailyLossLimitOld <= model.ruledaily_loss_limitOld)
                    {
                        model.daily_loss_limitachievestatus = "check_circle";
                    }
                    else
                    {
                        model.daily_loss_limitachievestatus = "cancel";
                    }

                    //For Max Drowdown Check Mark
                    if (model.MaxDrowdownOld <= 0)
                    {
                        model.MaxDrowdownOld = model.MaxDrowdownOld * -1;
                        model.MaxDrowdown = (model.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(model.MaxDrowdownOld.Value).ToString("#,##0");
                    }
                    if (model.MaxDrowdownOld <= model.rulemax_drowdownOld)
                    {
                        model.max_drowdownachievestatus = "check_circle";
                    }
                    else
                    {
                        model.max_drowdownachievestatus = "cancel";
                    }

                    //For Maintain Account Balance Check Mark
                    if (model.MainAccountBalanceOld >= model.rulemaintain_account_balanceOld)
                    {
                        model.maintain_account_balanceachievestatus = "check_circle";
                    }
                    else
                    {
                        model.maintain_account_balanceachievestatus = "cancel";
                    }

                    //For Trades Per Day Check Mark
                    if (model.TradeRequiredNoOfDays <= model.ruletrade_required_no_of_days)
                    {
                        model.trade_required_no_of_daysachievestatus = "cancel";
                    }
                    else
                    {
                        model.trade_required_no_of_daysachievestatus = "check_circle";
                    }
                }
                else
                {
                    var query2 = (from US in dbConnection.user_subscription
                                  join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                                  where US.user_subscription_id == user_subscription_id
                                  select new { US, SM }).OrderByDescending(x => x.SM.subscription_id).FirstOrDefault();

                    if (query2 != null)
                    {
                        model = new Entity.MemberModel.RuleModel();
                        model.user_subscription_id = user_subscription_id;
                        model.ruleprofit_target = "$" + Math.Abs(query2.SM.profite_target).ToString("#,##0");
                        model.ruledaily_loss_limit = "$" + Math.Abs(query2.SM.daily_loss_limit.Value).ToString("#,##0");
                        model.rulemax_drowdown = "$" + Math.Abs(query2.SM.max_drawdown.Value).ToString("#,##0");
                        model.rulemaintain_account_balance = "$" + Math.Abs(query2.SM.starting_balance).ToString("#,##0");
                        //model.ProfitTarget = "-";
                        //model.DailyLossLimit = "-";
                        //model.MaxDrowdown = "-";
                        //model.MainAccountBalance = "-";
                        model.ruletrade_required_no_of_days = 10;
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// Changed function according to client feedback - 20July2018
        /// By shoaib
        /// Earlier we are displaying stats first row (3 boxs) according to currently selected user subscription
        /// Now it's cumulative and it will not be changed on Next/Previous buttons, so created new method for the same and old 
        /// one is considered as backup
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Sequence"></param>
        /// <param name="userSubscriptionID"></param>
        /// <returns></returns>
        public Entity.MemberModel.RuleModel GetRuleNew(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0)
        {
            try
            {
                Entity.MemberModel.RuleModel model = new Entity.MemberModel.RuleModel();

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;

                //Newly added code - 20July2018 
                //In case of reset consider stats for those user subscriptions which are purchased after reset user subscription

                var LstUserResets = dbConnection.reset_requests.Where(x => x.user_id == userID && x.reset_status == true).OrderByDescending(x => x.reset_request_id).ToList();

                var ObjUserSubscriptions = dbConnection.user_subscription.Where(x => x.user_id == userID).ToList();

                if (LstUserResets.Count() > 0)
                {
                    ObjUserSubscriptions.Where(x => x.user_subscription_id > LstUserResets.First().user_subscription_id);
                }

                List<long> ArrSubIds = new List<long>();

                if (ObjUserSubscriptions.Count() == 1 && ObjUserSubscriptions[0].free_trial == true)
                {
                    ArrSubIds.Add(ObjUserSubscriptions[0].user_subscription_id);
                }
                else if (ObjUserSubscriptions.Count() > 0)
                {
                    ArrSubIds = ObjUserSubscriptions.Where(x => x.free_trial == false).Select(x => x.user_subscription_id).ToList();
                }
                //Newly added code - 20July2018 

                var query = (from BH in dbConnection.balance_history
                             join US in dbConnection.user_subscription on BH.user_subscription_id equals US.user_subscription_id
                             join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                             where ArrSubIds.Contains(US.user_subscription_id)
                             select new { BH, SM }).OrderByDescending(x => x.BH.balance_history_id).ToList();

                // Unnecessary query - we can get the trading count from the above query too
                // Commented by - Shoaib - 20July2018
                //var QueryTradingCount = (from BH in dbConnection.balance_history
                //                         join US in dbConnection.user_subscription on BH.user_subscription_id equals US.user_subscription_id
                //                         where US.user_id == userID & US.is_current == true
                //                         select new { BH }).ToList();

                if (query.Count > 0)
                {
                    model = new Entity.MemberModel.RuleModel();
                    //By shoaib for Getting Rule Values from user current Subscription - 15-Sep-2017
                    model.user_subscription_id = ArrSubIds[0];
                    model.ruleprofit_targetOld = query.First().SM.profite_target;
                    model.ruleprofit_target = (model.ruleprofit_targetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruleprofit_targetOld.Value).ToString("#,##0");
                    model.ruledaily_loss_limitOld = query.First().SM.daily_loss_limit;
                    model.ruledaily_loss_limit = (model.ruledaily_loss_limitOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruledaily_loss_limitOld.Value).ToString("#,##0");
                    model.rulemax_drowdownOld = query.First().BH.auto_liquidate_threshold_value;
                    model.rulemax_drowdown = (model.rulemax_drowdownOld < 0 ? "-" : "") + "$" + Math.Abs(model.rulemax_drowdownOld.Value).ToString("#,##0");
                    model.rulemaintain_account_balanceOld = query.First().SM.starting_balance;
                    model.rulemaintain_account_balance = (model.rulemaintain_account_balanceOld < 0 ? "-" : "") + "$" + Math.Abs(model.rulemaintain_account_balanceOld.Value).ToString("#,##0");
                    model.ruletrade_required_no_of_days = 15;
                    model.MaxDrowdownOld = decimal.Round(query.First().BH.current_balance.Value, 0, MidpointRounding.AwayFromZero);
                    model.MaxDrowdown = (model.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(query.First().BH.current_balance.Value).ToString("#,##0");

                    #region for Cumulative Profit Target

                    //var firstDay = DateTime.UtcNow;

                    var cumulativequery = from UT in dbConnection.balance_history
                                          join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                                          where ArrSubIds.Contains(US.user_subscription_id)
                                          orderby UT.transaction_date ascending
                                          select new
                                          {
                                              transaction_day = UT.transaction_date,
                                              net_pnl = UT.net_pnl,
                                              current_balance = UT.current_balance,
                                          };

                    decimal? CumulativeProfitTarget = 0;

                    if (cumulativequery != null)
                    {
                        List<decimal> CumulativeList = cumulativequery.Select(x => x.net_pnl.Value).ToList();
                        CumulativeProfitTarget = CumulativeList.Sum();
                    }
                    #endregion

                    if (CumulativeProfitTarget.Value >= 0)
                    {
                        model.ProfitTargetOld = CumulativeProfitTarget;
                        model.ProfitTarget = (model.ProfitTargetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ProfitTargetOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.ProfitTarget = "$0";
                    }

                    decimal? LowestPNL = cumulativequery.ToList().Min(x => x.net_pnl);
                    if (LowestPNL != null && LowestPNL.Value < 0)
                    {
                        model.DailyLossLimitOld = decimal.Round(LowestPNL.Value, 0, MidpointRounding.AwayFromZero);
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.DailyLossLimit = "$0";
                    }

                    // Developer - Shoaib M
                    // 6 Sep 2017
                    // Client Feedback
                    // Max Drawdown is the cumulative loss on the account. In this test scenario, my balance is higher than my starting balance. 
                    // I did not lose money. Therefore, the max drawdown should be zero. And the green checkmark should therefore be showing, not the red X. 
                    // decimal MAxDrowdown = dbConnection.balance_history.OrderBy(x => x.net_pnl).Select(x => x.net_pnl).FirstOrDefault().Value;

                    // New comments from David - 20July2018
                    // I think the only way that we can calculate is...yesterday's account balance figure + today's Low P & l
                    // That will show the point at which the account balance was at its lowest.

                    //03-Aug-2018 Hardik
                    //moved to main query for fetched current balance

                    //
                    //var ObjUserTrans = from UT in dbConnection.user_transactions
                    //                   join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                    //                   where ArrSubIds.Contains(US.user_subscription_id)
                    //                   orderby UT.transaction_id descending
                    //                   select UT;

                    //decimal? TodayPNL = 0, YesterdayAccBal = 0;
                    //if (ObjUserTrans.ToList().Count() > 0)
                    //{
                    //    TodayPNL = ObjUserTrans.First().low_profit_loss;
                    //}

                    //if (cumulativequery.ToList().Count >= 2)
                    //{
                    //    YesterdayAccBal = cumulativequery.ToList()[1].current_balance.Value;
                    //}

                    //decimal ? MAxDrowdown = cumulativequery.First().current_balance;

                    //decimal? MAxDrowdown = YesterdayAccBal + TodayPNL;
                    //model.MaxDrowdownOld = decimal.Round(MAxDrowdown.Value, 0, MidpointRounding.AwayFromZero);
                    //model.MaxDrowdown = (model.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(model.MaxDrowdownOld.Value).ToString("#,##0");

                    // Field removed on client suggestion 
                    //if (query.BH.cash_balance.Value > 0)
                    //{
                    //    model.MainAccountBalanceOld = decimal.Round(query.BH.cash_balance.Value, 0, MidpointRounding.AwayFromZero);
                    //    model.MainAccountBalance = (model.MainAccountBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(model.MainAccountBalanceOld.Value).ToString("#,##0");
                    //}
                    //else
                    //{
                    //    model.MainAccountBalance = "$0";
                    //}

                    //Developer - Shoaib M
                    //6 Sep 2017
                    //Client Feedback
                    //Trade Number of Required Days: I traded only one day on this account so rather than "70" in test column, 
                    //it should read "1" and therefore the red X should be showing. 
                    //The green checkmark should only show when the number of test days traded is equal to or greater than the number 
                    //in the Required field(10)
                    if (query.Count > 0)
                    {
                        model.TradeRequiredNoOfDays = query.Count;
                    }
                    else
                    {
                        model.TradeRequiredNoOfDays = 0;
                    }

                    //For Profit Target Check Mark
                    if (model.ProfitTargetOld <= model.ruleprofit_targetOld)
                    {
                        model.profit_targetachievestatus = "cancel";
                    }
                    else
                    {
                        model.profit_targetachievestatus = "check_circle";
                    }

                    //For Daily Loss Limit Check Mark
                    if (model.DailyLossLimitOld <= 0)
                    {
                        model.DailyLossLimitOld = model.DailyLossLimitOld * -1;
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }

                    if (model.DailyLossLimitOld <= model.ruledaily_loss_limitOld)
                    {
                        model.daily_loss_limitachievestatus = "check_circle";
                    }
                    else
                    {
                        model.daily_loss_limitachievestatus = "cancel";
                    }

                    //For Max Drowdown Check Mark
                    //if (model.MaxDrowdownOld <= 0)
                    //{
                    //    //not need abs showing direct current balance
                    //  //  model.MaxDrowdownOld = model.MaxDrowdownOld * -1;
                    //    model.MaxDrowdown =  "$" + model.MaxDrowdownOld.Value.ToString("#,##0");
                    //}

                    if (model.MaxDrowdownOld >= model.rulemax_drowdownOld)
                    {
                        model.max_drowdownachievestatus = "check_circle";
                    }
                    else
                    {
                        model.max_drowdownachievestatus = "cancel";
                    }

                    //For Maintain Account Balance Check Mark
                    if (model.MainAccountBalanceOld >= model.rulemaintain_account_balanceOld)
                    {
                        model.maintain_account_balanceachievestatus = "check_circle";
                    }
                    else
                    {
                        model.maintain_account_balanceachievestatus = "cancel";
                    }

                    //For Trades Per Day Check Mark
                    if (model.TradeRequiredNoOfDays <= model.ruletrade_required_no_of_days)
                    {
                        model.trade_required_no_of_daysachievestatus = "cancel";
                    }
                    else
                    {
                        model.trade_required_no_of_daysachievestatus = "check_circle";
                    }
                }
                else
                {
                    var query2 = (from US in dbConnection.user_subscription
                                  join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                                  where US.user_subscription_id == user_subscription_id
                                  select new { US, SM }).OrderByDescending(x => x.SM.subscription_id).FirstOrDefault();

                    if (query2 != null)
                    {
                        model = new Entity.MemberModel.RuleModel();
                        model.user_subscription_id = user_subscription_id;
                        model.ruleprofit_target = "$" + Math.Abs(query2.SM.profite_target).ToString("#,##0");
                        model.ruledaily_loss_limit = "$" + Math.Abs(query2.SM.daily_loss_limit.Value).ToString("#,##0");
                        model.rulemax_drowdown = "$" + Math.Abs(query2.SM.max_drawdown.Value).ToString("#,##0");
                        model.rulemaintain_account_balance = "$" + Math.Abs(query2.SM.starting_balance).ToString("#,##0");
                        //model.ProfitTarget = "-";
                        //model.DailyLossLimit = "-";
                        //model.MaxDrowdown = "-";
                        //model.MainAccountBalance = "-";
                        model.ruletrade_required_no_of_days = 10;
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Get Current Balance List
        /// <summary>
        /// Get Current Balance List with filters
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 17/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<CurrentbalChart>> GetCurrentBalanceList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0)
        {
            ServiceResponse<List<CurrentbalChart>> response = new ServiceResponse<List<CurrentbalChart>>();
            List<CurrentbalChart> payments = new List<CurrentbalChart>();
            List<CurrentbalChart> revisePayments = new List<CurrentbalChart>();
            //CurrentbalChartRewise objModel;
            try
            {
                string ChartType = null;
                //var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).OrderByDescending(x => x.user_subscription_id).Select(x => x.user_subscription_id).ToList();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                var is_prev = true;
                var is_next = true;
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;

                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                            if (user_subscription_id == 0)
                            {
                                is_prev = true;
                                is_next = true;
                            }
                            else
                            {
                                is_prev = false;
                                is_next = true;
                            }
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                            if (user_subscription_id == 0)
                            {
                                is_prev = false;
                                is_next = false;
                            }
                            else
                            {
                                is_prev = false;
                                is_next = true;
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();

                        long prev_id = user_subscription_id_list.TakeWhile(x => !x.Equals(user_subscription_id)).LastOrDefault();
                        if (prev_id == 0)
                        {
                            is_prev = false;
                            is_next = true;
                        }

                        long next_id = user_subscription_id_list.SkipWhile(x => !x.Equals(user_subscription_id)).Skip(1).FirstOrDefault();
                        if (next_id == 0)
                        {
                            is_prev = true;
                            is_next = false;
                        }

                    }
                }

                //for usersubscriptionid
                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                var queryTran = from BH in dbConnection.balance_history
                                join US in dbConnection.user_subscription on BH.user_subscription_id equals US.user_subscription_id
                                //where US.user_id == userID & US.is_current == true
                                where US.user_id == userID & US.user_subscription_id == user_subscription_id
                                orderby BH.transaction_date ascending
                                select BH.transaction_date;

                var firstDay = DateTime.UtcNow.Date;
                var lastDay = DateTime.UtcNow.Date;
                var tempLastDay = DateTime.UtcNow.Date;

                if (user_subscription_id > 0)
                {
                    if (queryTran.Count() > 0)
                    {
                        //firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault();
                        //lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault();
                        firstDay = queryTran.ToList().FirstOrDefault().Value;
                        lastDay = queryTran.ToList().LastOrDefault().Value;
                        firstDay = firstDay.AddDays(-1);
                    }
                }
                tempLastDay = lastDay;

                if (queryTran.Count() > 0)
                {
                    var firstTransactionDate = queryTran.ToList()[0].Value;
                    int lastRecIndex = queryTran.Count() - 1;
                    var lastTransactionDate = queryTran.ToList()[lastRecIndex].Value;

                    if (DateTime.UtcNow.Date < lastDay.Date)
                    {
                        lastDay = DateTime.UtcNow.Date;
                    }
                }

                var query =
                    //from UT in dbConnection.balance_history
                    from UT in dbConnection.user_transactions
                    join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                    //where US.user_id == userID & US.is_current == true & UT.transaction_date >= firstDay & UT.transaction_date <= lastDay
                    where US.user_id == userID & US.user_subscription_id == user_subscription_id & UT.transaction_date >= firstDay & UT.transaction_date <= lastDay
                    orderby UT.transaction_date ascending
                    select new TempCurrentBal
                    {
                        user_subscription_id = US.user_subscription_id,
                        transaction_day = UT.transaction_date,
                        //transaction_day = DateTime.ParseExact(UT.transaction_date.Value.Date.ToString(), "dd/MM/yyyy", null),
                        net_pnl = UT.net_profit_loss,
                        Exchange = UT.exchange,
                    };

                if (string.IsNullOrEmpty(Exchange))                     //Added by Bharat
                {
                    Exchange = "All Markets";
                }

                if (Exchange != "All Markets")
                {
                    query = query.Where(x => x.Exchange == Exchange);
                }


                if (query.Count() > 0)   // added by Bharat 16/09/2017
                {
                    CurrentbalChart objTransactionModel;

                    for (var day = firstDay.Date; day.Date <= lastDay.Date; day = day.AddDays(1))
                    {
                        objTransactionModel = new CurrentbalChart();
                        objTransactionModel.x = string.Format("{0:00}/{1:00}/{2:0000}",
                                                day.Date.Day,
                                                day.Date.Month,
                                                day.Date.Year);

                        objTransactionModel.y = "0";
                        objTransactionModel.dx = day.Date;
                        objTransactionModel.user_subscription_id = user_subscription_id; //added by Bharat
                        objTransactionModel.Is_Prev = is_prev;//added by Bharat
                        objTransactionModel.Is_next = is_next;//added by Bharat
                        payments.Add(objTransactionModel);
                    }
                }
                else
                {
                    var day = DateTime.UtcNow;
                    CurrentbalChart objTransactionModel = new CurrentbalChart();
                    objTransactionModel.user_subscription_id = user_subscription_id; //added by Bharat
                    objTransactionModel.x = string.Format("{0:00}/{1:00}/{2:0000}",
                                                day.Date.Day,
                                                day.Date.Month,
                                                day.Date.Year);

                    objTransactionModel.y = "0";
                    objTransactionModel.dx = day.Date;
                    objTransactionModel.Is_Prev = is_prev;//added by Bharat
                    objTransactionModel.Is_next = is_next;//added by Bharat
                    payments.Add(objTransactionModel);
                }

                //by shoaib - for sum (17-Nov-2017)
                List<TempCurrentBal> LstTransactions;// = new List<TempCurrentBal>();
                LstTransactions = query.ToList();

                List<TempCurrentBal> result = LstTransactions
                .GroupBy(l => l.transaction_day.Date)
                .Select(cl => new TempCurrentBal
                {
                    user_subscription_id = cl.FirstOrDefault().user_subscription_id,
                    transaction_day = cl.FirstOrDefault().transaction_day,
                    net_pnl = cl.Sum(c => c.net_pnl),
                    Exchange = cl.FirstOrDefault().Exchange,
                }).ToList();
                //by shoaib - for sum (17-Nov-2017)

                if (query != null)
                {
                    decimal? summedNumber2 = 0;
                    decimal? summedNumberTemp = 0;
                    //foreach (var model in query)
                    foreach (var model in result)
                    {
                        summedNumber2 = summedNumber2 + model.net_pnl;
                        summedNumberTemp = model.net_pnl;

                        for (var day = model.transaction_day; day <= lastDay.Date.AddDays(1); day = day.AddDays(1))
                        {
                            foreach (var tom in payments.Where(w => w.dx.Date == day.Date))   // added by Bharat 16/09/2017
                            {
                                tom.y = summedNumber2.ToString();
                            }
                        }
                    }
                }

                payments.ForEach(x => x.chartType = ChartType);

                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }

        #endregion

        #region Get Net P/L List
        /// <summary>
        /// Get Net P/L List with filters
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<NetPlScript>> GetNetPLList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0)
        {
            ServiceResponse<List<NetPlScript>> response = new ServiceResponse<List<NetPlScript>>();
            List<NetPlScript> payments = new List<NetPlScript>();
            List<NetPlScript> revisePayments = new List<NetPlScript>();
            try
            {
                var firstDay = DateTime.UtcNow;
                var lastDay = DateTime.UtcNow;

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();


                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                if (user_subscription_id > 0)
                {
                    firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault();
                    lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault();
                    firstDay = firstDay.AddDays(-1);
                }

                if (DateTime.UtcNow.Date < lastDay.Date)
                {
                    lastDay = DateTime.UtcNow.Date;
                }

                var query =
                        from UT in dbConnection.user_transactions
                        join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                        where US.user_id == userID & US.user_subscription_id == user_subscription_id & UT.transaction_date >= firstDay & UT.transaction_date <= lastDay
                        orderby UT.symbol ascending
                        select new
                        {
                            trade_script = UT.symbol,
                            net_profit_loss = UT.net_profit_loss,
                            Exchange = UT.exchange,
                        };

                if (string.IsNullOrEmpty(Exchange))                     //Added by Bharat
                {
                    Exchange = "All Markets";
                }

                if (Exchange != "All Markets")
                {
                    query = query.Where(x => x.Exchange == Exchange);
                }


                if (query.Count() > 0)
                {
                    //Added by Hardik on 23-Oct-2017
                    //for average same symbol 
                    var results =
                          from kvp in query
                          group kvp by kvp.trade_script.ToUpper() into g
                          select new
                          {
                              Script = g.Key,
                              Amount = g.Sum(kvp => kvp.net_profit_loss)
                          };


                    NetPlScript objNetPlModel;
                    foreach (var model in results)
                    {

                        objNetPlModel = new NetPlScript();
                        objNetPlModel.Script = model.Script;
                        objNetPlModel.net_profit_loss = model.Amount;
                        objNetPlModel.user_subscription_id = user_subscription_id; //added by Bharat
                        payments.Add(objNetPlModel);

                    }
                }
                else
                {
                    NetPlScript objNetPlModel = new NetPlScript();
                    objNetPlModel.Script = String.Empty;
                    objNetPlModel.net_profit_loss = 0;
                    objNetPlModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objNetPlModel);
                }
                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region Get Total Trades List
        /// <summary>
        /// Get Total Trades List
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<TotalTrade>> GetTradeLList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0)
        {
            ServiceResponse<List<TotalTrade>> response = new ServiceResponse<List<TotalTrade>>();
            List<TotalTrade> payments = new List<TotalTrade>();
            List<TotalTrade> revisePayments = new List<TotalTrade>();
            try
            {

                var firstDay = DateTime.UtcNow;
                var lastDay = DateTime.UtcNow;

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();


                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                if (user_subscription_id > 0)
                {
                    firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault();
                    lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault();
                    firstDay = firstDay.AddDays(-1);
                }

                if (DateTime.UtcNow.Date < lastDay.Date)
                {
                    lastDay = DateTime.UtcNow.Date;
                }

                var query =
                     //from UT in dbConnection.balance_history
                     from UT in dbConnection.user_transactions
                     join US in dbConnection.user_subscription on UT.user_subscription_id equals US.user_subscription_id
                     where US.user_id == userID & US.user_subscription_id == user_subscription_id & UT.transaction_date >= firstDay & UT.transaction_date <= lastDay
                     orderby UT.transaction_date ascending
                     select new
                     {
                         winning_trade = UT.winning_trade,
                         losing_trade = UT.losing_trade,
                         Exchange = UT.exchange,
                     };

                if (string.IsNullOrEmpty(Exchange))                     //Added by Bharat
                {
                    Exchange = "All Markets";
                }

                if (Exchange != "All Markets")
                {
                    query = query.Where(x => x.Exchange == Exchange);
                }


                if (query.Count() > 0)
                {
                    TotalTrade objTotalTradeModel;
                    objTotalTradeModel = new TotalTrade();
                    objTotalTradeModel.NoofwiningTrade = query.Select(x => x.winning_trade) == null ? "0" : (query.Select(x => x.winning_trade).Sum()).ToString();
                    objTotalTradeModel.NooflosingTrade = query.Select(x => x.losing_trade) == null ? "0" : (query.Select(x => x.losing_trade).Sum()).ToString();
                    objTotalTradeModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objTotalTradeModel);

                }
                else    //for prev/next record in case we dont have any record then and also we have to send an user_subscription_id
                {
                    TotalTrade objTotalTradeModel;
                    objTotalTradeModel = new TotalTrade();
                    objTotalTradeModel.NoofwiningTrade = "0";
                    objTotalTradeModel.NooflosingTrade = "0";
                    objTotalTradeModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objTotalTradeModel);
                }
                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region TT User Account Balance New
        public HomePageModel TTGetUserAccountBalance(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0, string sAccountNo = "")
        {
            try
            {
                HomePageModel objModel = new HomePageModel();
                DateTime CurrentDate = System.DateTime.UtcNow.AddDays(-1);
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                var LstUserResets = dbConnection.reset_requests.Where(x => x.user_id == userID && x.reset_status == true).OrderByDescending(x => x.reset_request_id).ToList();
                var ObjUserSubscriptions = dbConnection.user_subscription.Where(x => x.user_id == userID).ToList();
                if (LstUserResets.Count() > 0)
                {
                    ObjUserSubscriptions.Where(x => x.user_subscription_id > LstUserResets.First().user_subscription_id);
                }
                List<long> ArrSubIds = new List<long>();
                if (ObjUserSubscriptions.Count() == 1 && ObjUserSubscriptions[0].free_trial == true)
                {
                    ArrSubIds.Add(ObjUserSubscriptions[0].user_subscription_id);
                }
                else if (ObjUserSubscriptions.Count() > 0)
                {
                    ArrSubIds = ObjUserSubscriptions.Where(x => x.free_trial == false).Select(x => x.user_subscription_id).ToList();
                }
                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                                (user_subscription_id > 0 ? x.user_subscription_id == user_subscription_id : x.user_subscription_id == x.user_subscription_id)
                                                && (x.user_id == userID)).OrderBy(x => x.date).ToList();

                if (!string.IsNullOrEmpty(sAccountNo))
                {
                    lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(sAccountNo.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }
                var lstFillMasterGroupDate = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();

                if (lstFillMasterGroupDate.Count() > 0)
                {
                    decimal dHighPnL = 0;
                    decimal dLowPnL = 0;
                    decimal dHighBalance = 0;
                    decimal dLowBalance = 0;
                    bool bHighLowSetted = false;
                    decimal dCurrentBalance = 0;
                    decimal totalWinningDay = 0;
                    decimal totalLosingDay = 0;
                    decimal totalWinningAmount = 0;
                    decimal totalLossingAmount = 0;
                    decimal dLastDayBalance = 0;
                    foreach (var item in lstFillMasterGroupDate)
                    {
                        List<int> lstTradeNumber = item.Select(x => x.trade_number).Distinct().ToList();
                        decimal dTempHighBalance = dLastDayBalance + item.OrderBy(x => x.trade_number).LastOrDefault().high_pnl.Value;
                        decimal dTempLowBalance = dLastDayBalance + item.OrderBy(x => x.trade_number).LastOrDefault().low_pnl.Value;

                        if (dHighBalance < dTempHighBalance)
                        {
                            dHighBalance = dTempHighBalance;
                        }
                        if (dLowBalance > dTempLowBalance)
                        {
                            dLowBalance = dTempLowBalance;
                        }
                        if (!bHighLowSetted)
                        {
                            dHighPnL = item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                            dLowPnL = item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                            bHighLowSetted = true;
                        }
                        else
                        {
                            if (dHighPnL <= item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value)
                            {
                                dHighPnL = item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                            }
                            if (dLowPnL > item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value)
                            {
                                dLowPnL = item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                            }
                        }
                        dLastDayBalance = item.OrderBy(x => x.trade_number).LastOrDefault().current_balance_pnl.Value;
                        if (item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value > 0)
                        {
                            totalWinningDay = totalWinningDay + 1;
                            totalWinningAmount = totalWinningAmount + item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                        }
                        else if (item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value < 0)
                        {
                            totalLosingDay = totalLosingDay + 1;
                            totalLossingAmount = totalLossingAmount + item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                        }
                    }
                    dCurrentBalance = lstFillMasterGroupDate.LastOrDefault().LastOrDefault().current_balance_pnl.Value;

                    objModel = new HomePageModel();
                    if (lstFillMasterGroupDate.FirstOrDefault().Any())
                    {
                        objModel.FirstName = lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.first_name;
                        objModel.LastName = lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.last_name;
                        if (lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.country_master != null)
                        {
                            objModel.Country = lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.country_master.country_name;
                        }
                        objModel.account_number = lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.account_number;
                        objModel.UserEmail = lstFillMasterGroupDate.FirstOrDefault().FirstOrDefault().user_master.user_email;
                    }
                    objModel.UserSubscriptionID = Convert.ToInt64(lstFillMasterGroupDate.FirstOrDefault().Select(x => x.user_subscription_id).FirstOrDefault());

                    //High Balance
                    objModel.HighBalanceOld = decimal.Round(dHighBalance, 0, MidpointRounding.AwayFromZero);
                    //added to stop negative high balance
                    objModel.HighBalanceOld = objModel.HighBalanceOld > 0 ? objModel.HighBalanceOld : 0;
                    objModel.HighBalance = (objModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.HighBalanceOld.Value).ToString("#,##0");

                    //Low Balance
                    objModel.LowBalanceOld = decimal.Round(dLowBalance, 0, MidpointRounding.AwayFromZero);
                    //added to stop negative high balance on 25-July-2018 hardik
                    objModel.LowBalanceOld = objModel.LowBalanceOld < 0 ? objModel.LowBalanceOld : 0;
                    objModel.LowBalance = (objModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.LowBalanceOld.Value).ToString("#,##0");

                    //Current Balance
                    if (dCurrentBalance != 0)
                    {
                        objModel.CurrentBalanceOld = decimal.Round(dCurrentBalance, 0, MidpointRounding.AwayFromZero);
                        objModel.CurrentBalance = (objModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.CurrentBalanceOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.CurrentBalance = "$0";
                    }

                    //Total winnig day
                    if (totalWinningDay != 0 && totalWinningAmount != 0)
                    {
                        objModel.AvgWinningDayOld = decimal.Round(totalWinningAmount / totalWinningDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgWinningDay = (objModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgWinningDayOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.AvgWinningDay = "$0";
                    }

                    //Best Day
                    objModel.BestDayOld = decimal.Round(dHighPnL, 0, MidpointRounding.AwayFromZero);
                    objModel.BestDay = (objModel.BestDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.BestDayOld.Value).ToString("#,##0");

                    //Loosing Day
                    if (totalLosingDay != 0 && totalLossingAmount != 0)
                    {
                        objModel.AvgLosingDayOld = decimal.Round(totalLossingAmount / totalLosingDay, 0, MidpointRounding.AwayFromZero);
                        objModel.AvgLosingDay = (objModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.AvgLosingDayOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        objModel.AvgLosingDay = "$0";
                    }

                    //worst Day
                    objModel.WorstDayOld = decimal.Round(dLowPnL, 0, MidpointRounding.AwayFromZero);
                    objModel.WorstDay = (objModel.WorstDayOld < 0 ? "-" : "") + "$" + Math.Abs(objModel.WorstDayOld.Value).ToString("#,##0");

                    return objModel;
                }
                else
                {
                    objModel = new HomePageModel();
                    user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == userID).FirstOrDefault();
                    if (objUserMaster != null)
                    {
                        objModel.FirstName = objUserMaster.first_name;
                        objModel.LastName = objUserMaster.last_name;
                        if (objUserMaster.country_master != null)
                        {
                            objModel.Country = objUserMaster.country_master.country_name;
                        }
                        objModel.account_number = objUserMaster.account_number;
                        objModel.UserEmail = objUserMaster.user_email;
                    }
                    objModel.HighBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.CurrentBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.LowBalance = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgWinningDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.BestDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.AvgLosingDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.WorstDay = "$" + Math.Abs(0).ToString("#,##0");
                    objModel.UserSubscriptionID = user_subscription_id;
                }

                return objModel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region TT Get Rule
        public Entity.MemberModel.RuleModel TTGetRule(Int64 userID = 0, string Sequence = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            try
            {
                Entity.MemberModel.RuleModel model = new Entity.MemberModel.RuleModel();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                long user_subscription_id = userSubscriptionID;
                var LstUserResets = dbConnection.reset_requests.Where(x => x.user_id == userID && x.reset_status == true).OrderByDescending(x => x.reset_request_id).ToList();
                var ObjUserSubscriptions = dbConnection.user_subscription.Where(x => x.user_id == userID).ToList();
                if (LstUserResets.Count() > 0)
                {
                    ObjUserSubscriptions.Where(x => x.user_subscription_id > LstUserResets.First().user_subscription_id);
                }
                List<long> ArrSubIds = new List<long>();
                if (ObjUserSubscriptions.Count() == 1 && ObjUserSubscriptions[0].free_trial == true)
                {
                    ArrSubIds.Add(ObjUserSubscriptions[0].user_subscription_id);
                }
                else if (ObjUserSubscriptions.Count() > 0)
                {
                    ArrSubIds = ObjUserSubscriptions.Where(x => x.free_trial == false).Select(x => x.user_subscription_id).ToList();
                }
                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                             (user_subscription_id > 0 ? x.user_subscription_id == user_subscription_id : x.user_subscription_id == x.user_subscription_id)
                                             && (x.user_id == userID)).OrderByDescending(x => x.date).ToList();
                var cumulativequery = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();


                if (!string.IsNullOrEmpty(saccountallias))
                {
                    cumulativequery = cumulativequery.Where(x => x.FirstOrDefault().account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        cumulativequery = cumulativequery.Where(x => x.FirstOrDefault().account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }

                var query = (from CQ in cumulativequery
                             join US in dbConnection.user_subscription on CQ.FirstOrDefault().user_subscription_id equals US.user_subscription_id
                             join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                             where ArrSubIds.Contains(US.user_subscription_id)
                             select new { CQ, SM }).OrderByDescending(x => x.CQ.FirstOrDefault().date).OrderByDescending(x => x.SM.created_on).ToList();
                Decimal dHignPnlOverall = 0;
                Decimal dLowPnlOverall = 0;
                Decimal dTempHignPnlOverall = 0;
                Decimal dTempLowPnlOverall = 0;
                Decimal dLowPnL = 0;
                Decimal dLastDayBalance = 0;

                foreach (var item in cumulativequery.OrderBy(x => x.FirstOrDefault().date.Value))
                {
                    dTempHignPnlOverall = Convert.ToDecimal(dLastDayBalance + item.OrderBy(x => x.trade_number).LastOrDefault().high_pnl);
                    dTempLowPnlOverall = Convert.ToDecimal(dLastDayBalance + item.OrderBy(x => x.trade_number).LastOrDefault().low_pnl);
                    if (dHignPnlOverall < dTempHignPnlOverall)
                    {
                        dHignPnlOverall = dTempHignPnlOverall;
                    }
                    if (dLowPnlOverall > dTempLowPnlOverall)
                    {
                        dLowPnlOverall = dTempLowPnlOverall;
                    }
                    if (dLowPnL > item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value)
                    {
                        dLowPnL = item.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value;
                    }
                    dLastDayBalance = item.OrderBy(x => x.trade_number).LastOrDefault().current_balance_pnl.Value;
                }

                if (query.Count > 0)
                {
                    model = new Entity.MemberModel.RuleModel();
                    //By shoaib for Getting Rule Values from user current Subscription - 15-Sep-2017
                    model.user_subscription_id = ArrSubIds[0];
                    model.ruleprofit_targetOld = query.First().SM.profite_target;
                    model.ruleprofit_target = (model.ruleprofit_targetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruleprofit_targetOld.Value).ToString("#,##0");
                    model.ruledaily_loss_limitOld = query.First().SM.daily_loss_limit * -1;
                    model.ruledaily_loss_limit = (model.ruledaily_loss_limitOld < 0 ? "-" : "") + "$" + Math.Abs(model.ruledaily_loss_limitOld.Value).ToString("#,##0");
                    //model.rulemax_drowdownOld = decimal.Round(dHignPnlOverall, 0, MidpointRounding.AwayFromZero) + Convert.ToDecimal(-2000);
                    model.rulemax_drowdownOld =Convert.ToDecimal(-2000);
                    if (model.rulemax_drowdownOld > 0)
                    {
                        model.rulemax_drowdownOld = 0;
                    }
                    model.rulemax_drowdown = (model.rulemax_drowdownOld < 0 ? "-" : "") + "$" + Math.Abs(model.rulemax_drowdownOld.Value).ToString("#,##0");
                    model.rulemaintain_account_balanceOld = query.First().SM.starting_balance;
                    model.rulemaintain_account_balance = (model.rulemaintain_account_balanceOld < 0 ? "-" : "") + "$" + Math.Abs(model.rulemaintain_account_balanceOld.Value).ToString("#,##0");
                    model.ruletrade_required_no_of_days = 10;
                    #region for Cumulative Profit Target
                    decimal? CumulativeProfitTarget = 0;
                    if (cumulativequery != null)
                    {
                        List<decimal> CumulativeList = cumulativequery.Select(x => x.FirstOrDefault().current_balance_pnl.Value).ToList();
                        CumulativeProfitTarget = CumulativeList.FirstOrDefault();
                    }
                    #endregion

                    model.ProfitTargetOld = CumulativeProfitTarget;
                    model.ProfitTarget = (model.ProfitTargetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ProfitTargetOld.Value).ToString("#,##0");

                    model.MaxDrowdownOld = CumulativeProfitTarget;
                    model.MaxDrowdown = (model.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(Convert.ToDecimal(model.MaxDrowdownOld)).ToString("#,##0");

                    //if (CumulativeProfitTarget.Value >= 0)
                    //{
                    //    model.ProfitTargetOld = CumulativeProfitTarget;
                    //    model.ProfitTarget = (model.ProfitTargetOld < 0 ? "-" : "") + "$" + Math.Abs(model.ProfitTargetOld.Value).ToString("#,##0");
                    //}
                    //else
                    //{
                    //    model.ProfitTarget = "$0";
                    //}

                    if (dLowPnlOverall < 0)
                    {
                        model.DailyLossLimitOld = decimal.Round(dLowPnL, 0, MidpointRounding.AwayFromZero);
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }
                    else
                    {
                        model.DailyLossLimit = "$0";
                    }

                    if (query.Count > 0)
                    {
                        model.TradeRequiredNoOfDays = query.GroupBy(x => x.CQ.FirstOrDefault().date.Value.Date).Count();
                    }
                    else
                    {
                        model.TradeRequiredNoOfDays = 0;
                    }

                    //For Profit Target Check Mark
                    if (model.ProfitTargetOld <= model.ruleprofit_targetOld)
                    {
                        model.profit_targetachievestatus = "cancel";
                    }
                    else
                    {
                        model.profit_targetachievestatus = "check_circle";
                    }

                    //For Daily Loss Limit Check Mark
                    if (model.DailyLossLimitOld <= 0)
                    {
                        model.DailyLossLimit = (model.DailyLossLimitOld < 0 ? "-" : "") + "$" + Math.Abs(model.DailyLossLimitOld.Value).ToString("#,##0");
                    }

                    if (model.DailyLossLimitOld >= -1000)
                    {
                        model.daily_loss_limitachievestatus = "check_circle";
                    }
                    else
                    {
                        model.daily_loss_limitachievestatus = "cancel";
                    }

                    if (model.MaxDrowdownOld >= model.rulemax_drowdownOld)
                    {
                        model.max_drowdownachievestatus = "check_circle";
                    }
                    else
                    {
                        model.max_drowdownachievestatus = "cancel";
                    }

                    //For Maintain Account Balance Check Mark
                    if (model.MainAccountBalanceOld >= model.rulemaintain_account_balanceOld)
                    {
                        model.maintain_account_balanceachievestatus = "check_circle";
                    }
                    else
                    {
                        model.maintain_account_balanceachievestatus = "cancel";
                    }

                    //For Trades Per Day Check Mark
                    if (model.TradeRequiredNoOfDays <= model.ruletrade_required_no_of_days)
                    {
                        model.trade_required_no_of_daysachievestatus = "cancel";
                    }
                    else
                    {
                        model.trade_required_no_of_daysachievestatus = "check_circle";
                    }
                }
                else
                {
                    var query2 = (from US in dbConnection.user_subscription
                                  join SM in dbConnection.subscription_master on US.subscription_id equals SM.subscription_id
                                  where US.user_subscription_id == user_subscription_id
                                  select new { US, SM }).OrderByDescending(x => x.SM.subscription_id).FirstOrDefault();

                    if (query2 != null)
                    {
                        model = new Entity.MemberModel.RuleModel();
                        model.user_subscription_id = user_subscription_id;
                        model.ruleprofit_target = "$" + Math.Abs(query2.SM.profite_target).ToString("#,##0");
                        model.ruledaily_loss_limit = "$" + Math.Abs(query2.SM.daily_loss_limit.Value).ToString("#,##0");
                        model.rulemax_drowdown = "$" + Math.Abs(query2.SM.max_drawdown.Value).ToString("#,##0");
                        model.rulemaintain_account_balance = "$" + Math.Abs(query2.SM.starting_balance).ToString("#,##0");
                        model.ruletrade_required_no_of_days = 10;
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get Current Balance List
        /// <summary>
        /// Get Current Balance List with filters
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 17/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<CurrentbalChart>> GetTTCurrentBalanceList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            ServiceResponse<List<CurrentbalChart>> response = new ServiceResponse<List<CurrentbalChart>>();
            List<CurrentbalChart> payments = new List<CurrentbalChart>();
            List<CurrentbalChart> revisePayments = new List<CurrentbalChart>();
            if (string.IsNullOrEmpty(saccountallias))
            {
                return response;
            }
            //CurrentbalChartRewise objModel;
            try
            {
                string ChartType = null;
                //var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).OrderByDescending(x => x.user_subscription_id).Select(x => x.user_subscription_id).ToList();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                var is_prev = true;
                var is_next = true;
                long prev_id = 0;
                long next_id = 0;
                decimal starting_balance = 0;
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;

                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                            if (user_subscription_id == 0)
                            {
                                is_prev = true;
                                is_next = true;
                            }
                            else
                            {
                                is_prev = false;
                                is_next = true;
                            }
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                            if (user_subscription_id == 0)
                            {
                                is_prev = false;
                                is_next = false;
                            }
                            else
                            {
                                is_prev = false;
                                is_next = true;
                            }
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                        prev_id = user_subscription_id_list.TakeWhile(x => !x.Equals(user_subscription_id)).LastOrDefault();
                        if (prev_id == 0)
                        {
                            is_prev = false;
                            is_next = true;
                        }
                        next_id = user_subscription_id_list.SkipWhile(x => !x.Equals(user_subscription_id)).Skip(1).FirstOrDefault();
                        if (next_id == 0)
                        {
                            is_prev = true;
                            is_next = false;
                        }
                    }
                }

                //for usersubscriptionid
                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                var queryTran = dbConnection.tt_fill_master_manual.Where(x => (x.account_no == saccountallias) && (x.user_id == userID))
                                .Select(x => DbFunctions.TruncateTime(x.date)).Distinct().OrderBy(x => x.Value)
                                .ToList();
                //where US.user_id == userID & US.is_current == true


                var firstDay = DateTime.UtcNow.Date;
                var lastDay = DateTime.UtcNow.Date;
                var tempLastDay = DateTime.UtcNow.Date;

                if (user_subscription_id > 0)
                {
                    if (queryTran.Count() > 0)
                    {
                        //firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault().Date;
                        //lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault().Date;
                        firstDay = queryTran.ToList().FirstOrDefault().Value;
                        lastDay = queryTran.ToList().LastOrDefault().Value;
                        firstDay = firstDay.AddDays(-1);
                    }
                }
                tempLastDay = lastDay;

                if (queryTran.Count() > 0)
                {
                    var firstTransactionDate = queryTran.ToList()[0];
                    int lastRecIndex = queryTran.Count() - 1;
                    var lastTransactionDate = queryTran.ToList()[lastRecIndex];

                    if (DateTime.UtcNow.Date > lastDay.Date)
                    {
                        lastDay = DateTime.UtcNow.Date;
                    }
                }

                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                                (x.account_no == saccountallias)
                                                && (x.user_id == userID)).OrderByDescending(x => x.date).ToList();
                if (!string.IsNullOrEmpty(saccountallias))
                {
                    lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }

                var lstFillMasterGroupDate = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();

                List<TempCurrentBal> query = new List<TempCurrentBal>();
                TempCurrentBal objTempCurrentBal1 = new TempCurrentBal();
                foreach (var item in lstFillMasterGroupDate)
                {
                    objTempCurrentBal1 = new TempCurrentBal();
                    objTempCurrentBal1.user_subscription_id = item.FirstOrDefault().user_subscription_id.Value;
                    objTempCurrentBal1.transaction_day = item.FirstOrDefault().date.Value;
                    objTempCurrentBal1.net_pnl = item.FirstOrDefault().current_balance_pnl.Value;
                    objTempCurrentBal1.Exchange = item.FirstOrDefault().exchange;
                    query.Add(objTempCurrentBal1);
                }
                if (string.IsNullOrEmpty(Exchange))
                {
                    Exchange = "All Markets";
                }
                if (query.Count() > 0)
                {
                    CurrentbalChart objTransactionModel;
                    for (var day = firstDay.Date; day.Date <= lastDay.Date; day = day.AddDays(1))
                    {
                        objTransactionModel = new CurrentbalChart();
                        objTransactionModel.x = string.Format("{0:00}/{1:00}/{2:0000}",
                                                day.Date.Day,
                                                day.Date.Month,
                                                day.Date.Year);
                        objTransactionModel.y = "0";
                        objTransactionModel.dx = day.Date;
                        objTransactionModel.user_subscription_id = user_subscription_id; //added by Bharat
                        objTransactionModel.Is_Prev = is_prev;//added by Bharat
                        objTransactionModel.Is_next = is_next;//added by Bharat
                        payments.Add(objTransactionModel);
                    }
                }
                else
                {
                    var day = DateTime.UtcNow;
                    CurrentbalChart objTransactionModel = new CurrentbalChart();
                    objTransactionModel.user_subscription_id = user_subscription_id; //added by Bharat
                    objTransactionModel.x = string.Format("{0:00}/{1:00}/{2:0000}",
                                                day.Date.Day,
                                                day.Date.Month,
                                                day.Date.Year);

                    objTransactionModel.y = "0";
                    objTransactionModel.dx = day.Date;
                    objTransactionModel.Is_Prev = is_prev;//added by Bharat
                    objTransactionModel.Is_next = is_next;//added by Bharat
                    payments.Add(objTransactionModel);
                }
                if (prev_id != 0)
                {
                    starting_balance = (from X1 in dbConnection.balance_history
                                        where X1.user_subscription_id == prev_id
                                        select X1.current_balance.Value).ToList().LastOrDefault();
                }

                List<TempCurrentBal> LstTransactions;// = new List<TempCurrentBal>();
                LstTransactions = query.ToList();
                if (LstTransactions.Count() > 0)
                {
                    if (LstTransactions.FirstOrDefault().transaction_day > firstDay)
                    {
                        TempCurrentBal objTempCurrentBal = new TempCurrentBal();
                        objTempCurrentBal.user_subscription_id = user_subscription_id;
                        objTempCurrentBal.transaction_day = firstDay;
                        objTempCurrentBal.net_pnl = starting_balance;
                        LstTransactions.Add(objTempCurrentBal);
                    }
                    else if (LstTransactions.FirstOrDefault().transaction_day == firstDay)
                    {
                        LstTransactions.FirstOrDefault().net_pnl = LstTransactions.FirstOrDefault().net_pnl + starting_balance;
                    }
                }
                LstTransactions = LstTransactions.OrderBy(x => x.transaction_day).ToList();
                List<TempCurrentBal> result = LstTransactions
                .GroupBy(l => l.transaction_day.Date)
                .Select(cl => new TempCurrentBal
                {
                    user_subscription_id = cl.FirstOrDefault().user_subscription_id,
                    transaction_day = cl.FirstOrDefault().transaction_day,
                    net_pnl = cl.FirstOrDefault().net_pnl,
                    Exchange = cl.FirstOrDefault().Exchange,
                }).ToList();

                //by shoaib - for sum (17-Nov-2017)
                if (query != null)
                {
                    decimal? summedNumber2 = 0;
                    foreach (var model in result)
                    {
                        summedNumber2 = model.net_pnl;
                        //summedNumber2 = summedNumber2 + model.net_pnl;
                        for (var day = model.transaction_day; day <= lastDay.Date.AddDays(1); day = day.AddDays(1))
                        {
                            foreach (var tom in payments.Where(w => w.dx.Date == day.Date))
                            {
                                tom.y = summedNumber2.ToString();
                            }
                        }
                    }
                }

                payments.ForEach(x => x.chartType = ChartType);
                payments = payments.Where(x => x.dx.Date != DateTime.UtcNow.Date).ToList();
                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region Get Net P/L List
        /// <summary>
        /// Get Net P/L List with filters
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<NetPlScript>> GetTTNetPLList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            ServiceResponse<List<NetPlScript>> response = new ServiceResponse<List<NetPlScript>>();
            List<NetPlScript> payments = new List<NetPlScript>();
            List<NetPlScript> revisePayments = new List<NetPlScript>();
            try
            {
                var firstDay = DateTime.UtcNow;
                var lastDay = DateTime.UtcNow;

                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();

                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                if (user_subscription_id > 0)
                {
                    firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault();
                    lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault();
                    firstDay = firstDay.AddDays(-1);
                }

                if (DateTime.UtcNow.Date > lastDay.Date)
                {
                    lastDay = DateTime.UtcNow.Date;
                }
                var query = dbConnection.tt_fill_master_manual.Where(x => x.user_id == userID && x.account_no == saccountallias).ToList();

                if (!string.IsNullOrEmpty(saccountallias))
                {
                    query = query.Where(x => x.account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        query = query.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }

                var Cumalitivequery =
                        (from UT in query
                             //join US in dbConnection.user_subscription on UT.account_no equals US.
                             // where US.user_id == userID & US.user_subscription_id == user_subscription_id & UT.date >= firstDay// & UT.created_on <= lastDay
                         orderby UT.contract ascending
                         select new
                         {
                             trade_script = UT.contract,
                             net_profit_loss = UT.realised_pnl_usd_with_commision,
                             trade_number = UT.trade_number,
                             Exchange = "CME",
                         }).Distinct();

                if (string.IsNullOrEmpty(Exchange))
                {
                    Exchange = "All Markets";
                }

                if (Exchange != "All Markets")
                {
                    Cumalitivequery = Cumalitivequery.Where(x => x.Exchange == Exchange);
                }

                if (Cumalitivequery.Count() > 0)
                {

                    //Added by Hardik on 23-Oct-2017
                    //for average same symbol 
                    var results =
                          from kvp in Cumalitivequery
                          group kvp by kvp.trade_script.ToUpper() into g
                          select new
                          {
                              Script = g.Key.Substring(0, 2),
                              Amount = g.Sum(kvp => kvp.net_profit_loss),
                          };

                    NetPlScript objNetPlModel;
                    foreach (var model in results)
                    {
                        objNetPlModel = new NetPlScript();
                        objNetPlModel.Script = model.Script;
                        objNetPlModel.net_profit_loss = Math.Round(Convert.ToDecimal(model.Amount));
                        objNetPlModel.user_subscription_id = user_subscription_id; //added by Bharat
                        payments.Add(objNetPlModel);
                    }
                }
                else
                {
                    NetPlScript objNetPlModel = new NetPlScript();
                    objNetPlModel.Script = String.Empty;
                    objNetPlModel.net_profit_loss = 0;
                    objNetPlModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objNetPlModel);
                }
                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region Get Total Trades List
        /// <summary>
        /// Get Total Trades List
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="Year"></param>
        /// <param name="Month"></param>
        /// <param name="Week"></param>
        /// Date : 20/7/2017
        /// Dev By: Bharat Katua
        /// <returns></returns>
        public ServiceResponse<List<TotalTrade>> GetTTTradeLList(Int64 userID = 0, string Sequence = "", string Exchange = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            ServiceResponse<List<TotalTrade>> response = new ServiceResponse<List<TotalTrade>>();
            List<TotalTrade> payments = new List<TotalTrade>();
            List<TotalTrade> revisePayments = new List<TotalTrade>();
            try
            {
                var firstDay = DateTime.UtcNow;
                var lastDay = DateTime.UtcNow;
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }

                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }

                if (user_subscription_id > 0)
                {
                    firstDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_start_date.Value).FirstOrDefault();
                    lastDay = dbConnection.user_subscription.Where(x => x.user_subscription_id == user_subscription_id).Select(x => x.subscription_end_date.Value).FirstOrDefault();
                }

                if (DateTime.UtcNow.Date > lastDay.Date)
                {
                    lastDay = DateTime.UtcNow.Date;
                }

                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                                 (x.account_no == saccountallias)
                                                 && (x.user_id == userID)).OrderByDescending(x => x.date).ToList();

                if (!string.IsNullOrEmpty(saccountallias))
                {
                    lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }

                var lstFillMasterGroupDate = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();
                List<int> lstWinnigTrade = new List<int>();
                List<int> lstLossingTrade = new List<int>();
                List<int> lstScratchTrade = new List<int>();
                foreach (var model in lstFillMasterGroupDate)
                {
                    List<int> TotalTrades = model.Select(x => x.trade_number).Distinct().ToList();
                    //winning Trade
                    var WinnigTrade = model.Where(x => x.w_trades == 1).ToList();
                    lstWinnigTrade.AddRange(WinnigTrade.Select(x => x.trade_number).Distinct().ToList());

                    //lossing Trade
                    var LossingTrade = model.Where(x => x.l_trades == 1).ToList();
                    lstLossingTrade.AddRange(LossingTrade.Select(x => x.trade_number).Distinct().ToList());

                    //scratch Trade
                    var ScratchTrade = model.Where(x => x.s_trades == 1).ToList();
                    lstScratchTrade.AddRange(ScratchTrade.Select(x => x.trade_number).Distinct().ToList());
                }

                if (string.IsNullOrEmpty(Exchange))                     //Added by Bharat
                {
                    Exchange = "All Markets";
                }


                if (lstFillMasterGroupDate.Count() > 0)
                {
                    TotalTrade objTotalTradeModel;
                    objTotalTradeModel = new TotalTrade();
                    objTotalTradeModel.NoofwiningTrade = lstWinnigTrade.Count().ToString();
                    objTotalTradeModel.NooflosingTrade = lstLossingTrade.Count().ToString();
                    objTotalTradeModel.NoofsketchTrade = lstScratchTrade.Count().ToString();
                    objTotalTradeModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objTotalTradeModel);

                }
                else    //for prev/next record in case we dont have any record then and also we have to send an user_subscription_id
                {
                    TotalTrade objTotalTradeModel;
                    objTotalTradeModel = new TotalTrade();
                    objTotalTradeModel.NoofwiningTrade = "0";
                    objTotalTradeModel.NooflosingTrade = "0";
                    objTotalTradeModel.NoofsketchTrade = "0";
                    objTotalTradeModel.user_subscription_id = user_subscription_id; //added by Bharat
                    payments.Add(objTotalTradeModel);
                }
                response.Result = payments;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return response;
        }
        #endregion

        #region User Transaction List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 12/Dec/2018
        /// Dev By: Hardik Savaliya
        public List<HomePageModel> GetTTUserTransactionList(out long NextSubId, string Sequence = "", Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            try
            {
                ObjectParameter returnId = new ObjectParameter("nextsubid", typeof(int));
                List<HomePageModel> lstUserTransactionModel = new List<HomePageModel>();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }
                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }
                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                                  (x.account_no == saccountallias)
                                                  && (x.user_id == userID)).OrderByDescending(x => x.date).ToList();
                if (!string.IsNullOrEmpty(saccountallias))
                {
                    lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }
                var lstFillMasterGroupDate = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();
                if (string.IsNullOrEmpty(Exchange))
                {
                    Exchange = "All Markets";
                }
                NextSubId = 1;
                NextSubId = user_subscription_id;
                HomePageModel objTransactionModel;
                Decimal dHignPnlOverall = 0;
                Decimal dLowPnlOverall = 0;
                Decimal dTempHignPnlOverall = 0;
                Decimal dTempLowPnlOverall = 0;
                Decimal dLastDayBalance = 0;
                Decimal dTempLastDayHighPnl = 0;
                Decimal dTempLastDayLowPnl = 0;
                foreach (var model in lstFillMasterGroupDate)
                {
                    objTransactionModel = new HomePageModel();
                    objTransactionModel.UserID = model.FirstOrDefault().user_id.Value;
                    objTransactionModel.FirstName = model.FirstOrDefault().user_master.first_name;
                    objTransactionModel.LastName = model.FirstOrDefault().user_master.last_name;
                    objTransactionModel.isActive = model.FirstOrDefault().user_subscription.is_current.Value;
                    objTransactionModel.freeTrial = model.FirstOrDefault().user_subscription.free_trial.Value;
                    if (model.FirstOrDefault().user_subscription.free_trial.Value)
                        objTransactionModel.strFreeTrial = "Demo";
                    else
                        objTransactionModel.strFreeTrial = "Chanllenge";
                    objTransactionModel.account_number = model.FirstOrDefault().account_no;
                    objTransactionModel.UserSubscriptionID = model.FirstOrDefault().user_subscription_id;
                    objTransactionModel.CurrentBalanceOld = decimal.Round(model.OrderBy(x => x.trade_number).FirstOrDefault().current_balance_pnl.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.CurrentBalance = (objTransactionModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.CurrentBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.HighBalanceOld = decimal.Round(model.Max(x => x.realised_pnl_usd_with_commision).Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.HighBalance = (objTransactionModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.LowBalanceOld = decimal.Round(model.Min(x => x.realised_pnl_usd_with_commision).Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.LowBalance = (objTransactionModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.NetProfitLossOld = decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.NetProfitLoss = (objTransactionModel.NetProfitLossOld < 0 ? "-" : "") + "$" + Convert.ToDecimal(Math.Abs(objTransactionModel.NetProfitLossOld.Value)).ToString("#,##0.00");
                    objTransactionModel.GrossProfitLossOld = decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().cumalitve_pnl_without_commision.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.GrossProfitLoss = (objTransactionModel.GrossProfitLossOld < 0 ? "-" : "") + "$" + Convert.ToDecimal(Math.Abs(objTransactionModel.GrossProfitLossOld.Value)).ToString("#,##0.00");
                    objTransactionModel.HighProfitLossOld = model.OrderBy(x => x.trade_number).LastOrDefault().high_pnl != null ? decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().high_pnl.Value, 2, MidpointRounding.AwayFromZero) : 0;
                    objTransactionModel.HighProfitLoss = (objTransactionModel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objTransactionModel.LowProfitLossOld = model.OrderBy(x => x.trade_number).LastOrDefault().low_pnl != null ? decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().low_pnl.Value, 2, MidpointRounding.AwayFromZero) : 0;
                    objTransactionModel.LowProfitLoss = (objTransactionModel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objTransactionModel.TotalContracts = Convert.ToInt32(model.Sum(x => x.fill_qty));
                    objTransactionModel.Transaction_date = model.Key;

                    //Total Commision and trade
                    List<int> TotalTrades = model.Select(x => x.trade_number).Distinct().ToList();
                    decimal dCommission = 0;
                    foreach (var item in TotalTrades)
                    {
                        dCommission = dCommission + model.Where(x => x.trade_number == item).FirstOrDefault().commission.Value;
                    }
                    objTransactionModel.TotalCommisionsOld = decimal.Round(dCommission, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.TotalCommisions = (objTransactionModel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objTransactionModel.TotalTrades = TotalTrades.Count();

                    //winning Trade
                    var WinnigTrade = model.Where(x => x.w_trades == 1).ToList();
                    List<int> lstWinnigTrade = WinnigTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dWinTotalAmount = 0;
                    decimal dWinAvgAmount = 0;
                    Double dblWinnigTime = 0;
                    Double dblAvgWinnigTime = 0;
                    foreach (var item in lstWinnigTrade)
                    {
                        dWinTotalAmount = dWinTotalAmount + model.Where(x => x.trade_number == item).OrderBy(x => x.trade_number).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                        DateTime dblStartTime = model.Where(x => x.trade_number == item && x.trade_entry == 1).OrderBy(x => x.trade_number).FirstOrDefault().time.Value;
                        DateTime dblEndTime = model.Where(x => x.trade_number == item && x.trade_exit == 1).OrderBy(x => x.trade_number).FirstOrDefault().time.Value;
                        var diffInSeconds = (dblStartTime - dblEndTime).TotalSeconds;
                        dblWinnigTime = dblWinnigTime + Math.Abs(diffInSeconds);
                    }
                    if (dWinTotalAmount != 0)
                    {
                        dWinAvgAmount = (dWinTotalAmount) / lstWinnigTrade.Count();
                    }
                    if (dblWinnigTime != 0)
                    {
                        dblAvgWinnigTime = dblWinnigTime / lstWinnigTrade.Count();
                    }

                    //lossing Trade
                    var LossingTrade = model.Where(x => x.l_trades == 1).ToList();
                    List<int> lstLossingTrade = LossingTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dLossTotalAmount = 0;
                    decimal dLossAvgAmount = 0;
                    Double dblLossingTime = 0;
                    Double dblAvgLossingTime = 0;
                    foreach (var item in lstLossingTrade)
                    {
                        DateTime dblStartTime = System.DateTime.UtcNow;
                        DateTime dblEndTime = DateTime.UtcNow;
                        dLossTotalAmount = dLossTotalAmount + model.Where(x => x.trade_number == item).OrderBy(x => x.trade_number).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                        if (model.Where(x => x.trade_number == item && x.trade_entry == 1).Any())
                            dblStartTime = model.Where(x => x.trade_number == item && x.trade_entry == 1).OrderBy(x => x.trade_number).FirstOrDefault().time.Value;
                        if (model.Where(x => x.trade_number == item && x.trade_exit == 1).Any())
                            dblEndTime = model.Where(x => x.trade_number == item && x.trade_exit == 1).OrderBy(x => x.trade_number).FirstOrDefault().time.Value;
                        var diffInSeconds = (dblStartTime - dblEndTime).TotalSeconds;
                        dblLossingTime = dblLossingTime + Math.Abs(diffInSeconds);
                    }
                    if (dLossTotalAmount != 0)
                    {
                        dLossAvgAmount = (dLossTotalAmount) / lstLossingTrade.Count();
                    }
                    if (dblLossingTime != 0)
                    {
                        dblAvgLossingTime = dblLossingTime / lstLossingTrade.Count();
                    }

                    //scratch Trade
                    var ScratchTrade = model.Where(x => x.s_trades == 1).ToList();
                    List<int> lstScratchTrade = ScratchTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dScratchTotalAmount = 0;
                    decimal dScratchAvgAmount = 0;
                    foreach (var item in lstScratchTrade)
                    {
                        dScratchTotalAmount = dScratchTotalAmount + model.Where(x => x.trade_number == item).OrderBy(x => x.trade_number).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                    }
                    if (dScratchTotalAmount != 0)
                    {
                        dScratchAvgAmount = (dScratchTotalAmount) / lstScratchTrade.Count();
                    }

                    //Winning Trade Percentage
                    decimal dWinPercentage = 0;
                    if (lstWinnigTrade.Count != 0)
                    {
                        dWinPercentage = lstWinnigTrade.Count() * 100 / TotalTrades.Count();
                    }

                    objTransactionModel.AvgWinningTradeOld = decimal.Round(dWinAvgAmount, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgWinningTrade = (objTransactionModel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgLosingTradeOld = decimal.Round(dLossAvgAmount, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgLosingTrade = (objTransactionModel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objTransactionModel.TransactionDateString = model.OrderBy(x => x.trade_number).FirstOrDefault().date.Value.ToString("MMM d");
                    objTransactionModel.MaxConsWin = lstWinnigTrade.Count();
                    objTransactionModel.MaxConsLoss = lstLossingTrade.Count();
                    objTransactionModel.WinningTradePerOld = decimal.Round(dWinPercentage, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.WinningTradePer = (objTransactionModel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objTransactionModel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgLosingDayOld = decimal.Round(Convert.ToDecimal(dblAvgLossingTime), 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgLosingDay = (objTransactionModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgWinningDayOld = decimal.Round(Convert.ToDecimal(dblAvgWinnigTime), 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgWinningDay = (objTransactionModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objTransactionModel.Transaction_date = model.OrderBy(x => x.trade_number).FirstOrDefault().date;
                    double avg_winning_day = Convert.ToDouble(dblAvgWinnigTime);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);
                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = "W " + hh + ":" + mm + ":" + ss;
                    double avg_losing_day = Convert.ToDouble(dblAvgLossingTime);
                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = "L " + hh + ":" + mm + ":" + ss;
                    objTransactionModel.AvgWinningDay_str = str_avg_Winning_day == "W 0:0:0" ? "W --:--:--" : str_avg_Winning_day;
                    objTransactionModel.AvgLosingDay_str = str_avg_losing_day == "L 0:0:0" ? "L --:--:--" : str_avg_losing_day;
                    lstUserTransactionModel.Add(objTransactionModel);
                }
                //return lstUserTransactionModel;

                foreach (var item in lstUserTransactionModel.OrderBy(x => x.Transaction_date.Value))
                {
                    dTempHignPnlOverall = Convert.ToDecimal(dLastDayBalance + item.HighProfitLossOld);
                    dTempLowPnlOverall = Convert.ToDecimal(dLastDayBalance + item.LowProfitLossOld);

                    if (dHignPnlOverall < dTempHignPnlOverall)
                    {
                        dHignPnlOverall = dTempHignPnlOverall;
                        dTempLastDayHighPnl = dTempHignPnlOverall;
                        item.HighProfitLossOld = decimal.Round(dTempHignPnlOverall, 2, MidpointRounding.AwayFromZero);
                        item.HighProfitLoss = (item.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(dTempHignPnlOverall).ToString("#,##0.00");
                    }
                    else
                    {
                        item.HighProfitLossOld = decimal.Round(dTempLastDayHighPnl, 2, MidpointRounding.AwayFromZero);
                        item.HighProfitLoss = (dTempLastDayHighPnl < 0 ? "-" : "") + "$" + Math.Abs(dTempLastDayHighPnl).ToString("#,##0.00");

                    }
                    if (dLowPnlOverall > dTempLowPnlOverall)
                    {
                        dLowPnlOverall = dTempLowPnlOverall;
                        dTempLastDayLowPnl = dTempLowPnlOverall;
                        item.LowProfitLossOld = decimal.Round(dTempLowPnlOverall, 2, MidpointRounding.AwayFromZero);
                        item.LowProfitLoss = (dTempLowPnlOverall < 0 ? "-" : "") + "$" + Math.Abs(dTempLowPnlOverall).ToString("#,##0.00");
                    }
                    else
                    {
                        item.LowProfitLossOld = decimal.Round(dTempLastDayLowPnl, 2, MidpointRounding.AwayFromZero);
                        item.LowProfitLoss = (dTempLastDayLowPnl < 0 ? "-" : "") + "$" + Math.Abs(dTempLastDayLowPnl).ToString("#,##0.00");
                    }
                    dLastDayBalance = item.CurrentBalanceOld.Value;
                }

                if (Exchange == "All Markets")
                {
                    List<HomePageModel> result = lstUserTransactionModel
                    .GroupBy(x => x.Transaction_date)
                    .Select(cl => new HomePageModel
                    {
                        UserID = cl.First().UserID,
                        FirstName = cl.FirstOrDefault().FirstName,
                        LastName = cl.FirstOrDefault().LastName,
                        isActive = cl.FirstOrDefault().isActive,
                        account_number = cl.First().account_number,
                        freeTrial = cl.FirstOrDefault().freeTrial,
                        strFreeTrial = cl.FirstOrDefault().strFreeTrial,
                        TransactionDateString = cl.First().TransactionDateString,
                        CurrentBalanceOld = cl.First().CurrentBalanceOld,
                        CurrentBalance = cl.First().CurrentBalance,
                        NetProfitLossOld = decimal.Round(cl.Sum(c => c.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        NetProfitLoss = (cl.Sum(c => c.NetProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.NetProfitLossOld.Value)).ToString("#,##0.00"),
                        GrossProfitLossOld = decimal.Round(cl.Sum(c => c.GrossProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        GrossProfitLoss = (cl.Sum(c => c.GrossProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.GrossProfitLossOld.Value)).ToString("#,##0.00"),
                        HighProfitLossOld = decimal.Round(cl.Sum(c => c.HighProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        HighProfitLoss = (cl.Sum(c => c.HighProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.HighProfitLossOld.Value)).ToString("#,##0.00"),
                        LowProfitLossOld = decimal.Round(cl.Sum(c => c.LowProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        LowProfitLoss = (cl.Sum(c => c.LowProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.LowProfitLossOld.Value)).ToString("#,##0.00"),
                        TotalContracts = cl.Sum(c => c.TotalContracts),
                        TotalCommisionsOld = decimal.Round(cl.Sum(c => c.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero),
                        TotalCommisions = (cl.Sum(c => c.TotalCommisionsOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.TotalCommisionsOld.Value)).ToString("#,##0.00"),
                        TotalTrades = cl.Sum(c => c.TotalTrades),
                        AvgWinningTradeOld = decimal.Round(cl.Sum(c => c.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningTrade = (cl.Sum(c => c.AvgWinningTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningTradeOld.Value)).ToString("#,##0.00"),
                        AvgLosingTradeOld = decimal.Round(cl.Sum(c => c.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingTrade = (cl.Sum(c => c.AvgLosingTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingTradeOld.Value)).ToString("#,##0.00"),
                        WinningTradePerOld = cl.Average(c => c.WinningTradePerOld),
                        //WinningTradePerOld = decimal.Round(cl.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero),
                        WinningTradePer = (cl.Average(c => c.WinningTradePerOld) < 0 ? "-" : "") + Math.Abs(cl.Average(c => c.WinningTradePerOld.Value)).ToString("#,##0.00"),
                        MaxConsWin = cl.Sum(c => c.MaxConsWin),
                        MaxConsLoss = cl.Sum(c => c.MaxConsLoss),
                        AvgWinningDayOld = decimal.Round(cl.Average(c => c.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningDay = (cl.Sum(c => c.AvgWinningDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningDayOld.Value)).ToString("#,##0.00"),
                        AvgLosingDayOld = decimal.Round(cl.Average(c => c.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingDay = (cl.Sum(c => c.AvgLosingDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingDayOld.Value)).ToString("#,##0.00"),
                        AvgWinningDay_str = string.Empty,
                        AvgLosingDay_str = string.Empty,
                        Transaction_date = cl.First().Transaction_date,
                        UserSubscriptionID = user_subscription_id,//Added by Bharat
                    }).ToList();

                    List<HomePageModel> result1 = new List<HomePageModel>();
                    foreach (var item in result)
                    {
                        string StrTemp = "";
                        TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgWinningDayOld));
                        StrTemp = "W " + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
                        item.AvgWinningDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;
                        ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgLosingDayOld));
                        StrTemp = "L " + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
                        item.AvgLosingDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;
                        result1.Add(item);
                    }
                    lstUserTransactionModel = result1;
                }

                dHignPnlOverall = Convert.ToDecimal(lstUserTransactionModel.Max(x => x.HighProfitLossOld));
                dLowPnlOverall = Convert.ToDecimal(lstUserTransactionModel.Min(x => x.LowProfitLossOld));



                if (dHignPnlOverall < 0)
                {
                    dHignPnlOverall = 0;
                }
                if (dLowPnlOverall > 0)
                {
                    dLowPnlOverall = 0;
                }
                if (Exchange == "All Markets" && lstUserTransactionModel.Count() > 1)
                {
                    HomePageModel objmodel = new HomePageModel();
                    objmodel.FirstName = lstUserTransactionModel.FirstOrDefault().FirstName;
                    objmodel.LastName = lstUserTransactionModel.FirstOrDefault().LastName;
                    objmodel.account_number = lstUserTransactionModel.FirstOrDefault().account_number;
                    objmodel.freeTrial = lstUserTransactionModel.FirstOrDefault().freeTrial;
                    objmodel.strFreeTrial = lstUserTransactionModel.FirstOrDefault().strFreeTrial;
                    objmodel.TransactionDateString = "Overall";
                    objmodel.Exchange = string.Empty;
                    objmodel.Symbol = string.Empty;
                    objmodel.CurrentBalance = (lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value < 0 ? "-" : "") + "$" + Math.Abs(lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value).ToString("#,##0.00");
                    objmodel.CurrentBalanceOld = decimal.Round(lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value, 2, MidpointRounding.AwayFromZero);
                    objmodel.CurrentBalance = (objmodel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.CurrentBalanceOld.Value).ToString("#,##0.00");
                    objmodel.NetProfitLossOld = decimal.Round(lstUserTransactionModel.Sum(x => x.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.NetProfitLoss = (objmodel.NetProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.NetProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.GrossProfitLossOld = decimal.Round(lstUserTransactionModel.Sum(x => x.GrossProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.GrossProfitLoss = (objmodel.GrossProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.GrossProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.HighProfitLossOld = decimal.Round(dHignPnlOverall, 2, MidpointRounding.AwayFromZero);
                    objmodel.HighProfitLoss = (objmodel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.LowProfitLossOld = decimal.Round(dLowPnlOverall, 2, MidpointRounding.AwayFromZero);
                    objmodel.LowProfitLoss = (objmodel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.TotalContracts = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.TotalContracts));
                    objmodel.TotalCommisionsOld = decimal.Round(lstUserTransactionModel.Sum(x => x.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.TotalCommisions = (objmodel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objmodel.TotalTrades = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.TotalTrades));
                    objmodel.AvgWinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningTrade = (objmodel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.AvgLosingTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingTrade = (objmodel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTrade = (objmodel.WinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.WinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.MaxDrowdownOld = decimal.Round(lstUserTransactionModel.Average(x => x.MaxDrowdownOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.MaxDrowdown = (objmodel.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.MaxDrowdownOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTradePer = (objmodel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objmodel.LosingTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.LosingTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.LosingTradePer = (objmodel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.LosingTradePerOld.Value).ToString("#,##0.00");
                    objmodel.MaxConsWin = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.MaxConsWin));
                    objmodel.MaxConsLoss = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.MaxConsLoss));
                    objmodel.AvgLosingDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingDay = (objmodel.AvgLosingDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWinningDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningDay = (objmodel.AvgWinningDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWLDuration = "Avg. W/L Duration";
                    double avg_winning_day = Convert.ToDouble(objmodel.AvgWinningDay);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);
                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = "W " + hh + ":" + mm + ":" + ss;
                    double avg_losing_day = Convert.ToDouble(objmodel.AvgLosingDay);

                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = "L " + hh + ":" + mm + ":" + ss;
                    objmodel.AvgWinningDay_str = str_avg_Winning_day == "W 0:0:0" ? "W --:--:--" : str_avg_Winning_day;
                    objmodel.AvgLosingDay_str = str_avg_losing_day == "L 0:0:0" ? "L --:--:--" : str_avg_losing_day;
                    objmodel.UserSubscriptionID = user_subscription_id;//Added by Bharat
                    lstUserTransactionModel.Insert(0, objmodel);
                    lstUserTransactionModel.Reverse();
                    return lstUserTransactionModel;
                }
                return lstUserTransactionModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region User Overall Report List
        /// <summary>
        ///  Transaction List
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        ///  Date : 12/Dec/2018
        /// Dev By: Hardik Savaliya
        public List<HomePageModel> GetTTUserOverallReportList(out long NextSubId, string Sequence = "", Int64 userID = 0, string Exchange = "", Int64 userSubscriptionID = 0, string saccountallias = "")
        {
            try
            {
                ObjectParameter returnId = new ObjectParameter("nextsubid", typeof(int));
                List<HomePageModel> lstUserTransactionModel = new List<HomePageModel>();
                var user_subscription_id_list = dbConnection.user_subscription.Where(x => x.user_id == userID).Select(x => x.user_subscription_id).ToList();
                long user_subscription_id = userSubscriptionID;
                long user_subscription_id_ = userSubscriptionID;
                if (userSubscriptionID > 0)
                {
                    if (!string.IsNullOrEmpty(Sequence))
                    {
                        if (Sequence == "Prev")
                        {
                            user_subscription_id = user_subscription_id_list.TakeWhile(x => !x.Equals(userSubscriptionID)).LastOrDefault();
                        }
                        else if (Sequence == "Next")
                        {
                            user_subscription_id = user_subscription_id_list.SkipWhile(x => !x.Equals(userSubscriptionID)).Skip(1).FirstOrDefault();
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(Sequence))
                    {
                        user_subscription_id = dbConnection.user_subscription.Where(x => x.user_id == userID & x.is_current == true).Select(x => x.user_subscription_id).FirstOrDefault();
                    }
                }
                if (user_subscription_id == 0)
                {
                    user_subscription_id = user_subscription_id_;
                }
                List<tt_fill_master_manual> lstFillMaster = dbConnection.tt_fill_master_manual.Where(x =>
                                                  (user_subscription_id > 0 ? x.user_subscription_id == user_subscription_id : x.user_subscription_id == x.user_subscription_id)
                                                  && (x.user_id == userID)).OrderByDescending(x => x.date).ToList();
                if (!string.IsNullOrEmpty(saccountallias))
                {
                    lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).ToList();
                }
                else
                {
                    var lstCurrentPlatformObj = dbConnection.user_platform.Where(x => x.user_id == userID && x.is_active == true && x.is_delete != true).ToList();
                    if (lstCurrentPlatformObj.Count() > 0)
                    {
                        lstFillMaster = lstFillMaster.Where(x => x.account_no.Trim().ToLower().Equals(lstCurrentPlatformObj.FirstOrDefault().account_alias.Trim().ToLower())).ToList();
                    }
                }
                var lstFillMasterGroupDate = lstFillMaster.GroupBy(x => x.date.Value.Date).ToList();
                if (string.IsNullOrEmpty(Exchange))
                {
                    Exchange = "All Markets";
                }
                NextSubId = 1;
                NextSubId = user_subscription_id;
                HomePageModel objTransactionModel;
                Decimal dHignPnlOverall = 0;
                Decimal dLowPnlOverall = 0;
                Decimal dTempHignPnlOverall = 0;
                Decimal dTempLowPnlOverall = 0;
                Decimal dLastDayBalance = 0;
                Decimal dTempLastDayHighPnl = 0;
                Decimal dTempLastDayLowPnl = 0;
                foreach (var model in lstFillMasterGroupDate)
                {
                    objTransactionModel = new HomePageModel();
                    objTransactionModel.UserID = model.FirstOrDefault().user_id.Value;
                    objTransactionModel.FirstName = model.FirstOrDefault().user_master.first_name;
                    objTransactionModel.LastName = model.FirstOrDefault().user_master.last_name;
                    objTransactionModel.isActive = model.FirstOrDefault().user_subscription.is_current.Value;
                    objTransactionModel.freeTrial = model.FirstOrDefault().user_subscription.free_trial.Value;
                    if (model.FirstOrDefault().user_subscription.free_trial.Value)
                        objTransactionModel.strFreeTrial = "Demo";
                    else
                        objTransactionModel.strFreeTrial = "Chanllenge";
                    objTransactionModel.account_number = model.FirstOrDefault().account_no;
                    objTransactionModel.UserSubscriptionID = model.FirstOrDefault().user_subscription_id;
                    objTransactionModel.CurrentBalanceOld = decimal.Round(model.OrderBy(x => x.trade_number).FirstOrDefault().current_balance_pnl.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.CurrentBalance = (objTransactionModel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.CurrentBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.HighBalanceOld = decimal.Round(model.Max(x => x.realised_pnl_usd_with_commision).Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.HighBalance = (objTransactionModel.HighBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.LowBalanceOld = decimal.Round(model.Min(x => x.realised_pnl_usd_with_commision).Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.LowBalance = (objTransactionModel.LowBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowBalanceOld.Value).ToString("#,##0.00");
                    objTransactionModel.NetProfitLossOld = decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().cumulative_pnl.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.NetProfitLoss = (objTransactionModel.NetProfitLossOld < 0 ? "-" : "") + "$" + Convert.ToDecimal(Math.Abs(objTransactionModel.NetProfitLossOld.Value)).ToString("#,##0.00");
                    objTransactionModel.GrossProfitLossOld = decimal.Round(model.OrderBy(x => x.trade_number).LastOrDefault().cumalitve_pnl_without_commision.Value, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.GrossProfitLoss = (objTransactionModel.GrossProfitLossOld < 0 ? "-" : "") + "$" + Convert.ToDecimal(Math.Abs(objTransactionModel.GrossProfitLossOld.Value)).ToString("#,##0.00");
                    objTransactionModel.HighProfitLossOld = model.LastOrDefault().high_pnl != null ? decimal.Round(model.LastOrDefault().high_pnl.Value, 2, MidpointRounding.AwayFromZero) : 0;
                    objTransactionModel.HighProfitLoss = (objTransactionModel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objTransactionModel.LowProfitLossOld = model.LastOrDefault().low_pnl != null ? decimal.Round(model.LastOrDefault().low_pnl.Value, 2, MidpointRounding.AwayFromZero) : 0;
                    objTransactionModel.LowProfitLoss = (objTransactionModel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objTransactionModel.TotalContracts = Convert.ToInt32(model.Sum(x => x.fill_qty));
                    objTransactionModel.Transaction_date = model.Key;

                    //Total Commision and trade
                    List<int> TotalTrades = model.Select(x => x.trade_number).Distinct().ToList();
                    decimal dCommission = 0;
                    foreach (var item in TotalTrades)
                    {
                        dCommission = dCommission + model.Where(x => x.trade_number == item).FirstOrDefault().commission.Value;
                    }
                    objTransactionModel.TotalCommisionsOld = decimal.Round(dCommission, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.TotalCommisions = (objTransactionModel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objTransactionModel.TotalTrades = TotalTrades.Count();

                    //winning Trade
                    var WinnigTrade = model.Where(x => x.w_trades == 1).ToList();
                    List<int> lstWinnigTrade = WinnigTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dWinTotalAmount = 0;
                    decimal dWinAvgAmount = 0;
                    Double dblWinnigTime = 0;
                    Double dblAvgWinnigTime = 0;
                    foreach (var item in lstWinnigTrade)
                    {
                        dWinTotalAmount = dWinTotalAmount + model.Where(x => x.trade_number == item).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                        DateTime dblStartTime = model.Where(x => x.trade_number == item && x.trade_entry == 1).FirstOrDefault().time.Value;
                        DateTime dblEndTime = model.Where(x => x.trade_number == item && x.trade_exit == 1).FirstOrDefault().time.Value;
                        var diffInSeconds = (dblStartTime - dblEndTime).TotalSeconds;
                        dblWinnigTime = dblWinnigTime + Math.Abs(diffInSeconds);
                    }
                    if (dWinTotalAmount != 0)
                    {
                        dWinAvgAmount = (dWinTotalAmount) / lstWinnigTrade.Count();
                    }
                    if (dblWinnigTime != 0)
                    {
                        dblAvgWinnigTime = dblWinnigTime / lstWinnigTrade.Count();
                    }

                    //lossing Trade
                    var LossingTrade = model.Where(x => x.l_trades == 1).ToList();
                    List<int> lstLossingTrade = LossingTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dLossTotalAmount = 0;
                    decimal dLossAvgAmount = 0;
                    Double dblLossingTime = 0;
                    Double dblAvgLossingTime = 0;
                    foreach (var item in lstLossingTrade)
                    {
                        DateTime dblStartTime = System.DateTime.UtcNow;
                        DateTime dblEndTime = DateTime.UtcNow;
                        dLossTotalAmount = dLossTotalAmount + model.Where(x => x.trade_number == item).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                        if (model.Where(x => x.trade_number == item && x.trade_entry == 1).Any())
                            dblStartTime = model.Where(x => x.trade_number == item && x.trade_entry == 1).FirstOrDefault().time.Value;
                        if (model.Where(x => x.trade_number == item && x.trade_exit == 1).Any())
                            dblEndTime = model.Where(x => x.trade_number == item && x.trade_exit == 1).FirstOrDefault().time.Value;
                        var diffInSeconds = (dblStartTime - dblEndTime).TotalSeconds;
                        dblLossingTime = dblLossingTime + Math.Abs(diffInSeconds);
                    }
                    if (dLossTotalAmount != 0)
                    {
                        dLossAvgAmount = (dLossTotalAmount) / lstLossingTrade.Count();
                    }
                    if (dblLossingTime != 0)
                    {
                        dblAvgLossingTime = dblLossingTime / lstLossingTrade.Count();
                    }

                    //scratch Trade
                    var ScratchTrade = model.Where(x => x.s_trades == 1).ToList();
                    List<int> lstScratchTrade = ScratchTrade.Select(x => x.trade_number).Distinct().ToList();
                    decimal dScratchTotalAmount = 0;
                    decimal dScratchAvgAmount = 0;
                    foreach (var item in lstScratchTrade)
                    {
                        dScratchTotalAmount = dScratchTotalAmount + model.Where(x => x.trade_number == item).FirstOrDefault().realised_pnl_usd_with_commision.Value;
                    }
                    if (dScratchTotalAmount != 0)
                    {
                        dScratchAvgAmount = (dScratchTotalAmount) / lstScratchTrade.Count();
                    }

                    //Winning Trade Percentage
                    decimal dWinPercentage = 0;
                    if (lstWinnigTrade.Count != 0)
                    {
                        dWinPercentage = lstWinnigTrade.Count() * 100 / TotalTrades.Count();
                    }

                    objTransactionModel.AvgWinningTradeOld = decimal.Round(dWinAvgAmount, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgWinningTrade = (objTransactionModel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgLosingTradeOld = decimal.Round(dLossAvgAmount, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgLosingTrade = (objTransactionModel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objTransactionModel.TransactionDateString = model.FirstOrDefault().date.Value.ToString("MMM d");
                    objTransactionModel.MaxConsWin = lstWinnigTrade.Count();
                    objTransactionModel.MaxConsLoss = lstLossingTrade.Count();
                    objTransactionModel.WinningTradePerOld = decimal.Round(dWinPercentage, 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.WinningTradePer = (objTransactionModel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objTransactionModel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgLosingDayOld = decimal.Round(Convert.ToDecimal(dblAvgLossingTime), 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgLosingDay = (objTransactionModel.AvgLosingDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objTransactionModel.AvgWinningDayOld = decimal.Round(Convert.ToDecimal(dblAvgWinnigTime), 2, MidpointRounding.AwayFromZero);
                    objTransactionModel.AvgWinningDay = (objTransactionModel.AvgWinningDayOld < 0 ? "-" : "") + "$" + Math.Abs(objTransactionModel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objTransactionModel.Transaction_date = model.FirstOrDefault().date;
                    double avg_winning_day = Convert.ToDouble(dblAvgWinnigTime);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);
                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = "W " + hh + ":" + mm + ":" + ss;
                    double avg_losing_day = Convert.ToDouble(dblAvgLossingTime);
                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = "L " + hh + ":" + mm + ":" + ss;
                    objTransactionModel.AvgWinningDay_str = str_avg_Winning_day == "W 0:0:0" ? "W --:--:--" : str_avg_Winning_day;
                    objTransactionModel.AvgLosingDay_str = str_avg_losing_day == "L 0:0:0" ? "L --:--:--" : str_avg_losing_day;
                    lstUserTransactionModel.Add(objTransactionModel);
                }
                //return lstUserTransactionModel;

                foreach (var item in lstUserTransactionModel.OrderBy(x => x.Transaction_date.Value))
                {
                    dTempHignPnlOverall = Convert.ToDecimal(dLastDayBalance + item.HighProfitLossOld);
                    dTempLowPnlOverall = Convert.ToDecimal(dLastDayBalance + item.LowProfitLossOld);

                    if (dHignPnlOverall < dTempHignPnlOverall)
                    {
                        dHignPnlOverall = dTempHignPnlOverall;
                        dTempLastDayHighPnl = dTempHignPnlOverall;
                        item.HighProfitLossOld = decimal.Round(dTempHignPnlOverall, 2, MidpointRounding.AwayFromZero);
                        item.HighProfitLoss = (item.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(dTempHignPnlOverall).ToString("#,##0.00");
                    }
                    else
                    {
                        item.HighProfitLossOld = decimal.Round(dTempLastDayHighPnl, 2, MidpointRounding.AwayFromZero);
                        item.HighProfitLoss = (dTempLastDayHighPnl < 0 ? "-" : "") + "$" + Math.Abs(dTempLastDayHighPnl).ToString("#,##0.00");

                    }
                    if (dLowPnlOverall > dTempLowPnlOverall)
                    {
                        dLowPnlOverall = dTempLowPnlOverall;
                        dTempLastDayLowPnl = dTempLowPnlOverall;
                        item.LowProfitLossOld = decimal.Round(dTempLowPnlOverall, 2, MidpointRounding.AwayFromZero);
                        item.LowProfitLoss = (dTempLowPnlOverall < 0 ? "-" : "") + "$" + Math.Abs(dTempLowPnlOverall).ToString("#,##0.00");
                    }
                    else
                    {
                        item.LowProfitLossOld = decimal.Round(dTempLastDayLowPnl, 2, MidpointRounding.AwayFromZero);
                        item.LowProfitLoss = (dTempLastDayLowPnl < 0 ? "-" : "") + "$" + Math.Abs(dTempLastDayLowPnl).ToString("#,##0.00");
                    }


                    dLastDayBalance = item.CurrentBalanceOld.Value;
                }

                if (Exchange == "All Markets")
                {
                    List<HomePageModel> result = lstUserTransactionModel
                    .GroupBy(x => x.Transaction_date)
                    .Select(cl => new HomePageModel
                    {
                        UserID = cl.First().UserID,
                        FirstName = cl.FirstOrDefault().FirstName,
                        LastName = cl.FirstOrDefault().LastName,
                        isActive = cl.FirstOrDefault().isActive,
                        account_number = cl.First().account_number,
                        freeTrial = cl.FirstOrDefault().freeTrial,
                        strFreeTrial = cl.FirstOrDefault().strFreeTrial,
                        TransactionDateString = cl.First().TransactionDateString,
                        CurrentBalanceOld = cl.First().CurrentBalanceOld,
                        CurrentBalance = cl.First().CurrentBalance,
                        NetProfitLossOld = decimal.Round(cl.Sum(c => c.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        NetProfitLoss = (cl.Sum(c => c.NetProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.NetProfitLossOld.Value)).ToString("#,##0.00"),
                        GrossProfitLossOld = decimal.Round(cl.Sum(c => c.GrossProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        GrossProfitLoss = (cl.Sum(c => c.GrossProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.GrossProfitLossOld.Value)).ToString("#,##0.00"),
                        HighProfitLossOld = decimal.Round(cl.Sum(c => c.HighProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        HighProfitLoss = (cl.Sum(c => c.HighProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.HighProfitLossOld.Value)).ToString("#,##0.00"),
                        LowProfitLossOld = decimal.Round(cl.Sum(c => c.LowProfitLossOld.Value), 2, MidpointRounding.AwayFromZero),
                        LowProfitLoss = (cl.Sum(c => c.LowProfitLossOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.LowProfitLossOld.Value)).ToString("#,##0.00"),
                        TotalContracts = cl.Sum(c => c.TotalContracts),
                        TotalCommisionsOld = decimal.Round(cl.Sum(c => c.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero),
                        TotalCommisions = (cl.Sum(c => c.TotalCommisionsOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.TotalCommisionsOld.Value)).ToString("#,##0.00"),
                        TotalTrades = cl.Sum(c => c.TotalTrades),
                        AvgWinningTradeOld = decimal.Round(cl.Sum(c => c.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningTrade = (cl.Sum(c => c.AvgWinningTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningTradeOld.Value)).ToString("#,##0.00"),
                        AvgLosingTradeOld = decimal.Round(cl.Sum(c => c.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingTrade = (cl.Sum(c => c.AvgLosingTradeOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingTradeOld.Value)).ToString("#,##0.00"),
                        WinningTradePerOld = cl.Average(c => c.WinningTradePerOld),
                        //WinningTradePerOld = decimal.Round(cl.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero),
                        WinningTradePer = (cl.Average(c => c.WinningTradePerOld) < 0 ? "-" : "") + Math.Abs(cl.Average(c => c.WinningTradePerOld.Value)).ToString("#,##0.00"),
                        MaxConsWin = cl.Sum(c => c.MaxConsWin),
                        MaxConsLoss = cl.Sum(c => c.MaxConsLoss),
                        AvgWinningDayOld = decimal.Round(cl.Average(c => c.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgWinningDay = (cl.Sum(c => c.AvgWinningDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgWinningDayOld.Value)).ToString("#,##0.00"),
                        AvgLosingDayOld = decimal.Round(cl.Average(c => c.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero),
                        AvgLosingDay = (cl.Sum(c => c.AvgLosingDayOld) < 0 ? "-" : "") + "$" + Math.Abs(cl.Sum(c => c.AvgLosingDayOld.Value)).ToString("#,##0.00"),
                        AvgWinningDay_str = string.Empty,
                        AvgLosingDay_str = string.Empty,
                        Transaction_date = cl.First().Transaction_date,
                        UserSubscriptionID = user_subscription_id,//Added by Bharat
                    }).ToList();

                    List<HomePageModel> result1 = new List<HomePageModel>();
                    foreach (var item in result)
                    {
                        string StrTemp = "";
                        TimeSpan ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgWinningDayOld));
                        StrTemp = "W " + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
                        item.AvgWinningDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;
                        ts = TimeSpan.FromSeconds(Convert.ToDouble(item.AvgLosingDayOld));
                        StrTemp = "L " + ts.Hours + ":" + ts.Minutes + ":" + ts.Seconds;
                        item.AvgLosingDay_str = StrTemp == "0:0:0" ? "--:--:--" : StrTemp;
                        result1.Add(item);
                    }
                    lstUserTransactionModel = result1;
                }

                dHignPnlOverall = Convert.ToDecimal(lstUserTransactionModel.Max(x => x.HighProfitLossOld));
                dLowPnlOverall = Convert.ToDecimal(lstUserTransactionModel.Min(x => x.LowProfitLossOld));



                if (dHignPnlOverall < 0)
                {
                    dHignPnlOverall = 0;
                }
                if (dLowPnlOverall > 0)
                {
                    dLowPnlOverall = 0;
                }
                if (Exchange == "All Markets" && lstUserTransactionModel.Count() > 1)
                {
                    HomePageModel objmodel = new HomePageModel();
                    objmodel.TransactionDateString = "Overall";
                    objmodel.Exchange = string.Empty;
                    objmodel.Symbol = string.Empty;
                    objmodel.CurrentBalance = (lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value < 0 ? "-" : "") + "$" + Math.Abs(lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value).ToString("#,##0.00");
                    objmodel.CurrentBalanceOld = decimal.Round(lstUserTransactionModel.FirstOrDefault().CurrentBalanceOld.Value, 2, MidpointRounding.AwayFromZero);
                    objmodel.CurrentBalance = (objmodel.CurrentBalanceOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.CurrentBalanceOld.Value).ToString("#,##0.00");
                    objmodel.NetProfitLossOld = decimal.Round(lstUserTransactionModel.Sum(x => x.NetProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.NetProfitLoss = (objmodel.NetProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.NetProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.GrossProfitLossOld = decimal.Round(lstUserTransactionModel.Sum(x => x.GrossProfitLossOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.GrossProfitLoss = (objmodel.GrossProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.GrossProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.HighProfitLossOld = decimal.Round(dHignPnlOverall, 2, MidpointRounding.AwayFromZero);
                    objmodel.HighProfitLoss = (objmodel.HighProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.HighProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.LowProfitLossOld = decimal.Round(dLowPnlOverall, 2, MidpointRounding.AwayFromZero);
                    objmodel.LowProfitLoss = (objmodel.LowProfitLossOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.LowProfitLossOld.Value).ToString("#,##0.00");
                    objmodel.TotalContracts = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.TotalContracts));
                    objmodel.TotalCommisionsOld = decimal.Round(lstUserTransactionModel.Sum(x => x.TotalCommisionsOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.TotalCommisions = (objmodel.TotalCommisionsOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.TotalCommisionsOld.Value).ToString("#,##0.00");
                    objmodel.TotalTrades = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.TotalTrades));
                    objmodel.AvgWinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningTrade = (objmodel.AvgWinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgWinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.AvgLosingTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingTrade = (objmodel.AvgLosingTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.AvgLosingTradeOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradeOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradeOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTrade = (objmodel.WinningTradeOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.WinningTradeOld.Value).ToString("#,##0.00");
                    objmodel.MaxDrowdownOld = decimal.Round(lstUserTransactionModel.Average(x => x.MaxDrowdownOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.MaxDrowdown = (objmodel.MaxDrowdownOld < 0 ? "-" : "") + "$" + Math.Abs(objmodel.MaxDrowdownOld.Value).ToString("#,##0.00");
                    objmodel.WinningTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.WinningTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.WinningTradePer = (objmodel.WinningTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.WinningTradePerOld.Value).ToString("#,##0.00");
                    objmodel.LosingTradePerOld = decimal.Round(lstUserTransactionModel.Average(x => x.LosingTradePerOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.LosingTradePer = (objmodel.LosingTradePerOld < 0 ? "-" : "") + Math.Abs(objmodel.LosingTradePerOld.Value).ToString("#,##0.00");
                    objmodel.MaxConsWin = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.MaxConsWin));
                    objmodel.MaxConsLoss = Convert.ToInt32(lstUserTransactionModel.Sum(x => x.MaxConsLoss));
                    objmodel.AvgLosingDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgLosingDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgLosingDay = (objmodel.AvgLosingDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgLosingDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWinningDayOld = decimal.Round(lstUserTransactionModel.Average(x => x.AvgWinningDayOld.Value), 2, MidpointRounding.AwayFromZero);
                    objmodel.AvgWinningDay = (objmodel.AvgWinningDayOld < 0 ? "-" : "") + Math.Abs(objmodel.AvgWinningDayOld.Value).ToString("#,##0.00");
                    objmodel.AvgWLDuration = "Avg. W/L Duration";
                    double avg_winning_day = Convert.ToDouble(objmodel.AvgWinningDay);
                    TimeSpan time_avg_winning_day = TimeSpan.FromSeconds(avg_winning_day);
                    int hh = time_avg_winning_day.Hours;
                    int mm = time_avg_winning_day.Minutes;
                    int ss = time_avg_winning_day.Seconds;
                    int ms = time_avg_winning_day.Milliseconds;
                    string str_avg_Winning_day = "W " + hh + ":" + mm + ":" + ss;
                    double avg_losing_day = Convert.ToDouble(objmodel.AvgLosingDay);

                    TimeSpan time_avg_losing_day = TimeSpan.FromSeconds(avg_losing_day);
                    mm = time_avg_losing_day.Minutes;
                    hh = time_avg_losing_day.Hours;
                    ss = time_avg_losing_day.Seconds;
                    ms = time_avg_losing_day.Milliseconds;
                    string str_avg_losing_day = "L " + hh + ":" + mm + ":" + ss;
                    objmodel.AvgWinningDay_str = str_avg_Winning_day == "W 0:0:0" ? "W --:--:--" : str_avg_Winning_day;
                    objmodel.AvgLosingDay_str = str_avg_losing_day == "L 0:0:0" ? "L --:--:--" : str_avg_losing_day;
                    objmodel.UserSubscriptionID = user_subscription_id;//Added by Bharat
                    lstUserTransactionModel.Insert(0, objmodel);
                    lstUserTransactionModel.Reverse();
                    return lstUserTransactionModel;
                }

                return lstUserTransactionModel;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}
