﻿using System;
using System.Collections.Generic;
using System.Linq;
using Traderdock.Common;
using Traderdock.Common.Constants;
using Traderdock.Common.Enumerations;
using Traderdock.Common.Exceptions;
using Traderdock.Entity.MemberModel;
using Traderdock.Entity.Model;
using Traderdock.Entity.ViewModel;
using Traderdock.Model.Model;
using Traderdock.ORM;

namespace Traderdock.Services
{
    public class UserService
    {
        #region Variable
        Traderdock_DBEntities dbConnection = new Traderdock_DBEntities();
        #endregion

        #region User List
        public PagedList<UserDashboardModel> DashBoardUserList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<UserDashboardModel> lstUserModel = new List<UserDashboardModel>();
                //Linq Query
                var query =
                        from UM in dbConnection.user_master
                          .Where(um => um.user_type_id == (Int32)EnumUserType.Normal && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where UM.is_deleted == false
                        orderby UM.created_on descending
                        select new
                        {
                            UserId = UM.user_id,
                            FirstName = UM.first_name,
                            LastName = UM.last_name,
                            Email = UM.user_email,
                            SubscriptionMode = UM.subscription_mode,
                            SubscriptionType = dbConnection.subscription_master.Where(x => x.subscription_id == ((UM.user_subscription.Where(y => y.user_id == UM.user_id && y.is_current == true).Select(y => y.subscription_id)).FirstOrDefault())),
                            JoinDate = UM.created_on,
                            //ContactNumber = UM.contact_number,
                            UserStatus = UM.is_active,
                        };

                if (query != null)
                {
                    foreach (var model in query)
                    {
                        var objUserDashboardModel = new UserDashboardModel
                        {
                            user_id = model.UserId,
                            first_name = model.FirstName,
                            last_name = model.LastName,
                            SubscriptionModeString = model.SubscriptionType.ToList().Select(x => x.subscription_name)
                                .FirstOrDefault(),
                            join_date = model.JoinDate.ToString("dd-MM-yyyy"),
                            Subscription_count = dbConnection.user_subscription.Where(x => x.user_id == model.UserId)
                                .Count()
                        };
                        //objUserDashboardModel.SubscriptionModeString = model.SubscriptionMode == (Int32)EnumSubscriptionMode.Free_Trial ? EnumSubscriptionMode.Free_Trial.ToString() : EnumSubscriptionMode.Subscribed.ToString();
                        //objUserModel.user_status = model.UserStatus == true ? "Active" : "Inactive";
                        //objUserDashboardModel.UserStatus = model.UserStatus;
                        lstUserModel.Add(objUserDashboardModel);
                    }
                    var result = new PagedList<UserDashboardModel>(lstUserModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region User List
        public PagedList<UserMasterModel> UserList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", Int64 UserId = 0)
        {
            try
            {
                IList<UserMasterModel> lstUserModel = new List<UserMasterModel>();
                var query =
                        from UM in dbConnection.user_master
                          .Where(um => (um.first_name.Contains(filter) || um.last_name.Contains(filter) || um.user_email.Contains(filter) || um.contact_number.Contains(filter)) && um.user_type_id == (Int32)EnumUserType.Normal && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where UM.is_deleted == false
                        orderby UM.created_on descending
                        select new
                        {
                            UserId = UM.user_id,
                            FirstName = UM.first_name,
                            LastName = UM.last_name,
                            user_subscription = UM.user_subscription.Where(X => X.is_current == true),
                            AccountNumber = UM.account_number,
                            Email = UM.user_email,
                            SubscriptionMode = UM.subscription_mode,
                            //CurrentTradingBalance=UM.user_subscription.Where(x=>x.is_current==true).FirstOrDefault().balance_history.OrderByDescending(x=>x.transaction_date).FirstOrDefault().current_balance,
                            SubscriptionType = dbConnection.subscription_master.Where(x => x.subscription_id == ((UM.user_subscription.Where(y => y.user_id == UM.user_id && y.is_current == true).FirstOrDefault().subscription_id))),
                            JoinDate = UM.created_on,
                            Contact_Number = UM.contact_number,
                            //ContactNumber = UM.contact_number,
                            TraingPlatformID = UM.trading_platform,
                            UserStatus = UM.is_active,
                            Address = UM.address,
                        };

                UserMasterModel objUserModel;
                if (query != null)
                {
                    ReportService objReportService = new ReportService();
                    List<OverallUserReportModel> lstOverallUserReportModel = new List<OverallUserReportModel>();
                    lstOverallUserReportModel = objReportService.GetDiscountedOverallReportList();
                    List<ReportTransactionModel> lstReportTransactionModel = new List<ReportTransactionModel>();
                    lstReportTransactionModel = objReportService.GetTTUserTransactionReportList();
                    foreach (var model in query)
                    {
                        objUserModel = new UserMasterModel()
                        {
                            user_id = model.UserId,
                            first_name = model.FirstName,
                            last_name = model.LastName,
                            user_email = model.Email
                        };
                        if (model.SubscriptionType.ToList().Any())
                        {
                            objUserModel.subscription_id = model.SubscriptionType.ToList().Select(x => x.subscription_id).FirstOrDefault();
                        }
                        objUserModel.trading_platform = model.TraingPlatformID;
                        objUserModel.trading_platform_string = model.TraingPlatformID == 1 ? "Rithmic" : "Trading Techonology";
                        objUserModel.account_number = model.AccountNumber;
                        if (model.user_subscription.Count() > 0)
                        {
                            objUserModel.subscription_id = model.user_subscription.FirstOrDefault().subscription_id;
                            objUserModel.last_payment_date = model.user_subscription.FirstOrDefault().purchase_date;
                            objUserModel.next_payment_date = model.user_subscription.FirstOrDefault().subscription_end_date;
                            objUserModel.next_payment_date_str = model.user_subscription.FirstOrDefault().subscription_end_date.Value.ToString("dd-MM-yyyy");
                            if (model.user_subscription.FirstOrDefault().balance_history.Count > 0)
                                objUserModel.current_trading_balance = model.user_subscription.FirstOrDefault().balance_history.OrderByDescending(x => x.transaction_date).FirstOrDefault().current_balance;
                        }
                        objUserModel.SubscriptionModeString = model.SubscriptionType.ToList().Select(x => x.subscription_name).FirstOrDefault();
                        if (model.user_subscription.ToList().OrderByDescending(X => X.user_subscription_id).Select(x => x.free_trial).FirstOrDefault() != null)
                        {
                            objUserModel.user_plan = model.user_subscription.ToList().OrderByDescending(X => X.user_subscription_id).Select(x => x.free_trial).FirstOrDefault().Value;
                            objUserModel.user_plan_string = objUserModel.user_plan == true ? "T" : "CH";
                        }
                        objUserModel.join_date = model.JoinDate.ToString("dd-MM-yyyy");
                        objUserModel.join_date_original = model.JoinDate;
                        objUserModel.UserStatus = model.UserStatus;
                        objUserModel.mobile_number = model.Contact_Number;
                        objUserModel.address = model.Address;
                        if (lstOverallUserReportModel.Count() > 0)
                        {
                            if (lstOverallUserReportModel.Any(x => x.userID == model.UserId))
                            {
                                objUserModel.discounted_balance = lstOverallUserReportModel.Where(x => x.userID == model.UserId).FirstOrDefault().DiscountedBalOld;
                            }
                        }
                        if (lstReportTransactionModel.Count() > 0)
                        {
                            if (lstReportTransactionModel.Any(x => x.userID == model.UserId))
                            {
                                objUserModel.net_pnl_without_commision = lstReportTransactionModel.Where(x => x.userID == model.UserId).LastOrDefault().NetProfitLossOld;
                                objUserModel.cumalative_balance = lstReportTransactionModel.Where(x => x.userID == model.UserId).LastOrDefault().CurrentBalanceOld;
                            }
                        }
                        
                        lstUserModel.Add(objUserModel);
                    }
                    var result = new PagedList<UserMasterModel>(lstUserModel, pageIndex, pageSize);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Error Registered UserList
        /// <summary>
        /// Listing of those users who registered with some error
        /// </summary>
        /// <param name="filter"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="statusFilter"></param>
        /// <param name="users"></param>
        /// <returns></returns>
        public PagedList<UserMasterModel> ErrorRegisteredUserList(string filter = "", int pageIndex = 0, int pageSize = int.MaxValue, string statusFilter = "", List<string> users = null)
        {
            try
            {
                IList<UserMasterModel> lstUserModel = new List<UserMasterModel>();
                char[] CharSeparator = new char[1];
                CharSeparator[0] = ',';
                //Linq Query
                var query =
                        from UM in dbConnection.user_master
                            //.Where(um => um.first_name.Contains(filter) && um.user_type_id == (Int32)EnumUserType.Normal && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? um.is_active == true || um.is_active == false : ((statusFilter == "Yes") ? um.is_active == true : um.is_active == false))).DefaultIfEmpty()
                        where users.Contains(UM.user_id.ToString()) && string.IsNullOrEmpty(UM.account_number) && UM.first_name.Contains(filter) && UM.user_type_id == (Int32)EnumUserType.Normal && ((string.IsNullOrEmpty(statusFilter) || statusFilter == "All") ? UM.is_active == true || UM.is_active == false : ((statusFilter == "Yes") ? UM.is_active == true : UM.is_active == false))
                        where UM.is_deleted == false
                        orderby UM.created_on descending
                        select new
                        {

                            UserId = UM.user_id,
                            Email = UM.user_email,
                            FirstName = UM.first_name,
                            LastName = UM.last_name,
                            Address = UM.address,
                            City = UM.city_name,
                            Country = UM.country_master.country_name,
                            State = UM.state_name,
                            Postal = UM.postal_code,
                            ContactNumber = UM.contact_number,
                            UserStatus = UM.is_active,
                        };

                if (query != null)
                {
                    foreach (var model in query)
                    {
                        var objUserModel = new UserMasterModel
                        {
                            user_id = model.UserId,
                            user_email = model.Email,
                            first_name = model.FirstName,
                            last_name = model.LastName,
                            address =
                                !string.IsNullOrEmpty(model.Address) ? model.Address.Split(CharSeparator)[0] : "",
                            city_name = model.City,
                            country_name = model.Country,
                            state_name = model.State,
                            postal_code = model.Postal,
                            mobile_number = model.ContactNumber,
                            UserStatus = model.UserStatus
                        };
                        //objUserModel.SubscriptionModeString = model.SubscriptionMode == (Int32)EnumSubscriptionMode.Free_Trial ? EnumSubscriptionMode.Free_Trial.ToString() : EnumSubscriptionMode.Subscribed.ToString();
                        //objUserModel.SubscriptionModeString = model.SubscriptionType.ToList().Select(x => x.subscription_name).FirstOrDefault();
                        //objUserModel.join_date = model.JoinDate.ToString("dd-MM-yyyy");
                        //objUserModel.user_status = model.UserStatus == true ? "Active" : "Inactive";
                        lstUserModel.Add(objUserModel);
                    }
                    var result = new PagedList<UserMasterModel>(lstUserModel, 0, 10);
                    return result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Add/Edit User
        public bool SaveUserImage(long userID, string fileName)
        {
            bool Status = false;
            user_master UserDetails = dbConnection.user_master.Where(x => x.user_id == userID).FirstOrDefault();
            if (UserDetails != null)
            {
                UserDetails.profile_image_url = Utilities.GetImageName(fileName);
                dbConnection.SaveChanges();
                Status = true;
            }
            return Status;
        }
        public bool CheckAdminUser()
        {
            bool Status = false;
            var objuser_master = new user_master();
            int Count = dbConnection.user_master.Where(x => x.user_type_id == (Int32)EnumUserType.Admin).Count();
            if (Count < 1)
            {
                var countryMaster = new country_master { country_name = "India", country_code = "+91" };
                dbConnection.country_master.Add(countryMaster);
                dbConnection.SaveChanges();
                objuser_master.first_name = "Admin_FirstName";
                objuser_master.last_name = "Admin_LastName";
                objuser_master.user_email = "netpeerbits@gmail.com";
                objuser_master.crypto_key = Utilities.GenerateKey();
                objuser_master.password = Utilities.EncryptPassword("123456", objuser_master.crypto_key);
                objuser_master.gender = true;
                objuser_master.birth_date = DateTime.UtcNow;
                objuser_master.state_name = "Gujarat";
                objuser_master.city_name = "Ahmedabad";
                objuser_master.country_id = 1;
                objuser_master.is_active = true;
                objuser_master.is_blocked = false;
                objuser_master.is_deleted = false;
                objuser_master.user_type_id = (Int32)EnumUserType.Admin;
                objuser_master.subscription_mode = (Int32)EnumSubscriptionMode.Subscribed;
                objuser_master.is_number_verified = false;
                objuser_master.is_admin_verified = true;
                objuser_master.created_by = 0;
                objuser_master.created_on = DateTime.UtcNow;
                dbConnection.user_master.Add(objuser_master);
                dbConnection.SaveChanges();
                Status = true;

            }
            return Status;
        }
        public bool AddEditUser(UserMasterModel objUserModel)
        {
            try
            {
                bool bFlag = false;
                if (objUserModel.user_id > 0)
                {
                    user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == objUserModel.user_id).FirstOrDefault();
                    objUserMaster.first_name = objUserModel.first_name;
                    objUserMaster.last_name = objUserModel.last_name;
                    objUserMaster.user_email = objUserModel.user_email;
                    objUserMaster.gender = objUserModel.gender;
                    if (objUserModel.dob != null)
                        objUserMaster.birth_date = Convert.ToDateTime(objUserModel.dob);

                    objUserMaster.city_name = objUserModel.city_name;
                    objUserMaster.state_name = objUserModel.state_name;
                    objUserMaster.country_id = objUserModel.country_id;
                    objUserMaster.address = objUserModel.address;
                    objUserMaster.account_number = objUserModel.account_number;
                    objUserMaster.trading_platform = objUserModel.trading_platform;
                    objUserMaster.postal_code = objUserModel.postal_code;
                    objUserMaster.contact_number = objUserModel.mobile_number;
                    if (objUserModel.profile_image_url != null)
                        objUserMaster.profile_image_url = Utilities.GetImageName(objUserModel.profile_image_url);
                    objUserMaster.modified_on = objUserModel.modified_on;
                    objUserMaster.modified_by = objUserModel.modified_by;
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                else
                {

                    user_master objUserMaster = new user_master();
                    objUserMaster.first_name = objUserModel.first_name;
                    objUserMaster.last_name = objUserModel.last_name;
                    objUserMaster.user_email = objUserModel.user_email;
                    objUserMaster.crypto_key = Utilities.GenerateKey();
                    objUserMaster.password = Utilities.EncryptPassword(objUserModel.password, objUserMaster.crypto_key);
                    objUserMaster.gender = objUserModel.gender;
                    objUserMaster.birth_date = Convert.ToDateTime(objUserModel.dob);
                    objUserMaster.city_name = objUserModel.city_name;
                    objUserMaster.state_name = objUserModel.state_name;
                    objUserMaster.country_id = objUserModel.country_id;
                    objUserMaster.address = objUserModel.address;
                    objUserMaster.postal_code = objUserModel.postal_code;
                    objUserMaster.contact_number = objUserModel.mobile_number;
                    objUserMaster.profile_image_url = Utilities.GetImageName(objUserModel.profile_image_url);
                    objUserMaster.is_active = true;//Default False need Email Account Confirmation
                    objUserMaster.is_blocked = false;
                    objUserMaster.is_deleted = false;
                    objUserMaster.user_type_id = (Int32)EnumUserType.Normal;
                    objUserMaster.subscription_mode = (Int32)EnumSubscriptionMode.Free_Trial;
                    objUserMaster.is_number_verified = false;
                    objUserMaster.is_admin_verified = true;
                    objUserMaster.created_on = objUserModel.created_on;
                    objUserMaster.created_by = objUserModel.created_by;
                    dbConnection.user_master.Add(objUserMaster);
                    dbConnection.SaveChanges();
                    bFlag = true;
                }
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UpdateUserAccountDetails(string Email, string FCM, string IB, string Account)
        {
            bool Status = false;
            CommonService commonServiceobj = new CommonService();
            user_master UserDetails = dbConnection.user_master.Where(x => x.user_email == Email).FirstOrDefault();
            if (UserDetails != null)
            {
                UserDetails.account_number = Account;
                UserDetails.FCM = FCM;
                UserDetails.IB = IB;
                dbConnection.SaveChanges();
                Status = true;
                if (!string.IsNullOrEmpty(UserDetails.user_email))
                {
                    string first_name = !string.IsNullOrEmpty(UserDetails.first_name) ? UserDetails.first_name : "";
                    string last_name = !string.IsNullOrEmpty(UserDetails.last_name) ? " " + UserDetails.last_name : "";
                    string Full_name = first_name;
                    if (Full_name.Length < 2)
                    {
                        Full_name = "User";
                    }
                    Dictionary<string, string> parameters = new Dictionary<string, string>();
                    parameters.Add(EmailTemplateParameters.FullName, Full_name);//userMaster.first_name);
                    parameters.Add(EmailTemplateParameters.Email, UserDetails.user_email);
                    parameters.Add(EmailTemplateParameters.Password, Utilities.DecryptPassword(UserDetails.password, UserDetails.crypto_key));
                    try
                    {
                        bool emailResponse = commonServiceobj.SendEmail(UserDetails.user_email, EmailTemplateNames.RithmicAccount, parameters, null);
                    }
                    catch (Exception)
                    {
                    }

                }
            }
            return Status;
        }
        #endregion

        #region Get Users
        public List<user_master> GetUsers(List<Int64> iDs)
        {
            try
            {
                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where iDs.Contains(UM.user_id)
                                                   select UM).ToList();
                return objUserMaster;
            }
            catch (Exception)
            {
                throw;
            }

        }
        public UserMasterModel GetUser(Int64? id)
        {
            try
            {
                user_master UserDetails = dbConnection.user_master.Where(x => x.user_id == id).FirstOrDefault();
                if (UserDetails != null)
                {
                    UserMasterModel objUserModel = new UserMasterModel();
                    objUserModel = new UserMasterModel();
                    objUserModel.user_id = UserDetails.user_id;
                    objUserModel.first_name = UserDetails.first_name;
                    objUserModel.last_name = UserDetails.last_name;
                    objUserModel.user_email = UserDetails.user_email;
                    objUserModel.join_date = UserDetails.created_on.ToString("dd-MM-yyyy");
                    if (UserDetails.gender != null)
                    {
                        objUserModel.gender = UserDetails.gender;//EnumGenderType
                        objUserModel.gender_String = UserDetails.gender == true ? "Male" : "Female";
                    }
                    else
                    {
                        objUserModel.gender_String = "Not Defined";
                    }
                    if (UserDetails.birth_date != null)
                    {
                        objUserModel.birth_date = UserDetails.birth_date.Value.ToString("dd-MM-yyyy");
                    }
                    objUserModel.city_name = UserDetails.city_name;
                    objUserModel.state_name = UserDetails.state_name;
                    //not proper condition
                    //if (UserDetails.country_id != null)
                    if (UserDetails.country_id > 0)
                    {
                        objUserModel.country_id = UserDetails.country_id.Value;
                    }
                    objUserModel.address = UserDetails.address;
                    objUserModel.postal_code = UserDetails.postal_code;
                    objUserModel.profile_image_url = !string.IsNullOrEmpty(UserDetails.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + UserDetails.profile_image_url : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objUserModel.FileName = !string.IsNullOrEmpty(UserDetails.profile_image_url) ? Utilities.GetImagePath() + "/Images/" + UserDetails.profile_image_url : Utilities.GetImagePath() + "/Images/Default_profile.png";
                    objUserModel.mobile_number = UserDetails.contact_number;
                    objUserModel.user_status = UserDetails.is_active == true ? "Active" : "Inactive";
                    objUserModel.is_active = UserDetails.is_active;
                    objUserModel.is_blocked = UserDetails.is_blocked;
                    objUserModel.is_deleted = UserDetails.is_deleted;
                    objUserModel.is_admin_verified = UserDetails.is_admin_verified;
                    objUserModel.is_number_verified = UserDetails.is_number_verified;
                    objUserModel.crypto_key = UserDetails.crypto_key;
                    objUserModel.account_number = UserDetails.account_number;
                    objUserModel.password = Utilities.DecryptPassword(UserDetails.password, UserDetails.crypto_key);
                    objUserModel.email_sent_count = UserDetails.email_sent_count != null ? UserDetails.email_sent_count : 0;
                    if (UserDetails.trading_platform != null)
                    {
                        objUserModel.trading_platform = UserDetails.trading_platform;//EnumGenderType
                        objUserModel.trading_platform_string = UserDetails.trading_platform == 1 ? "Rithmic" : "Trading Techonologies";
                    }
                    return objUserModel;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public bool ActiveInactive(List<long> lIds, long LoginUserId, bool IsActiveRequest)
        {
            try
            {
                bool bResult = false;

                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where (lIds).Contains(UM.user_id)
                                                   select UM).AsEnumerable().ToList();
                objUserMaster.ForEach(x => x.modified_by = LoginUserId);
                objUserMaster.ForEach(x => x.modified_on = DateTime.UtcNow);
                objUserMaster.ForEach(x => x.is_active = IsActiveRequest);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Delete User
        public bool DeleteUsers(IList<user_master> objUserMasterModelList)
        {
            try
            {
                bool bResult = false;
                var Ids = objUserMasterModelList.Select(x => x.user_id).ToList();
                List<user_master> objUserMaster = (from UM in dbConnection.user_master
                                                   where (Ids).Contains(UM.user_id)
                                                   select UM).ToList();
                objUserMaster.ForEach(x => x.is_deleted = true);
                dbConnection.SaveChanges();
                bResult = true;
                return bResult;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetSubscriptionAsExpired(long Id)
        {
            try
            {
                var objSubscriptionInfo = dbConnection.user_subscription.Where(x => x.user_subscription_id == Id).FirstOrDefault();
                if (objSubscriptionInfo != null)
                {
                    objSubscriptionInfo.is_expired = true;
                    dbConnection.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Check User Email Exists
        public bool CheckUserEmailExist(string email)
        {
            try
            {
                var userMaster = dbConnection.user_master.Where(x => x.user_email == email && x.is_deleted == false).FirstOrDefault();
                bool bFlag = (userMaster != null) ? true : false;
                return bFlag;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Login
        public UserMasterModel Login(LoginModel objLoginModel)
        {
            UserMasterModel objUserModel = new UserMasterModel();
            user_master objUserMaster = dbConnection.user_master.Where(x => x.user_type_id == (Int32)EnumUserType.Admin && (x.user_email == objLoginModel.Email)).FirstOrDefault();
            if (objUserMaster != null)
            {
                var password = Utilities.DecryptPassword(objUserMaster.password, objUserMaster.crypto_key);
                if (password == objLoginModel.Password)
                {
                    objUserModel = GetUser(objUserMaster.user_id);
                }
                else
                {
                    objUserModel.is_password_false = true;
                }
            }
            else
            {
                objUserModel.is_email_false = true;
            }
            return objUserModel;
        }
        #endregion

        #region Forgot/Change Password
        public UserMasterModel ForgotPassword(string email)
        {
            UserMasterModel objUserModel = new UserMasterModel();
            user_master objUserMaster = dbConnection.user_master.Where(x => x.user_email.Equals(email) && x.user_type_id == (int)EnumUserType.Admin && x.is_active).FirstOrDefault();
            if (objUserMaster != null)
            {
                objUserModel = GetUser(objUserMaster.user_id);
            }
            return objUserModel;
        }
        public UserMasterModel AdminPasswordReset(string e, string k)
        {
            UserMasterModel objUserMasterModel = new UserMasterModel();
            user_master userMaster = dbConnection.user_master.Where(x => x.user_email.Equals(e) && x.user_type_id == (int)EnumUserType.Admin && x.is_active).FirstOrDefault();
            if (userMaster != null)
            {
                objUserMasterModel = GetUser(userMaster.user_id);
            }
            return objUserMasterModel;

        }
        public bool AdminPasswordReset(UserPasswordModel objUserPasswordModel)
        {
            bool Status = false;
            user_master objuser_master = dbConnection.user_master.Where(x => x.user_email.Equals(objUserPasswordModel.email) && x.is_active).FirstOrDefault();
            if (objuser_master != null)
            {
                if (objUserPasswordModel.new_password == objUserPasswordModel.confirm_password)
                {
                    objuser_master.crypto_key = Utilities.GenerateKey();
                    objuser_master.password = Utilities.EncryptPassword(objUserPasswordModel.new_password, objuser_master.crypto_key);
                    //objstore_master.password = storepassModel.new_password;
                    dbConnection.SaveChanges();
                    Status = true;
                }
            }
            return Status;
        }
        public bool AdminPasswordChange(Entity.Model.ChangePasswordModel objChangePasswordModel, long lUserId)
        {
            bool Status = false;
            user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == lUserId).FirstOrDefault();
            if (objUserMaster != null)
            {
                string OLD_PASSWORD = Utilities.DecryptPassword(objUserMaster.password, objUserMaster.crypto_key);
                if (OLD_PASSWORD.Equals(objChangePasswordModel.old_password))
                {
                    objUserMaster.crypto_key = Utilities.GenerateKey();
                    objUserMaster.password = Utilities.EncryptPassword(objChangePasswordModel.new_password, objUserMaster.crypto_key);
                    dbConnection.SaveChanges();
                    Status = true;
                }
            }
            return Status;

        }
        #endregion

        #region csv to DB
        /// <summary>
        /// csv to DB
        /// dev by : Bharat Katua
        /// date : 26/07/2017
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public UserTransactionModel csvtodb(string[] data, string Transection_date)
        {
            UserTransactionModel model = new UserTransactionModel();

            //string tdate = data[1];
            string fcmbroker = data[0];     //FCM/Broker
            string ibgroup = data[1];       //IB/Group
            string account = data[2];       //Account
            string symbol = data[3];        //Symbol
            //DateTime transactiondate = Convert.ToDateTime(tdate);
            DateTime transactiondate = Convert.ToDateTime(Transection_date);
            var query = (from db in dbConnection.user_transactions
                         join db1 in dbConnection.user_subscription on db.user_subscription_id equals db1.user_subscription_id
                         join db2 in dbConnection.user_master on db1.user_id equals db2.user_id
                         //where db.transaction_date == transactiondate && db2.user_email == email
                         where db.transaction_date == transactiondate && db.symbol == symbol && db2.account_number == account && db2.IB == ibgroup && db2.FCM == fcmbroker
                         select db).FirstOrDefault();
            if (query == null)
            {

                //string date = data[1];          //transaction date       
                //string fcmbroker = data[2];     //FCM/Broker
                //string ibgroup = data[3];       //IB/Group
                //string account = data[4];       //Account

                string exchange = data[4];                      //Exchange
                string closedpnl = data[5];                     //ClosedPnl
                string netpnl = data[6];                        //NetPnl
                string commission = data[7];                    //Commissions
                string trades = data[8];                        //Trades
                string wintrade = data[9];                      //WinTrades
                string losertrade = data[10];                   //LoserTrades
                string winpct = data[11];                       //WinPct
                string loserpct = data[12];                     //LoserPct
                string buyqty = data[13];                       //BuyQty
                string sellqty = data[14];                      //SellQty
                string netqty = data[15];                       //NetQty
                string qtytrades = data[16];                    //QtyTraded
                string notradingdays = data[17];                //NoTradingDays
                string cashbal = data[18];                      //CashBal
                string acctbal = data[19];                      //AcctBal
                string autoliquidatethresholdvalue = data[20];  //AutoLiquidateThresholdValue
                string highpnl = data[21];                      //HighPnl
                string lowpnl = data[22];                       //LowPnl
                string avgwin = data[23];                       //AvgWin
                string avgloss = data[24];                      //AvgLoss
                string maxconsecwin = data[25];                 //MaxConsecWin
                string maxconsecloss = data[26];                //MaxConsecLoss
                string avgwinduration = data[27];               //AvgWinDuration
                string avglossduration = data[28];              //AvgLossDuration
                var user_subscriptionID = (from db2 in dbConnection.user_master
                                           join US in dbConnection.user_subscription on db2.user_id equals US.user_id
                                           where db2.account_number == account && db2.IB == ibgroup && db2.FCM == fcmbroker && US.is_current == true
                                           select US.user_subscription_id).FirstOrDefault();
                user_transactions obj = new user_transactions();
                obj.user_subscription_id = user_subscriptionID;
                obj.current_balance = cashbal == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(cashbal);
                obj.high_balance = 0;   //?
                obj.low_balance = 0;    //?
                obj.avg_winning_day = avgwinduration == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(avgwinduration);
                obj.avg_losing_day = avglossduration == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(avglossduration);
                obj.best_day = 0;       //?
                obj.worst_day = 0;      //?
                obj.net_profit_loss = netpnl == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(netpnl);
                obj.high_profit_loss = highpnl == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(highpnl);
                obj.low_profit_loss = lowpnl == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(lowpnl);
                obj.total_contracts = 0;//?
                obj.total_commision = commission == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(commission);
                obj.total_trades = trades == "" ? Convert.ToInt32("0") : Convert.ToInt32(trades);
                obj.avg_winning_trade = avgwin == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(avgwin);
                obj.avg_losing_trader = avgloss == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(avgloss);
                obj.winning_trade = wintrade == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(wintrade);
                obj.losing_trade = losertrade == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(losertrade);
                obj.transaction_date = Convert.ToDateTime(Transection_date);
                obj.profit_target = 0; //?
                obj.daily_loss_limit = 0;//?
                obj.max_drowdown = 0;  //?
                obj.exchange = string.IsNullOrEmpty(exchange) ? string.Empty : exchange;  //?
                obj.maintain_account_balance = acctbal == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(acctbal);
                obj.trade_required_no_of_days = notradingdays == "" ? Convert.ToInt32("0") : Convert.ToInt32(notradingdays);
                obj.symbol = symbol;
                obj.max_cons_win = maxconsecwin == "" ? Convert.ToInt32("0") : Convert.ToInt32(maxconsecwin);
                obj.max_cons_loss = maxconsecloss == "" ? Convert.ToInt32("0") : Convert.ToInt32(maxconsecloss);
                obj.losing_trade_per = loserpct == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(loserpct);
                obj.winning_trade_per = winpct == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(winpct);
                obj.created_date = DateTime.UtcNow;
                obj.updated_date = DateTime.UtcNow;
                obj.is_active = true;
                obj.is_deleted = false;
                dbConnection.user_transactions.Add(obj);
                dbConnection.SaveChanges();
            }
            return model;
        }

        /// <summary>
        /// csv to DB
        /// dev by : Hardik Savaliya
        /// date : 16/08/2017
        /// </summary>
        /// <param name="data"></param>
        /// <param name="Transection_date"></param>
        /// <returns></returns>
        public UserTransactionModel csvtodbACReport(string[] data, string Transection_date)
        {
            UserTransactionModel model = new UserTransactionModel();

            //string tdate = data[1];
            string fcmbroker = data[0];     //FCM/Broker
            string ibgroup = data[1];       //IB/Group
            string account = data[2];       //Account

            //DateTime transactiondate = Convert.ToDateTime(tdate);
            DateTime transactiondate = Convert.ToDateTime(Transection_date);
            var query = (from db in dbConnection.balance_history
                         join db1 in dbConnection.user_subscription on db.user_subscription_id equals db1.user_subscription_id
                         join db2 in dbConnection.user_master on db1.user_id equals db2.user_id
                         where db.transaction_date == transactiondate && db2.account_number == account && db2.IB == ibgroup && db2.FCM == fcmbroker
                         select db).FirstOrDefault();
            if (query == null)
            {
                string closedpnl = data[3];     //ClosedPnl
                string netpnl = data[4];        //NetPnl
                string commission = data[5];    //Commissions
                string trades = data[6];       //Trades
                string wintrade = data[7];     //WinTrades
                string losertrade = data[8];   //LoserTrades
                string cashbal = data[16];      //CashBal
                string acctbal = data[17];      //AcctBal
                string liquidatethresholdvalue = data[18]; //liquidatethresholdvalue
                var user_subscriptionID = (from db2 in dbConnection.user_master
                                           join US in dbConnection.user_subscription on db2.user_id equals US.user_id
                                           where db2.account_number == account && db2.IB == ibgroup && db2.FCM == fcmbroker && US.is_current == true
                                           select US.user_subscription_id).FirstOrDefault();

                balance_history objBalanceHistory = new balance_history();
                objBalanceHistory.user_subscription_id = user_subscriptionID;
                objBalanceHistory.current_balance = acctbal == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(acctbal);
                objBalanceHistory.cash_balance = cashbal == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(cashbal);
                objBalanceHistory.net_pnl = netpnl == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(netpnl);
                objBalanceHistory.closed_pnl = closedpnl == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(closedpnl);
                objBalanceHistory.commision = commission == "" ? Convert.ToDecimal("0") : Convert.ToDecimal(commission);
                objBalanceHistory.trades = trades == "" ? Convert.ToInt32("0") : Convert.ToInt32(trades);
                objBalanceHistory.winning_trades = wintrade == "" ? Convert.ToInt32("0") : Convert.ToInt32(wintrade);
                objBalanceHistory.losing_trades = losertrade == "" ? Convert.ToInt32("0") : Convert.ToInt32(losertrade);
                objBalanceHistory.auto_liquidate_threshold_value = liquidatethresholdvalue == "" ? Convert.ToInt32("0") : Convert.ToDecimal(liquidatethresholdvalue);
                objBalanceHistory.transaction_date = Convert.ToDateTime(Transection_date);
                objBalanceHistory.trading_day_status = (netpnl == "0.000000" ? 2 : (Convert.ToDecimal(netpnl) > 0 ? 1 : (Convert.ToDecimal(netpnl) < 0 ? 0 : 1)));
                dbConnection.balance_history.Add(objBalanceHistory);
                dbConnection.SaveChanges();
            }
            return model;
        }
        #endregion

        #region Get Member Data For CSV
        /// <summary>
        /// GetDataForCSV
        /// </summary>
        /// dev by : Bharat Katua
        /// date : 26/07/2017
        /// <param name="userID"></param>
        /// <returns></returns>
        public UserMasterModel GetDataForCSV(Int64 userID = 0)
        {
            try
            {
                UserMasterModel objmodel = new UserMasterModel();

                var Query = (from UM in dbConnection.user_master
                                 //join CM in dbConnection.country_master on UM.country_id equals CM.country_id
                                 //Commented on 26/07/2017 due to state_master change
                                 //join SM in dbConnection.state_master on UM.state_id equals SM.state_id
                             where UM.is_active == true && UM.user_id == userID
                             //select new { UM, CM }).FirstOrDefault();
                             select new { UM }).FirstOrDefault();

                if (Query != null)
                {
                    objmodel = new UserMasterModel();
                    objmodel.user_id = Query.UM.user_id;
                    objmodel.first_name = Query.UM.first_name == null ? "NA" : Query.UM.first_name;
                    objmodel.last_name = Query.UM.last_name == null ? "NA" : Query.UM.last_name;
                    objmodel.user_email = Query.UM.user_email == null ? "NA" : Query.UM.user_email;
                    objmodel.address = Query.UM.address == null ? "NA" : Query.UM.address;
                    objmodel.city_name = Query.UM.city_name == null ? "NA" : Query.UM.city_name;
                    objmodel.country_id = Query.UM.country_id.Value;
                    objmodel.country_name = dbConnection.country_master.Where(x => x.country_id == objmodel.country_id).Select(x => x.country_name).FirstOrDefault(); // Query.CM.country_name == null ? "NA" : Query.CM.country_name;
                    objmodel.country_name = string.IsNullOrEmpty(objmodel.country_name) ? string.Empty : objmodel.country_name;
                    objmodel.state_name = Query.UM.state_name == null ? "NA" : Query.UM.state_name;
                    objmodel.postal_code = Query.UM.postal_code == null ? "NA" : Query.UM.postal_code;
                    objmodel.mobile_number = Query.UM.contact_number == null ? "NA" : Query.UM.contact_number;
                    string encPassword = Query.UM.password;
                    string CryptoKey = Query.UM.crypto_key;
                    objmodel.password = Utilities.DecryptPassword(encPassword, CryptoKey);
                    objmodel.subscription_id = dbConnection.user_subscription.Where(x => x.user_id == objmodel.user_id).OrderByDescending(x => x.user_subscription_id).FirstOrDefault().subscription_id;

                    return objmodel;
                }
                return objmodel;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Update TT Account User Details
        public bool UpdateAccountUserPlatform(long user_id, long lTTAccountId = 0, string sAccountAllias = "", int iPlatformId = 0)
        {
            user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == user_id).FirstOrDefault();
            if (objUserMaster != null)
            {
                objUserMaster.account_number = sAccountAllias.ToString();
                objUserMaster.tt_account_id = lTTAccountId;
                List<user_platform> lstUserPlatform = dbConnection.user_platform.Where(x => x.user_id == user_id && x.is_delete != true).ToList();
                lstUserPlatform.ForEach(x => x.is_active = false);
                user_platform objUserPlatform = new user_platform();
                objUserPlatform.account_alias = sAccountAllias;
                objUserPlatform.account_id = lTTAccountId;
                //objUserPlatform.account_user_id = lTTUserId.ToString();
                objUserPlatform.created_by = user_id;
                objUserPlatform.created_on = System.DateTime.UtcNow;
                objUserPlatform.is_active = true;
                objUserPlatform.is_delete = false;
                if (iPlatformId == (Int32)EnumTradingPlatform.Rithmic)
                    objUserPlatform.platform_id = (Int32)EnumTradingPlatform.Rithmic;
                else if (iPlatformId == (Int32)EnumTradingPlatform.TradingTechnologies)
                    objUserPlatform.platform_id = (Int32)EnumTradingPlatform.TradingTechnologies;
                objUserPlatform.user_id = user_id;
                dbConnection.user_platform.Add(objUserPlatform);
                dbConnection.SaveChanges();
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Add TT Platform
        public bool AddtoTTPlatform(MemberModel objMemberModel, bool is_demo)
        {
            UserService objUserService = new UserService();
            //Ok...so let's choose TDyymmdd[Initials][C if Challenge, D if Demo][Add sequential number if not unique] e.g. 
            //TD181122DHC would be my account I'd if I joined the Challenge today. 

            bool bStatus = objUserService.UpdateAccountUserPlatform(objMemberModel.user_id, 0, objMemberModel.AccountAllias, (Int32)EnumTradingPlatform.TradingTechnologies);
            //TTService objTTService = new TTService();
            //UserService objUserService = new UserService();
            //TokenResponseClass objTokenResponseClass = new TokenResponseClass();
            //objTokenResponseClass = objTTService.GetToken().Result;
            //CommonFunc.WriteLog("Token Creted" + objTokenResponseClass.access_token, LogWriterType.Log);
            //FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(1,
            //                "TTToken",
            //                DateTime.Now,
            //                DateTime.Now.AddHours(11), //12 hours validated
            //                true,
            //                objTokenResponseClass.access_token,
            //                FormsAuthentication.FormsCookiePath);
            //// Encrypt the ticket.
            //string encTicket = FormsAuthentication.Encrypt(ticket);
            //// Create the cookie.
            //if (objTokenResponseClass.status.ToLower() == "ok")
            //{
            //    if (objTokenResponseClass.seconds_until_expiry > 600)
            //    {
            //        //add account
            //        List<string> lstTTAccountId = objTTService.AddAccount(objUserMasterModel.user_id, objTokenResponseClass);
            //        CommonFunc.WriteLog("Add Account" + lstTTAccountId[0] + "," + lstTTAccountId[1] + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //        if (lstTTAccountId.Count() > 0)
            //        {
            //            //add user
            //            long lTTUserId = objTTService.AddUser(objUserMasterModel.user_id, lstTTAccountId.ElementAt(0), objUserMasterModel, objTokenResponseClass);
            //            CommonFunc.WriteLog("Add User" + lTTUserId + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //            if (lTTUserId > 0)
            //            {
            //                //map Account and user id to user_master table
            //                objUserService = new UserService();
            //                bool bStatus = objUserService.UpdateAccountUserDetails(objUserMasterModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, lstTTAccountId.ElementAt(1), (Int32)EnumTradingPlatform.TradingTechnologies);
            //                if (bStatus)
            //                {
            //                    //map account user
            //                    long lAccountUser = objTTService.MapAccountUser(objUserMasterModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, objTokenResponseClass);
            //                    CommonFunc.WriteLog("Update Map Account User" + lAccountUser + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //                    if (lAccountUser > 0)
            //                    {
            //                        ////Trade Restrictions
            //                        //bool bTradeRestrictions = objTTService.MapTradeRestrictions(objMemberModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, objTokenResponseClass);
            //                        //CommonFunc.WriteLog("Update Trade Restrictions" + bTradeRestrictions + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //                        ////map market
            //                        //long lMarketMapping = objTTService.MapAccountMarket(objMemberModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, objTokenResponseClass);
            //                        //CommonFunc.WriteLog("Update Market Mapping" + lMarketMapping + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //                        //TTGetAccountResponse objTTGetAccountResponse = new TTGetAccountResponse();
            //                        //objTTGetAccountResponse = objTTService.GetAccount(objTokenResponseClass, lstTTAccountId.ElementAt(0)).Result;
            //                        //if (objTTGetAccountResponse.riskLimitSettings != null)
            //                        //{
            //                        //    long lMarketRiskLimitSetting = objTTService.MapMarketRiskLimitSetting(objMemberModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, objTokenResponseClass, objTTGetAccountResponse);
            //                        //    CommonFunc.WriteLog("Update Risk Limit setting" + lMarketRiskLimitSetting + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //                        //}
            //                        //if (objTTGetAccountResponse.riskSettings != null)
            //                        //{
            //                        //    long lRiskSetting = objTTService.MapRiskSetting(objMemberModel.user_id, lstTTAccountId.ElementAt(0), lTTUserId, objTokenResponseClass, objTTGetAccountResponse);
            //                        //    CommonFunc.WriteLog("Update Risk setting" + lRiskSetting + "Date:" + System.DateTime.UtcNow, LogWriterType.Log);
            //                        //}
            //                        //CommonFunc.WriteLog("Done TT", LogWriterType.Log);
            //                    }
            //                    else
            //                    {
            //                        //failed to map account user
            //                    }
            //                }
            //                else
            //                {
            //                    //failed to map account user
            //                }
            //            }
            //            else
            //            {
            //                //failed to create user
            //            }
            //        }
            //        else
            //        {
            //            //failed to create account
            //        }
            //    }
            //}

            return true;
        }
        #endregion

        #region Account Alliad Name
        public string GetAccountAlliasName(bool is_demo, string sFirstInitial, string sLastInitial)
        {
            int iCount = dbConnection.user_master.ToList().Where(x => x.created_on.Date == System.DateTime.UtcNow.Date).Count();
            string sAccountAllias = (is_demo == true ? "D" : "C") + "TD" + DateTime.UtcNow.ToString("yyMMdd")
                    + sFirstInitial
                    + sLastInitial
                    + (iCount + 1);
            return sAccountAllias;
        }
        #endregion

        #region Get User Account List
        public List<UserPlatform> GetUserAccountList(long lUserId)
        {
            List<UserPlatform> lstUserPlatform = new List<UserPlatform>();
            UserPlatform objUserPlatform = new UserPlatform();
            //add tt Platform account 
            List<user_platform> lstUserPlatformMaster = dbConnection.user_platform.Where(x => x.user_id == lUserId && x.is_delete != true).ToList();
            //add tt platform for old data
            List<tt_fill_master_manual> lstFillMasterManual = dbConnection.tt_fill_master_manual.Where(x => x.user_id == lUserId).ToList();

            List<string> lstAccount = new List<string>();
            lstAccount = lstUserPlatformMaster.Select(x => x.account_alias).Distinct().ToList();
            foreach (var item in lstAccount)
            {
                objUserPlatform = new UserPlatform();
                objUserPlatform.account_alias = item;
                lstUserPlatform.Add(objUserPlatform);
            }
            return lstUserPlatform;
        }
        #endregion

        #region Get User Account List Details
        public List<UserPlatform> GetUserAccountListDetails(long lUserId)
        {
            List<UserPlatform> lstUserPlatform = new List<UserPlatform>();
            UserPlatform objUserPlatform;
            //add tt Platform account 
            List<user_platform> lstUserMaster = dbConnection.user_platform.Where(x => x.user_id == lUserId && x.is_delete != true).ToList();
            foreach (var item in lstUserMaster)
            {
                objUserPlatform = new UserPlatform();
                objUserPlatform.account_alias = item.account_alias;
                objUserPlatform.account_id = item.account_id;
                //objUserPlatform.account_user_id = item.account_user_id;
                objUserPlatform.created_by = item.created_by;
                objUserPlatform.created_on = item.created_on;
                objUserPlatform.is_active = item.is_active;
                objUserPlatform.is_delete = item.is_delete;
                objUserPlatform.platform_id = item.platform_id;
                objUserPlatform.user_id = item.user_id;
                objUserPlatform.user_platform_id = item.user_platform_id;

                lstUserPlatform.Add(objUserPlatform);
            }
            return lstUserPlatform;
        }
        #endregion

        #region Delete TT Account User Details
        public bool DeleteAccountUserPlatform(long user_id, string[] sAccountAllias, int iPlatformId = 0)
        {
            if (sAccountAllias.Count() > 0)
            {
                foreach (var item in sAccountAllias)
                {
                    user_master objUserMaster = dbConnection.user_master.Where(x => x.user_id == user_id).FirstOrDefault();
                    List<user_platform> lstUserPlatformAll = dbConnection.user_platform.Where(x => x.user_id == user_id && x.is_delete != true).ToList();
                    if (objUserMaster != null && lstUserPlatformAll.Count() > 0)
                    {
                        if (lstUserPlatformAll.Count == 1)
                        {
                            return false;
                        }
                        user_platform filterUserPlatform = lstUserPlatformAll.Where(x => x.account_alias.Trim().ToLower().Equals(item.Trim().ToLower())).FirstOrDefault();
                        if (iPlatformId == (Int32)EnumTradingPlatform.TradingTechnologies)
                        {
                            List<tt_fill_master_manual> lstFillMasterManual = dbConnection.tt_fill_master_manual.Where(x => x.account_no.Trim().ToLower().Equals(filterUserPlatform.account_alias.Trim().ToLower())).ToList();
                            dbConnection.tt_fill_master_manual.RemoveRange(lstFillMasterManual);
                        }
                        dbConnection.user_platform.Remove(filterUserPlatform);
                        lstUserPlatformAll.Remove(filterUserPlatform);
                        if (lstUserPlatformAll.Count() > 0)
                        {
                            lstUserPlatformAll.LastOrDefault().is_active = true;
                            lstUserPlatformAll.LastOrDefault().modified_on = System.DateTime.UtcNow;
                            var LastAccount = lstUserPlatformAll.LastOrDefault();
                            objUserMaster.account_number = LastAccount.account_alias;
                            if (LastAccount.account_id > 0)
                            {
                                objUserMaster.tt_account_id = Convert.ToInt64(LastAccount.account_id);
                            }
                        }
                        dbConnection.SaveChanges();
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            else
                return false;
        }
        #endregion

        #region Get Account List with account id
        public List<UserPlatform> GetUserAccountNumberList()
        {
            List<UserPlatform> lstUserPlatform = new List<UserPlatform>();
            UserPlatform objUserPlatform = new UserPlatform();
            //add tt Platform account 
            List<user_platform> lstUserPlatformMaster = dbConnection.user_platform.Where(x => x.is_delete != true).ToList();
            var lstAccount = lstUserPlatformMaster.GroupBy(x => x.account_alias, (key, group) => group.First());
            foreach (var item in lstAccount)
            {
                objUserPlatform = new UserPlatform();
                objUserPlatform.account_id = item.account_id;
                objUserPlatform.account_alias = item.account_alias;
                objUserPlatform.full_name_with_alias = item.account_alias + " (" + item.user_master.first_name + " " + item.user_master.last_name + ")";
                objUserPlatform.user_id = item.user_id;
                lstUserPlatform.Add(objUserPlatform);
            }
            return lstUserPlatform;
        }
        #endregion

        #region Get User Via Email
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        public UserMasterModel GetUserViaEmail(string strEmail)
        {
            try
            {
                if (!string.IsNullOrEmpty(strEmail))
                {
                    UserMasterModel objUserMasterModel = new UserMasterModel();
                    user_master objUserMaster = dbConnection.user_master.Where(x => x.user_email.Trim().ToLower().Equals(strEmail.Trim().ToLower())).FirstOrDefault();
                    if (objUserMaster != null)
                    {
                        return GetUser(objUserMaster.user_id);
                    }
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion

        #region Resend Mail
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEmail"></param>
        /// <returns></returns>
        public bool ResendMail(string strEmail)
        {
            try
            {
                if (!string.IsNullOrEmpty(strEmail))
                {
                    UserMasterModel objUserMasterModel = GetUserViaEmail(strEmail);
                    if (objUserMasterModel.email_sent_count <= 3)
                    {
                        CommonService objCommonService = new CommonService();
                        Dictionary<string, string> parameters = new Dictionary<string, string>();
                        parameters.Add(EmailTemplateParameters.link, System.Configuration.ConfigurationManager.AppSettings["SiteLink"] + "/Account/ConfirmRegistration?email=" + objUserMasterModel.user_email + "&&key=" + objUserMasterModel.crypto_key);
                        parameters.Add(EmailTemplateParameters.Email, strEmail);
                        parameters.Add(EmailTemplateParameters.FullName, objUserMasterModel.first_name);
                        parameters.Add(EmailTemplateParameters.Password, objUserMasterModel.password);
                        new CommonService().SendEmail(objUserMasterModel.user_email, EmailTemplateNames.WelcomeFreeSubscriptionTT, parameters, null);
                        bool bEmailCountUpdate = objCommonService.UpdateEmailSentCount(objUserMasterModel.user_email);
                        if (bEmailCountUpdate)
                            return true;
                        else
                            return false;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Get User Via Account Allias
        /// <summary>
        /// 
        /// </summary>
        /// <param name="saccountallias"></param>
        /// <returns></returns>
        public long GetUserViaAccountAllias(string saccountallias)
        {
            try
            {
                if (!string.IsNullOrEmpty(saccountallias))
                {
                    user_platform objUserPlatform = dbConnection.user_platform.Where(x => x.account_alias.Trim().ToLower().Equals(saccountallias.Trim().ToLower())).FirstOrDefault();
                    if (objUserPlatform != null)
                    {
                        if (objUserPlatform.user_id != null)
                            return objUserPlatform.user_id.Value;
                        else
                            return 0;
                    }
                    else
                        return 0;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                throw new ServiceLayerException(ex);
            }
        }
        #endregion
    }
}